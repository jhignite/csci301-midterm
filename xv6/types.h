typedef unsigned int   uint;
typedef unsigned short ushort;
typedef unsigned char  uchar;
typedef uint pde_t;

struct capture_stats {
    int tick_count;
    int proc_exit_count;
    int proc_tick_count;
    int proc_firstrun_count;
};

struct syscallstats_s {
	int counts[256];
	int ticks[256];
};

enum scheduler_types { SCHED_ROUND_ROBIN, SCHED_FIFO, SCHED_LIFO, SCHED_MLFQ };

