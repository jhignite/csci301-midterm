
_syscallstats:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	83 e4 f0             	and    $0xfffffff0,%esp
	syscallstats();
   6:	e8 25 03 00 00       	call   330 <syscallstats>
	return 1;
   b:	b8 01 00 00 00       	mov    $0x1,%eax
}
  10:	89 ec                	mov    %ebp,%esp
  12:	5d                   	pop    %ebp
  13:	c3                   	ret    

00000014 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
  14:	55                   	push   %ebp
  15:	89 e5                	mov    %esp,%ebp
  17:	57                   	push   %edi
  18:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
  19:	8b 4d 08             	mov    0x8(%ebp),%ecx
  1c:	8b 55 10             	mov    0x10(%ebp),%edx
  1f:	8b 45 0c             	mov    0xc(%ebp),%eax
  22:	89 cb                	mov    %ecx,%ebx
  24:	89 df                	mov    %ebx,%edi
  26:	89 d1                	mov    %edx,%ecx
  28:	fc                   	cld    
  29:	f3 aa                	rep stos %al,%es:(%edi)
  2b:	89 ca                	mov    %ecx,%edx
  2d:	89 fb                	mov    %edi,%ebx
  2f:	89 5d 08             	mov    %ebx,0x8(%ebp)
  32:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
  35:	5b                   	pop    %ebx
  36:	5f                   	pop    %edi
  37:	5d                   	pop    %ebp
  38:	c3                   	ret    

00000039 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
  39:	55                   	push   %ebp
  3a:	89 e5                	mov    %esp,%ebp
  3c:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
  3f:	8b 45 08             	mov    0x8(%ebp),%eax
  42:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
  45:	8b 45 0c             	mov    0xc(%ebp),%eax
  48:	0f b6 10             	movzbl (%eax),%edx
  4b:	8b 45 08             	mov    0x8(%ebp),%eax
  4e:	88 10                	mov    %dl,(%eax)
  50:	8b 45 08             	mov    0x8(%ebp),%eax
  53:	0f b6 00             	movzbl (%eax),%eax
  56:	84 c0                	test   %al,%al
  58:	0f 95 c0             	setne  %al
  5b:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  5f:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
  63:	84 c0                	test   %al,%al
  65:	75 de                	jne    45 <strcpy+0xc>
    ;
  return os;
  67:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  6a:	c9                   	leave  
  6b:	c3                   	ret    

0000006c <strcmp>:

int
strcmp(const char *p, const char *q)
{
  6c:	55                   	push   %ebp
  6d:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
  6f:	eb 08                	jmp    79 <strcmp+0xd>
    p++, q++;
  71:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  75:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
  79:	8b 45 08             	mov    0x8(%ebp),%eax
  7c:	0f b6 00             	movzbl (%eax),%eax
  7f:	84 c0                	test   %al,%al
  81:	74 10                	je     93 <strcmp+0x27>
  83:	8b 45 08             	mov    0x8(%ebp),%eax
  86:	0f b6 10             	movzbl (%eax),%edx
  89:	8b 45 0c             	mov    0xc(%ebp),%eax
  8c:	0f b6 00             	movzbl (%eax),%eax
  8f:	38 c2                	cmp    %al,%dl
  91:	74 de                	je     71 <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
  93:	8b 45 08             	mov    0x8(%ebp),%eax
  96:	0f b6 00             	movzbl (%eax),%eax
  99:	0f b6 d0             	movzbl %al,%edx
  9c:	8b 45 0c             	mov    0xc(%ebp),%eax
  9f:	0f b6 00             	movzbl (%eax),%eax
  a2:	0f b6 c0             	movzbl %al,%eax
  a5:	89 d1                	mov    %edx,%ecx
  a7:	29 c1                	sub    %eax,%ecx
  a9:	89 c8                	mov    %ecx,%eax
}
  ab:	5d                   	pop    %ebp
  ac:	c3                   	ret    

000000ad <strlen>:

uint
strlen(char *s)
{
  ad:	55                   	push   %ebp
  ae:	89 e5                	mov    %esp,%ebp
  b0:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
  b3:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  ba:	eb 04                	jmp    c0 <strlen+0x13>
  bc:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  c0:	8b 45 fc             	mov    -0x4(%ebp),%eax
  c3:	03 45 08             	add    0x8(%ebp),%eax
  c6:	0f b6 00             	movzbl (%eax),%eax
  c9:	84 c0                	test   %al,%al
  cb:	75 ef                	jne    bc <strlen+0xf>
    ;
  return n;
  cd:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  d0:	c9                   	leave  
  d1:	c3                   	ret    

000000d2 <memset>:

void*
memset(void *dst, int c, uint n)
{
  d2:	55                   	push   %ebp
  d3:	89 e5                	mov    %esp,%ebp
  d5:	83 ec 0c             	sub    $0xc,%esp
  stosb(dst, c, n);
  d8:	8b 45 10             	mov    0x10(%ebp),%eax
  db:	89 44 24 08          	mov    %eax,0x8(%esp)
  df:	8b 45 0c             	mov    0xc(%ebp),%eax
  e2:	89 44 24 04          	mov    %eax,0x4(%esp)
  e6:	8b 45 08             	mov    0x8(%ebp),%eax
  e9:	89 04 24             	mov    %eax,(%esp)
  ec:	e8 23 ff ff ff       	call   14 <stosb>
  return dst;
  f1:	8b 45 08             	mov    0x8(%ebp),%eax
}
  f4:	c9                   	leave  
  f5:	c3                   	ret    

000000f6 <strchr>:

char*
strchr(const char *s, char c)
{
  f6:	55                   	push   %ebp
  f7:	89 e5                	mov    %esp,%ebp
  f9:	83 ec 04             	sub    $0x4,%esp
  fc:	8b 45 0c             	mov    0xc(%ebp),%eax
  ff:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 102:	eb 14                	jmp    118 <strchr+0x22>
    if(*s == c)
 104:	8b 45 08             	mov    0x8(%ebp),%eax
 107:	0f b6 00             	movzbl (%eax),%eax
 10a:	3a 45 fc             	cmp    -0x4(%ebp),%al
 10d:	75 05                	jne    114 <strchr+0x1e>
      return (char*)s;
 10f:	8b 45 08             	mov    0x8(%ebp),%eax
 112:	eb 13                	jmp    127 <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 114:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 118:	8b 45 08             	mov    0x8(%ebp),%eax
 11b:	0f b6 00             	movzbl (%eax),%eax
 11e:	84 c0                	test   %al,%al
 120:	75 e2                	jne    104 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 122:	b8 00 00 00 00       	mov    $0x0,%eax
}
 127:	c9                   	leave  
 128:	c3                   	ret    

00000129 <gets>:

char*
gets(char *buf, int max)
{
 129:	55                   	push   %ebp
 12a:	89 e5                	mov    %esp,%ebp
 12c:	83 ec 28             	sub    $0x28,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 12f:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 136:	eb 44                	jmp    17c <gets+0x53>
    cc = read(0, &c, 1);
 138:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 13f:	00 
 140:	8d 45 ef             	lea    -0x11(%ebp),%eax
 143:	89 44 24 04          	mov    %eax,0x4(%esp)
 147:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 14e:	e8 3d 01 00 00       	call   290 <read>
 153:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(cc < 1)
 156:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 15a:	7e 2d                	jle    189 <gets+0x60>
      break;
    buf[i++] = c;
 15c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 15f:	03 45 08             	add    0x8(%ebp),%eax
 162:	0f b6 55 ef          	movzbl -0x11(%ebp),%edx
 166:	88 10                	mov    %dl,(%eax)
 168:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
    if(c == '\n' || c == '\r')
 16c:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 170:	3c 0a                	cmp    $0xa,%al
 172:	74 16                	je     18a <gets+0x61>
 174:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 178:	3c 0d                	cmp    $0xd,%al
 17a:	74 0e                	je     18a <gets+0x61>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 17c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 17f:	83 c0 01             	add    $0x1,%eax
 182:	3b 45 0c             	cmp    0xc(%ebp),%eax
 185:	7c b1                	jl     138 <gets+0xf>
 187:	eb 01                	jmp    18a <gets+0x61>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 189:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 18a:	8b 45 f0             	mov    -0x10(%ebp),%eax
 18d:	03 45 08             	add    0x8(%ebp),%eax
 190:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 193:	8b 45 08             	mov    0x8(%ebp),%eax
}
 196:	c9                   	leave  
 197:	c3                   	ret    

00000198 <stat>:

int
stat(char *n, struct stat *st)
{
 198:	55                   	push   %ebp
 199:	89 e5                	mov    %esp,%ebp
 19b:	83 ec 28             	sub    $0x28,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 19e:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 1a5:	00 
 1a6:	8b 45 08             	mov    0x8(%ebp),%eax
 1a9:	89 04 24             	mov    %eax,(%esp)
 1ac:	e8 07 01 00 00       	call   2b8 <open>
 1b1:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(fd < 0)
 1b4:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 1b8:	79 07                	jns    1c1 <stat+0x29>
    return -1;
 1ba:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 1bf:	eb 23                	jmp    1e4 <stat+0x4c>
  r = fstat(fd, st);
 1c1:	8b 45 0c             	mov    0xc(%ebp),%eax
 1c4:	89 44 24 04          	mov    %eax,0x4(%esp)
 1c8:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1cb:	89 04 24             	mov    %eax,(%esp)
 1ce:	e8 fd 00 00 00       	call   2d0 <fstat>
 1d3:	89 45 f4             	mov    %eax,-0xc(%ebp)
  close(fd);
 1d6:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1d9:	89 04 24             	mov    %eax,(%esp)
 1dc:	e8 bf 00 00 00       	call   2a0 <close>
  return r;
 1e1:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
 1e4:	c9                   	leave  
 1e5:	c3                   	ret    

000001e6 <atoi>:

int
atoi(const char *s)
{
 1e6:	55                   	push   %ebp
 1e7:	89 e5                	mov    %esp,%ebp
 1e9:	83 ec 10             	sub    $0x10,%esp
  int n;

  n = 0;
 1ec:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while('0' <= *s && *s <= '9')
 1f3:	eb 24                	jmp    219 <atoi+0x33>
    n = n*10 + *s++ - '0';
 1f5:	8b 55 fc             	mov    -0x4(%ebp),%edx
 1f8:	89 d0                	mov    %edx,%eax
 1fa:	c1 e0 02             	shl    $0x2,%eax
 1fd:	01 d0                	add    %edx,%eax
 1ff:	01 c0                	add    %eax,%eax
 201:	89 c2                	mov    %eax,%edx
 203:	8b 45 08             	mov    0x8(%ebp),%eax
 206:	0f b6 00             	movzbl (%eax),%eax
 209:	0f be c0             	movsbl %al,%eax
 20c:	8d 04 02             	lea    (%edx,%eax,1),%eax
 20f:	83 e8 30             	sub    $0x30,%eax
 212:	89 45 fc             	mov    %eax,-0x4(%ebp)
 215:	83 45 08 01          	addl   $0x1,0x8(%ebp)
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 219:	8b 45 08             	mov    0x8(%ebp),%eax
 21c:	0f b6 00             	movzbl (%eax),%eax
 21f:	3c 2f                	cmp    $0x2f,%al
 221:	7e 0a                	jle    22d <atoi+0x47>
 223:	8b 45 08             	mov    0x8(%ebp),%eax
 226:	0f b6 00             	movzbl (%eax),%eax
 229:	3c 39                	cmp    $0x39,%al
 22b:	7e c8                	jle    1f5 <atoi+0xf>
    n = n*10 + *s++ - '0';
  return n;
 22d:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 230:	c9                   	leave  
 231:	c3                   	ret    

00000232 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 232:	55                   	push   %ebp
 233:	89 e5                	mov    %esp,%ebp
 235:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 238:	8b 45 08             	mov    0x8(%ebp),%eax
 23b:	89 45 f8             	mov    %eax,-0x8(%ebp)
  src = vsrc;
 23e:	8b 45 0c             	mov    0xc(%ebp),%eax
 241:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0)
 244:	eb 13                	jmp    259 <memmove+0x27>
    *dst++ = *src++;
 246:	8b 45 fc             	mov    -0x4(%ebp),%eax
 249:	0f b6 10             	movzbl (%eax),%edx
 24c:	8b 45 f8             	mov    -0x8(%ebp),%eax
 24f:	88 10                	mov    %dl,(%eax)
 251:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
 255:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 259:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
 25d:	0f 9f c0             	setg   %al
 260:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
 264:	84 c0                	test   %al,%al
 266:	75 de                	jne    246 <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 268:	8b 45 08             	mov    0x8(%ebp),%eax
}
 26b:	c9                   	leave  
 26c:	c3                   	ret    
 26d:	90                   	nop
 26e:	90                   	nop
 26f:	90                   	nop

00000270 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 270:	b8 01 00 00 00       	mov    $0x1,%eax
 275:	cd 40                	int    $0x40
 277:	c3                   	ret    

00000278 <exit>:
SYSCALL(exit)
 278:	b8 02 00 00 00       	mov    $0x2,%eax
 27d:	cd 40                	int    $0x40
 27f:	c3                   	ret    

00000280 <wait>:
SYSCALL(wait)
 280:	b8 03 00 00 00       	mov    $0x3,%eax
 285:	cd 40                	int    $0x40
 287:	c3                   	ret    

00000288 <pipe>:
SYSCALL(pipe)
 288:	b8 04 00 00 00       	mov    $0x4,%eax
 28d:	cd 40                	int    $0x40
 28f:	c3                   	ret    

00000290 <read>:
SYSCALL(read)
 290:	b8 05 00 00 00       	mov    $0x5,%eax
 295:	cd 40                	int    $0x40
 297:	c3                   	ret    

00000298 <write>:
SYSCALL(write)
 298:	b8 10 00 00 00       	mov    $0x10,%eax
 29d:	cd 40                	int    $0x40
 29f:	c3                   	ret    

000002a0 <close>:
SYSCALL(close)
 2a0:	b8 15 00 00 00       	mov    $0x15,%eax
 2a5:	cd 40                	int    $0x40
 2a7:	c3                   	ret    

000002a8 <kill>:
SYSCALL(kill)
 2a8:	b8 06 00 00 00       	mov    $0x6,%eax
 2ad:	cd 40                	int    $0x40
 2af:	c3                   	ret    

000002b0 <exec>:
SYSCALL(exec)
 2b0:	b8 07 00 00 00       	mov    $0x7,%eax
 2b5:	cd 40                	int    $0x40
 2b7:	c3                   	ret    

000002b8 <open>:
SYSCALL(open)
 2b8:	b8 0f 00 00 00       	mov    $0xf,%eax
 2bd:	cd 40                	int    $0x40
 2bf:	c3                   	ret    

000002c0 <mknod>:
SYSCALL(mknod)
 2c0:	b8 11 00 00 00       	mov    $0x11,%eax
 2c5:	cd 40                	int    $0x40
 2c7:	c3                   	ret    

000002c8 <unlink>:
SYSCALL(unlink)
 2c8:	b8 12 00 00 00       	mov    $0x12,%eax
 2cd:	cd 40                	int    $0x40
 2cf:	c3                   	ret    

000002d0 <fstat>:
SYSCALL(fstat)
 2d0:	b8 08 00 00 00       	mov    $0x8,%eax
 2d5:	cd 40                	int    $0x40
 2d7:	c3                   	ret    

000002d8 <link>:
SYSCALL(link)
 2d8:	b8 13 00 00 00       	mov    $0x13,%eax
 2dd:	cd 40                	int    $0x40
 2df:	c3                   	ret    

000002e0 <mkdir>:
SYSCALL(mkdir)
 2e0:	b8 14 00 00 00       	mov    $0x14,%eax
 2e5:	cd 40                	int    $0x40
 2e7:	c3                   	ret    

000002e8 <chdir>:
SYSCALL(chdir)
 2e8:	b8 09 00 00 00       	mov    $0x9,%eax
 2ed:	cd 40                	int    $0x40
 2ef:	c3                   	ret    

000002f0 <dup>:
SYSCALL(dup)
 2f0:	b8 0a 00 00 00       	mov    $0xa,%eax
 2f5:	cd 40                	int    $0x40
 2f7:	c3                   	ret    

000002f8 <getpid>:
SYSCALL(getpid)
 2f8:	b8 0b 00 00 00       	mov    $0xb,%eax
 2fd:	cd 40                	int    $0x40
 2ff:	c3                   	ret    

00000300 <sbrk>:
SYSCALL(sbrk)
 300:	b8 0c 00 00 00       	mov    $0xc,%eax
 305:	cd 40                	int    $0x40
 307:	c3                   	ret    

00000308 <sleep>:
SYSCALL(sleep)
 308:	b8 0d 00 00 00       	mov    $0xd,%eax
 30d:	cd 40                	int    $0x40
 30f:	c3                   	ret    

00000310 <uptime>:
SYSCALL(uptime)
 310:	b8 0e 00 00 00       	mov    $0xe,%eax
 315:	cd 40                	int    $0x40
 317:	c3                   	ret    

00000318 <start_capture>:
SYSCALL(start_capture)
 318:	b8 1e 00 00 00       	mov    $0x1e,%eax
 31d:	cd 40                	int    $0x40
 31f:	c3                   	ret    

00000320 <stop_capture>:
SYSCALL(stop_capture)
 320:	b8 1f 00 00 00       	mov    $0x1f,%eax
 325:	cd 40                	int    $0x40
 327:	c3                   	ret    

00000328 <switch_scheduler>:
SYSCALL(switch_scheduler)
 328:	b8 20 00 00 00       	mov    $0x20,%eax
 32d:	cd 40                	int    $0x40
 32f:	c3                   	ret    

00000330 <syscallstats>:
SYSCALL(syscallstats)
 330:	b8 21 00 00 00       	mov    $0x21,%eax
 335:	cd 40                	int    $0x40
 337:	c3                   	ret    

00000338 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 338:	55                   	push   %ebp
 339:	89 e5                	mov    %esp,%ebp
 33b:	83 ec 28             	sub    $0x28,%esp
 33e:	8b 45 0c             	mov    0xc(%ebp),%eax
 341:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 344:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 34b:	00 
 34c:	8d 45 f4             	lea    -0xc(%ebp),%eax
 34f:	89 44 24 04          	mov    %eax,0x4(%esp)
 353:	8b 45 08             	mov    0x8(%ebp),%eax
 356:	89 04 24             	mov    %eax,(%esp)
 359:	e8 3a ff ff ff       	call   298 <write>
}
 35e:	c9                   	leave  
 35f:	c3                   	ret    

00000360 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 360:	55                   	push   %ebp
 361:	89 e5                	mov    %esp,%ebp
 363:	53                   	push   %ebx
 364:	83 ec 44             	sub    $0x44,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 367:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 36e:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 372:	74 17                	je     38b <printint+0x2b>
 374:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 378:	79 11                	jns    38b <printint+0x2b>
    neg = 1;
 37a:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 381:	8b 45 0c             	mov    0xc(%ebp),%eax
 384:	f7 d8                	neg    %eax
 386:	89 45 f4             	mov    %eax,-0xc(%ebp)
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
 389:	eb 06                	jmp    391 <printint+0x31>
    neg = 1;
    x = -xx;
  } else {
    x = xx;
 38b:	8b 45 0c             	mov    0xc(%ebp),%eax
 38e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  }

  i = 0;
 391:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  do{
    buf[i++] = digits[x % base];
 398:	8b 4d ec             	mov    -0x14(%ebp),%ecx
 39b:	8b 5d 10             	mov    0x10(%ebp),%ebx
 39e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 3a1:	ba 00 00 00 00       	mov    $0x0,%edx
 3a6:	f7 f3                	div    %ebx
 3a8:	89 d0                	mov    %edx,%eax
 3aa:	0f b6 80 e4 07 00 00 	movzbl 0x7e4(%eax),%eax
 3b1:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
 3b5:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
  }while((x /= base) != 0);
 3b9:	8b 45 10             	mov    0x10(%ebp),%eax
 3bc:	89 45 d4             	mov    %eax,-0x2c(%ebp)
 3bf:	8b 45 f4             	mov    -0xc(%ebp),%eax
 3c2:	ba 00 00 00 00       	mov    $0x0,%edx
 3c7:	f7 75 d4             	divl   -0x2c(%ebp)
 3ca:	89 45 f4             	mov    %eax,-0xc(%ebp)
 3cd:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 3d1:	75 c5                	jne    398 <printint+0x38>
  if(neg)
 3d3:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 3d7:	74 2a                	je     403 <printint+0xa3>
    buf[i++] = '-';
 3d9:	8b 45 ec             	mov    -0x14(%ebp),%eax
 3dc:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)
 3e1:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)

  while(--i >= 0)
 3e5:	eb 1d                	jmp    404 <printint+0xa4>
    putc(fd, buf[i]);
 3e7:	8b 45 ec             	mov    -0x14(%ebp),%eax
 3ea:	0f b6 44 05 dc       	movzbl -0x24(%ebp,%eax,1),%eax
 3ef:	0f be c0             	movsbl %al,%eax
 3f2:	89 44 24 04          	mov    %eax,0x4(%esp)
 3f6:	8b 45 08             	mov    0x8(%ebp),%eax
 3f9:	89 04 24             	mov    %eax,(%esp)
 3fc:	e8 37 ff ff ff       	call   338 <putc>
 401:	eb 01                	jmp    404 <printint+0xa4>
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 403:	90                   	nop
 404:	83 6d ec 01          	subl   $0x1,-0x14(%ebp)
 408:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 40c:	79 d9                	jns    3e7 <printint+0x87>
    putc(fd, buf[i]);
}
 40e:	83 c4 44             	add    $0x44,%esp
 411:	5b                   	pop    %ebx
 412:	5d                   	pop    %ebp
 413:	c3                   	ret    

00000414 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 414:	55                   	push   %ebp
 415:	89 e5                	mov    %esp,%ebp
 417:	83 ec 38             	sub    $0x38,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 41a:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 421:	8d 45 0c             	lea    0xc(%ebp),%eax
 424:	83 c0 04             	add    $0x4,%eax
 427:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(i = 0; fmt[i]; i++){
 42a:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 431:	e9 7e 01 00 00       	jmp    5b4 <printf+0x1a0>
    c = fmt[i] & 0xff;
 436:	8b 55 0c             	mov    0xc(%ebp),%edx
 439:	8b 45 ec             	mov    -0x14(%ebp),%eax
 43c:	8d 04 02             	lea    (%edx,%eax,1),%eax
 43f:	0f b6 00             	movzbl (%eax),%eax
 442:	0f be c0             	movsbl %al,%eax
 445:	25 ff 00 00 00       	and    $0xff,%eax
 44a:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(state == 0){
 44d:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 451:	75 2c                	jne    47f <printf+0x6b>
      if(c == '%'){
 453:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 457:	75 0c                	jne    465 <printf+0x51>
        state = '%';
 459:	c7 45 f0 25 00 00 00 	movl   $0x25,-0x10(%ebp)
 460:	e9 4b 01 00 00       	jmp    5b0 <printf+0x19c>
      } else {
        putc(fd, c);
 465:	8b 45 e8             	mov    -0x18(%ebp),%eax
 468:	0f be c0             	movsbl %al,%eax
 46b:	89 44 24 04          	mov    %eax,0x4(%esp)
 46f:	8b 45 08             	mov    0x8(%ebp),%eax
 472:	89 04 24             	mov    %eax,(%esp)
 475:	e8 be fe ff ff       	call   338 <putc>
 47a:	e9 31 01 00 00       	jmp    5b0 <printf+0x19c>
      }
    } else if(state == '%'){
 47f:	83 7d f0 25          	cmpl   $0x25,-0x10(%ebp)
 483:	0f 85 27 01 00 00    	jne    5b0 <printf+0x19c>
      if(c == 'd'){
 489:	83 7d e8 64          	cmpl   $0x64,-0x18(%ebp)
 48d:	75 2d                	jne    4bc <printf+0xa8>
        printint(fd, *ap, 10, 1);
 48f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 492:	8b 00                	mov    (%eax),%eax
 494:	c7 44 24 0c 01 00 00 	movl   $0x1,0xc(%esp)
 49b:	00 
 49c:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
 4a3:	00 
 4a4:	89 44 24 04          	mov    %eax,0x4(%esp)
 4a8:	8b 45 08             	mov    0x8(%ebp),%eax
 4ab:	89 04 24             	mov    %eax,(%esp)
 4ae:	e8 ad fe ff ff       	call   360 <printint>
        ap++;
 4b3:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 4b7:	e9 ed 00 00 00       	jmp    5a9 <printf+0x195>
      } else if(c == 'x' || c == 'p'){
 4bc:	83 7d e8 78          	cmpl   $0x78,-0x18(%ebp)
 4c0:	74 06                	je     4c8 <printf+0xb4>
 4c2:	83 7d e8 70          	cmpl   $0x70,-0x18(%ebp)
 4c6:	75 2d                	jne    4f5 <printf+0xe1>
        printint(fd, *ap, 16, 0);
 4c8:	8b 45 f4             	mov    -0xc(%ebp),%eax
 4cb:	8b 00                	mov    (%eax),%eax
 4cd:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
 4d4:	00 
 4d5:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
 4dc:	00 
 4dd:	89 44 24 04          	mov    %eax,0x4(%esp)
 4e1:	8b 45 08             	mov    0x8(%ebp),%eax
 4e4:	89 04 24             	mov    %eax,(%esp)
 4e7:	e8 74 fe ff ff       	call   360 <printint>
        ap++;
 4ec:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
      }
    } else if(state == '%'){
      if(c == 'd'){
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 4f0:	e9 b4 00 00 00       	jmp    5a9 <printf+0x195>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 4f5:	83 7d e8 73          	cmpl   $0x73,-0x18(%ebp)
 4f9:	75 46                	jne    541 <printf+0x12d>
        s = (char*)*ap;
 4fb:	8b 45 f4             	mov    -0xc(%ebp),%eax
 4fe:	8b 00                	mov    (%eax),%eax
 500:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        ap++;
 503:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
        if(s == 0)
 507:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 50b:	75 27                	jne    534 <printf+0x120>
          s = "(null)";
 50d:	c7 45 e4 db 07 00 00 	movl   $0x7db,-0x1c(%ebp)
        while(*s != 0){
 514:	eb 1f                	jmp    535 <printf+0x121>
          putc(fd, *s);
 516:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 519:	0f b6 00             	movzbl (%eax),%eax
 51c:	0f be c0             	movsbl %al,%eax
 51f:	89 44 24 04          	mov    %eax,0x4(%esp)
 523:	8b 45 08             	mov    0x8(%ebp),%eax
 526:	89 04 24             	mov    %eax,(%esp)
 529:	e8 0a fe ff ff       	call   338 <putc>
          s++;
 52e:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
 532:	eb 01                	jmp    535 <printf+0x121>
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 534:	90                   	nop
 535:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 538:	0f b6 00             	movzbl (%eax),%eax
 53b:	84 c0                	test   %al,%al
 53d:	75 d7                	jne    516 <printf+0x102>
 53f:	eb 68                	jmp    5a9 <printf+0x195>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 541:	83 7d e8 63          	cmpl   $0x63,-0x18(%ebp)
 545:	75 1d                	jne    564 <printf+0x150>
        putc(fd, *ap);
 547:	8b 45 f4             	mov    -0xc(%ebp),%eax
 54a:	8b 00                	mov    (%eax),%eax
 54c:	0f be c0             	movsbl %al,%eax
 54f:	89 44 24 04          	mov    %eax,0x4(%esp)
 553:	8b 45 08             	mov    0x8(%ebp),%eax
 556:	89 04 24             	mov    %eax,(%esp)
 559:	e8 da fd ff ff       	call   338 <putc>
        ap++;
 55e:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 562:	eb 45                	jmp    5a9 <printf+0x195>
      } else if(c == '%'){
 564:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 568:	75 17                	jne    581 <printf+0x16d>
        putc(fd, c);
 56a:	8b 45 e8             	mov    -0x18(%ebp),%eax
 56d:	0f be c0             	movsbl %al,%eax
 570:	89 44 24 04          	mov    %eax,0x4(%esp)
 574:	8b 45 08             	mov    0x8(%ebp),%eax
 577:	89 04 24             	mov    %eax,(%esp)
 57a:	e8 b9 fd ff ff       	call   338 <putc>
 57f:	eb 28                	jmp    5a9 <printf+0x195>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 581:	c7 44 24 04 25 00 00 	movl   $0x25,0x4(%esp)
 588:	00 
 589:	8b 45 08             	mov    0x8(%ebp),%eax
 58c:	89 04 24             	mov    %eax,(%esp)
 58f:	e8 a4 fd ff ff       	call   338 <putc>
        putc(fd, c);
 594:	8b 45 e8             	mov    -0x18(%ebp),%eax
 597:	0f be c0             	movsbl %al,%eax
 59a:	89 44 24 04          	mov    %eax,0x4(%esp)
 59e:	8b 45 08             	mov    0x8(%ebp),%eax
 5a1:	89 04 24             	mov    %eax,(%esp)
 5a4:	e8 8f fd ff ff       	call   338 <putc>
      }
      state = 0;
 5a9:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 5b0:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
 5b4:	8b 55 0c             	mov    0xc(%ebp),%edx
 5b7:	8b 45 ec             	mov    -0x14(%ebp),%eax
 5ba:	8d 04 02             	lea    (%edx,%eax,1),%eax
 5bd:	0f b6 00             	movzbl (%eax),%eax
 5c0:	84 c0                	test   %al,%al
 5c2:	0f 85 6e fe ff ff    	jne    436 <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 5c8:	c9                   	leave  
 5c9:	c3                   	ret    
 5ca:	90                   	nop
 5cb:	90                   	nop

000005cc <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 5cc:	55                   	push   %ebp
 5cd:	89 e5                	mov    %esp,%ebp
 5cf:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 5d2:	8b 45 08             	mov    0x8(%ebp),%eax
 5d5:	83 e8 08             	sub    $0x8,%eax
 5d8:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 5db:	a1 00 08 00 00       	mov    0x800,%eax
 5e0:	89 45 fc             	mov    %eax,-0x4(%ebp)
 5e3:	eb 24                	jmp    609 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 5e5:	8b 45 fc             	mov    -0x4(%ebp),%eax
 5e8:	8b 00                	mov    (%eax),%eax
 5ea:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 5ed:	77 12                	ja     601 <free+0x35>
 5ef:	8b 45 f8             	mov    -0x8(%ebp),%eax
 5f2:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 5f5:	77 24                	ja     61b <free+0x4f>
 5f7:	8b 45 fc             	mov    -0x4(%ebp),%eax
 5fa:	8b 00                	mov    (%eax),%eax
 5fc:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 5ff:	77 1a                	ja     61b <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 601:	8b 45 fc             	mov    -0x4(%ebp),%eax
 604:	8b 00                	mov    (%eax),%eax
 606:	89 45 fc             	mov    %eax,-0x4(%ebp)
 609:	8b 45 f8             	mov    -0x8(%ebp),%eax
 60c:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 60f:	76 d4                	jbe    5e5 <free+0x19>
 611:	8b 45 fc             	mov    -0x4(%ebp),%eax
 614:	8b 00                	mov    (%eax),%eax
 616:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 619:	76 ca                	jbe    5e5 <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 61b:	8b 45 f8             	mov    -0x8(%ebp),%eax
 61e:	8b 40 04             	mov    0x4(%eax),%eax
 621:	c1 e0 03             	shl    $0x3,%eax
 624:	89 c2                	mov    %eax,%edx
 626:	03 55 f8             	add    -0x8(%ebp),%edx
 629:	8b 45 fc             	mov    -0x4(%ebp),%eax
 62c:	8b 00                	mov    (%eax),%eax
 62e:	39 c2                	cmp    %eax,%edx
 630:	75 24                	jne    656 <free+0x8a>
    bp->s.size += p->s.ptr->s.size;
 632:	8b 45 f8             	mov    -0x8(%ebp),%eax
 635:	8b 50 04             	mov    0x4(%eax),%edx
 638:	8b 45 fc             	mov    -0x4(%ebp),%eax
 63b:	8b 00                	mov    (%eax),%eax
 63d:	8b 40 04             	mov    0x4(%eax),%eax
 640:	01 c2                	add    %eax,%edx
 642:	8b 45 f8             	mov    -0x8(%ebp),%eax
 645:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 648:	8b 45 fc             	mov    -0x4(%ebp),%eax
 64b:	8b 00                	mov    (%eax),%eax
 64d:	8b 10                	mov    (%eax),%edx
 64f:	8b 45 f8             	mov    -0x8(%ebp),%eax
 652:	89 10                	mov    %edx,(%eax)
 654:	eb 0a                	jmp    660 <free+0x94>
  } else
    bp->s.ptr = p->s.ptr;
 656:	8b 45 fc             	mov    -0x4(%ebp),%eax
 659:	8b 10                	mov    (%eax),%edx
 65b:	8b 45 f8             	mov    -0x8(%ebp),%eax
 65e:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 660:	8b 45 fc             	mov    -0x4(%ebp),%eax
 663:	8b 40 04             	mov    0x4(%eax),%eax
 666:	c1 e0 03             	shl    $0x3,%eax
 669:	03 45 fc             	add    -0x4(%ebp),%eax
 66c:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 66f:	75 20                	jne    691 <free+0xc5>
    p->s.size += bp->s.size;
 671:	8b 45 fc             	mov    -0x4(%ebp),%eax
 674:	8b 50 04             	mov    0x4(%eax),%edx
 677:	8b 45 f8             	mov    -0x8(%ebp),%eax
 67a:	8b 40 04             	mov    0x4(%eax),%eax
 67d:	01 c2                	add    %eax,%edx
 67f:	8b 45 fc             	mov    -0x4(%ebp),%eax
 682:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 685:	8b 45 f8             	mov    -0x8(%ebp),%eax
 688:	8b 10                	mov    (%eax),%edx
 68a:	8b 45 fc             	mov    -0x4(%ebp),%eax
 68d:	89 10                	mov    %edx,(%eax)
 68f:	eb 08                	jmp    699 <free+0xcd>
  } else
    p->s.ptr = bp;
 691:	8b 45 fc             	mov    -0x4(%ebp),%eax
 694:	8b 55 f8             	mov    -0x8(%ebp),%edx
 697:	89 10                	mov    %edx,(%eax)
  freep = p;
 699:	8b 45 fc             	mov    -0x4(%ebp),%eax
 69c:	a3 00 08 00 00       	mov    %eax,0x800
}
 6a1:	c9                   	leave  
 6a2:	c3                   	ret    

000006a3 <morecore>:

static Header*
morecore(uint nu)
{
 6a3:	55                   	push   %ebp
 6a4:	89 e5                	mov    %esp,%ebp
 6a6:	83 ec 28             	sub    $0x28,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 6a9:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 6b0:	77 07                	ja     6b9 <morecore+0x16>
    nu = 4096;
 6b2:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 6b9:	8b 45 08             	mov    0x8(%ebp),%eax
 6bc:	c1 e0 03             	shl    $0x3,%eax
 6bf:	89 04 24             	mov    %eax,(%esp)
 6c2:	e8 39 fc ff ff       	call   300 <sbrk>
 6c7:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(p == (char*)-1)
 6ca:	83 7d f0 ff          	cmpl   $0xffffffff,-0x10(%ebp)
 6ce:	75 07                	jne    6d7 <morecore+0x34>
    return 0;
 6d0:	b8 00 00 00 00       	mov    $0x0,%eax
 6d5:	eb 22                	jmp    6f9 <morecore+0x56>
  hp = (Header*)p;
 6d7:	8b 45 f0             	mov    -0x10(%ebp),%eax
 6da:	89 45 f4             	mov    %eax,-0xc(%ebp)
  hp->s.size = nu;
 6dd:	8b 45 f4             	mov    -0xc(%ebp),%eax
 6e0:	8b 55 08             	mov    0x8(%ebp),%edx
 6e3:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 6e6:	8b 45 f4             	mov    -0xc(%ebp),%eax
 6e9:	83 c0 08             	add    $0x8,%eax
 6ec:	89 04 24             	mov    %eax,(%esp)
 6ef:	e8 d8 fe ff ff       	call   5cc <free>
  return freep;
 6f4:	a1 00 08 00 00       	mov    0x800,%eax
}
 6f9:	c9                   	leave  
 6fa:	c3                   	ret    

000006fb <malloc>:

void*
malloc(uint nbytes)
{
 6fb:	55                   	push   %ebp
 6fc:	89 e5                	mov    %esp,%ebp
 6fe:	83 ec 28             	sub    $0x28,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 701:	8b 45 08             	mov    0x8(%ebp),%eax
 704:	83 c0 07             	add    $0x7,%eax
 707:	c1 e8 03             	shr    $0x3,%eax
 70a:	83 c0 01             	add    $0x1,%eax
 70d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((prevp = freep) == 0){
 710:	a1 00 08 00 00       	mov    0x800,%eax
 715:	89 45 f0             	mov    %eax,-0x10(%ebp)
 718:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 71c:	75 23                	jne    741 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 71e:	c7 45 f0 f8 07 00 00 	movl   $0x7f8,-0x10(%ebp)
 725:	8b 45 f0             	mov    -0x10(%ebp),%eax
 728:	a3 00 08 00 00       	mov    %eax,0x800
 72d:	a1 00 08 00 00       	mov    0x800,%eax
 732:	a3 f8 07 00 00       	mov    %eax,0x7f8
    base.s.size = 0;
 737:	c7 05 fc 07 00 00 00 	movl   $0x0,0x7fc
 73e:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 741:	8b 45 f0             	mov    -0x10(%ebp),%eax
 744:	8b 00                	mov    (%eax),%eax
 746:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(p->s.size >= nunits){
 749:	8b 45 ec             	mov    -0x14(%ebp),%eax
 74c:	8b 40 04             	mov    0x4(%eax),%eax
 74f:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 752:	72 4d                	jb     7a1 <malloc+0xa6>
      if(p->s.size == nunits)
 754:	8b 45 ec             	mov    -0x14(%ebp),%eax
 757:	8b 40 04             	mov    0x4(%eax),%eax
 75a:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 75d:	75 0c                	jne    76b <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 75f:	8b 45 ec             	mov    -0x14(%ebp),%eax
 762:	8b 10                	mov    (%eax),%edx
 764:	8b 45 f0             	mov    -0x10(%ebp),%eax
 767:	89 10                	mov    %edx,(%eax)
 769:	eb 26                	jmp    791 <malloc+0x96>
      else {
        p->s.size -= nunits;
 76b:	8b 45 ec             	mov    -0x14(%ebp),%eax
 76e:	8b 40 04             	mov    0x4(%eax),%eax
 771:	89 c2                	mov    %eax,%edx
 773:	2b 55 f4             	sub    -0xc(%ebp),%edx
 776:	8b 45 ec             	mov    -0x14(%ebp),%eax
 779:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 77c:	8b 45 ec             	mov    -0x14(%ebp),%eax
 77f:	8b 40 04             	mov    0x4(%eax),%eax
 782:	c1 e0 03             	shl    $0x3,%eax
 785:	01 45 ec             	add    %eax,-0x14(%ebp)
        p->s.size = nunits;
 788:	8b 45 ec             	mov    -0x14(%ebp),%eax
 78b:	8b 55 f4             	mov    -0xc(%ebp),%edx
 78e:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 791:	8b 45 f0             	mov    -0x10(%ebp),%eax
 794:	a3 00 08 00 00       	mov    %eax,0x800
      return (void*)(p + 1);
 799:	8b 45 ec             	mov    -0x14(%ebp),%eax
 79c:	83 c0 08             	add    $0x8,%eax
 79f:	eb 38                	jmp    7d9 <malloc+0xde>
    }
    if(p == freep)
 7a1:	a1 00 08 00 00       	mov    0x800,%eax
 7a6:	39 45 ec             	cmp    %eax,-0x14(%ebp)
 7a9:	75 1b                	jne    7c6 <malloc+0xcb>
      if((p = morecore(nunits)) == 0)
 7ab:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7ae:	89 04 24             	mov    %eax,(%esp)
 7b1:	e8 ed fe ff ff       	call   6a3 <morecore>
 7b6:	89 45 ec             	mov    %eax,-0x14(%ebp)
 7b9:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 7bd:	75 07                	jne    7c6 <malloc+0xcb>
        return 0;
 7bf:	b8 00 00 00 00       	mov    $0x0,%eax
 7c4:	eb 13                	jmp    7d9 <malloc+0xde>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 7c6:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7c9:	89 45 f0             	mov    %eax,-0x10(%ebp)
 7cc:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7cf:	8b 00                	mov    (%eax),%eax
 7d1:	89 45 ec             	mov    %eax,-0x14(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 7d4:	e9 70 ff ff ff       	jmp    749 <malloc+0x4e>
}
 7d9:	c9                   	leave  
 7da:	c3                   	ret    
