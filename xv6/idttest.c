#include "types.h"
#include "stat.h"
#include "user.h"

int main(void)
{
    printf(1, "What kind of interrupt do you want? Give a number between 0 and 7 (inclusive): ");
    char buf[10];
    gets(buf, 10);
    int interrupt = atoi(buf);
    printf(1, "You wanted interrupt %d. Ok, here it goes...\n", interrupt);
    switch(interrupt) {
        case 0: asm volatile("int $0\n"); // divide error
        case 1: asm volatile("int $1\n"); // debug exception
        case 2: asm volatile("int $2\n"); // non-maskable interrupt
        case 3: asm volatile("int $3\n"); // breakpoint
        case 4: asm volatile("int $4\n"); // overflow
        case 5: asm volatile("int $5\n"); // bounds check
        case 6: asm volatile("int $6\n"); // illegal opcode
        case 7: asm volatile("int $7\n"); // device not available
    }
    exit();
}
