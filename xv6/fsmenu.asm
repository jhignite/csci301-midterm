
_fsmenu:     file format elf32-i386


Disassembly of section .text:

00000000 <getcmd>:
#include "stat.h"
#include "user.h"

int
getcmd(char *buf, int nbuf)
{
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	83 ec 18             	sub    $0x18,%esp
	printf(2, "$ ");
   6:	c7 44 24 04 5c 0a 00 	movl   $0xa5c,0x4(%esp)
   d:	00 
   e:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
  15:	e8 7a 06 00 00       	call   694 <printf>
	memset(buf, 0, nbuf);
  1a:	8b 45 0c             	mov    0xc(%ebp),%eax
  1d:	89 44 24 08          	mov    %eax,0x8(%esp)
  21:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  28:	00 
  29:	8b 45 08             	mov    0x8(%ebp),%eax
  2c:	89 04 24             	mov    %eax,(%esp)
  2f:	e8 1e 03 00 00       	call   352 <memset>
	gets(buf, nbuf);
  34:	8b 45 0c             	mov    0xc(%ebp),%eax
  37:	89 44 24 04          	mov    %eax,0x4(%esp)
  3b:	8b 45 08             	mov    0x8(%ebp),%eax
  3e:	89 04 24             	mov    %eax,(%esp)
  41:	e8 63 03 00 00       	call   3a9 <gets>
	if(buf[0] == 0) // EOF
  46:	8b 45 08             	mov    0x8(%ebp),%eax
  49:	0f b6 00             	movzbl (%eax),%eax
  4c:	84 c0                	test   %al,%al
  4e:	75 07                	jne    57 <getcmd+0x57>
		return -1;
  50:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  55:	eb 05                	jmp    5c <getcmd+0x5c>
	return 0;
  57:	b8 00 00 00 00       	mov    $0x0,%eax
}
  5c:	c9                   	leave  
  5d:	c3                   	ret    

0000005e <main>:

int
main(int argc, char *argv[])
{
  5e:	55                   	push   %ebp
  5f:	89 e5                	mov    %esp,%ebp
  61:	83 e4 f0             	and    $0xfffffff0,%esp
  64:	83 ec 40             	sub    $0x40,%esp
	static char buf[100];
	//static char buf2[100];
	char *bufarg = buf+2;
  67:	c7 44 24 30 42 0b 00 	movl   $0xb42,0x30(%esp)
  6e:	00 
	char **bufargv = &bufarg;
  6f:	8d 44 24 30          	lea    0x30(%esp),%eax
  73:	89 44 24 34          	mov    %eax,0x34(%esp)
	char *prompt = "Choose an action: l = list files, d = delete a file, r = rename a file, q = quit => ";
  77:	c7 44 24 38 60 0a 00 	movl   $0xa60,0x38(%esp)
  7e:	00 
	char *largv[] = {0};
  7f:	c7 44 24 2c 00 00 00 	movl   $0x0,0x2c(%esp)
  86:	00 
	printf(2, prompt);
  87:	8b 44 24 38          	mov    0x38(%esp),%eax
  8b:	89 44 24 04          	mov    %eax,0x4(%esp)
  8f:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
  96:	e8 f9 05 00 00       	call   694 <printf>
	while(getcmd(buf, sizeof(buf)) >= 0){
  9b:	e9 cf 01 00 00       	jmp    26f <main+0x211>
		if(buf[0] == 'l'){
  a0:	0f b6 05 40 0b 00 00 	movzbl 0xb40,%eax
  a7:	3c 6c                	cmp    $0x6c,%al
  a9:	75 2d                	jne    d8 <main+0x7a>
    			exec("ls", largv);
  ab:	8d 44 24 2c          	lea    0x2c(%esp),%eax
  af:	89 44 24 04          	mov    %eax,0x4(%esp)
  b3:	c7 04 24 b5 0a 00 00 	movl   $0xab5,(%esp)
  ba:	e8 71 04 00 00       	call   530 <exec>
			printf(2, prompt);
  bf:	8b 44 24 38          	mov    0x38(%esp),%eax
  c3:	89 44 24 04          	mov    %eax,0x4(%esp)
  c7:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
  ce:	e8 c1 05 00 00       	call   694 <printf>
  d3:	e9 81 01 00 00       	jmp    259 <main+0x1fb>
		} else if(buf[0] == 'd'){
  d8:	0f b6 05 40 0b 00 00 	movzbl 0xb40,%eax
  df:	3c 64                	cmp    $0x64,%al
  e1:	75 77                	jne    15a <main+0xfc>
			//Don't know why this isn't working
			printf(2, "Name of file to delete: ");
  e3:	c7 44 24 04 b8 0a 00 	movl   $0xab8,0x4(%esp)
  ea:	00 
  eb:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
  f2:	e8 9d 05 00 00       	call   694 <printf>
			while(getcmd(buf, sizeof(buf)) >= 0){
  f7:	90                   	nop
  f8:	c7 44 24 04 64 00 00 	movl   $0x64,0x4(%esp)
  ff:	00 
 100:	c7 04 24 40 0b 00 00 	movl   $0xb40,(%esp)
 107:	e8 f4 fe ff ff       	call   0 <getcmd>
 10c:	85 c0                	test   %eax,%eax
 10e:	78 31                	js     141 <main+0xe3>
				char *rargv[] = {buf};
 110:	c7 44 24 28 40 0b 00 	movl   $0xb40,0x28(%esp)
 117:	00 
				printf(2, rargv[0]);
 118:	8b 44 24 28          	mov    0x28(%esp),%eax
 11c:	89 44 24 04          	mov    %eax,0x4(%esp)
 120:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
 127:	e8 68 05 00 00       	call   694 <printf>
				exec("rm", rargv);
 12c:	8d 44 24 28          	lea    0x28(%esp),%eax
 130:	89 44 24 04          	mov    %eax,0x4(%esp)
 134:	c7 04 24 d1 0a 00 00 	movl   $0xad1,(%esp)
 13b:	e8 f0 03 00 00       	call   530 <exec>
				break;
 140:	90                   	nop
			}
			printf(2, prompt);
 141:	8b 44 24 38          	mov    0x38(%esp),%eax
 145:	89 44 24 04          	mov    %eax,0x4(%esp)
 149:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
 150:	e8 3f 05 00 00       	call   694 <printf>
 155:	e9 ff 00 00 00       	jmp    259 <main+0x1fb>
		} else if(buf[0] == 'r'){
 15a:	0f b6 05 40 0b 00 00 	movzbl 0xb40,%eax
 161:	3c 72                	cmp    $0x72,%al
 163:	0f 85 e0 00 00 00    	jne    249 <main+0x1eb>
			printf(2, "Enter original file name: ");
 169:	c7 44 24 04 d4 0a 00 	movl   $0xad4,0x4(%esp)
 170:	00 
 171:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
 178:	e8 17 05 00 00       	call   694 <printf>
			char *temp;
			while(getcmd(buf, sizeof(buf)) >= 0){
 17d:	90                   	nop
 17e:	c7 44 24 04 64 00 00 	movl   $0x64,0x4(%esp)
 185:	00 
 186:	c7 04 24 40 0b 00 00 	movl   $0xb40,(%esp)
 18d:	e8 6e fe ff ff       	call   0 <getcmd>
 192:	85 c0                	test   %eax,%eax
 194:	78 09                	js     19f <main+0x141>
				temp = buf+' ';
 196:	c7 44 24 3c 60 0b 00 	movl   $0xb60,0x3c(%esp)
 19d:	00 
				break;
 19e:	90                   	nop
			}
			printf(2, "Enter new file name: ");
 19f:	c7 44 24 04 ef 0a 00 	movl   $0xaef,0x4(%esp)
 1a6:	00 
 1a7:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
 1ae:	e8 e1 04 00 00       	call   694 <printf>
			while(getcmd(buf, sizeof(buf)) >= 0){
 1b3:	eb 50                	jmp    205 <main+0x1a7>
				char *lnargv[] = {temp, buf, " ", 0};
 1b5:	8b 44 24 3c          	mov    0x3c(%esp),%eax
 1b9:	89 44 24 18          	mov    %eax,0x18(%esp)
 1bd:	c7 44 24 1c 40 0b 00 	movl   $0xb40,0x1c(%esp)
 1c4:	00 
 1c5:	c7 44 24 20 05 0b 00 	movl   $0xb05,0x20(%esp)
 1cc:	00 
 1cd:	c7 44 24 24 00 00 00 	movl   $0x0,0x24(%esp)
 1d4:	00 
				char *rargv[] = {temp};
 1d5:	8b 44 24 3c          	mov    0x3c(%esp),%eax
 1d9:	89 44 24 14          	mov    %eax,0x14(%esp)
				exec("ln", lnargv);
 1dd:	8d 44 24 18          	lea    0x18(%esp),%eax
 1e1:	89 44 24 04          	mov    %eax,0x4(%esp)
 1e5:	c7 04 24 07 0b 00 00 	movl   $0xb07,(%esp)
 1ec:	e8 3f 03 00 00       	call   530 <exec>
				exec("rm", rargv);
 1f1:	8d 44 24 14          	lea    0x14(%esp),%eax
 1f5:	89 44 24 04          	mov    %eax,0x4(%esp)
 1f9:	c7 04 24 d1 0a 00 00 	movl   $0xad1,(%esp)
 200:	e8 2b 03 00 00       	call   530 <exec>
			while(getcmd(buf, sizeof(buf)) >= 0){
				temp = buf+' ';
				break;
			}
			printf(2, "Enter new file name: ");
			while(getcmd(buf, sizeof(buf)) >= 0){
 205:	c7 44 24 04 64 00 00 	movl   $0x64,0x4(%esp)
 20c:	00 
 20d:	c7 04 24 40 0b 00 00 	movl   $0xb40,(%esp)
 214:	e8 e7 fd ff ff       	call   0 <getcmd>
 219:	85 c0                	test   %eax,%eax
 21b:	79 98                	jns    1b5 <main+0x157>
				char *lnargv[] = {temp, buf, " ", 0};
				char *rargv[] = {temp};
				exec("ln", lnargv);
				exec("rm", rargv);
			}
			exec("ln", bufargv);
 21d:	8b 44 24 34          	mov    0x34(%esp),%eax
 221:	89 44 24 04          	mov    %eax,0x4(%esp)
 225:	c7 04 24 07 0b 00 00 	movl   $0xb07,(%esp)
 22c:	e8 ff 02 00 00       	call   530 <exec>
			buf[strlen(buf)-1] = 0;
 231:	c7 04 24 40 0b 00 00 	movl   $0xb40,(%esp)
 238:	e8 f0 00 00 00       	call   32d <strlen>
 23d:	83 e8 01             	sub    $0x1,%eax
 240:	c6 80 40 0b 00 00 00 	movb   $0x0,0xb40(%eax)
 247:	eb 10                	jmp    259 <main+0x1fb>
		} else if(buf[0] == 'q')
 249:	0f b6 05 40 0b 00 00 	movzbl 0xb40,%eax
 250:	3c 71                	cmp    $0x71,%al
 252:	75 05                	jne    259 <main+0x1fb>
			exit();
 254:	e8 9f 02 00 00       	call   4f8 <exit>
		buf[strlen(buf)-1] = 0;
 259:	c7 04 24 40 0b 00 00 	movl   $0xb40,(%esp)
 260:	e8 c8 00 00 00       	call   32d <strlen>
 265:	83 e8 01             	sub    $0x1,%eax
 268:	c6 80 40 0b 00 00 00 	movb   $0x0,0xb40(%eax)
	char *bufarg = buf+2;
	char **bufargv = &bufarg;
	char *prompt = "Choose an action: l = list files, d = delete a file, r = rename a file, q = quit => ";
	char *largv[] = {0};
	printf(2, prompt);
	while(getcmd(buf, sizeof(buf)) >= 0){
 26f:	c7 44 24 04 64 00 00 	movl   $0x64,0x4(%esp)
 276:	00 
 277:	c7 04 24 40 0b 00 00 	movl   $0xb40,(%esp)
 27e:	e8 7d fd ff ff       	call   0 <getcmd>
 283:	85 c0                	test   %eax,%eax
 285:	0f 89 15 fe ff ff    	jns    a0 <main+0x42>
			buf[strlen(buf)-1] = 0;
		} else if(buf[0] == 'q')
			exit();
		buf[strlen(buf)-1] = 0;
	}
	return 1;
 28b:	b8 01 00 00 00       	mov    $0x1,%eax
}
 290:	c9                   	leave  
 291:	c3                   	ret    
 292:	90                   	nop
 293:	90                   	nop

00000294 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
 294:	55                   	push   %ebp
 295:	89 e5                	mov    %esp,%ebp
 297:	57                   	push   %edi
 298:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
 299:	8b 4d 08             	mov    0x8(%ebp),%ecx
 29c:	8b 55 10             	mov    0x10(%ebp),%edx
 29f:	8b 45 0c             	mov    0xc(%ebp),%eax
 2a2:	89 cb                	mov    %ecx,%ebx
 2a4:	89 df                	mov    %ebx,%edi
 2a6:	89 d1                	mov    %edx,%ecx
 2a8:	fc                   	cld    
 2a9:	f3 aa                	rep stos %al,%es:(%edi)
 2ab:	89 ca                	mov    %ecx,%edx
 2ad:	89 fb                	mov    %edi,%ebx
 2af:	89 5d 08             	mov    %ebx,0x8(%ebp)
 2b2:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
 2b5:	5b                   	pop    %ebx
 2b6:	5f                   	pop    %edi
 2b7:	5d                   	pop    %ebp
 2b8:	c3                   	ret    

000002b9 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
 2b9:	55                   	push   %ebp
 2ba:	89 e5                	mov    %esp,%ebp
 2bc:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 2bf:	8b 45 08             	mov    0x8(%ebp),%eax
 2c2:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 2c5:	8b 45 0c             	mov    0xc(%ebp),%eax
 2c8:	0f b6 10             	movzbl (%eax),%edx
 2cb:	8b 45 08             	mov    0x8(%ebp),%eax
 2ce:	88 10                	mov    %dl,(%eax)
 2d0:	8b 45 08             	mov    0x8(%ebp),%eax
 2d3:	0f b6 00             	movzbl (%eax),%eax
 2d6:	84 c0                	test   %al,%al
 2d8:	0f 95 c0             	setne  %al
 2db:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 2df:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
 2e3:	84 c0                	test   %al,%al
 2e5:	75 de                	jne    2c5 <strcpy+0xc>
    ;
  return os;
 2e7:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 2ea:	c9                   	leave  
 2eb:	c3                   	ret    

000002ec <strcmp>:

int
strcmp(const char *p, const char *q)
{
 2ec:	55                   	push   %ebp
 2ed:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 2ef:	eb 08                	jmp    2f9 <strcmp+0xd>
    p++, q++;
 2f1:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 2f5:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 2f9:	8b 45 08             	mov    0x8(%ebp),%eax
 2fc:	0f b6 00             	movzbl (%eax),%eax
 2ff:	84 c0                	test   %al,%al
 301:	74 10                	je     313 <strcmp+0x27>
 303:	8b 45 08             	mov    0x8(%ebp),%eax
 306:	0f b6 10             	movzbl (%eax),%edx
 309:	8b 45 0c             	mov    0xc(%ebp),%eax
 30c:	0f b6 00             	movzbl (%eax),%eax
 30f:	38 c2                	cmp    %al,%dl
 311:	74 de                	je     2f1 <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 313:	8b 45 08             	mov    0x8(%ebp),%eax
 316:	0f b6 00             	movzbl (%eax),%eax
 319:	0f b6 d0             	movzbl %al,%edx
 31c:	8b 45 0c             	mov    0xc(%ebp),%eax
 31f:	0f b6 00             	movzbl (%eax),%eax
 322:	0f b6 c0             	movzbl %al,%eax
 325:	89 d1                	mov    %edx,%ecx
 327:	29 c1                	sub    %eax,%ecx
 329:	89 c8                	mov    %ecx,%eax
}
 32b:	5d                   	pop    %ebp
 32c:	c3                   	ret    

0000032d <strlen>:

uint
strlen(char *s)
{
 32d:	55                   	push   %ebp
 32e:	89 e5                	mov    %esp,%ebp
 330:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 333:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 33a:	eb 04                	jmp    340 <strlen+0x13>
 33c:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 340:	8b 45 fc             	mov    -0x4(%ebp),%eax
 343:	03 45 08             	add    0x8(%ebp),%eax
 346:	0f b6 00             	movzbl (%eax),%eax
 349:	84 c0                	test   %al,%al
 34b:	75 ef                	jne    33c <strlen+0xf>
    ;
  return n;
 34d:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 350:	c9                   	leave  
 351:	c3                   	ret    

00000352 <memset>:

void*
memset(void *dst, int c, uint n)
{
 352:	55                   	push   %ebp
 353:	89 e5                	mov    %esp,%ebp
 355:	83 ec 0c             	sub    $0xc,%esp
  stosb(dst, c, n);
 358:	8b 45 10             	mov    0x10(%ebp),%eax
 35b:	89 44 24 08          	mov    %eax,0x8(%esp)
 35f:	8b 45 0c             	mov    0xc(%ebp),%eax
 362:	89 44 24 04          	mov    %eax,0x4(%esp)
 366:	8b 45 08             	mov    0x8(%ebp),%eax
 369:	89 04 24             	mov    %eax,(%esp)
 36c:	e8 23 ff ff ff       	call   294 <stosb>
  return dst;
 371:	8b 45 08             	mov    0x8(%ebp),%eax
}
 374:	c9                   	leave  
 375:	c3                   	ret    

00000376 <strchr>:

char*
strchr(const char *s, char c)
{
 376:	55                   	push   %ebp
 377:	89 e5                	mov    %esp,%ebp
 379:	83 ec 04             	sub    $0x4,%esp
 37c:	8b 45 0c             	mov    0xc(%ebp),%eax
 37f:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 382:	eb 14                	jmp    398 <strchr+0x22>
    if(*s == c)
 384:	8b 45 08             	mov    0x8(%ebp),%eax
 387:	0f b6 00             	movzbl (%eax),%eax
 38a:	3a 45 fc             	cmp    -0x4(%ebp),%al
 38d:	75 05                	jne    394 <strchr+0x1e>
      return (char*)s;
 38f:	8b 45 08             	mov    0x8(%ebp),%eax
 392:	eb 13                	jmp    3a7 <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 394:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 398:	8b 45 08             	mov    0x8(%ebp),%eax
 39b:	0f b6 00             	movzbl (%eax),%eax
 39e:	84 c0                	test   %al,%al
 3a0:	75 e2                	jne    384 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 3a2:	b8 00 00 00 00       	mov    $0x0,%eax
}
 3a7:	c9                   	leave  
 3a8:	c3                   	ret    

000003a9 <gets>:

char*
gets(char *buf, int max)
{
 3a9:	55                   	push   %ebp
 3aa:	89 e5                	mov    %esp,%ebp
 3ac:	83 ec 28             	sub    $0x28,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 3af:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 3b6:	eb 44                	jmp    3fc <gets+0x53>
    cc = read(0, &c, 1);
 3b8:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 3bf:	00 
 3c0:	8d 45 ef             	lea    -0x11(%ebp),%eax
 3c3:	89 44 24 04          	mov    %eax,0x4(%esp)
 3c7:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 3ce:	e8 3d 01 00 00       	call   510 <read>
 3d3:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(cc < 1)
 3d6:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 3da:	7e 2d                	jle    409 <gets+0x60>
      break;
    buf[i++] = c;
 3dc:	8b 45 f0             	mov    -0x10(%ebp),%eax
 3df:	03 45 08             	add    0x8(%ebp),%eax
 3e2:	0f b6 55 ef          	movzbl -0x11(%ebp),%edx
 3e6:	88 10                	mov    %dl,(%eax)
 3e8:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
    if(c == '\n' || c == '\r')
 3ec:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 3f0:	3c 0a                	cmp    $0xa,%al
 3f2:	74 16                	je     40a <gets+0x61>
 3f4:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 3f8:	3c 0d                	cmp    $0xd,%al
 3fa:	74 0e                	je     40a <gets+0x61>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 3fc:	8b 45 f0             	mov    -0x10(%ebp),%eax
 3ff:	83 c0 01             	add    $0x1,%eax
 402:	3b 45 0c             	cmp    0xc(%ebp),%eax
 405:	7c b1                	jl     3b8 <gets+0xf>
 407:	eb 01                	jmp    40a <gets+0x61>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 409:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 40a:	8b 45 f0             	mov    -0x10(%ebp),%eax
 40d:	03 45 08             	add    0x8(%ebp),%eax
 410:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 413:	8b 45 08             	mov    0x8(%ebp),%eax
}
 416:	c9                   	leave  
 417:	c3                   	ret    

00000418 <stat>:

int
stat(char *n, struct stat *st)
{
 418:	55                   	push   %ebp
 419:	89 e5                	mov    %esp,%ebp
 41b:	83 ec 28             	sub    $0x28,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 41e:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 425:	00 
 426:	8b 45 08             	mov    0x8(%ebp),%eax
 429:	89 04 24             	mov    %eax,(%esp)
 42c:	e8 07 01 00 00       	call   538 <open>
 431:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(fd < 0)
 434:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 438:	79 07                	jns    441 <stat+0x29>
    return -1;
 43a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 43f:	eb 23                	jmp    464 <stat+0x4c>
  r = fstat(fd, st);
 441:	8b 45 0c             	mov    0xc(%ebp),%eax
 444:	89 44 24 04          	mov    %eax,0x4(%esp)
 448:	8b 45 f0             	mov    -0x10(%ebp),%eax
 44b:	89 04 24             	mov    %eax,(%esp)
 44e:	e8 fd 00 00 00       	call   550 <fstat>
 453:	89 45 f4             	mov    %eax,-0xc(%ebp)
  close(fd);
 456:	8b 45 f0             	mov    -0x10(%ebp),%eax
 459:	89 04 24             	mov    %eax,(%esp)
 45c:	e8 bf 00 00 00       	call   520 <close>
  return r;
 461:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
 464:	c9                   	leave  
 465:	c3                   	ret    

00000466 <atoi>:

int
atoi(const char *s)
{
 466:	55                   	push   %ebp
 467:	89 e5                	mov    %esp,%ebp
 469:	83 ec 10             	sub    $0x10,%esp
  int n;

  n = 0;
 46c:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while('0' <= *s && *s <= '9')
 473:	eb 24                	jmp    499 <atoi+0x33>
    n = n*10 + *s++ - '0';
 475:	8b 55 fc             	mov    -0x4(%ebp),%edx
 478:	89 d0                	mov    %edx,%eax
 47a:	c1 e0 02             	shl    $0x2,%eax
 47d:	01 d0                	add    %edx,%eax
 47f:	01 c0                	add    %eax,%eax
 481:	89 c2                	mov    %eax,%edx
 483:	8b 45 08             	mov    0x8(%ebp),%eax
 486:	0f b6 00             	movzbl (%eax),%eax
 489:	0f be c0             	movsbl %al,%eax
 48c:	8d 04 02             	lea    (%edx,%eax,1),%eax
 48f:	83 e8 30             	sub    $0x30,%eax
 492:	89 45 fc             	mov    %eax,-0x4(%ebp)
 495:	83 45 08 01          	addl   $0x1,0x8(%ebp)
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 499:	8b 45 08             	mov    0x8(%ebp),%eax
 49c:	0f b6 00             	movzbl (%eax),%eax
 49f:	3c 2f                	cmp    $0x2f,%al
 4a1:	7e 0a                	jle    4ad <atoi+0x47>
 4a3:	8b 45 08             	mov    0x8(%ebp),%eax
 4a6:	0f b6 00             	movzbl (%eax),%eax
 4a9:	3c 39                	cmp    $0x39,%al
 4ab:	7e c8                	jle    475 <atoi+0xf>
    n = n*10 + *s++ - '0';
  return n;
 4ad:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 4b0:	c9                   	leave  
 4b1:	c3                   	ret    

000004b2 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 4b2:	55                   	push   %ebp
 4b3:	89 e5                	mov    %esp,%ebp
 4b5:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 4b8:	8b 45 08             	mov    0x8(%ebp),%eax
 4bb:	89 45 f8             	mov    %eax,-0x8(%ebp)
  src = vsrc;
 4be:	8b 45 0c             	mov    0xc(%ebp),%eax
 4c1:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0)
 4c4:	eb 13                	jmp    4d9 <memmove+0x27>
    *dst++ = *src++;
 4c6:	8b 45 fc             	mov    -0x4(%ebp),%eax
 4c9:	0f b6 10             	movzbl (%eax),%edx
 4cc:	8b 45 f8             	mov    -0x8(%ebp),%eax
 4cf:	88 10                	mov    %dl,(%eax)
 4d1:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
 4d5:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 4d9:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
 4dd:	0f 9f c0             	setg   %al
 4e0:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
 4e4:	84 c0                	test   %al,%al
 4e6:	75 de                	jne    4c6 <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 4e8:	8b 45 08             	mov    0x8(%ebp),%eax
}
 4eb:	c9                   	leave  
 4ec:	c3                   	ret    
 4ed:	90                   	nop
 4ee:	90                   	nop
 4ef:	90                   	nop

000004f0 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 4f0:	b8 01 00 00 00       	mov    $0x1,%eax
 4f5:	cd 40                	int    $0x40
 4f7:	c3                   	ret    

000004f8 <exit>:
SYSCALL(exit)
 4f8:	b8 02 00 00 00       	mov    $0x2,%eax
 4fd:	cd 40                	int    $0x40
 4ff:	c3                   	ret    

00000500 <wait>:
SYSCALL(wait)
 500:	b8 03 00 00 00       	mov    $0x3,%eax
 505:	cd 40                	int    $0x40
 507:	c3                   	ret    

00000508 <pipe>:
SYSCALL(pipe)
 508:	b8 04 00 00 00       	mov    $0x4,%eax
 50d:	cd 40                	int    $0x40
 50f:	c3                   	ret    

00000510 <read>:
SYSCALL(read)
 510:	b8 05 00 00 00       	mov    $0x5,%eax
 515:	cd 40                	int    $0x40
 517:	c3                   	ret    

00000518 <write>:
SYSCALL(write)
 518:	b8 10 00 00 00       	mov    $0x10,%eax
 51d:	cd 40                	int    $0x40
 51f:	c3                   	ret    

00000520 <close>:
SYSCALL(close)
 520:	b8 15 00 00 00       	mov    $0x15,%eax
 525:	cd 40                	int    $0x40
 527:	c3                   	ret    

00000528 <kill>:
SYSCALL(kill)
 528:	b8 06 00 00 00       	mov    $0x6,%eax
 52d:	cd 40                	int    $0x40
 52f:	c3                   	ret    

00000530 <exec>:
SYSCALL(exec)
 530:	b8 07 00 00 00       	mov    $0x7,%eax
 535:	cd 40                	int    $0x40
 537:	c3                   	ret    

00000538 <open>:
SYSCALL(open)
 538:	b8 0f 00 00 00       	mov    $0xf,%eax
 53d:	cd 40                	int    $0x40
 53f:	c3                   	ret    

00000540 <mknod>:
SYSCALL(mknod)
 540:	b8 11 00 00 00       	mov    $0x11,%eax
 545:	cd 40                	int    $0x40
 547:	c3                   	ret    

00000548 <unlink>:
SYSCALL(unlink)
 548:	b8 12 00 00 00       	mov    $0x12,%eax
 54d:	cd 40                	int    $0x40
 54f:	c3                   	ret    

00000550 <fstat>:
SYSCALL(fstat)
 550:	b8 08 00 00 00       	mov    $0x8,%eax
 555:	cd 40                	int    $0x40
 557:	c3                   	ret    

00000558 <link>:
SYSCALL(link)
 558:	b8 13 00 00 00       	mov    $0x13,%eax
 55d:	cd 40                	int    $0x40
 55f:	c3                   	ret    

00000560 <mkdir>:
SYSCALL(mkdir)
 560:	b8 14 00 00 00       	mov    $0x14,%eax
 565:	cd 40                	int    $0x40
 567:	c3                   	ret    

00000568 <chdir>:
SYSCALL(chdir)
 568:	b8 09 00 00 00       	mov    $0x9,%eax
 56d:	cd 40                	int    $0x40
 56f:	c3                   	ret    

00000570 <dup>:
SYSCALL(dup)
 570:	b8 0a 00 00 00       	mov    $0xa,%eax
 575:	cd 40                	int    $0x40
 577:	c3                   	ret    

00000578 <getpid>:
SYSCALL(getpid)
 578:	b8 0b 00 00 00       	mov    $0xb,%eax
 57d:	cd 40                	int    $0x40
 57f:	c3                   	ret    

00000580 <sbrk>:
SYSCALL(sbrk)
 580:	b8 0c 00 00 00       	mov    $0xc,%eax
 585:	cd 40                	int    $0x40
 587:	c3                   	ret    

00000588 <sleep>:
SYSCALL(sleep)
 588:	b8 0d 00 00 00       	mov    $0xd,%eax
 58d:	cd 40                	int    $0x40
 58f:	c3                   	ret    

00000590 <uptime>:
SYSCALL(uptime)
 590:	b8 0e 00 00 00       	mov    $0xe,%eax
 595:	cd 40                	int    $0x40
 597:	c3                   	ret    

00000598 <start_capture>:
SYSCALL(start_capture)
 598:	b8 1e 00 00 00       	mov    $0x1e,%eax
 59d:	cd 40                	int    $0x40
 59f:	c3                   	ret    

000005a0 <stop_capture>:
SYSCALL(stop_capture)
 5a0:	b8 1f 00 00 00       	mov    $0x1f,%eax
 5a5:	cd 40                	int    $0x40
 5a7:	c3                   	ret    

000005a8 <switch_scheduler>:
SYSCALL(switch_scheduler)
 5a8:	b8 20 00 00 00       	mov    $0x20,%eax
 5ad:	cd 40                	int    $0x40
 5af:	c3                   	ret    

000005b0 <syscallstats>:
SYSCALL(syscallstats)
 5b0:	b8 21 00 00 00       	mov    $0x21,%eax
 5b5:	cd 40                	int    $0x40
 5b7:	c3                   	ret    

000005b8 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 5b8:	55                   	push   %ebp
 5b9:	89 e5                	mov    %esp,%ebp
 5bb:	83 ec 28             	sub    $0x28,%esp
 5be:	8b 45 0c             	mov    0xc(%ebp),%eax
 5c1:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 5c4:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 5cb:	00 
 5cc:	8d 45 f4             	lea    -0xc(%ebp),%eax
 5cf:	89 44 24 04          	mov    %eax,0x4(%esp)
 5d3:	8b 45 08             	mov    0x8(%ebp),%eax
 5d6:	89 04 24             	mov    %eax,(%esp)
 5d9:	e8 3a ff ff ff       	call   518 <write>
}
 5de:	c9                   	leave  
 5df:	c3                   	ret    

000005e0 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 5e0:	55                   	push   %ebp
 5e1:	89 e5                	mov    %esp,%ebp
 5e3:	53                   	push   %ebx
 5e4:	83 ec 44             	sub    $0x44,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 5e7:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 5ee:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 5f2:	74 17                	je     60b <printint+0x2b>
 5f4:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 5f8:	79 11                	jns    60b <printint+0x2b>
    neg = 1;
 5fa:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 601:	8b 45 0c             	mov    0xc(%ebp),%eax
 604:	f7 d8                	neg    %eax
 606:	89 45 f4             	mov    %eax,-0xc(%ebp)
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
 609:	eb 06                	jmp    611 <printint+0x31>
    neg = 1;
    x = -xx;
  } else {
    x = xx;
 60b:	8b 45 0c             	mov    0xc(%ebp),%eax
 60e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  }

  i = 0;
 611:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  do{
    buf[i++] = digits[x % base];
 618:	8b 4d ec             	mov    -0x14(%ebp),%ecx
 61b:	8b 5d 10             	mov    0x10(%ebp),%ebx
 61e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 621:	ba 00 00 00 00       	mov    $0x0,%edx
 626:	f7 f3                	div    %ebx
 628:	89 d0                	mov    %edx,%eax
 62a:	0f b6 80 14 0b 00 00 	movzbl 0xb14(%eax),%eax
 631:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
 635:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
  }while((x /= base) != 0);
 639:	8b 45 10             	mov    0x10(%ebp),%eax
 63c:	89 45 d4             	mov    %eax,-0x2c(%ebp)
 63f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 642:	ba 00 00 00 00       	mov    $0x0,%edx
 647:	f7 75 d4             	divl   -0x2c(%ebp)
 64a:	89 45 f4             	mov    %eax,-0xc(%ebp)
 64d:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 651:	75 c5                	jne    618 <printint+0x38>
  if(neg)
 653:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 657:	74 2a                	je     683 <printint+0xa3>
    buf[i++] = '-';
 659:	8b 45 ec             	mov    -0x14(%ebp),%eax
 65c:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)
 661:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)

  while(--i >= 0)
 665:	eb 1d                	jmp    684 <printint+0xa4>
    putc(fd, buf[i]);
 667:	8b 45 ec             	mov    -0x14(%ebp),%eax
 66a:	0f b6 44 05 dc       	movzbl -0x24(%ebp,%eax,1),%eax
 66f:	0f be c0             	movsbl %al,%eax
 672:	89 44 24 04          	mov    %eax,0x4(%esp)
 676:	8b 45 08             	mov    0x8(%ebp),%eax
 679:	89 04 24             	mov    %eax,(%esp)
 67c:	e8 37 ff ff ff       	call   5b8 <putc>
 681:	eb 01                	jmp    684 <printint+0xa4>
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 683:	90                   	nop
 684:	83 6d ec 01          	subl   $0x1,-0x14(%ebp)
 688:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 68c:	79 d9                	jns    667 <printint+0x87>
    putc(fd, buf[i]);
}
 68e:	83 c4 44             	add    $0x44,%esp
 691:	5b                   	pop    %ebx
 692:	5d                   	pop    %ebp
 693:	c3                   	ret    

00000694 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 694:	55                   	push   %ebp
 695:	89 e5                	mov    %esp,%ebp
 697:	83 ec 38             	sub    $0x38,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 69a:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 6a1:	8d 45 0c             	lea    0xc(%ebp),%eax
 6a4:	83 c0 04             	add    $0x4,%eax
 6a7:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(i = 0; fmt[i]; i++){
 6aa:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 6b1:	e9 7e 01 00 00       	jmp    834 <printf+0x1a0>
    c = fmt[i] & 0xff;
 6b6:	8b 55 0c             	mov    0xc(%ebp),%edx
 6b9:	8b 45 ec             	mov    -0x14(%ebp),%eax
 6bc:	8d 04 02             	lea    (%edx,%eax,1),%eax
 6bf:	0f b6 00             	movzbl (%eax),%eax
 6c2:	0f be c0             	movsbl %al,%eax
 6c5:	25 ff 00 00 00       	and    $0xff,%eax
 6ca:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(state == 0){
 6cd:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 6d1:	75 2c                	jne    6ff <printf+0x6b>
      if(c == '%'){
 6d3:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 6d7:	75 0c                	jne    6e5 <printf+0x51>
        state = '%';
 6d9:	c7 45 f0 25 00 00 00 	movl   $0x25,-0x10(%ebp)
 6e0:	e9 4b 01 00 00       	jmp    830 <printf+0x19c>
      } else {
        putc(fd, c);
 6e5:	8b 45 e8             	mov    -0x18(%ebp),%eax
 6e8:	0f be c0             	movsbl %al,%eax
 6eb:	89 44 24 04          	mov    %eax,0x4(%esp)
 6ef:	8b 45 08             	mov    0x8(%ebp),%eax
 6f2:	89 04 24             	mov    %eax,(%esp)
 6f5:	e8 be fe ff ff       	call   5b8 <putc>
 6fa:	e9 31 01 00 00       	jmp    830 <printf+0x19c>
      }
    } else if(state == '%'){
 6ff:	83 7d f0 25          	cmpl   $0x25,-0x10(%ebp)
 703:	0f 85 27 01 00 00    	jne    830 <printf+0x19c>
      if(c == 'd'){
 709:	83 7d e8 64          	cmpl   $0x64,-0x18(%ebp)
 70d:	75 2d                	jne    73c <printf+0xa8>
        printint(fd, *ap, 10, 1);
 70f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 712:	8b 00                	mov    (%eax),%eax
 714:	c7 44 24 0c 01 00 00 	movl   $0x1,0xc(%esp)
 71b:	00 
 71c:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
 723:	00 
 724:	89 44 24 04          	mov    %eax,0x4(%esp)
 728:	8b 45 08             	mov    0x8(%ebp),%eax
 72b:	89 04 24             	mov    %eax,(%esp)
 72e:	e8 ad fe ff ff       	call   5e0 <printint>
        ap++;
 733:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 737:	e9 ed 00 00 00       	jmp    829 <printf+0x195>
      } else if(c == 'x' || c == 'p'){
 73c:	83 7d e8 78          	cmpl   $0x78,-0x18(%ebp)
 740:	74 06                	je     748 <printf+0xb4>
 742:	83 7d e8 70          	cmpl   $0x70,-0x18(%ebp)
 746:	75 2d                	jne    775 <printf+0xe1>
        printint(fd, *ap, 16, 0);
 748:	8b 45 f4             	mov    -0xc(%ebp),%eax
 74b:	8b 00                	mov    (%eax),%eax
 74d:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
 754:	00 
 755:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
 75c:	00 
 75d:	89 44 24 04          	mov    %eax,0x4(%esp)
 761:	8b 45 08             	mov    0x8(%ebp),%eax
 764:	89 04 24             	mov    %eax,(%esp)
 767:	e8 74 fe ff ff       	call   5e0 <printint>
        ap++;
 76c:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
      }
    } else if(state == '%'){
      if(c == 'd'){
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 770:	e9 b4 00 00 00       	jmp    829 <printf+0x195>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 775:	83 7d e8 73          	cmpl   $0x73,-0x18(%ebp)
 779:	75 46                	jne    7c1 <printf+0x12d>
        s = (char*)*ap;
 77b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 77e:	8b 00                	mov    (%eax),%eax
 780:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        ap++;
 783:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
        if(s == 0)
 787:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 78b:	75 27                	jne    7b4 <printf+0x120>
          s = "(null)";
 78d:	c7 45 e4 0a 0b 00 00 	movl   $0xb0a,-0x1c(%ebp)
        while(*s != 0){
 794:	eb 1f                	jmp    7b5 <printf+0x121>
          putc(fd, *s);
 796:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 799:	0f b6 00             	movzbl (%eax),%eax
 79c:	0f be c0             	movsbl %al,%eax
 79f:	89 44 24 04          	mov    %eax,0x4(%esp)
 7a3:	8b 45 08             	mov    0x8(%ebp),%eax
 7a6:	89 04 24             	mov    %eax,(%esp)
 7a9:	e8 0a fe ff ff       	call   5b8 <putc>
          s++;
 7ae:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
 7b2:	eb 01                	jmp    7b5 <printf+0x121>
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 7b4:	90                   	nop
 7b5:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 7b8:	0f b6 00             	movzbl (%eax),%eax
 7bb:	84 c0                	test   %al,%al
 7bd:	75 d7                	jne    796 <printf+0x102>
 7bf:	eb 68                	jmp    829 <printf+0x195>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 7c1:	83 7d e8 63          	cmpl   $0x63,-0x18(%ebp)
 7c5:	75 1d                	jne    7e4 <printf+0x150>
        putc(fd, *ap);
 7c7:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7ca:	8b 00                	mov    (%eax),%eax
 7cc:	0f be c0             	movsbl %al,%eax
 7cf:	89 44 24 04          	mov    %eax,0x4(%esp)
 7d3:	8b 45 08             	mov    0x8(%ebp),%eax
 7d6:	89 04 24             	mov    %eax,(%esp)
 7d9:	e8 da fd ff ff       	call   5b8 <putc>
        ap++;
 7de:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 7e2:	eb 45                	jmp    829 <printf+0x195>
      } else if(c == '%'){
 7e4:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 7e8:	75 17                	jne    801 <printf+0x16d>
        putc(fd, c);
 7ea:	8b 45 e8             	mov    -0x18(%ebp),%eax
 7ed:	0f be c0             	movsbl %al,%eax
 7f0:	89 44 24 04          	mov    %eax,0x4(%esp)
 7f4:	8b 45 08             	mov    0x8(%ebp),%eax
 7f7:	89 04 24             	mov    %eax,(%esp)
 7fa:	e8 b9 fd ff ff       	call   5b8 <putc>
 7ff:	eb 28                	jmp    829 <printf+0x195>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 801:	c7 44 24 04 25 00 00 	movl   $0x25,0x4(%esp)
 808:	00 
 809:	8b 45 08             	mov    0x8(%ebp),%eax
 80c:	89 04 24             	mov    %eax,(%esp)
 80f:	e8 a4 fd ff ff       	call   5b8 <putc>
        putc(fd, c);
 814:	8b 45 e8             	mov    -0x18(%ebp),%eax
 817:	0f be c0             	movsbl %al,%eax
 81a:	89 44 24 04          	mov    %eax,0x4(%esp)
 81e:	8b 45 08             	mov    0x8(%ebp),%eax
 821:	89 04 24             	mov    %eax,(%esp)
 824:	e8 8f fd ff ff       	call   5b8 <putc>
      }
      state = 0;
 829:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 830:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
 834:	8b 55 0c             	mov    0xc(%ebp),%edx
 837:	8b 45 ec             	mov    -0x14(%ebp),%eax
 83a:	8d 04 02             	lea    (%edx,%eax,1),%eax
 83d:	0f b6 00             	movzbl (%eax),%eax
 840:	84 c0                	test   %al,%al
 842:	0f 85 6e fe ff ff    	jne    6b6 <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 848:	c9                   	leave  
 849:	c3                   	ret    
 84a:	90                   	nop
 84b:	90                   	nop

0000084c <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 84c:	55                   	push   %ebp
 84d:	89 e5                	mov    %esp,%ebp
 84f:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 852:	8b 45 08             	mov    0x8(%ebp),%eax
 855:	83 e8 08             	sub    $0x8,%eax
 858:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 85b:	a1 ac 0b 00 00       	mov    0xbac,%eax
 860:	89 45 fc             	mov    %eax,-0x4(%ebp)
 863:	eb 24                	jmp    889 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 865:	8b 45 fc             	mov    -0x4(%ebp),%eax
 868:	8b 00                	mov    (%eax),%eax
 86a:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 86d:	77 12                	ja     881 <free+0x35>
 86f:	8b 45 f8             	mov    -0x8(%ebp),%eax
 872:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 875:	77 24                	ja     89b <free+0x4f>
 877:	8b 45 fc             	mov    -0x4(%ebp),%eax
 87a:	8b 00                	mov    (%eax),%eax
 87c:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 87f:	77 1a                	ja     89b <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 881:	8b 45 fc             	mov    -0x4(%ebp),%eax
 884:	8b 00                	mov    (%eax),%eax
 886:	89 45 fc             	mov    %eax,-0x4(%ebp)
 889:	8b 45 f8             	mov    -0x8(%ebp),%eax
 88c:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 88f:	76 d4                	jbe    865 <free+0x19>
 891:	8b 45 fc             	mov    -0x4(%ebp),%eax
 894:	8b 00                	mov    (%eax),%eax
 896:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 899:	76 ca                	jbe    865 <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 89b:	8b 45 f8             	mov    -0x8(%ebp),%eax
 89e:	8b 40 04             	mov    0x4(%eax),%eax
 8a1:	c1 e0 03             	shl    $0x3,%eax
 8a4:	89 c2                	mov    %eax,%edx
 8a6:	03 55 f8             	add    -0x8(%ebp),%edx
 8a9:	8b 45 fc             	mov    -0x4(%ebp),%eax
 8ac:	8b 00                	mov    (%eax),%eax
 8ae:	39 c2                	cmp    %eax,%edx
 8b0:	75 24                	jne    8d6 <free+0x8a>
    bp->s.size += p->s.ptr->s.size;
 8b2:	8b 45 f8             	mov    -0x8(%ebp),%eax
 8b5:	8b 50 04             	mov    0x4(%eax),%edx
 8b8:	8b 45 fc             	mov    -0x4(%ebp),%eax
 8bb:	8b 00                	mov    (%eax),%eax
 8bd:	8b 40 04             	mov    0x4(%eax),%eax
 8c0:	01 c2                	add    %eax,%edx
 8c2:	8b 45 f8             	mov    -0x8(%ebp),%eax
 8c5:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 8c8:	8b 45 fc             	mov    -0x4(%ebp),%eax
 8cb:	8b 00                	mov    (%eax),%eax
 8cd:	8b 10                	mov    (%eax),%edx
 8cf:	8b 45 f8             	mov    -0x8(%ebp),%eax
 8d2:	89 10                	mov    %edx,(%eax)
 8d4:	eb 0a                	jmp    8e0 <free+0x94>
  } else
    bp->s.ptr = p->s.ptr;
 8d6:	8b 45 fc             	mov    -0x4(%ebp),%eax
 8d9:	8b 10                	mov    (%eax),%edx
 8db:	8b 45 f8             	mov    -0x8(%ebp),%eax
 8de:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 8e0:	8b 45 fc             	mov    -0x4(%ebp),%eax
 8e3:	8b 40 04             	mov    0x4(%eax),%eax
 8e6:	c1 e0 03             	shl    $0x3,%eax
 8e9:	03 45 fc             	add    -0x4(%ebp),%eax
 8ec:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 8ef:	75 20                	jne    911 <free+0xc5>
    p->s.size += bp->s.size;
 8f1:	8b 45 fc             	mov    -0x4(%ebp),%eax
 8f4:	8b 50 04             	mov    0x4(%eax),%edx
 8f7:	8b 45 f8             	mov    -0x8(%ebp),%eax
 8fa:	8b 40 04             	mov    0x4(%eax),%eax
 8fd:	01 c2                	add    %eax,%edx
 8ff:	8b 45 fc             	mov    -0x4(%ebp),%eax
 902:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 905:	8b 45 f8             	mov    -0x8(%ebp),%eax
 908:	8b 10                	mov    (%eax),%edx
 90a:	8b 45 fc             	mov    -0x4(%ebp),%eax
 90d:	89 10                	mov    %edx,(%eax)
 90f:	eb 08                	jmp    919 <free+0xcd>
  } else
    p->s.ptr = bp;
 911:	8b 45 fc             	mov    -0x4(%ebp),%eax
 914:	8b 55 f8             	mov    -0x8(%ebp),%edx
 917:	89 10                	mov    %edx,(%eax)
  freep = p;
 919:	8b 45 fc             	mov    -0x4(%ebp),%eax
 91c:	a3 ac 0b 00 00       	mov    %eax,0xbac
}
 921:	c9                   	leave  
 922:	c3                   	ret    

00000923 <morecore>:

static Header*
morecore(uint nu)
{
 923:	55                   	push   %ebp
 924:	89 e5                	mov    %esp,%ebp
 926:	83 ec 28             	sub    $0x28,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 929:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 930:	77 07                	ja     939 <morecore+0x16>
    nu = 4096;
 932:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 939:	8b 45 08             	mov    0x8(%ebp),%eax
 93c:	c1 e0 03             	shl    $0x3,%eax
 93f:	89 04 24             	mov    %eax,(%esp)
 942:	e8 39 fc ff ff       	call   580 <sbrk>
 947:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(p == (char*)-1)
 94a:	83 7d f0 ff          	cmpl   $0xffffffff,-0x10(%ebp)
 94e:	75 07                	jne    957 <morecore+0x34>
    return 0;
 950:	b8 00 00 00 00       	mov    $0x0,%eax
 955:	eb 22                	jmp    979 <morecore+0x56>
  hp = (Header*)p;
 957:	8b 45 f0             	mov    -0x10(%ebp),%eax
 95a:	89 45 f4             	mov    %eax,-0xc(%ebp)
  hp->s.size = nu;
 95d:	8b 45 f4             	mov    -0xc(%ebp),%eax
 960:	8b 55 08             	mov    0x8(%ebp),%edx
 963:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 966:	8b 45 f4             	mov    -0xc(%ebp),%eax
 969:	83 c0 08             	add    $0x8,%eax
 96c:	89 04 24             	mov    %eax,(%esp)
 96f:	e8 d8 fe ff ff       	call   84c <free>
  return freep;
 974:	a1 ac 0b 00 00       	mov    0xbac,%eax
}
 979:	c9                   	leave  
 97a:	c3                   	ret    

0000097b <malloc>:

void*
malloc(uint nbytes)
{
 97b:	55                   	push   %ebp
 97c:	89 e5                	mov    %esp,%ebp
 97e:	83 ec 28             	sub    $0x28,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 981:	8b 45 08             	mov    0x8(%ebp),%eax
 984:	83 c0 07             	add    $0x7,%eax
 987:	c1 e8 03             	shr    $0x3,%eax
 98a:	83 c0 01             	add    $0x1,%eax
 98d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((prevp = freep) == 0){
 990:	a1 ac 0b 00 00       	mov    0xbac,%eax
 995:	89 45 f0             	mov    %eax,-0x10(%ebp)
 998:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 99c:	75 23                	jne    9c1 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 99e:	c7 45 f0 a4 0b 00 00 	movl   $0xba4,-0x10(%ebp)
 9a5:	8b 45 f0             	mov    -0x10(%ebp),%eax
 9a8:	a3 ac 0b 00 00       	mov    %eax,0xbac
 9ad:	a1 ac 0b 00 00       	mov    0xbac,%eax
 9b2:	a3 a4 0b 00 00       	mov    %eax,0xba4
    base.s.size = 0;
 9b7:	c7 05 a8 0b 00 00 00 	movl   $0x0,0xba8
 9be:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 9c1:	8b 45 f0             	mov    -0x10(%ebp),%eax
 9c4:	8b 00                	mov    (%eax),%eax
 9c6:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(p->s.size >= nunits){
 9c9:	8b 45 ec             	mov    -0x14(%ebp),%eax
 9cc:	8b 40 04             	mov    0x4(%eax),%eax
 9cf:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 9d2:	72 4d                	jb     a21 <malloc+0xa6>
      if(p->s.size == nunits)
 9d4:	8b 45 ec             	mov    -0x14(%ebp),%eax
 9d7:	8b 40 04             	mov    0x4(%eax),%eax
 9da:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 9dd:	75 0c                	jne    9eb <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 9df:	8b 45 ec             	mov    -0x14(%ebp),%eax
 9e2:	8b 10                	mov    (%eax),%edx
 9e4:	8b 45 f0             	mov    -0x10(%ebp),%eax
 9e7:	89 10                	mov    %edx,(%eax)
 9e9:	eb 26                	jmp    a11 <malloc+0x96>
      else {
        p->s.size -= nunits;
 9eb:	8b 45 ec             	mov    -0x14(%ebp),%eax
 9ee:	8b 40 04             	mov    0x4(%eax),%eax
 9f1:	89 c2                	mov    %eax,%edx
 9f3:	2b 55 f4             	sub    -0xc(%ebp),%edx
 9f6:	8b 45 ec             	mov    -0x14(%ebp),%eax
 9f9:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 9fc:	8b 45 ec             	mov    -0x14(%ebp),%eax
 9ff:	8b 40 04             	mov    0x4(%eax),%eax
 a02:	c1 e0 03             	shl    $0x3,%eax
 a05:	01 45 ec             	add    %eax,-0x14(%ebp)
        p->s.size = nunits;
 a08:	8b 45 ec             	mov    -0x14(%ebp),%eax
 a0b:	8b 55 f4             	mov    -0xc(%ebp),%edx
 a0e:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 a11:	8b 45 f0             	mov    -0x10(%ebp),%eax
 a14:	a3 ac 0b 00 00       	mov    %eax,0xbac
      return (void*)(p + 1);
 a19:	8b 45 ec             	mov    -0x14(%ebp),%eax
 a1c:	83 c0 08             	add    $0x8,%eax
 a1f:	eb 38                	jmp    a59 <malloc+0xde>
    }
    if(p == freep)
 a21:	a1 ac 0b 00 00       	mov    0xbac,%eax
 a26:	39 45 ec             	cmp    %eax,-0x14(%ebp)
 a29:	75 1b                	jne    a46 <malloc+0xcb>
      if((p = morecore(nunits)) == 0)
 a2b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 a2e:	89 04 24             	mov    %eax,(%esp)
 a31:	e8 ed fe ff ff       	call   923 <morecore>
 a36:	89 45 ec             	mov    %eax,-0x14(%ebp)
 a39:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 a3d:	75 07                	jne    a46 <malloc+0xcb>
        return 0;
 a3f:	b8 00 00 00 00       	mov    $0x0,%eax
 a44:	eb 13                	jmp    a59 <malloc+0xde>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 a46:	8b 45 ec             	mov    -0x14(%ebp),%eax
 a49:	89 45 f0             	mov    %eax,-0x10(%ebp)
 a4c:	8b 45 ec             	mov    -0x14(%ebp),%eax
 a4f:	8b 00                	mov    (%eax),%eax
 a51:	89 45 ec             	mov    %eax,-0x14(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 a54:	e9 70 ff ff ff       	jmp    9c9 <malloc+0x4e>
}
 a59:	c9                   	leave  
 a5a:	c3                   	ret    
