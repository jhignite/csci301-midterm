#include "types.h"
#include "stat.h"
#include "user.h"

int
getcmd(char *buf, int nbuf)
{
	printf(2, "$ ");
	memset(buf, 0, nbuf);
	gets(buf, nbuf);
	if(buf[0] == 0) // EOF
		return -1;
	return 0;
}

int
main(int argc, char *argv[])
{
	static char buf[100];
	//static char buf2[100];
	char *bufarg = buf+2;
	char **bufargv = &bufarg;
	char *prompt = "Choose an action: l = list files, d = delete a file, r = rename a file, q = quit => ";
	char *largv[] = {0};
	printf(2, prompt);
	while(getcmd(buf, sizeof(buf)) >= 0){
		if(buf[0] == 'l'){
    			exec("ls", largv);
			printf(2, prompt);
		} else if(buf[0] == 'd'){
			//Don't know why this isn't working
			printf(2, "Name of file to delete: ");
			while(getcmd(buf, sizeof(buf)) >= 0){
				char *rargv[] = {buf};
				printf(2, rargv[0]);
				exec("rm", rargv);
				break;
			}
			printf(2, prompt);
		} else if(buf[0] == 'r'){
			printf(2, "Enter original file name: ");
			char *temp;
			while(getcmd(buf, sizeof(buf)) >= 0){
				temp = buf+' ';
				break;
			}
			printf(2, "Enter new file name: ");
			while(getcmd(buf, sizeof(buf)) >= 0){
				char *lnargv[] = {temp, buf, " ", 0};
				char *rargv[] = {temp};
				exec("ln", lnargv);
				exec("rm", rargv);
			}
			exec("ln", bufargv);
			buf[strlen(buf)-1] = 0;
		} else if(buf[0] == 'q')
			exit();
		buf[strlen(buf)-1] = 0;
	}
	return 1;
}
