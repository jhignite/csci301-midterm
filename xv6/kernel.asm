
kernel:     file format elf32-i386


Disassembly of section .text:

80100000 <multiboot_header>:
80100000:	02 b0 ad 1b 00 00    	add    0x1bad(%eax),%dh
80100006:	00 00                	add    %al,(%eax)
80100008:	fe 4f 52             	decb   0x52(%edi)
8010000b:	e4 0f                	in     $0xf,%al

8010000c <entry>:

# Entering xv6 on boot processor, with paging off.
.globl entry
entry:
  # Turn on page size extension for 4Mbyte pages
  movl    %cr4, %eax
8010000c:	0f 20 e0             	mov    %cr4,%eax
  orl     $(CR4_PSE), %eax
8010000f:	83 c8 10             	or     $0x10,%eax
  movl    %eax, %cr4
80100012:	0f 22 e0             	mov    %eax,%cr4
  # Set page directory
  movl    $(V2P_WO(entrypgdir)), %eax
80100015:	b8 00 b0 10 00       	mov    $0x10b000,%eax
  movl    %eax, %cr3
8010001a:	0f 22 d8             	mov    %eax,%cr3
  # Turn on paging.
  movl    %cr0, %eax
8010001d:	0f 20 c0             	mov    %cr0,%eax
  orl     $(CR0_PG|CR0_WP), %eax
80100020:	0d 00 00 01 80       	or     $0x80010000,%eax
  movl    %eax, %cr0
80100025:	0f 22 c0             	mov    %eax,%cr0

  # Set up the stack pointer.
  movl    $(stack + KSTACKSIZE), %esp
80100028:	bc d0 de 10 80       	mov    $0x8010ded0,%esp
  # Jump to main() and switch to executing at high addresses.
  # Need an explicit long jump (indirect jump would work as
  # well, as would a push/ret trick to save 1 byte) because
  # the assembler produces a PC-relative instruction for a
  # direct jump.
  ljmp    $(SEG_KCODE<<3), $main
8010002d:	ea 56 3d 10 80 08 00 	ljmp   $0x8,$0x80103d56

80100034 <binit>:
  struct buf head;
} bcache;

void
binit(void)
{
80100034:	55                   	push   %ebp
80100035:	89 e5                	mov    %esp,%ebp
80100037:	83 ec 28             	sub    $0x28,%esp
  struct buf *b;

  initlock(&bcache.lock, "bcache");
8010003a:	c7 44 24 04 40 8e 10 	movl   $0x80108e40,0x4(%esp)
80100041:	80 
80100042:	c7 04 24 e0 de 10 80 	movl   $0x8010dee0,(%esp)
80100049:	e8 d8 56 00 00       	call   80105726 <initlock>

//PAGEBREAK!
  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
8010004e:	c7 05 f0 1d 11 80 e4 	movl   $0x80111de4,0x80111df0
80100055:	1d 11 80 
  bcache.head.next = &bcache.head;
80100058:	c7 05 f4 1d 11 80 e4 	movl   $0x80111de4,0x80111df4
8010005f:	1d 11 80 
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
80100062:	c7 45 f4 14 df 10 80 	movl   $0x8010df14,-0xc(%ebp)
80100069:	eb 3a                	jmp    801000a5 <binit+0x71>
    b->next = bcache.head.next;
8010006b:	8b 15 f4 1d 11 80    	mov    0x80111df4,%edx
80100071:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100074:	89 50 10             	mov    %edx,0x10(%eax)
    b->prev = &bcache.head;
80100077:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010007a:	c7 40 0c e4 1d 11 80 	movl   $0x80111de4,0xc(%eax)
    b->dev = -1;
80100081:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100084:	c7 40 04 ff ff ff ff 	movl   $0xffffffff,0x4(%eax)
    bcache.head.next->prev = b;
8010008b:	a1 f4 1d 11 80       	mov    0x80111df4,%eax
80100090:	8b 55 f4             	mov    -0xc(%ebp),%edx
80100093:	89 50 0c             	mov    %edx,0xc(%eax)
    bcache.head.next = b;
80100096:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100099:	a3 f4 1d 11 80       	mov    %eax,0x80111df4

//PAGEBREAK!
  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
  bcache.head.next = &bcache.head;
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
8010009e:	81 45 f4 18 02 00 00 	addl   $0x218,-0xc(%ebp)
801000a5:	b8 e4 1d 11 80       	mov    $0x80111de4,%eax
801000aa:	39 45 f4             	cmp    %eax,-0xc(%ebp)
801000ad:	72 bc                	jb     8010006b <binit+0x37>
    b->prev = &bcache.head;
    b->dev = -1;
    bcache.head.next->prev = b;
    bcache.head.next = b;
  }
}
801000af:	c9                   	leave  
801000b0:	c3                   	ret    

801000b1 <bget>:
// Look through buffer cache for block on device dev.
// If not found, allocate a buffer.
// In either case, return B_BUSY buffer.
static struct buf*
bget(uint dev, uint blockno)
{
801000b1:	55                   	push   %ebp
801000b2:	89 e5                	mov    %esp,%ebp
801000b4:	83 ec 28             	sub    $0x28,%esp
  struct buf *b;

  acquire(&bcache.lock);
801000b7:	c7 04 24 e0 de 10 80 	movl   $0x8010dee0,(%esp)
801000be:	e8 84 56 00 00       	call   80105747 <acquire>

 loop:
  // Is the block already cached?
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
801000c3:	a1 f4 1d 11 80       	mov    0x80111df4,%eax
801000c8:	89 45 f4             	mov    %eax,-0xc(%ebp)
801000cb:	eb 63                	jmp    80100130 <bget+0x7f>
    if(b->dev == dev && b->blockno == blockno){
801000cd:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000d0:	8b 40 04             	mov    0x4(%eax),%eax
801000d3:	3b 45 08             	cmp    0x8(%ebp),%eax
801000d6:	75 4f                	jne    80100127 <bget+0x76>
801000d8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000db:	8b 40 08             	mov    0x8(%eax),%eax
801000de:	3b 45 0c             	cmp    0xc(%ebp),%eax
801000e1:	75 44                	jne    80100127 <bget+0x76>
      if(!(b->flags & B_BUSY)){
801000e3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000e6:	8b 00                	mov    (%eax),%eax
801000e8:	83 e0 01             	and    $0x1,%eax
801000eb:	85 c0                	test   %eax,%eax
801000ed:	75 23                	jne    80100112 <bget+0x61>
        b->flags |= B_BUSY;
801000ef:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000f2:	8b 00                	mov    (%eax),%eax
801000f4:	89 c2                	mov    %eax,%edx
801000f6:	83 ca 01             	or     $0x1,%edx
801000f9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000fc:	89 10                	mov    %edx,(%eax)
        release(&bcache.lock);
801000fe:	c7 04 24 e0 de 10 80 	movl   $0x8010dee0,(%esp)
80100105:	e8 a6 56 00 00       	call   801057b0 <release>
        return b;
8010010a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010010d:	e9 93 00 00 00       	jmp    801001a5 <bget+0xf4>
      }
      sleep(b, &bcache.lock);
80100112:	c7 44 24 04 e0 de 10 	movl   $0x8010dee0,0x4(%esp)
80100119:	80 
8010011a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010011d:	89 04 24             	mov    %eax,(%esp)
80100120:	e8 f9 52 00 00       	call   8010541e <sleep>
      goto loop;
80100125:	eb 9c                	jmp    801000c3 <bget+0x12>

  acquire(&bcache.lock);

 loop:
  // Is the block already cached?
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
80100127:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010012a:	8b 40 10             	mov    0x10(%eax),%eax
8010012d:	89 45 f4             	mov    %eax,-0xc(%ebp)
80100130:	81 7d f4 e4 1d 11 80 	cmpl   $0x80111de4,-0xc(%ebp)
80100137:	75 94                	jne    801000cd <bget+0x1c>
  }

  // Not cached; recycle some non-busy and clean buffer.
  // "clean" because B_DIRTY and !B_BUSY means log.c
  // hasn't yet committed the changes to the buffer.
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
80100139:	a1 f0 1d 11 80       	mov    0x80111df0,%eax
8010013e:	89 45 f4             	mov    %eax,-0xc(%ebp)
80100141:	eb 4d                	jmp    80100190 <bget+0xdf>
    if((b->flags & B_BUSY) == 0 && (b->flags & B_DIRTY) == 0){
80100143:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100146:	8b 00                	mov    (%eax),%eax
80100148:	83 e0 01             	and    $0x1,%eax
8010014b:	85 c0                	test   %eax,%eax
8010014d:	75 38                	jne    80100187 <bget+0xd6>
8010014f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100152:	8b 00                	mov    (%eax),%eax
80100154:	83 e0 04             	and    $0x4,%eax
80100157:	85 c0                	test   %eax,%eax
80100159:	75 2c                	jne    80100187 <bget+0xd6>
      b->dev = dev;
8010015b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010015e:	8b 55 08             	mov    0x8(%ebp),%edx
80100161:	89 50 04             	mov    %edx,0x4(%eax)
      b->blockno = blockno;
80100164:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100167:	8b 55 0c             	mov    0xc(%ebp),%edx
8010016a:	89 50 08             	mov    %edx,0x8(%eax)
      b->flags = B_BUSY;
8010016d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100170:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
      release(&bcache.lock);
80100176:	c7 04 24 e0 de 10 80 	movl   $0x8010dee0,(%esp)
8010017d:	e8 2e 56 00 00       	call   801057b0 <release>
      return b;
80100182:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100185:	eb 1e                	jmp    801001a5 <bget+0xf4>
  }

  // Not cached; recycle some non-busy and clean buffer.
  // "clean" because B_DIRTY and !B_BUSY means log.c
  // hasn't yet committed the changes to the buffer.
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
80100187:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010018a:	8b 40 0c             	mov    0xc(%eax),%eax
8010018d:	89 45 f4             	mov    %eax,-0xc(%ebp)
80100190:	81 7d f4 e4 1d 11 80 	cmpl   $0x80111de4,-0xc(%ebp)
80100197:	75 aa                	jne    80100143 <bget+0x92>
      b->flags = B_BUSY;
      release(&bcache.lock);
      return b;
    }
  }
  panic("bget: no buffers");
80100199:	c7 04 24 47 8e 10 80 	movl   $0x80108e47,(%esp)
801001a0:	e8 c4 06 00 00       	call   80100869 <panic>
}
801001a5:	c9                   	leave  
801001a6:	c3                   	ret    

801001a7 <bread>:

// Return a B_BUSY buf with the contents of the indicated block.
struct buf*
bread(uint dev, uint blockno)
{
801001a7:	55                   	push   %ebp
801001a8:	89 e5                	mov    %esp,%ebp
801001aa:	83 ec 28             	sub    $0x28,%esp
  struct buf *b;

  b = bget(dev, blockno);
801001ad:	8b 45 0c             	mov    0xc(%ebp),%eax
801001b0:	89 44 24 04          	mov    %eax,0x4(%esp)
801001b4:	8b 45 08             	mov    0x8(%ebp),%eax
801001b7:	89 04 24             	mov    %eax,(%esp)
801001ba:	e8 f2 fe ff ff       	call   801000b1 <bget>
801001bf:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(!(b->flags & B_VALID)) {
801001c2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801001c5:	8b 00                	mov    (%eax),%eax
801001c7:	83 e0 02             	and    $0x2,%eax
801001ca:	85 c0                	test   %eax,%eax
801001cc:	75 0b                	jne    801001d9 <bread+0x32>
    iderw(b);
801001ce:	8b 45 f4             	mov    -0xc(%ebp),%eax
801001d1:	89 04 24             	mov    %eax,(%esp)
801001d4:	e8 37 2e 00 00       	call   80103010 <iderw>
  }
  return b;
801001d9:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801001dc:	c9                   	leave  
801001dd:	c3                   	ret    

801001de <bwrite>:

// Write b's contents to disk.  Must be B_BUSY.
void
bwrite(struct buf *b)
{
801001de:	55                   	push   %ebp
801001df:	89 e5                	mov    %esp,%ebp
801001e1:	83 ec 18             	sub    $0x18,%esp
  if((b->flags & B_BUSY) == 0)
801001e4:	8b 45 08             	mov    0x8(%ebp),%eax
801001e7:	8b 00                	mov    (%eax),%eax
801001e9:	83 e0 01             	and    $0x1,%eax
801001ec:	85 c0                	test   %eax,%eax
801001ee:	75 0c                	jne    801001fc <bwrite+0x1e>
    panic("bwrite");
801001f0:	c7 04 24 58 8e 10 80 	movl   $0x80108e58,(%esp)
801001f7:	e8 6d 06 00 00       	call   80100869 <panic>
  b->flags |= B_DIRTY;
801001fc:	8b 45 08             	mov    0x8(%ebp),%eax
801001ff:	8b 00                	mov    (%eax),%eax
80100201:	89 c2                	mov    %eax,%edx
80100203:	83 ca 04             	or     $0x4,%edx
80100206:	8b 45 08             	mov    0x8(%ebp),%eax
80100209:	89 10                	mov    %edx,(%eax)
  iderw(b);
8010020b:	8b 45 08             	mov    0x8(%ebp),%eax
8010020e:	89 04 24             	mov    %eax,(%esp)
80100211:	e8 fa 2d 00 00       	call   80103010 <iderw>
}
80100216:	c9                   	leave  
80100217:	c3                   	ret    

80100218 <brelse>:

// Release a B_BUSY buffer.
// Move to the head of the MRU list.
void
brelse(struct buf *b)
{
80100218:	55                   	push   %ebp
80100219:	89 e5                	mov    %esp,%ebp
8010021b:	83 ec 18             	sub    $0x18,%esp
  if((b->flags & B_BUSY) == 0)
8010021e:	8b 45 08             	mov    0x8(%ebp),%eax
80100221:	8b 00                	mov    (%eax),%eax
80100223:	83 e0 01             	and    $0x1,%eax
80100226:	85 c0                	test   %eax,%eax
80100228:	75 0c                	jne    80100236 <brelse+0x1e>
    panic("brelse");
8010022a:	c7 04 24 5f 8e 10 80 	movl   $0x80108e5f,(%esp)
80100231:	e8 33 06 00 00       	call   80100869 <panic>

  acquire(&bcache.lock);
80100236:	c7 04 24 e0 de 10 80 	movl   $0x8010dee0,(%esp)
8010023d:	e8 05 55 00 00       	call   80105747 <acquire>

  b->next->prev = b->prev;
80100242:	8b 45 08             	mov    0x8(%ebp),%eax
80100245:	8b 40 10             	mov    0x10(%eax),%eax
80100248:	8b 55 08             	mov    0x8(%ebp),%edx
8010024b:	8b 52 0c             	mov    0xc(%edx),%edx
8010024e:	89 50 0c             	mov    %edx,0xc(%eax)
  b->prev->next = b->next;
80100251:	8b 45 08             	mov    0x8(%ebp),%eax
80100254:	8b 40 0c             	mov    0xc(%eax),%eax
80100257:	8b 55 08             	mov    0x8(%ebp),%edx
8010025a:	8b 52 10             	mov    0x10(%edx),%edx
8010025d:	89 50 10             	mov    %edx,0x10(%eax)
  b->next = bcache.head.next;
80100260:	8b 15 f4 1d 11 80    	mov    0x80111df4,%edx
80100266:	8b 45 08             	mov    0x8(%ebp),%eax
80100269:	89 50 10             	mov    %edx,0x10(%eax)
  b->prev = &bcache.head;
8010026c:	8b 45 08             	mov    0x8(%ebp),%eax
8010026f:	c7 40 0c e4 1d 11 80 	movl   $0x80111de4,0xc(%eax)
  bcache.head.next->prev = b;
80100276:	a1 f4 1d 11 80       	mov    0x80111df4,%eax
8010027b:	8b 55 08             	mov    0x8(%ebp),%edx
8010027e:	89 50 0c             	mov    %edx,0xc(%eax)
  bcache.head.next = b;
80100281:	8b 45 08             	mov    0x8(%ebp),%eax
80100284:	a3 f4 1d 11 80       	mov    %eax,0x80111df4

  b->flags &= ~B_BUSY;
80100289:	8b 45 08             	mov    0x8(%ebp),%eax
8010028c:	8b 00                	mov    (%eax),%eax
8010028e:	89 c2                	mov    %eax,%edx
80100290:	83 e2 fe             	and    $0xfffffffe,%edx
80100293:	8b 45 08             	mov    0x8(%ebp),%eax
80100296:	89 10                	mov    %edx,(%eax)
  wakeup(b);
80100298:	8b 45 08             	mov    0x8(%ebp),%eax
8010029b:	89 04 24             	mov    %eax,(%esp)
8010029e:	e8 58 52 00 00       	call   801054fb <wakeup>

  release(&bcache.lock);
801002a3:	c7 04 24 e0 de 10 80 	movl   $0x8010dee0,(%esp)
801002aa:	e8 01 55 00 00       	call   801057b0 <release>
}
801002af:	c9                   	leave  
801002b0:	c3                   	ret    
801002b1:	00 00                	add    %al,(%eax)
	...

801002b4 <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
801002b4:	55                   	push   %ebp
801002b5:	89 e5                	mov    %esp,%ebp
801002b7:	83 ec 14             	sub    $0x14,%esp
801002ba:	8b 45 08             	mov    0x8(%ebp),%eax
801002bd:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801002c1:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
801002c5:	89 c2                	mov    %eax,%edx
801002c7:	ec                   	in     (%dx),%al
801002c8:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
801002cb:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
801002cf:	c9                   	leave  
801002d0:	c3                   	ret    

801002d1 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
801002d1:	55                   	push   %ebp
801002d2:	89 e5                	mov    %esp,%ebp
801002d4:	83 ec 08             	sub    $0x8,%esp
801002d7:	8b 55 08             	mov    0x8(%ebp),%edx
801002da:	8b 45 0c             	mov    0xc(%ebp),%eax
801002dd:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
801002e1:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801002e4:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
801002e8:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
801002ec:	ee                   	out    %al,(%dx)
}
801002ed:	c9                   	leave  
801002ee:	c3                   	ret    

801002ef <cmosread>:
#include "date.h"
#include "x86.h"
#include "cmos.h"

uint cmosread(uint reg)
{
801002ef:	55                   	push   %ebp
801002f0:	89 e5                	mov    %esp,%ebp
801002f2:	83 ec 18             	sub    $0x18,%esp
  if(reg >= CMOS_NMI_BIT)
801002f5:	83 7d 08 7f          	cmpl   $0x7f,0x8(%ebp)
801002f9:	76 0c                	jbe    80100307 <cmosread+0x18>
    panic("cmosread: invalid register");
801002fb:	c7 04 24 66 8e 10 80 	movl   $0x80108e66,(%esp)
80100302:	e8 62 05 00 00       	call   80100869 <panic>

  outb(CMOS_PORT, reg);
80100307:	8b 45 08             	mov    0x8(%ebp),%eax
8010030a:	0f b6 c0             	movzbl %al,%eax
8010030d:	89 44 24 04          	mov    %eax,0x4(%esp)
80100311:	c7 04 24 70 00 00 00 	movl   $0x70,(%esp)
80100318:	e8 b4 ff ff ff       	call   801002d1 <outb>
  microdelay(200);
8010031d:	c7 04 24 c8 00 00 00 	movl   $0xc8,(%esp)
80100324:	e8 52 34 00 00       	call   8010377b <microdelay>
  return inb(CMOS_RETURN);
80100329:	c7 04 24 71 00 00 00 	movl   $0x71,(%esp)
80100330:	e8 7f ff ff ff       	call   801002b4 <inb>
80100335:	0f b6 c0             	movzbl %al,%eax
}
80100338:	c9                   	leave  
80100339:	c3                   	ret    

8010033a <cmoswrite>:

void cmoswrite(uint reg, uint data)
{
8010033a:	55                   	push   %ebp
8010033b:	89 e5                	mov    %esp,%ebp
8010033d:	83 ec 18             	sub    $0x18,%esp
  if(reg >= CMOS_NMI_BIT)
80100340:	83 7d 08 7f          	cmpl   $0x7f,0x8(%ebp)
80100344:	76 0c                	jbe    80100352 <cmoswrite+0x18>
    panic("cmoswrite: invalid register");
80100346:	c7 04 24 81 8e 10 80 	movl   $0x80108e81,(%esp)
8010034d:	e8 17 05 00 00       	call   80100869 <panic>
  if(data > 0xff)
80100352:	81 7d 0c ff 00 00 00 	cmpl   $0xff,0xc(%ebp)
80100359:	76 0c                	jbe    80100367 <cmoswrite+0x2d>
    panic("cmoswrite: invalid data");
8010035b:	c7 04 24 9d 8e 10 80 	movl   $0x80108e9d,(%esp)
80100362:	e8 02 05 00 00       	call   80100869 <panic>

  outb(CMOS_PORT,  reg);
80100367:	8b 45 08             	mov    0x8(%ebp),%eax
8010036a:	0f b6 c0             	movzbl %al,%eax
8010036d:	89 44 24 04          	mov    %eax,0x4(%esp)
80100371:	c7 04 24 70 00 00 00 	movl   $0x70,(%esp)
80100378:	e8 54 ff ff ff       	call   801002d1 <outb>
  microdelay(200);
8010037d:	c7 04 24 c8 00 00 00 	movl   $0xc8,(%esp)
80100384:	e8 f2 33 00 00       	call   8010377b <microdelay>
  outb(CMOS_RETURN, data);
80100389:	8b 45 0c             	mov    0xc(%ebp),%eax
8010038c:	0f b6 c0             	movzbl %al,%eax
8010038f:	89 44 24 04          	mov    %eax,0x4(%esp)
80100393:	c7 04 24 71 00 00 00 	movl   $0x71,(%esp)
8010039a:	e8 32 ff ff ff       	call   801002d1 <outb>
}
8010039f:	c9                   	leave  
801003a0:	c3                   	ret    

801003a1 <fill_rtcdate>:

static void fill_rtcdate(struct rtcdate *r)
{
801003a1:	55                   	push   %ebp
801003a2:	89 e5                	mov    %esp,%ebp
801003a4:	83 ec 18             	sub    $0x18,%esp
  r->second = cmosread(CMOS_SECS);
801003a7:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
801003ae:	e8 3c ff ff ff       	call   801002ef <cmosread>
801003b3:	8b 55 08             	mov    0x8(%ebp),%edx
801003b6:	89 02                	mov    %eax,(%edx)
  r->minute = cmosread(CMOS_MINS);
801003b8:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
801003bf:	e8 2b ff ff ff       	call   801002ef <cmosread>
801003c4:	8b 55 08             	mov    0x8(%ebp),%edx
801003c7:	89 42 04             	mov    %eax,0x4(%edx)
  r->hour   = cmosread(CMOS_HOURS);
801003ca:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
801003d1:	e8 19 ff ff ff       	call   801002ef <cmosread>
801003d6:	8b 55 08             	mov    0x8(%ebp),%edx
801003d9:	89 42 08             	mov    %eax,0x8(%edx)
  r->day    = cmosread(CMOS_DAY);
801003dc:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
801003e3:	e8 07 ff ff ff       	call   801002ef <cmosread>
801003e8:	8b 55 08             	mov    0x8(%ebp),%edx
801003eb:	89 42 0c             	mov    %eax,0xc(%edx)
  r->month  = cmosread(CMOS_MONTH);
801003ee:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
801003f5:	e8 f5 fe ff ff       	call   801002ef <cmosread>
801003fa:	8b 55 08             	mov    0x8(%ebp),%edx
801003fd:	89 42 10             	mov    %eax,0x10(%edx)
  r->year   = cmosread(CMOS_YEAR);
80100400:	c7 04 24 09 00 00 00 	movl   $0x9,(%esp)
80100407:	e8 e3 fe ff ff       	call   801002ef <cmosread>
8010040c:	8b 55 08             	mov    0x8(%ebp),%edx
8010040f:	89 42 14             	mov    %eax,0x14(%edx)
}
80100412:	c9                   	leave  
80100413:	c3                   	ret    

80100414 <cmostime>:

// qemu seems to use 24-hour GWT and the values are BCD encoded
void cmostime(struct rtcdate *r)
{
80100414:	55                   	push   %ebp
80100415:	89 e5                	mov    %esp,%ebp
80100417:	83 ec 58             	sub    $0x58,%esp
  struct rtcdate t1, t2;
  int sb, bcd, tf;

  sb = cmosread(CMOS_STATB);
8010041a:	c7 04 24 0b 00 00 00 	movl   $0xb,(%esp)
80100421:	e8 c9 fe ff ff       	call   801002ef <cmosread>
80100426:	89 45 ec             	mov    %eax,-0x14(%ebp)

  bcd = (sb & CMOS_BINARY_BIT) == 0;
80100429:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010042c:	83 e0 04             	and    $0x4,%eax
8010042f:	85 c0                	test   %eax,%eax
80100431:	0f 94 c0             	sete   %al
80100434:	0f b6 c0             	movzbl %al,%eax
80100437:	89 45 f0             	mov    %eax,-0x10(%ebp)
  tf = (sb & CMOS_24H_BIT) != 0;
8010043a:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010043d:	83 e0 02             	and    $0x2,%eax
80100440:	85 c0                	test   %eax,%eax
80100442:	0f 95 c0             	setne  %al
80100445:	0f b6 c0             	movzbl %al,%eax
80100448:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010044b:	eb 01                	jmp    8010044e <cmostime+0x3a>
    if(cmosread(CMOS_STATA) & CMOS_UIP_BIT)
      continue;
    fill_rtcdate(&t2);
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
      break;
  }
8010044d:	90                   	nop
  bcd = (sb & CMOS_BINARY_BIT) == 0;
  tf = (sb & CMOS_24H_BIT) != 0;

  // make sure CMOS doesn't modify time while we read it
  for(;;){
    fill_rtcdate(&t1);
8010044e:	8d 45 d4             	lea    -0x2c(%ebp),%eax
80100451:	89 04 24             	mov    %eax,(%esp)
80100454:	e8 48 ff ff ff       	call   801003a1 <fill_rtcdate>
    if(cmosread(CMOS_STATA) & CMOS_UIP_BIT)
80100459:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
80100460:	e8 8a fe ff ff       	call   801002ef <cmosread>
80100465:	25 80 00 00 00       	and    $0x80,%eax
8010046a:	85 c0                	test   %eax,%eax
8010046c:	74 03                	je     80100471 <cmostime+0x5d>
      continue;
8010046e:	90                   	nop
    fill_rtcdate(&t2);
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
      break;
  }
8010046f:	eb dd                	jmp    8010044e <cmostime+0x3a>
  // make sure CMOS doesn't modify time while we read it
  for(;;){
    fill_rtcdate(&t1);
    if(cmosread(CMOS_STATA) & CMOS_UIP_BIT)
      continue;
    fill_rtcdate(&t2);
80100471:	8d 45 bc             	lea    -0x44(%ebp),%eax
80100474:	89 04 24             	mov    %eax,(%esp)
80100477:	e8 25 ff ff ff       	call   801003a1 <fill_rtcdate>
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
8010047c:	c7 44 24 08 18 00 00 	movl   $0x18,0x8(%esp)
80100483:	00 
80100484:	8d 45 bc             	lea    -0x44(%ebp),%eax
80100487:	89 44 24 04          	mov    %eax,0x4(%esp)
8010048b:	8d 45 d4             	lea    -0x2c(%ebp),%eax
8010048e:	89 04 24             	mov    %eax,(%esp)
80100491:	e8 8b 55 00 00       	call   80105a21 <memcmp>
80100496:	85 c0                	test   %eax,%eax
80100498:	75 b3                	jne    8010044d <cmostime+0x39>
      break;
8010049a:	90                   	nop
  }

  // backup raw values since BCD conversion removes PM bit from hour
  t2 = t1;
8010049b:	8b 45 d4             	mov    -0x2c(%ebp),%eax
8010049e:	89 45 bc             	mov    %eax,-0x44(%ebp)
801004a1:	8b 45 d8             	mov    -0x28(%ebp),%eax
801004a4:	89 45 c0             	mov    %eax,-0x40(%ebp)
801004a7:	8b 45 dc             	mov    -0x24(%ebp),%eax
801004aa:	89 45 c4             	mov    %eax,-0x3c(%ebp)
801004ad:	8b 45 e0             	mov    -0x20(%ebp),%eax
801004b0:	89 45 c8             	mov    %eax,-0x38(%ebp)
801004b3:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801004b6:	89 45 cc             	mov    %eax,-0x34(%ebp)
801004b9:	8b 45 e8             	mov    -0x18(%ebp),%eax
801004bc:	89 45 d0             	mov    %eax,-0x30(%ebp)

  // convert t1 from BCD
  if(bcd){
801004bf:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801004c3:	0f 84 a8 00 00 00    	je     80100571 <cmostime+0x15d>
#define    CONV(x)     (t1.x = ((t1.x >> 4) * 10) + (t1.x & 0xf))
    CONV(second);
801004c9:	8b 45 d4             	mov    -0x2c(%ebp),%eax
801004cc:	89 c2                	mov    %eax,%edx
801004ce:	c1 ea 04             	shr    $0x4,%edx
801004d1:	89 d0                	mov    %edx,%eax
801004d3:	c1 e0 02             	shl    $0x2,%eax
801004d6:	01 d0                	add    %edx,%eax
801004d8:	01 c0                	add    %eax,%eax
801004da:	8b 55 d4             	mov    -0x2c(%ebp),%edx
801004dd:	83 e2 0f             	and    $0xf,%edx
801004e0:	01 d0                	add    %edx,%eax
801004e2:	89 45 d4             	mov    %eax,-0x2c(%ebp)
    CONV(minute);
801004e5:	8b 45 d8             	mov    -0x28(%ebp),%eax
801004e8:	89 c2                	mov    %eax,%edx
801004ea:	c1 ea 04             	shr    $0x4,%edx
801004ed:	89 d0                	mov    %edx,%eax
801004ef:	c1 e0 02             	shl    $0x2,%eax
801004f2:	01 d0                	add    %edx,%eax
801004f4:	01 c0                	add    %eax,%eax
801004f6:	8b 55 d8             	mov    -0x28(%ebp),%edx
801004f9:	83 e2 0f             	and    $0xf,%edx
801004fc:	01 d0                	add    %edx,%eax
801004fe:	89 45 d8             	mov    %eax,-0x28(%ebp)
    CONV(hour  );
80100501:	8b 45 dc             	mov    -0x24(%ebp),%eax
80100504:	89 c2                	mov    %eax,%edx
80100506:	c1 ea 04             	shr    $0x4,%edx
80100509:	89 d0                	mov    %edx,%eax
8010050b:	c1 e0 02             	shl    $0x2,%eax
8010050e:	01 d0                	add    %edx,%eax
80100510:	01 c0                	add    %eax,%eax
80100512:	8b 55 dc             	mov    -0x24(%ebp),%edx
80100515:	83 e2 0f             	and    $0xf,%edx
80100518:	01 d0                	add    %edx,%eax
8010051a:	89 45 dc             	mov    %eax,-0x24(%ebp)
    CONV(day   );
8010051d:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100520:	89 c2                	mov    %eax,%edx
80100522:	c1 ea 04             	shr    $0x4,%edx
80100525:	89 d0                	mov    %edx,%eax
80100527:	c1 e0 02             	shl    $0x2,%eax
8010052a:	01 d0                	add    %edx,%eax
8010052c:	01 c0                	add    %eax,%eax
8010052e:	8b 55 e0             	mov    -0x20(%ebp),%edx
80100531:	83 e2 0f             	and    $0xf,%edx
80100534:	01 d0                	add    %edx,%eax
80100536:	89 45 e0             	mov    %eax,-0x20(%ebp)
    CONV(month );
80100539:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010053c:	89 c2                	mov    %eax,%edx
8010053e:	c1 ea 04             	shr    $0x4,%edx
80100541:	89 d0                	mov    %edx,%eax
80100543:	c1 e0 02             	shl    $0x2,%eax
80100546:	01 d0                	add    %edx,%eax
80100548:	01 c0                	add    %eax,%eax
8010054a:	8b 55 e4             	mov    -0x1c(%ebp),%edx
8010054d:	83 e2 0f             	and    $0xf,%edx
80100550:	01 d0                	add    %edx,%eax
80100552:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    CONV(year  );
80100555:	8b 45 e8             	mov    -0x18(%ebp),%eax
80100558:	89 c2                	mov    %eax,%edx
8010055a:	c1 ea 04             	shr    $0x4,%edx
8010055d:	89 d0                	mov    %edx,%eax
8010055f:	c1 e0 02             	shl    $0x2,%eax
80100562:	01 d0                	add    %edx,%eax
80100564:	01 c0                	add    %eax,%eax
80100566:	8b 55 e8             	mov    -0x18(%ebp),%edx
80100569:	83 e2 0f             	and    $0xf,%edx
8010056c:	01 d0                	add    %edx,%eax
8010056e:	89 45 e8             	mov    %eax,-0x18(%ebp)
#undef     CONV
  }

  // convert 12 hour format to 24 hour format
  if(!tf){
80100571:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80100575:	75 2e                	jne    801005a5 <cmostime+0x191>
    if(t2.hour & CMOS_PM_BIT){
80100577:	8b 45 c4             	mov    -0x3c(%ebp),%eax
8010057a:	25 80 00 00 00       	and    $0x80,%eax
8010057f:	85 c0                	test   %eax,%eax
80100581:	74 22                	je     801005a5 <cmostime+0x191>
      t1.hour = (t1.hour + 12) % 24;
80100583:	8b 45 dc             	mov    -0x24(%ebp),%eax
80100586:	8d 48 0c             	lea    0xc(%eax),%ecx
80100589:	ba ab aa aa aa       	mov    $0xaaaaaaab,%edx
8010058e:	89 c8                	mov    %ecx,%eax
80100590:	f7 e2                	mul    %edx
80100592:	c1 ea 04             	shr    $0x4,%edx
80100595:	89 d0                	mov    %edx,%eax
80100597:	01 c0                	add    %eax,%eax
80100599:	01 d0                	add    %edx,%eax
8010059b:	c1 e0 03             	shl    $0x3,%eax
8010059e:	89 ca                	mov    %ecx,%edx
801005a0:	29 c2                	sub    %eax,%edx
801005a2:	89 55 dc             	mov    %edx,-0x24(%ebp)
    }
  }

  *r = t1;
801005a5:	8b 45 08             	mov    0x8(%ebp),%eax
801005a8:	8b 55 d4             	mov    -0x2c(%ebp),%edx
801005ab:	89 10                	mov    %edx,(%eax)
801005ad:	8b 55 d8             	mov    -0x28(%ebp),%edx
801005b0:	89 50 04             	mov    %edx,0x4(%eax)
801005b3:	8b 55 dc             	mov    -0x24(%ebp),%edx
801005b6:	89 50 08             	mov    %edx,0x8(%eax)
801005b9:	8b 55 e0             	mov    -0x20(%ebp),%edx
801005bc:	89 50 0c             	mov    %edx,0xc(%eax)
801005bf:	8b 55 e4             	mov    -0x1c(%ebp),%edx
801005c2:	89 50 10             	mov    %edx,0x10(%eax)
801005c5:	8b 55 e8             	mov    -0x18(%ebp),%edx
801005c8:	89 50 14             	mov    %edx,0x14(%eax)
  r->year += 2000;
801005cb:	8b 45 08             	mov    0x8(%ebp),%eax
801005ce:	8b 40 14             	mov    0x14(%eax),%eax
801005d1:	8d 90 d0 07 00 00    	lea    0x7d0(%eax),%edx
801005d7:	8b 45 08             	mov    0x8(%ebp),%eax
801005da:	89 50 14             	mov    %edx,0x14(%eax)
}
801005dd:	c9                   	leave  
801005de:	c3                   	ret    
	...

801005e0 <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
801005e0:	55                   	push   %ebp
801005e1:	89 e5                	mov    %esp,%ebp
801005e3:	83 ec 14             	sub    $0x14,%esp
801005e6:	8b 45 08             	mov    0x8(%ebp),%eax
801005e9:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801005ed:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
801005f1:	89 c2                	mov    %eax,%edx
801005f3:	ec                   	in     (%dx),%al
801005f4:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
801005f7:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
801005fb:	c9                   	leave  
801005fc:	c3                   	ret    

801005fd <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
801005fd:	55                   	push   %ebp
801005fe:	89 e5                	mov    %esp,%ebp
80100600:	83 ec 08             	sub    $0x8,%esp
80100603:	8b 55 08             	mov    0x8(%ebp),%edx
80100606:	8b 45 0c             	mov    0xc(%ebp),%eax
80100609:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
8010060d:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100610:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80100614:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80100618:	ee                   	out    %al,(%dx)
}
80100619:	c9                   	leave  
8010061a:	c3                   	ret    

8010061b <cli>:
  asm volatile("movw %0, %%gs" : : "r" (v));
}

static inline void
cli(void)
{
8010061b:	55                   	push   %ebp
8010061c:	89 e5                	mov    %esp,%ebp
  asm volatile("cli");
8010061e:	fa                   	cli    
}
8010061f:	5d                   	pop    %ebp
80100620:	c3                   	ret    

80100621 <printint>:
  int locking;
} cons;

static void
printint(int xx, int base, int sign)
{
80100621:	55                   	push   %ebp
80100622:	89 e5                	mov    %esp,%ebp
80100624:	53                   	push   %ebx
80100625:	83 ec 44             	sub    $0x44,%esp
  static char digits[] = "0123456789abcdef";
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = (xx < 0)))
80100628:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
8010062c:	74 19                	je     80100647 <printint+0x26>
8010062e:	8b 45 08             	mov    0x8(%ebp),%eax
80100631:	c1 e8 1f             	shr    $0x1f,%eax
80100634:	89 45 10             	mov    %eax,0x10(%ebp)
80100637:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
8010063b:	74 0a                	je     80100647 <printint+0x26>
    x = -xx;
8010063d:	8b 45 08             	mov    0x8(%ebp),%eax
80100640:	f7 d8                	neg    %eax
80100642:	89 45 f4             	mov    %eax,-0xc(%ebp)
  static char digits[] = "0123456789abcdef";
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = (xx < 0)))
80100645:	eb 06                	jmp    8010064d <printint+0x2c>
    x = -xx;
  else
    x = xx;
80100647:	8b 45 08             	mov    0x8(%ebp),%eax
8010064a:	89 45 f4             	mov    %eax,-0xc(%ebp)

  i = 0;
8010064d:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  do{
    buf[i++] = digits[x % base];
80100654:	8b 4d f0             	mov    -0x10(%ebp),%ecx
80100657:	8b 5d 0c             	mov    0xc(%ebp),%ebx
8010065a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010065d:	ba 00 00 00 00       	mov    $0x0,%edx
80100662:	f7 f3                	div    %ebx
80100664:	89 d0                	mov    %edx,%eax
80100666:	0f b6 80 04 a0 10 80 	movzbl -0x7fef5ffc(%eax),%eax
8010066d:	88 44 0d e0          	mov    %al,-0x20(%ebp,%ecx,1)
80100671:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
  }while((x /= base) != 0);
80100675:	8b 45 0c             	mov    0xc(%ebp),%eax
80100678:	89 45 d4             	mov    %eax,-0x2c(%ebp)
8010067b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010067e:	ba 00 00 00 00       	mov    $0x0,%edx
80100683:	f7 75 d4             	divl   -0x2c(%ebp)
80100686:	89 45 f4             	mov    %eax,-0xc(%ebp)
80100689:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010068d:	75 c5                	jne    80100654 <printint+0x33>

  if(sign)
8010068f:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80100693:	74 23                	je     801006b8 <printint+0x97>
    buf[i++] = '-';
80100695:	8b 45 f0             	mov    -0x10(%ebp),%eax
80100698:	c6 44 05 e0 2d       	movb   $0x2d,-0x20(%ebp,%eax,1)
8010069d:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)

  while(--i >= 0)
801006a1:	eb 16                	jmp    801006b9 <printint+0x98>
    consputc(buf[i]);
801006a3:	8b 45 f0             	mov    -0x10(%ebp),%eax
801006a6:	0f b6 44 05 e0       	movzbl -0x20(%ebp,%eax,1),%eax
801006ab:	0f be c0             	movsbl %al,%eax
801006ae:	89 04 24             	mov    %eax,(%esp)
801006b1:	e8 ea 03 00 00       	call   80100aa0 <consputc>
801006b6:	eb 01                	jmp    801006b9 <printint+0x98>
  }while((x /= base) != 0);

  if(sign)
    buf[i++] = '-';

  while(--i >= 0)
801006b8:	90                   	nop
801006b9:	83 6d f0 01          	subl   $0x1,-0x10(%ebp)
801006bd:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801006c1:	79 e0                	jns    801006a3 <printint+0x82>
    consputc(buf[i]);
}
801006c3:	83 c4 44             	add    $0x44,%esp
801006c6:	5b                   	pop    %ebx
801006c7:	5d                   	pop    %ebp
801006c8:	c3                   	ret    

801006c9 <cprintf>:
//PAGEBREAK: 50

// Print to the console. only understands %d, %x, %p, %s.
void
cprintf(char *fmt, ...)
{
801006c9:	55                   	push   %ebp
801006ca:	89 e5                	mov    %esp,%ebp
801006cc:	83 ec 38             	sub    $0x38,%esp
  int i, c, locking;
  uint *argp;
  char *s;

  locking = cons.locking;
801006cf:	a1 34 c6 10 80       	mov    0x8010c634,%eax
801006d4:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if(locking)
801006d7:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
801006db:	74 0c                	je     801006e9 <cprintf+0x20>
    acquire(&cons.lock);
801006dd:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
801006e4:	e8 5e 50 00 00       	call   80105747 <acquire>

  if (fmt == 0)
801006e9:	8b 45 08             	mov    0x8(%ebp),%eax
801006ec:	85 c0                	test   %eax,%eax
801006ee:	75 0c                	jne    801006fc <cprintf+0x33>
    panic("null fmt");
801006f0:	c7 04 24 b5 8e 10 80 	movl   $0x80108eb5,(%esp)
801006f7:	e8 6d 01 00 00       	call   80100869 <panic>

  argp = (uint*)(void*)(&fmt + 1);
801006fc:	8d 45 08             	lea    0x8(%ebp),%eax
801006ff:	83 c0 04             	add    $0x4,%eax
80100702:	89 45 f0             	mov    %eax,-0x10(%ebp)
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100705:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
8010070c:	e9 20 01 00 00       	jmp    80100831 <cprintf+0x168>
    if(c != '%'){
80100711:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
80100715:	74 10                	je     80100727 <cprintf+0x5e>
      consputc(c);
80100717:	8b 45 e8             	mov    -0x18(%ebp),%eax
8010071a:	89 04 24             	mov    %eax,(%esp)
8010071d:	e8 7e 03 00 00       	call   80100aa0 <consputc>
      continue;
80100722:	e9 06 01 00 00       	jmp    8010082d <cprintf+0x164>
    }
    c = fmt[++i] & 0xff;
80100727:	8b 55 08             	mov    0x8(%ebp),%edx
8010072a:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
8010072e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100731:	8d 04 02             	lea    (%edx,%eax,1),%eax
80100734:	0f b6 00             	movzbl (%eax),%eax
80100737:	0f be c0             	movsbl %al,%eax
8010073a:	25 ff 00 00 00       	and    $0xff,%eax
8010073f:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(c == 0)
80100742:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
80100746:	0f 84 08 01 00 00    	je     80100854 <cprintf+0x18b>
      break;
    switch(c){
8010074c:	8b 45 e8             	mov    -0x18(%ebp),%eax
8010074f:	83 f8 70             	cmp    $0x70,%eax
80100752:	74 4d                	je     801007a1 <cprintf+0xd8>
80100754:	83 f8 70             	cmp    $0x70,%eax
80100757:	7f 13                	jg     8010076c <cprintf+0xa3>
80100759:	83 f8 25             	cmp    $0x25,%eax
8010075c:	0f 84 a6 00 00 00    	je     80100808 <cprintf+0x13f>
80100762:	83 f8 64             	cmp    $0x64,%eax
80100765:	74 14                	je     8010077b <cprintf+0xb2>
80100767:	e9 aa 00 00 00       	jmp    80100816 <cprintf+0x14d>
8010076c:	83 f8 73             	cmp    $0x73,%eax
8010076f:	74 53                	je     801007c4 <cprintf+0xfb>
80100771:	83 f8 78             	cmp    $0x78,%eax
80100774:	74 2b                	je     801007a1 <cprintf+0xd8>
80100776:	e9 9b 00 00 00       	jmp    80100816 <cprintf+0x14d>
    case 'd':
      printint(*argp++, 10, 1);
8010077b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010077e:	8b 00                	mov    (%eax),%eax
80100780:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
80100784:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
8010078b:	00 
8010078c:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
80100793:	00 
80100794:	89 04 24             	mov    %eax,(%esp)
80100797:	e8 85 fe ff ff       	call   80100621 <printint>
      break;
8010079c:	e9 8c 00 00 00       	jmp    8010082d <cprintf+0x164>
    case 'x':
    case 'p':
      printint(*argp++, 16, 0);
801007a1:	8b 45 f0             	mov    -0x10(%ebp),%eax
801007a4:	8b 00                	mov    (%eax),%eax
801007a6:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
801007aa:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
801007b1:	00 
801007b2:	c7 44 24 04 10 00 00 	movl   $0x10,0x4(%esp)
801007b9:	00 
801007ba:	89 04 24             	mov    %eax,(%esp)
801007bd:	e8 5f fe ff ff       	call   80100621 <printint>
      break;
801007c2:	eb 69                	jmp    8010082d <cprintf+0x164>
    case 's':
      if((s = (char*)*argp++) == 0)
801007c4:	8b 45 f0             	mov    -0x10(%ebp),%eax
801007c7:	8b 00                	mov    (%eax),%eax
801007c9:	89 45 f4             	mov    %eax,-0xc(%ebp)
801007cc:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801007d0:	0f 94 c0             	sete   %al
801007d3:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
801007d7:	84 c0                	test   %al,%al
801007d9:	74 20                	je     801007fb <cprintf+0x132>
        s = "(null)";
801007db:	c7 45 f4 be 8e 10 80 	movl   $0x80108ebe,-0xc(%ebp)
      for(; *s; s++)
801007e2:	eb 18                	jmp    801007fc <cprintf+0x133>
        consputc(*s);
801007e4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801007e7:	0f b6 00             	movzbl (%eax),%eax
801007ea:	0f be c0             	movsbl %al,%eax
801007ed:	89 04 24             	mov    %eax,(%esp)
801007f0:	e8 ab 02 00 00       	call   80100aa0 <consputc>
      printint(*argp++, 16, 0);
      break;
    case 's':
      if((s = (char*)*argp++) == 0)
        s = "(null)";
      for(; *s; s++)
801007f5:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801007f9:	eb 01                	jmp    801007fc <cprintf+0x133>
801007fb:	90                   	nop
801007fc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801007ff:	0f b6 00             	movzbl (%eax),%eax
80100802:	84 c0                	test   %al,%al
80100804:	75 de                	jne    801007e4 <cprintf+0x11b>
        consputc(*s);
      break;
80100806:	eb 25                	jmp    8010082d <cprintf+0x164>
    case '%':
      consputc('%');
80100808:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
8010080f:	e8 8c 02 00 00       	call   80100aa0 <consputc>
      break;
80100814:	eb 17                	jmp    8010082d <cprintf+0x164>
    default:
      // Print unknown % sequence to draw attention.
      consputc('%');
80100816:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
8010081d:	e8 7e 02 00 00       	call   80100aa0 <consputc>
      consputc(c);
80100822:	8b 45 e8             	mov    -0x18(%ebp),%eax
80100825:	89 04 24             	mov    %eax,(%esp)
80100828:	e8 73 02 00 00       	call   80100aa0 <consputc>

  if (fmt == 0)
    panic("null fmt");

  argp = (uint*)(void*)(&fmt + 1);
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
8010082d:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
80100831:	8b 55 08             	mov    0x8(%ebp),%edx
80100834:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100837:	8d 04 02             	lea    (%edx,%eax,1),%eax
8010083a:	0f b6 00             	movzbl (%eax),%eax
8010083d:	0f be c0             	movsbl %al,%eax
80100840:	25 ff 00 00 00       	and    $0xff,%eax
80100845:	89 45 e8             	mov    %eax,-0x18(%ebp)
80100848:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
8010084c:	0f 85 bf fe ff ff    	jne    80100711 <cprintf+0x48>
80100852:	eb 01                	jmp    80100855 <cprintf+0x18c>
      consputc(c);
      continue;
    }
    c = fmt[++i] & 0xff;
    if(c == 0)
      break;
80100854:	90                   	nop
      consputc(c);
      break;
    }
  }

  if(locking)
80100855:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80100859:	74 0c                	je     80100867 <cprintf+0x19e>
    release(&cons.lock);
8010085b:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80100862:	e8 49 4f 00 00       	call   801057b0 <release>
}
80100867:	c9                   	leave  
80100868:	c3                   	ret    

80100869 <panic>:

void
panic(char *s)
{
80100869:	55                   	push   %ebp
8010086a:	89 e5                	mov    %esp,%ebp
8010086c:	83 ec 48             	sub    $0x48,%esp
  int i;
  uint pcs[10];
  
  cli();
8010086f:	e8 a7 fd ff ff       	call   8010061b <cli>
  cons.locking = 0;
80100874:	c7 05 34 c6 10 80 00 	movl   $0x0,0x8010c634
8010087b:	00 00 00 
  cprintf("cpu%d: panic: ", cpu->id);
8010087e:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80100884:	0f b6 00             	movzbl (%eax),%eax
80100887:	0f b6 c0             	movzbl %al,%eax
8010088a:	89 44 24 04          	mov    %eax,0x4(%esp)
8010088e:	c7 04 24 c5 8e 10 80 	movl   $0x80108ec5,(%esp)
80100895:	e8 2f fe ff ff       	call   801006c9 <cprintf>
  cprintf(s);
8010089a:	8b 45 08             	mov    0x8(%ebp),%eax
8010089d:	89 04 24             	mov    %eax,(%esp)
801008a0:	e8 24 fe ff ff       	call   801006c9 <cprintf>
  cprintf("\n");
801008a5:	c7 04 24 d4 8e 10 80 	movl   $0x80108ed4,(%esp)
801008ac:	e8 18 fe ff ff       	call   801006c9 <cprintf>
  getcallerpcs(&s, NELEM(pcs), pcs);
801008b1:	8d 45 cc             	lea    -0x34(%ebp),%eax
801008b4:	89 44 24 08          	mov    %eax,0x8(%esp)
801008b8:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
801008bf:	00 
801008c0:	8d 45 08             	lea    0x8(%ebp),%eax
801008c3:	89 04 24             	mov    %eax,(%esp)
801008c6:	e8 34 4f 00 00       	call   801057ff <getcallerpcs>
  for(i=0; i<10; i++)
801008cb:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
801008d2:	eb 1b                	jmp    801008ef <panic+0x86>
    cprintf(" %p", pcs[i]);
801008d4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801008d7:	8b 44 85 cc          	mov    -0x34(%ebp,%eax,4),%eax
801008db:	89 44 24 04          	mov    %eax,0x4(%esp)
801008df:	c7 04 24 d6 8e 10 80 	movl   $0x80108ed6,(%esp)
801008e6:	e8 de fd ff ff       	call   801006c9 <cprintf>
  cons.locking = 0;
  cprintf("cpu%d: panic: ", cpu->id);
  cprintf(s);
  cprintf("\n");
  getcallerpcs(&s, NELEM(pcs), pcs);
  for(i=0; i<10; i++)
801008eb:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801008ef:	83 7d f4 09          	cmpl   $0x9,-0xc(%ebp)
801008f3:	7e df                	jle    801008d4 <panic+0x6b>
    cprintf(" %p", pcs[i]);
  panicked = 1; // freeze other CPU
801008f5:	c7 05 e0 c5 10 80 01 	movl   $0x1,0x8010c5e0
801008fc:	00 00 00 
  for(;;)
    ;
801008ff:	eb fe                	jmp    801008ff <panic+0x96>

80100901 <cgaputc>:
#define CRTPORT 0x3d4
static ushort *crt = (ushort*)P2V(0xb8000);  // CGA memory

static void
cgaputc(int c)
{
80100901:	55                   	push   %ebp
80100902:	89 e5                	mov    %esp,%ebp
80100904:	83 ec 28             	sub    $0x28,%esp
  int pos;
  
  // Cursor position: col + 80*row.
  outb(CRTPORT, 14);
80100907:	c7 44 24 04 0e 00 00 	movl   $0xe,0x4(%esp)
8010090e:	00 
8010090f:	c7 04 24 d4 03 00 00 	movl   $0x3d4,(%esp)
80100916:	e8 e2 fc ff ff       	call   801005fd <outb>
  pos = inb(CRTPORT+1) << 8;
8010091b:	c7 04 24 d5 03 00 00 	movl   $0x3d5,(%esp)
80100922:	e8 b9 fc ff ff       	call   801005e0 <inb>
80100927:	0f b6 c0             	movzbl %al,%eax
8010092a:	c1 e0 08             	shl    $0x8,%eax
8010092d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  outb(CRTPORT, 15);
80100930:	c7 44 24 04 0f 00 00 	movl   $0xf,0x4(%esp)
80100937:	00 
80100938:	c7 04 24 d4 03 00 00 	movl   $0x3d4,(%esp)
8010093f:	e8 b9 fc ff ff       	call   801005fd <outb>
  pos |= inb(CRTPORT+1);
80100944:	c7 04 24 d5 03 00 00 	movl   $0x3d5,(%esp)
8010094b:	e8 90 fc ff ff       	call   801005e0 <inb>
80100950:	0f b6 c0             	movzbl %al,%eax
80100953:	09 45 f4             	or     %eax,-0xc(%ebp)

  if(c == '\n')
80100956:	83 7d 08 0a          	cmpl   $0xa,0x8(%ebp)
8010095a:	75 30                	jne    8010098c <cgaputc+0x8b>
    pos += 80 - pos%80;
8010095c:	8b 4d f4             	mov    -0xc(%ebp),%ecx
8010095f:	ba 67 66 66 66       	mov    $0x66666667,%edx
80100964:	89 c8                	mov    %ecx,%eax
80100966:	f7 ea                	imul   %edx
80100968:	c1 fa 05             	sar    $0x5,%edx
8010096b:	89 c8                	mov    %ecx,%eax
8010096d:	c1 f8 1f             	sar    $0x1f,%eax
80100970:	29 c2                	sub    %eax,%edx
80100972:	89 d0                	mov    %edx,%eax
80100974:	c1 e0 02             	shl    $0x2,%eax
80100977:	01 d0                	add    %edx,%eax
80100979:	c1 e0 04             	shl    $0x4,%eax
8010097c:	89 ca                	mov    %ecx,%edx
8010097e:	29 c2                	sub    %eax,%edx
80100980:	b8 50 00 00 00       	mov    $0x50,%eax
80100985:	29 d0                	sub    %edx,%eax
80100987:	01 45 f4             	add    %eax,-0xc(%ebp)
8010098a:	eb 33                	jmp    801009bf <cgaputc+0xbe>
  else if(c == BACKSPACE){
8010098c:	81 7d 08 00 01 00 00 	cmpl   $0x100,0x8(%ebp)
80100993:	75 0c                	jne    801009a1 <cgaputc+0xa0>
    if(pos > 0) --pos;
80100995:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80100999:	7e 24                	jle    801009bf <cgaputc+0xbe>
8010099b:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
8010099f:	eb 1e                	jmp    801009bf <cgaputc+0xbe>
  } else
    crt[pos++] = (c&0xff) | 0x0700;  // black on white
801009a1:	a1 00 a0 10 80       	mov    0x8010a000,%eax
801009a6:	8b 55 f4             	mov    -0xc(%ebp),%edx
801009a9:	01 d2                	add    %edx,%edx
801009ab:	8d 14 10             	lea    (%eax,%edx,1),%edx
801009ae:	8b 45 08             	mov    0x8(%ebp),%eax
801009b1:	66 25 ff 00          	and    $0xff,%ax
801009b5:	80 cc 07             	or     $0x7,%ah
801009b8:	66 89 02             	mov    %ax,(%edx)
801009bb:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)

  if(pos < 0 || pos > 25*80)
801009bf:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801009c3:	78 09                	js     801009ce <cgaputc+0xcd>
801009c5:	81 7d f4 d0 07 00 00 	cmpl   $0x7d0,-0xc(%ebp)
801009cc:	7e 0c                	jle    801009da <cgaputc+0xd9>
    panic("pos under/overflow");
801009ce:	c7 04 24 da 8e 10 80 	movl   $0x80108eda,(%esp)
801009d5:	e8 8f fe ff ff       	call   80100869 <panic>
  
  if((pos/80) >= 24){  // Scroll up.
801009da:	81 7d f4 7f 07 00 00 	cmpl   $0x77f,-0xc(%ebp)
801009e1:	7e 53                	jle    80100a36 <cgaputc+0x135>
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
801009e3:	a1 00 a0 10 80       	mov    0x8010a000,%eax
801009e8:	8d 90 a0 00 00 00    	lea    0xa0(%eax),%edx
801009ee:	a1 00 a0 10 80       	mov    0x8010a000,%eax
801009f3:	c7 44 24 08 60 0e 00 	movl   $0xe60,0x8(%esp)
801009fa:	00 
801009fb:	89 54 24 04          	mov    %edx,0x4(%esp)
801009ff:	89 04 24             	mov    %eax,(%esp)
80100a02:	e8 76 50 00 00       	call   80105a7d <memmove>
    pos -= 80;
80100a07:	83 6d f4 50          	subl   $0x50,-0xc(%ebp)
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
80100a0b:	b8 80 07 00 00       	mov    $0x780,%eax
80100a10:	2b 45 f4             	sub    -0xc(%ebp),%eax
80100a13:	8d 14 00             	lea    (%eax,%eax,1),%edx
80100a16:	a1 00 a0 10 80       	mov    0x8010a000,%eax
80100a1b:	8b 4d f4             	mov    -0xc(%ebp),%ecx
80100a1e:	01 c9                	add    %ecx,%ecx
80100a20:	01 c8                	add    %ecx,%eax
80100a22:	89 54 24 08          	mov    %edx,0x8(%esp)
80100a26:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80100a2d:	00 
80100a2e:	89 04 24             	mov    %eax,(%esp)
80100a31:	e8 74 4f 00 00       	call   801059aa <memset>
  }
  
  outb(CRTPORT, 14);
80100a36:	c7 44 24 04 0e 00 00 	movl   $0xe,0x4(%esp)
80100a3d:	00 
80100a3e:	c7 04 24 d4 03 00 00 	movl   $0x3d4,(%esp)
80100a45:	e8 b3 fb ff ff       	call   801005fd <outb>
  outb(CRTPORT+1, pos>>8);
80100a4a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100a4d:	c1 f8 08             	sar    $0x8,%eax
80100a50:	0f b6 c0             	movzbl %al,%eax
80100a53:	89 44 24 04          	mov    %eax,0x4(%esp)
80100a57:	c7 04 24 d5 03 00 00 	movl   $0x3d5,(%esp)
80100a5e:	e8 9a fb ff ff       	call   801005fd <outb>
  outb(CRTPORT, 15);
80100a63:	c7 44 24 04 0f 00 00 	movl   $0xf,0x4(%esp)
80100a6a:	00 
80100a6b:	c7 04 24 d4 03 00 00 	movl   $0x3d4,(%esp)
80100a72:	e8 86 fb ff ff       	call   801005fd <outb>
  outb(CRTPORT+1, pos);
80100a77:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100a7a:	0f b6 c0             	movzbl %al,%eax
80100a7d:	89 44 24 04          	mov    %eax,0x4(%esp)
80100a81:	c7 04 24 d5 03 00 00 	movl   $0x3d5,(%esp)
80100a88:	e8 70 fb ff ff       	call   801005fd <outb>
  crt[pos] = ' ' | 0x0700;
80100a8d:	a1 00 a0 10 80       	mov    0x8010a000,%eax
80100a92:	8b 55 f4             	mov    -0xc(%ebp),%edx
80100a95:	01 d2                	add    %edx,%edx
80100a97:	01 d0                	add    %edx,%eax
80100a99:	66 c7 00 20 07       	movw   $0x720,(%eax)
}
80100a9e:	c9                   	leave  
80100a9f:	c3                   	ret    

80100aa0 <consputc>:

void
consputc(int c)
{
80100aa0:	55                   	push   %ebp
80100aa1:	89 e5                	mov    %esp,%ebp
80100aa3:	83 ec 18             	sub    $0x18,%esp
  if(panicked){
80100aa6:	a1 e0 c5 10 80       	mov    0x8010c5e0,%eax
80100aab:	85 c0                	test   %eax,%eax
80100aad:	74 07                	je     80100ab6 <consputc+0x16>
    cli();
80100aaf:	e8 67 fb ff ff       	call   8010061b <cli>
    for(;;)
      ;
80100ab4:	eb fe                	jmp    80100ab4 <consputc+0x14>
  }

  if(c == BACKSPACE){
80100ab6:	81 7d 08 00 01 00 00 	cmpl   $0x100,0x8(%ebp)
80100abd:	75 26                	jne    80100ae5 <consputc+0x45>
    uartputc('\b'); uartputc(' '); uartputc('\b');
80100abf:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
80100ac6:	e8 01 6a 00 00       	call   801074cc <uartputc>
80100acb:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
80100ad2:	e8 f5 69 00 00       	call   801074cc <uartputc>
80100ad7:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
80100ade:	e8 e9 69 00 00       	call   801074cc <uartputc>
80100ae3:	eb 0b                	jmp    80100af0 <consputc+0x50>
  } else
    uartputc(c);
80100ae5:	8b 45 08             	mov    0x8(%ebp),%eax
80100ae8:	89 04 24             	mov    %eax,(%esp)
80100aeb:	e8 dc 69 00 00       	call   801074cc <uartputc>
  cgaputc(c);
80100af0:	8b 45 08             	mov    0x8(%ebp),%eax
80100af3:	89 04 24             	mov    %eax,(%esp)
80100af6:	e8 06 fe ff ff       	call   80100901 <cgaputc>
}
80100afb:	c9                   	leave  
80100afc:	c3                   	ret    

80100afd <consoleintr>:

#define C(x)  ((x)-'@')  // Control-x

void
consoleintr(int (*getc)(void))
{
80100afd:	55                   	push   %ebp
80100afe:	89 e5                	mov    %esp,%ebp
80100b00:	83 ec 28             	sub    $0x28,%esp
  int c, doprocdump = 0;
80100b03:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

  acquire(&cons.lock);
80100b0a:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80100b11:	e8 31 4c 00 00       	call   80105747 <acquire>
  while((c = getc()) >= 0){
80100b16:	e9 40 01 00 00       	jmp    80100c5b <consoleintr+0x15e>
    switch(c){
80100b1b:	8b 45 f0             	mov    -0x10(%ebp),%eax
80100b1e:	83 f8 10             	cmp    $0x10,%eax
80100b21:	74 1e                	je     80100b41 <consoleintr+0x44>
80100b23:	83 f8 10             	cmp    $0x10,%eax
80100b26:	7f 0a                	jg     80100b32 <consoleintr+0x35>
80100b28:	83 f8 08             	cmp    $0x8,%eax
80100b2b:	74 6a                	je     80100b97 <consoleintr+0x9a>
80100b2d:	e9 96 00 00 00       	jmp    80100bc8 <consoleintr+0xcb>
80100b32:	83 f8 15             	cmp    $0x15,%eax
80100b35:	74 31                	je     80100b68 <consoleintr+0x6b>
80100b37:	83 f8 7f             	cmp    $0x7f,%eax
80100b3a:	74 5b                	je     80100b97 <consoleintr+0x9a>
80100b3c:	e9 87 00 00 00       	jmp    80100bc8 <consoleintr+0xcb>
    case C('P'):  // Process listing.
      doprocdump = 1;   // procdump() locks cons.lock indirectly; invoke later
80100b41:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
      break;
80100b48:	e9 0e 01 00 00       	jmp    80100c5b <consoleintr+0x15e>
    case C('U'):  // Kill line.
      while(input.e != input.w &&
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
        input.e--;
80100b4d:	a1 88 20 11 80       	mov    0x80112088,%eax
80100b52:	83 e8 01             	sub    $0x1,%eax
80100b55:	a3 88 20 11 80       	mov    %eax,0x80112088
        consputc(BACKSPACE);
80100b5a:	c7 04 24 00 01 00 00 	movl   $0x100,(%esp)
80100b61:	e8 3a ff ff ff       	call   80100aa0 <consputc>
80100b66:	eb 01                	jmp    80100b69 <consoleintr+0x6c>
    switch(c){
    case C('P'):  // Process listing.
      doprocdump = 1;   // procdump() locks cons.lock indirectly; invoke later
      break;
    case C('U'):  // Kill line.
      while(input.e != input.w &&
80100b68:	90                   	nop
80100b69:	8b 15 88 20 11 80    	mov    0x80112088,%edx
80100b6f:	a1 84 20 11 80       	mov    0x80112084,%eax
80100b74:	39 c2                	cmp    %eax,%edx
80100b76:	0f 84 db 00 00 00    	je     80100c57 <consoleintr+0x15a>
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
80100b7c:	a1 88 20 11 80       	mov    0x80112088,%eax
80100b81:	83 e8 01             	sub    $0x1,%eax
80100b84:	83 e0 7f             	and    $0x7f,%eax
80100b87:	0f b6 80 00 20 11 80 	movzbl -0x7feee000(%eax),%eax
    switch(c){
    case C('P'):  // Process listing.
      doprocdump = 1;   // procdump() locks cons.lock indirectly; invoke later
      break;
    case C('U'):  // Kill line.
      while(input.e != input.w &&
80100b8e:	3c 0a                	cmp    $0xa,%al
80100b90:	75 bb                	jne    80100b4d <consoleintr+0x50>
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
        input.e--;
        consputc(BACKSPACE);
      }
      break;
80100b92:	e9 c4 00 00 00       	jmp    80100c5b <consoleintr+0x15e>
    case C('H'): case '\x7f':  // Backspace
      if(input.e != input.w){
80100b97:	8b 15 88 20 11 80    	mov    0x80112088,%edx
80100b9d:	a1 84 20 11 80       	mov    0x80112084,%eax
80100ba2:	39 c2                	cmp    %eax,%edx
80100ba4:	0f 84 b0 00 00 00    	je     80100c5a <consoleintr+0x15d>
        input.e--;
80100baa:	a1 88 20 11 80       	mov    0x80112088,%eax
80100baf:	83 e8 01             	sub    $0x1,%eax
80100bb2:	a3 88 20 11 80       	mov    %eax,0x80112088
        consputc(BACKSPACE);
80100bb7:	c7 04 24 00 01 00 00 	movl   $0x100,(%esp)
80100bbe:	e8 dd fe ff ff       	call   80100aa0 <consputc>
      }
      break;
80100bc3:	e9 93 00 00 00       	jmp    80100c5b <consoleintr+0x15e>
    default:
      if(c != 0 && input.e-input.r < INPUT_BUF){
80100bc8:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80100bcc:	0f 84 89 00 00 00    	je     80100c5b <consoleintr+0x15e>
80100bd2:	8b 15 88 20 11 80    	mov    0x80112088,%edx
80100bd8:	a1 80 20 11 80       	mov    0x80112080,%eax
80100bdd:	89 d1                	mov    %edx,%ecx
80100bdf:	29 c1                	sub    %eax,%ecx
80100be1:	89 c8                	mov    %ecx,%eax
80100be3:	83 f8 7f             	cmp    $0x7f,%eax
80100be6:	77 73                	ja     80100c5b <consoleintr+0x15e>
        c = (c == '\r') ? '\n' : c;
80100be8:	83 7d f0 0d          	cmpl   $0xd,-0x10(%ebp)
80100bec:	74 05                	je     80100bf3 <consoleintr+0xf6>
80100bee:	8b 45 f0             	mov    -0x10(%ebp),%eax
80100bf1:	eb 05                	jmp    80100bf8 <consoleintr+0xfb>
80100bf3:	b8 0a 00 00 00       	mov    $0xa,%eax
80100bf8:	89 45 f0             	mov    %eax,-0x10(%ebp)
        input.buf[input.e++ % INPUT_BUF] = c;
80100bfb:	a1 88 20 11 80       	mov    0x80112088,%eax
80100c00:	89 c1                	mov    %eax,%ecx
80100c02:	83 e1 7f             	and    $0x7f,%ecx
80100c05:	8b 55 f0             	mov    -0x10(%ebp),%edx
80100c08:	88 91 00 20 11 80    	mov    %dl,-0x7feee000(%ecx)
80100c0e:	83 c0 01             	add    $0x1,%eax
80100c11:	a3 88 20 11 80       	mov    %eax,0x80112088
        consputc(c);
80100c16:	8b 45 f0             	mov    -0x10(%ebp),%eax
80100c19:	89 04 24             	mov    %eax,(%esp)
80100c1c:	e8 7f fe ff ff       	call   80100aa0 <consputc>
        if(c == '\n' || c == C('D') || input.e == input.r+INPUT_BUF){
80100c21:	83 7d f0 0a          	cmpl   $0xa,-0x10(%ebp)
80100c25:	74 18                	je     80100c3f <consoleintr+0x142>
80100c27:	83 7d f0 04          	cmpl   $0x4,-0x10(%ebp)
80100c2b:	74 12                	je     80100c3f <consoleintr+0x142>
80100c2d:	a1 88 20 11 80       	mov    0x80112088,%eax
80100c32:	8b 15 80 20 11 80    	mov    0x80112080,%edx
80100c38:	83 ea 80             	sub    $0xffffff80,%edx
80100c3b:	39 d0                	cmp    %edx,%eax
80100c3d:	75 1c                	jne    80100c5b <consoleintr+0x15e>
          input.w = input.e;
80100c3f:	a1 88 20 11 80       	mov    0x80112088,%eax
80100c44:	a3 84 20 11 80       	mov    %eax,0x80112084
          wakeup(&input.r);
80100c49:	c7 04 24 80 20 11 80 	movl   $0x80112080,(%esp)
80100c50:	e8 a6 48 00 00       	call   801054fb <wakeup>
80100c55:	eb 04                	jmp    80100c5b <consoleintr+0x15e>
      while(input.e != input.w &&
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
        input.e--;
        consputc(BACKSPACE);
      }
      break;
80100c57:	90                   	nop
80100c58:	eb 01                	jmp    80100c5b <consoleintr+0x15e>
    case C('H'): case '\x7f':  // Backspace
      if(input.e != input.w){
        input.e--;
        consputc(BACKSPACE);
      }
      break;
80100c5a:	90                   	nop
consoleintr(int (*getc)(void))
{
  int c, doprocdump = 0;

  acquire(&cons.lock);
  while((c = getc()) >= 0){
80100c5b:	8b 45 08             	mov    0x8(%ebp),%eax
80100c5e:	ff d0                	call   *%eax
80100c60:	89 45 f0             	mov    %eax,-0x10(%ebp)
80100c63:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80100c67:	0f 89 ae fe ff ff    	jns    80100b1b <consoleintr+0x1e>
        }
      }
      break;
    }
  }
  release(&cons.lock);
80100c6d:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80100c74:	e8 37 4b 00 00       	call   801057b0 <release>
  if(doprocdump) {
80100c79:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80100c7d:	74 05                	je     80100c84 <consoleintr+0x187>
    procdump();  // now call procdump() wo. cons.lock held
80100c7f:	e8 1e 49 00 00       	call   801055a2 <procdump>
  }
}
80100c84:	c9                   	leave  
80100c85:	c3                   	ret    

80100c86 <consoleread>:

int
consoleread(struct inode *ip, char *dst, int n)
{
80100c86:	55                   	push   %ebp
80100c87:	89 e5                	mov    %esp,%ebp
80100c89:	83 ec 28             	sub    $0x28,%esp
  uint target;
  int c;

  iunlock(ip);
80100c8c:	8b 45 08             	mov    0x8(%ebp),%eax
80100c8f:	89 04 24             	mov    %eax,(%esp)
80100c92:	e8 31 15 00 00       	call   801021c8 <iunlock>
  target = n;
80100c97:	8b 45 10             	mov    0x10(%ebp),%eax
80100c9a:	89 45 f0             	mov    %eax,-0x10(%ebp)
  acquire(&cons.lock);
80100c9d:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80100ca4:	e8 9e 4a 00 00       	call   80105747 <acquire>
  while(n > 0){
80100ca9:	e9 a8 00 00 00       	jmp    80100d56 <consoleread+0xd0>
    while(input.r == input.w){
      if(proc->killed){
80100cae:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100cb4:	8b 40 24             	mov    0x24(%eax),%eax
80100cb7:	85 c0                	test   %eax,%eax
80100cb9:	74 21                	je     80100cdc <consoleread+0x56>
        release(&cons.lock);
80100cbb:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80100cc2:	e8 e9 4a 00 00       	call   801057b0 <release>
        ilock(ip);
80100cc7:	8b 45 08             	mov    0x8(%ebp),%eax
80100cca:	89 04 24             	mov    %eax,(%esp)
80100ccd:	e8 9f 13 00 00       	call   80102071 <ilock>
        return -1;
80100cd2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100cd7:	e9 a9 00 00 00       	jmp    80100d85 <consoleread+0xff>
      }
      sleep(&input.r, &cons.lock);
80100cdc:	c7 44 24 04 00 c6 10 	movl   $0x8010c600,0x4(%esp)
80100ce3:	80 
80100ce4:	c7 04 24 80 20 11 80 	movl   $0x80112080,(%esp)
80100ceb:	e8 2e 47 00 00       	call   8010541e <sleep>
80100cf0:	eb 01                	jmp    80100cf3 <consoleread+0x6d>

  iunlock(ip);
  target = n;
  acquire(&cons.lock);
  while(n > 0){
    while(input.r == input.w){
80100cf2:	90                   	nop
80100cf3:	8b 15 80 20 11 80    	mov    0x80112080,%edx
80100cf9:	a1 84 20 11 80       	mov    0x80112084,%eax
80100cfe:	39 c2                	cmp    %eax,%edx
80100d00:	74 ac                	je     80100cae <consoleread+0x28>
        ilock(ip);
        return -1;
      }
      sleep(&input.r, &cons.lock);
    }
    c = input.buf[input.r++ % INPUT_BUF];
80100d02:	a1 80 20 11 80       	mov    0x80112080,%eax
80100d07:	89 c2                	mov    %eax,%edx
80100d09:	83 e2 7f             	and    $0x7f,%edx
80100d0c:	0f b6 92 00 20 11 80 	movzbl -0x7feee000(%edx),%edx
80100d13:	0f be d2             	movsbl %dl,%edx
80100d16:	89 55 f4             	mov    %edx,-0xc(%ebp)
80100d19:	83 c0 01             	add    $0x1,%eax
80100d1c:	a3 80 20 11 80       	mov    %eax,0x80112080
    if(c == C('D')){  // EOF
80100d21:	83 7d f4 04          	cmpl   $0x4,-0xc(%ebp)
80100d25:	75 17                	jne    80100d3e <consoleread+0xb8>
      if(n < target){
80100d27:	8b 45 10             	mov    0x10(%ebp),%eax
80100d2a:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80100d2d:	73 2f                	jae    80100d5e <consoleread+0xd8>
        // Save ^D for next time, to make sure
        // caller gets a 0-byte result.
        input.r--;
80100d2f:	a1 80 20 11 80       	mov    0x80112080,%eax
80100d34:	83 e8 01             	sub    $0x1,%eax
80100d37:	a3 80 20 11 80       	mov    %eax,0x80112080
      }
      break;
80100d3c:	eb 24                	jmp    80100d62 <consoleread+0xdc>
    }
    *dst++ = c;
80100d3e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100d41:	89 c2                	mov    %eax,%edx
80100d43:	8b 45 0c             	mov    0xc(%ebp),%eax
80100d46:	88 10                	mov    %dl,(%eax)
80100d48:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
    --n;
80100d4c:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
    if(c == '\n')
80100d50:	83 7d f4 0a          	cmpl   $0xa,-0xc(%ebp)
80100d54:	74 0b                	je     80100d61 <consoleread+0xdb>
  int c;

  iunlock(ip);
  target = n;
  acquire(&cons.lock);
  while(n > 0){
80100d56:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80100d5a:	7f 96                	jg     80100cf2 <consoleread+0x6c>
80100d5c:	eb 04                	jmp    80100d62 <consoleread+0xdc>
      if(n < target){
        // Save ^D for next time, to make sure
        // caller gets a 0-byte result.
        input.r--;
      }
      break;
80100d5e:	90                   	nop
80100d5f:	eb 01                	jmp    80100d62 <consoleread+0xdc>
    }
    *dst++ = c;
    --n;
    if(c == '\n')
      break;
80100d61:	90                   	nop
  }
  release(&cons.lock);
80100d62:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80100d69:	e8 42 4a 00 00       	call   801057b0 <release>
  ilock(ip);
80100d6e:	8b 45 08             	mov    0x8(%ebp),%eax
80100d71:	89 04 24             	mov    %eax,(%esp)
80100d74:	e8 f8 12 00 00       	call   80102071 <ilock>

  return target - n;
80100d79:	8b 45 10             	mov    0x10(%ebp),%eax
80100d7c:	8b 55 f0             	mov    -0x10(%ebp),%edx
80100d7f:	89 d1                	mov    %edx,%ecx
80100d81:	29 c1                	sub    %eax,%ecx
80100d83:	89 c8                	mov    %ecx,%eax
}
80100d85:	c9                   	leave  
80100d86:	c3                   	ret    

80100d87 <consolewrite>:

int
consolewrite(struct inode *ip, char *buf, int n)
{
80100d87:	55                   	push   %ebp
80100d88:	89 e5                	mov    %esp,%ebp
80100d8a:	83 ec 28             	sub    $0x28,%esp
  int i;

  iunlock(ip);
80100d8d:	8b 45 08             	mov    0x8(%ebp),%eax
80100d90:	89 04 24             	mov    %eax,(%esp)
80100d93:	e8 30 14 00 00       	call   801021c8 <iunlock>
  acquire(&cons.lock);
80100d98:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80100d9f:	e8 a3 49 00 00       	call   80105747 <acquire>
  for(i = 0; i < n; i++)
80100da4:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80100dab:	eb 1d                	jmp    80100dca <consolewrite+0x43>
    consputc(buf[i] & 0xff);
80100dad:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100db0:	03 45 0c             	add    0xc(%ebp),%eax
80100db3:	0f b6 00             	movzbl (%eax),%eax
80100db6:	0f be c0             	movsbl %al,%eax
80100db9:	25 ff 00 00 00       	and    $0xff,%eax
80100dbe:	89 04 24             	mov    %eax,(%esp)
80100dc1:	e8 da fc ff ff       	call   80100aa0 <consputc>
{
  int i;

  iunlock(ip);
  acquire(&cons.lock);
  for(i = 0; i < n; i++)
80100dc6:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80100dca:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100dcd:	3b 45 10             	cmp    0x10(%ebp),%eax
80100dd0:	7c db                	jl     80100dad <consolewrite+0x26>
    consputc(buf[i] & 0xff);
  release(&cons.lock);
80100dd2:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80100dd9:	e8 d2 49 00 00       	call   801057b0 <release>
  ilock(ip);
80100dde:	8b 45 08             	mov    0x8(%ebp),%eax
80100de1:	89 04 24             	mov    %eax,(%esp)
80100de4:	e8 88 12 00 00       	call   80102071 <ilock>

  return n;
80100de9:	8b 45 10             	mov    0x10(%ebp),%eax
}
80100dec:	c9                   	leave  
80100ded:	c3                   	ret    

80100dee <consoleinit>:

void
consoleinit(void)
{
80100dee:	55                   	push   %ebp
80100def:	89 e5                	mov    %esp,%ebp
80100df1:	83 ec 18             	sub    $0x18,%esp
  initlock(&cons.lock, "console");
80100df4:	c7 44 24 04 ed 8e 10 	movl   $0x80108eed,0x4(%esp)
80100dfb:	80 
80100dfc:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80100e03:	e8 1e 49 00 00       	call   80105726 <initlock>

  devsw[CONSOLE].write = consolewrite;
80100e08:	c7 05 4c 2a 11 80 87 	movl   $0x80100d87,0x80112a4c
80100e0f:	0d 10 80 
  devsw[CONSOLE].read = consoleread;
80100e12:	c7 05 48 2a 11 80 86 	movl   $0x80100c86,0x80112a48
80100e19:	0c 10 80 
  cons.locking = 1;
80100e1c:	c7 05 34 c6 10 80 01 	movl   $0x1,0x8010c634
80100e23:	00 00 00 

  picenable(IRQ_KBD);
80100e26:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80100e2d:	e8 93 36 00 00       	call   801044c5 <picenable>
  ioapicenable(IRQ_KBD, 0);
80100e32:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80100e39:	00 
80100e3a:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80100e41:	e8 b4 23 00 00       	call   801031fa <ioapicenable>
}
80100e46:	c9                   	leave  
80100e47:	c3                   	ret    

80100e48 <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
80100e48:	55                   	push   %ebp
80100e49:	89 e5                	mov    %esp,%ebp
80100e4b:	83 ec 14             	sub    $0x14,%esp
80100e4e:	8b 45 08             	mov    0x8(%ebp),%eax
80100e51:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80100e55:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
80100e59:	89 c2                	mov    %eax,%edx
80100e5b:	ec                   	in     (%dx),%al
80100e5c:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80100e5f:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80100e63:	c9                   	leave  
80100e64:	c3                   	ret    

80100e65 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80100e65:	55                   	push   %ebp
80100e66:	89 e5                	mov    %esp,%ebp
80100e68:	83 ec 08             	sub    $0x8,%esp
80100e6b:	8b 55 08             	mov    0x8(%ebp),%edx
80100e6e:	8b 45 0c             	mov    0xc(%ebp),%eax
80100e71:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80100e75:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100e78:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80100e7c:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80100e80:	ee                   	out    %al,(%dx)
}
80100e81:	c9                   	leave  
80100e82:	c3                   	ret    

80100e83 <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
80100e83:	55                   	push   %ebp
80100e84:	89 e5                	mov    %esp,%ebp
80100e86:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80100e89:	9c                   	pushf  
80100e8a:	58                   	pop    %eax
80100e8b:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return eflags;
80100e8e:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80100e91:	c9                   	leave  
80100e92:	c3                   	ret    

80100e93 <cli>:
  asm volatile("movw %0, %%gs" : : "r" (v));
}

static inline void
cli(void)
{
80100e93:	55                   	push   %ebp
80100e94:	89 e5                	mov    %esp,%ebp
  asm volatile("cli");
80100e96:	fa                   	cli    
}
80100e97:	5d                   	pop    %ebp
80100e98:	c3                   	ret    

80100e99 <sti>:

static inline void
sti(void)
{
80100e99:	55                   	push   %ebp
80100e9a:	89 e5                	mov    %esp,%ebp
  asm volatile("sti");
80100e9c:	fb                   	sti    
}
80100e9d:	5d                   	pop    %ebp
80100e9e:	c3                   	ret    

80100e9f <xchg>:

static inline uint
xchg(volatile uint *addr, uint newval)
{
80100e9f:	55                   	push   %ebp
80100ea0:	89 e5                	mov    %esp,%ebp
80100ea2:	83 ec 10             	sub    $0x10,%esp
  uint result;

  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
80100ea5:	8b 55 08             	mov    0x8(%ebp),%edx
80100ea8:	8b 45 0c             	mov    0xc(%ebp),%eax
80100eab:	8b 4d 08             	mov    0x8(%ebp),%ecx
80100eae:	f0 87 02             	lock xchg %eax,(%edx)
80100eb1:	89 45 fc             	mov    %eax,-0x4(%ebp)
               "+m" (*addr), "=a" (result) :
               "1" (newval) :
               "cc");
  return result;
80100eb4:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80100eb7:	c9                   	leave  
80100eb8:	c3                   	ret    

80100eb9 <lock>:
// debugf(), we don't have to worry about which CPU we're on. The spinlocks in
// xv6 presumably pay that prize to make their interface simpler?

static void
lock(int *intena)
{
80100eb9:	55                   	push   %ebp
80100eba:	89 e5                	mov    %esp,%ebp
80100ebc:	83 ec 08             	sub    $0x8,%esp
  *intena = readeflags() & FL_IF;
80100ebf:	e8 bf ff ff ff       	call   80100e83 <readeflags>
80100ec4:	89 c2                	mov    %eax,%edx
80100ec6:	81 e2 00 02 00 00    	and    $0x200,%edx
80100ecc:	8b 45 08             	mov    0x8(%ebp),%eax
80100ecf:	89 10                	mov    %edx,(%eax)
  // Migration is possible *right here*, but if so, FL_IF was asserted when
  // we sampled it and is still asserted now. That holds true up until the
  // CLI instruction itself. (Also see TRICKS.)
  cli();
80100ed1:	e8 bd ff ff ff       	call   80100e93 <cli>
  while(xchg(&locked, 1) != 0)
80100ed6:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80100edd:	00 
80100ede:	c7 04 24 3c c6 10 80 	movl   $0x8010c63c,(%esp)
80100ee5:	e8 b5 ff ff ff       	call   80100e9f <xchg>
80100eea:	85 c0                	test   %eax,%eax
80100eec:	75 e8                	jne    80100ed6 <lock+0x1d>
    ;
}
80100eee:	c9                   	leave  
80100eef:	c3                   	ret    

80100ef0 <unlock>:

static void
unlock(int intena)
{
80100ef0:	55                   	push   %ebp
80100ef1:	89 e5                	mov    %esp,%ebp
80100ef3:	83 ec 08             	sub    $0x8,%esp
  xchg(&locked, 0);
80100ef6:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80100efd:	00 
80100efe:	c7 04 24 3c c6 10 80 	movl   $0x8010c63c,(%esp)
80100f05:	e8 95 ff ff ff       	call   80100e9f <xchg>
  if(intena)
80100f0a:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80100f0e:	74 05                	je     80100f15 <unlock+0x25>
    sti();
80100f10:	e8 84 ff ff ff       	call   80100e99 <sti>
}
80100f15:	c9                   	leave  
80100f16:	c3                   	ret    

80100f17 <cantransmit>:

static int
cantransmit(void)
{
80100f17:	55                   	push   %ebp
80100f18:	89 e5                	mov    %esp,%ebp
80100f1a:	83 ec 04             	sub    $0x4,%esp
  return inb(COM2+UART_LINE_STATUS) & UART_TRANSMIT_READY;
80100f1d:	c7 04 24 fd 02 00 00 	movl   $0x2fd,(%esp)
80100f24:	e8 1f ff ff ff       	call   80100e48 <inb>
80100f29:	0f b6 c0             	movzbl %al,%eax
80100f2c:	83 e0 20             	and    $0x20,%eax
}
80100f2f:	c9                   	leave  
80100f30:	c3                   	ret    

80100f31 <debugputc>:

static void
debugputc(int c)
{
80100f31:	55                   	push   %ebp
80100f32:	89 e5                	mov    %esp,%ebp
80100f34:	83 ec 28             	sub    $0x28,%esp
  int i;

  if(!uart)
80100f37:	a1 38 c6 10 80       	mov    0x8010c638,%eax
80100f3c:	85 c0                	test   %eax,%eax
80100f3e:	74 40                	je     80100f80 <debugputc+0x4f>
    return;
  for(i = 0; i < 128 && !cantransmit(); i++)
80100f40:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80100f47:	eb 10                	jmp    80100f59 <debugputc+0x28>
    microdelay(10);
80100f49:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
80100f50:	e8 26 28 00 00       	call   8010377b <microdelay>
{
  int i;

  if(!uart)
    return;
  for(i = 0; i < 128 && !cantransmit(); i++)
80100f55:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80100f59:	83 7d f4 7f          	cmpl   $0x7f,-0xc(%ebp)
80100f5d:	7f 09                	jg     80100f68 <debugputc+0x37>
80100f5f:	e8 b3 ff ff ff       	call   80100f17 <cantransmit>
80100f64:	85 c0                	test   %eax,%eax
80100f66:	74 e1                	je     80100f49 <debugputc+0x18>
    microdelay(10);
  outb(COM2+UART_TRANSMIT_BUFFER, c);
80100f68:	8b 45 08             	mov    0x8(%ebp),%eax
80100f6b:	0f b6 c0             	movzbl %al,%eax
80100f6e:	89 44 24 04          	mov    %eax,0x4(%esp)
80100f72:	c7 04 24 f8 02 00 00 	movl   $0x2f8,(%esp)
80100f79:	e8 e7 fe ff ff       	call   80100e65 <outb>
80100f7e:	eb 01                	jmp    80100f81 <debugputc+0x50>
debugputc(int c)
{
  int i;

  if(!uart)
    return;
80100f80:	90                   	nop
  for(i = 0; i < 128 && !cantransmit(); i++)
    microdelay(10);
  outb(COM2+UART_TRANSMIT_BUFFER, c);
}
80100f81:	c9                   	leave  
80100f82:	c3                   	ret    

80100f83 <debuginit>:

void
debuginit(void)
{
80100f83:	55                   	push   %ebp
80100f84:	89 e5                	mov    %esp,%ebp
80100f86:	83 ec 08             	sub    $0x8,%esp
  // Turn off the FIFO
  outb(COM2+UART_FIFO_CONTROL, 0);
80100f89:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80100f90:	00 
80100f91:	c7 04 24 fa 02 00 00 	movl   $0x2fa,(%esp)
80100f98:	e8 c8 fe ff ff       	call   80100e65 <outb>

  // 9600 baud, 8 data bits, 1 stop bit, parity off.
  outb(COM2+UART_LINE_CONTROL, UART_DIVISOR_LATCH);
80100f9d:	c7 44 24 04 80 00 00 	movl   $0x80,0x4(%esp)
80100fa4:	00 
80100fa5:	c7 04 24 fb 02 00 00 	movl   $0x2fb,(%esp)
80100fac:	e8 b4 fe ff ff       	call   80100e65 <outb>
  outb(COM2+UART_DIVISOR_LOW, 115200/9600);
80100fb1:	c7 44 24 04 0c 00 00 	movl   $0xc,0x4(%esp)
80100fb8:	00 
80100fb9:	c7 04 24 f8 02 00 00 	movl   $0x2f8,(%esp)
80100fc0:	e8 a0 fe ff ff       	call   80100e65 <outb>
  outb(COM2+UART_DIVISOR_HIGH, 0);
80100fc5:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80100fcc:	00 
80100fcd:	c7 04 24 f9 02 00 00 	movl   $0x2f9,(%esp)
80100fd4:	e8 8c fe ff ff       	call   80100e65 <outb>
  outb(COM2+UART_LINE_CONTROL, 0x03);   // 8 data bits.
80100fd9:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
80100fe0:	00 
80100fe1:	c7 04 24 fb 02 00 00 	movl   $0x2fb,(%esp)
80100fe8:	e8 78 fe ff ff       	call   80100e65 <outb>
  outb(COM2+UART_MODEM_CONTROL, 0);     // No RTS/DTR handshake.
80100fed:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80100ff4:	00 
80100ff5:	c7 04 24 fc 02 00 00 	movl   $0x2fc,(%esp)
80100ffc:	e8 64 fe ff ff       	call   80100e65 <outb>
  outb(COM2+UART_INTERRUPT_ENABLE, 0);  // Disable interrupts.
80101001:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80101008:	00 
80101009:	c7 04 24 f9 02 00 00 	movl   $0x2f9,(%esp)
80101010:	e8 50 fe ff ff       	call   80100e65 <outb>

  // If status is 0xFF, no serial port.
  if(inb(COM2+UART_LINE_STATUS) == 0xFF)
80101015:	c7 04 24 fd 02 00 00 	movl   $0x2fd,(%esp)
8010101c:	e8 27 fe ff ff       	call   80100e48 <inb>
80101021:	3c ff                	cmp    $0xff,%al
80101023:	74 24                	je     80101049 <debuginit+0xc6>
    return;
  uart = 1;
80101025:	c7 05 38 c6 10 80 01 	movl   $0x1,0x8010c638
8010102c:	00 00 00 

  // Acknowledge pre-existing interrupt conditions, if any.
  // We don't use any of the interrupt stuff ourselves.
  inb(COM2+UART_INTERRUPT_ID);
8010102f:	c7 04 24 fa 02 00 00 	movl   $0x2fa,(%esp)
80101036:	e8 0d fe ff ff       	call   80100e48 <inb>
  inb(COM2+UART_RECEIVE_BUFFER);
8010103b:	c7 04 24 f8 02 00 00 	movl   $0x2f8,(%esp)
80101042:	e8 01 fe ff ff       	call   80100e48 <inb>
80101047:	eb 01                	jmp    8010104a <debuginit+0xc7>
  outb(COM2+UART_MODEM_CONTROL, 0);     // No RTS/DTR handshake.
  outb(COM2+UART_INTERRUPT_ENABLE, 0);  // Disable interrupts.

  // If status is 0xFF, no serial port.
  if(inb(COM2+UART_LINE_STATUS) == 0xFF)
    return;
80101049:	90                   	nop

  // Acknowledge pre-existing interrupt conditions, if any.
  // We don't use any of the interrupt stuff ourselves.
  inb(COM2+UART_INTERRUPT_ID);
  inb(COM2+UART_RECEIVE_BUFFER);
}
8010104a:	c9                   	leave  
8010104b:	c3                   	ret    

8010104c <printint>:
// It's certainly sad that we have to replicate the printf code *again* for
// this module. Alas there doesn't seem to be a nice way to modularize.

static void
printint(int xx, int base, int sign)
{
8010104c:	55                   	push   %ebp
8010104d:	89 e5                	mov    %esp,%ebp
8010104f:	53                   	push   %ebx
80101050:	83 ec 44             	sub    $0x44,%esp
  static char digits[] = "0123456789abcdef";
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = (xx < 0)))
80101053:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80101057:	74 19                	je     80101072 <printint+0x26>
80101059:	8b 45 08             	mov    0x8(%ebp),%eax
8010105c:	c1 e8 1f             	shr    $0x1f,%eax
8010105f:	89 45 10             	mov    %eax,0x10(%ebp)
80101062:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80101066:	74 0a                	je     80101072 <printint+0x26>
    x = -xx;
80101068:	8b 45 08             	mov    0x8(%ebp),%eax
8010106b:	f7 d8                	neg    %eax
8010106d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  static char digits[] = "0123456789abcdef";
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = (xx < 0)))
80101070:	eb 06                	jmp    80101078 <printint+0x2c>
    x = -xx;
  else
    x = xx;
80101072:	8b 45 08             	mov    0x8(%ebp),%eax
80101075:	89 45 f4             	mov    %eax,-0xc(%ebp)

  i = 0;
80101078:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  do{
    buf[i++] = digits[x % base];
8010107f:	8b 4d f0             	mov    -0x10(%ebp),%ecx
80101082:	8b 5d 0c             	mov    0xc(%ebp),%ebx
80101085:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101088:	ba 00 00 00 00       	mov    $0x0,%edx
8010108d:	f7 f3                	div    %ebx
8010108f:	89 d0                	mov    %edx,%eax
80101091:	0f b6 80 18 a0 10 80 	movzbl -0x7fef5fe8(%eax),%eax
80101098:	88 44 0d e0          	mov    %al,-0x20(%ebp,%ecx,1)
8010109c:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
  }while((x /= base) != 0);
801010a0:	8b 45 0c             	mov    0xc(%ebp),%eax
801010a3:	89 45 d4             	mov    %eax,-0x2c(%ebp)
801010a6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801010a9:	ba 00 00 00 00       	mov    $0x0,%edx
801010ae:	f7 75 d4             	divl   -0x2c(%ebp)
801010b1:	89 45 f4             	mov    %eax,-0xc(%ebp)
801010b4:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801010b8:	75 c5                	jne    8010107f <printint+0x33>

  if(sign)
801010ba:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
801010be:	74 23                	je     801010e3 <printint+0x97>
    buf[i++] = '-';
801010c0:	8b 45 f0             	mov    -0x10(%ebp),%eax
801010c3:	c6 44 05 e0 2d       	movb   $0x2d,-0x20(%ebp,%eax,1)
801010c8:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)

  while(--i >= 0)
801010cc:	eb 16                	jmp    801010e4 <printint+0x98>
    debugputc(buf[i]);
801010ce:	8b 45 f0             	mov    -0x10(%ebp),%eax
801010d1:	0f b6 44 05 e0       	movzbl -0x20(%ebp,%eax,1),%eax
801010d6:	0f be c0             	movsbl %al,%eax
801010d9:	89 04 24             	mov    %eax,(%esp)
801010dc:	e8 50 fe ff ff       	call   80100f31 <debugputc>
801010e1:	eb 01                	jmp    801010e4 <printint+0x98>
  }while((x /= base) != 0);

  if(sign)
    buf[i++] = '-';

  while(--i >= 0)
801010e3:	90                   	nop
801010e4:	83 6d f0 01          	subl   $0x1,-0x10(%ebp)
801010e8:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801010ec:	79 e0                	jns    801010ce <printint+0x82>
    debugputc(buf[i]);
}
801010ee:	83 c4 44             	add    $0x44,%esp
801010f1:	5b                   	pop    %ebx
801010f2:	5d                   	pop    %ebp
801010f3:	c3                   	ret    

801010f4 <debugf>:

void
debugf(char *fmt, ...)
{
801010f4:	55                   	push   %ebp
801010f5:	89 e5                	mov    %esp,%ebp
801010f7:	83 ec 38             	sub    $0x38,%esp
  int i, c;
  uint *argp;
  char *s;
  int intena;

  if(fmt == 0){
801010fa:	8b 45 08             	mov    0x8(%ebp),%eax
801010fd:	85 c0                	test   %eax,%eax
801010ff:	75 11                	jne    80101112 <debugf+0x1e>
    debugf("debugf: null fmt\n");  // no panic(), no console.c dependency
80101101:	c7 04 24 f5 8e 10 80 	movl   $0x80108ef5,(%esp)
80101108:	e8 e7 ff ff ff       	call   801010f4 <debugf>
    return;
8010110d:	e9 6f 01 00 00       	jmp    80101281 <debugf+0x18d>
  }

  lock(&intena);
80101112:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80101115:	89 04 24             	mov    %eax,(%esp)
80101118:	e8 9c fd ff ff       	call   80100eb9 <lock>

  argp = (uint*)(void*)(&fmt + 1);
8010111d:	8d 45 08             	lea    0x8(%ebp),%eax
80101120:	83 c0 04             	add    $0x4,%eax
80101123:	89 45 f0             	mov    %eax,-0x10(%ebp)
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80101126:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
8010112d:	e9 20 01 00 00       	jmp    80101252 <debugf+0x15e>
    if(c != '%'){
80101132:	83 7d ec 25          	cmpl   $0x25,-0x14(%ebp)
80101136:	74 10                	je     80101148 <debugf+0x54>
      debugputc(c);
80101138:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010113b:	89 04 24             	mov    %eax,(%esp)
8010113e:	e8 ee fd ff ff       	call   80100f31 <debugputc>
      continue;
80101143:	e9 06 01 00 00       	jmp    8010124e <debugf+0x15a>
    }
    c = fmt[++i] & 0xff;
80101148:	8b 55 08             	mov    0x8(%ebp),%edx
8010114b:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
8010114f:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101152:	8d 04 02             	lea    (%edx,%eax,1),%eax
80101155:	0f b6 00             	movzbl (%eax),%eax
80101158:	0f be c0             	movsbl %al,%eax
8010115b:	25 ff 00 00 00       	and    $0xff,%eax
80101160:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(c == 0)
80101163:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80101167:	0f 84 08 01 00 00    	je     80101275 <debugf+0x181>
      break;
    switch(c){
8010116d:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101170:	83 f8 70             	cmp    $0x70,%eax
80101173:	74 4d                	je     801011c2 <debugf+0xce>
80101175:	83 f8 70             	cmp    $0x70,%eax
80101178:	7f 13                	jg     8010118d <debugf+0x99>
8010117a:	83 f8 25             	cmp    $0x25,%eax
8010117d:	0f 84 a6 00 00 00    	je     80101229 <debugf+0x135>
80101183:	83 f8 64             	cmp    $0x64,%eax
80101186:	74 14                	je     8010119c <debugf+0xa8>
80101188:	e9 aa 00 00 00       	jmp    80101237 <debugf+0x143>
8010118d:	83 f8 73             	cmp    $0x73,%eax
80101190:	74 53                	je     801011e5 <debugf+0xf1>
80101192:	83 f8 78             	cmp    $0x78,%eax
80101195:	74 2b                	je     801011c2 <debugf+0xce>
80101197:	e9 9b 00 00 00       	jmp    80101237 <debugf+0x143>
    case 'd':
      printint(*argp++, 10, 1);
8010119c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010119f:	8b 00                	mov    (%eax),%eax
801011a1:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
801011a5:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
801011ac:	00 
801011ad:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
801011b4:	00 
801011b5:	89 04 24             	mov    %eax,(%esp)
801011b8:	e8 8f fe ff ff       	call   8010104c <printint>
      break;
801011bd:	e9 8c 00 00 00       	jmp    8010124e <debugf+0x15a>
    case 'x':
    case 'p':
      printint(*argp++, 16, 0);
801011c2:	8b 45 f0             	mov    -0x10(%ebp),%eax
801011c5:	8b 00                	mov    (%eax),%eax
801011c7:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
801011cb:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
801011d2:	00 
801011d3:	c7 44 24 04 10 00 00 	movl   $0x10,0x4(%esp)
801011da:	00 
801011db:	89 04 24             	mov    %eax,(%esp)
801011de:	e8 69 fe ff ff       	call   8010104c <printint>
      break;
801011e3:	eb 69                	jmp    8010124e <debugf+0x15a>
    case 's':
      if((s = (char*)*argp++) == 0)
801011e5:	8b 45 f0             	mov    -0x10(%ebp),%eax
801011e8:	8b 00                	mov    (%eax),%eax
801011ea:	89 45 f4             	mov    %eax,-0xc(%ebp)
801011ed:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801011f1:	0f 94 c0             	sete   %al
801011f4:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
801011f8:	84 c0                	test   %al,%al
801011fa:	74 20                	je     8010121c <debugf+0x128>
        s = "(null)";
801011fc:	c7 45 f4 07 8f 10 80 	movl   $0x80108f07,-0xc(%ebp)
      for(; *s; s++)
80101203:	eb 18                	jmp    8010121d <debugf+0x129>
        debugputc(*s);
80101205:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101208:	0f b6 00             	movzbl (%eax),%eax
8010120b:	0f be c0             	movsbl %al,%eax
8010120e:	89 04 24             	mov    %eax,(%esp)
80101211:	e8 1b fd ff ff       	call   80100f31 <debugputc>
      printint(*argp++, 16, 0);
      break;
    case 's':
      if((s = (char*)*argp++) == 0)
        s = "(null)";
      for(; *s; s++)
80101216:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
8010121a:	eb 01                	jmp    8010121d <debugf+0x129>
8010121c:	90                   	nop
8010121d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101220:	0f b6 00             	movzbl (%eax),%eax
80101223:	84 c0                	test   %al,%al
80101225:	75 de                	jne    80101205 <debugf+0x111>
        debugputc(*s);
      break;
80101227:	eb 25                	jmp    8010124e <debugf+0x15a>
    case '%':
      debugputc('%');
80101229:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
80101230:	e8 fc fc ff ff       	call   80100f31 <debugputc>
      break;
80101235:	eb 17                	jmp    8010124e <debugf+0x15a>
    default:
      // Print unknown % sequence to draw attention.
      debugputc('%');
80101237:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
8010123e:	e8 ee fc ff ff       	call   80100f31 <debugputc>
      debugputc(c);
80101243:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101246:	89 04 24             	mov    %eax,(%esp)
80101249:	e8 e3 fc ff ff       	call   80100f31 <debugputc>
  }

  lock(&intena);

  argp = (uint*)(void*)(&fmt + 1);
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
8010124e:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
80101252:	8b 55 08             	mov    0x8(%ebp),%edx
80101255:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101258:	8d 04 02             	lea    (%edx,%eax,1),%eax
8010125b:	0f b6 00             	movzbl (%eax),%eax
8010125e:	0f be c0             	movsbl %al,%eax
80101261:	25 ff 00 00 00       	and    $0xff,%eax
80101266:	89 45 ec             	mov    %eax,-0x14(%ebp)
80101269:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
8010126d:	0f 85 bf fe ff ff    	jne    80101132 <debugf+0x3e>
80101273:	eb 01                	jmp    80101276 <debugf+0x182>
      debugputc(c);
      continue;
    }
    c = fmt[++i] & 0xff;
    if(c == 0)
      break;
80101275:	90                   	nop
      debugputc(c);
      break;
    }
  }

  unlock(intena);
80101276:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101279:	89 04 24             	mov    %eax,(%esp)
8010127c:	e8 6f fc ff ff       	call   80100ef0 <unlock>
}
80101281:	c9                   	leave  
80101282:	c3                   	ret    
	...

80101284 <exec>:
#include "x86.h"
#include "elf.h"

int
exec(char *path, char **argv)
{
80101284:	55                   	push   %ebp
80101285:	89 e5                	mov    %esp,%ebp
80101287:	81 ec 38 01 00 00    	sub    $0x138,%esp
  struct elfhdr elf;
  struct inode *ip;
  struct proghdr ph;
  pde_t *pgdir, *oldpgdir;

  begin_op();
8010128d:	e8 d8 27 00 00       	call   80103a6a <begin_op>
  if((ip = namei(path)) == 0){
80101292:	8b 45 08             	mov    0x8(%ebp),%eax
80101295:	89 04 24             	mov    %eax,(%esp)
80101298:	e8 82 19 00 00       	call   80102c1f <namei>
8010129d:	89 45 ec             	mov    %eax,-0x14(%ebp)
801012a0:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
801012a4:	75 0f                	jne    801012b5 <exec+0x31>
    end_op();
801012a6:	e8 41 28 00 00       	call   80103aec <end_op>
    return -1;
801012ab:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801012b0:	e9 e2 03 00 00       	jmp    80101697 <exec+0x413>
  }
  ilock(ip);
801012b5:	8b 45 ec             	mov    -0x14(%ebp),%eax
801012b8:	89 04 24             	mov    %eax,(%esp)
801012bb:	e8 b1 0d 00 00       	call   80102071 <ilock>
  pgdir = 0;
801012c0:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) < sizeof(elf))
801012c7:	8d 85 0c ff ff ff    	lea    -0xf4(%ebp),%eax
801012cd:	c7 44 24 0c 34 00 00 	movl   $0x34,0xc(%esp)
801012d4:	00 
801012d5:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
801012dc:	00 
801012dd:	89 44 24 04          	mov    %eax,0x4(%esp)
801012e1:	8b 45 ec             	mov    -0x14(%ebp),%eax
801012e4:	89 04 24             	mov    %eax,(%esp)
801012e7:	e8 84 12 00 00       	call   80102570 <readi>
801012ec:	83 f8 33             	cmp    $0x33,%eax
801012ef:	0f 86 57 03 00 00    	jbe    8010164c <exec+0x3c8>
    goto bad;
  if(elf.magic != ELF_MAGIC)
801012f5:	8b 85 0c ff ff ff    	mov    -0xf4(%ebp),%eax
801012fb:	3d 7f 45 4c 46       	cmp    $0x464c457f,%eax
80101300:	0f 85 49 03 00 00    	jne    8010164f <exec+0x3cb>
    goto bad;

  if((pgdir = setupkvm()) == 0)
80101306:	e8 ce 72 00 00       	call   801085d9 <setupkvm>
8010130b:	89 45 f0             	mov    %eax,-0x10(%ebp)
8010130e:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80101312:	0f 84 3a 03 00 00    	je     80101652 <exec+0x3ce>
    goto bad;

  // Load program into memory.
  sz = 0;
80101318:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
8010131f:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
80101326:	8b 85 28 ff ff ff    	mov    -0xd8(%ebp),%eax
8010132c:	89 45 dc             	mov    %eax,-0x24(%ebp)
8010132f:	e9 ca 00 00 00       	jmp    801013fe <exec+0x17a>
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
80101334:	8b 55 dc             	mov    -0x24(%ebp),%edx
80101337:	8d 85 ec fe ff ff    	lea    -0x114(%ebp),%eax
8010133d:	c7 44 24 0c 20 00 00 	movl   $0x20,0xc(%esp)
80101344:	00 
80101345:	89 54 24 08          	mov    %edx,0x8(%esp)
80101349:	89 44 24 04          	mov    %eax,0x4(%esp)
8010134d:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101350:	89 04 24             	mov    %eax,(%esp)
80101353:	e8 18 12 00 00       	call   80102570 <readi>
80101358:	83 f8 20             	cmp    $0x20,%eax
8010135b:	0f 85 f4 02 00 00    	jne    80101655 <exec+0x3d1>
      goto bad;
    if(ph.type != ELF_PROG_LOAD)
80101361:	8b 85 ec fe ff ff    	mov    -0x114(%ebp),%eax
80101367:	83 f8 01             	cmp    $0x1,%eax
8010136a:	0f 85 80 00 00 00    	jne    801013f0 <exec+0x16c>
      continue;
    if(ph.memsz < ph.filesz)
80101370:	8b 95 00 ff ff ff    	mov    -0x100(%ebp),%edx
80101376:	8b 85 fc fe ff ff    	mov    -0x104(%ebp),%eax
8010137c:	39 c2                	cmp    %eax,%edx
8010137e:	0f 82 d4 02 00 00    	jb     80101658 <exec+0x3d4>
      goto bad;
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
80101384:	8b 95 f4 fe ff ff    	mov    -0x10c(%ebp),%edx
8010138a:	8b 85 00 ff ff ff    	mov    -0x100(%ebp),%eax
80101390:	8d 04 02             	lea    (%edx,%eax,1),%eax
80101393:	89 44 24 08          	mov    %eax,0x8(%esp)
80101397:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010139a:	89 44 24 04          	mov    %eax,0x4(%esp)
8010139e:	8b 45 f0             	mov    -0x10(%ebp),%eax
801013a1:	89 04 24             	mov    %eax,(%esp)
801013a4:	e8 09 76 00 00       	call   801089b2 <allocuvm>
801013a9:	89 45 e4             	mov    %eax,-0x1c(%ebp)
801013ac:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
801013b0:	0f 84 a5 02 00 00    	je     8010165b <exec+0x3d7>
      goto bad;
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
801013b6:	8b 8d fc fe ff ff    	mov    -0x104(%ebp),%ecx
801013bc:	8b 95 f0 fe ff ff    	mov    -0x110(%ebp),%edx
801013c2:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
801013c8:	89 4c 24 10          	mov    %ecx,0x10(%esp)
801013cc:	89 54 24 0c          	mov    %edx,0xc(%esp)
801013d0:	8b 55 ec             	mov    -0x14(%ebp),%edx
801013d3:	89 54 24 08          	mov    %edx,0x8(%esp)
801013d7:	89 44 24 04          	mov    %eax,0x4(%esp)
801013db:	8b 45 f0             	mov    -0x10(%ebp),%eax
801013de:	89 04 24             	mov    %eax,(%esp)
801013e1:	e8 e4 74 00 00       	call   801088ca <loaduvm>
801013e6:	85 c0                	test   %eax,%eax
801013e8:	0f 88 70 02 00 00    	js     8010165e <exec+0x3da>
801013ee:	eb 01                	jmp    801013f1 <exec+0x16d>
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
      goto bad;
    if(ph.type != ELF_PROG_LOAD)
      continue;
801013f0:	90                   	nop
  if((pgdir = setupkvm()) == 0)
    goto bad;

  // Load program into memory.
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
801013f1:	83 45 d8 01          	addl   $0x1,-0x28(%ebp)
801013f5:	8b 45 dc             	mov    -0x24(%ebp),%eax
801013f8:	83 c0 20             	add    $0x20,%eax
801013fb:	89 45 dc             	mov    %eax,-0x24(%ebp)
801013fe:	0f b7 85 38 ff ff ff 	movzwl -0xc8(%ebp),%eax
80101405:	0f b7 c0             	movzwl %ax,%eax
80101408:	3b 45 d8             	cmp    -0x28(%ebp),%eax
8010140b:	0f 8f 23 ff ff ff    	jg     80101334 <exec+0xb0>
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
      goto bad;
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
      goto bad;
  }
  iunlockput(ip);
80101411:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101414:	89 04 24             	mov    %eax,(%esp)
80101417:	e8 e2 0e 00 00       	call   801022fe <iunlockput>
  end_op();
8010141c:	e8 cb 26 00 00       	call   80103aec <end_op>
  ip = 0;
80101421:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)

  // Allocate two pages at the next page boundary.
  // Make the first inaccessible.  Use the second as the user stack.
  sz = PGROUNDUP(sz);
80101428:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010142b:	05 ff 0f 00 00       	add    $0xfff,%eax
80101430:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80101435:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
80101438:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010143b:	05 00 20 00 00       	add    $0x2000,%eax
80101440:	89 44 24 08          	mov    %eax,0x8(%esp)
80101444:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101447:	89 44 24 04          	mov    %eax,0x4(%esp)
8010144b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010144e:	89 04 24             	mov    %eax,(%esp)
80101451:	e8 5c 75 00 00       	call   801089b2 <allocuvm>
80101456:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80101459:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
8010145d:	0f 84 fe 01 00 00    	je     80101661 <exec+0x3dd>
    goto bad;
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
80101463:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101466:	2d 00 20 00 00       	sub    $0x2000,%eax
8010146b:	89 44 24 04          	mov    %eax,0x4(%esp)
8010146f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101472:	89 04 24             	mov    %eax,(%esp)
80101475:	e8 61 77 00 00       	call   80108bdb <clearpteu>
  sp = sz;
8010147a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010147d:	89 45 e8             	mov    %eax,-0x18(%ebp)

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
80101480:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
80101487:	e9 81 00 00 00       	jmp    8010150d <exec+0x289>
    if(argc >= MAXARG)
8010148c:	83 7d e0 1f          	cmpl   $0x1f,-0x20(%ebp)
80101490:	0f 87 ce 01 00 00    	ja     80101664 <exec+0x3e0>
      goto bad;
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
80101496:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101499:	c1 e0 02             	shl    $0x2,%eax
8010149c:	03 45 0c             	add    0xc(%ebp),%eax
8010149f:	8b 00                	mov    (%eax),%eax
801014a1:	89 04 24             	mov    %eax,(%esp)
801014a4:	e8 82 47 00 00       	call   80105c2b <strlen>
801014a9:	f7 d0                	not    %eax
801014ab:	03 45 e8             	add    -0x18(%ebp),%eax
801014ae:	83 e0 fc             	and    $0xfffffffc,%eax
801014b1:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
801014b4:	8b 45 e0             	mov    -0x20(%ebp),%eax
801014b7:	c1 e0 02             	shl    $0x2,%eax
801014ba:	03 45 0c             	add    0xc(%ebp),%eax
801014bd:	8b 00                	mov    (%eax),%eax
801014bf:	89 04 24             	mov    %eax,(%esp)
801014c2:	e8 64 47 00 00       	call   80105c2b <strlen>
801014c7:	83 c0 01             	add    $0x1,%eax
801014ca:	89 c2                	mov    %eax,%edx
801014cc:	8b 45 e0             	mov    -0x20(%ebp),%eax
801014cf:	c1 e0 02             	shl    $0x2,%eax
801014d2:	03 45 0c             	add    0xc(%ebp),%eax
801014d5:	8b 00                	mov    (%eax),%eax
801014d7:	89 54 24 0c          	mov    %edx,0xc(%esp)
801014db:	89 44 24 08          	mov    %eax,0x8(%esp)
801014df:	8b 45 e8             	mov    -0x18(%ebp),%eax
801014e2:	89 44 24 04          	mov    %eax,0x4(%esp)
801014e6:	8b 45 f0             	mov    -0x10(%ebp),%eax
801014e9:	89 04 24             	mov    %eax,(%esp)
801014ec:	e8 a2 78 00 00       	call   80108d93 <copyout>
801014f1:	85 c0                	test   %eax,%eax
801014f3:	0f 88 6e 01 00 00    	js     80101667 <exec+0x3e3>
      goto bad;
    ustack[3+argc] = sp;
801014f9:	8b 45 e0             	mov    -0x20(%ebp),%eax
801014fc:	8d 50 03             	lea    0x3(%eax),%edx
801014ff:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101502:	89 84 95 40 ff ff ff 	mov    %eax,-0xc0(%ebp,%edx,4)
    goto bad;
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
  sp = sz;

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
80101509:	83 45 e0 01          	addl   $0x1,-0x20(%ebp)
8010150d:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101510:	c1 e0 02             	shl    $0x2,%eax
80101513:	03 45 0c             	add    0xc(%ebp),%eax
80101516:	8b 00                	mov    (%eax),%eax
80101518:	85 c0                	test   %eax,%eax
8010151a:	0f 85 6c ff ff ff    	jne    8010148c <exec+0x208>
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
      goto bad;
    ustack[3+argc] = sp;
  }
  ustack[3+argc] = 0;
80101520:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101523:	83 c0 03             	add    $0x3,%eax
80101526:	c7 84 85 40 ff ff ff 	movl   $0x0,-0xc0(%ebp,%eax,4)
8010152d:	00 00 00 00 

  ustack[0] = 0xffffffff;  // fake return PC
80101531:	c7 85 40 ff ff ff ff 	movl   $0xffffffff,-0xc0(%ebp)
80101538:	ff ff ff 
  ustack[1] = argc;
8010153b:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010153e:	89 85 44 ff ff ff    	mov    %eax,-0xbc(%ebp)
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80101544:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101547:	83 c0 01             	add    $0x1,%eax
8010154a:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80101551:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101554:	29 d0                	sub    %edx,%eax
80101556:	89 85 48 ff ff ff    	mov    %eax,-0xb8(%ebp)

  sp -= (3+argc+1) * 4;
8010155c:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010155f:	83 c0 04             	add    $0x4,%eax
80101562:	c1 e0 02             	shl    $0x2,%eax
80101565:	29 45 e8             	sub    %eax,-0x18(%ebp)
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
80101568:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010156b:	83 c0 04             	add    $0x4,%eax
8010156e:	c1 e0 02             	shl    $0x2,%eax
80101571:	89 44 24 0c          	mov    %eax,0xc(%esp)
80101575:	8d 85 40 ff ff ff    	lea    -0xc0(%ebp),%eax
8010157b:	89 44 24 08          	mov    %eax,0x8(%esp)
8010157f:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101582:	89 44 24 04          	mov    %eax,0x4(%esp)
80101586:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101589:	89 04 24             	mov    %eax,(%esp)
8010158c:	e8 02 78 00 00       	call   80108d93 <copyout>
80101591:	85 c0                	test   %eax,%eax
80101593:	0f 88 d1 00 00 00    	js     8010166a <exec+0x3e6>
    goto bad;

  // Save program name for debugging.
  for(last=s=path; *s; s++)
80101599:	8b 45 08             	mov    0x8(%ebp),%eax
8010159c:	89 45 d0             	mov    %eax,-0x30(%ebp)
8010159f:	8b 45 d0             	mov    -0x30(%ebp),%eax
801015a2:	89 45 d4             	mov    %eax,-0x2c(%ebp)
801015a5:	eb 17                	jmp    801015be <exec+0x33a>
    if(*s == '/')
801015a7:	8b 45 d0             	mov    -0x30(%ebp),%eax
801015aa:	0f b6 00             	movzbl (%eax),%eax
801015ad:	3c 2f                	cmp    $0x2f,%al
801015af:	75 09                	jne    801015ba <exec+0x336>
      last = s+1;
801015b1:	8b 45 d0             	mov    -0x30(%ebp),%eax
801015b4:	83 c0 01             	add    $0x1,%eax
801015b7:	89 45 d4             	mov    %eax,-0x2c(%ebp)
  sp -= (3+argc+1) * 4;
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
    goto bad;

  // Save program name for debugging.
  for(last=s=path; *s; s++)
801015ba:	83 45 d0 01          	addl   $0x1,-0x30(%ebp)
801015be:	8b 45 d0             	mov    -0x30(%ebp),%eax
801015c1:	0f b6 00             	movzbl (%eax),%eax
801015c4:	84 c0                	test   %al,%al
801015c6:	75 df                	jne    801015a7 <exec+0x323>
    if(*s == '/')
      last = s+1;
  safestrcpy(proc->name, last, sizeof(proc->name));
801015c8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801015ce:	8d 50 6c             	lea    0x6c(%eax),%edx
801015d1:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
801015d8:	00 
801015d9:	8b 45 d4             	mov    -0x2c(%ebp),%eax
801015dc:	89 44 24 04          	mov    %eax,0x4(%esp)
801015e0:	89 14 24             	mov    %edx,(%esp)
801015e3:	e8 f5 45 00 00       	call   80105bdd <safestrcpy>

  // Commit to the user image.
  oldpgdir = proc->pgdir;
801015e8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801015ee:	8b 40 04             	mov    0x4(%eax),%eax
801015f1:	89 45 f4             	mov    %eax,-0xc(%ebp)
  proc->pgdir = pgdir;
801015f4:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801015fa:	8b 55 f0             	mov    -0x10(%ebp),%edx
801015fd:	89 50 04             	mov    %edx,0x4(%eax)
  proc->sz = sz;
80101600:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80101606:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80101609:	89 10                	mov    %edx,(%eax)
  proc->tf->eip = elf.entry;  // main
8010160b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80101611:	8b 40 18             	mov    0x18(%eax),%eax
80101614:	8b 95 24 ff ff ff    	mov    -0xdc(%ebp),%edx
8010161a:	89 50 38             	mov    %edx,0x38(%eax)
  proc->tf->esp = sp;
8010161d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80101623:	8b 40 18             	mov    0x18(%eax),%eax
80101626:	8b 55 e8             	mov    -0x18(%ebp),%edx
80101629:	89 50 44             	mov    %edx,0x44(%eax)
  switchuvm(proc);
8010162c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80101632:	89 04 24             	mov    %eax,(%esp)
80101635:	e8 82 70 00 00       	call   801086bc <switchuvm>
  freevm(oldpgdir);
8010163a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010163d:	89 04 24             	mov    %eax,(%esp)
80101640:	e8 0b 75 00 00       	call   80108b50 <freevm>
  return 0;
80101645:	b8 00 00 00 00       	mov    $0x0,%eax
8010164a:	eb 4b                	jmp    80101697 <exec+0x413>
  ilock(ip);
  pgdir = 0;

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) < sizeof(elf))
    goto bad;
8010164c:	90                   	nop
8010164d:	eb 1c                	jmp    8010166b <exec+0x3e7>
  if(elf.magic != ELF_MAGIC)
    goto bad;
8010164f:	90                   	nop
80101650:	eb 19                	jmp    8010166b <exec+0x3e7>

  if((pgdir = setupkvm()) == 0)
    goto bad;
80101652:	90                   	nop
80101653:	eb 16                	jmp    8010166b <exec+0x3e7>

  // Load program into memory.
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
      goto bad;
80101655:	90                   	nop
80101656:	eb 13                	jmp    8010166b <exec+0x3e7>
    if(ph.type != ELF_PROG_LOAD)
      continue;
    if(ph.memsz < ph.filesz)
      goto bad;
80101658:	90                   	nop
80101659:	eb 10                	jmp    8010166b <exec+0x3e7>
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
      goto bad;
8010165b:	90                   	nop
8010165c:	eb 0d                	jmp    8010166b <exec+0x3e7>
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
      goto bad;
8010165e:	90                   	nop
8010165f:	eb 0a                	jmp    8010166b <exec+0x3e7>

  // Allocate two pages at the next page boundary.
  // Make the first inaccessible.  Use the second as the user stack.
  sz = PGROUNDUP(sz);
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
    goto bad;
80101661:	90                   	nop
80101662:	eb 07                	jmp    8010166b <exec+0x3e7>
  sp = sz;

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
    if(argc >= MAXARG)
      goto bad;
80101664:	90                   	nop
80101665:	eb 04                	jmp    8010166b <exec+0x3e7>
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
      goto bad;
80101667:	90                   	nop
80101668:	eb 01                	jmp    8010166b <exec+0x3e7>
  ustack[1] = argc;
  ustack[2] = sp - (argc+1)*4;  // argv pointer

  sp -= (3+argc+1) * 4;
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
    goto bad;
8010166a:	90                   	nop
  switchuvm(proc);
  freevm(oldpgdir);
  return 0;

 bad:
  if(pgdir)
8010166b:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010166f:	74 0b                	je     8010167c <exec+0x3f8>
    freevm(pgdir);
80101671:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101674:	89 04 24             	mov    %eax,(%esp)
80101677:	e8 d4 74 00 00       	call   80108b50 <freevm>
  if(ip){
8010167c:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80101680:	74 10                	je     80101692 <exec+0x40e>
    iunlockput(ip);
80101682:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101685:	89 04 24             	mov    %eax,(%esp)
80101688:	e8 71 0c 00 00       	call   801022fe <iunlockput>
    end_op();
8010168d:	e8 5a 24 00 00       	call   80103aec <end_op>
  }
  return -1;
80101692:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80101697:	c9                   	leave  
80101698:	c3                   	ret    
80101699:	00 00                	add    %al,(%eax)
	...

8010169c <fileinit>:
  struct file file[NFILE];
} ftable;

void
fileinit(void)
{
8010169c:	55                   	push   %ebp
8010169d:	89 e5                	mov    %esp,%ebp
8010169f:	83 ec 18             	sub    $0x18,%esp
  initlock(&ftable.lock, "ftable");
801016a2:	c7 44 24 04 0e 8f 10 	movl   $0x80108f0e,0x4(%esp)
801016a9:	80 
801016aa:	c7 04 24 a0 20 11 80 	movl   $0x801120a0,(%esp)
801016b1:	e8 70 40 00 00       	call   80105726 <initlock>
}
801016b6:	c9                   	leave  
801016b7:	c3                   	ret    

801016b8 <filealloc>:

// Allocate a file structure.
struct file*
filealloc(void)
{
801016b8:	55                   	push   %ebp
801016b9:	89 e5                	mov    %esp,%ebp
801016bb:	83 ec 28             	sub    $0x28,%esp
  struct file *f;

  acquire(&ftable.lock);
801016be:	c7 04 24 a0 20 11 80 	movl   $0x801120a0,(%esp)
801016c5:	e8 7d 40 00 00       	call   80105747 <acquire>
  for(f = ftable.file; f < ftable.file + NFILE; f++){
801016ca:	c7 45 f4 d4 20 11 80 	movl   $0x801120d4,-0xc(%ebp)
801016d1:	eb 29                	jmp    801016fc <filealloc+0x44>
    if(f->ref == 0){
801016d3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801016d6:	8b 40 04             	mov    0x4(%eax),%eax
801016d9:	85 c0                	test   %eax,%eax
801016db:	75 1b                	jne    801016f8 <filealloc+0x40>
      f->ref = 1;
801016dd:	8b 45 f4             	mov    -0xc(%ebp),%eax
801016e0:	c7 40 04 01 00 00 00 	movl   $0x1,0x4(%eax)
      release(&ftable.lock);
801016e7:	c7 04 24 a0 20 11 80 	movl   $0x801120a0,(%esp)
801016ee:	e8 bd 40 00 00       	call   801057b0 <release>
      return f;
801016f3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801016f6:	eb 1f                	jmp    80101717 <filealloc+0x5f>
filealloc(void)
{
  struct file *f;

  acquire(&ftable.lock);
  for(f = ftable.file; f < ftable.file + NFILE; f++){
801016f8:	83 45 f4 18          	addl   $0x18,-0xc(%ebp)
801016fc:	b8 34 2a 11 80       	mov    $0x80112a34,%eax
80101701:	39 45 f4             	cmp    %eax,-0xc(%ebp)
80101704:	72 cd                	jb     801016d3 <filealloc+0x1b>
      f->ref = 1;
      release(&ftable.lock);
      return f;
    }
  }
  release(&ftable.lock);
80101706:	c7 04 24 a0 20 11 80 	movl   $0x801120a0,(%esp)
8010170d:	e8 9e 40 00 00       	call   801057b0 <release>
  return 0;
80101712:	b8 00 00 00 00       	mov    $0x0,%eax
}
80101717:	c9                   	leave  
80101718:	c3                   	ret    

80101719 <filedup>:

// Increment ref count for file f.
struct file*
filedup(struct file *f)
{
80101719:	55                   	push   %ebp
8010171a:	89 e5                	mov    %esp,%ebp
8010171c:	83 ec 18             	sub    $0x18,%esp
  acquire(&ftable.lock);
8010171f:	c7 04 24 a0 20 11 80 	movl   $0x801120a0,(%esp)
80101726:	e8 1c 40 00 00       	call   80105747 <acquire>
  if(f->ref < 1)
8010172b:	8b 45 08             	mov    0x8(%ebp),%eax
8010172e:	8b 40 04             	mov    0x4(%eax),%eax
80101731:	85 c0                	test   %eax,%eax
80101733:	7f 0c                	jg     80101741 <filedup+0x28>
    panic("filedup");
80101735:	c7 04 24 15 8f 10 80 	movl   $0x80108f15,(%esp)
8010173c:	e8 28 f1 ff ff       	call   80100869 <panic>
  f->ref++;
80101741:	8b 45 08             	mov    0x8(%ebp),%eax
80101744:	8b 40 04             	mov    0x4(%eax),%eax
80101747:	8d 50 01             	lea    0x1(%eax),%edx
8010174a:	8b 45 08             	mov    0x8(%ebp),%eax
8010174d:	89 50 04             	mov    %edx,0x4(%eax)
  release(&ftable.lock);
80101750:	c7 04 24 a0 20 11 80 	movl   $0x801120a0,(%esp)
80101757:	e8 54 40 00 00       	call   801057b0 <release>
  return f;
8010175c:	8b 45 08             	mov    0x8(%ebp),%eax
}
8010175f:	c9                   	leave  
80101760:	c3                   	ret    

80101761 <fileclose>:

// Close file f.  (Decrement ref count, close when reaches 0.)
void
fileclose(struct file *f)
{
80101761:	55                   	push   %ebp
80101762:	89 e5                	mov    %esp,%ebp
80101764:	83 ec 38             	sub    $0x38,%esp
  struct file ff;

  acquire(&ftable.lock);
80101767:	c7 04 24 a0 20 11 80 	movl   $0x801120a0,(%esp)
8010176e:	e8 d4 3f 00 00       	call   80105747 <acquire>
  if(f->ref < 1)
80101773:	8b 45 08             	mov    0x8(%ebp),%eax
80101776:	8b 40 04             	mov    0x4(%eax),%eax
80101779:	85 c0                	test   %eax,%eax
8010177b:	7f 0c                	jg     80101789 <fileclose+0x28>
    panic("fileclose");
8010177d:	c7 04 24 1d 8f 10 80 	movl   $0x80108f1d,(%esp)
80101784:	e8 e0 f0 ff ff       	call   80100869 <panic>
  if(--f->ref > 0){
80101789:	8b 45 08             	mov    0x8(%ebp),%eax
8010178c:	8b 40 04             	mov    0x4(%eax),%eax
8010178f:	8d 50 ff             	lea    -0x1(%eax),%edx
80101792:	8b 45 08             	mov    0x8(%ebp),%eax
80101795:	89 50 04             	mov    %edx,0x4(%eax)
80101798:	8b 45 08             	mov    0x8(%ebp),%eax
8010179b:	8b 40 04             	mov    0x4(%eax),%eax
8010179e:	85 c0                	test   %eax,%eax
801017a0:	7e 11                	jle    801017b3 <fileclose+0x52>
    release(&ftable.lock);
801017a2:	c7 04 24 a0 20 11 80 	movl   $0x801120a0,(%esp)
801017a9:	e8 02 40 00 00       	call   801057b0 <release>
    return;
801017ae:	e9 82 00 00 00       	jmp    80101835 <fileclose+0xd4>
  }
  ff = *f;
801017b3:	8b 45 08             	mov    0x8(%ebp),%eax
801017b6:	8b 10                	mov    (%eax),%edx
801017b8:	89 55 e0             	mov    %edx,-0x20(%ebp)
801017bb:	8b 50 04             	mov    0x4(%eax),%edx
801017be:	89 55 e4             	mov    %edx,-0x1c(%ebp)
801017c1:	8b 50 08             	mov    0x8(%eax),%edx
801017c4:	89 55 e8             	mov    %edx,-0x18(%ebp)
801017c7:	8b 50 0c             	mov    0xc(%eax),%edx
801017ca:	89 55 ec             	mov    %edx,-0x14(%ebp)
801017cd:	8b 50 10             	mov    0x10(%eax),%edx
801017d0:	89 55 f0             	mov    %edx,-0x10(%ebp)
801017d3:	8b 40 14             	mov    0x14(%eax),%eax
801017d6:	89 45 f4             	mov    %eax,-0xc(%ebp)
  f->ref = 0;
801017d9:	8b 45 08             	mov    0x8(%ebp),%eax
801017dc:	c7 40 04 00 00 00 00 	movl   $0x0,0x4(%eax)
  f->type = FD_NONE;
801017e3:	8b 45 08             	mov    0x8(%ebp),%eax
801017e6:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  release(&ftable.lock);
801017ec:	c7 04 24 a0 20 11 80 	movl   $0x801120a0,(%esp)
801017f3:	e8 b8 3f 00 00       	call   801057b0 <release>
  
  if(ff.type == FD_PIPE)
801017f8:	8b 45 e0             	mov    -0x20(%ebp),%eax
801017fb:	83 f8 01             	cmp    $0x1,%eax
801017fe:	75 18                	jne    80101818 <fileclose+0xb7>
    pipeclose(ff.pipe, ff.writable);
80101800:	0f b6 45 e9          	movzbl -0x17(%ebp),%eax
80101804:	0f be d0             	movsbl %al,%edx
80101807:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010180a:	89 54 24 04          	mov    %edx,0x4(%esp)
8010180e:	89 04 24             	mov    %eax,(%esp)
80101811:	e8 69 2f 00 00       	call   8010477f <pipeclose>
80101816:	eb 1d                	jmp    80101835 <fileclose+0xd4>
  else if(ff.type == FD_INODE){
80101818:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010181b:	83 f8 02             	cmp    $0x2,%eax
8010181e:	75 15                	jne    80101835 <fileclose+0xd4>
    begin_op();
80101820:	e8 45 22 00 00       	call   80103a6a <begin_op>
    iput(ff.ip);
80101825:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101828:	89 04 24             	mov    %eax,(%esp)
8010182b:	e8 fd 09 00 00       	call   8010222d <iput>
    end_op();
80101830:	e8 b7 22 00 00       	call   80103aec <end_op>
  }
}
80101835:	c9                   	leave  
80101836:	c3                   	ret    

80101837 <filestat>:

// Get metadata about file f.
int
filestat(struct file *f, struct stat *st)
{
80101837:	55                   	push   %ebp
80101838:	89 e5                	mov    %esp,%ebp
8010183a:	83 ec 18             	sub    $0x18,%esp
  if(f->type == FD_INODE){
8010183d:	8b 45 08             	mov    0x8(%ebp),%eax
80101840:	8b 00                	mov    (%eax),%eax
80101842:	83 f8 02             	cmp    $0x2,%eax
80101845:	75 38                	jne    8010187f <filestat+0x48>
    ilock(f->ip);
80101847:	8b 45 08             	mov    0x8(%ebp),%eax
8010184a:	8b 40 10             	mov    0x10(%eax),%eax
8010184d:	89 04 24             	mov    %eax,(%esp)
80101850:	e8 1c 08 00 00       	call   80102071 <ilock>
    stati(f->ip, st);
80101855:	8b 45 08             	mov    0x8(%ebp),%eax
80101858:	8b 40 10             	mov    0x10(%eax),%eax
8010185b:	8b 55 0c             	mov    0xc(%ebp),%edx
8010185e:	89 54 24 04          	mov    %edx,0x4(%esp)
80101862:	89 04 24             	mov    %eax,(%esp)
80101865:	e8 c1 0c 00 00       	call   8010252b <stati>
    iunlock(f->ip);
8010186a:	8b 45 08             	mov    0x8(%ebp),%eax
8010186d:	8b 40 10             	mov    0x10(%eax),%eax
80101870:	89 04 24             	mov    %eax,(%esp)
80101873:	e8 50 09 00 00       	call   801021c8 <iunlock>
    return 0;
80101878:	b8 00 00 00 00       	mov    $0x0,%eax
8010187d:	eb 05                	jmp    80101884 <filestat+0x4d>
  }
  return -1;
8010187f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80101884:	c9                   	leave  
80101885:	c3                   	ret    

80101886 <fileread>:

// Read from file f.
int
fileread(struct file *f, char *addr, int n)
{
80101886:	55                   	push   %ebp
80101887:	89 e5                	mov    %esp,%ebp
80101889:	83 ec 28             	sub    $0x28,%esp
  int r;

  if(f->readable == 0)
8010188c:	8b 45 08             	mov    0x8(%ebp),%eax
8010188f:	0f b6 40 08          	movzbl 0x8(%eax),%eax
80101893:	84 c0                	test   %al,%al
80101895:	75 0a                	jne    801018a1 <fileread+0x1b>
    return -1;
80101897:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010189c:	e9 9f 00 00 00       	jmp    80101940 <fileread+0xba>
  if(f->type == FD_PIPE)
801018a1:	8b 45 08             	mov    0x8(%ebp),%eax
801018a4:	8b 00                	mov    (%eax),%eax
801018a6:	83 f8 01             	cmp    $0x1,%eax
801018a9:	75 1e                	jne    801018c9 <fileread+0x43>
    return piperead(f->pipe, addr, n);
801018ab:	8b 45 08             	mov    0x8(%ebp),%eax
801018ae:	8b 40 0c             	mov    0xc(%eax),%eax
801018b1:	8b 55 10             	mov    0x10(%ebp),%edx
801018b4:	89 54 24 08          	mov    %edx,0x8(%esp)
801018b8:	8b 55 0c             	mov    0xc(%ebp),%edx
801018bb:	89 54 24 04          	mov    %edx,0x4(%esp)
801018bf:	89 04 24             	mov    %eax,(%esp)
801018c2:	e8 3a 30 00 00       	call   80104901 <piperead>
801018c7:	eb 77                	jmp    80101940 <fileread+0xba>
  if(f->type == FD_INODE){
801018c9:	8b 45 08             	mov    0x8(%ebp),%eax
801018cc:	8b 00                	mov    (%eax),%eax
801018ce:	83 f8 02             	cmp    $0x2,%eax
801018d1:	75 61                	jne    80101934 <fileread+0xae>
    ilock(f->ip);
801018d3:	8b 45 08             	mov    0x8(%ebp),%eax
801018d6:	8b 40 10             	mov    0x10(%eax),%eax
801018d9:	89 04 24             	mov    %eax,(%esp)
801018dc:	e8 90 07 00 00       	call   80102071 <ilock>
    if((r = readi(f->ip, addr, f->off, n)) > 0)
801018e1:	8b 4d 10             	mov    0x10(%ebp),%ecx
801018e4:	8b 45 08             	mov    0x8(%ebp),%eax
801018e7:	8b 50 14             	mov    0x14(%eax),%edx
801018ea:	8b 45 08             	mov    0x8(%ebp),%eax
801018ed:	8b 40 10             	mov    0x10(%eax),%eax
801018f0:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
801018f4:	89 54 24 08          	mov    %edx,0x8(%esp)
801018f8:	8b 55 0c             	mov    0xc(%ebp),%edx
801018fb:	89 54 24 04          	mov    %edx,0x4(%esp)
801018ff:	89 04 24             	mov    %eax,(%esp)
80101902:	e8 69 0c 00 00       	call   80102570 <readi>
80101907:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010190a:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010190e:	7e 11                	jle    80101921 <fileread+0x9b>
      f->off += r;
80101910:	8b 45 08             	mov    0x8(%ebp),%eax
80101913:	8b 50 14             	mov    0x14(%eax),%edx
80101916:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101919:	01 c2                	add    %eax,%edx
8010191b:	8b 45 08             	mov    0x8(%ebp),%eax
8010191e:	89 50 14             	mov    %edx,0x14(%eax)
    iunlock(f->ip);
80101921:	8b 45 08             	mov    0x8(%ebp),%eax
80101924:	8b 40 10             	mov    0x10(%eax),%eax
80101927:	89 04 24             	mov    %eax,(%esp)
8010192a:	e8 99 08 00 00       	call   801021c8 <iunlock>
    return r;
8010192f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101932:	eb 0c                	jmp    80101940 <fileread+0xba>
  }
  panic("fileread");
80101934:	c7 04 24 27 8f 10 80 	movl   $0x80108f27,(%esp)
8010193b:	e8 29 ef ff ff       	call   80100869 <panic>
}
80101940:	c9                   	leave  
80101941:	c3                   	ret    

80101942 <filewrite>:

//PAGEBREAK!
// Write to file f.
int
filewrite(struct file *f, char *addr, int n)
{
80101942:	55                   	push   %ebp
80101943:	89 e5                	mov    %esp,%ebp
80101945:	53                   	push   %ebx
80101946:	83 ec 24             	sub    $0x24,%esp
  int r;

  if(f->writable == 0)
80101949:	8b 45 08             	mov    0x8(%ebp),%eax
8010194c:	0f b6 40 09          	movzbl 0x9(%eax),%eax
80101950:	84 c0                	test   %al,%al
80101952:	75 0a                	jne    8010195e <filewrite+0x1c>
    return -1;
80101954:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101959:	e9 23 01 00 00       	jmp    80101a81 <filewrite+0x13f>
  if(f->type == FD_PIPE)
8010195e:	8b 45 08             	mov    0x8(%ebp),%eax
80101961:	8b 00                	mov    (%eax),%eax
80101963:	83 f8 01             	cmp    $0x1,%eax
80101966:	75 21                	jne    80101989 <filewrite+0x47>
    return pipewrite(f->pipe, addr, n);
80101968:	8b 45 08             	mov    0x8(%ebp),%eax
8010196b:	8b 40 0c             	mov    0xc(%eax),%eax
8010196e:	8b 55 10             	mov    0x10(%ebp),%edx
80101971:	89 54 24 08          	mov    %edx,0x8(%esp)
80101975:	8b 55 0c             	mov    0xc(%ebp),%edx
80101978:	89 54 24 04          	mov    %edx,0x4(%esp)
8010197c:	89 04 24             	mov    %eax,(%esp)
8010197f:	e8 8d 2e 00 00       	call   80104811 <pipewrite>
80101984:	e9 f8 00 00 00       	jmp    80101a81 <filewrite+0x13f>
  if(f->type == FD_INODE){
80101989:	8b 45 08             	mov    0x8(%ebp),%eax
8010198c:	8b 00                	mov    (%eax),%eax
8010198e:	83 f8 02             	cmp    $0x2,%eax
80101991:	0f 85 de 00 00 00    	jne    80101a75 <filewrite+0x133>
    // the maximum log transaction size, including
    // i-node, indirect block, allocation blocks,
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((LOGSIZE-1-1-2) / 2) * BSIZE;
80101997:	c7 45 ec 00 1a 00 00 	movl   $0x1a00,-0x14(%ebp)
    int i = 0;
8010199e:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
    while(i < n){
801019a5:	e9 a8 00 00 00       	jmp    80101a52 <filewrite+0x110>
      int n1 = n - i;
801019aa:	8b 45 f0             	mov    -0x10(%ebp),%eax
801019ad:	8b 55 10             	mov    0x10(%ebp),%edx
801019b0:	89 d1                	mov    %edx,%ecx
801019b2:	29 c1                	sub    %eax,%ecx
801019b4:	89 c8                	mov    %ecx,%eax
801019b6:	89 45 f4             	mov    %eax,-0xc(%ebp)
      if(n1 > max)
801019b9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801019bc:	3b 45 ec             	cmp    -0x14(%ebp),%eax
801019bf:	7e 06                	jle    801019c7 <filewrite+0x85>
        n1 = max;
801019c1:	8b 45 ec             	mov    -0x14(%ebp),%eax
801019c4:	89 45 f4             	mov    %eax,-0xc(%ebp)

      begin_op();
801019c7:	e8 9e 20 00 00       	call   80103a6a <begin_op>
      ilock(f->ip);
801019cc:	8b 45 08             	mov    0x8(%ebp),%eax
801019cf:	8b 40 10             	mov    0x10(%eax),%eax
801019d2:	89 04 24             	mov    %eax,(%esp)
801019d5:	e8 97 06 00 00       	call   80102071 <ilock>
      if ((r = writei(f->ip, addr + i, f->off, n1)) > 0)
801019da:	8b 5d f4             	mov    -0xc(%ebp),%ebx
801019dd:	8b 45 08             	mov    0x8(%ebp),%eax
801019e0:	8b 48 14             	mov    0x14(%eax),%ecx
801019e3:	8b 45 f0             	mov    -0x10(%ebp),%eax
801019e6:	89 c2                	mov    %eax,%edx
801019e8:	03 55 0c             	add    0xc(%ebp),%edx
801019eb:	8b 45 08             	mov    0x8(%ebp),%eax
801019ee:	8b 40 10             	mov    0x10(%eax),%eax
801019f1:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
801019f5:	89 4c 24 08          	mov    %ecx,0x8(%esp)
801019f9:	89 54 24 04          	mov    %edx,0x4(%esp)
801019fd:	89 04 24             	mov    %eax,(%esp)
80101a00:	e8 d7 0c 00 00       	call   801026dc <writei>
80101a05:	89 45 e8             	mov    %eax,-0x18(%ebp)
80101a08:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
80101a0c:	7e 11                	jle    80101a1f <filewrite+0xdd>
        f->off += r;
80101a0e:	8b 45 08             	mov    0x8(%ebp),%eax
80101a11:	8b 50 14             	mov    0x14(%eax),%edx
80101a14:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101a17:	01 c2                	add    %eax,%edx
80101a19:	8b 45 08             	mov    0x8(%ebp),%eax
80101a1c:	89 50 14             	mov    %edx,0x14(%eax)
      iunlock(f->ip);
80101a1f:	8b 45 08             	mov    0x8(%ebp),%eax
80101a22:	8b 40 10             	mov    0x10(%eax),%eax
80101a25:	89 04 24             	mov    %eax,(%esp)
80101a28:	e8 9b 07 00 00       	call   801021c8 <iunlock>
      end_op();
80101a2d:	e8 ba 20 00 00       	call   80103aec <end_op>

      if(r < 0)
80101a32:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
80101a36:	78 28                	js     80101a60 <filewrite+0x11e>
        break;
      if(r != n1)
80101a38:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101a3b:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80101a3e:	74 0c                	je     80101a4c <filewrite+0x10a>
        panic("short filewrite");
80101a40:	c7 04 24 30 8f 10 80 	movl   $0x80108f30,(%esp)
80101a47:	e8 1d ee ff ff       	call   80100869 <panic>
      i += r;
80101a4c:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101a4f:	01 45 f0             	add    %eax,-0x10(%ebp)
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((LOGSIZE-1-1-2) / 2) * BSIZE;
    int i = 0;
    while(i < n){
80101a52:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101a55:	3b 45 10             	cmp    0x10(%ebp),%eax
80101a58:	0f 8c 4c ff ff ff    	jl     801019aa <filewrite+0x68>
80101a5e:	eb 01                	jmp    80101a61 <filewrite+0x11f>
        f->off += r;
      iunlock(f->ip);
      end_op();

      if(r < 0)
        break;
80101a60:	90                   	nop
      if(r != n1)
        panic("short filewrite");
      i += r;
    }
    return i == n ? n : -1;
80101a61:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101a64:	3b 45 10             	cmp    0x10(%ebp),%eax
80101a67:	75 05                	jne    80101a6e <filewrite+0x12c>
80101a69:	8b 45 10             	mov    0x10(%ebp),%eax
80101a6c:	eb 05                	jmp    80101a73 <filewrite+0x131>
80101a6e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101a73:	eb 0c                	jmp    80101a81 <filewrite+0x13f>
  }
  panic("filewrite");
80101a75:	c7 04 24 40 8f 10 80 	movl   $0x80108f40,(%esp)
80101a7c:	e8 e8 ed ff ff       	call   80100869 <panic>
}
80101a81:	83 c4 24             	add    $0x24,%esp
80101a84:	5b                   	pop    %ebx
80101a85:	5d                   	pop    %ebp
80101a86:	c3                   	ret    
	...

80101a88 <readsb>:
struct superblock sb;

// Read the super block.
void
readsb(int dev, struct superblock *sb)
{
80101a88:	55                   	push   %ebp
80101a89:	89 e5                	mov    %esp,%ebp
80101a8b:	83 ec 28             	sub    $0x28,%esp
  struct buf *bp;

  bp = bread(dev, 1);
80101a8e:	8b 45 08             	mov    0x8(%ebp),%eax
80101a91:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80101a98:	00 
80101a99:	89 04 24             	mov    %eax,(%esp)
80101a9c:	e8 06 e7 ff ff       	call   801001a7 <bread>
80101aa1:	89 45 f4             	mov    %eax,-0xc(%ebp)
  memmove(sb, bp->data, sizeof(*sb));
80101aa4:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101aa7:	83 c0 18             	add    $0x18,%eax
80101aaa:	c7 44 24 08 1c 00 00 	movl   $0x1c,0x8(%esp)
80101ab1:	00 
80101ab2:	89 44 24 04          	mov    %eax,0x4(%esp)
80101ab6:	8b 45 0c             	mov    0xc(%ebp),%eax
80101ab9:	89 04 24             	mov    %eax,(%esp)
80101abc:	e8 bc 3f 00 00       	call   80105a7d <memmove>
  brelse(bp);
80101ac1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101ac4:	89 04 24             	mov    %eax,(%esp)
80101ac7:	e8 4c e7 ff ff       	call   80100218 <brelse>
}
80101acc:	c9                   	leave  
80101acd:	c3                   	ret    

80101ace <bzero>:

// Zero a block.
static void
bzero(int dev, int bno)
{
80101ace:	55                   	push   %ebp
80101acf:	89 e5                	mov    %esp,%ebp
80101ad1:	83 ec 28             	sub    $0x28,%esp
  struct buf *bp;

  bp = bread(dev, bno);
80101ad4:	8b 55 0c             	mov    0xc(%ebp),%edx
80101ad7:	8b 45 08             	mov    0x8(%ebp),%eax
80101ada:	89 54 24 04          	mov    %edx,0x4(%esp)
80101ade:	89 04 24             	mov    %eax,(%esp)
80101ae1:	e8 c1 e6 ff ff       	call   801001a7 <bread>
80101ae6:	89 45 f4             	mov    %eax,-0xc(%ebp)
  memset(bp->data, 0, BSIZE);
80101ae9:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101aec:	83 c0 18             	add    $0x18,%eax
80101aef:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
80101af6:	00 
80101af7:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80101afe:	00 
80101aff:	89 04 24             	mov    %eax,(%esp)
80101b02:	e8 a3 3e 00 00       	call   801059aa <memset>
  log_write(bp);
80101b07:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101b0a:	89 04 24             	mov    %eax,(%esp)
80101b0d:	e8 5e 21 00 00       	call   80103c70 <log_write>
  brelse(bp);
80101b12:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101b15:	89 04 24             	mov    %eax,(%esp)
80101b18:	e8 fb e6 ff ff       	call   80100218 <brelse>
}
80101b1d:	c9                   	leave  
80101b1e:	c3                   	ret    

80101b1f <balloc>:
// Blocks.

// Allocate a zeroed disk block.
static uint
balloc(uint dev)
{
80101b1f:	55                   	push   %ebp
80101b20:	89 e5                	mov    %esp,%ebp
80101b22:	53                   	push   %ebx
80101b23:	83 ec 24             	sub    $0x24,%esp
  int b, bi, m;
  struct buf *bp;

  bp = 0;
80101b26:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  for(b = 0; b < sb.size; b += BPB){
80101b2d:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
80101b34:	e9 16 01 00 00       	jmp    80101c4f <balloc+0x130>
    bp = bread(dev, BBLOCK(b, sb));
80101b39:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101b3c:	8d 90 ff 0f 00 00    	lea    0xfff(%eax),%edx
80101b42:	85 c0                	test   %eax,%eax
80101b44:	0f 48 c2             	cmovs  %edx,%eax
80101b47:	c1 f8 0c             	sar    $0xc,%eax
80101b4a:	89 c2                	mov    %eax,%edx
80101b4c:	a1 b8 2a 11 80       	mov    0x80112ab8,%eax
80101b51:	8d 04 02             	lea    (%edx,%eax,1),%eax
80101b54:	89 44 24 04          	mov    %eax,0x4(%esp)
80101b58:	8b 45 08             	mov    0x8(%ebp),%eax
80101b5b:	89 04 24             	mov    %eax,(%esp)
80101b5e:	e8 44 e6 ff ff       	call   801001a7 <bread>
80101b63:	89 45 f4             	mov    %eax,-0xc(%ebp)
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
80101b66:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
80101b6d:	e9 aa 00 00 00       	jmp    80101c1c <balloc+0xfd>
      m = 1 << (bi % 8);
80101b72:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101b75:	89 c2                	mov    %eax,%edx
80101b77:	c1 fa 1f             	sar    $0x1f,%edx
80101b7a:	c1 ea 1d             	shr    $0x1d,%edx
80101b7d:	01 d0                	add    %edx,%eax
80101b7f:	83 e0 07             	and    $0x7,%eax
80101b82:	29 d0                	sub    %edx,%eax
80101b84:	ba 01 00 00 00       	mov    $0x1,%edx
80101b89:	89 d3                	mov    %edx,%ebx
80101b8b:	89 c1                	mov    %eax,%ecx
80101b8d:	d3 e3                	shl    %cl,%ebx
80101b8f:	89 d8                	mov    %ebx,%eax
80101b91:	89 45 f0             	mov    %eax,-0x10(%ebp)
      if((bp->data[bi/8] & m) == 0){  // Is block free?
80101b94:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101b97:	8d 50 07             	lea    0x7(%eax),%edx
80101b9a:	85 c0                	test   %eax,%eax
80101b9c:	0f 48 c2             	cmovs  %edx,%eax
80101b9f:	c1 f8 03             	sar    $0x3,%eax
80101ba2:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101ba5:	0f b6 44 02 18       	movzbl 0x18(%edx,%eax,1),%eax
80101baa:	0f b6 c0             	movzbl %al,%eax
80101bad:	23 45 f0             	and    -0x10(%ebp),%eax
80101bb0:	85 c0                	test   %eax,%eax
80101bb2:	75 64                	jne    80101c18 <balloc+0xf9>
        bp->data[bi/8] |= m;  // Mark block in use.
80101bb4:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101bb7:	8d 50 07             	lea    0x7(%eax),%edx
80101bba:	85 c0                	test   %eax,%eax
80101bbc:	0f 48 c2             	cmovs  %edx,%eax
80101bbf:	c1 f8 03             	sar    $0x3,%eax
80101bc2:	89 c2                	mov    %eax,%edx
80101bc4:	8b 4d f4             	mov    -0xc(%ebp),%ecx
80101bc7:	0f b6 44 01 18       	movzbl 0x18(%ecx,%eax,1),%eax
80101bcc:	89 c1                	mov    %eax,%ecx
80101bce:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101bd1:	09 c8                	or     %ecx,%eax
80101bd3:	89 c1                	mov    %eax,%ecx
80101bd5:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101bd8:	88 4c 10 18          	mov    %cl,0x18(%eax,%edx,1)
        log_write(bp);
80101bdc:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101bdf:	89 04 24             	mov    %eax,(%esp)
80101be2:	e8 89 20 00 00       	call   80103c70 <log_write>
        brelse(bp);
80101be7:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101bea:	89 04 24             	mov    %eax,(%esp)
80101bed:	e8 26 e6 ff ff       	call   80100218 <brelse>
        bzero(dev, b + bi);
80101bf2:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101bf5:	8b 55 e8             	mov    -0x18(%ebp),%edx
80101bf8:	01 c2                	add    %eax,%edx
80101bfa:	8b 45 08             	mov    0x8(%ebp),%eax
80101bfd:	89 54 24 04          	mov    %edx,0x4(%esp)
80101c01:	89 04 24             	mov    %eax,(%esp)
80101c04:	e8 c5 fe ff ff       	call   80101ace <bzero>
        return b + bi;
80101c09:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101c0c:	8b 55 e8             	mov    -0x18(%ebp),%edx
80101c0f:	8d 04 02             	lea    (%edx,%eax,1),%eax
      }
    }
    brelse(bp);
  }
  panic("balloc: out of blocks");
}
80101c12:	83 c4 24             	add    $0x24,%esp
80101c15:	5b                   	pop    %ebx
80101c16:	5d                   	pop    %ebp
80101c17:	c3                   	ret    
  struct buf *bp;

  bp = 0;
  for(b = 0; b < sb.size; b += BPB){
    bp = bread(dev, BBLOCK(b, sb));
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
80101c18:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
80101c1c:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%ebp)
80101c23:	7f 18                	jg     80101c3d <balloc+0x11e>
80101c25:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101c28:	8b 55 e8             	mov    -0x18(%ebp),%edx
80101c2b:	8d 04 02             	lea    (%edx,%eax,1),%eax
80101c2e:	89 c2                	mov    %eax,%edx
80101c30:	a1 a0 2a 11 80       	mov    0x80112aa0,%eax
80101c35:	39 c2                	cmp    %eax,%edx
80101c37:	0f 82 35 ff ff ff    	jb     80101b72 <balloc+0x53>
        brelse(bp);
        bzero(dev, b + bi);
        return b + bi;
      }
    }
    brelse(bp);
80101c3d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101c40:	89 04 24             	mov    %eax,(%esp)
80101c43:	e8 d0 e5 ff ff       	call   80100218 <brelse>
{
  int b, bi, m;
  struct buf *bp;

  bp = 0;
  for(b = 0; b < sb.size; b += BPB){
80101c48:	81 45 e8 00 10 00 00 	addl   $0x1000,-0x18(%ebp)
80101c4f:	8b 55 e8             	mov    -0x18(%ebp),%edx
80101c52:	a1 a0 2a 11 80       	mov    0x80112aa0,%eax
80101c57:	39 c2                	cmp    %eax,%edx
80101c59:	0f 82 da fe ff ff    	jb     80101b39 <balloc+0x1a>
        return b + bi;
      }
    }
    brelse(bp);
  }
  panic("balloc: out of blocks");
80101c5f:	c7 04 24 4c 8f 10 80 	movl   $0x80108f4c,(%esp)
80101c66:	e8 fe eb ff ff       	call   80100869 <panic>

80101c6b <bfree>:
}

// Free a disk block.
static void
bfree(int dev, uint b)
{
80101c6b:	55                   	push   %ebp
80101c6c:	89 e5                	mov    %esp,%ebp
80101c6e:	53                   	push   %ebx
80101c6f:	83 ec 24             	sub    $0x24,%esp
  struct buf *bp;
  int bi, m;

  readsb(dev, &sb);
80101c72:	c7 44 24 04 a0 2a 11 	movl   $0x80112aa0,0x4(%esp)
80101c79:	80 
80101c7a:	8b 45 08             	mov    0x8(%ebp),%eax
80101c7d:	89 04 24             	mov    %eax,(%esp)
80101c80:	e8 03 fe ff ff       	call   80101a88 <readsb>
  bp = bread(dev, BBLOCK(b, sb));
80101c85:	8b 45 0c             	mov    0xc(%ebp),%eax
80101c88:	89 c2                	mov    %eax,%edx
80101c8a:	c1 ea 0c             	shr    $0xc,%edx
80101c8d:	a1 b8 2a 11 80       	mov    0x80112ab8,%eax
80101c92:	01 c2                	add    %eax,%edx
80101c94:	8b 45 08             	mov    0x8(%ebp),%eax
80101c97:	89 54 24 04          	mov    %edx,0x4(%esp)
80101c9b:	89 04 24             	mov    %eax,(%esp)
80101c9e:	e8 04 e5 ff ff       	call   801001a7 <bread>
80101ca3:	89 45 ec             	mov    %eax,-0x14(%ebp)
  bi = b % BPB;
80101ca6:	8b 45 0c             	mov    0xc(%ebp),%eax
80101ca9:	25 ff 0f 00 00       	and    $0xfff,%eax
80101cae:	89 45 f0             	mov    %eax,-0x10(%ebp)
  m = 1 << (bi % 8);
80101cb1:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101cb4:	89 c2                	mov    %eax,%edx
80101cb6:	c1 fa 1f             	sar    $0x1f,%edx
80101cb9:	c1 ea 1d             	shr    $0x1d,%edx
80101cbc:	01 d0                	add    %edx,%eax
80101cbe:	83 e0 07             	and    $0x7,%eax
80101cc1:	29 d0                	sub    %edx,%eax
80101cc3:	ba 01 00 00 00       	mov    $0x1,%edx
80101cc8:	89 d3                	mov    %edx,%ebx
80101cca:	89 c1                	mov    %eax,%ecx
80101ccc:	d3 e3                	shl    %cl,%ebx
80101cce:	89 d8                	mov    %ebx,%eax
80101cd0:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((bp->data[bi/8] & m) == 0)
80101cd3:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101cd6:	8d 50 07             	lea    0x7(%eax),%edx
80101cd9:	85 c0                	test   %eax,%eax
80101cdb:	0f 48 c2             	cmovs  %edx,%eax
80101cde:	c1 f8 03             	sar    $0x3,%eax
80101ce1:	8b 55 ec             	mov    -0x14(%ebp),%edx
80101ce4:	0f b6 44 02 18       	movzbl 0x18(%edx,%eax,1),%eax
80101ce9:	0f b6 c0             	movzbl %al,%eax
80101cec:	23 45 f4             	and    -0xc(%ebp),%eax
80101cef:	85 c0                	test   %eax,%eax
80101cf1:	75 0c                	jne    80101cff <bfree+0x94>
    panic("freeing free block");
80101cf3:	c7 04 24 62 8f 10 80 	movl   $0x80108f62,(%esp)
80101cfa:	e8 6a eb ff ff       	call   80100869 <panic>
  bp->data[bi/8] &= ~m;
80101cff:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101d02:	8d 50 07             	lea    0x7(%eax),%edx
80101d05:	85 c0                	test   %eax,%eax
80101d07:	0f 48 c2             	cmovs  %edx,%eax
80101d0a:	c1 f8 03             	sar    $0x3,%eax
80101d0d:	89 c2                	mov    %eax,%edx
80101d0f:	8b 4d ec             	mov    -0x14(%ebp),%ecx
80101d12:	0f b6 44 01 18       	movzbl 0x18(%ecx,%eax,1),%eax
80101d17:	8b 4d f4             	mov    -0xc(%ebp),%ecx
80101d1a:	f7 d1                	not    %ecx
80101d1c:	21 c8                	and    %ecx,%eax
80101d1e:	89 c1                	mov    %eax,%ecx
80101d20:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101d23:	88 4c 10 18          	mov    %cl,0x18(%eax,%edx,1)
  log_write(bp);
80101d27:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101d2a:	89 04 24             	mov    %eax,(%esp)
80101d2d:	e8 3e 1f 00 00       	call   80103c70 <log_write>
  brelse(bp);
80101d32:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101d35:	89 04 24             	mov    %eax,(%esp)
80101d38:	e8 db e4 ff ff       	call   80100218 <brelse>
}
80101d3d:	83 c4 24             	add    $0x24,%esp
80101d40:	5b                   	pop    %ebx
80101d41:	5d                   	pop    %ebp
80101d42:	c3                   	ret    

80101d43 <iinit>:
  struct inode inode[NINODE];
} icache;

void
iinit(int dev)
{
80101d43:	55                   	push   %ebp
80101d44:	89 e5                	mov    %esp,%ebp
80101d46:	57                   	push   %edi
80101d47:	56                   	push   %esi
80101d48:	53                   	push   %ebx
80101d49:	83 ec 4c             	sub    $0x4c,%esp
  initlock(&icache.lock, "icache");
80101d4c:	c7 44 24 04 75 8f 10 	movl   $0x80108f75,0x4(%esp)
80101d53:	80 
80101d54:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
80101d5b:	e8 c6 39 00 00       	call   80105726 <initlock>
  readsb(dev, &sb);
80101d60:	c7 44 24 04 a0 2a 11 	movl   $0x80112aa0,0x4(%esp)
80101d67:	80 
80101d68:	8b 45 08             	mov    0x8(%ebp),%eax
80101d6b:	89 04 24             	mov    %eax,(%esp)
80101d6e:	e8 15 fd ff ff       	call   80101a88 <readsb>
  cprintf(
80101d73:	a1 b8 2a 11 80       	mov    0x80112ab8,%eax
80101d78:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80101d7b:	8b 3d b4 2a 11 80    	mov    0x80112ab4,%edi
80101d81:	8b 35 b0 2a 11 80    	mov    0x80112ab0,%esi
80101d87:	8b 1d ac 2a 11 80    	mov    0x80112aac,%ebx
80101d8d:	8b 0d a8 2a 11 80    	mov    0x80112aa8,%ecx
80101d93:	8b 15 a4 2a 11 80    	mov    0x80112aa4,%edx
80101d99:	a1 a0 2a 11 80       	mov    0x80112aa0,%eax
80101d9e:	89 45 d4             	mov    %eax,-0x2c(%ebp)
80101da1:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101da4:	89 44 24 1c          	mov    %eax,0x1c(%esp)
80101da8:	89 7c 24 18          	mov    %edi,0x18(%esp)
80101dac:	89 74 24 14          	mov    %esi,0x14(%esp)
80101db0:	89 5c 24 10          	mov    %ebx,0x10(%esp)
80101db4:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
80101db8:	89 54 24 08          	mov    %edx,0x8(%esp)
80101dbc:	8b 45 d4             	mov    -0x2c(%ebp),%eax
80101dbf:	89 44 24 04          	mov    %eax,0x4(%esp)
80101dc3:	c7 04 24 7c 8f 10 80 	movl   $0x80108f7c,(%esp)
80101dca:	e8 fa e8 ff ff       	call   801006c9 <cprintf>
    "sb: size %d nblocks %d ninodes %d nlog %d logstart %d "
    "inodestart %d bmap start %d\n", sb.size, sb.nblocks, sb.ninodes,
    sb.nlog, sb.logstart, sb.inodestart, sb.bmapstart);
}
80101dcf:	83 c4 4c             	add    $0x4c,%esp
80101dd2:	5b                   	pop    %ebx
80101dd3:	5e                   	pop    %esi
80101dd4:	5f                   	pop    %edi
80101dd5:	5d                   	pop    %ebp
80101dd6:	c3                   	ret    

80101dd7 <ialloc>:
//PAGEBREAK!
// Allocate a new inode with the given type on device dev.
// A free inode has a type of zero.
struct inode*
ialloc(uint dev, short type)
{
80101dd7:	55                   	push   %ebp
80101dd8:	89 e5                	mov    %esp,%ebp
80101dda:	83 ec 38             	sub    $0x38,%esp
80101ddd:	8b 45 0c             	mov    0xc(%ebp),%eax
80101de0:	66 89 45 e4          	mov    %ax,-0x1c(%ebp)
  int inum;
  struct buf *bp;
  struct dinode *dip;

  for(inum = 1; inum < sb.ninodes; inum++){
80101de4:	c7 45 ec 01 00 00 00 	movl   $0x1,-0x14(%ebp)
80101deb:	e9 9f 00 00 00       	jmp    80101e8f <ialloc+0xb8>
    bp = bread(dev, IBLOCK(inum, sb));
80101df0:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101df3:	89 c2                	mov    %eax,%edx
80101df5:	c1 ea 03             	shr    $0x3,%edx
80101df8:	a1 b4 2a 11 80       	mov    0x80112ab4,%eax
80101dfd:	8d 04 02             	lea    (%edx,%eax,1),%eax
80101e00:	89 44 24 04          	mov    %eax,0x4(%esp)
80101e04:	8b 45 08             	mov    0x8(%ebp),%eax
80101e07:	89 04 24             	mov    %eax,(%esp)
80101e0a:	e8 98 e3 ff ff       	call   801001a7 <bread>
80101e0f:	89 45 f0             	mov    %eax,-0x10(%ebp)
    dip = (struct dinode*)bp->data + inum%IPB;
80101e12:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101e15:	83 c0 18             	add    $0x18,%eax
80101e18:	8b 55 ec             	mov    -0x14(%ebp),%edx
80101e1b:	83 e2 07             	and    $0x7,%edx
80101e1e:	c1 e2 06             	shl    $0x6,%edx
80101e21:	01 d0                	add    %edx,%eax
80101e23:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(dip->type == 0){  // a free inode
80101e26:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101e29:	0f b7 00             	movzwl (%eax),%eax
80101e2c:	66 85 c0             	test   %ax,%ax
80101e2f:	75 4f                	jne    80101e80 <ialloc+0xa9>
      memset(dip, 0, sizeof(*dip));
80101e31:	c7 44 24 08 40 00 00 	movl   $0x40,0x8(%esp)
80101e38:	00 
80101e39:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80101e40:	00 
80101e41:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101e44:	89 04 24             	mov    %eax,(%esp)
80101e47:	e8 5e 3b 00 00       	call   801059aa <memset>
      dip->type = type;
80101e4c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101e4f:	0f b7 55 e4          	movzwl -0x1c(%ebp),%edx
80101e53:	66 89 10             	mov    %dx,(%eax)
      log_write(bp);   // mark it allocated on the disk
80101e56:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101e59:	89 04 24             	mov    %eax,(%esp)
80101e5c:	e8 0f 1e 00 00       	call   80103c70 <log_write>
      brelse(bp);
80101e61:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101e64:	89 04 24             	mov    %eax,(%esp)
80101e67:	e8 ac e3 ff ff       	call   80100218 <brelse>
      return iget(dev, inum);
80101e6c:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101e6f:	89 44 24 04          	mov    %eax,0x4(%esp)
80101e73:	8b 45 08             	mov    0x8(%ebp),%eax
80101e76:	89 04 24             	mov    %eax,(%esp)
80101e79:	e8 ee 00 00 00       	call   80101f6c <iget>
    }
    brelse(bp);
  }
  panic("ialloc: no inodes");
}
80101e7e:	c9                   	leave  
80101e7f:	c3                   	ret    
      dip->type = type;
      log_write(bp);   // mark it allocated on the disk
      brelse(bp);
      return iget(dev, inum);
    }
    brelse(bp);
80101e80:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101e83:	89 04 24             	mov    %eax,(%esp)
80101e86:	e8 8d e3 ff ff       	call   80100218 <brelse>
{
  int inum;
  struct buf *bp;
  struct dinode *dip;

  for(inum = 1; inum < sb.ninodes; inum++){
80101e8b:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
80101e8f:	8b 55 ec             	mov    -0x14(%ebp),%edx
80101e92:	a1 a8 2a 11 80       	mov    0x80112aa8,%eax
80101e97:	39 c2                	cmp    %eax,%edx
80101e99:	0f 82 51 ff ff ff    	jb     80101df0 <ialloc+0x19>
      brelse(bp);
      return iget(dev, inum);
    }
    brelse(bp);
  }
  panic("ialloc: no inodes");
80101e9f:	c7 04 24 cf 8f 10 80 	movl   $0x80108fcf,(%esp)
80101ea6:	e8 be e9 ff ff       	call   80100869 <panic>

80101eab <iupdate>:
}

// Copy a modified in-memory inode to disk.
void
iupdate(struct inode *ip)
{
80101eab:	55                   	push   %ebp
80101eac:	89 e5                	mov    %esp,%ebp
80101eae:	83 ec 28             	sub    $0x28,%esp
  struct buf *bp;
  struct dinode *dip;

  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
80101eb1:	8b 45 08             	mov    0x8(%ebp),%eax
80101eb4:	8b 40 04             	mov    0x4(%eax),%eax
80101eb7:	89 c2                	mov    %eax,%edx
80101eb9:	c1 ea 03             	shr    $0x3,%edx
80101ebc:	a1 b4 2a 11 80       	mov    0x80112ab4,%eax
80101ec1:	01 c2                	add    %eax,%edx
80101ec3:	8b 45 08             	mov    0x8(%ebp),%eax
80101ec6:	8b 00                	mov    (%eax),%eax
80101ec8:	89 54 24 04          	mov    %edx,0x4(%esp)
80101ecc:	89 04 24             	mov    %eax,(%esp)
80101ecf:	e8 d3 e2 ff ff       	call   801001a7 <bread>
80101ed4:	89 45 f0             	mov    %eax,-0x10(%ebp)
  dip = (struct dinode*)bp->data + ip->inum%IPB;
80101ed7:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101eda:	83 c0 18             	add    $0x18,%eax
80101edd:	89 c2                	mov    %eax,%edx
80101edf:	8b 45 08             	mov    0x8(%ebp),%eax
80101ee2:	8b 40 04             	mov    0x4(%eax),%eax
80101ee5:	83 e0 07             	and    $0x7,%eax
80101ee8:	c1 e0 06             	shl    $0x6,%eax
80101eeb:	8d 04 02             	lea    (%edx,%eax,1),%eax
80101eee:	89 45 f4             	mov    %eax,-0xc(%ebp)
  dip->type = ip->type;
80101ef1:	8b 45 08             	mov    0x8(%ebp),%eax
80101ef4:	0f b7 50 10          	movzwl 0x10(%eax),%edx
80101ef8:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101efb:	66 89 10             	mov    %dx,(%eax)
  dip->major = ip->major;
80101efe:	8b 45 08             	mov    0x8(%ebp),%eax
80101f01:	0f b7 50 12          	movzwl 0x12(%eax),%edx
80101f05:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101f08:	66 89 50 02          	mov    %dx,0x2(%eax)
  dip->minor = ip->minor;
80101f0c:	8b 45 08             	mov    0x8(%ebp),%eax
80101f0f:	0f b7 50 14          	movzwl 0x14(%eax),%edx
80101f13:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101f16:	66 89 50 04          	mov    %dx,0x4(%eax)
  dip->nlink = ip->nlink;
80101f1a:	8b 45 08             	mov    0x8(%ebp),%eax
80101f1d:	0f b7 50 16          	movzwl 0x16(%eax),%edx
80101f21:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101f24:	66 89 50 06          	mov    %dx,0x6(%eax)
  dip->size = ip->size;
80101f28:	8b 45 08             	mov    0x8(%ebp),%eax
80101f2b:	8b 50 18             	mov    0x18(%eax),%edx
80101f2e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101f31:	89 50 08             	mov    %edx,0x8(%eax)
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
80101f34:	8b 45 08             	mov    0x8(%ebp),%eax
80101f37:	8d 50 1c             	lea    0x1c(%eax),%edx
80101f3a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101f3d:	83 c0 0c             	add    $0xc,%eax
80101f40:	c7 44 24 08 34 00 00 	movl   $0x34,0x8(%esp)
80101f47:	00 
80101f48:	89 54 24 04          	mov    %edx,0x4(%esp)
80101f4c:	89 04 24             	mov    %eax,(%esp)
80101f4f:	e8 29 3b 00 00       	call   80105a7d <memmove>
  log_write(bp);
80101f54:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101f57:	89 04 24             	mov    %eax,(%esp)
80101f5a:	e8 11 1d 00 00       	call   80103c70 <log_write>
  brelse(bp);
80101f5f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101f62:	89 04 24             	mov    %eax,(%esp)
80101f65:	e8 ae e2 ff ff       	call   80100218 <brelse>
}
80101f6a:	c9                   	leave  
80101f6b:	c3                   	ret    

80101f6c <iget>:
// Find the inode with number inum on device dev
// and return the in-memory copy. Does not lock
// the inode and does not read it from disk.
static struct inode*
iget(uint dev, uint inum)
{
80101f6c:	55                   	push   %ebp
80101f6d:	89 e5                	mov    %esp,%ebp
80101f6f:	83 ec 28             	sub    $0x28,%esp
  struct inode *ip, *empty;

  acquire(&icache.lock);
80101f72:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
80101f79:	e8 c9 37 00 00       	call   80105747 <acquire>

  // Is the inode already cached?
  empty = 0;
80101f7e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
80101f85:	c7 45 f0 f4 2a 11 80 	movl   $0x80112af4,-0x10(%ebp)
80101f8c:	eb 59                	jmp    80101fe7 <iget+0x7b>
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
80101f8e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101f91:	8b 40 08             	mov    0x8(%eax),%eax
80101f94:	85 c0                	test   %eax,%eax
80101f96:	7e 35                	jle    80101fcd <iget+0x61>
80101f98:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101f9b:	8b 00                	mov    (%eax),%eax
80101f9d:	3b 45 08             	cmp    0x8(%ebp),%eax
80101fa0:	75 2b                	jne    80101fcd <iget+0x61>
80101fa2:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101fa5:	8b 40 04             	mov    0x4(%eax),%eax
80101fa8:	3b 45 0c             	cmp    0xc(%ebp),%eax
80101fab:	75 20                	jne    80101fcd <iget+0x61>
      ip->ref++;
80101fad:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101fb0:	8b 40 08             	mov    0x8(%eax),%eax
80101fb3:	8d 50 01             	lea    0x1(%eax),%edx
80101fb6:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101fb9:	89 50 08             	mov    %edx,0x8(%eax)
      release(&icache.lock);
80101fbc:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
80101fc3:	e8 e8 37 00 00       	call   801057b0 <release>
      return ip;
80101fc8:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101fcb:	eb 70                	jmp    8010203d <iget+0xd1>
    }
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
80101fcd:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80101fd1:	75 10                	jne    80101fe3 <iget+0x77>
80101fd3:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101fd6:	8b 40 08             	mov    0x8(%eax),%eax
80101fd9:	85 c0                	test   %eax,%eax
80101fdb:	75 06                	jne    80101fe3 <iget+0x77>
      empty = ip;
80101fdd:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101fe0:	89 45 f4             	mov    %eax,-0xc(%ebp)

  acquire(&icache.lock);

  // Is the inode already cached?
  empty = 0;
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
80101fe3:	83 45 f0 50          	addl   $0x50,-0x10(%ebp)
80101fe7:	b8 94 3a 11 80       	mov    $0x80113a94,%eax
80101fec:	39 45 f0             	cmp    %eax,-0x10(%ebp)
80101fef:	72 9d                	jb     80101f8e <iget+0x22>
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
      empty = ip;
  }

  // Recycle an inode cache entry.
  if(empty == 0)
80101ff1:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80101ff5:	75 0c                	jne    80102003 <iget+0x97>
    panic("iget: no inodes");
80101ff7:	c7 04 24 e1 8f 10 80 	movl   $0x80108fe1,(%esp)
80101ffe:	e8 66 e8 ff ff       	call   80100869 <panic>

  ip = empty;
80102003:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102006:	89 45 f0             	mov    %eax,-0x10(%ebp)
  ip->dev = dev;
80102009:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010200c:	8b 55 08             	mov    0x8(%ebp),%edx
8010200f:	89 10                	mov    %edx,(%eax)
  ip->inum = inum;
80102011:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102014:	8b 55 0c             	mov    0xc(%ebp),%edx
80102017:	89 50 04             	mov    %edx,0x4(%eax)
  ip->ref = 1;
8010201a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010201d:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)
  ip->flags = 0;
80102024:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102027:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
  release(&icache.lock);
8010202e:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
80102035:	e8 76 37 00 00       	call   801057b0 <release>

  return ip;
8010203a:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
8010203d:	c9                   	leave  
8010203e:	c3                   	ret    

8010203f <idup>:

// Increment reference count for ip.
// Returns ip to enable ip = idup(ip1) idiom.
struct inode*
idup(struct inode *ip)
{
8010203f:	55                   	push   %ebp
80102040:	89 e5                	mov    %esp,%ebp
80102042:	83 ec 18             	sub    $0x18,%esp
  acquire(&icache.lock);
80102045:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
8010204c:	e8 f6 36 00 00       	call   80105747 <acquire>
  ip->ref++;
80102051:	8b 45 08             	mov    0x8(%ebp),%eax
80102054:	8b 40 08             	mov    0x8(%eax),%eax
80102057:	8d 50 01             	lea    0x1(%eax),%edx
8010205a:	8b 45 08             	mov    0x8(%ebp),%eax
8010205d:	89 50 08             	mov    %edx,0x8(%eax)
  release(&icache.lock);
80102060:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
80102067:	e8 44 37 00 00       	call   801057b0 <release>
  return ip;
8010206c:	8b 45 08             	mov    0x8(%ebp),%eax
}
8010206f:	c9                   	leave  
80102070:	c3                   	ret    

80102071 <ilock>:

// Lock the given inode.
// Reads the inode from disk if necessary.
void
ilock(struct inode *ip)
{
80102071:	55                   	push   %ebp
80102072:	89 e5                	mov    %esp,%ebp
80102074:	83 ec 28             	sub    $0x28,%esp
  struct buf *bp;
  struct dinode *dip;

  if(ip == 0 || ip->ref < 1)
80102077:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
8010207b:	74 0a                	je     80102087 <ilock+0x16>
8010207d:	8b 45 08             	mov    0x8(%ebp),%eax
80102080:	8b 40 08             	mov    0x8(%eax),%eax
80102083:	85 c0                	test   %eax,%eax
80102085:	7f 0c                	jg     80102093 <ilock+0x22>
    panic("ilock");
80102087:	c7 04 24 f1 8f 10 80 	movl   $0x80108ff1,(%esp)
8010208e:	e8 d6 e7 ff ff       	call   80100869 <panic>

  acquire(&icache.lock);
80102093:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
8010209a:	e8 a8 36 00 00       	call   80105747 <acquire>
  while(ip->flags & I_BUSY)
8010209f:	eb 13                	jmp    801020b4 <ilock+0x43>
    sleep(ip, &icache.lock);
801020a1:	c7 44 24 04 c0 2a 11 	movl   $0x80112ac0,0x4(%esp)
801020a8:	80 
801020a9:	8b 45 08             	mov    0x8(%ebp),%eax
801020ac:	89 04 24             	mov    %eax,(%esp)
801020af:	e8 6a 33 00 00       	call   8010541e <sleep>

  if(ip == 0 || ip->ref < 1)
    panic("ilock");

  acquire(&icache.lock);
  while(ip->flags & I_BUSY)
801020b4:	8b 45 08             	mov    0x8(%ebp),%eax
801020b7:	8b 40 0c             	mov    0xc(%eax),%eax
801020ba:	83 e0 01             	and    $0x1,%eax
801020bd:	84 c0                	test   %al,%al
801020bf:	75 e0                	jne    801020a1 <ilock+0x30>
    sleep(ip, &icache.lock);
  ip->flags |= I_BUSY;
801020c1:	8b 45 08             	mov    0x8(%ebp),%eax
801020c4:	8b 40 0c             	mov    0xc(%eax),%eax
801020c7:	89 c2                	mov    %eax,%edx
801020c9:	83 ca 01             	or     $0x1,%edx
801020cc:	8b 45 08             	mov    0x8(%ebp),%eax
801020cf:	89 50 0c             	mov    %edx,0xc(%eax)
  release(&icache.lock);
801020d2:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
801020d9:	e8 d2 36 00 00       	call   801057b0 <release>

  if(!(ip->flags & I_VALID)){
801020de:	8b 45 08             	mov    0x8(%ebp),%eax
801020e1:	8b 40 0c             	mov    0xc(%eax),%eax
801020e4:	83 e0 02             	and    $0x2,%eax
801020e7:	85 c0                	test   %eax,%eax
801020e9:	0f 85 d7 00 00 00    	jne    801021c6 <ilock+0x155>
    bp = bread(ip->dev, IBLOCK(ip->inum, sb));
801020ef:	8b 45 08             	mov    0x8(%ebp),%eax
801020f2:	8b 40 04             	mov    0x4(%eax),%eax
801020f5:	89 c2                	mov    %eax,%edx
801020f7:	c1 ea 03             	shr    $0x3,%edx
801020fa:	a1 b4 2a 11 80       	mov    0x80112ab4,%eax
801020ff:	01 c2                	add    %eax,%edx
80102101:	8b 45 08             	mov    0x8(%ebp),%eax
80102104:	8b 00                	mov    (%eax),%eax
80102106:	89 54 24 04          	mov    %edx,0x4(%esp)
8010210a:	89 04 24             	mov    %eax,(%esp)
8010210d:	e8 95 e0 ff ff       	call   801001a7 <bread>
80102112:	89 45 f0             	mov    %eax,-0x10(%ebp)
    dip = (struct dinode*)bp->data + ip->inum%IPB;
80102115:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102118:	83 c0 18             	add    $0x18,%eax
8010211b:	89 c2                	mov    %eax,%edx
8010211d:	8b 45 08             	mov    0x8(%ebp),%eax
80102120:	8b 40 04             	mov    0x4(%eax),%eax
80102123:	83 e0 07             	and    $0x7,%eax
80102126:	c1 e0 06             	shl    $0x6,%eax
80102129:	8d 04 02             	lea    (%edx,%eax,1),%eax
8010212c:	89 45 f4             	mov    %eax,-0xc(%ebp)
    ip->type = dip->type;
8010212f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102132:	0f b7 10             	movzwl (%eax),%edx
80102135:	8b 45 08             	mov    0x8(%ebp),%eax
80102138:	66 89 50 10          	mov    %dx,0x10(%eax)
    ip->major = dip->major;
8010213c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010213f:	0f b7 50 02          	movzwl 0x2(%eax),%edx
80102143:	8b 45 08             	mov    0x8(%ebp),%eax
80102146:	66 89 50 12          	mov    %dx,0x12(%eax)
    ip->minor = dip->minor;
8010214a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010214d:	0f b7 50 04          	movzwl 0x4(%eax),%edx
80102151:	8b 45 08             	mov    0x8(%ebp),%eax
80102154:	66 89 50 14          	mov    %dx,0x14(%eax)
    ip->nlink = dip->nlink;
80102158:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010215b:	0f b7 50 06          	movzwl 0x6(%eax),%edx
8010215f:	8b 45 08             	mov    0x8(%ebp),%eax
80102162:	66 89 50 16          	mov    %dx,0x16(%eax)
    ip->size = dip->size;
80102166:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102169:	8b 50 08             	mov    0x8(%eax),%edx
8010216c:	8b 45 08             	mov    0x8(%ebp),%eax
8010216f:	89 50 18             	mov    %edx,0x18(%eax)
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
80102172:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102175:	8d 50 0c             	lea    0xc(%eax),%edx
80102178:	8b 45 08             	mov    0x8(%ebp),%eax
8010217b:	83 c0 1c             	add    $0x1c,%eax
8010217e:	c7 44 24 08 34 00 00 	movl   $0x34,0x8(%esp)
80102185:	00 
80102186:	89 54 24 04          	mov    %edx,0x4(%esp)
8010218a:	89 04 24             	mov    %eax,(%esp)
8010218d:	e8 eb 38 00 00       	call   80105a7d <memmove>
    brelse(bp);
80102192:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102195:	89 04 24             	mov    %eax,(%esp)
80102198:	e8 7b e0 ff ff       	call   80100218 <brelse>
    ip->flags |= I_VALID;
8010219d:	8b 45 08             	mov    0x8(%ebp),%eax
801021a0:	8b 40 0c             	mov    0xc(%eax),%eax
801021a3:	89 c2                	mov    %eax,%edx
801021a5:	83 ca 02             	or     $0x2,%edx
801021a8:	8b 45 08             	mov    0x8(%ebp),%eax
801021ab:	89 50 0c             	mov    %edx,0xc(%eax)
    if(ip->type == 0)
801021ae:	8b 45 08             	mov    0x8(%ebp),%eax
801021b1:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801021b5:	66 85 c0             	test   %ax,%ax
801021b8:	75 0c                	jne    801021c6 <ilock+0x155>
      panic("ilock: no type");
801021ba:	c7 04 24 f7 8f 10 80 	movl   $0x80108ff7,(%esp)
801021c1:	e8 a3 e6 ff ff       	call   80100869 <panic>
  }
}
801021c6:	c9                   	leave  
801021c7:	c3                   	ret    

801021c8 <iunlock>:

// Unlock the given inode.
void
iunlock(struct inode *ip)
{
801021c8:	55                   	push   %ebp
801021c9:	89 e5                	mov    %esp,%ebp
801021cb:	83 ec 18             	sub    $0x18,%esp
  if(ip == 0 || !(ip->flags & I_BUSY) || ip->ref < 1)
801021ce:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
801021d2:	74 17                	je     801021eb <iunlock+0x23>
801021d4:	8b 45 08             	mov    0x8(%ebp),%eax
801021d7:	8b 40 0c             	mov    0xc(%eax),%eax
801021da:	83 e0 01             	and    $0x1,%eax
801021dd:	85 c0                	test   %eax,%eax
801021df:	74 0a                	je     801021eb <iunlock+0x23>
801021e1:	8b 45 08             	mov    0x8(%ebp),%eax
801021e4:	8b 40 08             	mov    0x8(%eax),%eax
801021e7:	85 c0                	test   %eax,%eax
801021e9:	7f 0c                	jg     801021f7 <iunlock+0x2f>
    panic("iunlock");
801021eb:	c7 04 24 06 90 10 80 	movl   $0x80109006,(%esp)
801021f2:	e8 72 e6 ff ff       	call   80100869 <panic>

  acquire(&icache.lock);
801021f7:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
801021fe:	e8 44 35 00 00       	call   80105747 <acquire>
  ip->flags &= ~I_BUSY;
80102203:	8b 45 08             	mov    0x8(%ebp),%eax
80102206:	8b 40 0c             	mov    0xc(%eax),%eax
80102209:	89 c2                	mov    %eax,%edx
8010220b:	83 e2 fe             	and    $0xfffffffe,%edx
8010220e:	8b 45 08             	mov    0x8(%ebp),%eax
80102211:	89 50 0c             	mov    %edx,0xc(%eax)
  wakeup(ip);
80102214:	8b 45 08             	mov    0x8(%ebp),%eax
80102217:	89 04 24             	mov    %eax,(%esp)
8010221a:	e8 dc 32 00 00       	call   801054fb <wakeup>
  release(&icache.lock);
8010221f:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
80102226:	e8 85 35 00 00       	call   801057b0 <release>
}
8010222b:	c9                   	leave  
8010222c:	c3                   	ret    

8010222d <iput>:
// to it, free the inode (and its content) on disk.
// All calls to iput() must be inside a transaction in
// case it has to free the inode.
void
iput(struct inode *ip)
{
8010222d:	55                   	push   %ebp
8010222e:	89 e5                	mov    %esp,%ebp
80102230:	83 ec 18             	sub    $0x18,%esp
  acquire(&icache.lock);
80102233:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
8010223a:	e8 08 35 00 00       	call   80105747 <acquire>
  if(ip->ref == 1 && (ip->flags & I_VALID) && ip->nlink == 0){
8010223f:	8b 45 08             	mov    0x8(%ebp),%eax
80102242:	8b 40 08             	mov    0x8(%eax),%eax
80102245:	83 f8 01             	cmp    $0x1,%eax
80102248:	0f 85 93 00 00 00    	jne    801022e1 <iput+0xb4>
8010224e:	8b 45 08             	mov    0x8(%ebp),%eax
80102251:	8b 40 0c             	mov    0xc(%eax),%eax
80102254:	83 e0 02             	and    $0x2,%eax
80102257:	85 c0                	test   %eax,%eax
80102259:	0f 84 82 00 00 00    	je     801022e1 <iput+0xb4>
8010225f:	8b 45 08             	mov    0x8(%ebp),%eax
80102262:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80102266:	66 85 c0             	test   %ax,%ax
80102269:	75 76                	jne    801022e1 <iput+0xb4>
    // inode has no links and no other references: truncate and free.
    if(ip->flags & I_BUSY)
8010226b:	8b 45 08             	mov    0x8(%ebp),%eax
8010226e:	8b 40 0c             	mov    0xc(%eax),%eax
80102271:	83 e0 01             	and    $0x1,%eax
80102274:	84 c0                	test   %al,%al
80102276:	74 0c                	je     80102284 <iput+0x57>
      panic("iput busy");
80102278:	c7 04 24 0e 90 10 80 	movl   $0x8010900e,(%esp)
8010227f:	e8 e5 e5 ff ff       	call   80100869 <panic>
    ip->flags |= I_BUSY;
80102284:	8b 45 08             	mov    0x8(%ebp),%eax
80102287:	8b 40 0c             	mov    0xc(%eax),%eax
8010228a:	89 c2                	mov    %eax,%edx
8010228c:	83 ca 01             	or     $0x1,%edx
8010228f:	8b 45 08             	mov    0x8(%ebp),%eax
80102292:	89 50 0c             	mov    %edx,0xc(%eax)
    release(&icache.lock);
80102295:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
8010229c:	e8 0f 35 00 00       	call   801057b0 <release>
    itrunc(ip);
801022a1:	8b 45 08             	mov    0x8(%ebp),%eax
801022a4:	89 04 24             	mov    %eax,(%esp)
801022a7:	e8 72 01 00 00       	call   8010241e <itrunc>
    ip->type = 0;
801022ac:	8b 45 08             	mov    0x8(%ebp),%eax
801022af:	66 c7 40 10 00 00    	movw   $0x0,0x10(%eax)
    iupdate(ip);
801022b5:	8b 45 08             	mov    0x8(%ebp),%eax
801022b8:	89 04 24             	mov    %eax,(%esp)
801022bb:	e8 eb fb ff ff       	call   80101eab <iupdate>
    acquire(&icache.lock);
801022c0:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
801022c7:	e8 7b 34 00 00       	call   80105747 <acquire>
    ip->flags = 0;
801022cc:	8b 45 08             	mov    0x8(%ebp),%eax
801022cf:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
    wakeup(ip);
801022d6:	8b 45 08             	mov    0x8(%ebp),%eax
801022d9:	89 04 24             	mov    %eax,(%esp)
801022dc:	e8 1a 32 00 00       	call   801054fb <wakeup>
  }
  ip->ref--;
801022e1:	8b 45 08             	mov    0x8(%ebp),%eax
801022e4:	8b 40 08             	mov    0x8(%eax),%eax
801022e7:	8d 50 ff             	lea    -0x1(%eax),%edx
801022ea:	8b 45 08             	mov    0x8(%ebp),%eax
801022ed:	89 50 08             	mov    %edx,0x8(%eax)
  release(&icache.lock);
801022f0:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
801022f7:	e8 b4 34 00 00       	call   801057b0 <release>
}
801022fc:	c9                   	leave  
801022fd:	c3                   	ret    

801022fe <iunlockput>:

// Common idiom: unlock, then put.
void
iunlockput(struct inode *ip)
{
801022fe:	55                   	push   %ebp
801022ff:	89 e5                	mov    %esp,%ebp
80102301:	83 ec 18             	sub    $0x18,%esp
  iunlock(ip);
80102304:	8b 45 08             	mov    0x8(%ebp),%eax
80102307:	89 04 24             	mov    %eax,(%esp)
8010230a:	e8 b9 fe ff ff       	call   801021c8 <iunlock>
  iput(ip);
8010230f:	8b 45 08             	mov    0x8(%ebp),%eax
80102312:	89 04 24             	mov    %eax,(%esp)
80102315:	e8 13 ff ff ff       	call   8010222d <iput>
}
8010231a:	c9                   	leave  
8010231b:	c3                   	ret    

8010231c <bmap>:

// Return the disk block address of the nth block in inode ip.
// If there is no such block, bmap allocates one.
static uint
bmap(struct inode *ip, uint bn)
{
8010231c:	55                   	push   %ebp
8010231d:	89 e5                	mov    %esp,%ebp
8010231f:	53                   	push   %ebx
80102320:	83 ec 24             	sub    $0x24,%esp
  uint addr, *a;
  struct buf *bp;

  if(bn < NDIRECT){
80102323:	83 7d 0c 0b          	cmpl   $0xb,0xc(%ebp)
80102327:	77 3e                	ja     80102367 <bmap+0x4b>
    if((addr = ip->addrs[bn]) == 0)
80102329:	8b 55 0c             	mov    0xc(%ebp),%edx
8010232c:	8b 45 08             	mov    0x8(%ebp),%eax
8010232f:	83 c2 04             	add    $0x4,%edx
80102332:	8b 44 90 0c          	mov    0xc(%eax,%edx,4),%eax
80102336:	89 45 ec             	mov    %eax,-0x14(%ebp)
80102339:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
8010233d:	75 20                	jne    8010235f <bmap+0x43>
      ip->addrs[bn] = addr = balloc(ip->dev);
8010233f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
80102342:	8b 45 08             	mov    0x8(%ebp),%eax
80102345:	8b 00                	mov    (%eax),%eax
80102347:	89 04 24             	mov    %eax,(%esp)
8010234a:	e8 d0 f7 ff ff       	call   80101b1f <balloc>
8010234f:	89 45 ec             	mov    %eax,-0x14(%ebp)
80102352:	8b 45 08             	mov    0x8(%ebp),%eax
80102355:	8d 4b 04             	lea    0x4(%ebx),%ecx
80102358:	8b 55 ec             	mov    -0x14(%ebp),%edx
8010235b:	89 54 88 0c          	mov    %edx,0xc(%eax,%ecx,4)
    return addr;
8010235f:	8b 45 ec             	mov    -0x14(%ebp),%eax
80102362:	e9 b1 00 00 00       	jmp    80102418 <bmap+0xfc>
  }
  bn -= NDIRECT;
80102367:	83 6d 0c 0c          	subl   $0xc,0xc(%ebp)

  if(bn < NINDIRECT){
8010236b:	83 7d 0c 7f          	cmpl   $0x7f,0xc(%ebp)
8010236f:	0f 87 97 00 00 00    	ja     8010240c <bmap+0xf0>
    // Load indirect block, allocating if necessary.
    if((addr = ip->addrs[NDIRECT]) == 0)
80102375:	8b 45 08             	mov    0x8(%ebp),%eax
80102378:	8b 40 4c             	mov    0x4c(%eax),%eax
8010237b:	89 45 ec             	mov    %eax,-0x14(%ebp)
8010237e:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80102382:	75 19                	jne    8010239d <bmap+0x81>
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
80102384:	8b 45 08             	mov    0x8(%ebp),%eax
80102387:	8b 00                	mov    (%eax),%eax
80102389:	89 04 24             	mov    %eax,(%esp)
8010238c:	e8 8e f7 ff ff       	call   80101b1f <balloc>
80102391:	89 45 ec             	mov    %eax,-0x14(%ebp)
80102394:	8b 45 08             	mov    0x8(%ebp),%eax
80102397:	8b 55 ec             	mov    -0x14(%ebp),%edx
8010239a:	89 50 4c             	mov    %edx,0x4c(%eax)
    bp = bread(ip->dev, addr);
8010239d:	8b 45 08             	mov    0x8(%ebp),%eax
801023a0:	8b 00                	mov    (%eax),%eax
801023a2:	8b 55 ec             	mov    -0x14(%ebp),%edx
801023a5:	89 54 24 04          	mov    %edx,0x4(%esp)
801023a9:	89 04 24             	mov    %eax,(%esp)
801023ac:	e8 f6 dd ff ff       	call   801001a7 <bread>
801023b1:	89 45 f4             	mov    %eax,-0xc(%ebp)
    a = (uint*)bp->data;
801023b4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801023b7:	83 c0 18             	add    $0x18,%eax
801023ba:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if((addr = a[bn]) == 0){
801023bd:	8b 45 0c             	mov    0xc(%ebp),%eax
801023c0:	c1 e0 02             	shl    $0x2,%eax
801023c3:	03 45 f0             	add    -0x10(%ebp),%eax
801023c6:	8b 00                	mov    (%eax),%eax
801023c8:	89 45 ec             	mov    %eax,-0x14(%ebp)
801023cb:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
801023cf:	75 2b                	jne    801023fc <bmap+0xe0>
      a[bn] = addr = balloc(ip->dev);
801023d1:	8b 45 0c             	mov    0xc(%ebp),%eax
801023d4:	c1 e0 02             	shl    $0x2,%eax
801023d7:	89 c3                	mov    %eax,%ebx
801023d9:	03 5d f0             	add    -0x10(%ebp),%ebx
801023dc:	8b 45 08             	mov    0x8(%ebp),%eax
801023df:	8b 00                	mov    (%eax),%eax
801023e1:	89 04 24             	mov    %eax,(%esp)
801023e4:	e8 36 f7 ff ff       	call   80101b1f <balloc>
801023e9:	89 45 ec             	mov    %eax,-0x14(%ebp)
801023ec:	8b 45 ec             	mov    -0x14(%ebp),%eax
801023ef:	89 03                	mov    %eax,(%ebx)
      log_write(bp);
801023f1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801023f4:	89 04 24             	mov    %eax,(%esp)
801023f7:	e8 74 18 00 00       	call   80103c70 <log_write>
    }
    brelse(bp);
801023fc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801023ff:	89 04 24             	mov    %eax,(%esp)
80102402:	e8 11 de ff ff       	call   80100218 <brelse>
    return addr;
80102407:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010240a:	eb 0c                	jmp    80102418 <bmap+0xfc>
  }

  panic("bmap: out of range");
8010240c:	c7 04 24 18 90 10 80 	movl   $0x80109018,(%esp)
80102413:	e8 51 e4 ff ff       	call   80100869 <panic>
}
80102418:	83 c4 24             	add    $0x24,%esp
8010241b:	5b                   	pop    %ebx
8010241c:	5d                   	pop    %ebp
8010241d:	c3                   	ret    

8010241e <itrunc>:
// to it (no directory entries referring to it)
// and has no in-memory reference to it (is
// not an open file or current directory).
static void
itrunc(struct inode *ip)
{
8010241e:	55                   	push   %ebp
8010241f:	89 e5                	mov    %esp,%ebp
80102421:	83 ec 28             	sub    $0x28,%esp
  int i, j;
  struct buf *bp;
  uint *a;

  for(i = 0; i < NDIRECT; i++){
80102424:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
8010242b:	eb 44                	jmp    80102471 <itrunc+0x53>
    if(ip->addrs[i]){
8010242d:	8b 55 e8             	mov    -0x18(%ebp),%edx
80102430:	8b 45 08             	mov    0x8(%ebp),%eax
80102433:	83 c2 04             	add    $0x4,%edx
80102436:	8b 44 90 0c          	mov    0xc(%eax,%edx,4),%eax
8010243a:	85 c0                	test   %eax,%eax
8010243c:	74 2f                	je     8010246d <itrunc+0x4f>
      bfree(ip->dev, ip->addrs[i]);
8010243e:	8b 55 e8             	mov    -0x18(%ebp),%edx
80102441:	8b 45 08             	mov    0x8(%ebp),%eax
80102444:	83 c2 04             	add    $0x4,%edx
80102447:	8b 54 90 0c          	mov    0xc(%eax,%edx,4),%edx
8010244b:	8b 45 08             	mov    0x8(%ebp),%eax
8010244e:	8b 00                	mov    (%eax),%eax
80102450:	89 54 24 04          	mov    %edx,0x4(%esp)
80102454:	89 04 24             	mov    %eax,(%esp)
80102457:	e8 0f f8 ff ff       	call   80101c6b <bfree>
      ip->addrs[i] = 0;
8010245c:	8b 55 e8             	mov    -0x18(%ebp),%edx
8010245f:	8b 45 08             	mov    0x8(%ebp),%eax
80102462:	83 c2 04             	add    $0x4,%edx
80102465:	c7 44 90 0c 00 00 00 	movl   $0x0,0xc(%eax,%edx,4)
8010246c:	00 
{
  int i, j;
  struct buf *bp;
  uint *a;

  for(i = 0; i < NDIRECT; i++){
8010246d:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
80102471:	83 7d e8 0b          	cmpl   $0xb,-0x18(%ebp)
80102475:	7e b6                	jle    8010242d <itrunc+0xf>
      bfree(ip->dev, ip->addrs[i]);
      ip->addrs[i] = 0;
    }
  }

  if(ip->addrs[NDIRECT]){
80102477:	8b 45 08             	mov    0x8(%ebp),%eax
8010247a:	8b 40 4c             	mov    0x4c(%eax),%eax
8010247d:	85 c0                	test   %eax,%eax
8010247f:	0f 84 8f 00 00 00    	je     80102514 <itrunc+0xf6>
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
80102485:	8b 45 08             	mov    0x8(%ebp),%eax
80102488:	8b 50 4c             	mov    0x4c(%eax),%edx
8010248b:	8b 45 08             	mov    0x8(%ebp),%eax
8010248e:	8b 00                	mov    (%eax),%eax
80102490:	89 54 24 04          	mov    %edx,0x4(%esp)
80102494:	89 04 24             	mov    %eax,(%esp)
80102497:	e8 0b dd ff ff       	call   801001a7 <bread>
8010249c:	89 45 f0             	mov    %eax,-0x10(%ebp)
    a = (uint*)bp->data;
8010249f:	8b 45 f0             	mov    -0x10(%ebp),%eax
801024a2:	83 c0 18             	add    $0x18,%eax
801024a5:	89 45 f4             	mov    %eax,-0xc(%ebp)
    for(j = 0; j < NINDIRECT; j++){
801024a8:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
801024af:	eb 2f                	jmp    801024e0 <itrunc+0xc2>
      if(a[j])
801024b1:	8b 45 ec             	mov    -0x14(%ebp),%eax
801024b4:	c1 e0 02             	shl    $0x2,%eax
801024b7:	03 45 f4             	add    -0xc(%ebp),%eax
801024ba:	8b 00                	mov    (%eax),%eax
801024bc:	85 c0                	test   %eax,%eax
801024be:	74 1c                	je     801024dc <itrunc+0xbe>
        bfree(ip->dev, a[j]);
801024c0:	8b 45 ec             	mov    -0x14(%ebp),%eax
801024c3:	c1 e0 02             	shl    $0x2,%eax
801024c6:	03 45 f4             	add    -0xc(%ebp),%eax
801024c9:	8b 10                	mov    (%eax),%edx
801024cb:	8b 45 08             	mov    0x8(%ebp),%eax
801024ce:	8b 00                	mov    (%eax),%eax
801024d0:	89 54 24 04          	mov    %edx,0x4(%esp)
801024d4:	89 04 24             	mov    %eax,(%esp)
801024d7:	e8 8f f7 ff ff       	call   80101c6b <bfree>
  }

  if(ip->addrs[NDIRECT]){
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
    a = (uint*)bp->data;
    for(j = 0; j < NINDIRECT; j++){
801024dc:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
801024e0:	8b 45 ec             	mov    -0x14(%ebp),%eax
801024e3:	83 f8 7f             	cmp    $0x7f,%eax
801024e6:	76 c9                	jbe    801024b1 <itrunc+0x93>
      if(a[j])
        bfree(ip->dev, a[j]);
    }
    brelse(bp);
801024e8:	8b 45 f0             	mov    -0x10(%ebp),%eax
801024eb:	89 04 24             	mov    %eax,(%esp)
801024ee:	e8 25 dd ff ff       	call   80100218 <brelse>
    bfree(ip->dev, ip->addrs[NDIRECT]);
801024f3:	8b 45 08             	mov    0x8(%ebp),%eax
801024f6:	8b 50 4c             	mov    0x4c(%eax),%edx
801024f9:	8b 45 08             	mov    0x8(%ebp),%eax
801024fc:	8b 00                	mov    (%eax),%eax
801024fe:	89 54 24 04          	mov    %edx,0x4(%esp)
80102502:	89 04 24             	mov    %eax,(%esp)
80102505:	e8 61 f7 ff ff       	call   80101c6b <bfree>
    ip->addrs[NDIRECT] = 0;
8010250a:	8b 45 08             	mov    0x8(%ebp),%eax
8010250d:	c7 40 4c 00 00 00 00 	movl   $0x0,0x4c(%eax)
  }

  ip->size = 0;
80102514:	8b 45 08             	mov    0x8(%ebp),%eax
80102517:	c7 40 18 00 00 00 00 	movl   $0x0,0x18(%eax)
  iupdate(ip);
8010251e:	8b 45 08             	mov    0x8(%ebp),%eax
80102521:	89 04 24             	mov    %eax,(%esp)
80102524:	e8 82 f9 ff ff       	call   80101eab <iupdate>
}
80102529:	c9                   	leave  
8010252a:	c3                   	ret    

8010252b <stati>:

// Copy stat information from inode.
void
stati(struct inode *ip, struct stat *st)
{
8010252b:	55                   	push   %ebp
8010252c:	89 e5                	mov    %esp,%ebp
  st->dev = ip->dev;
8010252e:	8b 45 08             	mov    0x8(%ebp),%eax
80102531:	8b 00                	mov    (%eax),%eax
80102533:	89 c2                	mov    %eax,%edx
80102535:	8b 45 0c             	mov    0xc(%ebp),%eax
80102538:	89 50 04             	mov    %edx,0x4(%eax)
  st->ino = ip->inum;
8010253b:	8b 45 08             	mov    0x8(%ebp),%eax
8010253e:	8b 50 04             	mov    0x4(%eax),%edx
80102541:	8b 45 0c             	mov    0xc(%ebp),%eax
80102544:	89 50 08             	mov    %edx,0x8(%eax)
  st->type = ip->type;
80102547:	8b 45 08             	mov    0x8(%ebp),%eax
8010254a:	0f b7 50 10          	movzwl 0x10(%eax),%edx
8010254e:	8b 45 0c             	mov    0xc(%ebp),%eax
80102551:	66 89 10             	mov    %dx,(%eax)
  st->nlink = ip->nlink;
80102554:	8b 45 08             	mov    0x8(%ebp),%eax
80102557:	0f b7 50 16          	movzwl 0x16(%eax),%edx
8010255b:	8b 45 0c             	mov    0xc(%ebp),%eax
8010255e:	66 89 50 0c          	mov    %dx,0xc(%eax)
  st->size = ip->size;
80102562:	8b 45 08             	mov    0x8(%ebp),%eax
80102565:	8b 50 18             	mov    0x18(%eax),%edx
80102568:	8b 45 0c             	mov    0xc(%ebp),%eax
8010256b:	89 50 10             	mov    %edx,0x10(%eax)
}
8010256e:	5d                   	pop    %ebp
8010256f:	c3                   	ret    

80102570 <readi>:

//PAGEBREAK!
// Read data from inode.
int
readi(struct inode *ip, char *dst, uint off, uint n)
{
80102570:	55                   	push   %ebp
80102571:	89 e5                	mov    %esp,%ebp
80102573:	53                   	push   %ebx
80102574:	83 ec 24             	sub    $0x24,%esp
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
80102577:	8b 45 08             	mov    0x8(%ebp),%eax
8010257a:	0f b7 40 10          	movzwl 0x10(%eax),%eax
8010257e:	66 83 f8 03          	cmp    $0x3,%ax
80102582:	75 60                	jne    801025e4 <readi+0x74>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
80102584:	8b 45 08             	mov    0x8(%ebp),%eax
80102587:	0f b7 40 12          	movzwl 0x12(%eax),%eax
8010258b:	66 85 c0             	test   %ax,%ax
8010258e:	78 20                	js     801025b0 <readi+0x40>
80102590:	8b 45 08             	mov    0x8(%ebp),%eax
80102593:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80102597:	66 83 f8 09          	cmp    $0x9,%ax
8010259b:	7f 13                	jg     801025b0 <readi+0x40>
8010259d:	8b 45 08             	mov    0x8(%ebp),%eax
801025a0:	0f b7 40 12          	movzwl 0x12(%eax),%eax
801025a4:	98                   	cwtl   
801025a5:	8b 04 c5 40 2a 11 80 	mov    -0x7feed5c0(,%eax,8),%eax
801025ac:	85 c0                	test   %eax,%eax
801025ae:	75 0a                	jne    801025ba <readi+0x4a>
      return -1;
801025b0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801025b5:	e9 1c 01 00 00       	jmp    801026d6 <readi+0x166>
    return devsw[ip->major].read(ip, dst, n);
801025ba:	8b 45 08             	mov    0x8(%ebp),%eax
801025bd:	0f b7 40 12          	movzwl 0x12(%eax),%eax
801025c1:	98                   	cwtl   
801025c2:	8b 14 c5 40 2a 11 80 	mov    -0x7feed5c0(,%eax,8),%edx
801025c9:	8b 45 14             	mov    0x14(%ebp),%eax
801025cc:	89 44 24 08          	mov    %eax,0x8(%esp)
801025d0:	8b 45 0c             	mov    0xc(%ebp),%eax
801025d3:	89 44 24 04          	mov    %eax,0x4(%esp)
801025d7:	8b 45 08             	mov    0x8(%ebp),%eax
801025da:	89 04 24             	mov    %eax,(%esp)
801025dd:	ff d2                	call   *%edx
801025df:	e9 f2 00 00 00       	jmp    801026d6 <readi+0x166>
  }

  if(off > ip->size || off + n < off)
801025e4:	8b 45 08             	mov    0x8(%ebp),%eax
801025e7:	8b 40 18             	mov    0x18(%eax),%eax
801025ea:	3b 45 10             	cmp    0x10(%ebp),%eax
801025ed:	72 0e                	jb     801025fd <readi+0x8d>
801025ef:	8b 45 14             	mov    0x14(%ebp),%eax
801025f2:	8b 55 10             	mov    0x10(%ebp),%edx
801025f5:	8d 04 02             	lea    (%edx,%eax,1),%eax
801025f8:	3b 45 10             	cmp    0x10(%ebp),%eax
801025fb:	73 0a                	jae    80102607 <readi+0x97>
    return -1;
801025fd:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102602:	e9 cf 00 00 00       	jmp    801026d6 <readi+0x166>
  if(off + n > ip->size)
80102607:	8b 45 14             	mov    0x14(%ebp),%eax
8010260a:	8b 55 10             	mov    0x10(%ebp),%edx
8010260d:	01 c2                	add    %eax,%edx
8010260f:	8b 45 08             	mov    0x8(%ebp),%eax
80102612:	8b 40 18             	mov    0x18(%eax),%eax
80102615:	39 c2                	cmp    %eax,%edx
80102617:	76 0c                	jbe    80102625 <readi+0xb5>
    n = ip->size - off;
80102619:	8b 45 08             	mov    0x8(%ebp),%eax
8010261c:	8b 40 18             	mov    0x18(%eax),%eax
8010261f:	2b 45 10             	sub    0x10(%ebp),%eax
80102622:	89 45 14             	mov    %eax,0x14(%ebp)

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80102625:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
8010262c:	e9 96 00 00 00       	jmp    801026c7 <readi+0x157>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80102631:	8b 45 10             	mov    0x10(%ebp),%eax
80102634:	c1 e8 09             	shr    $0x9,%eax
80102637:	89 44 24 04          	mov    %eax,0x4(%esp)
8010263b:	8b 45 08             	mov    0x8(%ebp),%eax
8010263e:	89 04 24             	mov    %eax,(%esp)
80102641:	e8 d6 fc ff ff       	call   8010231c <bmap>
80102646:	8b 55 08             	mov    0x8(%ebp),%edx
80102649:	8b 12                	mov    (%edx),%edx
8010264b:	89 44 24 04          	mov    %eax,0x4(%esp)
8010264f:	89 14 24             	mov    %edx,(%esp)
80102652:	e8 50 db ff ff       	call   801001a7 <bread>
80102657:	89 45 f4             	mov    %eax,-0xc(%ebp)
    m = min(n - tot, BSIZE - off%BSIZE);
8010265a:	8b 45 10             	mov    0x10(%ebp),%eax
8010265d:	89 c2                	mov    %eax,%edx
8010265f:	81 e2 ff 01 00 00    	and    $0x1ff,%edx
80102665:	b8 00 02 00 00       	mov    $0x200,%eax
8010266a:	89 c1                	mov    %eax,%ecx
8010266c:	29 d1                	sub    %edx,%ecx
8010266e:	89 ca                	mov    %ecx,%edx
80102670:	8b 45 ec             	mov    -0x14(%ebp),%eax
80102673:	8b 4d 14             	mov    0x14(%ebp),%ecx
80102676:	89 cb                	mov    %ecx,%ebx
80102678:	29 c3                	sub    %eax,%ebx
8010267a:	89 d8                	mov    %ebx,%eax
8010267c:	39 c2                	cmp    %eax,%edx
8010267e:	0f 46 c2             	cmovbe %edx,%eax
80102681:	89 45 f0             	mov    %eax,-0x10(%ebp)
    memmove(dst, bp->data + off%BSIZE, m);
80102684:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102687:	8d 50 18             	lea    0x18(%eax),%edx
8010268a:	8b 45 10             	mov    0x10(%ebp),%eax
8010268d:	25 ff 01 00 00       	and    $0x1ff,%eax
80102692:	01 c2                	add    %eax,%edx
80102694:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102697:	89 44 24 08          	mov    %eax,0x8(%esp)
8010269b:	89 54 24 04          	mov    %edx,0x4(%esp)
8010269f:	8b 45 0c             	mov    0xc(%ebp),%eax
801026a2:	89 04 24             	mov    %eax,(%esp)
801026a5:	e8 d3 33 00 00       	call   80105a7d <memmove>
    brelse(bp);
801026aa:	8b 45 f4             	mov    -0xc(%ebp),%eax
801026ad:	89 04 24             	mov    %eax,(%esp)
801026b0:	e8 63 db ff ff       	call   80100218 <brelse>
  if(off > ip->size || off + n < off)
    return -1;
  if(off + n > ip->size)
    n = ip->size - off;

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
801026b5:	8b 45 f0             	mov    -0x10(%ebp),%eax
801026b8:	01 45 ec             	add    %eax,-0x14(%ebp)
801026bb:	8b 45 f0             	mov    -0x10(%ebp),%eax
801026be:	01 45 10             	add    %eax,0x10(%ebp)
801026c1:	8b 45 f0             	mov    -0x10(%ebp),%eax
801026c4:	01 45 0c             	add    %eax,0xc(%ebp)
801026c7:	8b 45 ec             	mov    -0x14(%ebp),%eax
801026ca:	3b 45 14             	cmp    0x14(%ebp),%eax
801026cd:	0f 82 5e ff ff ff    	jb     80102631 <readi+0xc1>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
    m = min(n - tot, BSIZE - off%BSIZE);
    memmove(dst, bp->data + off%BSIZE, m);
    brelse(bp);
  }
  return n;
801026d3:	8b 45 14             	mov    0x14(%ebp),%eax
}
801026d6:	83 c4 24             	add    $0x24,%esp
801026d9:	5b                   	pop    %ebx
801026da:	5d                   	pop    %ebp
801026db:	c3                   	ret    

801026dc <writei>:

// PAGEBREAK!
// Write data to inode.
int
writei(struct inode *ip, char *src, uint off, uint n)
{
801026dc:	55                   	push   %ebp
801026dd:	89 e5                	mov    %esp,%ebp
801026df:	53                   	push   %ebx
801026e0:	83 ec 24             	sub    $0x24,%esp
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
801026e3:	8b 45 08             	mov    0x8(%ebp),%eax
801026e6:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801026ea:	66 83 f8 03          	cmp    $0x3,%ax
801026ee:	75 60                	jne    80102750 <writei+0x74>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
801026f0:	8b 45 08             	mov    0x8(%ebp),%eax
801026f3:	0f b7 40 12          	movzwl 0x12(%eax),%eax
801026f7:	66 85 c0             	test   %ax,%ax
801026fa:	78 20                	js     8010271c <writei+0x40>
801026fc:	8b 45 08             	mov    0x8(%ebp),%eax
801026ff:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80102703:	66 83 f8 09          	cmp    $0x9,%ax
80102707:	7f 13                	jg     8010271c <writei+0x40>
80102709:	8b 45 08             	mov    0x8(%ebp),%eax
8010270c:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80102710:	98                   	cwtl   
80102711:	8b 04 c5 44 2a 11 80 	mov    -0x7feed5bc(,%eax,8),%eax
80102718:	85 c0                	test   %eax,%eax
8010271a:	75 0a                	jne    80102726 <writei+0x4a>
      return -1;
8010271c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102721:	e9 48 01 00 00       	jmp    8010286e <writei+0x192>
    return devsw[ip->major].write(ip, src, n);
80102726:	8b 45 08             	mov    0x8(%ebp),%eax
80102729:	0f b7 40 12          	movzwl 0x12(%eax),%eax
8010272d:	98                   	cwtl   
8010272e:	8b 14 c5 44 2a 11 80 	mov    -0x7feed5bc(,%eax,8),%edx
80102735:	8b 45 14             	mov    0x14(%ebp),%eax
80102738:	89 44 24 08          	mov    %eax,0x8(%esp)
8010273c:	8b 45 0c             	mov    0xc(%ebp),%eax
8010273f:	89 44 24 04          	mov    %eax,0x4(%esp)
80102743:	8b 45 08             	mov    0x8(%ebp),%eax
80102746:	89 04 24             	mov    %eax,(%esp)
80102749:	ff d2                	call   *%edx
8010274b:	e9 1e 01 00 00       	jmp    8010286e <writei+0x192>
  }

  if(off > ip->size || off + n < off)
80102750:	8b 45 08             	mov    0x8(%ebp),%eax
80102753:	8b 40 18             	mov    0x18(%eax),%eax
80102756:	3b 45 10             	cmp    0x10(%ebp),%eax
80102759:	72 0e                	jb     80102769 <writei+0x8d>
8010275b:	8b 45 14             	mov    0x14(%ebp),%eax
8010275e:	8b 55 10             	mov    0x10(%ebp),%edx
80102761:	8d 04 02             	lea    (%edx,%eax,1),%eax
80102764:	3b 45 10             	cmp    0x10(%ebp),%eax
80102767:	73 0a                	jae    80102773 <writei+0x97>
    return -1;
80102769:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010276e:	e9 fb 00 00 00       	jmp    8010286e <writei+0x192>
  if(off + n > MAXFILE*BSIZE)
80102773:	8b 45 14             	mov    0x14(%ebp),%eax
80102776:	8b 55 10             	mov    0x10(%ebp),%edx
80102779:	8d 04 02             	lea    (%edx,%eax,1),%eax
8010277c:	3d 00 18 01 00       	cmp    $0x11800,%eax
80102781:	76 0a                	jbe    8010278d <writei+0xb1>
    return -1;
80102783:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102788:	e9 e1 00 00 00       	jmp    8010286e <writei+0x192>

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
8010278d:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
80102794:	e9 a1 00 00 00       	jmp    8010283a <writei+0x15e>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80102799:	8b 45 10             	mov    0x10(%ebp),%eax
8010279c:	c1 e8 09             	shr    $0x9,%eax
8010279f:	89 44 24 04          	mov    %eax,0x4(%esp)
801027a3:	8b 45 08             	mov    0x8(%ebp),%eax
801027a6:	89 04 24             	mov    %eax,(%esp)
801027a9:	e8 6e fb ff ff       	call   8010231c <bmap>
801027ae:	8b 55 08             	mov    0x8(%ebp),%edx
801027b1:	8b 12                	mov    (%edx),%edx
801027b3:	89 44 24 04          	mov    %eax,0x4(%esp)
801027b7:	89 14 24             	mov    %edx,(%esp)
801027ba:	e8 e8 d9 ff ff       	call   801001a7 <bread>
801027bf:	89 45 f4             	mov    %eax,-0xc(%ebp)
    m = min(n - tot, BSIZE - off%BSIZE);
801027c2:	8b 45 10             	mov    0x10(%ebp),%eax
801027c5:	89 c2                	mov    %eax,%edx
801027c7:	81 e2 ff 01 00 00    	and    $0x1ff,%edx
801027cd:	b8 00 02 00 00       	mov    $0x200,%eax
801027d2:	89 c1                	mov    %eax,%ecx
801027d4:	29 d1                	sub    %edx,%ecx
801027d6:	89 ca                	mov    %ecx,%edx
801027d8:	8b 45 ec             	mov    -0x14(%ebp),%eax
801027db:	8b 4d 14             	mov    0x14(%ebp),%ecx
801027de:	89 cb                	mov    %ecx,%ebx
801027e0:	29 c3                	sub    %eax,%ebx
801027e2:	89 d8                	mov    %ebx,%eax
801027e4:	39 c2                	cmp    %eax,%edx
801027e6:	0f 46 c2             	cmovbe %edx,%eax
801027e9:	89 45 f0             	mov    %eax,-0x10(%ebp)
    memmove(bp->data + off%BSIZE, src, m);
801027ec:	8b 45 f4             	mov    -0xc(%ebp),%eax
801027ef:	8d 50 18             	lea    0x18(%eax),%edx
801027f2:	8b 45 10             	mov    0x10(%ebp),%eax
801027f5:	25 ff 01 00 00       	and    $0x1ff,%eax
801027fa:	01 c2                	add    %eax,%edx
801027fc:	8b 45 f0             	mov    -0x10(%ebp),%eax
801027ff:	89 44 24 08          	mov    %eax,0x8(%esp)
80102803:	8b 45 0c             	mov    0xc(%ebp),%eax
80102806:	89 44 24 04          	mov    %eax,0x4(%esp)
8010280a:	89 14 24             	mov    %edx,(%esp)
8010280d:	e8 6b 32 00 00       	call   80105a7d <memmove>
    log_write(bp);
80102812:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102815:	89 04 24             	mov    %eax,(%esp)
80102818:	e8 53 14 00 00       	call   80103c70 <log_write>
    brelse(bp);
8010281d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102820:	89 04 24             	mov    %eax,(%esp)
80102823:	e8 f0 d9 ff ff       	call   80100218 <brelse>
  if(off > ip->size || off + n < off)
    return -1;
  if(off + n > MAXFILE*BSIZE)
    return -1;

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80102828:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010282b:	01 45 ec             	add    %eax,-0x14(%ebp)
8010282e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102831:	01 45 10             	add    %eax,0x10(%ebp)
80102834:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102837:	01 45 0c             	add    %eax,0xc(%ebp)
8010283a:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010283d:	3b 45 14             	cmp    0x14(%ebp),%eax
80102840:	0f 82 53 ff ff ff    	jb     80102799 <writei+0xbd>
    memmove(bp->data + off%BSIZE, src, m);
    log_write(bp);
    brelse(bp);
  }

  if(n > 0 && off > ip->size){
80102846:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
8010284a:	74 1f                	je     8010286b <writei+0x18f>
8010284c:	8b 45 08             	mov    0x8(%ebp),%eax
8010284f:	8b 40 18             	mov    0x18(%eax),%eax
80102852:	3b 45 10             	cmp    0x10(%ebp),%eax
80102855:	73 14                	jae    8010286b <writei+0x18f>
    ip->size = off;
80102857:	8b 45 08             	mov    0x8(%ebp),%eax
8010285a:	8b 55 10             	mov    0x10(%ebp),%edx
8010285d:	89 50 18             	mov    %edx,0x18(%eax)
    iupdate(ip);
80102860:	8b 45 08             	mov    0x8(%ebp),%eax
80102863:	89 04 24             	mov    %eax,(%esp)
80102866:	e8 40 f6 ff ff       	call   80101eab <iupdate>
  }
  return n;
8010286b:	8b 45 14             	mov    0x14(%ebp),%eax
}
8010286e:	83 c4 24             	add    $0x24,%esp
80102871:	5b                   	pop    %ebx
80102872:	5d                   	pop    %ebp
80102873:	c3                   	ret    

80102874 <namecmp>:
//PAGEBREAK!
// Directories

int
namecmp(const char *s, const char *t)
{
80102874:	55                   	push   %ebp
80102875:	89 e5                	mov    %esp,%ebp
80102877:	83 ec 18             	sub    $0x18,%esp
  return strncmp(s, t, DIRSIZ);
8010287a:	c7 44 24 08 0e 00 00 	movl   $0xe,0x8(%esp)
80102881:	00 
80102882:	8b 45 0c             	mov    0xc(%ebp),%eax
80102885:	89 44 24 04          	mov    %eax,0x4(%esp)
80102889:	8b 45 08             	mov    0x8(%ebp),%eax
8010288c:	89 04 24             	mov    %eax,(%esp)
8010288f:	e8 91 32 00 00       	call   80105b25 <strncmp>
}
80102894:	c9                   	leave  
80102895:	c3                   	ret    

80102896 <dirlookup>:

// Look for a directory entry in a directory.
// If found, set *poff to byte offset of entry.
struct inode*
dirlookup(struct inode *dp, char *name, uint *poff)
{
80102896:	55                   	push   %ebp
80102897:	89 e5                	mov    %esp,%ebp
80102899:	83 ec 38             	sub    $0x38,%esp
  uint off, inum;
  struct dirent de;

  if(dp->type != T_DIR)
8010289c:	8b 45 08             	mov    0x8(%ebp),%eax
8010289f:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801028a3:	66 83 f8 01          	cmp    $0x1,%ax
801028a7:	74 0c                	je     801028b5 <dirlookup+0x1f>
    panic("dirlookup not DIR");
801028a9:	c7 04 24 2b 90 10 80 	movl   $0x8010902b,(%esp)
801028b0:	e8 b4 df ff ff       	call   80100869 <panic>

  for(off = 0; off < dp->size; off += sizeof(de)){
801028b5:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
801028bc:	e9 87 00 00 00       	jmp    80102948 <dirlookup+0xb2>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801028c1:	8d 45 e0             	lea    -0x20(%ebp),%eax
801028c4:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
801028cb:	00 
801028cc:	8b 55 f0             	mov    -0x10(%ebp),%edx
801028cf:	89 54 24 08          	mov    %edx,0x8(%esp)
801028d3:	89 44 24 04          	mov    %eax,0x4(%esp)
801028d7:	8b 45 08             	mov    0x8(%ebp),%eax
801028da:	89 04 24             	mov    %eax,(%esp)
801028dd:	e8 8e fc ff ff       	call   80102570 <readi>
801028e2:	83 f8 10             	cmp    $0x10,%eax
801028e5:	74 0c                	je     801028f3 <dirlookup+0x5d>
      panic("dirlink read");
801028e7:	c7 04 24 3d 90 10 80 	movl   $0x8010903d,(%esp)
801028ee:	e8 76 df ff ff       	call   80100869 <panic>
    if(de.inum == 0)
801028f3:	0f b7 45 e0          	movzwl -0x20(%ebp),%eax
801028f7:	66 85 c0             	test   %ax,%ax
801028fa:	74 47                	je     80102943 <dirlookup+0xad>
      continue;
    if(namecmp(name, de.name) == 0){
801028fc:	8d 45 e0             	lea    -0x20(%ebp),%eax
801028ff:	83 c0 02             	add    $0x2,%eax
80102902:	89 44 24 04          	mov    %eax,0x4(%esp)
80102906:	8b 45 0c             	mov    0xc(%ebp),%eax
80102909:	89 04 24             	mov    %eax,(%esp)
8010290c:	e8 63 ff ff ff       	call   80102874 <namecmp>
80102911:	85 c0                	test   %eax,%eax
80102913:	75 2f                	jne    80102944 <dirlookup+0xae>
      // entry matches path element
      if(poff)
80102915:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80102919:	74 08                	je     80102923 <dirlookup+0x8d>
        *poff = off;
8010291b:	8b 45 10             	mov    0x10(%ebp),%eax
8010291e:	8b 55 f0             	mov    -0x10(%ebp),%edx
80102921:	89 10                	mov    %edx,(%eax)
      inum = de.inum;
80102923:	0f b7 45 e0          	movzwl -0x20(%ebp),%eax
80102927:	0f b7 c0             	movzwl %ax,%eax
8010292a:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return iget(dp->dev, inum);
8010292d:	8b 45 08             	mov    0x8(%ebp),%eax
80102930:	8b 00                	mov    (%eax),%eax
80102932:	8b 55 f4             	mov    -0xc(%ebp),%edx
80102935:	89 54 24 04          	mov    %edx,0x4(%esp)
80102939:	89 04 24             	mov    %eax,(%esp)
8010293c:	e8 2b f6 ff ff       	call   80101f6c <iget>
80102941:	eb 19                	jmp    8010295c <dirlookup+0xc6>

  for(off = 0; off < dp->size; off += sizeof(de)){
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("dirlink read");
    if(de.inum == 0)
      continue;
80102943:	90                   	nop
  struct dirent de;

  if(dp->type != T_DIR)
    panic("dirlookup not DIR");

  for(off = 0; off < dp->size; off += sizeof(de)){
80102944:	83 45 f0 10          	addl   $0x10,-0x10(%ebp)
80102948:	8b 45 08             	mov    0x8(%ebp),%eax
8010294b:	8b 40 18             	mov    0x18(%eax),%eax
8010294e:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80102951:	0f 87 6a ff ff ff    	ja     801028c1 <dirlookup+0x2b>
      inum = de.inum;
      return iget(dp->dev, inum);
    }
  }

  return 0;
80102957:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010295c:	c9                   	leave  
8010295d:	c3                   	ret    

8010295e <dirlink>:

// Write a new directory entry (name, inum) into the directory dp.
int
dirlink(struct inode *dp, char *name, uint inum)
{
8010295e:	55                   	push   %ebp
8010295f:	89 e5                	mov    %esp,%ebp
80102961:	83 ec 38             	sub    $0x38,%esp
  int off;
  struct dirent de;
  struct inode *ip;

  // Check that name is not present.
  if((ip = dirlookup(dp, name, 0)) != 0){
80102964:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
8010296b:	00 
8010296c:	8b 45 0c             	mov    0xc(%ebp),%eax
8010296f:	89 44 24 04          	mov    %eax,0x4(%esp)
80102973:	8b 45 08             	mov    0x8(%ebp),%eax
80102976:	89 04 24             	mov    %eax,(%esp)
80102979:	e8 18 ff ff ff       	call   80102896 <dirlookup>
8010297e:	89 45 f4             	mov    %eax,-0xc(%ebp)
80102981:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80102985:	74 15                	je     8010299c <dirlink+0x3e>
    iput(ip);
80102987:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010298a:	89 04 24             	mov    %eax,(%esp)
8010298d:	e8 9b f8 ff ff       	call   8010222d <iput>
    return -1;
80102992:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102997:	e9 b8 00 00 00       	jmp    80102a54 <dirlink+0xf6>
  }

  // Look for an empty dirent.
  for(off = 0; off < dp->size; off += sizeof(de)){
8010299c:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
801029a3:	eb 44                	jmp    801029e9 <dirlink+0x8b>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801029a5:	8b 55 f0             	mov    -0x10(%ebp),%edx
801029a8:	8d 45 e0             	lea    -0x20(%ebp),%eax
801029ab:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
801029b2:	00 
801029b3:	89 54 24 08          	mov    %edx,0x8(%esp)
801029b7:	89 44 24 04          	mov    %eax,0x4(%esp)
801029bb:	8b 45 08             	mov    0x8(%ebp),%eax
801029be:	89 04 24             	mov    %eax,(%esp)
801029c1:	e8 aa fb ff ff       	call   80102570 <readi>
801029c6:	83 f8 10             	cmp    $0x10,%eax
801029c9:	74 0c                	je     801029d7 <dirlink+0x79>
      panic("dirlink read");
801029cb:	c7 04 24 3d 90 10 80 	movl   $0x8010903d,(%esp)
801029d2:	e8 92 de ff ff       	call   80100869 <panic>
    if(de.inum == 0)
801029d7:	0f b7 45 e0          	movzwl -0x20(%ebp),%eax
801029db:	66 85 c0             	test   %ax,%ax
801029de:	74 18                	je     801029f8 <dirlink+0x9a>
    iput(ip);
    return -1;
  }

  // Look for an empty dirent.
  for(off = 0; off < dp->size; off += sizeof(de)){
801029e0:	8b 45 f0             	mov    -0x10(%ebp),%eax
801029e3:	83 c0 10             	add    $0x10,%eax
801029e6:	89 45 f0             	mov    %eax,-0x10(%ebp)
801029e9:	8b 55 f0             	mov    -0x10(%ebp),%edx
801029ec:	8b 45 08             	mov    0x8(%ebp),%eax
801029ef:	8b 40 18             	mov    0x18(%eax),%eax
801029f2:	39 c2                	cmp    %eax,%edx
801029f4:	72 af                	jb     801029a5 <dirlink+0x47>
801029f6:	eb 01                	jmp    801029f9 <dirlink+0x9b>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("dirlink read");
    if(de.inum == 0)
      break;
801029f8:	90                   	nop
  }

  strncpy(de.name, name, DIRSIZ);
801029f9:	c7 44 24 08 0e 00 00 	movl   $0xe,0x8(%esp)
80102a00:	00 
80102a01:	8b 45 0c             	mov    0xc(%ebp),%eax
80102a04:	89 44 24 04          	mov    %eax,0x4(%esp)
80102a08:	8d 45 e0             	lea    -0x20(%ebp),%eax
80102a0b:	83 c0 02             	add    $0x2,%eax
80102a0e:	89 04 24             	mov    %eax,(%esp)
80102a11:	e8 67 31 00 00       	call   80105b7d <strncpy>
  de.inum = inum;
80102a16:	8b 45 10             	mov    0x10(%ebp),%eax
80102a19:	66 89 45 e0          	mov    %ax,-0x20(%ebp)
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80102a1d:	8b 55 f0             	mov    -0x10(%ebp),%edx
80102a20:	8d 45 e0             	lea    -0x20(%ebp),%eax
80102a23:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
80102a2a:	00 
80102a2b:	89 54 24 08          	mov    %edx,0x8(%esp)
80102a2f:	89 44 24 04          	mov    %eax,0x4(%esp)
80102a33:	8b 45 08             	mov    0x8(%ebp),%eax
80102a36:	89 04 24             	mov    %eax,(%esp)
80102a39:	e8 9e fc ff ff       	call   801026dc <writei>
80102a3e:	83 f8 10             	cmp    $0x10,%eax
80102a41:	74 0c                	je     80102a4f <dirlink+0xf1>
    panic("dirlink");
80102a43:	c7 04 24 4a 90 10 80 	movl   $0x8010904a,(%esp)
80102a4a:	e8 1a de ff ff       	call   80100869 <panic>

  return 0;
80102a4f:	b8 00 00 00 00       	mov    $0x0,%eax
}
80102a54:	c9                   	leave  
80102a55:	c3                   	ret    

80102a56 <skipelem>:
//   skipelem("a", name) = "", setting name = "a"
//   skipelem("", name) = skipelem("////", name) = 0
//
static char*
skipelem(char *path, char *name)
{
80102a56:	55                   	push   %ebp
80102a57:	89 e5                	mov    %esp,%ebp
80102a59:	83 ec 28             	sub    $0x28,%esp
  char *s;
  int len;

  while(*path == '/')
80102a5c:	eb 04                	jmp    80102a62 <skipelem+0xc>
    path++;
80102a5e:	83 45 08 01          	addl   $0x1,0x8(%ebp)
skipelem(char *path, char *name)
{
  char *s;
  int len;

  while(*path == '/')
80102a62:	8b 45 08             	mov    0x8(%ebp),%eax
80102a65:	0f b6 00             	movzbl (%eax),%eax
80102a68:	3c 2f                	cmp    $0x2f,%al
80102a6a:	74 f2                	je     80102a5e <skipelem+0x8>
    path++;
  if(*path == 0)
80102a6c:	8b 45 08             	mov    0x8(%ebp),%eax
80102a6f:	0f b6 00             	movzbl (%eax),%eax
80102a72:	84 c0                	test   %al,%al
80102a74:	75 0a                	jne    80102a80 <skipelem+0x2a>
    return 0;
80102a76:	b8 00 00 00 00       	mov    $0x0,%eax
80102a7b:	e9 86 00 00 00       	jmp    80102b06 <skipelem+0xb0>
  s = path;
80102a80:	8b 45 08             	mov    0x8(%ebp),%eax
80102a83:	89 45 f0             	mov    %eax,-0x10(%ebp)
  while(*path != '/' && *path != 0)
80102a86:	eb 04                	jmp    80102a8c <skipelem+0x36>
    path++;
80102a88:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  while(*path == '/')
    path++;
  if(*path == 0)
    return 0;
  s = path;
  while(*path != '/' && *path != 0)
80102a8c:	8b 45 08             	mov    0x8(%ebp),%eax
80102a8f:	0f b6 00             	movzbl (%eax),%eax
80102a92:	3c 2f                	cmp    $0x2f,%al
80102a94:	74 0a                	je     80102aa0 <skipelem+0x4a>
80102a96:	8b 45 08             	mov    0x8(%ebp),%eax
80102a99:	0f b6 00             	movzbl (%eax),%eax
80102a9c:	84 c0                	test   %al,%al
80102a9e:	75 e8                	jne    80102a88 <skipelem+0x32>
    path++;
  len = path - s;
80102aa0:	8b 55 08             	mov    0x8(%ebp),%edx
80102aa3:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102aa6:	89 d1                	mov    %edx,%ecx
80102aa8:	29 c1                	sub    %eax,%ecx
80102aaa:	89 c8                	mov    %ecx,%eax
80102aac:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(len >= DIRSIZ)
80102aaf:	83 7d f4 0d          	cmpl   $0xd,-0xc(%ebp)
80102ab3:	7e 1c                	jle    80102ad1 <skipelem+0x7b>
    memmove(name, s, DIRSIZ);
80102ab5:	c7 44 24 08 0e 00 00 	movl   $0xe,0x8(%esp)
80102abc:	00 
80102abd:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102ac0:	89 44 24 04          	mov    %eax,0x4(%esp)
80102ac4:	8b 45 0c             	mov    0xc(%ebp),%eax
80102ac7:	89 04 24             	mov    %eax,(%esp)
80102aca:	e8 ae 2f 00 00       	call   80105a7d <memmove>
  else {
    memmove(name, s, len);
    name[len] = 0;
  }
  while(*path == '/')
80102acf:	eb 28                	jmp    80102af9 <skipelem+0xa3>
    path++;
  len = path - s;
  if(len >= DIRSIZ)
    memmove(name, s, DIRSIZ);
  else {
    memmove(name, s, len);
80102ad1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102ad4:	89 44 24 08          	mov    %eax,0x8(%esp)
80102ad8:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102adb:	89 44 24 04          	mov    %eax,0x4(%esp)
80102adf:	8b 45 0c             	mov    0xc(%ebp),%eax
80102ae2:	89 04 24             	mov    %eax,(%esp)
80102ae5:	e8 93 2f 00 00       	call   80105a7d <memmove>
    name[len] = 0;
80102aea:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102aed:	03 45 0c             	add    0xc(%ebp),%eax
80102af0:	c6 00 00             	movb   $0x0,(%eax)
  }
  while(*path == '/')
80102af3:	eb 04                	jmp    80102af9 <skipelem+0xa3>
    path++;
80102af5:	83 45 08 01          	addl   $0x1,0x8(%ebp)
    memmove(name, s, DIRSIZ);
  else {
    memmove(name, s, len);
    name[len] = 0;
  }
  while(*path == '/')
80102af9:	8b 45 08             	mov    0x8(%ebp),%eax
80102afc:	0f b6 00             	movzbl (%eax),%eax
80102aff:	3c 2f                	cmp    $0x2f,%al
80102b01:	74 f2                	je     80102af5 <skipelem+0x9f>
    path++;
  return path;
80102b03:	8b 45 08             	mov    0x8(%ebp),%eax
}
80102b06:	c9                   	leave  
80102b07:	c3                   	ret    

80102b08 <namex>:
// If parent != 0, return the inode for the parent and copy the final
// path element into name, which must have room for DIRSIZ bytes.
// Must be called inside a transaction since it calls iput().
static struct inode*
namex(char *path, int nameiparent, char *name)
{
80102b08:	55                   	push   %ebp
80102b09:	89 e5                	mov    %esp,%ebp
80102b0b:	83 ec 28             	sub    $0x28,%esp
  struct inode *ip, *next;

  if(*path == '/')
80102b0e:	8b 45 08             	mov    0x8(%ebp),%eax
80102b11:	0f b6 00             	movzbl (%eax),%eax
80102b14:	3c 2f                	cmp    $0x2f,%al
80102b16:	75 1c                	jne    80102b34 <namex+0x2c>
    ip = iget(ROOTDEV, ROOTINO);
80102b18:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80102b1f:	00 
80102b20:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80102b27:	e8 40 f4 ff ff       	call   80101f6c <iget>
80102b2c:	89 45 f0             	mov    %eax,-0x10(%ebp)
  else
    ip = idup(proc->cwd);

  while((path = skipelem(path, name)) != 0){
80102b2f:	e9 af 00 00 00       	jmp    80102be3 <namex+0xdb>
  struct inode *ip, *next;

  if(*path == '/')
    ip = iget(ROOTDEV, ROOTINO);
  else
    ip = idup(proc->cwd);
80102b34:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80102b3a:	8b 40 68             	mov    0x68(%eax),%eax
80102b3d:	89 04 24             	mov    %eax,(%esp)
80102b40:	e8 fa f4 ff ff       	call   8010203f <idup>
80102b45:	89 45 f0             	mov    %eax,-0x10(%ebp)

  while((path = skipelem(path, name)) != 0){
80102b48:	e9 96 00 00 00       	jmp    80102be3 <namex+0xdb>
    ilock(ip);
80102b4d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102b50:	89 04 24             	mov    %eax,(%esp)
80102b53:	e8 19 f5 ff ff       	call   80102071 <ilock>
    if(ip->type != T_DIR){
80102b58:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102b5b:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80102b5f:	66 83 f8 01          	cmp    $0x1,%ax
80102b63:	74 15                	je     80102b7a <namex+0x72>
      iunlockput(ip);
80102b65:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102b68:	89 04 24             	mov    %eax,(%esp)
80102b6b:	e8 8e f7 ff ff       	call   801022fe <iunlockput>
      return 0;
80102b70:	b8 00 00 00 00       	mov    $0x0,%eax
80102b75:	e9 a3 00 00 00       	jmp    80102c1d <namex+0x115>
    }
    if(nameiparent && *path == '\0'){
80102b7a:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
80102b7e:	74 1d                	je     80102b9d <namex+0x95>
80102b80:	8b 45 08             	mov    0x8(%ebp),%eax
80102b83:	0f b6 00             	movzbl (%eax),%eax
80102b86:	84 c0                	test   %al,%al
80102b88:	75 13                	jne    80102b9d <namex+0x95>
      // Stop one level early.
      iunlock(ip);
80102b8a:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102b8d:	89 04 24             	mov    %eax,(%esp)
80102b90:	e8 33 f6 ff ff       	call   801021c8 <iunlock>
      return ip;
80102b95:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102b98:	e9 80 00 00 00       	jmp    80102c1d <namex+0x115>
    }
    if((next = dirlookup(ip, name, 0)) == 0){
80102b9d:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80102ba4:	00 
80102ba5:	8b 45 10             	mov    0x10(%ebp),%eax
80102ba8:	89 44 24 04          	mov    %eax,0x4(%esp)
80102bac:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102baf:	89 04 24             	mov    %eax,(%esp)
80102bb2:	e8 df fc ff ff       	call   80102896 <dirlookup>
80102bb7:	89 45 f4             	mov    %eax,-0xc(%ebp)
80102bba:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80102bbe:	75 12                	jne    80102bd2 <namex+0xca>
      iunlockput(ip);
80102bc0:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102bc3:	89 04 24             	mov    %eax,(%esp)
80102bc6:	e8 33 f7 ff ff       	call   801022fe <iunlockput>
      return 0;
80102bcb:	b8 00 00 00 00       	mov    $0x0,%eax
80102bd0:	eb 4b                	jmp    80102c1d <namex+0x115>
    }
    iunlockput(ip);
80102bd2:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102bd5:	89 04 24             	mov    %eax,(%esp)
80102bd8:	e8 21 f7 ff ff       	call   801022fe <iunlockput>
    ip = next;
80102bdd:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102be0:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(*path == '/')
    ip = iget(ROOTDEV, ROOTINO);
  else
    ip = idup(proc->cwd);

  while((path = skipelem(path, name)) != 0){
80102be3:	8b 45 10             	mov    0x10(%ebp),%eax
80102be6:	89 44 24 04          	mov    %eax,0x4(%esp)
80102bea:	8b 45 08             	mov    0x8(%ebp),%eax
80102bed:	89 04 24             	mov    %eax,(%esp)
80102bf0:	e8 61 fe ff ff       	call   80102a56 <skipelem>
80102bf5:	89 45 08             	mov    %eax,0x8(%ebp)
80102bf8:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80102bfc:	0f 85 4b ff ff ff    	jne    80102b4d <namex+0x45>
      return 0;
    }
    iunlockput(ip);
    ip = next;
  }
  if(nameiparent){
80102c02:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
80102c06:	74 12                	je     80102c1a <namex+0x112>
    iput(ip);
80102c08:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102c0b:	89 04 24             	mov    %eax,(%esp)
80102c0e:	e8 1a f6 ff ff       	call   8010222d <iput>
    return 0;
80102c13:	b8 00 00 00 00       	mov    $0x0,%eax
80102c18:	eb 03                	jmp    80102c1d <namex+0x115>
  }
  return ip;
80102c1a:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
80102c1d:	c9                   	leave  
80102c1e:	c3                   	ret    

80102c1f <namei>:

struct inode*
namei(char *path)
{
80102c1f:	55                   	push   %ebp
80102c20:	89 e5                	mov    %esp,%ebp
80102c22:	83 ec 28             	sub    $0x28,%esp
  char name[DIRSIZ];
  return namex(path, 0, name);
80102c25:	8d 45 ea             	lea    -0x16(%ebp),%eax
80102c28:	89 44 24 08          	mov    %eax,0x8(%esp)
80102c2c:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80102c33:	00 
80102c34:	8b 45 08             	mov    0x8(%ebp),%eax
80102c37:	89 04 24             	mov    %eax,(%esp)
80102c3a:	e8 c9 fe ff ff       	call   80102b08 <namex>
}
80102c3f:	c9                   	leave  
80102c40:	c3                   	ret    

80102c41 <nameiparent>:

struct inode*
nameiparent(char *path, char *name)
{
80102c41:	55                   	push   %ebp
80102c42:	89 e5                	mov    %esp,%ebp
80102c44:	83 ec 18             	sub    $0x18,%esp
  return namex(path, 1, name);
80102c47:	8b 45 0c             	mov    0xc(%ebp),%eax
80102c4a:	89 44 24 08          	mov    %eax,0x8(%esp)
80102c4e:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80102c55:	00 
80102c56:	8b 45 08             	mov    0x8(%ebp),%eax
80102c59:	89 04 24             	mov    %eax,(%esp)
80102c5c:	e8 a7 fe ff ff       	call   80102b08 <namex>
}
80102c61:	c9                   	leave  
80102c62:	c3                   	ret    
	...

80102c64 <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
80102c64:	55                   	push   %ebp
80102c65:	89 e5                	mov    %esp,%ebp
80102c67:	83 ec 14             	sub    $0x14,%esp
80102c6a:	8b 45 08             	mov    0x8(%ebp),%eax
80102c6d:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102c71:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
80102c75:	89 c2                	mov    %eax,%edx
80102c77:	ec                   	in     (%dx),%al
80102c78:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80102c7b:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80102c7f:	c9                   	leave  
80102c80:	c3                   	ret    

80102c81 <insl>:

static inline void
insl(int port, void *addr, int cnt)
{
80102c81:	55                   	push   %ebp
80102c82:	89 e5                	mov    %esp,%ebp
80102c84:	57                   	push   %edi
80102c85:	53                   	push   %ebx
  asm volatile("cld; rep insl" :
80102c86:	8b 55 08             	mov    0x8(%ebp),%edx
80102c89:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80102c8c:	8b 45 10             	mov    0x10(%ebp),%eax
80102c8f:	89 cb                	mov    %ecx,%ebx
80102c91:	89 df                	mov    %ebx,%edi
80102c93:	89 c1                	mov    %eax,%ecx
80102c95:	fc                   	cld    
80102c96:	f3 6d                	rep insl (%dx),%es:(%edi)
80102c98:	89 c8                	mov    %ecx,%eax
80102c9a:	89 fb                	mov    %edi,%ebx
80102c9c:	89 5d 0c             	mov    %ebx,0xc(%ebp)
80102c9f:	89 45 10             	mov    %eax,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "d" (port), "0" (addr), "1" (cnt) :
               "memory", "cc");
}
80102ca2:	5b                   	pop    %ebx
80102ca3:	5f                   	pop    %edi
80102ca4:	5d                   	pop    %ebp
80102ca5:	c3                   	ret    

80102ca6 <outb>:

static inline void
outb(ushort port, uchar data)
{
80102ca6:	55                   	push   %ebp
80102ca7:	89 e5                	mov    %esp,%ebp
80102ca9:	83 ec 08             	sub    $0x8,%esp
80102cac:	8b 55 08             	mov    0x8(%ebp),%edx
80102caf:	8b 45 0c             	mov    0xc(%ebp),%eax
80102cb2:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80102cb6:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102cb9:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80102cbd:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80102cc1:	ee                   	out    %al,(%dx)
}
80102cc2:	c9                   	leave  
80102cc3:	c3                   	ret    

80102cc4 <outsl>:
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
}

static inline void
outsl(int port, const void *addr, int cnt)
{
80102cc4:	55                   	push   %ebp
80102cc5:	89 e5                	mov    %esp,%ebp
80102cc7:	56                   	push   %esi
80102cc8:	53                   	push   %ebx
  asm volatile("cld; rep outsl" :
80102cc9:	8b 55 08             	mov    0x8(%ebp),%edx
80102ccc:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80102ccf:	8b 45 10             	mov    0x10(%ebp),%eax
80102cd2:	89 cb                	mov    %ecx,%ebx
80102cd4:	89 de                	mov    %ebx,%esi
80102cd6:	89 c1                	mov    %eax,%ecx
80102cd8:	fc                   	cld    
80102cd9:	f3 6f                	rep outsl %ds:(%esi),(%dx)
80102cdb:	89 c8                	mov    %ecx,%eax
80102cdd:	89 f3                	mov    %esi,%ebx
80102cdf:	89 5d 0c             	mov    %ebx,0xc(%ebp)
80102ce2:	89 45 10             	mov    %eax,0x10(%ebp)
               "=S" (addr), "=c" (cnt) :
               "d" (port), "0" (addr), "1" (cnt) :
               "cc");
}
80102ce5:	5b                   	pop    %ebx
80102ce6:	5e                   	pop    %esi
80102ce7:	5d                   	pop    %ebp
80102ce8:	c3                   	ret    

80102ce9 <diskready>:
static int havedisk1;
static void idestart(struct buf*);

static int
diskready(int *status)
{
80102ce9:	55                   	push   %ebp
80102cea:	89 e5                	mov    %esp,%ebp
80102cec:	83 ec 04             	sub    $0x4,%esp
  *status = inb(IDE_DATA_PRIMARY+IDE_REG_STATUS);
80102cef:	c7 04 24 f7 01 00 00 	movl   $0x1f7,(%esp)
80102cf6:	e8 69 ff ff ff       	call   80102c64 <inb>
80102cfb:	0f b6 d0             	movzbl %al,%edx
80102cfe:	8b 45 08             	mov    0x8(%ebp),%eax
80102d01:	89 10                	mov    %edx,(%eax)
  return (*status & (IDE_BSY|IDE_DRDY)) == IDE_DRDY;
80102d03:	8b 45 08             	mov    0x8(%ebp),%eax
80102d06:	8b 00                	mov    (%eax),%eax
80102d08:	25 c0 00 00 00       	and    $0xc0,%eax
80102d0d:	83 f8 40             	cmp    $0x40,%eax
80102d10:	0f 94 c0             	sete   %al
80102d13:	0f b6 c0             	movzbl %al,%eax
}
80102d16:	c9                   	leave  
80102d17:	c3                   	ret    

80102d18 <idewait>:

// Wait for IDE disk to become ready.
static int
idewait(int checkerr)
{
80102d18:	55                   	push   %ebp
80102d19:	89 e5                	mov    %esp,%ebp
80102d1b:	83 ec 14             	sub    $0x14,%esp
  int r;

  while(!diskready(&r))
80102d1e:	8d 45 fc             	lea    -0x4(%ebp),%eax
80102d21:	89 04 24             	mov    %eax,(%esp)
80102d24:	e8 c0 ff ff ff       	call   80102ce9 <diskready>
80102d29:	85 c0                	test   %eax,%eax
80102d2b:	74 f1                	je     80102d1e <idewait+0x6>
    ;
  if(checkerr && (r & (IDE_DF|IDE_ERR)) != 0)
80102d2d:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80102d31:	74 11                	je     80102d44 <idewait+0x2c>
80102d33:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102d36:	83 e0 21             	and    $0x21,%eax
80102d39:	85 c0                	test   %eax,%eax
80102d3b:	74 07                	je     80102d44 <idewait+0x2c>
    return -1;
80102d3d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102d42:	eb 05                	jmp    80102d49 <idewait+0x31>
  return 0;
80102d44:	b8 00 00 00 00       	mov    $0x0,%eax
}
80102d49:	c9                   	leave  
80102d4a:	c3                   	ret    

80102d4b <ideinit>:

void
ideinit(void)
{
80102d4b:	55                   	push   %ebp
80102d4c:	89 e5                	mov    %esp,%ebp
80102d4e:	83 ec 28             	sub    $0x28,%esp
  int i;

  initlock(&idelock, "ide");
80102d51:	c7 44 24 04 52 90 10 	movl   $0x80109052,0x4(%esp)
80102d58:	80 
80102d59:	c7 04 24 40 c6 10 80 	movl   $0x8010c640,(%esp)
80102d60:	e8 c1 29 00 00       	call   80105726 <initlock>
  picenable(IRQ_IDE);
80102d65:	c7 04 24 0e 00 00 00 	movl   $0xe,(%esp)
80102d6c:	e8 54 17 00 00       	call   801044c5 <picenable>
  ioapicenable(IRQ_IDE, ncpu - 1);
80102d71:	a1 c0 41 11 80       	mov    0x801141c0,%eax
80102d76:	83 e8 01             	sub    $0x1,%eax
80102d79:	89 44 24 04          	mov    %eax,0x4(%esp)
80102d7d:	c7 04 24 0e 00 00 00 	movl   $0xe,(%esp)
80102d84:	e8 71 04 00 00       	call   801031fa <ioapicenable>
  idewait(0);
80102d89:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80102d90:	e8 83 ff ff ff       	call   80102d18 <idewait>

  // Check if disk 1 is present
  outb(IDE_DATA_PRIMARY+IDE_REG_DISK, IDE_DISK_LBA | (1<<4));
80102d95:	c7 44 24 04 f0 00 00 	movl   $0xf0,0x4(%esp)
80102d9c:	00 
80102d9d:	c7 04 24 f6 01 00 00 	movl   $0x1f6,(%esp)
80102da4:	e8 fd fe ff ff       	call   80102ca6 <outb>
  for(i=0; i<1000; i++){
80102da9:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80102db0:	eb 20                	jmp    80102dd2 <ideinit+0x87>
    if(inb(IDE_DATA_PRIMARY+IDE_REG_STATUS) != 0){
80102db2:	c7 04 24 f7 01 00 00 	movl   $0x1f7,(%esp)
80102db9:	e8 a6 fe ff ff       	call   80102c64 <inb>
80102dbe:	84 c0                	test   %al,%al
80102dc0:	74 0c                	je     80102dce <ideinit+0x83>
      havedisk1 = 1;
80102dc2:	c7 05 78 c6 10 80 01 	movl   $0x1,0x8010c678
80102dc9:	00 00 00 
      break;
80102dcc:	eb 0d                	jmp    80102ddb <ideinit+0x90>
  ioapicenable(IRQ_IDE, ncpu - 1);
  idewait(0);

  // Check if disk 1 is present
  outb(IDE_DATA_PRIMARY+IDE_REG_DISK, IDE_DISK_LBA | (1<<4));
  for(i=0; i<1000; i++){
80102dce:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80102dd2:	81 7d f4 e7 03 00 00 	cmpl   $0x3e7,-0xc(%ebp)
80102dd9:	7e d7                	jle    80102db2 <ideinit+0x67>
      break;
    }
  }

  // Switch back to disk 0.
  outb(IDE_DATA_PRIMARY+IDE_REG_DISK, IDE_DISK_LBA | (0<<4));
80102ddb:	c7 44 24 04 e0 00 00 	movl   $0xe0,0x4(%esp)
80102de2:	00 
80102de3:	c7 04 24 f6 01 00 00 	movl   $0x1f6,(%esp)
80102dea:	e8 b7 fe ff ff       	call   80102ca6 <outb>
}
80102def:	c9                   	leave  
80102df0:	c3                   	ret    

80102df1 <idestart>:

// Start the request for b.  Caller must hold idelock.
static void
idestart(struct buf *b)
{
80102df1:	55                   	push   %ebp
80102df2:	89 e5                	mov    %esp,%ebp
80102df4:	83 ec 28             	sub    $0x28,%esp
  if(b == 0)
80102df7:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80102dfb:	75 0c                	jne    80102e09 <idestart+0x18>
    panic("idestart");
80102dfd:	c7 04 24 56 90 10 80 	movl   $0x80109056,(%esp)
80102e04:	e8 60 da ff ff       	call   80100869 <panic>
  if(b->blockno >= FSSIZE)
80102e09:	8b 45 08             	mov    0x8(%ebp),%eax
80102e0c:	8b 40 08             	mov    0x8(%eax),%eax
80102e0f:	3d e7 03 00 00       	cmp    $0x3e7,%eax
80102e14:	76 0c                	jbe    80102e22 <idestart+0x31>
    panic("incorrect blockno");
80102e16:	c7 04 24 5f 90 10 80 	movl   $0x8010905f,(%esp)
80102e1d:	e8 47 da ff ff       	call   80100869 <panic>
  int sector_per_block =  BSIZE/SECTOR_SIZE;
80102e22:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
  int sector = b->blockno * sector_per_block;
80102e29:	8b 45 08             	mov    0x8(%ebp),%eax
80102e2c:	8b 50 08             	mov    0x8(%eax),%edx
80102e2f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102e32:	0f af c2             	imul   %edx,%eax
80102e35:	89 45 f4             	mov    %eax,-0xc(%ebp)

  if (sector_per_block > 7) panic("idestart");
80102e38:	83 7d f0 07          	cmpl   $0x7,-0x10(%ebp)
80102e3c:	7e 0c                	jle    80102e4a <idestart+0x59>
80102e3e:	c7 04 24 56 90 10 80 	movl   $0x80109056,(%esp)
80102e45:	e8 1f da ff ff       	call   80100869 <panic>

  idewait(0);
80102e4a:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80102e51:	e8 c2 fe ff ff       	call   80102d18 <idewait>
  outb(IDE_CTRL_PRIMARY+IDE_REG_CTRL, 0);  // generate interrupt
80102e56:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80102e5d:	00 
80102e5e:	c7 04 24 f6 03 00 00 	movl   $0x3f6,(%esp)
80102e65:	e8 3c fe ff ff       	call   80102ca6 <outb>
  outb(IDE_DATA_PRIMARY+IDE_REG_SECTORS, sector_per_block);
80102e6a:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102e6d:	0f b6 c0             	movzbl %al,%eax
80102e70:	89 44 24 04          	mov    %eax,0x4(%esp)
80102e74:	c7 04 24 f2 01 00 00 	movl   $0x1f2,(%esp)
80102e7b:	e8 26 fe ff ff       	call   80102ca6 <outb>
  outb(IDE_DATA_PRIMARY+IDE_REG_LBA0, sector & 0xff);
80102e80:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102e83:	0f b6 c0             	movzbl %al,%eax
80102e86:	89 44 24 04          	mov    %eax,0x4(%esp)
80102e8a:	c7 04 24 f3 01 00 00 	movl   $0x1f3,(%esp)
80102e91:	e8 10 fe ff ff       	call   80102ca6 <outb>
  outb(IDE_DATA_PRIMARY+IDE_REG_LBA1, (sector >> 8) & 0xff);
80102e96:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102e99:	c1 f8 08             	sar    $0x8,%eax
80102e9c:	0f b6 c0             	movzbl %al,%eax
80102e9f:	89 44 24 04          	mov    %eax,0x4(%esp)
80102ea3:	c7 04 24 f4 01 00 00 	movl   $0x1f4,(%esp)
80102eaa:	e8 f7 fd ff ff       	call   80102ca6 <outb>
  outb(IDE_DATA_PRIMARY+IDE_REG_LBA2, (sector >> 16) & 0xff);
80102eaf:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102eb2:	c1 f8 10             	sar    $0x10,%eax
80102eb5:	0f b6 c0             	movzbl %al,%eax
80102eb8:	89 44 24 04          	mov    %eax,0x4(%esp)
80102ebc:	c7 04 24 f5 01 00 00 	movl   $0x1f5,(%esp)
80102ec3:	e8 de fd ff ff       	call   80102ca6 <outb>
  outb(IDE_DATA_PRIMARY+IDE_REG_DISK,
80102ec8:	8b 45 08             	mov    0x8(%ebp),%eax
80102ecb:	8b 40 04             	mov    0x4(%eax),%eax
80102ece:	83 e0 01             	and    $0x1,%eax
80102ed1:	89 c2                	mov    %eax,%edx
80102ed3:	c1 e2 04             	shl    $0x4,%edx
    IDE_DISK_LBA | ((b->dev&1)<<4) | ((sector>>24)&0x0f));
80102ed6:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102ed9:	c1 f8 18             	sar    $0x18,%eax
  outb(IDE_CTRL_PRIMARY+IDE_REG_CTRL, 0);  // generate interrupt
  outb(IDE_DATA_PRIMARY+IDE_REG_SECTORS, sector_per_block);
  outb(IDE_DATA_PRIMARY+IDE_REG_LBA0, sector & 0xff);
  outb(IDE_DATA_PRIMARY+IDE_REG_LBA1, (sector >> 8) & 0xff);
  outb(IDE_DATA_PRIMARY+IDE_REG_LBA2, (sector >> 16) & 0xff);
  outb(IDE_DATA_PRIMARY+IDE_REG_DISK,
80102edc:	83 e0 0f             	and    $0xf,%eax
80102edf:	09 d0                	or     %edx,%eax
80102ee1:	83 c8 e0             	or     $0xffffffe0,%eax
80102ee4:	0f b6 c0             	movzbl %al,%eax
80102ee7:	89 44 24 04          	mov    %eax,0x4(%esp)
80102eeb:	c7 04 24 f6 01 00 00 	movl   $0x1f6,(%esp)
80102ef2:	e8 af fd ff ff       	call   80102ca6 <outb>
    IDE_DISK_LBA | ((b->dev&1)<<4) | ((sector>>24)&0x0f));
  if(b->flags & B_DIRTY){
80102ef7:	8b 45 08             	mov    0x8(%ebp),%eax
80102efa:	8b 00                	mov    (%eax),%eax
80102efc:	83 e0 04             	and    $0x4,%eax
80102eff:	85 c0                	test   %eax,%eax
80102f01:	74 34                	je     80102f37 <idestart+0x146>
    outb(IDE_DATA_PRIMARY+IDE_REG_COMMAND, IDE_CMD_WRITE);
80102f03:	c7 44 24 04 30 00 00 	movl   $0x30,0x4(%esp)
80102f0a:	00 
80102f0b:	c7 04 24 f7 01 00 00 	movl   $0x1f7,(%esp)
80102f12:	e8 8f fd ff ff       	call   80102ca6 <outb>
    outsl(IDE_DATA_PRIMARY+IDE_REG_DATA, b->data, BSIZE/4);
80102f17:	8b 45 08             	mov    0x8(%ebp),%eax
80102f1a:	83 c0 18             	add    $0x18,%eax
80102f1d:	c7 44 24 08 80 00 00 	movl   $0x80,0x8(%esp)
80102f24:	00 
80102f25:	89 44 24 04          	mov    %eax,0x4(%esp)
80102f29:	c7 04 24 f0 01 00 00 	movl   $0x1f0,(%esp)
80102f30:	e8 8f fd ff ff       	call   80102cc4 <outsl>
80102f35:	eb 14                	jmp    80102f4b <idestart+0x15a>
  } else {
    outb(IDE_DATA_PRIMARY+IDE_REG_COMMAND, IDE_CMD_READ);
80102f37:	c7 44 24 04 20 00 00 	movl   $0x20,0x4(%esp)
80102f3e:	00 
80102f3f:	c7 04 24 f7 01 00 00 	movl   $0x1f7,(%esp)
80102f46:	e8 5b fd ff ff       	call   80102ca6 <outb>
  }
}
80102f4b:	c9                   	leave  
80102f4c:	c3                   	ret    

80102f4d <ideintr>:

// Interrupt handler.
void
ideintr(void)
{
80102f4d:	55                   	push   %ebp
80102f4e:	89 e5                	mov    %esp,%ebp
80102f50:	83 ec 28             	sub    $0x28,%esp
  struct buf *b;

  // First queued buffer is the active request.
  acquire(&idelock);
80102f53:	c7 04 24 40 c6 10 80 	movl   $0x8010c640,(%esp)
80102f5a:	e8 e8 27 00 00       	call   80105747 <acquire>
  if((b = idequeue) == 0){
80102f5f:	a1 74 c6 10 80       	mov    0x8010c674,%eax
80102f64:	89 45 f4             	mov    %eax,-0xc(%ebp)
80102f67:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80102f6b:	75 11                	jne    80102f7e <ideintr+0x31>
    release(&idelock);
80102f6d:	c7 04 24 40 c6 10 80 	movl   $0x8010c640,(%esp)
80102f74:	e8 37 28 00 00       	call   801057b0 <release>
    // cprintf("spurious IDE interrupt\n");
    return;
80102f79:	e9 90 00 00 00       	jmp    8010300e <ideintr+0xc1>
  }
  idequeue = b->qnext;
80102f7e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102f81:	8b 40 14             	mov    0x14(%eax),%eax
80102f84:	a3 74 c6 10 80       	mov    %eax,0x8010c674

  // Read data if needed.
  if(!(b->flags & B_DIRTY) && idewait(1) >= 0)
80102f89:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102f8c:	8b 00                	mov    (%eax),%eax
80102f8e:	83 e0 04             	and    $0x4,%eax
80102f91:	85 c0                	test   %eax,%eax
80102f93:	75 2e                	jne    80102fc3 <ideintr+0x76>
80102f95:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80102f9c:	e8 77 fd ff ff       	call   80102d18 <idewait>
80102fa1:	85 c0                	test   %eax,%eax
80102fa3:	78 1e                	js     80102fc3 <ideintr+0x76>
    insl(IDE_DATA_PRIMARY+IDE_REG_DATA, b->data, BSIZE/4);
80102fa5:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102fa8:	83 c0 18             	add    $0x18,%eax
80102fab:	c7 44 24 08 80 00 00 	movl   $0x80,0x8(%esp)
80102fb2:	00 
80102fb3:	89 44 24 04          	mov    %eax,0x4(%esp)
80102fb7:	c7 04 24 f0 01 00 00 	movl   $0x1f0,(%esp)
80102fbe:	e8 be fc ff ff       	call   80102c81 <insl>

  // Wake process waiting for this buf.
  b->flags |= B_VALID;
80102fc3:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102fc6:	8b 00                	mov    (%eax),%eax
80102fc8:	89 c2                	mov    %eax,%edx
80102fca:	83 ca 02             	or     $0x2,%edx
80102fcd:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102fd0:	89 10                	mov    %edx,(%eax)
  b->flags &= ~B_DIRTY;
80102fd2:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102fd5:	8b 00                	mov    (%eax),%eax
80102fd7:	89 c2                	mov    %eax,%edx
80102fd9:	83 e2 fb             	and    $0xfffffffb,%edx
80102fdc:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102fdf:	89 10                	mov    %edx,(%eax)
  wakeup(b);
80102fe1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102fe4:	89 04 24             	mov    %eax,(%esp)
80102fe7:	e8 0f 25 00 00       	call   801054fb <wakeup>

  // Start disk on next buf in queue.
  if(idequeue != 0)
80102fec:	a1 74 c6 10 80       	mov    0x8010c674,%eax
80102ff1:	85 c0                	test   %eax,%eax
80102ff3:	74 0d                	je     80103002 <ideintr+0xb5>
    idestart(idequeue);
80102ff5:	a1 74 c6 10 80       	mov    0x8010c674,%eax
80102ffa:	89 04 24             	mov    %eax,(%esp)
80102ffd:	e8 ef fd ff ff       	call   80102df1 <idestart>

  release(&idelock);
80103002:	c7 04 24 40 c6 10 80 	movl   $0x8010c640,(%esp)
80103009:	e8 a2 27 00 00       	call   801057b0 <release>
}
8010300e:	c9                   	leave  
8010300f:	c3                   	ret    

80103010 <iderw>:
// Sync buf with disk.
// If B_DIRTY is set, write buf to disk, clear B_DIRTY, set B_VALID.
// Else if B_VALID is not set, read buf from disk, set B_VALID.
void
iderw(struct buf *b)
{
80103010:	55                   	push   %ebp
80103011:	89 e5                	mov    %esp,%ebp
80103013:	83 ec 28             	sub    $0x28,%esp
  struct buf **pp;

  if(!(b->flags & B_BUSY))
80103016:	8b 45 08             	mov    0x8(%ebp),%eax
80103019:	8b 00                	mov    (%eax),%eax
8010301b:	83 e0 01             	and    $0x1,%eax
8010301e:	85 c0                	test   %eax,%eax
80103020:	75 0c                	jne    8010302e <iderw+0x1e>
    panic("iderw: buf not busy");
80103022:	c7 04 24 71 90 10 80 	movl   $0x80109071,(%esp)
80103029:	e8 3b d8 ff ff       	call   80100869 <panic>
  if((b->flags & (B_VALID|B_DIRTY)) == B_VALID)
8010302e:	8b 45 08             	mov    0x8(%ebp),%eax
80103031:	8b 00                	mov    (%eax),%eax
80103033:	83 e0 06             	and    $0x6,%eax
80103036:	83 f8 02             	cmp    $0x2,%eax
80103039:	75 0c                	jne    80103047 <iderw+0x37>
    panic("iderw: nothing to do");
8010303b:	c7 04 24 85 90 10 80 	movl   $0x80109085,(%esp)
80103042:	e8 22 d8 ff ff       	call   80100869 <panic>
  if(b->dev != 0 && !havedisk1)
80103047:	8b 45 08             	mov    0x8(%ebp),%eax
8010304a:	8b 40 04             	mov    0x4(%eax),%eax
8010304d:	85 c0                	test   %eax,%eax
8010304f:	74 15                	je     80103066 <iderw+0x56>
80103051:	a1 78 c6 10 80       	mov    0x8010c678,%eax
80103056:	85 c0                	test   %eax,%eax
80103058:	75 0c                	jne    80103066 <iderw+0x56>
    panic("iderw: ide disk 1 not present");
8010305a:	c7 04 24 9a 90 10 80 	movl   $0x8010909a,(%esp)
80103061:	e8 03 d8 ff ff       	call   80100869 <panic>

  acquire(&idelock);  //DOC:acquire-lock
80103066:	c7 04 24 40 c6 10 80 	movl   $0x8010c640,(%esp)
8010306d:	e8 d5 26 00 00       	call   80105747 <acquire>

  // Append b to idequeue.
  b->qnext = 0;
80103072:	8b 45 08             	mov    0x8(%ebp),%eax
80103075:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
8010307c:	c7 45 f4 74 c6 10 80 	movl   $0x8010c674,-0xc(%ebp)
80103083:	eb 0b                	jmp    80103090 <iderw+0x80>
80103085:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103088:	8b 00                	mov    (%eax),%eax
8010308a:	83 c0 14             	add    $0x14,%eax
8010308d:	89 45 f4             	mov    %eax,-0xc(%ebp)
80103090:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103093:	8b 00                	mov    (%eax),%eax
80103095:	85 c0                	test   %eax,%eax
80103097:	75 ec                	jne    80103085 <iderw+0x75>
    ;
  *pp = b;
80103099:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010309c:	8b 55 08             	mov    0x8(%ebp),%edx
8010309f:	89 10                	mov    %edx,(%eax)

  // Start disk if necessary.
  if(idequeue == b)
801030a1:	a1 74 c6 10 80       	mov    0x8010c674,%eax
801030a6:	3b 45 08             	cmp    0x8(%ebp),%eax
801030a9:	75 22                	jne    801030cd <iderw+0xbd>
    idestart(b);
801030ab:	8b 45 08             	mov    0x8(%ebp),%eax
801030ae:	89 04 24             	mov    %eax,(%esp)
801030b1:	e8 3b fd ff ff       	call   80102df1 <idestart>

  // Wait for request to finish.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
801030b6:	eb 16                	jmp    801030ce <iderw+0xbe>
    sleep(b, &idelock);
801030b8:	c7 44 24 04 40 c6 10 	movl   $0x8010c640,0x4(%esp)
801030bf:	80 
801030c0:	8b 45 08             	mov    0x8(%ebp),%eax
801030c3:	89 04 24             	mov    %eax,(%esp)
801030c6:	e8 53 23 00 00       	call   8010541e <sleep>
801030cb:	eb 01                	jmp    801030ce <iderw+0xbe>
  // Start disk if necessary.
  if(idequeue == b)
    idestart(b);

  // Wait for request to finish.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
801030cd:	90                   	nop
801030ce:	8b 45 08             	mov    0x8(%ebp),%eax
801030d1:	8b 00                	mov    (%eax),%eax
801030d3:	83 e0 06             	and    $0x6,%eax
801030d6:	83 f8 02             	cmp    $0x2,%eax
801030d9:	75 dd                	jne    801030b8 <iderw+0xa8>
    sleep(b, &idelock);
  }

  release(&idelock);
801030db:	c7 04 24 40 c6 10 80 	movl   $0x8010c640,(%esp)
801030e2:	e8 c9 26 00 00       	call   801057b0 <release>
}
801030e7:	c9                   	leave  
801030e8:	c3                   	ret    
801030e9:	00 00                	add    %al,(%eax)
	...

801030ec <ioapicread>:
  uint data;    // offset  10
};

static uint
ioapicread(int reg)
{
801030ec:	55                   	push   %ebp
801030ed:	89 e5                	mov    %esp,%ebp
  ioapic->reg = (reg & 0xFF);  // bits 0..7, see datasheet
801030ef:	a1 94 3a 11 80       	mov    0x80113a94,%eax
801030f4:	8b 55 08             	mov    0x8(%ebp),%edx
801030f7:	81 e2 ff 00 00 00    	and    $0xff,%edx
801030fd:	89 10                	mov    %edx,(%eax)
  return ioapic->data;
801030ff:	a1 94 3a 11 80       	mov    0x80113a94,%eax
80103104:	8b 40 10             	mov    0x10(%eax),%eax
}
80103107:	5d                   	pop    %ebp
80103108:	c3                   	ret    

80103109 <ioapicwrite>:

static void
ioapicwrite(int reg, uint data)
{
80103109:	55                   	push   %ebp
8010310a:	89 e5                	mov    %esp,%ebp
  ioapic->reg = (reg & 0xFF);  // bits 0..7, see datasheet
8010310c:	a1 94 3a 11 80       	mov    0x80113a94,%eax
80103111:	8b 55 08             	mov    0x8(%ebp),%edx
80103114:	81 e2 ff 00 00 00    	and    $0xff,%edx
8010311a:	89 10                	mov    %edx,(%eax)
  ioapic->data = data;
8010311c:	a1 94 3a 11 80       	mov    0x80113a94,%eax
80103121:	8b 55 0c             	mov    0xc(%ebp),%edx
80103124:	89 50 10             	mov    %edx,0x10(%eax)
}
80103127:	5d                   	pop    %ebp
80103128:	c3                   	ret    

80103129 <ioapicinit>:

void
ioapicinit(void)
{
80103129:	55                   	push   %ebp
8010312a:	89 e5                	mov    %esp,%ebp
8010312c:	83 ec 28             	sub    $0x28,%esp
  int i, id;

  if(!ismp)
8010312f:	a1 c4 3b 11 80       	mov    0x80113bc4,%eax
80103134:	85 c0                	test   %eax,%eax
80103136:	0f 84 bb 00 00 00    	je     801031f7 <ioapicinit+0xce>
    return;

  if(ioapic == 0) {
8010313c:	a1 94 3a 11 80       	mov    0x80113a94,%eax
80103141:	85 c0                	test   %eax,%eax
80103143:	75 16                	jne    8010315b <ioapicinit+0x32>
    ioapic = (volatile struct ioapic*)IOAPIC;
80103145:	c7 05 94 3a 11 80 00 	movl   $0xfec00000,0x80113a94
8010314c:	00 c0 fe 
    cprintf("ioapicinit: falling back to default ioapic address\n");
8010314f:	c7 04 24 b8 90 10 80 	movl   $0x801090b8,(%esp)
80103156:	e8 6e d5 ff ff       	call   801006c9 <cprintf>
  }
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF; // bits 16..23, see datasheet
8010315b:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80103162:	e8 85 ff ff ff       	call   801030ec <ioapicread>
80103167:	c1 e8 10             	shr    $0x10,%eax
8010316a:	25 ff 00 00 00       	and    $0xff,%eax
8010316f:	a3 7c c6 10 80       	mov    %eax,0x8010c67c
  id = (ioapicread(REG_ID) >> 24) & 0x0F;  // bits 24..27, see datasheet
80103174:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
8010317b:	e8 6c ff ff ff       	call   801030ec <ioapicread>
80103180:	c1 e8 18             	shr    $0x18,%eax
80103183:	83 e0 0f             	and    $0xf,%eax
80103186:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(id != ioapicid)
80103189:	0f b6 05 c0 3b 11 80 	movzbl 0x80113bc0,%eax
80103190:	0f b6 c0             	movzbl %al,%eax
80103193:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103196:	74 0c                	je     801031a4 <ioapicinit+0x7b>
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");
80103198:	c7 04 24 ec 90 10 80 	movl   $0x801090ec,(%esp)
8010319f:	e8 25 d5 ff ff       	call   801006c9 <cprintf>

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
801031a4:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
801031ab:	eb 3e                	jmp    801031eb <ioapicinit+0xc2>
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
801031ad:	8b 45 f0             	mov    -0x10(%ebp),%eax
801031b0:	83 c0 20             	add    $0x20,%eax
801031b3:	0d 00 00 01 00       	or     $0x10000,%eax
801031b8:	8b 55 f0             	mov    -0x10(%ebp),%edx
801031bb:	83 c2 08             	add    $0x8,%edx
801031be:	01 d2                	add    %edx,%edx
801031c0:	89 44 24 04          	mov    %eax,0x4(%esp)
801031c4:	89 14 24             	mov    %edx,(%esp)
801031c7:	e8 3d ff ff ff       	call   80103109 <ioapicwrite>
    ioapicwrite(REG_TABLE+2*i+1, 0);
801031cc:	8b 45 f0             	mov    -0x10(%ebp),%eax
801031cf:	83 c0 08             	add    $0x8,%eax
801031d2:	01 c0                	add    %eax,%eax
801031d4:	83 c0 01             	add    $0x1,%eax
801031d7:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801031de:	00 
801031df:	89 04 24             	mov    %eax,(%esp)
801031e2:	e8 22 ff ff ff       	call   80103109 <ioapicwrite>
  if(id != ioapicid)
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
801031e7:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
801031eb:	a1 7c c6 10 80       	mov    0x8010c67c,%eax
801031f0:	39 45 f0             	cmp    %eax,-0x10(%ebp)
801031f3:	7e b8                	jle    801031ad <ioapicinit+0x84>
801031f5:	eb 01                	jmp    801031f8 <ioapicinit+0xcf>
ioapicinit(void)
{
  int i, id;

  if(!ismp)
    return;
801031f7:	90                   	nop
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
    ioapicwrite(REG_TABLE+2*i+1, 0);
  }
}
801031f8:	c9                   	leave  
801031f9:	c3                   	ret    

801031fa <ioapicenable>:

void
ioapicenable(int irq, int cpunum)
{
801031fa:	55                   	push   %ebp
801031fb:	89 e5                	mov    %esp,%ebp
801031fd:	83 ec 18             	sub    $0x18,%esp
  if(!ismp)
80103200:	a1 c4 3b 11 80       	mov    0x80113bc4,%eax
80103205:	85 c0                	test   %eax,%eax
80103207:	74 63                	je     8010326c <ioapicenable+0x72>
    return;

  if(irq > maxintr)
80103209:	a1 7c c6 10 80       	mov    0x8010c67c,%eax
8010320e:	39 45 08             	cmp    %eax,0x8(%ebp)
80103211:	7e 1c                	jle    8010322f <ioapicenable+0x35>
    cprintf("ioapicenable: no irq %d, maximum is %d\n", irq, maxintr);
80103213:	a1 7c c6 10 80       	mov    0x8010c67c,%eax
80103218:	89 44 24 08          	mov    %eax,0x8(%esp)
8010321c:	8b 45 08             	mov    0x8(%ebp),%eax
8010321f:	89 44 24 04          	mov    %eax,0x4(%esp)
80103223:	c7 04 24 20 91 10 80 	movl   $0x80109120,(%esp)
8010322a:	e8 9a d4 ff ff       	call   801006c9 <cprintf>

  cpunum = cpunum & 0x0F; // used as APIC id below, 4 bits, see datasheet
8010322f:	83 65 0c 0f          	andl   $0xf,0xc(%ebp)

  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
80103233:	8b 45 08             	mov    0x8(%ebp),%eax
80103236:	83 c0 20             	add    $0x20,%eax
80103239:	8b 55 08             	mov    0x8(%ebp),%edx
8010323c:	83 c2 08             	add    $0x8,%edx
8010323f:	01 d2                	add    %edx,%edx
80103241:	89 44 24 04          	mov    %eax,0x4(%esp)
80103245:	89 14 24             	mov    %edx,(%esp)
80103248:	e8 bc fe ff ff       	call   80103109 <ioapicwrite>
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24); // bits 56..59, see datasheet
8010324d:	8b 45 0c             	mov    0xc(%ebp),%eax
80103250:	c1 e0 18             	shl    $0x18,%eax
80103253:	8b 55 08             	mov    0x8(%ebp),%edx
80103256:	83 c2 08             	add    $0x8,%edx
80103259:	01 d2                	add    %edx,%edx
8010325b:	83 c2 01             	add    $0x1,%edx
8010325e:	89 44 24 04          	mov    %eax,0x4(%esp)
80103262:	89 14 24             	mov    %edx,(%esp)
80103265:	e8 9f fe ff ff       	call   80103109 <ioapicwrite>
8010326a:	eb 01                	jmp    8010326d <ioapicenable+0x73>

void
ioapicenable(int irq, int cpunum)
{
  if(!ismp)
    return;
8010326c:	90                   	nop
  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24); // bits 56..59, see datasheet
}
8010326d:	c9                   	leave  
8010326e:	c3                   	ret    
	...

80103270 <kinit1>:
// the pages mapped by entrypgdir on free list.
// 2. main() calls kinit2() with the rest of the physical pages
// after installing a full page table that maps them on all cores.
void
kinit1(void *vstart, void *vend)
{
80103270:	55                   	push   %ebp
80103271:	89 e5                	mov    %esp,%ebp
80103273:	83 ec 18             	sub    $0x18,%esp
  initlock(&kmem.lock, "kmem");
80103276:	c7 44 24 04 48 91 10 	movl   $0x80109148,0x4(%esp)
8010327d:	80 
8010327e:	c7 04 24 a0 3a 11 80 	movl   $0x80113aa0,(%esp)
80103285:	e8 9c 24 00 00       	call   80105726 <initlock>
  kmem.use_lock = 0;
8010328a:	c7 05 d4 3a 11 80 00 	movl   $0x0,0x80113ad4
80103291:	00 00 00 
  freerange(vstart, vend);
80103294:	8b 45 0c             	mov    0xc(%ebp),%eax
80103297:	89 44 24 04          	mov    %eax,0x4(%esp)
8010329b:	8b 45 08             	mov    0x8(%ebp),%eax
8010329e:	89 04 24             	mov    %eax,(%esp)
801032a1:	e8 26 00 00 00       	call   801032cc <freerange>
}
801032a6:	c9                   	leave  
801032a7:	c3                   	ret    

801032a8 <kinit2>:

void
kinit2(void *vstart, void *vend)
{
801032a8:	55                   	push   %ebp
801032a9:	89 e5                	mov    %esp,%ebp
801032ab:	83 ec 18             	sub    $0x18,%esp
  freerange(vstart, vend);
801032ae:	8b 45 0c             	mov    0xc(%ebp),%eax
801032b1:	89 44 24 04          	mov    %eax,0x4(%esp)
801032b5:	8b 45 08             	mov    0x8(%ebp),%eax
801032b8:	89 04 24             	mov    %eax,(%esp)
801032bb:	e8 0c 00 00 00       	call   801032cc <freerange>
  kmem.use_lock = 1;
801032c0:	c7 05 d4 3a 11 80 01 	movl   $0x1,0x80113ad4
801032c7:	00 00 00 
}
801032ca:	c9                   	leave  
801032cb:	c3                   	ret    

801032cc <freerange>:

void
freerange(void *vstart, void *vend)
{
801032cc:	55                   	push   %ebp
801032cd:	89 e5                	mov    %esp,%ebp
801032cf:	83 ec 28             	sub    $0x28,%esp
  char *p;
  p = (char*)PGROUNDUP((uint)vstart);
801032d2:	8b 45 08             	mov    0x8(%ebp),%eax
801032d5:	05 ff 0f 00 00       	add    $0xfff,%eax
801032da:	25 00 f0 ff ff       	and    $0xfffff000,%eax
801032df:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801032e2:	eb 12                	jmp    801032f6 <freerange+0x2a>
    kfree(p);
801032e4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801032e7:	89 04 24             	mov    %eax,(%esp)
801032ea:	e8 19 00 00 00       	call   80103308 <kfree>
void
freerange(void *vstart, void *vend)
{
  char *p;
  p = (char*)PGROUNDUP((uint)vstart);
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801032ef:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
801032f6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801032f9:	8d 90 00 10 00 00    	lea    0x1000(%eax),%edx
801032ff:	8b 45 0c             	mov    0xc(%ebp),%eax
80103302:	39 c2                	cmp    %eax,%edx
80103304:	76 de                	jbe    801032e4 <freerange+0x18>
    kfree(p);
}
80103306:	c9                   	leave  
80103307:	c3                   	ret    

80103308 <kfree>:
// which normally should have been returned by a
// call to kalloc().  (The exception is when
// initializing the allocator; see kinit above.)
void
kfree(char *v)
{
80103308:	55                   	push   %ebp
80103309:	89 e5                	mov    %esp,%ebp
8010330b:	83 ec 28             	sub    $0x28,%esp
  struct run *r;

  if((uint)v % PGSIZE || v < end || V2P(v) >= PHYSTOP)
8010330e:	8b 45 08             	mov    0x8(%ebp),%eax
80103311:	25 ff 0f 00 00       	and    $0xfff,%eax
80103316:	85 c0                	test   %eax,%eax
80103318:	75 18                	jne    80103332 <kfree+0x2a>
8010331a:	81 7d 08 78 6b 11 80 	cmpl   $0x80116b78,0x8(%ebp)
80103321:	72 0f                	jb     80103332 <kfree+0x2a>
80103323:	8b 45 08             	mov    0x8(%ebp),%eax
80103326:	2d 00 00 00 80       	sub    $0x80000000,%eax
8010332b:	3d ff ff ff 0d       	cmp    $0xdffffff,%eax
80103330:	76 0c                	jbe    8010333e <kfree+0x36>
    panic("kfree");
80103332:	c7 04 24 4d 91 10 80 	movl   $0x8010914d,(%esp)
80103339:	e8 2b d5 ff ff       	call   80100869 <panic>

  // Fill with junk to catch dangling refs.
  memset(v, 1, PGSIZE);
8010333e:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
80103345:	00 
80103346:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
8010334d:	00 
8010334e:	8b 45 08             	mov    0x8(%ebp),%eax
80103351:	89 04 24             	mov    %eax,(%esp)
80103354:	e8 51 26 00 00       	call   801059aa <memset>

  if(kmem.use_lock)
80103359:	a1 d4 3a 11 80       	mov    0x80113ad4,%eax
8010335e:	85 c0                	test   %eax,%eax
80103360:	74 0c                	je     8010336e <kfree+0x66>
    acquire(&kmem.lock);
80103362:	c7 04 24 a0 3a 11 80 	movl   $0x80113aa0,(%esp)
80103369:	e8 d9 23 00 00       	call   80105747 <acquire>
  r = (struct run*)v;
8010336e:	8b 45 08             	mov    0x8(%ebp),%eax
80103371:	89 45 f4             	mov    %eax,-0xc(%ebp)
  r->next = kmem.freelist;
80103374:	8b 15 d8 3a 11 80    	mov    0x80113ad8,%edx
8010337a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010337d:	89 10                	mov    %edx,(%eax)
  kmem.freelist = r;
8010337f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103382:	a3 d8 3a 11 80       	mov    %eax,0x80113ad8
  if(kmem.use_lock)
80103387:	a1 d4 3a 11 80       	mov    0x80113ad4,%eax
8010338c:	85 c0                	test   %eax,%eax
8010338e:	74 0c                	je     8010339c <kfree+0x94>
    release(&kmem.lock);
80103390:	c7 04 24 a0 3a 11 80 	movl   $0x80113aa0,(%esp)
80103397:	e8 14 24 00 00       	call   801057b0 <release>
}
8010339c:	c9                   	leave  
8010339d:	c3                   	ret    

8010339e <kalloc>:
// Allocate one 4096-byte page of physical memory.
// Returns a pointer that the kernel can use.
// Returns 0 if the memory cannot be allocated.
char*
kalloc(void)
{
8010339e:	55                   	push   %ebp
8010339f:	89 e5                	mov    %esp,%ebp
801033a1:	83 ec 28             	sub    $0x28,%esp
  struct run *r;

  if(kmem.use_lock)
801033a4:	a1 d4 3a 11 80       	mov    0x80113ad4,%eax
801033a9:	85 c0                	test   %eax,%eax
801033ab:	74 0c                	je     801033b9 <kalloc+0x1b>
    acquire(&kmem.lock);
801033ad:	c7 04 24 a0 3a 11 80 	movl   $0x80113aa0,(%esp)
801033b4:	e8 8e 23 00 00       	call   80105747 <acquire>
  r = kmem.freelist;
801033b9:	a1 d8 3a 11 80       	mov    0x80113ad8,%eax
801033be:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(r)
801033c1:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801033c5:	74 0a                	je     801033d1 <kalloc+0x33>
    kmem.freelist = r->next;
801033c7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801033ca:	8b 00                	mov    (%eax),%eax
801033cc:	a3 d8 3a 11 80       	mov    %eax,0x80113ad8
  if(kmem.use_lock)
801033d1:	a1 d4 3a 11 80       	mov    0x80113ad4,%eax
801033d6:	85 c0                	test   %eax,%eax
801033d8:	74 0c                	je     801033e6 <kalloc+0x48>
    release(&kmem.lock);
801033da:	c7 04 24 a0 3a 11 80 	movl   $0x80113aa0,(%esp)
801033e1:	e8 ca 23 00 00       	call   801057b0 <release>
  return (char*)r;
801033e6:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801033e9:	c9                   	leave  
801033ea:	c3                   	ret    
	...

801033ec <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
801033ec:	55                   	push   %ebp
801033ed:	89 e5                	mov    %esp,%ebp
801033ef:	83 ec 14             	sub    $0x14,%esp
801033f2:	8b 45 08             	mov    0x8(%ebp),%eax
801033f5:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801033f9:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
801033fd:	89 c2                	mov    %eax,%edx
801033ff:	ec                   	in     (%dx),%al
80103400:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80103403:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80103407:	c9                   	leave  
80103408:	c3                   	ret    

80103409 <kbdgetc>:
#include "defs.h"
#include "kbd.h"

int
kbdgetc(void)
{
80103409:	55                   	push   %ebp
8010340a:	89 e5                	mov    %esp,%ebp
8010340c:	83 ec 14             	sub    $0x14,%esp
  static uchar *charcode[4] = {
    normalmap, shiftmap, ctlmap, ctlmap
  };
  uint st, data, c;

  st = inb(KBSTATP);
8010340f:	c7 04 24 64 00 00 00 	movl   $0x64,(%esp)
80103416:	e8 d1 ff ff ff       	call   801033ec <inb>
8010341b:	0f b6 c0             	movzbl %al,%eax
8010341e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((st & KBS_DIB) == 0)
80103421:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103424:	83 e0 01             	and    $0x1,%eax
80103427:	85 c0                	test   %eax,%eax
80103429:	75 0a                	jne    80103435 <kbdgetc+0x2c>
    return -1;
8010342b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80103430:	e9 20 01 00 00       	jmp    80103555 <kbdgetc+0x14c>
  data = inb(KBDATAP);
80103435:	c7 04 24 60 00 00 00 	movl   $0x60,(%esp)
8010343c:	e8 ab ff ff ff       	call   801033ec <inb>
80103441:	0f b6 c0             	movzbl %al,%eax
80103444:	89 45 f8             	mov    %eax,-0x8(%ebp)

  if(data == 0xE0){
80103447:	81 7d f8 e0 00 00 00 	cmpl   $0xe0,-0x8(%ebp)
8010344e:	75 17                	jne    80103467 <kbdgetc+0x5e>
    shift |= E0ESC;
80103450:	a1 80 c6 10 80       	mov    0x8010c680,%eax
80103455:	83 c8 40             	or     $0x40,%eax
80103458:	a3 80 c6 10 80       	mov    %eax,0x8010c680
    return 0;
8010345d:	b8 00 00 00 00       	mov    $0x0,%eax
80103462:	e9 ee 00 00 00       	jmp    80103555 <kbdgetc+0x14c>
  } else if(data & 0x80){
80103467:	8b 45 f8             	mov    -0x8(%ebp),%eax
8010346a:	25 80 00 00 00       	and    $0x80,%eax
8010346f:	85 c0                	test   %eax,%eax
80103471:	74 44                	je     801034b7 <kbdgetc+0xae>
    // Key released
    data = ((shift & E0ESC) ? data : data & 0x7F);
80103473:	a1 80 c6 10 80       	mov    0x8010c680,%eax
80103478:	83 e0 40             	and    $0x40,%eax
8010347b:	85 c0                	test   %eax,%eax
8010347d:	75 08                	jne    80103487 <kbdgetc+0x7e>
8010347f:	8b 45 f8             	mov    -0x8(%ebp),%eax
80103482:	83 e0 7f             	and    $0x7f,%eax
80103485:	eb 03                	jmp    8010348a <kbdgetc+0x81>
80103487:	8b 45 f8             	mov    -0x8(%ebp),%eax
8010348a:	89 45 f8             	mov    %eax,-0x8(%ebp)
    shift &= ~(shiftcode[data] | E0ESC);
8010348d:	8b 45 f8             	mov    -0x8(%ebp),%eax
80103490:	0f b6 80 40 a0 10 80 	movzbl -0x7fef5fc0(%eax),%eax
80103497:	83 c8 40             	or     $0x40,%eax
8010349a:	0f b6 c0             	movzbl %al,%eax
8010349d:	f7 d0                	not    %eax
8010349f:	89 c2                	mov    %eax,%edx
801034a1:	a1 80 c6 10 80       	mov    0x8010c680,%eax
801034a6:	21 d0                	and    %edx,%eax
801034a8:	a3 80 c6 10 80       	mov    %eax,0x8010c680
    return 0;
801034ad:	b8 00 00 00 00       	mov    $0x0,%eax
801034b2:	e9 9e 00 00 00       	jmp    80103555 <kbdgetc+0x14c>
  } else if(shift & E0ESC){
801034b7:	a1 80 c6 10 80       	mov    0x8010c680,%eax
801034bc:	83 e0 40             	and    $0x40,%eax
801034bf:	85 c0                	test   %eax,%eax
801034c1:	74 14                	je     801034d7 <kbdgetc+0xce>
    // Last character was an E0 escape; or with 0x80
    data |= 0x80;
801034c3:	81 4d f8 80 00 00 00 	orl    $0x80,-0x8(%ebp)
    shift &= ~E0ESC;
801034ca:	a1 80 c6 10 80       	mov    0x8010c680,%eax
801034cf:	83 e0 bf             	and    $0xffffffbf,%eax
801034d2:	a3 80 c6 10 80       	mov    %eax,0x8010c680
  }

  shift |= shiftcode[data];
801034d7:	8b 45 f8             	mov    -0x8(%ebp),%eax
801034da:	0f b6 80 40 a0 10 80 	movzbl -0x7fef5fc0(%eax),%eax
801034e1:	0f b6 d0             	movzbl %al,%edx
801034e4:	a1 80 c6 10 80       	mov    0x8010c680,%eax
801034e9:	09 d0                	or     %edx,%eax
801034eb:	a3 80 c6 10 80       	mov    %eax,0x8010c680
  shift ^= togglecode[data];
801034f0:	8b 45 f8             	mov    -0x8(%ebp),%eax
801034f3:	0f b6 80 40 a1 10 80 	movzbl -0x7fef5ec0(%eax),%eax
801034fa:	0f b6 d0             	movzbl %al,%edx
801034fd:	a1 80 c6 10 80       	mov    0x8010c680,%eax
80103502:	31 d0                	xor    %edx,%eax
80103504:	a3 80 c6 10 80       	mov    %eax,0x8010c680
  c = charcode[shift & (CTL | SHIFT)][data];
80103509:	a1 80 c6 10 80       	mov    0x8010c680,%eax
8010350e:	83 e0 03             	and    $0x3,%eax
80103511:	8b 04 85 40 a5 10 80 	mov    -0x7fef5ac0(,%eax,4),%eax
80103518:	03 45 f8             	add    -0x8(%ebp),%eax
8010351b:	0f b6 00             	movzbl (%eax),%eax
8010351e:	0f b6 c0             	movzbl %al,%eax
80103521:	89 45 fc             	mov    %eax,-0x4(%ebp)
  if(shift & CAPSLOCK){
80103524:	a1 80 c6 10 80       	mov    0x8010c680,%eax
80103529:	83 e0 08             	and    $0x8,%eax
8010352c:	85 c0                	test   %eax,%eax
8010352e:	74 22                	je     80103552 <kbdgetc+0x149>
    if('a' <= c && c <= 'z')
80103530:	83 7d fc 60          	cmpl   $0x60,-0x4(%ebp)
80103534:	76 0c                	jbe    80103542 <kbdgetc+0x139>
80103536:	83 7d fc 7a          	cmpl   $0x7a,-0x4(%ebp)
8010353a:	77 06                	ja     80103542 <kbdgetc+0x139>
      c += 'A' - 'a';
8010353c:	83 6d fc 20          	subl   $0x20,-0x4(%ebp)

  shift |= shiftcode[data];
  shift ^= togglecode[data];
  c = charcode[shift & (CTL | SHIFT)][data];
  if(shift & CAPSLOCK){
    if('a' <= c && c <= 'z')
80103540:	eb 10                	jmp    80103552 <kbdgetc+0x149>
      c += 'A' - 'a';
    else if('A' <= c && c <= 'Z')
80103542:	83 7d fc 40          	cmpl   $0x40,-0x4(%ebp)
80103546:	76 0a                	jbe    80103552 <kbdgetc+0x149>
80103548:	83 7d fc 5a          	cmpl   $0x5a,-0x4(%ebp)
8010354c:	77 04                	ja     80103552 <kbdgetc+0x149>
      c += 'a' - 'A';
8010354e:	83 45 fc 20          	addl   $0x20,-0x4(%ebp)
  }
  return c;
80103552:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80103555:	c9                   	leave  
80103556:	c3                   	ret    

80103557 <kbdintr>:

void
kbdintr(void)
{
80103557:	55                   	push   %ebp
80103558:	89 e5                	mov    %esp,%ebp
8010355a:	83 ec 18             	sub    $0x18,%esp
  consoleintr(kbdgetc);
8010355d:	c7 04 24 09 34 10 80 	movl   $0x80103409,(%esp)
80103564:	e8 94 d5 ff ff       	call   80100afd <consoleintr>
}
80103569:	c9                   	leave  
8010356a:	c3                   	ret    
	...

8010356c <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
8010356c:	55                   	push   %ebp
8010356d:	89 e5                	mov    %esp,%ebp
8010356f:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80103572:	9c                   	pushf  
80103573:	58                   	pop    %eax
80103574:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return eflags;
80103577:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
8010357a:	c9                   	leave  
8010357b:	c3                   	ret    

8010357c <lapicw>:

volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
8010357c:	55                   	push   %ebp
8010357d:	89 e5                	mov    %esp,%ebp
  lapic[index] = value;
8010357f:	a1 dc 3a 11 80       	mov    0x80113adc,%eax
80103584:	8b 55 08             	mov    0x8(%ebp),%edx
80103587:	c1 e2 02             	shl    $0x2,%edx
8010358a:	8d 14 10             	lea    (%eax,%edx,1),%edx
8010358d:	8b 45 0c             	mov    0xc(%ebp),%eax
80103590:	89 02                	mov    %eax,(%edx)
  lapic[ID];  // wait for write to finish, by reading
80103592:	a1 dc 3a 11 80       	mov    0x80113adc,%eax
80103597:	83 c0 20             	add    $0x20,%eax
8010359a:	8b 00                	mov    (%eax),%eax
}
8010359c:	5d                   	pop    %ebp
8010359d:	c3                   	ret    

8010359e <lapicinit>:
//PAGEBREAK!

void
lapicinit(void)
{
8010359e:	55                   	push   %ebp
8010359f:	89 e5                	mov    %esp,%ebp
801035a1:	83 ec 08             	sub    $0x8,%esp
  if(!lapic)
801035a4:	a1 dc 3a 11 80       	mov    0x80113adc,%eax
801035a9:	85 c0                	test   %eax,%eax
801035ab:	0f 84 46 01 00 00    	je     801036f7 <lapicinit+0x159>
    return;

  // Enable local APIC; set spurious interrupt vector.
  lapicw(SVR, ENABLE | (T_IRQ0 + IRQ_SPURIOUS));
801035b1:	c7 44 24 04 3f 01 00 	movl   $0x13f,0x4(%esp)
801035b8:	00 
801035b9:	c7 04 24 3c 00 00 00 	movl   $0x3c,(%esp)
801035c0:	e8 b7 ff ff ff       	call   8010357c <lapicw>

  // The timer repeatedly counts down at bus frequency
  // from lapic[TICR] and then issues an interrupt.
  // If xv6 cared more about precise timekeeping,
  // TICR would be calibrated using an external time source.
  lapicw(TDCR, X1);
801035c5:	c7 44 24 04 0b 00 00 	movl   $0xb,0x4(%esp)
801035cc:	00 
801035cd:	c7 04 24 f8 00 00 00 	movl   $0xf8,(%esp)
801035d4:	e8 a3 ff ff ff       	call   8010357c <lapicw>
  lapicw(TIMER, PERIODIC | (T_IRQ0 + IRQ_TIMER));
801035d9:	c7 44 24 04 20 00 02 	movl   $0x20020,0x4(%esp)
801035e0:	00 
801035e1:	c7 04 24 c8 00 00 00 	movl   $0xc8,(%esp)
801035e8:	e8 8f ff ff ff       	call   8010357c <lapicw>
  lapicw(TICR, 10000000);
801035ed:	c7 44 24 04 80 96 98 	movl   $0x989680,0x4(%esp)
801035f4:	00 
801035f5:	c7 04 24 e0 00 00 00 	movl   $0xe0,(%esp)
801035fc:	e8 7b ff ff ff       	call   8010357c <lapicw>

  // Disable logical interrupt lines.
  lapicw(LINT0, MASKED);
80103601:	c7 44 24 04 00 00 01 	movl   $0x10000,0x4(%esp)
80103608:	00 
80103609:	c7 04 24 d4 00 00 00 	movl   $0xd4,(%esp)
80103610:	e8 67 ff ff ff       	call   8010357c <lapicw>
  lapicw(LINT1, MASKED);
80103615:	c7 44 24 04 00 00 01 	movl   $0x10000,0x4(%esp)
8010361c:	00 
8010361d:	c7 04 24 d8 00 00 00 	movl   $0xd8,(%esp)
80103624:	e8 53 ff ff ff       	call   8010357c <lapicw>

  // Disable performance counter overflow interrupts
  // on machines that provide that interrupt entry.
  if(((lapic[VER]>>16) & 0xFF) >= 4)
80103629:	a1 dc 3a 11 80       	mov    0x80113adc,%eax
8010362e:	83 c0 30             	add    $0x30,%eax
80103631:	8b 00                	mov    (%eax),%eax
80103633:	c1 e8 10             	shr    $0x10,%eax
80103636:	25 ff 00 00 00       	and    $0xff,%eax
8010363b:	83 f8 03             	cmp    $0x3,%eax
8010363e:	76 14                	jbe    80103654 <lapicinit+0xb6>
    lapicw(PCINT, MASKED);
80103640:	c7 44 24 04 00 00 01 	movl   $0x10000,0x4(%esp)
80103647:	00 
80103648:	c7 04 24 d0 00 00 00 	movl   $0xd0,(%esp)
8010364f:	e8 28 ff ff ff       	call   8010357c <lapicw>

  // Map error interrupt to IRQ_ERROR.
  lapicw(ERROR, T_IRQ0 + IRQ_ERROR);
80103654:	c7 44 24 04 33 00 00 	movl   $0x33,0x4(%esp)
8010365b:	00 
8010365c:	c7 04 24 dc 00 00 00 	movl   $0xdc,(%esp)
80103663:	e8 14 ff ff ff       	call   8010357c <lapicw>

  // Clear error status register (requires back-to-back writes).
  lapicw(ESR, 0);
80103668:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
8010366f:	00 
80103670:	c7 04 24 a0 00 00 00 	movl   $0xa0,(%esp)
80103677:	e8 00 ff ff ff       	call   8010357c <lapicw>
  lapicw(ESR, 0);
8010367c:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80103683:	00 
80103684:	c7 04 24 a0 00 00 00 	movl   $0xa0,(%esp)
8010368b:	e8 ec fe ff ff       	call   8010357c <lapicw>

  // Ack any outstanding interrupts.
  lapicw(EOI, 0);
80103690:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80103697:	00 
80103698:	c7 04 24 2c 00 00 00 	movl   $0x2c,(%esp)
8010369f:	e8 d8 fe ff ff       	call   8010357c <lapicw>

  // Send an Init Level De-Assert to synchronise arbitration ID's.
  lapicw(ICRHI, 0);
801036a4:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801036ab:	00 
801036ac:	c7 04 24 c4 00 00 00 	movl   $0xc4,(%esp)
801036b3:	e8 c4 fe ff ff       	call   8010357c <lapicw>
  lapicw(ICRLO, BCAST | INIT | LEVEL);
801036b8:	c7 44 24 04 00 85 08 	movl   $0x88500,0x4(%esp)
801036bf:	00 
801036c0:	c7 04 24 c0 00 00 00 	movl   $0xc0,(%esp)
801036c7:	e8 b0 fe ff ff       	call   8010357c <lapicw>
  while(lapic[ICRLO] & DELIVS)
801036cc:	a1 dc 3a 11 80       	mov    0x80113adc,%eax
801036d1:	05 00 03 00 00       	add    $0x300,%eax
801036d6:	8b 00                	mov    (%eax),%eax
801036d8:	25 00 10 00 00       	and    $0x1000,%eax
801036dd:	85 c0                	test   %eax,%eax
801036df:	75 eb                	jne    801036cc <lapicinit+0x12e>
    ;

  // Enable interrupts on the APIC (but not on the processor).
  lapicw(TPR, 0);
801036e1:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801036e8:	00 
801036e9:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
801036f0:	e8 87 fe ff ff       	call   8010357c <lapicw>
801036f5:	eb 01                	jmp    801036f8 <lapicinit+0x15a>

void
lapicinit(void)
{
  if(!lapic)
    return;
801036f7:	90                   	nop
  while(lapic[ICRLO] & DELIVS)
    ;

  // Enable interrupts on the APIC (but not on the processor).
  lapicw(TPR, 0);
}
801036f8:	c9                   	leave  
801036f9:	c3                   	ret    

801036fa <cpunum>:

int
cpunum(void)
{
801036fa:	55                   	push   %ebp
801036fb:	89 e5                	mov    %esp,%ebp
801036fd:	83 ec 18             	sub    $0x18,%esp
  // Cannot call cpu when interrupts are enabled:
  // result not guaranteed to last long enough to be used!
  // Would prefer to panic but even printing is chancy here:
  // almost everything, including cprintf and panic, calls cpu,
  // often indirectly through acquire and release.
  if(readeflags()&FL_IF){
80103700:	e8 67 fe ff ff       	call   8010356c <readeflags>
80103705:	25 00 02 00 00       	and    $0x200,%eax
8010370a:	85 c0                	test   %eax,%eax
8010370c:	74 29                	je     80103737 <cpunum+0x3d>
    static int n;
    if(n++ == 0)
8010370e:	a1 84 c6 10 80       	mov    0x8010c684,%eax
80103713:	85 c0                	test   %eax,%eax
80103715:	0f 94 c2             	sete   %dl
80103718:	83 c0 01             	add    $0x1,%eax
8010371b:	a3 84 c6 10 80       	mov    %eax,0x8010c684
80103720:	84 d2                	test   %dl,%dl
80103722:	74 13                	je     80103737 <cpunum+0x3d>
      cprintf("cpu called from %x with interrupts enabled\n",
80103724:	8b 45 04             	mov    0x4(%ebp),%eax
80103727:	89 44 24 04          	mov    %eax,0x4(%esp)
8010372b:	c7 04 24 54 91 10 80 	movl   $0x80109154,(%esp)
80103732:	e8 92 cf ff ff       	call   801006c9 <cprintf>
        __builtin_return_address(0));
  }

  if(lapic)
80103737:	a1 dc 3a 11 80       	mov    0x80113adc,%eax
8010373c:	85 c0                	test   %eax,%eax
8010373e:	74 0f                	je     8010374f <cpunum+0x55>
    return lapic[ID]>>24;
80103740:	a1 dc 3a 11 80       	mov    0x80113adc,%eax
80103745:	83 c0 20             	add    $0x20,%eax
80103748:	8b 00                	mov    (%eax),%eax
8010374a:	c1 e8 18             	shr    $0x18,%eax
8010374d:	eb 05                	jmp    80103754 <cpunum+0x5a>
  return 0;
8010374f:	b8 00 00 00 00       	mov    $0x0,%eax
}
80103754:	c9                   	leave  
80103755:	c3                   	ret    

80103756 <lapiceoi>:

// Acknowledge interrupt.
void
lapiceoi(void)
{
80103756:	55                   	push   %ebp
80103757:	89 e5                	mov    %esp,%ebp
80103759:	83 ec 08             	sub    $0x8,%esp
  if(lapic)
8010375c:	a1 dc 3a 11 80       	mov    0x80113adc,%eax
80103761:	85 c0                	test   %eax,%eax
80103763:	74 14                	je     80103779 <lapiceoi+0x23>
    lapicw(EOI, 0);
80103765:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
8010376c:	00 
8010376d:	c7 04 24 2c 00 00 00 	movl   $0x2c,(%esp)
80103774:	e8 03 fe ff ff       	call   8010357c <lapicw>
}
80103779:	c9                   	leave  
8010377a:	c3                   	ret    

8010377b <microdelay>:

// Spin for a given number of microseconds.
// On real hardware would want to tune this dynamically.
void
microdelay(int us)
{
8010377b:	55                   	push   %ebp
8010377c:	89 e5                	mov    %esp,%ebp
}
8010377e:	5d                   	pop    %ebp
8010377f:	c3                   	ret    

80103780 <lapicstartap>:

// Start additional processor running entry code at addr.
// See Appendix B of MultiProcessor Specification.
void
lapicstartap(uchar apicid, uint addr)
{
80103780:	55                   	push   %ebp
80103781:	89 e5                	mov    %esp,%ebp
80103783:	83 ec 38             	sub    $0x38,%esp
80103786:	8b 45 08             	mov    0x8(%ebp),%eax
80103789:	88 45 e4             	mov    %al,-0x1c(%ebp)
  ushort *wrv;

  // "The BSP must initialize CMOS shutdown code to 0AH
  // and the warm reset vector (DWORD based at 40:67) to point at
  // the AP startup code prior to the [universal startup algorithm]."
  cmoswrite(CMOS_SHUTDOWN_STAT, CMOS_JMP_DWORD_NO_EOI);
8010378c:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
80103793:	00 
80103794:	c7 04 24 0f 00 00 00 	movl   $0xf,(%esp)
8010379b:	e8 9a cb ff ff       	call   8010033a <cmoswrite>
  wrv = (ushort*)P2V((0x40<<4 | 0x67));  // Warm reset vector
801037a0:	c7 45 f4 67 04 00 80 	movl   $0x80000467,-0xc(%ebp)
  wrv[0] = 0;
801037a7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801037aa:	66 c7 00 00 00       	movw   $0x0,(%eax)
  wrv[1] = addr >> 4;
801037af:	8b 45 f4             	mov    -0xc(%ebp),%eax
801037b2:	8d 50 02             	lea    0x2(%eax),%edx
801037b5:	8b 45 0c             	mov    0xc(%ebp),%eax
801037b8:	c1 e8 04             	shr    $0x4,%eax
801037bb:	66 89 02             	mov    %ax,(%edx)

  // "Universal startup algorithm."
  // Send INIT (level-triggered) interrupt to reset other CPU.
  lapicw(ICRHI, apicid<<24);
801037be:	0f b6 45 e4          	movzbl -0x1c(%ebp),%eax
801037c2:	c1 e0 18             	shl    $0x18,%eax
801037c5:	89 44 24 04          	mov    %eax,0x4(%esp)
801037c9:	c7 04 24 c4 00 00 00 	movl   $0xc4,(%esp)
801037d0:	e8 a7 fd ff ff       	call   8010357c <lapicw>
  lapicw(ICRLO, INIT | LEVEL | ASSERT);
801037d5:	c7 44 24 04 00 c5 00 	movl   $0xc500,0x4(%esp)
801037dc:	00 
801037dd:	c7 04 24 c0 00 00 00 	movl   $0xc0,(%esp)
801037e4:	e8 93 fd ff ff       	call   8010357c <lapicw>
  microdelay(200);
801037e9:	c7 04 24 c8 00 00 00 	movl   $0xc8,(%esp)
801037f0:	e8 86 ff ff ff       	call   8010377b <microdelay>
  lapicw(ICRLO, INIT | LEVEL);
801037f5:	c7 44 24 04 00 85 00 	movl   $0x8500,0x4(%esp)
801037fc:	00 
801037fd:	c7 04 24 c0 00 00 00 	movl   $0xc0,(%esp)
80103804:	e8 73 fd ff ff       	call   8010357c <lapicw>
  microdelay(100);    // should be 10ms, but too slow in Bochs!
80103809:	c7 04 24 64 00 00 00 	movl   $0x64,(%esp)
80103810:	e8 66 ff ff ff       	call   8010377b <microdelay>
  // Send startup IPI (twice!) to enter code.
  // Regular hardware is supposed to only accept a STARTUP
  // when it is in the halted state due to an INIT.  So the second
  // should be ignored, but it is part of the official Intel algorithm.
  // Bochs complains about the second one.  Too bad for Bochs.
  for(i = 0; i < 2; i++){
80103815:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
8010381c:	eb 40                	jmp    8010385e <lapicstartap+0xde>
    lapicw(ICRHI, apicid<<24);
8010381e:	0f b6 45 e4          	movzbl -0x1c(%ebp),%eax
80103822:	c1 e0 18             	shl    $0x18,%eax
80103825:	89 44 24 04          	mov    %eax,0x4(%esp)
80103829:	c7 04 24 c4 00 00 00 	movl   $0xc4,(%esp)
80103830:	e8 47 fd ff ff       	call   8010357c <lapicw>
    lapicw(ICRLO, STARTUP | (addr>>12));
80103835:	8b 45 0c             	mov    0xc(%ebp),%eax
80103838:	c1 e8 0c             	shr    $0xc,%eax
8010383b:	80 cc 06             	or     $0x6,%ah
8010383e:	89 44 24 04          	mov    %eax,0x4(%esp)
80103842:	c7 04 24 c0 00 00 00 	movl   $0xc0,(%esp)
80103849:	e8 2e fd ff ff       	call   8010357c <lapicw>
    microdelay(200);
8010384e:	c7 04 24 c8 00 00 00 	movl   $0xc8,(%esp)
80103855:	e8 21 ff ff ff       	call   8010377b <microdelay>
  // Send startup IPI (twice!) to enter code.
  // Regular hardware is supposed to only accept a STARTUP
  // when it is in the halted state due to an INIT.  So the second
  // should be ignored, but it is part of the official Intel algorithm.
  // Bochs complains about the second one.  Too bad for Bochs.
  for(i = 0; i < 2; i++){
8010385a:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
8010385e:	83 7d f0 01          	cmpl   $0x1,-0x10(%ebp)
80103862:	7e ba                	jle    8010381e <lapicstartap+0x9e>
    lapicw(ICRHI, apicid<<24);
    lapicw(ICRLO, STARTUP | (addr>>12));
    microdelay(200);
  }
}
80103864:	c9                   	leave  
80103865:	c3                   	ret    
	...

80103868 <initlog>:
static void recover_from_log(void);
static void commit();

void
initlog(int dev)
{
80103868:	55                   	push   %ebp
80103869:	89 e5                	mov    %esp,%ebp
8010386b:	83 ec 38             	sub    $0x38,%esp
  if (sizeof(struct logheader) >= BSIZE)
8010386e:	90                   	nop
    panic("initlog: too big logheader");

  struct superblock sb;
  initlock(&log.lock, "log");
8010386f:	c7 44 24 04 80 91 10 	movl   $0x80109180,0x4(%esp)
80103876:	80 
80103877:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
8010387e:	e8 a3 1e 00 00       	call   80105726 <initlock>
  readsb(dev, &sb);
80103883:	8d 45 dc             	lea    -0x24(%ebp),%eax
80103886:	89 44 24 04          	mov    %eax,0x4(%esp)
8010388a:	8b 45 08             	mov    0x8(%ebp),%eax
8010388d:	89 04 24             	mov    %eax,(%esp)
80103890:	e8 f3 e1 ff ff       	call   80101a88 <readsb>
  log.start = sb.logstart;
80103895:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103898:	a3 14 3b 11 80       	mov    %eax,0x80113b14
  log.size = sb.nlog;
8010389d:	8b 45 e8             	mov    -0x18(%ebp),%eax
801038a0:	a3 18 3b 11 80       	mov    %eax,0x80113b18
  log.dev = dev;
801038a5:	8b 45 08             	mov    0x8(%ebp),%eax
801038a8:	a3 24 3b 11 80       	mov    %eax,0x80113b24
  recover_from_log();
801038ad:	e8 97 01 00 00       	call   80103a49 <recover_from_log>
}
801038b2:	c9                   	leave  
801038b3:	c3                   	ret    

801038b4 <install_trans>:

// Copy committed blocks from log to their home location
static void 
install_trans(void)
{
801038b4:	55                   	push   %ebp
801038b5:	89 e5                	mov    %esp,%ebp
801038b7:	83 ec 28             	sub    $0x28,%esp
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
801038ba:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
801038c1:	e9 89 00 00 00       	jmp    8010394f <install_trans+0x9b>
    struct buf *lbuf = bread(log.dev, log.start+tail+1); // read log block
801038c6:	a1 14 3b 11 80       	mov    0x80113b14,%eax
801038cb:	03 45 ec             	add    -0x14(%ebp),%eax
801038ce:	83 c0 01             	add    $0x1,%eax
801038d1:	89 c2                	mov    %eax,%edx
801038d3:	a1 24 3b 11 80       	mov    0x80113b24,%eax
801038d8:	89 54 24 04          	mov    %edx,0x4(%esp)
801038dc:	89 04 24             	mov    %eax,(%esp)
801038df:	e8 c3 c8 ff ff       	call   801001a7 <bread>
801038e4:	89 45 f0             	mov    %eax,-0x10(%ebp)
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
801038e7:	8b 45 ec             	mov    -0x14(%ebp),%eax
801038ea:	83 c0 10             	add    $0x10,%eax
801038ed:	8b 04 85 ec 3a 11 80 	mov    -0x7feec514(,%eax,4),%eax
801038f4:	89 c2                	mov    %eax,%edx
801038f6:	a1 24 3b 11 80       	mov    0x80113b24,%eax
801038fb:	89 54 24 04          	mov    %edx,0x4(%esp)
801038ff:	89 04 24             	mov    %eax,(%esp)
80103902:	e8 a0 c8 ff ff       	call   801001a7 <bread>
80103907:	89 45 f4             	mov    %eax,-0xc(%ebp)
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
8010390a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010390d:	8d 50 18             	lea    0x18(%eax),%edx
80103910:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103913:	83 c0 18             	add    $0x18,%eax
80103916:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
8010391d:	00 
8010391e:	89 54 24 04          	mov    %edx,0x4(%esp)
80103922:	89 04 24             	mov    %eax,(%esp)
80103925:	e8 53 21 00 00       	call   80105a7d <memmove>
    bwrite(dbuf);  // write dst to disk
8010392a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010392d:	89 04 24             	mov    %eax,(%esp)
80103930:	e8 a9 c8 ff ff       	call   801001de <bwrite>
    brelse(lbuf); 
80103935:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103938:	89 04 24             	mov    %eax,(%esp)
8010393b:	e8 d8 c8 ff ff       	call   80100218 <brelse>
    brelse(dbuf);
80103940:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103943:	89 04 24             	mov    %eax,(%esp)
80103946:	e8 cd c8 ff ff       	call   80100218 <brelse>
static void 
install_trans(void)
{
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
8010394b:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
8010394f:	a1 28 3b 11 80       	mov    0x80113b28,%eax
80103954:	3b 45 ec             	cmp    -0x14(%ebp),%eax
80103957:	0f 8f 69 ff ff ff    	jg     801038c6 <install_trans+0x12>
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
    bwrite(dbuf);  // write dst to disk
    brelse(lbuf); 
    brelse(dbuf);
  }
}
8010395d:	c9                   	leave  
8010395e:	c3                   	ret    

8010395f <read_head>:

// Read the log header from disk into the in-memory log header
static void
read_head(void)
{
8010395f:	55                   	push   %ebp
80103960:	89 e5                	mov    %esp,%ebp
80103962:	83 ec 28             	sub    $0x28,%esp
  struct buf *buf = bread(log.dev, log.start);
80103965:	a1 14 3b 11 80       	mov    0x80113b14,%eax
8010396a:	89 c2                	mov    %eax,%edx
8010396c:	a1 24 3b 11 80       	mov    0x80113b24,%eax
80103971:	89 54 24 04          	mov    %edx,0x4(%esp)
80103975:	89 04 24             	mov    %eax,(%esp)
80103978:	e8 2a c8 ff ff       	call   801001a7 <bread>
8010397d:	89 45 ec             	mov    %eax,-0x14(%ebp)
  struct logheader *lh = (struct logheader *) (buf->data);
80103980:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103983:	83 c0 18             	add    $0x18,%eax
80103986:	89 45 f0             	mov    %eax,-0x10(%ebp)
  int i;
  log.lh.n = lh->n;
80103989:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010398c:	8b 00                	mov    (%eax),%eax
8010398e:	a3 28 3b 11 80       	mov    %eax,0x80113b28
  for (i = 0; i < log.lh.n; i++) {
80103993:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010399a:	eb 1b                	jmp    801039b7 <read_head+0x58>
    log.lh.block[i] = lh->block[i];
8010399c:	8b 4d f4             	mov    -0xc(%ebp),%ecx
8010399f:	8b 55 f4             	mov    -0xc(%ebp),%edx
801039a2:	8b 45 f0             	mov    -0x10(%ebp),%eax
801039a5:	8b 44 90 04          	mov    0x4(%eax,%edx,4),%eax
801039a9:	8d 51 10             	lea    0x10(%ecx),%edx
801039ac:	89 04 95 ec 3a 11 80 	mov    %eax,-0x7feec514(,%edx,4)
{
  struct buf *buf = bread(log.dev, log.start);
  struct logheader *lh = (struct logheader *) (buf->data);
  int i;
  log.lh.n = lh->n;
  for (i = 0; i < log.lh.n; i++) {
801039b3:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801039b7:	a1 28 3b 11 80       	mov    0x80113b28,%eax
801039bc:	3b 45 f4             	cmp    -0xc(%ebp),%eax
801039bf:	7f db                	jg     8010399c <read_head+0x3d>
    log.lh.block[i] = lh->block[i];
  }
  brelse(buf);
801039c1:	8b 45 ec             	mov    -0x14(%ebp),%eax
801039c4:	89 04 24             	mov    %eax,(%esp)
801039c7:	e8 4c c8 ff ff       	call   80100218 <brelse>
}
801039cc:	c9                   	leave  
801039cd:	c3                   	ret    

801039ce <write_head>:
// Write in-memory log header to disk.
// This is the true point at which the
// current transaction commits.
static void
write_head(void)
{
801039ce:	55                   	push   %ebp
801039cf:	89 e5                	mov    %esp,%ebp
801039d1:	83 ec 28             	sub    $0x28,%esp
  struct buf *buf = bread(log.dev, log.start);
801039d4:	a1 14 3b 11 80       	mov    0x80113b14,%eax
801039d9:	89 c2                	mov    %eax,%edx
801039db:	a1 24 3b 11 80       	mov    0x80113b24,%eax
801039e0:	89 54 24 04          	mov    %edx,0x4(%esp)
801039e4:	89 04 24             	mov    %eax,(%esp)
801039e7:	e8 bb c7 ff ff       	call   801001a7 <bread>
801039ec:	89 45 ec             	mov    %eax,-0x14(%ebp)
  struct logheader *hb = (struct logheader *) (buf->data);
801039ef:	8b 45 ec             	mov    -0x14(%ebp),%eax
801039f2:	83 c0 18             	add    $0x18,%eax
801039f5:	89 45 f0             	mov    %eax,-0x10(%ebp)
  int i;
  hb->n = log.lh.n;
801039f8:	8b 15 28 3b 11 80    	mov    0x80113b28,%edx
801039fe:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103a01:	89 10                	mov    %edx,(%eax)
  for (i = 0; i < log.lh.n; i++) {
80103a03:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80103a0a:	eb 1b                	jmp    80103a27 <write_head+0x59>
    hb->block[i] = log.lh.block[i];
80103a0c:	8b 55 f4             	mov    -0xc(%ebp),%edx
80103a0f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103a12:	83 c0 10             	add    $0x10,%eax
80103a15:	8b 0c 85 ec 3a 11 80 	mov    -0x7feec514(,%eax,4),%ecx
80103a1c:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103a1f:	89 4c 90 04          	mov    %ecx,0x4(%eax,%edx,4)
{
  struct buf *buf = bread(log.dev, log.start);
  struct logheader *hb = (struct logheader *) (buf->data);
  int i;
  hb->n = log.lh.n;
  for (i = 0; i < log.lh.n; i++) {
80103a23:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80103a27:	a1 28 3b 11 80       	mov    0x80113b28,%eax
80103a2c:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103a2f:	7f db                	jg     80103a0c <write_head+0x3e>
    hb->block[i] = log.lh.block[i];
  }
  bwrite(buf);
80103a31:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103a34:	89 04 24             	mov    %eax,(%esp)
80103a37:	e8 a2 c7 ff ff       	call   801001de <bwrite>
  brelse(buf);
80103a3c:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103a3f:	89 04 24             	mov    %eax,(%esp)
80103a42:	e8 d1 c7 ff ff       	call   80100218 <brelse>
}
80103a47:	c9                   	leave  
80103a48:	c3                   	ret    

80103a49 <recover_from_log>:

static void
recover_from_log(void)
{
80103a49:	55                   	push   %ebp
80103a4a:	89 e5                	mov    %esp,%ebp
80103a4c:	83 ec 08             	sub    $0x8,%esp
  read_head();      
80103a4f:	e8 0b ff ff ff       	call   8010395f <read_head>
  install_trans(); // if committed, copy from log to disk
80103a54:	e8 5b fe ff ff       	call   801038b4 <install_trans>
  log.lh.n = 0;
80103a59:	c7 05 28 3b 11 80 00 	movl   $0x0,0x80113b28
80103a60:	00 00 00 
  write_head(); // clear the log
80103a63:	e8 66 ff ff ff       	call   801039ce <write_head>
}
80103a68:	c9                   	leave  
80103a69:	c3                   	ret    

80103a6a <begin_op>:

// called at the start of each FS system call.
void
begin_op(void)
{
80103a6a:	55                   	push   %ebp
80103a6b:	89 e5                	mov    %esp,%ebp
80103a6d:	83 ec 18             	sub    $0x18,%esp
  acquire(&log.lock);
80103a70:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103a77:	e8 cb 1c 00 00       	call   80105747 <acquire>
  while(1){
    if(log.committing){
80103a7c:	a1 20 3b 11 80       	mov    0x80113b20,%eax
80103a81:	85 c0                	test   %eax,%eax
80103a83:	74 16                	je     80103a9b <begin_op+0x31>
      sleep(&log, &log.lock);
80103a85:	c7 44 24 04 e0 3a 11 	movl   $0x80113ae0,0x4(%esp)
80103a8c:	80 
80103a8d:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103a94:	e8 85 19 00 00       	call   8010541e <sleep>
    } else {
      log.outstanding += 1;
      release(&log.lock);
      break;
    }
  }
80103a99:	eb e1                	jmp    80103a7c <begin_op+0x12>
{
  acquire(&log.lock);
  while(1){
    if(log.committing){
      sleep(&log, &log.lock);
    } else if(log.lh.n + (log.outstanding+1)*MAXOPBLOCKS > LOGSIZE){
80103a9b:	8b 0d 28 3b 11 80    	mov    0x80113b28,%ecx
80103aa1:	a1 1c 3b 11 80       	mov    0x80113b1c,%eax
80103aa6:	8d 50 01             	lea    0x1(%eax),%edx
80103aa9:	89 d0                	mov    %edx,%eax
80103aab:	c1 e0 02             	shl    $0x2,%eax
80103aae:	01 d0                	add    %edx,%eax
80103ab0:	01 c0                	add    %eax,%eax
80103ab2:	8d 04 01             	lea    (%ecx,%eax,1),%eax
80103ab5:	83 f8 1e             	cmp    $0x1e,%eax
80103ab8:	7e 16                	jle    80103ad0 <begin_op+0x66>
      // this op might exhaust log space; wait for commit.
      sleep(&log, &log.lock);
80103aba:	c7 44 24 04 e0 3a 11 	movl   $0x80113ae0,0x4(%esp)
80103ac1:	80 
80103ac2:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103ac9:	e8 50 19 00 00       	call   8010541e <sleep>
    } else {
      log.outstanding += 1;
      release(&log.lock);
      break;
    }
  }
80103ace:	eb ac                	jmp    80103a7c <begin_op+0x12>
      sleep(&log, &log.lock);
    } else if(log.lh.n + (log.outstanding+1)*MAXOPBLOCKS > LOGSIZE){
      // this op might exhaust log space; wait for commit.
      sleep(&log, &log.lock);
    } else {
      log.outstanding += 1;
80103ad0:	a1 1c 3b 11 80       	mov    0x80113b1c,%eax
80103ad5:	83 c0 01             	add    $0x1,%eax
80103ad8:	a3 1c 3b 11 80       	mov    %eax,0x80113b1c
      release(&log.lock);
80103add:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103ae4:	e8 c7 1c 00 00       	call   801057b0 <release>
      break;
80103ae9:	90                   	nop
    }
  }
}
80103aea:	c9                   	leave  
80103aeb:	c3                   	ret    

80103aec <end_op>:

// called at the end of each FS system call.
// commits if this was the last outstanding operation.
void
end_op(void)
{
80103aec:	55                   	push   %ebp
80103aed:	89 e5                	mov    %esp,%ebp
80103aef:	83 ec 28             	sub    $0x28,%esp
  int do_commit = 0;
80103af2:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

  acquire(&log.lock);
80103af9:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103b00:	e8 42 1c 00 00       	call   80105747 <acquire>
  log.outstanding -= 1;
80103b05:	a1 1c 3b 11 80       	mov    0x80113b1c,%eax
80103b0a:	83 e8 01             	sub    $0x1,%eax
80103b0d:	a3 1c 3b 11 80       	mov    %eax,0x80113b1c
  if(log.committing)
80103b12:	a1 20 3b 11 80       	mov    0x80113b20,%eax
80103b17:	85 c0                	test   %eax,%eax
80103b19:	74 0c                	je     80103b27 <end_op+0x3b>
    panic("log.committing");
80103b1b:	c7 04 24 84 91 10 80 	movl   $0x80109184,(%esp)
80103b22:	e8 42 cd ff ff       	call   80100869 <panic>
  if(log.outstanding == 0){
80103b27:	a1 1c 3b 11 80       	mov    0x80113b1c,%eax
80103b2c:	85 c0                	test   %eax,%eax
80103b2e:	75 13                	jne    80103b43 <end_op+0x57>
    do_commit = 1;
80103b30:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
    log.committing = 1;
80103b37:	c7 05 20 3b 11 80 01 	movl   $0x1,0x80113b20
80103b3e:	00 00 00 
80103b41:	eb 0c                	jmp    80103b4f <end_op+0x63>
  } else {
    // begin_op() may be waiting for log space.
    wakeup(&log);
80103b43:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103b4a:	e8 ac 19 00 00       	call   801054fb <wakeup>
  }
  release(&log.lock);
80103b4f:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103b56:	e8 55 1c 00 00       	call   801057b0 <release>

  if(do_commit){
80103b5b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80103b5f:	74 33                	je     80103b94 <end_op+0xa8>
    // call commit w/o holding locks, since not allowed
    // to sleep with locks.
    commit();
80103b61:	e8 db 00 00 00       	call   80103c41 <commit>
    acquire(&log.lock);
80103b66:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103b6d:	e8 d5 1b 00 00       	call   80105747 <acquire>
    log.committing = 0;
80103b72:	c7 05 20 3b 11 80 00 	movl   $0x0,0x80113b20
80103b79:	00 00 00 
    wakeup(&log);
80103b7c:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103b83:	e8 73 19 00 00       	call   801054fb <wakeup>
    release(&log.lock);
80103b88:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103b8f:	e8 1c 1c 00 00       	call   801057b0 <release>
  }
}
80103b94:	c9                   	leave  
80103b95:	c3                   	ret    

80103b96 <write_log>:

// Copy modified blocks from cache to log.
static void 
write_log(void)
{
80103b96:	55                   	push   %ebp
80103b97:	89 e5                	mov    %esp,%ebp
80103b99:	83 ec 28             	sub    $0x28,%esp
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
80103b9c:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
80103ba3:	e9 89 00 00 00       	jmp    80103c31 <write_log+0x9b>
    struct buf *to = bread(log.dev, log.start+tail+1); // log block
80103ba8:	a1 14 3b 11 80       	mov    0x80113b14,%eax
80103bad:	03 45 ec             	add    -0x14(%ebp),%eax
80103bb0:	83 c0 01             	add    $0x1,%eax
80103bb3:	89 c2                	mov    %eax,%edx
80103bb5:	a1 24 3b 11 80       	mov    0x80113b24,%eax
80103bba:	89 54 24 04          	mov    %edx,0x4(%esp)
80103bbe:	89 04 24             	mov    %eax,(%esp)
80103bc1:	e8 e1 c5 ff ff       	call   801001a7 <bread>
80103bc6:	89 45 f0             	mov    %eax,-0x10(%ebp)
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
80103bc9:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103bcc:	83 c0 10             	add    $0x10,%eax
80103bcf:	8b 04 85 ec 3a 11 80 	mov    -0x7feec514(,%eax,4),%eax
80103bd6:	89 c2                	mov    %eax,%edx
80103bd8:	a1 24 3b 11 80       	mov    0x80113b24,%eax
80103bdd:	89 54 24 04          	mov    %edx,0x4(%esp)
80103be1:	89 04 24             	mov    %eax,(%esp)
80103be4:	e8 be c5 ff ff       	call   801001a7 <bread>
80103be9:	89 45 f4             	mov    %eax,-0xc(%ebp)
    memmove(to->data, from->data, BSIZE);
80103bec:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103bef:	8d 50 18             	lea    0x18(%eax),%edx
80103bf2:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103bf5:	83 c0 18             	add    $0x18,%eax
80103bf8:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
80103bff:	00 
80103c00:	89 54 24 04          	mov    %edx,0x4(%esp)
80103c04:	89 04 24             	mov    %eax,(%esp)
80103c07:	e8 71 1e 00 00       	call   80105a7d <memmove>
    bwrite(to);  // write the log
80103c0c:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103c0f:	89 04 24             	mov    %eax,(%esp)
80103c12:	e8 c7 c5 ff ff       	call   801001de <bwrite>
    brelse(from); 
80103c17:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103c1a:	89 04 24             	mov    %eax,(%esp)
80103c1d:	e8 f6 c5 ff ff       	call   80100218 <brelse>
    brelse(to);
80103c22:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103c25:	89 04 24             	mov    %eax,(%esp)
80103c28:	e8 eb c5 ff ff       	call   80100218 <brelse>
static void 
write_log(void)
{
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
80103c2d:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
80103c31:	a1 28 3b 11 80       	mov    0x80113b28,%eax
80103c36:	3b 45 ec             	cmp    -0x14(%ebp),%eax
80103c39:	0f 8f 69 ff ff ff    	jg     80103ba8 <write_log+0x12>
    memmove(to->data, from->data, BSIZE);
    bwrite(to);  // write the log
    brelse(from); 
    brelse(to);
  }
}
80103c3f:	c9                   	leave  
80103c40:	c3                   	ret    

80103c41 <commit>:

static void
commit()
{
80103c41:	55                   	push   %ebp
80103c42:	89 e5                	mov    %esp,%ebp
80103c44:	83 ec 08             	sub    $0x8,%esp
  if (log.lh.n > 0) {
80103c47:	a1 28 3b 11 80       	mov    0x80113b28,%eax
80103c4c:	85 c0                	test   %eax,%eax
80103c4e:	7e 1e                	jle    80103c6e <commit+0x2d>
    write_log();     // Write modified blocks from cache to log
80103c50:	e8 41 ff ff ff       	call   80103b96 <write_log>
    write_head();    // Write header to disk -- the real commit
80103c55:	e8 74 fd ff ff       	call   801039ce <write_head>
    install_trans(); // Now install writes to home locations
80103c5a:	e8 55 fc ff ff       	call   801038b4 <install_trans>
    log.lh.n = 0; 
80103c5f:	c7 05 28 3b 11 80 00 	movl   $0x0,0x80113b28
80103c66:	00 00 00 
    write_head();    // Erase the transaction from the log
80103c69:	e8 60 fd ff ff       	call   801039ce <write_head>
  }
}
80103c6e:	c9                   	leave  
80103c6f:	c3                   	ret    

80103c70 <log_write>:
//   modify bp->data[]
//   log_write(bp)
//   brelse(bp)
void
log_write(struct buf *b)
{
80103c70:	55                   	push   %ebp
80103c71:	89 e5                	mov    %esp,%ebp
80103c73:	83 ec 28             	sub    $0x28,%esp
  int i;

  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
80103c76:	a1 28 3b 11 80       	mov    0x80113b28,%eax
80103c7b:	83 f8 1d             	cmp    $0x1d,%eax
80103c7e:	7f 12                	jg     80103c92 <log_write+0x22>
80103c80:	a1 28 3b 11 80       	mov    0x80113b28,%eax
80103c85:	8b 15 18 3b 11 80    	mov    0x80113b18,%edx
80103c8b:	83 ea 01             	sub    $0x1,%edx
80103c8e:	39 d0                	cmp    %edx,%eax
80103c90:	7c 0c                	jl     80103c9e <log_write+0x2e>
    panic("too big a transaction");
80103c92:	c7 04 24 93 91 10 80 	movl   $0x80109193,(%esp)
80103c99:	e8 cb cb ff ff       	call   80100869 <panic>
  if (log.outstanding < 1)
80103c9e:	a1 1c 3b 11 80       	mov    0x80113b1c,%eax
80103ca3:	85 c0                	test   %eax,%eax
80103ca5:	7f 0c                	jg     80103cb3 <log_write+0x43>
    panic("log_write outside of trans");
80103ca7:	c7 04 24 a9 91 10 80 	movl   $0x801091a9,(%esp)
80103cae:	e8 b6 cb ff ff       	call   80100869 <panic>

  acquire(&log.lock);
80103cb3:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103cba:	e8 88 1a 00 00       	call   80105747 <acquire>
  for (i = 0; i < log.lh.n; i++) {
80103cbf:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80103cc6:	eb 1d                	jmp    80103ce5 <log_write+0x75>
    if (log.lh.block[i] == b->blockno)   // log absorbtion
80103cc8:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103ccb:	83 c0 10             	add    $0x10,%eax
80103cce:	8b 04 85 ec 3a 11 80 	mov    -0x7feec514(,%eax,4),%eax
80103cd5:	89 c2                	mov    %eax,%edx
80103cd7:	8b 45 08             	mov    0x8(%ebp),%eax
80103cda:	8b 40 08             	mov    0x8(%eax),%eax
80103cdd:	39 c2                	cmp    %eax,%edx
80103cdf:	74 10                	je     80103cf1 <log_write+0x81>
    panic("too big a transaction");
  if (log.outstanding < 1)
    panic("log_write outside of trans");

  acquire(&log.lock);
  for (i = 0; i < log.lh.n; i++) {
80103ce1:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80103ce5:	a1 28 3b 11 80       	mov    0x80113b28,%eax
80103cea:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103ced:	7f d9                	jg     80103cc8 <log_write+0x58>
80103cef:	eb 01                	jmp    80103cf2 <log_write+0x82>
    if (log.lh.block[i] == b->blockno)   // log absorbtion
      break;
80103cf1:	90                   	nop
  }
  log.lh.block[i] = b->blockno;
80103cf2:	8b 55 f4             	mov    -0xc(%ebp),%edx
80103cf5:	8b 45 08             	mov    0x8(%ebp),%eax
80103cf8:	8b 40 08             	mov    0x8(%eax),%eax
80103cfb:	83 c2 10             	add    $0x10,%edx
80103cfe:	89 04 95 ec 3a 11 80 	mov    %eax,-0x7feec514(,%edx,4)
  if (i == log.lh.n)
80103d05:	a1 28 3b 11 80       	mov    0x80113b28,%eax
80103d0a:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103d0d:	75 0d                	jne    80103d1c <log_write+0xac>
    log.lh.n++;
80103d0f:	a1 28 3b 11 80       	mov    0x80113b28,%eax
80103d14:	83 c0 01             	add    $0x1,%eax
80103d17:	a3 28 3b 11 80       	mov    %eax,0x80113b28
  b->flags |= B_DIRTY; // prevent eviction
80103d1c:	8b 45 08             	mov    0x8(%ebp),%eax
80103d1f:	8b 00                	mov    (%eax),%eax
80103d21:	89 c2                	mov    %eax,%edx
80103d23:	83 ca 04             	or     $0x4,%edx
80103d26:	8b 45 08             	mov    0x8(%ebp),%eax
80103d29:	89 10                	mov    %edx,(%eax)
  release(&log.lock);
80103d2b:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103d32:	e8 79 1a 00 00       	call   801057b0 <release>
}
80103d37:	c9                   	leave  
80103d38:	c3                   	ret    
80103d39:	00 00                	add    %al,(%eax)
	...

80103d3c <xchg>:
  asm volatile("sti");
}

static inline uint
xchg(volatile uint *addr, uint newval)
{
80103d3c:	55                   	push   %ebp
80103d3d:	89 e5                	mov    %esp,%ebp
80103d3f:	83 ec 10             	sub    $0x10,%esp
  uint result;

  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
80103d42:	8b 55 08             	mov    0x8(%ebp),%edx
80103d45:	8b 45 0c             	mov    0xc(%ebp),%eax
80103d48:	8b 4d 08             	mov    0x8(%ebp),%ecx
80103d4b:	f0 87 02             	lock xchg %eax,(%edx)
80103d4e:	89 45 fc             	mov    %eax,-0x4(%ebp)
               "+m" (*addr), "=a" (result) :
               "1" (newval) :
               "cc");
  return result;
80103d51:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80103d54:	c9                   	leave  
80103d55:	c3                   	ret    

80103d56 <main>:
// Bootstrap processor starts running C code here.
// Allocate a real stack and switch to it, first
// doing some setup required for memory allocator to work.
int
main(void)
{
80103d56:	55                   	push   %ebp
80103d57:	89 e5                	mov    %esp,%ebp
80103d59:	83 e4 f0             	and    $0xfffffff0,%esp
80103d5c:	83 ec 20             	sub    $0x20,%esp
  int i;
  for(i = 0; i < 256; i++){syscallstats_data.counts[i] = 0;}
80103d5f:	c7 44 24 1c 00 00 00 	movl   $0x0,0x1c(%esp)
80103d66:	00 
80103d67:	eb 14                	jmp    80103d7d <main+0x27>
80103d69:	8b 44 24 1c          	mov    0x1c(%esp),%eax
80103d6d:	c7 04 85 20 63 11 80 	movl   $0x0,-0x7fee9ce0(,%eax,4)
80103d74:	00 00 00 00 
80103d78:	83 44 24 1c 01       	addl   $0x1,0x1c(%esp)
80103d7d:	81 7c 24 1c ff 00 00 	cmpl   $0xff,0x1c(%esp)
80103d84:	00 
80103d85:	7e e2                	jle    80103d69 <main+0x13>
  debuginit();     // enable debug output first
80103d87:	e8 f7 d1 ff ff       	call   80100f83 <debuginit>
  kinit1(end, P2V(4*1024*1024)); // phys page allocator
80103d8c:	c7 44 24 04 00 00 40 	movl   $0x80400000,0x4(%esp)
80103d93:	80 
80103d94:	c7 04 24 78 6b 11 80 	movl   $0x80116b78,(%esp)
80103d9b:	e8 d0 f4 ff ff       	call   80103270 <kinit1>
  kvmalloc();      // kernel page table
80103da0:	e8 d1 48 00 00       	call   80108676 <kvmalloc>
  mpinit();        // collect info about this machine
80103da5:	e8 0f 04 00 00       	call   801041b9 <mpinit>
  lapicinit();
80103daa:	e8 ef f7 ff ff       	call   8010359e <lapicinit>
  seginit();       // set up segments
80103daf:	e8 8b 42 00 00       	call   8010803f <seginit>
  cprintf("\ncpu%d: starting xv6\n\n", cpu->id);
80103db4:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80103dba:	0f b6 00             	movzbl (%eax),%eax
80103dbd:	0f b6 c0             	movzbl %al,%eax
80103dc0:	89 44 24 04          	mov    %eax,0x4(%esp)
80103dc4:	c7 04 24 c4 91 10 80 	movl   $0x801091c4,(%esp)
80103dcb:	e8 f9 c8 ff ff       	call   801006c9 <cprintf>
  picinit();       // interrupt controller
80103dd0:	e8 25 07 00 00       	call   801044fa <picinit>
  ioapicinit();    // another interrupt controller
80103dd5:	e8 4f f3 ff ff       	call   80103129 <ioapicinit>
  consoleinit();   // I/O devices & their interrupts
80103dda:	e8 0f d0 ff ff       	call   80100dee <consoleinit>
  uartinit();      // serial port
80103ddf:	e8 a3 35 00 00       	call   80107387 <uartinit>
  pinit();         // process table
80103de4:	e8 21 0c 00 00       	call   80104a0a <pinit>
  tvinit();        // trap vectors
80103de9:	e8 56 30 00 00       	call   80106e44 <tvinit>
  binit();         // buffer cache
80103dee:	e8 41 c2 ff ff       	call   80100034 <binit>
  fileinit();      // file table
80103df3:	e8 a4 d8 ff ff       	call   8010169c <fileinit>
  ideinit();       // disk
80103df8:	e8 4e ef ff ff       	call   80102d4b <ideinit>
  if(!ismp)
80103dfd:	a1 c4 3b 11 80       	mov    0x80113bc4,%eax
80103e02:	85 c0                	test   %eax,%eax
80103e04:	75 05                	jne    80103e0b <main+0xb5>
    timerinit();   // uniprocessor timer
80103e06:	e8 7b 2f 00 00       	call   80106d86 <timerinit>
  startothers();   // start other processors
80103e0b:	e8 7f 00 00 00       	call   80103e8f <startothers>
  kinit2(P2V(4*1024*1024), P2V(PHYSTOP)); // must come after startothers()
80103e10:	c7 44 24 04 00 00 00 	movl   $0x8e000000,0x4(%esp)
80103e17:	8e 
80103e18:	c7 04 24 00 00 40 80 	movl   $0x80400000,(%esp)
80103e1f:	e8 84 f4 ff ff       	call   801032a8 <kinit2>
  userinit();      // first user process
80103e24:	e8 00 0d 00 00       	call   80104b29 <userinit>
  // Finish setting up this processor in mpmain.
  mpmain();
80103e29:	e8 1a 00 00 00       	call   80103e48 <mpmain>

80103e2e <mpenter>:
}

// Other CPUs jump here from entryother.S.
static void
mpenter(void)
{
80103e2e:	55                   	push   %ebp
80103e2f:	89 e5                	mov    %esp,%ebp
80103e31:	83 ec 08             	sub    $0x8,%esp
  switchkvm();
80103e34:	e8 69 48 00 00       	call   801086a2 <switchkvm>
  seginit();
80103e39:	e8 01 42 00 00       	call   8010803f <seginit>
  lapicinit();
80103e3e:	e8 5b f7 ff ff       	call   8010359e <lapicinit>
  mpmain();
80103e43:	e8 00 00 00 00       	call   80103e48 <mpmain>

80103e48 <mpmain>:
}

// Common CPU setup code.
static void
mpmain(void)
{
80103e48:	55                   	push   %ebp
80103e49:	89 e5                	mov    %esp,%ebp
80103e4b:	83 ec 18             	sub    $0x18,%esp
  cprintf("cpu%d: starting\n", cpu->id);
80103e4e:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80103e54:	0f b6 00             	movzbl (%eax),%eax
80103e57:	0f b6 c0             	movzbl %al,%eax
80103e5a:	89 44 24 04          	mov    %eax,0x4(%esp)
80103e5e:	c7 04 24 db 91 10 80 	movl   $0x801091db,(%esp)
80103e65:	e8 5f c8 ff ff       	call   801006c9 <cprintf>
  idtinit();       // load idt register
80103e6a:	e8 4f 31 00 00       	call   80106fbe <idtinit>
  xchg(&cpu->started, 1); // tell startothers() we're up
80103e6f:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80103e75:	05 a8 00 00 00       	add    $0xa8,%eax
80103e7a:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80103e81:	00 
80103e82:	89 04 24             	mov    %eax,(%esp)
80103e85:	e8 b2 fe ff ff       	call   80103d3c <xchg>
  scheduler();     // start running processes
80103e8a:	e8 f0 12 00 00       	call   8010517f <scheduler>

80103e8f <startothers>:
pde_t entrypgdir[];  // For entry.S

// Start the non-boot (AP) processors.
static void
startothers(void)
{
80103e8f:	55                   	push   %ebp
80103e90:	89 e5                	mov    %esp,%ebp
80103e92:	83 ec 28             	sub    $0x28,%esp
  char *stack;

  // Write entry code to unused memory at 0x7000.
  // The linker has placed the image of entryother.S in
  // _binary_entryother_start.
  code = P2V(0x7000);
80103e95:	c7 45 ec 00 70 00 80 	movl   $0x80007000,-0x14(%ebp)
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);
80103e9c:	b8 86 00 00 00       	mov    $0x86,%eax
80103ea1:	89 44 24 08          	mov    %eax,0x8(%esp)
80103ea5:	c7 44 24 04 4c c5 10 	movl   $0x8010c54c,0x4(%esp)
80103eac:	80 
80103ead:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103eb0:	89 04 24             	mov    %eax,(%esp)
80103eb3:	e8 c5 1b 00 00       	call   80105a7d <memmove>

  for(c = cpus; c < cpus+ncpu; c++){
80103eb8:	c7 45 f0 e0 3b 11 80 	movl   $0x80113be0,-0x10(%ebp)
80103ebf:	e9 93 00 00 00       	jmp    80103f57 <startothers+0xc8>
    if(c == cpus+cpunum())  // We've started already.
80103ec4:	e8 31 f8 ff ff       	call   801036fa <cpunum>
80103ec9:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
80103ecf:	05 e0 3b 11 80       	add    $0x80113be0,%eax
80103ed4:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80103ed7:	74 76                	je     80103f4f <startothers+0xc0>
      continue;

    // Tell entryother.S what stack to use, where to enter, and what
    // pgdir to use. We cannot use kpgdir yet, because the AP processor
    // is running in low  memory, so we use entrypgdir for the APs too.
    if((stack = kalloc()) == 0)
80103ed9:	e8 c0 f4 ff ff       	call   8010339e <kalloc>
80103ede:	89 45 f4             	mov    %eax,-0xc(%ebp)
80103ee1:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80103ee5:	75 0c                	jne    80103ef3 <startothers+0x64>
      panic("startothers: failed to allocate stack");
80103ee7:	c7 04 24 ec 91 10 80 	movl   $0x801091ec,(%esp)
80103eee:	e8 76 c9 ff ff       	call   80100869 <panic>
    *(void**)(code-4) = stack + KSTACKSIZE;
80103ef3:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103ef6:	83 e8 04             	sub    $0x4,%eax
80103ef9:	8b 55 f4             	mov    -0xc(%ebp),%edx
80103efc:	81 c2 00 10 00 00    	add    $0x1000,%edx
80103f02:	89 10                	mov    %edx,(%eax)
    *(void**)(code-8) = mpenter;
80103f04:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103f07:	83 e8 08             	sub    $0x8,%eax
80103f0a:	c7 00 2e 3e 10 80    	movl   $0x80103e2e,(%eax)
    *(int**)(code-12) = (void *) V2P(entrypgdir);
80103f10:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103f13:	8d 50 f4             	lea    -0xc(%eax),%edx
80103f16:	b8 00 b0 10 80       	mov    $0x8010b000,%eax
80103f1b:	2d 00 00 00 80       	sub    $0x80000000,%eax
80103f20:	89 02                	mov    %eax,(%edx)

    lapicstartap(c->id, V2P(code));
80103f22:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103f25:	8d 90 00 00 00 80    	lea    -0x80000000(%eax),%edx
80103f2b:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103f2e:	0f b6 00             	movzbl (%eax),%eax
80103f31:	0f b6 c0             	movzbl %al,%eax
80103f34:	89 54 24 04          	mov    %edx,0x4(%esp)
80103f38:	89 04 24             	mov    %eax,(%esp)
80103f3b:	e8 40 f8 ff ff       	call   80103780 <lapicstartap>

    // wait for cpu to finish mpmain()
    while(c->started == 0)
80103f40:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103f43:	8b 80 a8 00 00 00    	mov    0xa8(%eax),%eax
80103f49:	85 c0                	test   %eax,%eax
80103f4b:	74 f3                	je     80103f40 <startothers+0xb1>
80103f4d:	eb 01                	jmp    80103f50 <startothers+0xc1>
  code = P2V(0x7000);
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);

  for(c = cpus; c < cpus+ncpu; c++){
    if(c == cpus+cpunum())  // We've started already.
      continue;
80103f4f:	90                   	nop
  // The linker has placed the image of entryother.S in
  // _binary_entryother_start.
  code = P2V(0x7000);
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);

  for(c = cpus; c < cpus+ncpu; c++){
80103f50:	81 45 f0 bc 00 00 00 	addl   $0xbc,-0x10(%ebp)
80103f57:	a1 c0 41 11 80       	mov    0x801141c0,%eax
80103f5c:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
80103f62:	05 e0 3b 11 80       	add    $0x80113be0,%eax
80103f67:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80103f6a:	0f 87 54 ff ff ff    	ja     80103ec4 <startothers+0x35>

    // wait for cpu to finish mpmain()
    while(c->started == 0)
      ;
  }
}
80103f70:	c9                   	leave  
80103f71:	c3                   	ret    
	...

80103f74 <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
80103f74:	55                   	push   %ebp
80103f75:	89 e5                	mov    %esp,%ebp
80103f77:	83 ec 14             	sub    $0x14,%esp
80103f7a:	8b 45 08             	mov    0x8(%ebp),%eax
80103f7d:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80103f81:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
80103f85:	89 c2                	mov    %eax,%edx
80103f87:	ec                   	in     (%dx),%al
80103f88:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80103f8b:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80103f8f:	c9                   	leave  
80103f90:	c3                   	ret    

80103f91 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80103f91:	55                   	push   %ebp
80103f92:	89 e5                	mov    %esp,%ebp
80103f94:	83 ec 08             	sub    $0x8,%esp
80103f97:	8b 55 08             	mov    0x8(%ebp),%edx
80103f9a:	8b 45 0c             	mov    0xc(%ebp),%eax
80103f9d:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80103fa1:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80103fa4:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80103fa8:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80103fac:	ee                   	out    %al,(%dx)
}
80103fad:	c9                   	leave  
80103fae:	c3                   	ret    

80103faf <sum>:
int ncpu;
uchar ioapicid;

static uchar
sum(uchar *addr, int len)
{
80103faf:	55                   	push   %ebp
80103fb0:	89 e5                	mov    %esp,%ebp
80103fb2:	83 ec 10             	sub    $0x10,%esp
  int i, sum;

  sum = 0;
80103fb5:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  for(i=0; i<len; i++)
80103fbc:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)
80103fc3:	eb 13                	jmp    80103fd8 <sum+0x29>
    sum += addr[i];
80103fc5:	8b 45 f8             	mov    -0x8(%ebp),%eax
80103fc8:	03 45 08             	add    0x8(%ebp),%eax
80103fcb:	0f b6 00             	movzbl (%eax),%eax
80103fce:	0f b6 c0             	movzbl %al,%eax
80103fd1:	01 45 fc             	add    %eax,-0x4(%ebp)
sum(uchar *addr, int len)
{
  int i, sum;

  sum = 0;
  for(i=0; i<len; i++)
80103fd4:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
80103fd8:	8b 45 f8             	mov    -0x8(%ebp),%eax
80103fdb:	3b 45 0c             	cmp    0xc(%ebp),%eax
80103fde:	7c e5                	jl     80103fc5 <sum+0x16>
    sum += addr[i];
  return sum;
80103fe0:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80103fe3:	c9                   	leave  
80103fe4:	c3                   	ret    

80103fe5 <mpsearch1>:

// Look for an MP structure in the len bytes at addr.
static struct mp*
mpsearch1(uint a, int len)
{
80103fe5:	55                   	push   %ebp
80103fe6:	89 e5                	mov    %esp,%ebp
80103fe8:	83 ec 28             	sub    $0x28,%esp
  uchar *e, *p, *addr;

  addr = P2V(a);
80103feb:	8b 45 08             	mov    0x8(%ebp),%eax
80103fee:	2d 00 00 00 80       	sub    $0x80000000,%eax
80103ff3:	89 45 f4             	mov    %eax,-0xc(%ebp)
  e = addr+len;
80103ff6:	8b 45 0c             	mov    0xc(%ebp),%eax
80103ff9:	03 45 f4             	add    -0xc(%ebp),%eax
80103ffc:	89 45 ec             	mov    %eax,-0x14(%ebp)
  for(p = addr; p < e; p += sizeof(struct mp))
80103fff:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104002:	89 45 f0             	mov    %eax,-0x10(%ebp)
80104005:	eb 3f                	jmp    80104046 <mpsearch1+0x61>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
80104007:	c7 44 24 08 04 00 00 	movl   $0x4,0x8(%esp)
8010400e:	00 
8010400f:	c7 44 24 04 14 92 10 	movl   $0x80109214,0x4(%esp)
80104016:	80 
80104017:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010401a:	89 04 24             	mov    %eax,(%esp)
8010401d:	e8 ff 19 00 00       	call   80105a21 <memcmp>
80104022:	85 c0                	test   %eax,%eax
80104024:	75 1c                	jne    80104042 <mpsearch1+0x5d>
80104026:	c7 44 24 04 10 00 00 	movl   $0x10,0x4(%esp)
8010402d:	00 
8010402e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104031:	89 04 24             	mov    %eax,(%esp)
80104034:	e8 76 ff ff ff       	call   80103faf <sum>
80104039:	84 c0                	test   %al,%al
8010403b:	75 05                	jne    80104042 <mpsearch1+0x5d>
      return (struct mp*)p;
8010403d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104040:	eb 11                	jmp    80104053 <mpsearch1+0x6e>
{
  uchar *e, *p, *addr;

  addr = P2V(a);
  e = addr+len;
  for(p = addr; p < e; p += sizeof(struct mp))
80104042:	83 45 f0 10          	addl   $0x10,-0x10(%ebp)
80104046:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104049:	3b 45 ec             	cmp    -0x14(%ebp),%eax
8010404c:	72 b9                	jb     80104007 <mpsearch1+0x22>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
      return (struct mp*)p;
  return 0;
8010404e:	b8 00 00 00 00       	mov    $0x0,%eax
}
80104053:	c9                   	leave  
80104054:	c3                   	ret    

80104055 <mpsearch>:
// 1) in the first KB of the EBDA;
// 2) in the last KB of system base memory;
// 3) in the BIOS ROM between 0xF0000 and 0xFFFFF.
static struct mp*
mpsearch(void)
{
80104055:	55                   	push   %ebp
80104056:	89 e5                	mov    %esp,%ebp
80104058:	83 ec 28             	sub    $0x28,%esp
  uchar *bda;
  uint p;
  struct mp *mp;

  bda = (uchar *) P2V(0x400);
8010405b:	c7 45 ec 00 04 00 80 	movl   $0x80000400,-0x14(%ebp)
  if((p = ((bda[0x0F]<<8)| bda[0x0E]) << 4)){
80104062:	8b 45 ec             	mov    -0x14(%ebp),%eax
80104065:	83 c0 0f             	add    $0xf,%eax
80104068:	0f b6 00             	movzbl (%eax),%eax
8010406b:	0f b6 c0             	movzbl %al,%eax
8010406e:	89 c2                	mov    %eax,%edx
80104070:	c1 e2 08             	shl    $0x8,%edx
80104073:	8b 45 ec             	mov    -0x14(%ebp),%eax
80104076:	83 c0 0e             	add    $0xe,%eax
80104079:	0f b6 00             	movzbl (%eax),%eax
8010407c:	0f b6 c0             	movzbl %al,%eax
8010407f:	09 d0                	or     %edx,%eax
80104081:	c1 e0 04             	shl    $0x4,%eax
80104084:	89 45 f0             	mov    %eax,-0x10(%ebp)
80104087:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010408b:	74 21                	je     801040ae <mpsearch+0x59>
    if((mp = mpsearch1(p, 1024)))
8010408d:	c7 44 24 04 00 04 00 	movl   $0x400,0x4(%esp)
80104094:	00 
80104095:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104098:	89 04 24             	mov    %eax,(%esp)
8010409b:	e8 45 ff ff ff       	call   80103fe5 <mpsearch1>
801040a0:	89 45 f4             	mov    %eax,-0xc(%ebp)
801040a3:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801040a7:	74 50                	je     801040f9 <mpsearch+0xa4>
      return mp;
801040a9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801040ac:	eb 5f                	jmp    8010410d <mpsearch+0xb8>
  } else {
    p = ((bda[0x14]<<8)|bda[0x13])*1024;
801040ae:	8b 45 ec             	mov    -0x14(%ebp),%eax
801040b1:	83 c0 14             	add    $0x14,%eax
801040b4:	0f b6 00             	movzbl (%eax),%eax
801040b7:	0f b6 c0             	movzbl %al,%eax
801040ba:	89 c2                	mov    %eax,%edx
801040bc:	c1 e2 08             	shl    $0x8,%edx
801040bf:	8b 45 ec             	mov    -0x14(%ebp),%eax
801040c2:	83 c0 13             	add    $0x13,%eax
801040c5:	0f b6 00             	movzbl (%eax),%eax
801040c8:	0f b6 c0             	movzbl %al,%eax
801040cb:	09 d0                	or     %edx,%eax
801040cd:	c1 e0 0a             	shl    $0xa,%eax
801040d0:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if((mp = mpsearch1(p-1024, 1024)))
801040d3:	8b 45 f0             	mov    -0x10(%ebp),%eax
801040d6:	2d 00 04 00 00       	sub    $0x400,%eax
801040db:	c7 44 24 04 00 04 00 	movl   $0x400,0x4(%esp)
801040e2:	00 
801040e3:	89 04 24             	mov    %eax,(%esp)
801040e6:	e8 fa fe ff ff       	call   80103fe5 <mpsearch1>
801040eb:	89 45 f4             	mov    %eax,-0xc(%ebp)
801040ee:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801040f2:	74 05                	je     801040f9 <mpsearch+0xa4>
      return mp;
801040f4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801040f7:	eb 14                	jmp    8010410d <mpsearch+0xb8>
  }
  return mpsearch1(0xF0000, 0x10000);
801040f9:	c7 44 24 04 00 00 01 	movl   $0x10000,0x4(%esp)
80104100:	00 
80104101:	c7 04 24 00 00 0f 00 	movl   $0xf0000,(%esp)
80104108:	e8 d8 fe ff ff       	call   80103fe5 <mpsearch1>
}
8010410d:	c9                   	leave  
8010410e:	c3                   	ret    

8010410f <mpconfig>:
// Check for correct signature, calculate the checksum and,
// if correct, check the version.
// To do: check extended table checksum.
static struct mpconf*
mpconfig(struct mp **pmp)
{
8010410f:	55                   	push   %ebp
80104110:	89 e5                	mov    %esp,%ebp
80104112:	83 ec 28             	sub    $0x28,%esp
  struct mpconf *conf;
  struct mp *mp;

  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
80104115:	e8 3b ff ff ff       	call   80104055 <mpsearch>
8010411a:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010411d:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104121:	74 0a                	je     8010412d <mpconfig+0x1e>
80104123:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104126:	8b 40 04             	mov    0x4(%eax),%eax
80104129:	85 c0                	test   %eax,%eax
8010412b:	75 0a                	jne    80104137 <mpconfig+0x28>
    return 0;
8010412d:	b8 00 00 00 00       	mov    $0x0,%eax
80104132:	e9 80 00 00 00       	jmp    801041b7 <mpconfig+0xa8>
  conf = (struct mpconf*) P2V((uint) mp->physaddr);
80104137:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010413a:	8b 40 04             	mov    0x4(%eax),%eax
8010413d:	2d 00 00 00 80       	sub    $0x80000000,%eax
80104142:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(memcmp(conf, "PCMP", 4) != 0)
80104145:	c7 44 24 08 04 00 00 	movl   $0x4,0x8(%esp)
8010414c:	00 
8010414d:	c7 44 24 04 19 92 10 	movl   $0x80109219,0x4(%esp)
80104154:	80 
80104155:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104158:	89 04 24             	mov    %eax,(%esp)
8010415b:	e8 c1 18 00 00       	call   80105a21 <memcmp>
80104160:	85 c0                	test   %eax,%eax
80104162:	74 07                	je     8010416b <mpconfig+0x5c>
    return 0;
80104164:	b8 00 00 00 00       	mov    $0x0,%eax
80104169:	eb 4c                	jmp    801041b7 <mpconfig+0xa8>
  if(conf->specrev != 1 && conf->specrev != 4)
8010416b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010416e:	0f b6 40 06          	movzbl 0x6(%eax),%eax
80104172:	3c 01                	cmp    $0x1,%al
80104174:	74 12                	je     80104188 <mpconfig+0x79>
80104176:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104179:	0f b6 40 06          	movzbl 0x6(%eax),%eax
8010417d:	3c 04                	cmp    $0x4,%al
8010417f:	74 07                	je     80104188 <mpconfig+0x79>
    return 0;
80104181:	b8 00 00 00 00       	mov    $0x0,%eax
80104186:	eb 2f                	jmp    801041b7 <mpconfig+0xa8>
  if(sum((uchar*)conf, conf->length) != 0)
80104188:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010418b:	0f b7 40 04          	movzwl 0x4(%eax),%eax
8010418f:	0f b7 d0             	movzwl %ax,%edx
80104192:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104195:	89 54 24 04          	mov    %edx,0x4(%esp)
80104199:	89 04 24             	mov    %eax,(%esp)
8010419c:	e8 0e fe ff ff       	call   80103faf <sum>
801041a1:	84 c0                	test   %al,%al
801041a3:	74 07                	je     801041ac <mpconfig+0x9d>
    return 0;
801041a5:	b8 00 00 00 00       	mov    $0x0,%eax
801041aa:	eb 0b                	jmp    801041b7 <mpconfig+0xa8>
  *pmp = mp;
801041ac:	8b 45 08             	mov    0x8(%ebp),%eax
801041af:	8b 55 f4             	mov    -0xc(%ebp),%edx
801041b2:	89 10                	mov    %edx,(%eax)
  return conf;
801041b4:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
801041b7:	c9                   	leave  
801041b8:	c3                   	ret    

801041b9 <mpinit>:

void
mpinit(void)
{
801041b9:	55                   	push   %ebp
801041ba:	89 e5                	mov    %esp,%ebp
801041bc:	83 ec 38             	sub    $0x38,%esp
  struct mp *mp;
  struct mpconf *conf;
  struct mpproc *proc;
  struct mpioapic *mioapic;

  if((conf = mpconfig(&mp)) == 0)
801041bf:	8d 45 e0             	lea    -0x20(%ebp),%eax
801041c2:	89 04 24             	mov    %eax,(%esp)
801041c5:	e8 45 ff ff ff       	call   8010410f <mpconfig>
801041ca:	89 45 ec             	mov    %eax,-0x14(%ebp)
801041cd:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
801041d1:	0f 84 80 02 00 00    	je     80104457 <mpinit+0x29e>
    return;
  ismp = 1;
801041d7:	c7 05 c4 3b 11 80 01 	movl   $0x1,0x80113bc4
801041de:	00 00 00 
  lapic = (uint*)conf->lapicaddr;
801041e1:	8b 45 ec             	mov    -0x14(%ebp),%eax
801041e4:	8b 40 24             	mov    0x24(%eax),%eax
801041e7:	a3 dc 3a 11 80       	mov    %eax,0x80113adc
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
801041ec:	8b 45 ec             	mov    -0x14(%ebp),%eax
801041ef:	83 c0 2c             	add    $0x2c,%eax
801041f2:	89 45 e4             	mov    %eax,-0x1c(%ebp)
801041f5:	8b 55 ec             	mov    -0x14(%ebp),%edx
801041f8:	8b 45 ec             	mov    -0x14(%ebp),%eax
801041fb:	0f b7 40 04          	movzwl 0x4(%eax),%eax
801041ff:	0f b7 c0             	movzwl %ax,%eax
80104202:	8d 04 02             	lea    (%edx,%eax,1),%eax
80104205:	89 45 e8             	mov    %eax,-0x18(%ebp)
80104208:	e9 cb 01 00 00       	jmp    801043d8 <mpinit+0x21f>
    switch(*p){
8010420d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104210:	0f b6 00             	movzbl (%eax),%eax
80104213:	0f b6 c0             	movzbl %al,%eax
80104216:	83 f8 04             	cmp    $0x4,%eax
80104219:	0f 87 96 01 00 00    	ja     801043b5 <mpinit+0x1fc>
8010421f:	8b 04 85 2c 93 10 80 	mov    -0x7fef6cd4(,%eax,4),%eax
80104226:	ff e0                	jmp    *%eax
    case MPPROC:
      proc = (struct mpproc*)p;
80104228:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010422b:	89 45 f0             	mov    %eax,-0x10(%ebp)
      if(!(proc->flags & MP_PROC_ENABLED)){
8010422e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104231:	0f b6 40 03          	movzbl 0x3(%eax),%eax
80104235:	0f b6 c0             	movzbl %al,%eax
80104238:	83 e0 01             	and    $0x1,%eax
8010423b:	85 c0                	test   %eax,%eax
8010423d:	75 23                	jne    80104262 <mpinit+0xa9>
        cprintf("mpinit: ignoring cpu %d, not enabled\n", proc->apicid);
8010423f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104242:	0f b6 40 01          	movzbl 0x1(%eax),%eax
80104246:	0f b6 c0             	movzbl %al,%eax
80104249:	89 44 24 04          	mov    %eax,0x4(%esp)
8010424d:	c7 04 24 20 92 10 80 	movl   $0x80109220,(%esp)
80104254:	e8 70 c4 ff ff       	call   801006c9 <cprintf>
        p += sizeof(struct mpproc);
80104259:	83 45 e4 14          	addl   $0x14,-0x1c(%ebp)
        continue;
8010425d:	e9 76 01 00 00       	jmp    801043d8 <mpinit+0x21f>
      }
      if(!(proc->feature & MP_PROC_APIC)){
80104262:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104265:	8b 40 08             	mov    0x8(%eax),%eax
80104268:	25 00 02 00 00       	and    $0x200,%eax
8010426d:	85 c0                	test   %eax,%eax
8010426f:	75 23                	jne    80104294 <mpinit+0xdb>
        cprintf("mpinit: cpu %d has no working APIC, ignored\n", proc->apicid);
80104271:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104274:	0f b6 40 01          	movzbl 0x1(%eax),%eax
80104278:	0f b6 c0             	movzbl %al,%eax
8010427b:	89 44 24 04          	mov    %eax,0x4(%esp)
8010427f:	c7 04 24 48 92 10 80 	movl   $0x80109248,(%esp)
80104286:	e8 3e c4 ff ff       	call   801006c9 <cprintf>
        p += sizeof(struct mpproc);
8010428b:	83 45 e4 14          	addl   $0x14,-0x1c(%ebp)
        continue;
8010428f:	e9 44 01 00 00       	jmp    801043d8 <mpinit+0x21f>
      }
      if(ncpu >= NCPU){
80104294:	a1 c0 41 11 80       	mov    0x801141c0,%eax
80104299:	83 f8 07             	cmp    $0x7,%eax
8010429c:	7e 2b                	jle    801042c9 <mpinit+0x110>
        cprintf("mpinit: more than %d cpus, ignoring cpu%d\n",
          NCPU, proc->apicid);
8010429e:	8b 45 f0             	mov    -0x10(%ebp),%eax
801042a1:	0f b6 40 01          	movzbl 0x1(%eax),%eax
        cprintf("mpinit: cpu %d has no working APIC, ignored\n", proc->apicid);
        p += sizeof(struct mpproc);
        continue;
      }
      if(ncpu >= NCPU){
        cprintf("mpinit: more than %d cpus, ignoring cpu%d\n",
801042a5:	0f b6 c0             	movzbl %al,%eax
801042a8:	89 44 24 08          	mov    %eax,0x8(%esp)
801042ac:	c7 44 24 04 08 00 00 	movl   $0x8,0x4(%esp)
801042b3:	00 
801042b4:	c7 04 24 78 92 10 80 	movl   $0x80109278,(%esp)
801042bb:	e8 09 c4 ff ff       	call   801006c9 <cprintf>
          NCPU, proc->apicid);
        p += sizeof(struct mpproc);
801042c0:	83 45 e4 14          	addl   $0x14,-0x1c(%ebp)
        continue;
801042c4:	e9 0f 01 00 00       	jmp    801043d8 <mpinit+0x21f>
      // says that they DON'T have to be. As long as QEMU is used to simulate
      // multiple SOCKETS everything seems fine, but as soon as we add CORES
      // the code here breaks (and fails to identify them). The confusion
      // between the id we assign here and the actual APIC id continues in
      // ioapic.c for example. A rewrite is in order.
      if(ncpu != proc->apicid){
801042c9:	8b 45 f0             	mov    -0x10(%ebp),%eax
801042cc:	0f b6 40 01          	movzbl 0x1(%eax),%eax
801042d0:	0f b6 d0             	movzbl %al,%edx
801042d3:	a1 c0 41 11 80       	mov    0x801141c0,%eax
801042d8:	39 c2                	cmp    %eax,%edx
801042da:	74 2d                	je     80104309 <mpinit+0x150>
        cprintf("mpinit: ncpu=%d apicid=%d\n", ncpu, proc->apicid);
801042dc:	8b 45 f0             	mov    -0x10(%ebp),%eax
801042df:	0f b6 40 01          	movzbl 0x1(%eax),%eax
801042e3:	0f b6 d0             	movzbl %al,%edx
801042e6:	a1 c0 41 11 80       	mov    0x801141c0,%eax
801042eb:	89 54 24 08          	mov    %edx,0x8(%esp)
801042ef:	89 44 24 04          	mov    %eax,0x4(%esp)
801042f3:	c7 04 24 a3 92 10 80 	movl   $0x801092a3,(%esp)
801042fa:	e8 ca c3 ff ff       	call   801006c9 <cprintf>
        ismp = 0;
801042ff:	c7 05 c4 3b 11 80 00 	movl   $0x0,0x80113bc4
80104306:	00 00 00 
      }
      cpus[ncpu].id = ncpu;
80104309:	a1 c0 41 11 80       	mov    0x801141c0,%eax
8010430e:	8b 15 c0 41 11 80    	mov    0x801141c0,%edx
80104314:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
8010431a:	88 90 e0 3b 11 80    	mov    %dl,-0x7feec420(%eax)
      ncpu++;
80104320:	a1 c0 41 11 80       	mov    0x801141c0,%eax
80104325:	83 c0 01             	add    $0x1,%eax
80104328:	a3 c0 41 11 80       	mov    %eax,0x801141c0
      p += sizeof(struct mpproc);
8010432d:	83 45 e4 14          	addl   $0x14,-0x1c(%ebp)
      continue;
80104331:	e9 a2 00 00 00       	jmp    801043d8 <mpinit+0x21f>
    case MPIOAPIC:
      mioapic = (struct mpioapic*)p;
80104336:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104339:	89 45 f4             	mov    %eax,-0xc(%ebp)
      if(!(mioapic->flags & MP_APIC_ENABLED)){
8010433c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010433f:	0f b6 40 03          	movzbl 0x3(%eax),%eax
80104343:	0f b6 c0             	movzbl %al,%eax
80104346:	83 e0 01             	and    $0x1,%eax
80104349:	85 c0                	test   %eax,%eax
8010434b:	75 20                	jne    8010436d <mpinit+0x1b4>
        cprintf("mpinit: ioapic %d disabled, ignored\n", mioapic->apicid);
8010434d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104350:	0f b6 40 01          	movzbl 0x1(%eax),%eax
80104354:	0f b6 c0             	movzbl %al,%eax
80104357:	89 44 24 04          	mov    %eax,0x4(%esp)
8010435b:	c7 04 24 c0 92 10 80 	movl   $0x801092c0,(%esp)
80104362:	e8 62 c3 ff ff       	call   801006c9 <cprintf>
        p += sizeof(struct mpioapic);
80104367:	83 45 e4 08          	addl   $0x8,-0x1c(%ebp)
        continue;
8010436b:	eb 6b                	jmp    801043d8 <mpinit+0x21f>
      }
      if(ioapic == 0){
8010436d:	a1 94 3a 11 80       	mov    0x80113a94,%eax
80104372:	85 c0                	test   %eax,%eax
80104374:	75 19                	jne    8010438f <mpinit+0x1d6>
        ioapic = (volatile struct ioapic*)mioapic->addr;
80104376:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104379:	8b 40 04             	mov    0x4(%eax),%eax
8010437c:	a3 94 3a 11 80       	mov    %eax,0x80113a94
        ioapicid = mioapic->apicid;
80104381:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104384:	0f b6 40 01          	movzbl 0x1(%eax),%eax
80104388:	a2 c0 3b 11 80       	mov    %al,0x80113bc0
8010438d:	eb 1a                	jmp    801043a9 <mpinit+0x1f0>
      } else {
        cprintf("mpinit: ignored extra ioapic %d\n", mioapic->apicid);
8010438f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104392:	0f b6 40 01          	movzbl 0x1(%eax),%eax
80104396:	0f b6 c0             	movzbl %al,%eax
80104399:	89 44 24 04          	mov    %eax,0x4(%esp)
8010439d:	c7 04 24 e8 92 10 80 	movl   $0x801092e8,(%esp)
801043a4:	e8 20 c3 ff ff       	call   801006c9 <cprintf>
      }
      p += sizeof(struct mpioapic);
801043a9:	83 45 e4 08          	addl   $0x8,-0x1c(%ebp)
      continue;
801043ad:	eb 29                	jmp    801043d8 <mpinit+0x21f>
    case MPBUS:
    case MPIOINTR:
    case MPLINTR:
      p += 8;
801043af:	83 45 e4 08          	addl   $0x8,-0x1c(%ebp)
      continue;
801043b3:	eb 23                	jmp    801043d8 <mpinit+0x21f>
    default:
      cprintf("mpinit: unknown config type %x\n", *p);
801043b5:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801043b8:	0f b6 00             	movzbl (%eax),%eax
801043bb:	0f b6 c0             	movzbl %al,%eax
801043be:	89 44 24 04          	mov    %eax,0x4(%esp)
801043c2:	c7 04 24 0c 93 10 80 	movl   $0x8010930c,(%esp)
801043c9:	e8 fb c2 ff ff       	call   801006c9 <cprintf>
      ismp = 0;
801043ce:	c7 05 c4 3b 11 80 00 	movl   $0x0,0x80113bc4
801043d5:	00 00 00 

  if((conf = mpconfig(&mp)) == 0)
    return;
  ismp = 1;
  lapic = (uint*)conf->lapicaddr;
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
801043d8:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801043db:	3b 45 e8             	cmp    -0x18(%ebp),%eax
801043de:	0f 82 29 fe ff ff    	jb     8010420d <mpinit+0x54>
    default:
      cprintf("mpinit: unknown config type %x\n", *p);
      ismp = 0;
    }
  }
  if(!ismp){
801043e4:	a1 c4 3b 11 80       	mov    0x80113bc4,%eax
801043e9:	85 c0                	test   %eax,%eax
801043eb:	75 27                	jne    80104414 <mpinit+0x25b>
    // Didn't like what we found; fall back to no MP.
    ncpu = 1;
801043ed:	c7 05 c0 41 11 80 01 	movl   $0x1,0x801141c0
801043f4:	00 00 00 
    lapic = 0;
801043f7:	c7 05 dc 3a 11 80 00 	movl   $0x0,0x80113adc
801043fe:	00 00 00 
    ioapic = 0;
80104401:	c7 05 94 3a 11 80 00 	movl   $0x0,0x80113a94
80104408:	00 00 00 
    ioapicid = 0;
8010440b:	c6 05 c0 3b 11 80 00 	movb   $0x0,0x80113bc0
    return;
80104412:	eb 44                	jmp    80104458 <mpinit+0x29f>
  }

  if(mp->imcrp){
80104414:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104417:	0f b6 40 0c          	movzbl 0xc(%eax),%eax
8010441b:	84 c0                	test   %al,%al
8010441d:	74 39                	je     80104458 <mpinit+0x29f>
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
8010441f:	c7 44 24 04 70 00 00 	movl   $0x70,0x4(%esp)
80104426:	00 
80104427:	c7 04 24 22 00 00 00 	movl   $0x22,(%esp)
8010442e:	e8 5e fb ff ff       	call   80103f91 <outb>
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
80104433:	c7 04 24 23 00 00 00 	movl   $0x23,(%esp)
8010443a:	e8 35 fb ff ff       	call   80103f74 <inb>
8010443f:	83 c8 01             	or     $0x1,%eax
80104442:	0f b6 c0             	movzbl %al,%eax
80104445:	89 44 24 04          	mov    %eax,0x4(%esp)
80104449:	c7 04 24 23 00 00 00 	movl   $0x23,(%esp)
80104450:	e8 3c fb ff ff       	call   80103f91 <outb>
80104455:	eb 01                	jmp    80104458 <mpinit+0x29f>
  struct mpconf *conf;
  struct mpproc *proc;
  struct mpioapic *mioapic;

  if((conf = mpconfig(&mp)) == 0)
    return;
80104457:	90                   	nop
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
  }
}
80104458:	c9                   	leave  
80104459:	c3                   	ret    
	...

8010445c <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
8010445c:	55                   	push   %ebp
8010445d:	89 e5                	mov    %esp,%ebp
8010445f:	83 ec 08             	sub    $0x8,%esp
80104462:	8b 55 08             	mov    0x8(%ebp),%edx
80104465:	8b 45 0c             	mov    0xc(%ebp),%eax
80104468:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
8010446c:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010446f:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80104473:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80104477:	ee                   	out    %al,(%dx)
}
80104478:	c9                   	leave  
80104479:	c3                   	ret    

8010447a <picsetmask>:
// Initial IRQ mask has interrupt 2 enabled (for slave 8259A).
static ushort irqmask = 0xFFFF & ~(1<<IRQ_SLAVE);

static void
picsetmask(ushort mask)
{
8010447a:	55                   	push   %ebp
8010447b:	89 e5                	mov    %esp,%ebp
8010447d:	83 ec 0c             	sub    $0xc,%esp
80104480:	8b 45 08             	mov    0x8(%ebp),%eax
80104483:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  irqmask = mask;
80104487:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
8010448b:	66 a3 00 c0 10 80    	mov    %ax,0x8010c000
  outb(IO_PIC1+1, mask);
80104491:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
80104495:	0f b6 c0             	movzbl %al,%eax
80104498:	89 44 24 04          	mov    %eax,0x4(%esp)
8010449c:	c7 04 24 21 00 00 00 	movl   $0x21,(%esp)
801044a3:	e8 b4 ff ff ff       	call   8010445c <outb>
  outb(IO_PIC2+1, mask >> 8);
801044a8:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
801044ac:	66 c1 e8 08          	shr    $0x8,%ax
801044b0:	0f b6 c0             	movzbl %al,%eax
801044b3:	89 44 24 04          	mov    %eax,0x4(%esp)
801044b7:	c7 04 24 a1 00 00 00 	movl   $0xa1,(%esp)
801044be:	e8 99 ff ff ff       	call   8010445c <outb>
}
801044c3:	c9                   	leave  
801044c4:	c3                   	ret    

801044c5 <picenable>:

void
picenable(int irq)
{
801044c5:	55                   	push   %ebp
801044c6:	89 e5                	mov    %esp,%ebp
801044c8:	53                   	push   %ebx
801044c9:	83 ec 04             	sub    $0x4,%esp
  picsetmask(irqmask & ~(1<<irq));
801044cc:	8b 45 08             	mov    0x8(%ebp),%eax
801044cf:	ba 01 00 00 00       	mov    $0x1,%edx
801044d4:	89 d3                	mov    %edx,%ebx
801044d6:	89 c1                	mov    %eax,%ecx
801044d8:	d3 e3                	shl    %cl,%ebx
801044da:	89 d8                	mov    %ebx,%eax
801044dc:	89 c2                	mov    %eax,%edx
801044de:	f7 d2                	not    %edx
801044e0:	0f b7 05 00 c0 10 80 	movzwl 0x8010c000,%eax
801044e7:	21 d0                	and    %edx,%eax
801044e9:	0f b7 c0             	movzwl %ax,%eax
801044ec:	89 04 24             	mov    %eax,(%esp)
801044ef:	e8 86 ff ff ff       	call   8010447a <picsetmask>
}
801044f4:	83 c4 04             	add    $0x4,%esp
801044f7:	5b                   	pop    %ebx
801044f8:	5d                   	pop    %ebp
801044f9:	c3                   	ret    

801044fa <picinit>:

// Initialize the 8259A interrupt controllers.
void
picinit(void)
{
801044fa:	55                   	push   %ebp
801044fb:	89 e5                	mov    %esp,%ebp
801044fd:	83 ec 08             	sub    $0x8,%esp
  // mask all interrupts
  outb(IO_PIC1+1, 0xFF);
80104500:	c7 44 24 04 ff 00 00 	movl   $0xff,0x4(%esp)
80104507:	00 
80104508:	c7 04 24 21 00 00 00 	movl   $0x21,(%esp)
8010450f:	e8 48 ff ff ff       	call   8010445c <outb>
  outb(IO_PIC2+1, 0xFF);
80104514:	c7 44 24 04 ff 00 00 	movl   $0xff,0x4(%esp)
8010451b:	00 
8010451c:	c7 04 24 a1 00 00 00 	movl   $0xa1,(%esp)
80104523:	e8 34 ff ff ff       	call   8010445c <outb>

  // ICW1:  0001g0hi
  //    g:  0 = edge triggering, 1 = level triggering
  //    h:  0 = cascaded PICs, 1 = master only
  //    i:  0 = no ICW4, 1 = ICW4 required
  outb(IO_PIC1, 0x11);
80104528:	c7 44 24 04 11 00 00 	movl   $0x11,0x4(%esp)
8010452f:	00 
80104530:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
80104537:	e8 20 ff ff ff       	call   8010445c <outb>

  // ICW2:  Vector offset
  outb(IO_PIC1+1, T_IRQ0);
8010453c:	c7 44 24 04 20 00 00 	movl   $0x20,0x4(%esp)
80104543:	00 
80104544:	c7 04 24 21 00 00 00 	movl   $0x21,(%esp)
8010454b:	e8 0c ff ff ff       	call   8010445c <outb>

  // ICW3:  (master PIC) bit mask of IR lines connected to slaves
  //        (slave PIC) 3-bit # of slave's connection to master
  outb(IO_PIC1+1, 1<<IRQ_SLAVE);
80104550:	c7 44 24 04 04 00 00 	movl   $0x4,0x4(%esp)
80104557:	00 
80104558:	c7 04 24 21 00 00 00 	movl   $0x21,(%esp)
8010455f:	e8 f8 fe ff ff       	call   8010445c <outb>
  //    m:  0 = slave PIC, 1 = master PIC
  //      (ignored when b is 0, as the master/slave role
  //      can be hardwired).
  //    a:  1 = Automatic EOI mode
  //    p:  0 = MCS-80/85 mode, 1 = intel x86 mode
  outb(IO_PIC1+1, 0x3);
80104564:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
8010456b:	00 
8010456c:	c7 04 24 21 00 00 00 	movl   $0x21,(%esp)
80104573:	e8 e4 fe ff ff       	call   8010445c <outb>

  // Set up slave (8259A-2)
  outb(IO_PIC2, 0x11);                  // ICW1
80104578:	c7 44 24 04 11 00 00 	movl   $0x11,0x4(%esp)
8010457f:	00 
80104580:	c7 04 24 a0 00 00 00 	movl   $0xa0,(%esp)
80104587:	e8 d0 fe ff ff       	call   8010445c <outb>
  outb(IO_PIC2+1, T_IRQ0 + 8);      // ICW2
8010458c:	c7 44 24 04 28 00 00 	movl   $0x28,0x4(%esp)
80104593:	00 
80104594:	c7 04 24 a1 00 00 00 	movl   $0xa1,(%esp)
8010459b:	e8 bc fe ff ff       	call   8010445c <outb>
  outb(IO_PIC2+1, IRQ_SLAVE);           // ICW3
801045a0:	c7 44 24 04 02 00 00 	movl   $0x2,0x4(%esp)
801045a7:	00 
801045a8:	c7 04 24 a1 00 00 00 	movl   $0xa1,(%esp)
801045af:	e8 a8 fe ff ff       	call   8010445c <outb>
  // NB Automatic EOI mode doesn't tend to work on the slave.
  // Linux source code says it's "to be investigated".
  outb(IO_PIC2+1, 0x3);                 // ICW4
801045b4:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
801045bb:	00 
801045bc:	c7 04 24 a1 00 00 00 	movl   $0xa1,(%esp)
801045c3:	e8 94 fe ff ff       	call   8010445c <outb>

  // OCW3:  0ef01prs
  //   ef:  0x = NOP, 10 = clear specific mask, 11 = set specific mask
  //    p:  0 = no polling, 1 = polling mode
  //   rs:  0x = NOP, 10 = read IRR, 11 = read ISR
  outb(IO_PIC1, 0x68);             // clear specific mask
801045c8:	c7 44 24 04 68 00 00 	movl   $0x68,0x4(%esp)
801045cf:	00 
801045d0:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
801045d7:	e8 80 fe ff ff       	call   8010445c <outb>
  outb(IO_PIC1, 0x0a);             // read IRR by default
801045dc:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
801045e3:	00 
801045e4:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
801045eb:	e8 6c fe ff ff       	call   8010445c <outb>

  outb(IO_PIC2, 0x68);             // OCW3
801045f0:	c7 44 24 04 68 00 00 	movl   $0x68,0x4(%esp)
801045f7:	00 
801045f8:	c7 04 24 a0 00 00 00 	movl   $0xa0,(%esp)
801045ff:	e8 58 fe ff ff       	call   8010445c <outb>
  outb(IO_PIC2, 0x0a);             // OCW3
80104604:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
8010460b:	00 
8010460c:	c7 04 24 a0 00 00 00 	movl   $0xa0,(%esp)
80104613:	e8 44 fe ff ff       	call   8010445c <outb>

  if(irqmask != 0xFFFF)
80104618:	0f b7 05 00 c0 10 80 	movzwl 0x8010c000,%eax
8010461f:	66 83 f8 ff          	cmp    $0xffffffff,%ax
80104623:	74 12                	je     80104637 <picinit+0x13d>
    picsetmask(irqmask);
80104625:	0f b7 05 00 c0 10 80 	movzwl 0x8010c000,%eax
8010462c:	0f b7 c0             	movzwl %ax,%eax
8010462f:	89 04 24             	mov    %eax,(%esp)
80104632:	e8 43 fe ff ff       	call   8010447a <picsetmask>
}
80104637:	c9                   	leave  
80104638:	c3                   	ret    
80104639:	00 00                	add    %al,(%eax)
	...

8010463c <pipealloc>:
  int writeopen;  // write fd is still open
};

int
pipealloc(struct file **f0, struct file **f1)
{
8010463c:	55                   	push   %ebp
8010463d:	89 e5                	mov    %esp,%ebp
8010463f:	83 ec 28             	sub    $0x28,%esp
  struct pipe *p;

  p = 0;
80104642:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  *f0 = *f1 = 0;
80104649:	8b 45 0c             	mov    0xc(%ebp),%eax
8010464c:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
80104652:	8b 45 0c             	mov    0xc(%ebp),%eax
80104655:	8b 10                	mov    (%eax),%edx
80104657:	8b 45 08             	mov    0x8(%ebp),%eax
8010465a:	89 10                	mov    %edx,(%eax)
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
8010465c:	e8 57 d0 ff ff       	call   801016b8 <filealloc>
80104661:	8b 55 08             	mov    0x8(%ebp),%edx
80104664:	89 02                	mov    %eax,(%edx)
80104666:	8b 45 08             	mov    0x8(%ebp),%eax
80104669:	8b 00                	mov    (%eax),%eax
8010466b:	85 c0                	test   %eax,%eax
8010466d:	0f 84 c8 00 00 00    	je     8010473b <pipealloc+0xff>
80104673:	e8 40 d0 ff ff       	call   801016b8 <filealloc>
80104678:	8b 55 0c             	mov    0xc(%ebp),%edx
8010467b:	89 02                	mov    %eax,(%edx)
8010467d:	8b 45 0c             	mov    0xc(%ebp),%eax
80104680:	8b 00                	mov    (%eax),%eax
80104682:	85 c0                	test   %eax,%eax
80104684:	0f 84 b1 00 00 00    	je     8010473b <pipealloc+0xff>
    goto bad;
  if((p = (struct pipe*)kalloc()) == 0)
8010468a:	e8 0f ed ff ff       	call   8010339e <kalloc>
8010468f:	89 45 f4             	mov    %eax,-0xc(%ebp)
80104692:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104696:	0f 84 9e 00 00 00    	je     8010473a <pipealloc+0xfe>
    goto bad;
  p->readopen = 1;
8010469c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010469f:	c7 80 3c 02 00 00 01 	movl   $0x1,0x23c(%eax)
801046a6:	00 00 00 
  p->writeopen = 1;
801046a9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801046ac:	c7 80 40 02 00 00 01 	movl   $0x1,0x240(%eax)
801046b3:	00 00 00 
  p->nwrite = 0;
801046b6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801046b9:	c7 80 38 02 00 00 00 	movl   $0x0,0x238(%eax)
801046c0:	00 00 00 
  p->nread = 0;
801046c3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801046c6:	c7 80 34 02 00 00 00 	movl   $0x0,0x234(%eax)
801046cd:	00 00 00 
  initlock(&p->lock, "pipe");
801046d0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801046d3:	c7 44 24 04 40 93 10 	movl   $0x80109340,0x4(%esp)
801046da:	80 
801046db:	89 04 24             	mov    %eax,(%esp)
801046de:	e8 43 10 00 00       	call   80105726 <initlock>
  (*f0)->type = FD_PIPE;
801046e3:	8b 45 08             	mov    0x8(%ebp),%eax
801046e6:	8b 00                	mov    (%eax),%eax
801046e8:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f0)->readable = 1;
801046ee:	8b 45 08             	mov    0x8(%ebp),%eax
801046f1:	8b 00                	mov    (%eax),%eax
801046f3:	c6 40 08 01          	movb   $0x1,0x8(%eax)
  (*f0)->writable = 0;
801046f7:	8b 45 08             	mov    0x8(%ebp),%eax
801046fa:	8b 00                	mov    (%eax),%eax
801046fc:	c6 40 09 00          	movb   $0x0,0x9(%eax)
  (*f0)->pipe = p;
80104700:	8b 45 08             	mov    0x8(%ebp),%eax
80104703:	8b 00                	mov    (%eax),%eax
80104705:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104708:	89 50 0c             	mov    %edx,0xc(%eax)
  (*f1)->type = FD_PIPE;
8010470b:	8b 45 0c             	mov    0xc(%ebp),%eax
8010470e:	8b 00                	mov    (%eax),%eax
80104710:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f1)->readable = 0;
80104716:	8b 45 0c             	mov    0xc(%ebp),%eax
80104719:	8b 00                	mov    (%eax),%eax
8010471b:	c6 40 08 00          	movb   $0x0,0x8(%eax)
  (*f1)->writable = 1;
8010471f:	8b 45 0c             	mov    0xc(%ebp),%eax
80104722:	8b 00                	mov    (%eax),%eax
80104724:	c6 40 09 01          	movb   $0x1,0x9(%eax)
  (*f1)->pipe = p;
80104728:	8b 45 0c             	mov    0xc(%ebp),%eax
8010472b:	8b 00                	mov    (%eax),%eax
8010472d:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104730:	89 50 0c             	mov    %edx,0xc(%eax)
  return 0;
80104733:	b8 00 00 00 00       	mov    $0x0,%eax
80104738:	eb 43                	jmp    8010477d <pipealloc+0x141>
  p = 0;
  *f0 = *f1 = 0;
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
    goto bad;
  if((p = (struct pipe*)kalloc()) == 0)
    goto bad;
8010473a:	90                   	nop
  (*f1)->pipe = p;
  return 0;

//PAGEBREAK: 20
 bad:
  if(p)
8010473b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010473f:	74 0b                	je     8010474c <pipealloc+0x110>
    kfree((char*)p);
80104741:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104744:	89 04 24             	mov    %eax,(%esp)
80104747:	e8 bc eb ff ff       	call   80103308 <kfree>
  if(*f0)
8010474c:	8b 45 08             	mov    0x8(%ebp),%eax
8010474f:	8b 00                	mov    (%eax),%eax
80104751:	85 c0                	test   %eax,%eax
80104753:	74 0d                	je     80104762 <pipealloc+0x126>
    fileclose(*f0);
80104755:	8b 45 08             	mov    0x8(%ebp),%eax
80104758:	8b 00                	mov    (%eax),%eax
8010475a:	89 04 24             	mov    %eax,(%esp)
8010475d:	e8 ff cf ff ff       	call   80101761 <fileclose>
  if(*f1)
80104762:	8b 45 0c             	mov    0xc(%ebp),%eax
80104765:	8b 00                	mov    (%eax),%eax
80104767:	85 c0                	test   %eax,%eax
80104769:	74 0d                	je     80104778 <pipealloc+0x13c>
    fileclose(*f1);
8010476b:	8b 45 0c             	mov    0xc(%ebp),%eax
8010476e:	8b 00                	mov    (%eax),%eax
80104770:	89 04 24             	mov    %eax,(%esp)
80104773:	e8 e9 cf ff ff       	call   80101761 <fileclose>
  return -1;
80104778:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
8010477d:	c9                   	leave  
8010477e:	c3                   	ret    

8010477f <pipeclose>:

void
pipeclose(struct pipe *p, int writable)
{
8010477f:	55                   	push   %ebp
80104780:	89 e5                	mov    %esp,%ebp
80104782:	83 ec 18             	sub    $0x18,%esp
  acquire(&p->lock);
80104785:	8b 45 08             	mov    0x8(%ebp),%eax
80104788:	89 04 24             	mov    %eax,(%esp)
8010478b:	e8 b7 0f 00 00       	call   80105747 <acquire>
  if(writable){
80104790:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
80104794:	74 1f                	je     801047b5 <pipeclose+0x36>
    p->writeopen = 0;
80104796:	8b 45 08             	mov    0x8(%ebp),%eax
80104799:	c7 80 40 02 00 00 00 	movl   $0x0,0x240(%eax)
801047a0:	00 00 00 
    wakeup(&p->nread);
801047a3:	8b 45 08             	mov    0x8(%ebp),%eax
801047a6:	05 34 02 00 00       	add    $0x234,%eax
801047ab:	89 04 24             	mov    %eax,(%esp)
801047ae:	e8 48 0d 00 00       	call   801054fb <wakeup>
801047b3:	eb 1d                	jmp    801047d2 <pipeclose+0x53>
  } else {
    p->readopen = 0;
801047b5:	8b 45 08             	mov    0x8(%ebp),%eax
801047b8:	c7 80 3c 02 00 00 00 	movl   $0x0,0x23c(%eax)
801047bf:	00 00 00 
    wakeup(&p->nwrite);
801047c2:	8b 45 08             	mov    0x8(%ebp),%eax
801047c5:	05 38 02 00 00       	add    $0x238,%eax
801047ca:	89 04 24             	mov    %eax,(%esp)
801047cd:	e8 29 0d 00 00       	call   801054fb <wakeup>
  }
  if(p->readopen == 0 && p->writeopen == 0){
801047d2:	8b 45 08             	mov    0x8(%ebp),%eax
801047d5:	8b 80 3c 02 00 00    	mov    0x23c(%eax),%eax
801047db:	85 c0                	test   %eax,%eax
801047dd:	75 25                	jne    80104804 <pipeclose+0x85>
801047df:	8b 45 08             	mov    0x8(%ebp),%eax
801047e2:	8b 80 40 02 00 00    	mov    0x240(%eax),%eax
801047e8:	85 c0                	test   %eax,%eax
801047ea:	75 18                	jne    80104804 <pipeclose+0x85>
    release(&p->lock);
801047ec:	8b 45 08             	mov    0x8(%ebp),%eax
801047ef:	89 04 24             	mov    %eax,(%esp)
801047f2:	e8 b9 0f 00 00       	call   801057b0 <release>
    kfree((char*)p);
801047f7:	8b 45 08             	mov    0x8(%ebp),%eax
801047fa:	89 04 24             	mov    %eax,(%esp)
801047fd:	e8 06 eb ff ff       	call   80103308 <kfree>
    wakeup(&p->nread);
  } else {
    p->readopen = 0;
    wakeup(&p->nwrite);
  }
  if(p->readopen == 0 && p->writeopen == 0){
80104802:	eb 0b                	jmp    8010480f <pipeclose+0x90>
    release(&p->lock);
    kfree((char*)p);
  } else
    release(&p->lock);
80104804:	8b 45 08             	mov    0x8(%ebp),%eax
80104807:	89 04 24             	mov    %eax,(%esp)
8010480a:	e8 a1 0f 00 00       	call   801057b0 <release>
}
8010480f:	c9                   	leave  
80104810:	c3                   	ret    

80104811 <pipewrite>:

//PAGEBREAK: 40
int
pipewrite(struct pipe *p, char *addr, int n)
{
80104811:	55                   	push   %ebp
80104812:	89 e5                	mov    %esp,%ebp
80104814:	53                   	push   %ebx
80104815:	83 ec 24             	sub    $0x24,%esp
  int i;

  acquire(&p->lock);
80104818:	8b 45 08             	mov    0x8(%ebp),%eax
8010481b:	89 04 24             	mov    %eax,(%esp)
8010481e:	e8 24 0f 00 00       	call   80105747 <acquire>
  for(i = 0; i < n; i++){
80104823:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010482a:	e9 a6 00 00 00       	jmp    801048d5 <pipewrite+0xc4>
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
      if(p->readopen == 0 || proc->killed){
8010482f:	8b 45 08             	mov    0x8(%ebp),%eax
80104832:	8b 80 3c 02 00 00    	mov    0x23c(%eax),%eax
80104838:	85 c0                	test   %eax,%eax
8010483a:	74 0d                	je     80104849 <pipewrite+0x38>
8010483c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104842:	8b 40 24             	mov    0x24(%eax),%eax
80104845:	85 c0                	test   %eax,%eax
80104847:	74 15                	je     8010485e <pipewrite+0x4d>
        release(&p->lock);
80104849:	8b 45 08             	mov    0x8(%ebp),%eax
8010484c:	89 04 24             	mov    %eax,(%esp)
8010484f:	e8 5c 0f 00 00       	call   801057b0 <release>
        return -1;
80104854:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104859:	e9 9d 00 00 00       	jmp    801048fb <pipewrite+0xea>
      }
      wakeup(&p->nread);
8010485e:	8b 45 08             	mov    0x8(%ebp),%eax
80104861:	05 34 02 00 00       	add    $0x234,%eax
80104866:	89 04 24             	mov    %eax,(%esp)
80104869:	e8 8d 0c 00 00       	call   801054fb <wakeup>
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
8010486e:	8b 45 08             	mov    0x8(%ebp),%eax
80104871:	8b 55 08             	mov    0x8(%ebp),%edx
80104874:	81 c2 38 02 00 00    	add    $0x238,%edx
8010487a:	89 44 24 04          	mov    %eax,0x4(%esp)
8010487e:	89 14 24             	mov    %edx,(%esp)
80104881:	e8 98 0b 00 00       	call   8010541e <sleep>
80104886:	eb 01                	jmp    80104889 <pipewrite+0x78>
{
  int i;

  acquire(&p->lock);
  for(i = 0; i < n; i++){
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
80104888:	90                   	nop
80104889:	8b 45 08             	mov    0x8(%ebp),%eax
8010488c:	8b 90 38 02 00 00    	mov    0x238(%eax),%edx
80104892:	8b 45 08             	mov    0x8(%ebp),%eax
80104895:	8b 80 34 02 00 00    	mov    0x234(%eax),%eax
8010489b:	05 00 02 00 00       	add    $0x200,%eax
801048a0:	39 c2                	cmp    %eax,%edx
801048a2:	74 8b                	je     8010482f <pipewrite+0x1e>
        return -1;
      }
      wakeup(&p->nread);
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
    }
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
801048a4:	8b 45 08             	mov    0x8(%ebp),%eax
801048a7:	8b 80 38 02 00 00    	mov    0x238(%eax),%eax
801048ad:	89 c3                	mov    %eax,%ebx
801048af:	81 e3 ff 01 00 00    	and    $0x1ff,%ebx
801048b5:	8b 55 f4             	mov    -0xc(%ebp),%edx
801048b8:	03 55 0c             	add    0xc(%ebp),%edx
801048bb:	0f b6 0a             	movzbl (%edx),%ecx
801048be:	8b 55 08             	mov    0x8(%ebp),%edx
801048c1:	88 4c 1a 34          	mov    %cl,0x34(%edx,%ebx,1)
801048c5:	8d 50 01             	lea    0x1(%eax),%edx
801048c8:	8b 45 08             	mov    0x8(%ebp),%eax
801048cb:	89 90 38 02 00 00    	mov    %edx,0x238(%eax)
pipewrite(struct pipe *p, char *addr, int n)
{
  int i;

  acquire(&p->lock);
  for(i = 0; i < n; i++){
801048d1:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801048d5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801048d8:	3b 45 10             	cmp    0x10(%ebp),%eax
801048db:	7c ab                	jl     80104888 <pipewrite+0x77>
      wakeup(&p->nread);
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
    }
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
  }
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
801048dd:	8b 45 08             	mov    0x8(%ebp),%eax
801048e0:	05 34 02 00 00       	add    $0x234,%eax
801048e5:	89 04 24             	mov    %eax,(%esp)
801048e8:	e8 0e 0c 00 00       	call   801054fb <wakeup>
  release(&p->lock);
801048ed:	8b 45 08             	mov    0x8(%ebp),%eax
801048f0:	89 04 24             	mov    %eax,(%esp)
801048f3:	e8 b8 0e 00 00       	call   801057b0 <release>
  return n;
801048f8:	8b 45 10             	mov    0x10(%ebp),%eax
}
801048fb:	83 c4 24             	add    $0x24,%esp
801048fe:	5b                   	pop    %ebx
801048ff:	5d                   	pop    %ebp
80104900:	c3                   	ret    

80104901 <piperead>:

int
piperead(struct pipe *p, char *addr, int n)
{
80104901:	55                   	push   %ebp
80104902:	89 e5                	mov    %esp,%ebp
80104904:	53                   	push   %ebx
80104905:	83 ec 24             	sub    $0x24,%esp
  int i;

  acquire(&p->lock);
80104908:	8b 45 08             	mov    0x8(%ebp),%eax
8010490b:	89 04 24             	mov    %eax,(%esp)
8010490e:	e8 34 0e 00 00       	call   80105747 <acquire>
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
80104913:	eb 3a                	jmp    8010494f <piperead+0x4e>
    if(proc->killed){
80104915:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010491b:	8b 40 24             	mov    0x24(%eax),%eax
8010491e:	85 c0                	test   %eax,%eax
80104920:	74 15                	je     80104937 <piperead+0x36>
      release(&p->lock);
80104922:	8b 45 08             	mov    0x8(%ebp),%eax
80104925:	89 04 24             	mov    %eax,(%esp)
80104928:	e8 83 0e 00 00       	call   801057b0 <release>
      return -1;
8010492d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104932:	e9 b6 00 00 00       	jmp    801049ed <piperead+0xec>
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
80104937:	8b 45 08             	mov    0x8(%ebp),%eax
8010493a:	8b 55 08             	mov    0x8(%ebp),%edx
8010493d:	81 c2 34 02 00 00    	add    $0x234,%edx
80104943:	89 44 24 04          	mov    %eax,0x4(%esp)
80104947:	89 14 24             	mov    %edx,(%esp)
8010494a:	e8 cf 0a 00 00       	call   8010541e <sleep>
piperead(struct pipe *p, char *addr, int n)
{
  int i;

  acquire(&p->lock);
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
8010494f:	8b 45 08             	mov    0x8(%ebp),%eax
80104952:	8b 90 34 02 00 00    	mov    0x234(%eax),%edx
80104958:	8b 45 08             	mov    0x8(%ebp),%eax
8010495b:	8b 80 38 02 00 00    	mov    0x238(%eax),%eax
80104961:	39 c2                	cmp    %eax,%edx
80104963:	75 0d                	jne    80104972 <piperead+0x71>
80104965:	8b 45 08             	mov    0x8(%ebp),%eax
80104968:	8b 80 40 02 00 00    	mov    0x240(%eax),%eax
8010496e:	85 c0                	test   %eax,%eax
80104970:	75 a3                	jne    80104915 <piperead+0x14>
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
  }
  for(i = 0; i < n; i++){  //DOC: piperead-copy
80104972:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80104979:	eb 49                	jmp    801049c4 <piperead+0xc3>
    if(p->nread == p->nwrite)
8010497b:	8b 45 08             	mov    0x8(%ebp),%eax
8010497e:	8b 90 34 02 00 00    	mov    0x234(%eax),%edx
80104984:	8b 45 08             	mov    0x8(%ebp),%eax
80104987:	8b 80 38 02 00 00    	mov    0x238(%eax),%eax
8010498d:	39 c2                	cmp    %eax,%edx
8010498f:	74 3d                	je     801049ce <piperead+0xcd>
      break;
    addr[i] = p->data[p->nread++ % PIPESIZE];
80104991:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104994:	89 c2                	mov    %eax,%edx
80104996:	03 55 0c             	add    0xc(%ebp),%edx
80104999:	8b 45 08             	mov    0x8(%ebp),%eax
8010499c:	8b 80 34 02 00 00    	mov    0x234(%eax),%eax
801049a2:	89 c3                	mov    %eax,%ebx
801049a4:	81 e3 ff 01 00 00    	and    $0x1ff,%ebx
801049aa:	8b 4d 08             	mov    0x8(%ebp),%ecx
801049ad:	0f b6 4c 19 34       	movzbl 0x34(%ecx,%ebx,1),%ecx
801049b2:	88 0a                	mov    %cl,(%edx)
801049b4:	8d 50 01             	lea    0x1(%eax),%edx
801049b7:	8b 45 08             	mov    0x8(%ebp),%eax
801049ba:	89 90 34 02 00 00    	mov    %edx,0x234(%eax)
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
  }
  for(i = 0; i < n; i++){  //DOC: piperead-copy
801049c0:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801049c4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801049c7:	3b 45 10             	cmp    0x10(%ebp),%eax
801049ca:	7c af                	jl     8010497b <piperead+0x7a>
801049cc:	eb 01                	jmp    801049cf <piperead+0xce>
    if(p->nread == p->nwrite)
      break;
801049ce:	90                   	nop
    addr[i] = p->data[p->nread++ % PIPESIZE];
  }
  wakeup(&p->nwrite);  //DOC: piperead-wakeup
801049cf:	8b 45 08             	mov    0x8(%ebp),%eax
801049d2:	05 38 02 00 00       	add    $0x238,%eax
801049d7:	89 04 24             	mov    %eax,(%esp)
801049da:	e8 1c 0b 00 00       	call   801054fb <wakeup>
  release(&p->lock);
801049df:	8b 45 08             	mov    0x8(%ebp),%eax
801049e2:	89 04 24             	mov    %eax,(%esp)
801049e5:	e8 c6 0d 00 00       	call   801057b0 <release>
  return i;
801049ea:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801049ed:	83 c4 24             	add    $0x24,%esp
801049f0:	5b                   	pop    %ebx
801049f1:	5d                   	pop    %ebp
801049f2:	c3                   	ret    
	...

801049f4 <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
801049f4:	55                   	push   %ebp
801049f5:	89 e5                	mov    %esp,%ebp
801049f7:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
801049fa:	9c                   	pushf  
801049fb:	58                   	pop    %eax
801049fc:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return eflags;
801049ff:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80104a02:	c9                   	leave  
80104a03:	c3                   	ret    

80104a04 <sti>:
  asm volatile("cli");
}

static inline void
sti(void)
{
80104a04:	55                   	push   %ebp
80104a05:	89 e5                	mov    %esp,%ebp
  asm volatile("sti");
80104a07:	fb                   	sti    
}
80104a08:	5d                   	pop    %ebp
80104a09:	c3                   	ret    

80104a0a <pinit>:

static void wakeup1(void *chan);

void
pinit(void)
{
80104a0a:	55                   	push   %ebp
80104a0b:	89 e5                	mov    %esp,%ebp
80104a0d:	83 ec 18             	sub    $0x18,%esp
  initlock(&ptable.lock, "ptable");
80104a10:	c7 44 24 04 45 93 10 	movl   $0x80109345,0x4(%esp)
80104a17:	80 
80104a18:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80104a1f:	e8 02 0d 00 00       	call   80105726 <initlock>
}
80104a24:	c9                   	leave  
80104a25:	c3                   	ret    

80104a26 <allocproc>:
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
{
80104a26:	55                   	push   %ebp
80104a27:	89 e5                	mov    %esp,%ebp
80104a29:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);
80104a2c:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80104a33:	e8 0f 0d 00 00       	call   80105747 <acquire>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80104a38:	c7 45 f0 14 42 11 80 	movl   $0x80114214,-0x10(%ebp)
80104a3f:	eb 11                	jmp    80104a52 <allocproc+0x2c>
    if(p->state == UNUSED)
80104a41:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104a44:	8b 40 0c             	mov    0xc(%eax),%eax
80104a47:	85 c0                	test   %eax,%eax
80104a49:	74 27                	je     80104a72 <allocproc+0x4c>
{
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80104a4b:	81 45 f0 84 00 00 00 	addl   $0x84,-0x10(%ebp)
80104a52:	b8 14 63 11 80       	mov    $0x80116314,%eax
80104a57:	39 45 f0             	cmp    %eax,-0x10(%ebp)
80104a5a:	72 e5                	jb     80104a41 <allocproc+0x1b>
    if(p->state == UNUSED)
      goto found;
  release(&ptable.lock);
80104a5c:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80104a63:	e8 48 0d 00 00       	call   801057b0 <release>
  return 0;
80104a68:	b8 00 00 00 00       	mov    $0x0,%eax
80104a6d:	e9 b5 00 00 00       	jmp    80104b27 <allocproc+0x101>
  char *sp;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == UNUSED)
      goto found;
80104a72:	90                   	nop
  release(&ptable.lock);
  return 0;

found:
  p->state = EMBRYO;
80104a73:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104a76:	c7 40 0c 01 00 00 00 	movl   $0x1,0xc(%eax)
  p->pid = nextpid++;
80104a7d:	a1 04 c0 10 80       	mov    0x8010c004,%eax
80104a82:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104a85:	89 42 10             	mov    %eax,0x10(%edx)
80104a88:	83 c0 01             	add    $0x1,%eax
80104a8b:	a3 04 c0 10 80       	mov    %eax,0x8010c004
  release(&ptable.lock);
80104a90:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80104a97:	e8 14 0d 00 00       	call   801057b0 <release>

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
80104a9c:	e8 fd e8 ff ff       	call   8010339e <kalloc>
80104aa1:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104aa4:	89 42 08             	mov    %eax,0x8(%edx)
80104aa7:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104aaa:	8b 40 08             	mov    0x8(%eax),%eax
80104aad:	85 c0                	test   %eax,%eax
80104aaf:	75 11                	jne    80104ac2 <allocproc+0x9c>
    p->state = UNUSED;
80104ab1:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104ab4:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
    return 0;
80104abb:	b8 00 00 00 00       	mov    $0x0,%eax
80104ac0:	eb 65                	jmp    80104b27 <allocproc+0x101>
  }
  sp = p->kstack + KSTACKSIZE;
80104ac2:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104ac5:	8b 40 08             	mov    0x8(%eax),%eax
80104ac8:	05 00 10 00 00       	add    $0x1000,%eax
80104acd:	89 45 f4             	mov    %eax,-0xc(%ebp)

  // Leave room for trap frame.
  sp -= sizeof *p->tf;
80104ad0:	83 6d f4 4c          	subl   $0x4c,-0xc(%ebp)
  p->tf = (struct trapframe*)sp;
80104ad4:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104ad7:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104ada:	89 50 18             	mov    %edx,0x18(%eax)

  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 4;
80104add:	83 6d f4 04          	subl   $0x4,-0xc(%ebp)
  *(uint*)sp = (uint)trapret;
80104ae1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104ae4:	ba f8 6d 10 80       	mov    $0x80106df8,%edx
80104ae9:	89 10                	mov    %edx,(%eax)

  sp -= sizeof *p->context;
80104aeb:	83 6d f4 14          	subl   $0x14,-0xc(%ebp)
  p->context = (struct context*)sp;
80104aef:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104af2:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104af5:	89 50 1c             	mov    %edx,0x1c(%eax)
  memset(p->context, 0, sizeof *p->context);
80104af8:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104afb:	8b 40 1c             	mov    0x1c(%eax),%eax
80104afe:	c7 44 24 08 14 00 00 	movl   $0x14,0x8(%esp)
80104b05:	00 
80104b06:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80104b0d:	00 
80104b0e:	89 04 24             	mov    %eax,(%esp)
80104b11:	e8 94 0e 00 00       	call   801059aa <memset>
  p->context->eip = (uint)forkret;
80104b16:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104b19:	8b 40 1c             	mov    0x1c(%eax),%eax
80104b1c:	ba df 53 10 80       	mov    $0x801053df,%edx
80104b21:	89 50 10             	mov    %edx,0x10(%eax)
  return p;
80104b24:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
80104b27:	c9                   	leave  
80104b28:	c3                   	ret    

80104b29 <userinit>:

//PAGEBREAK: 32
// Set up first user process.
void
userinit(void)
{
80104b29:	55                   	push   %ebp
80104b2a:	89 e5                	mov    %esp,%ebp
80104b2c:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;
  extern char _binary_initcode_start[], _binary_initcode_size[];

  p = allocproc();
80104b2f:	e8 f2 fe ff ff       	call   80104a26 <allocproc>
80104b34:	89 45 f4             	mov    %eax,-0xc(%ebp)
  initproc = p;
80104b37:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b3a:	a3 90 c6 10 80       	mov    %eax,0x8010c690
  if((p->pgdir = setupkvm()) == 0)
80104b3f:	e8 95 3a 00 00       	call   801085d9 <setupkvm>
80104b44:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104b47:	89 42 04             	mov    %eax,0x4(%edx)
80104b4a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b4d:	8b 40 04             	mov    0x4(%eax),%eax
80104b50:	85 c0                	test   %eax,%eax
80104b52:	75 0c                	jne    80104b60 <userinit+0x37>
    panic("userinit: out of memory?");
80104b54:	c7 04 24 4c 93 10 80 	movl   $0x8010934c,(%esp)
80104b5b:	e8 09 bd ff ff       	call   80100869 <panic>
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
80104b60:	ba 2c 00 00 00       	mov    $0x2c,%edx
80104b65:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b68:	8b 40 04             	mov    0x4(%eax),%eax
80104b6b:	89 54 24 08          	mov    %edx,0x8(%esp)
80104b6f:	c7 44 24 04 20 c5 10 	movl   $0x8010c520,0x4(%esp)
80104b76:	80 
80104b77:	89 04 24             	mov    %eax,(%esp)
80104b7a:	e8 a1 3c 00 00       	call   80108820 <inituvm>
  p->sz = PGSIZE;
80104b7f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b82:	c7 00 00 10 00 00    	movl   $0x1000,(%eax)
  memset(p->tf, 0, sizeof(*p->tf));
80104b88:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b8b:	8b 40 18             	mov    0x18(%eax),%eax
80104b8e:	c7 44 24 08 4c 00 00 	movl   $0x4c,0x8(%esp)
80104b95:	00 
80104b96:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80104b9d:	00 
80104b9e:	89 04 24             	mov    %eax,(%esp)
80104ba1:	e8 04 0e 00 00       	call   801059aa <memset>
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
80104ba6:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104ba9:	8b 40 18             	mov    0x18(%eax),%eax
80104bac:	66 c7 40 3c 23 00    	movw   $0x23,0x3c(%eax)
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
80104bb2:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104bb5:	8b 40 18             	mov    0x18(%eax),%eax
80104bb8:	66 c7 40 2c 2b 00    	movw   $0x2b,0x2c(%eax)
  p->tf->es = p->tf->ds;
80104bbe:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104bc1:	8b 40 18             	mov    0x18(%eax),%eax
80104bc4:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104bc7:	8b 52 18             	mov    0x18(%edx),%edx
80104bca:	0f b7 52 2c          	movzwl 0x2c(%edx),%edx
80104bce:	66 89 50 28          	mov    %dx,0x28(%eax)
  p->tf->ss = p->tf->ds;
80104bd2:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104bd5:	8b 40 18             	mov    0x18(%eax),%eax
80104bd8:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104bdb:	8b 52 18             	mov    0x18(%edx),%edx
80104bde:	0f b7 52 2c          	movzwl 0x2c(%edx),%edx
80104be2:	66 89 50 48          	mov    %dx,0x48(%eax)
  p->tf->eflags = FL_IF;
80104be6:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104be9:	8b 40 18             	mov    0x18(%eax),%eax
80104bec:	c7 40 40 00 02 00 00 	movl   $0x200,0x40(%eax)
  p->tf->esp = PGSIZE;
80104bf3:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104bf6:	8b 40 18             	mov    0x18(%eax),%eax
80104bf9:	c7 40 44 00 10 00 00 	movl   $0x1000,0x44(%eax)
  p->tf->eip = 0;  // beginning of initcode.S
80104c00:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104c03:	8b 40 18             	mov    0x18(%eax),%eax
80104c06:	c7 40 38 00 00 00 00 	movl   $0x0,0x38(%eax)

  safestrcpy(p->name, "initcode", sizeof(p->name));
80104c0d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104c10:	83 c0 6c             	add    $0x6c,%eax
80104c13:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
80104c1a:	00 
80104c1b:	c7 44 24 04 65 93 10 	movl   $0x80109365,0x4(%esp)
80104c22:	80 
80104c23:	89 04 24             	mov    %eax,(%esp)
80104c26:	e8 b2 0f 00 00       	call   80105bdd <safestrcpy>
  p->cwd = namei("/");
80104c2b:	c7 04 24 6e 93 10 80 	movl   $0x8010936e,(%esp)
80104c32:	e8 e8 df ff ff       	call   80102c1f <namei>
80104c37:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104c3a:	89 42 68             	mov    %eax,0x68(%edx)

  p->state = RUNNABLE;
80104c3d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104c40:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
}
80104c47:	c9                   	leave  
80104c48:	c3                   	ret    

80104c49 <growproc>:

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n)
{
80104c49:	55                   	push   %ebp
80104c4a:	89 e5                	mov    %esp,%ebp
80104c4c:	83 ec 28             	sub    $0x28,%esp
  uint sz;

  sz = proc->sz;
80104c4f:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104c55:	8b 00                	mov    (%eax),%eax
80104c57:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(n > 0){
80104c5a:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80104c5e:	7e 34                	jle    80104c94 <growproc+0x4b>
    if((sz = allocuvm(proc->pgdir, sz, sz + n)) == 0)
80104c60:	8b 45 08             	mov    0x8(%ebp),%eax
80104c63:	89 c2                	mov    %eax,%edx
80104c65:	03 55 f4             	add    -0xc(%ebp),%edx
80104c68:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104c6e:	8b 40 04             	mov    0x4(%eax),%eax
80104c71:	89 54 24 08          	mov    %edx,0x8(%esp)
80104c75:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104c78:	89 54 24 04          	mov    %edx,0x4(%esp)
80104c7c:	89 04 24             	mov    %eax,(%esp)
80104c7f:	e8 2e 3d 00 00       	call   801089b2 <allocuvm>
80104c84:	89 45 f4             	mov    %eax,-0xc(%ebp)
80104c87:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104c8b:	75 41                	jne    80104cce <growproc+0x85>
      return -1;
80104c8d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104c92:	eb 58                	jmp    80104cec <growproc+0xa3>
  } else if(n < 0){
80104c94:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80104c98:	79 34                	jns    80104cce <growproc+0x85>
    if((sz = deallocuvm(proc->pgdir, sz, sz + n)) == 0)
80104c9a:	8b 45 08             	mov    0x8(%ebp),%eax
80104c9d:	89 c2                	mov    %eax,%edx
80104c9f:	03 55 f4             	add    -0xc(%ebp),%edx
80104ca2:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104ca8:	8b 40 04             	mov    0x4(%eax),%eax
80104cab:	89 54 24 08          	mov    %edx,0x8(%esp)
80104caf:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104cb2:	89 54 24 04          	mov    %edx,0x4(%esp)
80104cb6:	89 04 24             	mov    %eax,(%esp)
80104cb9:	e8 dc 3d 00 00       	call   80108a9a <deallocuvm>
80104cbe:	89 45 f4             	mov    %eax,-0xc(%ebp)
80104cc1:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104cc5:	75 07                	jne    80104cce <growproc+0x85>
      return -1;
80104cc7:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104ccc:	eb 1e                	jmp    80104cec <growproc+0xa3>
  }
  proc->sz = sz;
80104cce:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104cd4:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104cd7:	89 10                	mov    %edx,(%eax)
  switchuvm(proc);
80104cd9:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104cdf:	89 04 24             	mov    %eax,(%esp)
80104ce2:	e8 d5 39 00 00       	call   801086bc <switchuvm>
  return 0;
80104ce7:	b8 00 00 00 00       	mov    $0x0,%eax
}
80104cec:	c9                   	leave  
80104ced:	c3                   	ret    

80104cee <fork>:
// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int
fork(void)
{
80104cee:	55                   	push   %ebp
80104cef:	89 e5                	mov    %esp,%ebp
80104cf1:	57                   	push   %edi
80104cf2:	56                   	push   %esi
80104cf3:	53                   	push   %ebx
80104cf4:	83 ec 2c             	sub    $0x2c,%esp
  int i, pid;
  struct proc *np;

  // Allocate process.
  if((np = allocproc()) == 0)
80104cf7:	e8 2a fd ff ff       	call   80104a26 <allocproc>
80104cfc:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80104cff:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
80104d03:	75 0a                	jne    80104d0f <fork+0x21>
    return -1;
80104d05:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104d0a:	e9 6c 01 00 00       	jmp    80104e7b <fork+0x18d>

  // Copy process state from p.
  if((np->pgdir = copyuvm(proc->pgdir, proc->sz)) == 0){
80104d0f:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104d15:	8b 10                	mov    (%eax),%edx
80104d17:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104d1d:	8b 40 04             	mov    0x4(%eax),%eax
80104d20:	89 54 24 04          	mov    %edx,0x4(%esp)
80104d24:	89 04 24             	mov    %eax,(%esp)
80104d27:	e8 f5 3e 00 00       	call   80108c21 <copyuvm>
80104d2c:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80104d2f:	89 42 04             	mov    %eax,0x4(%edx)
80104d32:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d35:	8b 40 04             	mov    0x4(%eax),%eax
80104d38:	85 c0                	test   %eax,%eax
80104d3a:	75 2c                	jne    80104d68 <fork+0x7a>
    kfree(np->kstack);
80104d3c:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d3f:	8b 40 08             	mov    0x8(%eax),%eax
80104d42:	89 04 24             	mov    %eax,(%esp)
80104d45:	e8 be e5 ff ff       	call   80103308 <kfree>
    np->kstack = 0;
80104d4a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d4d:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
    np->state = UNUSED;
80104d54:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d57:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
    return -1;
80104d5e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104d63:	e9 13 01 00 00       	jmp    80104e7b <fork+0x18d>
  }
  np->sz = proc->sz;
80104d68:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104d6e:	8b 10                	mov    (%eax),%edx
80104d70:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d73:	89 10                	mov    %edx,(%eax)
  np->parent = proc;
80104d75:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
80104d7c:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d7f:	89 50 14             	mov    %edx,0x14(%eax)
  *np->tf = *proc->tf;
80104d82:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d85:	8b 50 18             	mov    0x18(%eax),%edx
80104d88:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104d8e:	8b 40 18             	mov    0x18(%eax),%eax
80104d91:	89 c3                	mov    %eax,%ebx
80104d93:	b8 13 00 00 00       	mov    $0x13,%eax
80104d98:	89 d7                	mov    %edx,%edi
80104d9a:	89 de                	mov    %ebx,%esi
80104d9c:	89 c1                	mov    %eax,%ecx
80104d9e:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;
80104da0:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104da3:	8b 40 18             	mov    0x18(%eax),%eax
80104da6:	c7 40 1c 00 00 00 00 	movl   $0x0,0x1c(%eax)

  for(i = 0; i < NOFILE; i++)
80104dad:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
80104db4:	eb 3d                	jmp    80104df3 <fork+0x105>
    if(proc->ofile[i])
80104db6:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104dbc:	8b 55 dc             	mov    -0x24(%ebp),%edx
80104dbf:	83 c2 08             	add    $0x8,%edx
80104dc2:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80104dc6:	85 c0                	test   %eax,%eax
80104dc8:	74 25                	je     80104def <fork+0x101>
      np->ofile[i] = filedup(proc->ofile[i]);
80104dca:	8b 5d dc             	mov    -0x24(%ebp),%ebx
80104dcd:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104dd3:	8b 55 dc             	mov    -0x24(%ebp),%edx
80104dd6:	83 c2 08             	add    $0x8,%edx
80104dd9:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80104ddd:	89 04 24             	mov    %eax,(%esp)
80104de0:	e8 34 c9 ff ff       	call   80101719 <filedup>
80104de5:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80104de8:	8d 4b 08             	lea    0x8(%ebx),%ecx
80104deb:	89 44 8a 08          	mov    %eax,0x8(%edx,%ecx,4)
  *np->tf = *proc->tf;

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;

  for(i = 0; i < NOFILE; i++)
80104def:	83 45 dc 01          	addl   $0x1,-0x24(%ebp)
80104df3:	83 7d dc 0f          	cmpl   $0xf,-0x24(%ebp)
80104df7:	7e bd                	jle    80104db6 <fork+0xc8>
    if(proc->ofile[i])
      np->ofile[i] = filedup(proc->ofile[i]);
  np->cwd = idup(proc->cwd);
80104df9:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104dff:	8b 40 68             	mov    0x68(%eax),%eax
80104e02:	89 04 24             	mov    %eax,(%esp)
80104e05:	e8 35 d2 ff ff       	call   8010203f <idup>
80104e0a:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80104e0d:	89 42 68             	mov    %eax,0x68(%edx)

  safestrcpy(np->name, proc->name, sizeof(proc->name));
80104e10:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104e16:	8d 50 6c             	lea    0x6c(%eax),%edx
80104e19:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104e1c:	83 c0 6c             	add    $0x6c,%eax
80104e1f:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
80104e26:	00 
80104e27:	89 54 24 04          	mov    %edx,0x4(%esp)
80104e2b:	89 04 24             	mov    %eax,(%esp)
80104e2e:	e8 aa 0d 00 00       	call   80105bdd <safestrcpy>

  pid = np->pid;
80104e33:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104e36:	8b 40 10             	mov    0x10(%eax),%eax
80104e39:	89 45 e0             	mov    %eax,-0x20(%ebp)

  // lock to force the compiler to emit the np->state write last.
  acquire(&ptable.lock);
80104e3c:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80104e43:	e8 ff 08 00 00       	call   80105747 <acquire>
  np->state = RUNNABLE;
80104e48:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104e4b:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  //cprintf("pid %d tickstart %d\n", np->pid, ticks);
  np->tickstart = ticks;
80104e52:	a1 74 6b 11 80       	mov    0x80116b74,%eax
80104e57:	89 c2                	mov    %eax,%edx
80104e59:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104e5c:	89 50 7c             	mov    %edx,0x7c(%eax)
  np->tickfirstrun = 0;
80104e5f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104e62:	c7 80 80 00 00 00 00 	movl   $0x0,0x80(%eax)
80104e69:	00 00 00 
  release(&ptable.lock);
80104e6c:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80104e73:	e8 38 09 00 00       	call   801057b0 <release>

  return pid;
80104e78:	8b 45 e0             	mov    -0x20(%ebp),%eax
}
80104e7b:	83 c4 2c             	add    $0x2c,%esp
80104e7e:	5b                   	pop    %ebx
80104e7f:	5e                   	pop    %esi
80104e80:	5f                   	pop    %edi
80104e81:	5d                   	pop    %ebp
80104e82:	c3                   	ret    

80104e83 <exit>:
// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
void
exit(void)
{
80104e83:	55                   	push   %ebp
80104e84:	89 e5                	mov    %esp,%ebp
80104e86:	53                   	push   %ebx
80104e87:	83 ec 24             	sub    $0x24,%esp
  struct proc *p;
  int fd;

  if(proc == initproc)
80104e8a:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
80104e91:	a1 90 c6 10 80       	mov    0x8010c690,%eax
80104e96:	39 c2                	cmp    %eax,%edx
80104e98:	75 0c                	jne    80104ea6 <exit+0x23>
    panic("init exiting");
80104e9a:	c7 04 24 70 93 10 80 	movl   $0x80109370,(%esp)
80104ea1:	e8 c3 b9 ff ff       	call   80100869 <panic>

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
80104ea6:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80104ead:	eb 44                	jmp    80104ef3 <exit+0x70>
    if(proc->ofile[fd]){
80104eaf:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104eb5:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104eb8:	83 c2 08             	add    $0x8,%edx
80104ebb:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80104ebf:	85 c0                	test   %eax,%eax
80104ec1:	74 2c                	je     80104eef <exit+0x6c>
      fileclose(proc->ofile[fd]);
80104ec3:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104ec9:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104ecc:	83 c2 08             	add    $0x8,%edx
80104ecf:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80104ed3:	89 04 24             	mov    %eax,(%esp)
80104ed6:	e8 86 c8 ff ff       	call   80101761 <fileclose>
      proc->ofile[fd] = 0;
80104edb:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104ee1:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104ee4:	83 c2 08             	add    $0x8,%edx
80104ee7:	c7 44 90 08 00 00 00 	movl   $0x0,0x8(%eax,%edx,4)
80104eee:	00 

  if(proc == initproc)
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
80104eef:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80104ef3:	83 7d f4 0f          	cmpl   $0xf,-0xc(%ebp)
80104ef7:	7e b6                	jle    80104eaf <exit+0x2c>
      fileclose(proc->ofile[fd]);
      proc->ofile[fd] = 0;
    }
  }

  begin_op();
80104ef9:	e8 6c eb ff ff       	call   80103a6a <begin_op>
  iput(proc->cwd);
80104efe:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104f04:	8b 40 68             	mov    0x68(%eax),%eax
80104f07:	89 04 24             	mov    %eax,(%esp)
80104f0a:	e8 1e d3 ff ff       	call   8010222d <iput>
  end_op();
80104f0f:	e8 d8 eb ff ff       	call   80103aec <end_op>
  proc->cwd = 0;
80104f14:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104f1a:	c7 40 68 00 00 00 00 	movl   $0x0,0x68(%eax)

  acquire(&ptable.lock);
80104f21:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80104f28:	e8 1a 08 00 00       	call   80105747 <acquire>

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);
80104f2d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104f33:	8b 40 14             	mov    0x14(%eax),%eax
80104f36:	89 04 24             	mov    %eax,(%esp)
80104f39:	e8 7b 05 00 00       	call   801054b9 <wakeup1>

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104f3e:	c7 45 f0 14 42 11 80 	movl   $0x80114214,-0x10(%ebp)
80104f45:	eb 3b                	jmp    80104f82 <exit+0xff>
    if(p->parent == proc){
80104f47:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104f4a:	8b 50 14             	mov    0x14(%eax),%edx
80104f4d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104f53:	39 c2                	cmp    %eax,%edx
80104f55:	75 24                	jne    80104f7b <exit+0xf8>
      p->parent = initproc;
80104f57:	8b 15 90 c6 10 80    	mov    0x8010c690,%edx
80104f5d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104f60:	89 50 14             	mov    %edx,0x14(%eax)
      if(p->state == ZOMBIE)
80104f63:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104f66:	8b 40 0c             	mov    0xc(%eax),%eax
80104f69:	83 f8 05             	cmp    $0x5,%eax
80104f6c:	75 0d                	jne    80104f7b <exit+0xf8>
        wakeup1(initproc);
80104f6e:	a1 90 c6 10 80       	mov    0x8010c690,%eax
80104f73:	89 04 24             	mov    %eax,(%esp)
80104f76:	e8 3e 05 00 00       	call   801054b9 <wakeup1>

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104f7b:	81 45 f0 84 00 00 00 	addl   $0x84,-0x10(%ebp)
80104f82:	b8 14 63 11 80       	mov    $0x80116314,%eax
80104f87:	39 45 f0             	cmp    %eax,-0x10(%ebp)
80104f8a:	72 bb                	jb     80104f47 <exit+0xc4>
        wakeup1(initproc);
    }
  }

  // Jump into the scheduler, never to return.
  proc->state = ZOMBIE;
80104f8c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104f92:	c7 40 0c 05 00 00 00 	movl   $0x5,0xc(%eax)
  if(capturing && proc->tickfirstrun > 0) { // may still be 0 if not capturing when proc started
80104f99:	a1 a0 c6 10 80       	mov    0x8010c6a0,%eax
80104f9e:	85 c0                	test   %eax,%eax
80104fa0:	74 6a                	je     8010500c <exit+0x189>
80104fa2:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104fa8:	8b 80 80 00 00 00    	mov    0x80(%eax),%eax
80104fae:	85 c0                	test   %eax,%eax
80104fb0:	7e 5a                	jle    8010500c <exit+0x189>
      cs.proc_exit_count++;
80104fb2:	a1 24 6b 11 80       	mov    0x80116b24,%eax
80104fb7:	83 c0 01             	add    $0x1,%eax
80104fba:	a3 24 6b 11 80       	mov    %eax,0x80116b24
      cs.proc_tick_count += (ticks - proc->tickstart);
80104fbf:	a1 28 6b 11 80       	mov    0x80116b28,%eax
80104fc4:	89 c2                	mov    %eax,%edx
80104fc6:	8b 0d 74 6b 11 80    	mov    0x80116b74,%ecx
80104fcc:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104fd2:	8b 40 7c             	mov    0x7c(%eax),%eax
80104fd5:	89 cb                	mov    %ecx,%ebx
80104fd7:	29 c3                	sub    %eax,%ebx
80104fd9:	89 d8                	mov    %ebx,%eax
80104fdb:	8d 04 02             	lea    (%edx,%eax,1),%eax
80104fde:	a3 28 6b 11 80       	mov    %eax,0x80116b28
      cs.proc_firstrun_count += (proc->tickfirstrun - proc->tickstart);
80104fe3:	8b 15 2c 6b 11 80    	mov    0x80116b2c,%edx
80104fe9:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104fef:	8b 88 80 00 00 00    	mov    0x80(%eax),%ecx
80104ff5:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104ffb:	8b 40 7c             	mov    0x7c(%eax),%eax
80104ffe:	89 cb                	mov    %ecx,%ebx
80105000:	29 c3                	sub    %eax,%ebx
80105002:	89 d8                	mov    %ebx,%eax
80105004:	8d 04 02             	lea    (%edx,%eax,1),%eax
80105007:	a3 2c 6b 11 80       	mov    %eax,0x80116b2c
  }
  sched();
8010500c:	e8 ea 02 00 00       	call   801052fb <sched>
  panic("zombie exit");
80105011:	c7 04 24 7d 93 10 80 	movl   $0x8010937d,(%esp)
80105018:	e8 4c b8 ff ff       	call   80100869 <panic>

8010501d <wait>:

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
int
wait(void)
{
8010501d:	55                   	push   %ebp
8010501e:	89 e5                	mov    %esp,%ebp
80105020:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;
  int havekids, pid;

  acquire(&ptable.lock);
80105023:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
8010502a:	e8 18 07 00 00       	call   80105747 <acquire>
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
8010502f:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80105036:	c7 45 ec 14 42 11 80 	movl   $0x80114214,-0x14(%ebp)
8010503d:	e9 9d 00 00 00       	jmp    801050df <wait+0xc2>
      if(p->parent != proc)
80105042:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105045:	8b 50 14             	mov    0x14(%eax),%edx
80105048:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010504e:	39 c2                	cmp    %eax,%edx
80105050:	0f 85 81 00 00 00    	jne    801050d7 <wait+0xba>
        continue;
      havekids = 1;
80105056:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
      if(p->state == ZOMBIE){
8010505d:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105060:	8b 40 0c             	mov    0xc(%eax),%eax
80105063:	83 f8 05             	cmp    $0x5,%eax
80105066:	75 70                	jne    801050d8 <wait+0xbb>
        // Found one.
        pid = p->pid;
80105068:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010506b:	8b 40 10             	mov    0x10(%eax),%eax
8010506e:	89 45 f4             	mov    %eax,-0xc(%ebp)
        kfree(p->kstack);
80105071:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105074:	8b 40 08             	mov    0x8(%eax),%eax
80105077:	89 04 24             	mov    %eax,(%esp)
8010507a:	e8 89 e2 ff ff       	call   80103308 <kfree>
        p->kstack = 0;
8010507f:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105082:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
        freevm(p->pgdir);
80105089:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010508c:	8b 40 04             	mov    0x4(%eax),%eax
8010508f:	89 04 24             	mov    %eax,(%esp)
80105092:	e8 b9 3a 00 00       	call   80108b50 <freevm>
        p->state = UNUSED;
80105097:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010509a:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
        p->pid = 0;
801050a1:	8b 45 ec             	mov    -0x14(%ebp),%eax
801050a4:	c7 40 10 00 00 00 00 	movl   $0x0,0x10(%eax)
        p->parent = 0;
801050ab:	8b 45 ec             	mov    -0x14(%ebp),%eax
801050ae:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)
        p->name[0] = 0;
801050b5:	8b 45 ec             	mov    -0x14(%ebp),%eax
801050b8:	c6 40 6c 00          	movb   $0x0,0x6c(%eax)
        p->killed = 0;
801050bc:	8b 45 ec             	mov    -0x14(%ebp),%eax
801050bf:	c7 40 24 00 00 00 00 	movl   $0x0,0x24(%eax)
        release(&ptable.lock);
801050c6:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
801050cd:	e8 de 06 00 00       	call   801057b0 <release>
        return pid;
801050d2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801050d5:	eb 57                	jmp    8010512e <wait+0x111>
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->parent != proc)
        continue;
801050d7:	90                   	nop

  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801050d8:	81 45 ec 84 00 00 00 	addl   $0x84,-0x14(%ebp)
801050df:	b8 14 63 11 80       	mov    $0x80116314,%eax
801050e4:	39 45 ec             	cmp    %eax,-0x14(%ebp)
801050e7:	0f 82 55 ff ff ff    	jb     80105042 <wait+0x25>
        return pid;
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || proc->killed){
801050ed:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801050f1:	74 0d                	je     80105100 <wait+0xe3>
801050f3:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801050f9:	8b 40 24             	mov    0x24(%eax),%eax
801050fc:	85 c0                	test   %eax,%eax
801050fe:	74 13                	je     80105113 <wait+0xf6>
      release(&ptable.lock);
80105100:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80105107:	e8 a4 06 00 00       	call   801057b0 <release>
      return -1;
8010510c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105111:	eb 1b                	jmp    8010512e <wait+0x111>
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(proc, &ptable.lock);  //DOC: wait-sleep
80105113:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105119:	c7 44 24 04 e0 41 11 	movl   $0x801141e0,0x4(%esp)
80105120:	80 
80105121:	89 04 24             	mov    %eax,(%esp)
80105124:	e8 f5 02 00 00       	call   8010541e <sleep>
  }
80105129:	e9 01 ff ff ff       	jmp    8010502f <wait+0x12>
}
8010512e:	c9                   	leave  
8010512f:	c3                   	ret    

80105130 <switch_scheduler>:
//  - eventually that process transfers control
//      via swtch back to the scheduler

int sched_type = SCHED_ROUND_ROBIN;

void switch_scheduler(int new_sched_type) {
80105130:	55                   	push   %ebp
80105131:	89 e5                	mov    %esp,%ebp
    // check for a valid scheduler type
    if(new_sched_type >= SCHED_ROUND_ROBIN && new_sched_type <= SCHED_MLFQ) {
80105133:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80105137:	78 0e                	js     80105147 <switch_scheduler+0x17>
80105139:	83 7d 08 03          	cmpl   $0x3,0x8(%ebp)
8010513d:	7f 08                	jg     80105147 <switch_scheduler+0x17>
        sched_type = new_sched_type;
8010513f:	8b 45 08             	mov    0x8(%ebp),%eax
80105142:	a3 88 c6 10 80       	mov    %eax,0x8010c688
    }
}
80105147:	5d                   	pop    %ebp
80105148:	c3                   	ret    

80105149 <sys_switch_scheduler>:

int sys_switch_scheduler(void)
{
80105149:	55                   	push   %ebp
8010514a:	89 e5                	mov    %esp,%ebp
8010514c:	83 ec 28             	sub    $0x28,%esp
    int new_sched_type;
    if(argint(0, &new_sched_type) < 0)
8010514f:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105152:	89 44 24 04          	mov    %eax,0x4(%esp)
80105156:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
8010515d:	e8 a4 0b 00 00       	call   80105d06 <argint>
80105162:	85 c0                	test   %eax,%eax
80105164:	79 07                	jns    8010516d <sys_switch_scheduler+0x24>
      return -1;
80105166:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010516b:	eb 10                	jmp    8010517d <sys_switch_scheduler+0x34>
    switch_scheduler(new_sched_type);
8010516d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105170:	89 04 24             	mov    %eax,(%esp)
80105173:	e8 b8 ff ff ff       	call   80105130 <switch_scheduler>
    return 0;
80105178:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010517d:	c9                   	leave  
8010517e:	c3                   	ret    

8010517f <scheduler>:

int lastprocidx = 0;
void
scheduler(void)
{
8010517f:	55                   	push   %ebp
80105180:	89 e5                	mov    %esp,%ebp
80105182:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;
  int idx;

  for(;;){
    // Enable interrupts on this processor.
    sti();
80105185:	e8 7a f8 ff ff       	call   80104a04 <sti>

    acquire(&ptable.lock);
8010518a:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80105191:	e8 b1 05 00 00       	call   80105747 <acquire>
    p = 0;
80105196:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
    if(sched_type == SCHED_ROUND_ROBIN) {
8010519d:	a1 88 c6 10 80       	mov    0x8010c688,%eax
801051a2:	85 c0                	test   %eax,%eax
801051a4:	75 7e                	jne    80105224 <scheduler+0xa5>
        // round-robin is reimplemented because I changed the nesting of the
        // outer infinite for() and the if's inside, to support different
        // schedulers
        idx = (lastprocidx + 1) % NPROC;
801051a6:	a1 8c c6 10 80       	mov    0x8010c68c,%eax
801051ab:	8d 50 01             	lea    0x1(%eax),%edx
801051ae:	89 d0                	mov    %edx,%eax
801051b0:	c1 f8 1f             	sar    $0x1f,%eax
801051b3:	c1 e8 1a             	shr    $0x1a,%eax
801051b6:	01 c2                	add    %eax,%edx
801051b8:	83 e2 3f             	and    $0x3f,%edx
801051bb:	89 d1                	mov    %edx,%ecx
801051bd:	29 c1                	sub    %eax,%ecx
801051bf:	89 c8                	mov    %ecx,%eax
801051c1:	89 45 f4             	mov    %eax,-0xc(%ebp)
801051c4:	eb 01                	jmp    801051c7 <scheduler+0x48>
            idx = (idx + 1) % NPROC;
            if(idx == (lastprocidx+1)) { // ran out of procs to check, nothing runnable
                p = 0;
                break;
            }
        }
801051c6:	90                   	nop
        // round-robin is reimplemented because I changed the nesting of the
        // outer infinite for() and the if's inside, to support different
        // schedulers
        idx = (lastprocidx + 1) % NPROC;
        while(1) {
            p = &ptable.proc[idx];
801051c7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801051ca:	c1 e0 02             	shl    $0x2,%eax
801051cd:	89 c2                	mov    %eax,%edx
801051cf:	c1 e2 05             	shl    $0x5,%edx
801051d2:	01 d0                	add    %edx,%eax
801051d4:	05 14 42 11 80       	add    $0x80114214,%eax
801051d9:	89 45 f0             	mov    %eax,-0x10(%ebp)
            if(p->state == RUNNABLE) {
801051dc:	8b 45 f0             	mov    -0x10(%ebp),%eax
801051df:	8b 40 0c             	mov    0xc(%eax),%eax
801051e2:	83 f8 03             	cmp    $0x3,%eax
801051e5:	75 0a                	jne    801051f1 <scheduler+0x72>
                lastprocidx = idx;
801051e7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801051ea:	a3 8c c6 10 80       	mov    %eax,0x8010c68c
                break;
801051ef:	eb 31                	jmp    80105222 <scheduler+0xa3>
            }
            idx = (idx + 1) % NPROC;
801051f1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801051f4:	8d 50 01             	lea    0x1(%eax),%edx
801051f7:	89 d0                	mov    %edx,%eax
801051f9:	c1 f8 1f             	sar    $0x1f,%eax
801051fc:	c1 e8 1a             	shr    $0x1a,%eax
801051ff:	01 c2                	add    %eax,%edx
80105201:	83 e2 3f             	and    $0x3f,%edx
80105204:	89 d1                	mov    %edx,%ecx
80105206:	29 c1                	sub    %eax,%ecx
80105208:	89 c8                	mov    %ecx,%eax
8010520a:	89 45 f4             	mov    %eax,-0xc(%ebp)
            if(idx == (lastprocidx+1)) { // ran out of procs to check, nothing runnable
8010520d:	a1 8c c6 10 80       	mov    0x8010c68c,%eax
80105212:	83 c0 01             	add    $0x1,%eax
80105215:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80105218:	75 ac                	jne    801051c6 <scheduler+0x47>
                p = 0;
8010521a:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
                break;
80105221:	90                   	nop
80105222:	eb 12                	jmp    80105236 <scheduler+0xb7>
            }
        }
    }
    else if(sched_type == SCHED_FIFO) {
80105224:	a1 88 c6 10 80       	mov    0x8010c688,%eax
80105229:	83 f8 01             	cmp    $0x1,%eax
8010522c:	74 08                	je     80105236 <scheduler+0xb7>
        // TODO
    }
    else if(sched_type == SCHED_LIFO) {
8010522e:	a1 88 c6 10 80       	mov    0x8010c688,%eax
80105233:	83 f8 02             	cmp    $0x2,%eax
        //
        // TODO
    }

    // if schedulers are broken and p == 0, try again to find a proc
    if(p == 0) {
80105236:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010523a:	75 28                	jne    80105264 <scheduler+0xe5>
        for(p = ptable.proc; p < &ptable.proc[NPROC]; p++) {
8010523c:	c7 45 f0 14 42 11 80 	movl   $0x80114214,-0x10(%ebp)
80105243:	eb 12                	jmp    80105257 <scheduler+0xd8>
            if(p->state == RUNNABLE) break;
80105245:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105248:	8b 40 0c             	mov    0xc(%eax),%eax
8010524b:	83 f8 03             	cmp    $0x3,%eax
8010524e:	74 13                	je     80105263 <scheduler+0xe4>
        // TODO
    }

    // if schedulers are broken and p == 0, try again to find a proc
    if(p == 0) {
        for(p = ptable.proc; p < &ptable.proc[NPROC]; p++) {
80105250:	81 45 f0 84 00 00 00 	addl   $0x84,-0x10(%ebp)
80105257:	b8 14 63 11 80       	mov    $0x80116314,%eax
8010525c:	39 45 f0             	cmp    %eax,-0x10(%ebp)
8010525f:	72 e4                	jb     80105245 <scheduler+0xc6>
80105261:	eb 01                	jmp    80105264 <scheduler+0xe5>
            if(p->state == RUNNABLE) break;
80105263:	90                   	nop
        }
    }

    // Now, p should be set to the process that we will switch to;
    // or, if no such process was found, p will be bogus and ignored
    if(p >= ptable.proc && p < &ptable.proc[NPROC]) {
80105264:	81 7d f0 14 42 11 80 	cmpl   $0x80114214,-0x10(%ebp)
8010526b:	72 7d                	jb     801052ea <scheduler+0x16b>
8010526d:	b8 14 63 11 80       	mov    $0x80116314,%eax
80105272:	39 45 f0             	cmp    %eax,-0x10(%ebp)
80105275:	73 73                	jae    801052ea <scheduler+0x16b>
        // Switch to chosen process.  It is the process's job
        // to release ptable.lock and then reacquire it
        // before jumping back to us.
        proc = p;
80105277:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010527a:	65 a3 04 00 00 00    	mov    %eax,%gs:0x4
        //cprintf("switching to %d\n", proc->pid);
        switchuvm(p);
80105280:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105283:	89 04 24             	mov    %eax,(%esp)
80105286:	e8 31 34 00 00       	call   801086bc <switchuvm>
        p->state = RUNNING;
8010528b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010528e:	c7 40 0c 04 00 00 00 	movl   $0x4,0xc(%eax)
        if(capturing && p->tickfirstrun == 0)
80105295:	a1 a0 c6 10 80       	mov    0x8010c6a0,%eax
8010529a:	85 c0                	test   %eax,%eax
8010529c:	74 1d                	je     801052bb <scheduler+0x13c>
8010529e:	8b 45 f0             	mov    -0x10(%ebp),%eax
801052a1:	8b 80 80 00 00 00    	mov    0x80(%eax),%eax
801052a7:	85 c0                	test   %eax,%eax
801052a9:	75 10                	jne    801052bb <scheduler+0x13c>
            p->tickfirstrun = ticks;
801052ab:	a1 74 6b 11 80       	mov    0x80116b74,%eax
801052b0:	89 c2                	mov    %eax,%edx
801052b2:	8b 45 f0             	mov    -0x10(%ebp),%eax
801052b5:	89 90 80 00 00 00    	mov    %edx,0x80(%eax)
        swtch(&cpu->scheduler, proc->context);
801052bb:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801052c1:	8b 40 1c             	mov    0x1c(%eax),%eax
801052c4:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
801052cb:	83 c2 04             	add    $0x4,%edx
801052ce:	89 44 24 04          	mov    %eax,0x4(%esp)
801052d2:	89 14 24             	mov    %edx,(%esp)
801052d5:	e8 76 09 00 00       	call   80105c50 <swtch>
        switchkvm();
801052da:	e8 c3 33 00 00       	call   801086a2 <switchkvm>

        // Process is done running for now.
        // It should have changed its p->state before coming back.
        proc = 0;
801052df:	65 c7 05 04 00 00 00 	movl   $0x0,%gs:0x4
801052e6:	00 00 00 00 
    }

    release(&ptable.lock);
801052ea:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
801052f1:	e8 ba 04 00 00       	call   801057b0 <release>

  }
801052f6:	e9 8a fe ff ff       	jmp    80105185 <scheduler+0x6>

801052fb <sched>:

// Enter scheduler.  Must hold only ptable.lock
// and have changed proc->state.
void
sched(void)
{
801052fb:	55                   	push   %ebp
801052fc:	89 e5                	mov    %esp,%ebp
801052fe:	83 ec 28             	sub    $0x28,%esp
  int intena;

  if(!holding(&ptable.lock))
80105301:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80105308:	e8 6e 05 00 00       	call   8010587b <holding>
8010530d:	85 c0                	test   %eax,%eax
8010530f:	75 0c                	jne    8010531d <sched+0x22>
    panic("sched ptable.lock");
80105311:	c7 04 24 89 93 10 80 	movl   $0x80109389,(%esp)
80105318:	e8 4c b5 ff ff       	call   80100869 <panic>
  if(cpu->ncli != 1)
8010531d:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105323:	8b 80 ac 00 00 00    	mov    0xac(%eax),%eax
80105329:	83 f8 01             	cmp    $0x1,%eax
8010532c:	74 0c                	je     8010533a <sched+0x3f>
    panic("sched locks");
8010532e:	c7 04 24 9b 93 10 80 	movl   $0x8010939b,(%esp)
80105335:	e8 2f b5 ff ff       	call   80100869 <panic>
  if(proc->state == RUNNING)
8010533a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105340:	8b 40 0c             	mov    0xc(%eax),%eax
80105343:	83 f8 04             	cmp    $0x4,%eax
80105346:	75 0c                	jne    80105354 <sched+0x59>
    panic("sched running");
80105348:	c7 04 24 a7 93 10 80 	movl   $0x801093a7,(%esp)
8010534f:	e8 15 b5 ff ff       	call   80100869 <panic>
  if(readeflags()&FL_IF)
80105354:	e8 9b f6 ff ff       	call   801049f4 <readeflags>
80105359:	25 00 02 00 00       	and    $0x200,%eax
8010535e:	85 c0                	test   %eax,%eax
80105360:	74 0c                	je     8010536e <sched+0x73>
    panic("sched interruptible");
80105362:	c7 04 24 b5 93 10 80 	movl   $0x801093b5,(%esp)
80105369:	e8 fb b4 ff ff       	call   80100869 <panic>
  intena = cpu->intena;
8010536e:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105374:	8b 80 b0 00 00 00    	mov    0xb0(%eax),%eax
8010537a:	89 45 f4             	mov    %eax,-0xc(%ebp)
  swtch(&proc->context, cpu->scheduler);
8010537d:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105383:	8b 40 04             	mov    0x4(%eax),%eax
80105386:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
8010538d:	83 c2 1c             	add    $0x1c,%edx
80105390:	89 44 24 04          	mov    %eax,0x4(%esp)
80105394:	89 14 24             	mov    %edx,(%esp)
80105397:	e8 b4 08 00 00       	call   80105c50 <swtch>
  cpu->intena = intena;
8010539c:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801053a2:	8b 55 f4             	mov    -0xc(%ebp),%edx
801053a5:	89 90 b0 00 00 00    	mov    %edx,0xb0(%eax)
}
801053ab:	c9                   	leave  
801053ac:	c3                   	ret    

801053ad <yield>:

// Give up the CPU for one scheduling round.
void
yield(void)
{
801053ad:	55                   	push   %ebp
801053ae:	89 e5                	mov    %esp,%ebp
801053b0:	83 ec 18             	sub    $0x18,%esp
  acquire(&ptable.lock);  //DOC: yieldlock
801053b3:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
801053ba:	e8 88 03 00 00       	call   80105747 <acquire>
  proc->state = RUNNABLE;
801053bf:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801053c5:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  sched();
801053cc:	e8 2a ff ff ff       	call   801052fb <sched>
  release(&ptable.lock);
801053d1:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
801053d8:	e8 d3 03 00 00       	call   801057b0 <release>
}
801053dd:	c9                   	leave  
801053de:	c3                   	ret    

801053df <forkret>:

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
801053df:	55                   	push   %ebp
801053e0:	89 e5                	mov    %esp,%ebp
801053e2:	83 ec 18             	sub    $0x18,%esp
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);
801053e5:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
801053ec:	e8 bf 03 00 00       	call   801057b0 <release>

  if (first) {
801053f1:	a1 20 c0 10 80       	mov    0x8010c020,%eax
801053f6:	85 c0                	test   %eax,%eax
801053f8:	74 22                	je     8010541c <forkret+0x3d>
    // Some initialization functions must be run in the context
    // of a regular process (e.g., they call sleep), and thus cannot
    // be run from main().
    first = 0;
801053fa:	c7 05 20 c0 10 80 00 	movl   $0x0,0x8010c020
80105401:	00 00 00 
    iinit(ROOTDEV);
80105404:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
8010540b:	e8 33 c9 ff ff       	call   80101d43 <iinit>
    initlog(ROOTDEV);
80105410:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80105417:	e8 4c e4 ff ff       	call   80103868 <initlog>
  }

  // Return to "caller", actually trapret (see allocproc).
}
8010541c:	c9                   	leave  
8010541d:	c3                   	ret    

8010541e <sleep>:

// Atomically release lock and sleep on chan.
// Reacquires lock when awakened.
void
sleep(void *chan, struct spinlock *lk)
{
8010541e:	55                   	push   %ebp
8010541f:	89 e5                	mov    %esp,%ebp
80105421:	83 ec 18             	sub    $0x18,%esp
  if(proc == 0)
80105424:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010542a:	85 c0                	test   %eax,%eax
8010542c:	75 0c                	jne    8010543a <sleep+0x1c>
    panic("sleep");
8010542e:	c7 04 24 c9 93 10 80 	movl   $0x801093c9,(%esp)
80105435:	e8 2f b4 ff ff       	call   80100869 <panic>

  if(lk == 0)
8010543a:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
8010543e:	75 0c                	jne    8010544c <sleep+0x2e>
    panic("sleep without lk");
80105440:	c7 04 24 cf 93 10 80 	movl   $0x801093cf,(%esp)
80105447:	e8 1d b4 ff ff       	call   80100869 <panic>
  // change p->state and then call sched.
  // Once we hold ptable.lock, we can be
  // guaranteed that we won't miss any wakeup
  // (wakeup runs with ptable.lock locked),
  // so it's okay to release lk.
  if(lk != &ptable.lock){  //DOC: sleeplock0
8010544c:	81 7d 0c e0 41 11 80 	cmpl   $0x801141e0,0xc(%ebp)
80105453:	74 17                	je     8010546c <sleep+0x4e>
    acquire(&ptable.lock);  //DOC: sleeplock1
80105455:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
8010545c:	e8 e6 02 00 00       	call   80105747 <acquire>
    release(lk);
80105461:	8b 45 0c             	mov    0xc(%ebp),%eax
80105464:	89 04 24             	mov    %eax,(%esp)
80105467:	e8 44 03 00 00       	call   801057b0 <release>
  }

  // Go to sleep.
  proc->chan = chan;
8010546c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105472:	8b 55 08             	mov    0x8(%ebp),%edx
80105475:	89 50 20             	mov    %edx,0x20(%eax)
  proc->state = SLEEPING;
80105478:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010547e:	c7 40 0c 02 00 00 00 	movl   $0x2,0xc(%eax)
  sched();
80105485:	e8 71 fe ff ff       	call   801052fb <sched>

  // Tidy up.
  proc->chan = 0;
8010548a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105490:	c7 40 20 00 00 00 00 	movl   $0x0,0x20(%eax)

  // Reacquire original lock.
  if(lk != &ptable.lock){  //DOC: sleeplock2
80105497:	81 7d 0c e0 41 11 80 	cmpl   $0x801141e0,0xc(%ebp)
8010549e:	74 17                	je     801054b7 <sleep+0x99>
    release(&ptable.lock);
801054a0:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
801054a7:	e8 04 03 00 00       	call   801057b0 <release>
    acquire(lk);
801054ac:	8b 45 0c             	mov    0xc(%ebp),%eax
801054af:	89 04 24             	mov    %eax,(%esp)
801054b2:	e8 90 02 00 00       	call   80105747 <acquire>
  }
}
801054b7:	c9                   	leave  
801054b8:	c3                   	ret    

801054b9 <wakeup1>:
//PAGEBREAK!
// Wake up all processes sleeping on chan.
// The ptable lock must be held.
static void
wakeup1(void *chan)
{
801054b9:	55                   	push   %ebp
801054ba:	89 e5                	mov    %esp,%ebp
801054bc:	83 ec 10             	sub    $0x10,%esp
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801054bf:	c7 45 fc 14 42 11 80 	movl   $0x80114214,-0x4(%ebp)
801054c6:	eb 27                	jmp    801054ef <wakeup1+0x36>
    if(p->state == SLEEPING && p->chan == chan)
801054c8:	8b 45 fc             	mov    -0x4(%ebp),%eax
801054cb:	8b 40 0c             	mov    0xc(%eax),%eax
801054ce:	83 f8 02             	cmp    $0x2,%eax
801054d1:	75 15                	jne    801054e8 <wakeup1+0x2f>
801054d3:	8b 45 fc             	mov    -0x4(%ebp),%eax
801054d6:	8b 40 20             	mov    0x20(%eax),%eax
801054d9:	3b 45 08             	cmp    0x8(%ebp),%eax
801054dc:	75 0a                	jne    801054e8 <wakeup1+0x2f>
      p->state = RUNNABLE;
801054de:	8b 45 fc             	mov    -0x4(%ebp),%eax
801054e1:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801054e8:	81 45 fc 84 00 00 00 	addl   $0x84,-0x4(%ebp)
801054ef:	b8 14 63 11 80       	mov    $0x80116314,%eax
801054f4:	39 45 fc             	cmp    %eax,-0x4(%ebp)
801054f7:	72 cf                	jb     801054c8 <wakeup1+0xf>
    if(p->state == SLEEPING && p->chan == chan)
      p->state = RUNNABLE;
}
801054f9:	c9                   	leave  
801054fa:	c3                   	ret    

801054fb <wakeup>:

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
801054fb:	55                   	push   %ebp
801054fc:	89 e5                	mov    %esp,%ebp
801054fe:	83 ec 18             	sub    $0x18,%esp
  acquire(&ptable.lock);
80105501:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80105508:	e8 3a 02 00 00       	call   80105747 <acquire>
  wakeup1(chan);
8010550d:	8b 45 08             	mov    0x8(%ebp),%eax
80105510:	89 04 24             	mov    %eax,(%esp)
80105513:	e8 a1 ff ff ff       	call   801054b9 <wakeup1>
  release(&ptable.lock);
80105518:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
8010551f:	e8 8c 02 00 00       	call   801057b0 <release>
}
80105524:	c9                   	leave  
80105525:	c3                   	ret    

80105526 <kill>:
// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid)
{
80105526:	55                   	push   %ebp
80105527:	89 e5                	mov    %esp,%ebp
80105529:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;

  acquire(&ptable.lock);
8010552c:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80105533:	e8 0f 02 00 00       	call   80105747 <acquire>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80105538:	c7 45 f4 14 42 11 80 	movl   $0x80114214,-0xc(%ebp)
8010553f:	eb 44                	jmp    80105585 <kill+0x5f>
    if(p->pid == pid){
80105541:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105544:	8b 40 10             	mov    0x10(%eax),%eax
80105547:	3b 45 08             	cmp    0x8(%ebp),%eax
8010554a:	75 32                	jne    8010557e <kill+0x58>
      p->killed = 1;
8010554c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010554f:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
80105556:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105559:	8b 40 0c             	mov    0xc(%eax),%eax
8010555c:	83 f8 02             	cmp    $0x2,%eax
8010555f:	75 0a                	jne    8010556b <kill+0x45>
        p->state = RUNNABLE;
80105561:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105564:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
      release(&ptable.lock);
8010556b:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80105572:	e8 39 02 00 00       	call   801057b0 <release>
      return 0;
80105577:	b8 00 00 00 00       	mov    $0x0,%eax
8010557c:	eb 22                	jmp    801055a0 <kill+0x7a>
kill(int pid)
{
  struct proc *p;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
8010557e:	81 45 f4 84 00 00 00 	addl   $0x84,-0xc(%ebp)
80105585:	b8 14 63 11 80       	mov    $0x80116314,%eax
8010558a:	39 45 f4             	cmp    %eax,-0xc(%ebp)
8010558d:	72 b2                	jb     80105541 <kill+0x1b>
        p->state = RUNNABLE;
      release(&ptable.lock);
      return 0;
    }
  }
  release(&ptable.lock);
8010558f:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80105596:	e8 15 02 00 00       	call   801057b0 <release>
  return -1;
8010559b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801055a0:	c9                   	leave  
801055a1:	c3                   	ret    

801055a2 <procdump>:
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
801055a2:	55                   	push   %ebp
801055a3:	89 e5                	mov    %esp,%ebp
801055a5:	83 ec 58             	sub    $0x58,%esp
  int i;
  struct proc *p;
  char *state;
  uint pc[10];

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801055a8:	c7 45 f0 14 42 11 80 	movl   $0x80114214,-0x10(%ebp)
801055af:	e9 e3 00 00 00       	jmp    80105697 <procdump+0xf5>
    if(p->state == UNUSED)
801055b4:	8b 45 f0             	mov    -0x10(%ebp),%eax
801055b7:	8b 40 0c             	mov    0xc(%eax),%eax
801055ba:	85 c0                	test   %eax,%eax
801055bc:	0f 84 cd 00 00 00    	je     8010568f <procdump+0xed>
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
801055c2:	8b 45 f0             	mov    -0x10(%ebp),%eax
801055c5:	8b 40 0c             	mov    0xc(%eax),%eax
801055c8:	83 f8 05             	cmp    $0x5,%eax
801055cb:	77 23                	ja     801055f0 <procdump+0x4e>
801055cd:	8b 45 f0             	mov    -0x10(%ebp),%eax
801055d0:	8b 40 0c             	mov    0xc(%eax),%eax
801055d3:	8b 04 85 08 c0 10 80 	mov    -0x7fef3ff8(,%eax,4),%eax
801055da:	85 c0                	test   %eax,%eax
801055dc:	74 12                	je     801055f0 <procdump+0x4e>
      state = states[p->state];
801055de:	8b 45 f0             	mov    -0x10(%ebp),%eax
801055e1:	8b 40 0c             	mov    0xc(%eax),%eax
801055e4:	8b 04 85 08 c0 10 80 	mov    -0x7fef3ff8(,%eax,4),%eax
801055eb:	89 45 f4             	mov    %eax,-0xc(%ebp)
  uint pc[10];

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED)
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
801055ee:	eb 07                	jmp    801055f7 <procdump+0x55>
      state = states[p->state];
    else
      state = "???";
801055f0:	c7 45 f4 e0 93 10 80 	movl   $0x801093e0,-0xc(%ebp)
    cprintf("%d %s %s", p->pid, state, p->name);
801055f7:	8b 45 f0             	mov    -0x10(%ebp),%eax
801055fa:	8d 50 6c             	lea    0x6c(%eax),%edx
801055fd:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105600:	8b 40 10             	mov    0x10(%eax),%eax
80105603:	89 54 24 0c          	mov    %edx,0xc(%esp)
80105607:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010560a:	89 54 24 08          	mov    %edx,0x8(%esp)
8010560e:	89 44 24 04          	mov    %eax,0x4(%esp)
80105612:	c7 04 24 e4 93 10 80 	movl   $0x801093e4,(%esp)
80105619:	e8 ab b0 ff ff       	call   801006c9 <cprintf>
    if(p->state == SLEEPING){
8010561e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105621:	8b 40 0c             	mov    0xc(%eax),%eax
80105624:	83 f8 02             	cmp    $0x2,%eax
80105627:	75 58                	jne    80105681 <procdump+0xdf>
      getcallerpcs((uint*)p->context->ebp+2, NELEM(pc), pc);
80105629:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010562c:	8b 40 1c             	mov    0x1c(%eax),%eax
8010562f:	8b 40 0c             	mov    0xc(%eax),%eax
80105632:	83 c0 08             	add    $0x8,%eax
80105635:	8d 55 c4             	lea    -0x3c(%ebp),%edx
80105638:	89 54 24 08          	mov    %edx,0x8(%esp)
8010563c:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
80105643:	00 
80105644:	89 04 24             	mov    %eax,(%esp)
80105647:	e8 b3 01 00 00       	call   801057ff <getcallerpcs>
      for(i=0; i<10 && pc[i] != 0; i++)
8010564c:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
80105653:	eb 1b                	jmp    80105670 <procdump+0xce>
        cprintf(" %p", pc[i]);
80105655:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105658:	8b 44 85 c4          	mov    -0x3c(%ebp,%eax,4),%eax
8010565c:	89 44 24 04          	mov    %eax,0x4(%esp)
80105660:	c7 04 24 ed 93 10 80 	movl   $0x801093ed,(%esp)
80105667:	e8 5d b0 ff ff       	call   801006c9 <cprintf>
    else
      state = "???";
    cprintf("%d %s %s", p->pid, state, p->name);
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, NELEM(pc), pc);
      for(i=0; i<10 && pc[i] != 0; i++)
8010566c:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
80105670:	83 7d ec 09          	cmpl   $0x9,-0x14(%ebp)
80105674:	7f 0b                	jg     80105681 <procdump+0xdf>
80105676:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105679:	8b 44 85 c4          	mov    -0x3c(%ebp,%eax,4),%eax
8010567d:	85 c0                	test   %eax,%eax
8010567f:	75 d4                	jne    80105655 <procdump+0xb3>
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
80105681:	c7 04 24 f1 93 10 80 	movl   $0x801093f1,(%esp)
80105688:	e8 3c b0 ff ff       	call   801006c9 <cprintf>
8010568d:	eb 01                	jmp    80105690 <procdump+0xee>
  char *state;
  uint pc[10];

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED)
      continue;
8010568f:	90                   	nop
  int i;
  struct proc *p;
  char *state;
  uint pc[10];

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80105690:	81 45 f0 84 00 00 00 	addl   $0x84,-0x10(%ebp)
80105697:	b8 14 63 11 80       	mov    $0x80116314,%eax
8010569c:	39 45 f0             	cmp    %eax,-0x10(%ebp)
8010569f:	0f 82 0f ff ff ff    	jb     801055b4 <procdump+0x12>
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
  }
}
801056a5:	c9                   	leave  
801056a6:	c3                   	ret    

801056a7 <sys_syscallstats>:

void
sys_syscallstats(void)
{
801056a7:	55                   	push   %ebp
801056a8:	89 e5                	mov    %esp,%ebp
801056aa:	83 ec 28             	sub    $0x28,%esp
	int i;
	for(i = 0; i < sizeof(syscallstats_data.counts); i++)
801056ad:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
801056b4:	eb 2c                	jmp    801056e2 <sys_syscallstats+0x3b>
	{
		if(syscallstats_data.counts[i] != 0)
801056b6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801056b9:	8b 04 85 20 63 11 80 	mov    -0x7fee9ce0(,%eax,4),%eax
801056c0:	85 c0                	test   %eax,%eax
801056c2:	74 1a                	je     801056de <sys_syscallstats+0x37>
		{
			cprintf("%s", syscallstats_data.counts[i]);
801056c4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801056c7:	8b 04 85 20 63 11 80 	mov    -0x7fee9ce0(,%eax,4),%eax
801056ce:	89 44 24 04          	mov    %eax,0x4(%esp)
801056d2:	c7 04 24 f3 93 10 80 	movl   $0x801093f3,(%esp)
801056d9:	e8 eb af ff ff       	call   801006c9 <cprintf>

void
sys_syscallstats(void)
{
	int i;
	for(i = 0; i < sizeof(syscallstats_data.counts); i++)
801056de:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801056e2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801056e5:	3d ff 03 00 00       	cmp    $0x3ff,%eax
801056ea:	76 ca                	jbe    801056b6 <sys_syscallstats+0xf>
		{
			cprintf("%s", syscallstats_data.counts[i]);
			//cprintf("Syscall id " + i + " has been called " + syscallstats_data.count[i] + " times, with a total of " + syscallstats_data.ticks[i] + " ticks for all calls");
		}
	}
}
801056ec:	c9                   	leave  
801056ed:	c3                   	ret    
	...

801056f0 <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
801056f0:	55                   	push   %ebp
801056f1:	89 e5                	mov    %esp,%ebp
801056f3:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
801056f6:	9c                   	pushf  
801056f7:	58                   	pop    %eax
801056f8:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return eflags;
801056fb:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
801056fe:	c9                   	leave  
801056ff:	c3                   	ret    

80105700 <cli>:
  asm volatile("movw %0, %%gs" : : "r" (v));
}

static inline void
cli(void)
{
80105700:	55                   	push   %ebp
80105701:	89 e5                	mov    %esp,%ebp
  asm volatile("cli");
80105703:	fa                   	cli    
}
80105704:	5d                   	pop    %ebp
80105705:	c3                   	ret    

80105706 <sti>:

static inline void
sti(void)
{
80105706:	55                   	push   %ebp
80105707:	89 e5                	mov    %esp,%ebp
  asm volatile("sti");
80105709:	fb                   	sti    
}
8010570a:	5d                   	pop    %ebp
8010570b:	c3                   	ret    

8010570c <xchg>:

static inline uint
xchg(volatile uint *addr, uint newval)
{
8010570c:	55                   	push   %ebp
8010570d:	89 e5                	mov    %esp,%ebp
8010570f:	83 ec 10             	sub    $0x10,%esp
  uint result;

  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
80105712:	8b 55 08             	mov    0x8(%ebp),%edx
80105715:	8b 45 0c             	mov    0xc(%ebp),%eax
80105718:	8b 4d 08             	mov    0x8(%ebp),%ecx
8010571b:	f0 87 02             	lock xchg %eax,(%edx)
8010571e:	89 45 fc             	mov    %eax,-0x4(%ebp)
               "+m" (*addr), "=a" (result) :
               "1" (newval) :
               "cc");
  return result;
80105721:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80105724:	c9                   	leave  
80105725:	c3                   	ret    

80105726 <initlock>:
#include "proc.h"
#include "spinlock.h"

void
initlock(struct spinlock *lk, char *name)
{
80105726:	55                   	push   %ebp
80105727:	89 e5                	mov    %esp,%ebp
  lk->name = name;
80105729:	8b 45 08             	mov    0x8(%ebp),%eax
8010572c:	8b 55 0c             	mov    0xc(%ebp),%edx
8010572f:	89 50 04             	mov    %edx,0x4(%eax)
  lk->locked = 0;
80105732:	8b 45 08             	mov    0x8(%ebp),%eax
80105735:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  lk->cpu = 0;
8010573b:	8b 45 08             	mov    0x8(%ebp),%eax
8010573e:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
}
80105745:	5d                   	pop    %ebp
80105746:	c3                   	ret    

80105747 <acquire>:
// Loops (spins) until the lock is acquired.
// Holding a lock for a long time may cause
// other CPUs to waste time spinning to acquire it.
void
acquire(struct spinlock *lk)
{
80105747:	55                   	push   %ebp
80105748:	89 e5                	mov    %esp,%ebp
8010574a:	83 ec 18             	sub    $0x18,%esp
  pushcli(); // disable interrupts to avoid deadlock.
8010574d:	e8 53 01 00 00       	call   801058a5 <pushcli>
  if(holding(lk))
80105752:	8b 45 08             	mov    0x8(%ebp),%eax
80105755:	89 04 24             	mov    %eax,(%esp)
80105758:	e8 1e 01 00 00       	call   8010587b <holding>
8010575d:	85 c0                	test   %eax,%eax
8010575f:	74 0c                	je     8010576d <acquire+0x26>
    panic("acquire");
80105761:	c7 04 24 20 94 10 80 	movl   $0x80109420,(%esp)
80105768:	e8 fc b0 ff ff       	call   80100869 <panic>

  // The xchg is atomic.
  // It also serializes, so that reads after acquire are not
  // reordered before it. 
  while(xchg(&lk->locked, 1) != 0)
8010576d:	8b 45 08             	mov    0x8(%ebp),%eax
80105770:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80105777:	00 
80105778:	89 04 24             	mov    %eax,(%esp)
8010577b:	e8 8c ff ff ff       	call   8010570c <xchg>
80105780:	85 c0                	test   %eax,%eax
80105782:	75 e9                	jne    8010576d <acquire+0x26>
    ;

  // Record info about lock acquisition for debugging.
  lk->cpu = cpu;
80105784:	8b 45 08             	mov    0x8(%ebp),%eax
80105787:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
8010578e:	89 50 08             	mov    %edx,0x8(%eax)
  getcallerpcs(&lk, NELEM(lk->pcs), lk->pcs);
80105791:	8b 45 08             	mov    0x8(%ebp),%eax
80105794:	83 c0 0c             	add    $0xc,%eax
80105797:	89 44 24 08          	mov    %eax,0x8(%esp)
8010579b:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
801057a2:	00 
801057a3:	8d 45 08             	lea    0x8(%ebp),%eax
801057a6:	89 04 24             	mov    %eax,(%esp)
801057a9:	e8 51 00 00 00       	call   801057ff <getcallerpcs>
}
801057ae:	c9                   	leave  
801057af:	c3                   	ret    

801057b0 <release>:

// Release the lock.
void
release(struct spinlock *lk)
{
801057b0:	55                   	push   %ebp
801057b1:	89 e5                	mov    %esp,%ebp
801057b3:	83 ec 18             	sub    $0x18,%esp
  if(!holding(lk))
801057b6:	8b 45 08             	mov    0x8(%ebp),%eax
801057b9:	89 04 24             	mov    %eax,(%esp)
801057bc:	e8 ba 00 00 00       	call   8010587b <holding>
801057c1:	85 c0                	test   %eax,%eax
801057c3:	75 0c                	jne    801057d1 <release+0x21>
    panic("release");
801057c5:	c7 04 24 28 94 10 80 	movl   $0x80109428,(%esp)
801057cc:	e8 98 b0 ff ff       	call   80100869 <panic>

  lk->pcs[0] = 0;
801057d1:	8b 45 08             	mov    0x8(%ebp),%eax
801057d4:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
  lk->cpu = 0;
801057db:	8b 45 08             	mov    0x8(%ebp),%eax
801057de:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
  // But the 2007 Intel 64 Architecture Memory Ordering White
  // Paper says that Intel 64 and IA-32 will not move a load
  // after a store. So lock->locked = 0 would work here.
  // The xchg being asm volatile ensures gcc emits it after
  // the above assignments (and after the critical section).
  xchg(&lk->locked, 0);
801057e5:	8b 45 08             	mov    0x8(%ebp),%eax
801057e8:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801057ef:	00 
801057f0:	89 04 24             	mov    %eax,(%esp)
801057f3:	e8 14 ff ff ff       	call   8010570c <xchg>

  popcli();
801057f8:	e8 f0 00 00 00       	call   801058ed <popcli>
}
801057fd:	c9                   	leave  
801057fe:	c3                   	ret    

801057ff <getcallerpcs>:

// Record the current call stack in pcs[] by following the %ebp chain.
void
getcallerpcs(void *v, uint n, uint pcs[])
{
801057ff:	55                   	push   %ebp
80105800:	89 e5                	mov    %esp,%ebp
80105802:	83 ec 10             	sub    $0x10,%esp
  uint *ebp;
  int i;
  
  ebp = (uint*)v - 2;
80105805:	8b 45 08             	mov    0x8(%ebp),%eax
80105808:	83 e8 08             	sub    $0x8,%eax
8010580b:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(i = 0; i < n; i++){
8010580e:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
80105815:	eb 34                	jmp    8010584b <getcallerpcs+0x4c>
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
80105817:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
8010581b:	74 4d                	je     8010586a <getcallerpcs+0x6b>
8010581d:	81 7d f8 ff ff ff 7f 	cmpl   $0x7fffffff,-0x8(%ebp)
80105824:	76 47                	jbe    8010586d <getcallerpcs+0x6e>
80105826:	83 7d f8 ff          	cmpl   $0xffffffff,-0x8(%ebp)
8010582a:	74 44                	je     80105870 <getcallerpcs+0x71>
      break;
    pcs[i] = ebp[1];     // saved %eip
8010582c:	8b 45 fc             	mov    -0x4(%ebp),%eax
8010582f:	c1 e0 02             	shl    $0x2,%eax
80105832:	03 45 10             	add    0x10(%ebp),%eax
80105835:	8b 55 f8             	mov    -0x8(%ebp),%edx
80105838:	83 c2 04             	add    $0x4,%edx
8010583b:	8b 12                	mov    (%edx),%edx
8010583d:	89 10                	mov    %edx,(%eax)
    ebp = (uint*)ebp[0]; // saved %ebp
8010583f:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105842:	8b 00                	mov    (%eax),%eax
80105844:	89 45 f8             	mov    %eax,-0x8(%ebp)
{
  uint *ebp;
  int i;
  
  ebp = (uint*)v - 2;
  for(i = 0; i < n; i++){
80105847:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
8010584b:	8b 45 fc             	mov    -0x4(%ebp),%eax
8010584e:	3b 45 0c             	cmp    0xc(%ebp),%eax
80105851:	72 c4                	jb     80105817 <getcallerpcs+0x18>
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
      break;
    pcs[i] = ebp[1];     // saved %eip
    ebp = (uint*)ebp[0]; // saved %ebp
  }
  for(; i < n; i++)
80105853:	eb 1c                	jmp    80105871 <getcallerpcs+0x72>
    pcs[i] = 0;
80105855:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105858:	c1 e0 02             	shl    $0x2,%eax
8010585b:	03 45 10             	add    0x10(%ebp),%eax
8010585e:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
      break;
    pcs[i] = ebp[1];     // saved %eip
    ebp = (uint*)ebp[0]; // saved %ebp
  }
  for(; i < n; i++)
80105864:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80105868:	eb 07                	jmp    80105871 <getcallerpcs+0x72>
8010586a:	90                   	nop
8010586b:	eb 04                	jmp    80105871 <getcallerpcs+0x72>
8010586d:	90                   	nop
8010586e:	eb 01                	jmp    80105871 <getcallerpcs+0x72>
80105870:	90                   	nop
80105871:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105874:	3b 45 0c             	cmp    0xc(%ebp),%eax
80105877:	72 dc                	jb     80105855 <getcallerpcs+0x56>
    pcs[i] = 0;
}
80105879:	c9                   	leave  
8010587a:	c3                   	ret    

8010587b <holding>:

// Check whether this cpu is holding the lock.
int
holding(struct spinlock *lock)
{
8010587b:	55                   	push   %ebp
8010587c:	89 e5                	mov    %esp,%ebp
  return lock->locked && lock->cpu == cpu;
8010587e:	8b 45 08             	mov    0x8(%ebp),%eax
80105881:	8b 00                	mov    (%eax),%eax
80105883:	85 c0                	test   %eax,%eax
80105885:	74 17                	je     8010589e <holding+0x23>
80105887:	8b 45 08             	mov    0x8(%ebp),%eax
8010588a:	8b 50 08             	mov    0x8(%eax),%edx
8010588d:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105893:	39 c2                	cmp    %eax,%edx
80105895:	75 07                	jne    8010589e <holding+0x23>
80105897:	b8 01 00 00 00       	mov    $0x1,%eax
8010589c:	eb 05                	jmp    801058a3 <holding+0x28>
8010589e:	b8 00 00 00 00       	mov    $0x0,%eax
}
801058a3:	5d                   	pop    %ebp
801058a4:	c3                   	ret    

801058a5 <pushcli>:
// it takes two popcli to undo two pushcli.  Also, if interrupts
// are off, then pushcli, popcli leaves them off.

void
pushcli(void)
{
801058a5:	55                   	push   %ebp
801058a6:	89 e5                	mov    %esp,%ebp
801058a8:	83 ec 10             	sub    $0x10,%esp
  int eflags;
  
  eflags = readeflags();
801058ab:	e8 40 fe ff ff       	call   801056f0 <readeflags>
801058b0:	89 45 fc             	mov    %eax,-0x4(%ebp)
  cli();
801058b3:	e8 48 fe ff ff       	call   80105700 <cli>
  if(cpu->ncli++ == 0)
801058b8:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801058be:	8b 90 ac 00 00 00    	mov    0xac(%eax),%edx
801058c4:	85 d2                	test   %edx,%edx
801058c6:	0f 94 c1             	sete   %cl
801058c9:	83 c2 01             	add    $0x1,%edx
801058cc:	89 90 ac 00 00 00    	mov    %edx,0xac(%eax)
801058d2:	84 c9                	test   %cl,%cl
801058d4:	74 15                	je     801058eb <pushcli+0x46>
    cpu->intena = eflags & FL_IF;
801058d6:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801058dc:	8b 55 fc             	mov    -0x4(%ebp),%edx
801058df:	81 e2 00 02 00 00    	and    $0x200,%edx
801058e5:	89 90 b0 00 00 00    	mov    %edx,0xb0(%eax)
}
801058eb:	c9                   	leave  
801058ec:	c3                   	ret    

801058ed <popcli>:

void
popcli(void)
{
801058ed:	55                   	push   %ebp
801058ee:	89 e5                	mov    %esp,%ebp
801058f0:	83 ec 18             	sub    $0x18,%esp
  if(readeflags()&FL_IF)
801058f3:	e8 f8 fd ff ff       	call   801056f0 <readeflags>
801058f8:	25 00 02 00 00       	and    $0x200,%eax
801058fd:	85 c0                	test   %eax,%eax
801058ff:	74 0c                	je     8010590d <popcli+0x20>
    panic("popcli - interruptible");
80105901:	c7 04 24 30 94 10 80 	movl   $0x80109430,(%esp)
80105908:	e8 5c af ff ff       	call   80100869 <panic>
  if(--cpu->ncli < 0)
8010590d:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105913:	8b 90 ac 00 00 00    	mov    0xac(%eax),%edx
80105919:	83 ea 01             	sub    $0x1,%edx
8010591c:	89 90 ac 00 00 00    	mov    %edx,0xac(%eax)
80105922:	8b 80 ac 00 00 00    	mov    0xac(%eax),%eax
80105928:	85 c0                	test   %eax,%eax
8010592a:	79 0c                	jns    80105938 <popcli+0x4b>
    panic("popcli");
8010592c:	c7 04 24 47 94 10 80 	movl   $0x80109447,(%esp)
80105933:	e8 31 af ff ff       	call   80100869 <panic>
  if(cpu->ncli == 0 && cpu->intena)
80105938:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010593e:	8b 80 ac 00 00 00    	mov    0xac(%eax),%eax
80105944:	85 c0                	test   %eax,%eax
80105946:	75 15                	jne    8010595d <popcli+0x70>
80105948:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010594e:	8b 80 b0 00 00 00    	mov    0xb0(%eax),%eax
80105954:	85 c0                	test   %eax,%eax
80105956:	74 05                	je     8010595d <popcli+0x70>
    sti();
80105958:	e8 a9 fd ff ff       	call   80105706 <sti>
}
8010595d:	c9                   	leave  
8010595e:	c3                   	ret    
	...

80105960 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
80105960:	55                   	push   %ebp
80105961:	89 e5                	mov    %esp,%ebp
80105963:	57                   	push   %edi
80105964:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
80105965:	8b 4d 08             	mov    0x8(%ebp),%ecx
80105968:	8b 55 10             	mov    0x10(%ebp),%edx
8010596b:	8b 45 0c             	mov    0xc(%ebp),%eax
8010596e:	89 cb                	mov    %ecx,%ebx
80105970:	89 df                	mov    %ebx,%edi
80105972:	89 d1                	mov    %edx,%ecx
80105974:	fc                   	cld    
80105975:	f3 aa                	rep stos %al,%es:(%edi)
80105977:	89 ca                	mov    %ecx,%edx
80105979:	89 fb                	mov    %edi,%ebx
8010597b:	89 5d 08             	mov    %ebx,0x8(%ebp)
8010597e:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
80105981:	5b                   	pop    %ebx
80105982:	5f                   	pop    %edi
80105983:	5d                   	pop    %ebp
80105984:	c3                   	ret    

80105985 <stosl>:

static inline void
stosl(void *addr, int data, int cnt)
{
80105985:	55                   	push   %ebp
80105986:	89 e5                	mov    %esp,%ebp
80105988:	57                   	push   %edi
80105989:	53                   	push   %ebx
  asm volatile("cld; rep stosl" :
8010598a:	8b 4d 08             	mov    0x8(%ebp),%ecx
8010598d:	8b 55 10             	mov    0x10(%ebp),%edx
80105990:	8b 45 0c             	mov    0xc(%ebp),%eax
80105993:	89 cb                	mov    %ecx,%ebx
80105995:	89 df                	mov    %ebx,%edi
80105997:	89 d1                	mov    %edx,%ecx
80105999:	fc                   	cld    
8010599a:	f3 ab                	rep stos %eax,%es:(%edi)
8010599c:	89 ca                	mov    %ecx,%edx
8010599e:	89 fb                	mov    %edi,%ebx
801059a0:	89 5d 08             	mov    %ebx,0x8(%ebp)
801059a3:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
801059a6:	5b                   	pop    %ebx
801059a7:	5f                   	pop    %edi
801059a8:	5d                   	pop    %ebp
801059a9:	c3                   	ret    

801059aa <memset>:
#include "types.h"
#include "x86.h"

void*
memset(void *dst, int c, uint n)
{
801059aa:	55                   	push   %ebp
801059ab:	89 e5                	mov    %esp,%ebp
801059ad:	83 ec 0c             	sub    $0xc,%esp
  if ((int)dst%4 == 0 && n%4 == 0){
801059b0:	8b 45 08             	mov    0x8(%ebp),%eax
801059b3:	83 e0 03             	and    $0x3,%eax
801059b6:	85 c0                	test   %eax,%eax
801059b8:	75 49                	jne    80105a03 <memset+0x59>
801059ba:	8b 45 10             	mov    0x10(%ebp),%eax
801059bd:	83 e0 03             	and    $0x3,%eax
801059c0:	85 c0                	test   %eax,%eax
801059c2:	75 3f                	jne    80105a03 <memset+0x59>
    c &= 0xFF;
801059c4:	81 65 0c ff 00 00 00 	andl   $0xff,0xc(%ebp)
    stosl(dst, (c<<24)|(c<<16)|(c<<8)|c, n/4);
801059cb:	8b 45 10             	mov    0x10(%ebp),%eax
801059ce:	c1 e8 02             	shr    $0x2,%eax
801059d1:	89 c2                	mov    %eax,%edx
801059d3:	8b 45 0c             	mov    0xc(%ebp),%eax
801059d6:	89 c1                	mov    %eax,%ecx
801059d8:	c1 e1 18             	shl    $0x18,%ecx
801059db:	8b 45 0c             	mov    0xc(%ebp),%eax
801059de:	c1 e0 10             	shl    $0x10,%eax
801059e1:	09 c1                	or     %eax,%ecx
801059e3:	8b 45 0c             	mov    0xc(%ebp),%eax
801059e6:	c1 e0 08             	shl    $0x8,%eax
801059e9:	09 c8                	or     %ecx,%eax
801059eb:	0b 45 0c             	or     0xc(%ebp),%eax
801059ee:	89 54 24 08          	mov    %edx,0x8(%esp)
801059f2:	89 44 24 04          	mov    %eax,0x4(%esp)
801059f6:	8b 45 08             	mov    0x8(%ebp),%eax
801059f9:	89 04 24             	mov    %eax,(%esp)
801059fc:	e8 84 ff ff ff       	call   80105985 <stosl>
#include "x86.h"

void*
memset(void *dst, int c, uint n)
{
  if ((int)dst%4 == 0 && n%4 == 0){
80105a01:	eb 19                	jmp    80105a1c <memset+0x72>
    c &= 0xFF;
    stosl(dst, (c<<24)|(c<<16)|(c<<8)|c, n/4);
  } else
    stosb(dst, c, n);
80105a03:	8b 45 10             	mov    0x10(%ebp),%eax
80105a06:	89 44 24 08          	mov    %eax,0x8(%esp)
80105a0a:	8b 45 0c             	mov    0xc(%ebp),%eax
80105a0d:	89 44 24 04          	mov    %eax,0x4(%esp)
80105a11:	8b 45 08             	mov    0x8(%ebp),%eax
80105a14:	89 04 24             	mov    %eax,(%esp)
80105a17:	e8 44 ff ff ff       	call   80105960 <stosb>
  return dst;
80105a1c:	8b 45 08             	mov    0x8(%ebp),%eax
}
80105a1f:	c9                   	leave  
80105a20:	c3                   	ret    

80105a21 <memcmp>:

int
memcmp(const void *v1, const void *v2, uint n)
{
80105a21:	55                   	push   %ebp
80105a22:	89 e5                	mov    %esp,%ebp
80105a24:	83 ec 10             	sub    $0x10,%esp
  const uchar *s1, *s2;
  
  s1 = v1;
80105a27:	8b 45 08             	mov    0x8(%ebp),%eax
80105a2a:	89 45 f8             	mov    %eax,-0x8(%ebp)
  s2 = v2;
80105a2d:	8b 45 0c             	mov    0xc(%ebp),%eax
80105a30:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0){
80105a33:	eb 32                	jmp    80105a67 <memcmp+0x46>
    if(*s1 != *s2)
80105a35:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105a38:	0f b6 10             	movzbl (%eax),%edx
80105a3b:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105a3e:	0f b6 00             	movzbl (%eax),%eax
80105a41:	38 c2                	cmp    %al,%dl
80105a43:	74 1a                	je     80105a5f <memcmp+0x3e>
      return *s1 - *s2;
80105a45:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105a48:	0f b6 00             	movzbl (%eax),%eax
80105a4b:	0f b6 d0             	movzbl %al,%edx
80105a4e:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105a51:	0f b6 00             	movzbl (%eax),%eax
80105a54:	0f b6 c0             	movzbl %al,%eax
80105a57:	89 d1                	mov    %edx,%ecx
80105a59:	29 c1                	sub    %eax,%ecx
80105a5b:	89 c8                	mov    %ecx,%eax
80105a5d:	eb 1c                	jmp    80105a7b <memcmp+0x5a>
    s1++, s2++;
80105a5f:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
80105a63:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
{
  const uchar *s1, *s2;
  
  s1 = v1;
  s2 = v2;
  while(n-- > 0){
80105a67:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105a6b:	0f 95 c0             	setne  %al
80105a6e:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105a72:	84 c0                	test   %al,%al
80105a74:	75 bf                	jne    80105a35 <memcmp+0x14>
    if(*s1 != *s2)
      return *s1 - *s2;
    s1++, s2++;
  }

  return 0;
80105a76:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105a7b:	c9                   	leave  
80105a7c:	c3                   	ret    

80105a7d <memmove>:

void*
memmove(void *dst, const void *src, uint n)
{
80105a7d:	55                   	push   %ebp
80105a7e:	89 e5                	mov    %esp,%ebp
80105a80:	83 ec 10             	sub    $0x10,%esp
  const char *s;
  char *d;

  s = src;
80105a83:	8b 45 0c             	mov    0xc(%ebp),%eax
80105a86:	89 45 f8             	mov    %eax,-0x8(%ebp)
  d = dst;
80105a89:	8b 45 08             	mov    0x8(%ebp),%eax
80105a8c:	89 45 fc             	mov    %eax,-0x4(%ebp)
  if(s < d && s + n > d){
80105a8f:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105a92:	3b 45 fc             	cmp    -0x4(%ebp),%eax
80105a95:	73 55                	jae    80105aec <memmove+0x6f>
80105a97:	8b 45 10             	mov    0x10(%ebp),%eax
80105a9a:	8b 55 f8             	mov    -0x8(%ebp),%edx
80105a9d:	8d 04 02             	lea    (%edx,%eax,1),%eax
80105aa0:	3b 45 fc             	cmp    -0x4(%ebp),%eax
80105aa3:	76 4a                	jbe    80105aef <memmove+0x72>
    s += n;
80105aa5:	8b 45 10             	mov    0x10(%ebp),%eax
80105aa8:	01 45 f8             	add    %eax,-0x8(%ebp)
    d += n;
80105aab:	8b 45 10             	mov    0x10(%ebp),%eax
80105aae:	01 45 fc             	add    %eax,-0x4(%ebp)
    while(n-- > 0)
80105ab1:	eb 13                	jmp    80105ac6 <memmove+0x49>
      *--d = *--s;
80105ab3:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
80105ab7:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
80105abb:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105abe:	0f b6 10             	movzbl (%eax),%edx
80105ac1:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105ac4:	88 10                	mov    %dl,(%eax)
  s = src;
  d = dst;
  if(s < d && s + n > d){
    s += n;
    d += n;
    while(n-- > 0)
80105ac6:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105aca:	0f 95 c0             	setne  %al
80105acd:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105ad1:	84 c0                	test   %al,%al
80105ad3:	75 de                	jne    80105ab3 <memmove+0x36>
  const char *s;
  char *d;

  s = src;
  d = dst;
  if(s < d && s + n > d){
80105ad5:	eb 28                	jmp    80105aff <memmove+0x82>
    d += n;
    while(n-- > 0)
      *--d = *--s;
  } else
    while(n-- > 0)
      *d++ = *s++;
80105ad7:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105ada:	0f b6 10             	movzbl (%eax),%edx
80105add:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105ae0:	88 10                	mov    %dl,(%eax)
80105ae2:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80105ae6:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
80105aea:	eb 04                	jmp    80105af0 <memmove+0x73>
    s += n;
    d += n;
    while(n-- > 0)
      *--d = *--s;
  } else
    while(n-- > 0)
80105aec:	90                   	nop
80105aed:	eb 01                	jmp    80105af0 <memmove+0x73>
80105aef:	90                   	nop
80105af0:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105af4:	0f 95 c0             	setne  %al
80105af7:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105afb:	84 c0                	test   %al,%al
80105afd:	75 d8                	jne    80105ad7 <memmove+0x5a>
      *d++ = *s++;

  return dst;
80105aff:	8b 45 08             	mov    0x8(%ebp),%eax
}
80105b02:	c9                   	leave  
80105b03:	c3                   	ret    

80105b04 <memcpy>:

// memcpy exists to placate GCC.  Use memmove.
void*
memcpy(void *dst, const void *src, uint n)
{
80105b04:	55                   	push   %ebp
80105b05:	89 e5                	mov    %esp,%ebp
80105b07:	83 ec 0c             	sub    $0xc,%esp
  return memmove(dst, src, n);
80105b0a:	8b 45 10             	mov    0x10(%ebp),%eax
80105b0d:	89 44 24 08          	mov    %eax,0x8(%esp)
80105b11:	8b 45 0c             	mov    0xc(%ebp),%eax
80105b14:	89 44 24 04          	mov    %eax,0x4(%esp)
80105b18:	8b 45 08             	mov    0x8(%ebp),%eax
80105b1b:	89 04 24             	mov    %eax,(%esp)
80105b1e:	e8 5a ff ff ff       	call   80105a7d <memmove>
}
80105b23:	c9                   	leave  
80105b24:	c3                   	ret    

80105b25 <strncmp>:

int
strncmp(const char *p, const char *q, uint n)
{
80105b25:	55                   	push   %ebp
80105b26:	89 e5                	mov    %esp,%ebp
  while(n > 0 && *p && *p == *q)
80105b28:	eb 0c                	jmp    80105b36 <strncmp+0x11>
    n--, p++, q++;
80105b2a:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105b2e:	83 45 08 01          	addl   $0x1,0x8(%ebp)
80105b32:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strncmp(const char *p, const char *q, uint n)
{
  while(n > 0 && *p && *p == *q)
80105b36:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105b3a:	74 1a                	je     80105b56 <strncmp+0x31>
80105b3c:	8b 45 08             	mov    0x8(%ebp),%eax
80105b3f:	0f b6 00             	movzbl (%eax),%eax
80105b42:	84 c0                	test   %al,%al
80105b44:	74 10                	je     80105b56 <strncmp+0x31>
80105b46:	8b 45 08             	mov    0x8(%ebp),%eax
80105b49:	0f b6 10             	movzbl (%eax),%edx
80105b4c:	8b 45 0c             	mov    0xc(%ebp),%eax
80105b4f:	0f b6 00             	movzbl (%eax),%eax
80105b52:	38 c2                	cmp    %al,%dl
80105b54:	74 d4                	je     80105b2a <strncmp+0x5>
    n--, p++, q++;
  if(n == 0)
80105b56:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105b5a:	75 07                	jne    80105b63 <strncmp+0x3e>
    return 0;
80105b5c:	b8 00 00 00 00       	mov    $0x0,%eax
80105b61:	eb 18                	jmp    80105b7b <strncmp+0x56>
  return (uchar)*p - (uchar)*q;
80105b63:	8b 45 08             	mov    0x8(%ebp),%eax
80105b66:	0f b6 00             	movzbl (%eax),%eax
80105b69:	0f b6 d0             	movzbl %al,%edx
80105b6c:	8b 45 0c             	mov    0xc(%ebp),%eax
80105b6f:	0f b6 00             	movzbl (%eax),%eax
80105b72:	0f b6 c0             	movzbl %al,%eax
80105b75:	89 d1                	mov    %edx,%ecx
80105b77:	29 c1                	sub    %eax,%ecx
80105b79:	89 c8                	mov    %ecx,%eax
}
80105b7b:	5d                   	pop    %ebp
80105b7c:	c3                   	ret    

80105b7d <strncpy>:

char*
strncpy(char *s, const char *t, int n)
{
80105b7d:	55                   	push   %ebp
80105b7e:	89 e5                	mov    %esp,%ebp
80105b80:	83 ec 10             	sub    $0x10,%esp
  char *os;
  
  os = s;
80105b83:	8b 45 08             	mov    0x8(%ebp),%eax
80105b86:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0 && (*s++ = *t++) != 0)
80105b89:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105b8d:	0f 9f c0             	setg   %al
80105b90:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105b94:	84 c0                	test   %al,%al
80105b96:	74 30                	je     80105bc8 <strncpy+0x4b>
80105b98:	8b 45 0c             	mov    0xc(%ebp),%eax
80105b9b:	0f b6 10             	movzbl (%eax),%edx
80105b9e:	8b 45 08             	mov    0x8(%ebp),%eax
80105ba1:	88 10                	mov    %dl,(%eax)
80105ba3:	8b 45 08             	mov    0x8(%ebp),%eax
80105ba6:	0f b6 00             	movzbl (%eax),%eax
80105ba9:	84 c0                	test   %al,%al
80105bab:	0f 95 c0             	setne  %al
80105bae:	83 45 08 01          	addl   $0x1,0x8(%ebp)
80105bb2:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
80105bb6:	84 c0                	test   %al,%al
80105bb8:	75 cf                	jne    80105b89 <strncpy+0xc>
    ;
  while(n-- > 0)
80105bba:	eb 0d                	jmp    80105bc9 <strncpy+0x4c>
    *s++ = 0;
80105bbc:	8b 45 08             	mov    0x8(%ebp),%eax
80105bbf:	c6 00 00             	movb   $0x0,(%eax)
80105bc2:	83 45 08 01          	addl   $0x1,0x8(%ebp)
80105bc6:	eb 01                	jmp    80105bc9 <strncpy+0x4c>
  char *os;
  
  os = s;
  while(n-- > 0 && (*s++ = *t++) != 0)
    ;
  while(n-- > 0)
80105bc8:	90                   	nop
80105bc9:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105bcd:	0f 9f c0             	setg   %al
80105bd0:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105bd4:	84 c0                	test   %al,%al
80105bd6:	75 e4                	jne    80105bbc <strncpy+0x3f>
    *s++ = 0;
  return os;
80105bd8:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80105bdb:	c9                   	leave  
80105bdc:	c3                   	ret    

80105bdd <safestrcpy>:

// Like strncpy but guaranteed to NUL-terminate.
char*
safestrcpy(char *s, const char *t, int n)
{
80105bdd:	55                   	push   %ebp
80105bde:	89 e5                	mov    %esp,%ebp
80105be0:	83 ec 10             	sub    $0x10,%esp
  char *os;
  
  os = s;
80105be3:	8b 45 08             	mov    0x8(%ebp),%eax
80105be6:	89 45 fc             	mov    %eax,-0x4(%ebp)
  if(n <= 0)
80105be9:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105bed:	7f 05                	jg     80105bf4 <safestrcpy+0x17>
    return os;
80105bef:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105bf2:	eb 35                	jmp    80105c29 <safestrcpy+0x4c>
  while(--n > 0 && (*s++ = *t++) != 0)
80105bf4:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105bf8:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105bfc:	7e 22                	jle    80105c20 <safestrcpy+0x43>
80105bfe:	8b 45 0c             	mov    0xc(%ebp),%eax
80105c01:	0f b6 10             	movzbl (%eax),%edx
80105c04:	8b 45 08             	mov    0x8(%ebp),%eax
80105c07:	88 10                	mov    %dl,(%eax)
80105c09:	8b 45 08             	mov    0x8(%ebp),%eax
80105c0c:	0f b6 00             	movzbl (%eax),%eax
80105c0f:	84 c0                	test   %al,%al
80105c11:	0f 95 c0             	setne  %al
80105c14:	83 45 08 01          	addl   $0x1,0x8(%ebp)
80105c18:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
80105c1c:	84 c0                	test   %al,%al
80105c1e:	75 d4                	jne    80105bf4 <safestrcpy+0x17>
    ;
  *s = 0;
80105c20:	8b 45 08             	mov    0x8(%ebp),%eax
80105c23:	c6 00 00             	movb   $0x0,(%eax)
  return os;
80105c26:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80105c29:	c9                   	leave  
80105c2a:	c3                   	ret    

80105c2b <strlen>:

int
strlen(const char *s)
{
80105c2b:	55                   	push   %ebp
80105c2c:	89 e5                	mov    %esp,%ebp
80105c2e:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
80105c31:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
80105c38:	eb 04                	jmp    80105c3e <strlen+0x13>
80105c3a:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80105c3e:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105c41:	03 45 08             	add    0x8(%ebp),%eax
80105c44:	0f b6 00             	movzbl (%eax),%eax
80105c47:	84 c0                	test   %al,%al
80105c49:	75 ef                	jne    80105c3a <strlen+0xf>
    ;
  return n;
80105c4b:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80105c4e:	c9                   	leave  
80105c4f:	c3                   	ret    

80105c50 <swtch>:
# Save current register context in old
# and then load register context from new.

.globl swtch
swtch:
  movl 4(%esp), %eax
80105c50:	8b 44 24 04          	mov    0x4(%esp),%eax
  movl 8(%esp), %edx
80105c54:	8b 54 24 08          	mov    0x8(%esp),%edx

  # Save old callee-save registers
  pushl %ebp
80105c58:	55                   	push   %ebp
  pushl %ebx
80105c59:	53                   	push   %ebx
  pushl %esi
80105c5a:	56                   	push   %esi
  pushl %edi
80105c5b:	57                   	push   %edi

  # Switch stacks
  movl %esp, (%eax)
80105c5c:	89 20                	mov    %esp,(%eax)
  movl %edx, %esp
80105c5e:	89 d4                	mov    %edx,%esp

  # Load new callee-save registers
  popl %edi
80105c60:	5f                   	pop    %edi
  popl %esi
80105c61:	5e                   	pop    %esi
  popl %ebx
80105c62:	5b                   	pop    %ebx
  popl %ebp
80105c63:	5d                   	pop    %ebp
  ret
80105c64:	c3                   	ret    
80105c65:	00 00                	add    %al,(%eax)
	...

80105c68 <fetchint>:
// to a saved program counter, and then the first argument.

// Fetch the int at addr from the current process.
int
fetchint(uint addr, int *ip)
{
80105c68:	55                   	push   %ebp
80105c69:	89 e5                	mov    %esp,%ebp
  if(addr >= proc->sz || addr+4 > proc->sz)
80105c6b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105c71:	8b 00                	mov    (%eax),%eax
80105c73:	3b 45 08             	cmp    0x8(%ebp),%eax
80105c76:	76 12                	jbe    80105c8a <fetchint+0x22>
80105c78:	8b 45 08             	mov    0x8(%ebp),%eax
80105c7b:	8d 50 04             	lea    0x4(%eax),%edx
80105c7e:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105c84:	8b 00                	mov    (%eax),%eax
80105c86:	39 c2                	cmp    %eax,%edx
80105c88:	76 07                	jbe    80105c91 <fetchint+0x29>
    return -1;
80105c8a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105c8f:	eb 0f                	jmp    80105ca0 <fetchint+0x38>
  *ip = *(int*)(addr);
80105c91:	8b 45 08             	mov    0x8(%ebp),%eax
80105c94:	8b 10                	mov    (%eax),%edx
80105c96:	8b 45 0c             	mov    0xc(%ebp),%eax
80105c99:	89 10                	mov    %edx,(%eax)
  return 0;
80105c9b:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105ca0:	5d                   	pop    %ebp
80105ca1:	c3                   	ret    

80105ca2 <fetchstr>:
// Fetch the nul-terminated string at addr from the current process.
// Doesn't actually copy the string - just sets *pp to point at it.
// Returns length of string, not including nul.
int
fetchstr(uint addr, char **pp)
{
80105ca2:	55                   	push   %ebp
80105ca3:	89 e5                	mov    %esp,%ebp
80105ca5:	83 ec 10             	sub    $0x10,%esp
  char *s, *ep;

  if(addr >= proc->sz)
80105ca8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105cae:	8b 00                	mov    (%eax),%eax
80105cb0:	3b 45 08             	cmp    0x8(%ebp),%eax
80105cb3:	77 07                	ja     80105cbc <fetchstr+0x1a>
    return -1;
80105cb5:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105cba:	eb 48                	jmp    80105d04 <fetchstr+0x62>
  *pp = (char*)addr;
80105cbc:	8b 55 08             	mov    0x8(%ebp),%edx
80105cbf:	8b 45 0c             	mov    0xc(%ebp),%eax
80105cc2:	89 10                	mov    %edx,(%eax)
  ep = (char*)proc->sz;
80105cc4:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105cca:	8b 00                	mov    (%eax),%eax
80105ccc:	89 45 fc             	mov    %eax,-0x4(%ebp)
  for(s = *pp; s < ep; s++)
80105ccf:	8b 45 0c             	mov    0xc(%ebp),%eax
80105cd2:	8b 00                	mov    (%eax),%eax
80105cd4:	89 45 f8             	mov    %eax,-0x8(%ebp)
80105cd7:	eb 1e                	jmp    80105cf7 <fetchstr+0x55>
    if(*s == 0)
80105cd9:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105cdc:	0f b6 00             	movzbl (%eax),%eax
80105cdf:	84 c0                	test   %al,%al
80105ce1:	75 10                	jne    80105cf3 <fetchstr+0x51>
      return s - *pp;
80105ce3:	8b 55 f8             	mov    -0x8(%ebp),%edx
80105ce6:	8b 45 0c             	mov    0xc(%ebp),%eax
80105ce9:	8b 00                	mov    (%eax),%eax
80105ceb:	89 d1                	mov    %edx,%ecx
80105ced:	29 c1                	sub    %eax,%ecx
80105cef:	89 c8                	mov    %ecx,%eax
80105cf1:	eb 11                	jmp    80105d04 <fetchstr+0x62>

  if(addr >= proc->sz)
    return -1;
  *pp = (char*)addr;
  ep = (char*)proc->sz;
  for(s = *pp; s < ep; s++)
80105cf3:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
80105cf7:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105cfa:	3b 45 fc             	cmp    -0x4(%ebp),%eax
80105cfd:	72 da                	jb     80105cd9 <fetchstr+0x37>
    if(*s == 0)
      return s - *pp;
  return -1;
80105cff:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105d04:	c9                   	leave  
80105d05:	c3                   	ret    

80105d06 <argint>:

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
80105d06:	55                   	push   %ebp
80105d07:	89 e5                	mov    %esp,%ebp
80105d09:	83 ec 08             	sub    $0x8,%esp
  return fetchint(proc->tf->esp + 4 + 4*n, ip);
80105d0c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105d12:	8b 40 18             	mov    0x18(%eax),%eax
80105d15:	8b 50 44             	mov    0x44(%eax),%edx
80105d18:	8b 45 08             	mov    0x8(%ebp),%eax
80105d1b:	c1 e0 02             	shl    $0x2,%eax
80105d1e:	8d 04 02             	lea    (%edx,%eax,1),%eax
80105d21:	8d 50 04             	lea    0x4(%eax),%edx
80105d24:	8b 45 0c             	mov    0xc(%ebp),%eax
80105d27:	89 44 24 04          	mov    %eax,0x4(%esp)
80105d2b:	89 14 24             	mov    %edx,(%esp)
80105d2e:	e8 35 ff ff ff       	call   80105c68 <fetchint>
}
80105d33:	c9                   	leave  
80105d34:	c3                   	ret    

80105d35 <argptr>:
// Fetch the nth word-sized system call argument as a pointer
// to a block of memory of size n bytes.  Check that the pointer
// lies within the process address space.
int
argptr(int n, char **pp, int size)
{
80105d35:	55                   	push   %ebp
80105d36:	89 e5                	mov    %esp,%ebp
80105d38:	83 ec 18             	sub    $0x18,%esp
  int i;

  if(argint(n, &i) < 0)
80105d3b:	8d 45 fc             	lea    -0x4(%ebp),%eax
80105d3e:	89 44 24 04          	mov    %eax,0x4(%esp)
80105d42:	8b 45 08             	mov    0x8(%ebp),%eax
80105d45:	89 04 24             	mov    %eax,(%esp)
80105d48:	e8 b9 ff ff ff       	call   80105d06 <argint>
80105d4d:	85 c0                	test   %eax,%eax
80105d4f:	79 07                	jns    80105d58 <argptr+0x23>
    return -1;
80105d51:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105d56:	eb 3d                	jmp    80105d95 <argptr+0x60>
  if((uint)i >= proc->sz || (uint)i+size > proc->sz)
80105d58:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105d5b:	89 c2                	mov    %eax,%edx
80105d5d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105d63:	8b 00                	mov    (%eax),%eax
80105d65:	39 c2                	cmp    %eax,%edx
80105d67:	73 16                	jae    80105d7f <argptr+0x4a>
80105d69:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105d6c:	89 c2                	mov    %eax,%edx
80105d6e:	8b 45 10             	mov    0x10(%ebp),%eax
80105d71:	01 c2                	add    %eax,%edx
80105d73:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105d79:	8b 00                	mov    (%eax),%eax
80105d7b:	39 c2                	cmp    %eax,%edx
80105d7d:	76 07                	jbe    80105d86 <argptr+0x51>
    return -1;
80105d7f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105d84:	eb 0f                	jmp    80105d95 <argptr+0x60>
  *pp = (char*)i;
80105d86:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105d89:	89 c2                	mov    %eax,%edx
80105d8b:	8b 45 0c             	mov    0xc(%ebp),%eax
80105d8e:	89 10                	mov    %edx,(%eax)
  return 0;
80105d90:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105d95:	c9                   	leave  
80105d96:	c3                   	ret    

80105d97 <argstr>:
// Check that the pointer is valid and the string is nul-terminated.
// (There is no shared writable memory, so the string can't change
// between this check and being used by the kernel.)
int
argstr(int n, char **pp)
{
80105d97:	55                   	push   %ebp
80105d98:	89 e5                	mov    %esp,%ebp
80105d9a:	83 ec 18             	sub    $0x18,%esp
  int addr;
  if(argint(n, &addr) < 0)
80105d9d:	8d 45 fc             	lea    -0x4(%ebp),%eax
80105da0:	89 44 24 04          	mov    %eax,0x4(%esp)
80105da4:	8b 45 08             	mov    0x8(%ebp),%eax
80105da7:	89 04 24             	mov    %eax,(%esp)
80105daa:	e8 57 ff ff ff       	call   80105d06 <argint>
80105daf:	85 c0                	test   %eax,%eax
80105db1:	79 07                	jns    80105dba <argstr+0x23>
    return -1;
80105db3:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105db8:	eb 12                	jmp    80105dcc <argstr+0x35>
  return fetchstr(addr, pp);
80105dba:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105dbd:	8b 55 0c             	mov    0xc(%ebp),%edx
80105dc0:	89 54 24 04          	mov    %edx,0x4(%esp)
80105dc4:	89 04 24             	mov    %eax,(%esp)
80105dc7:	e8 d6 fe ff ff       	call   80105ca2 <fetchstr>
}
80105dcc:	c9                   	leave  
80105dcd:	c3                   	ret    

80105dce <syscall>:
  [SYS_syscallstats] = sys_syscallstats,
};

void
syscall(void)
{
80105dce:	55                   	push   %ebp
80105dcf:	89 e5                	mov    %esp,%ebp
80105dd1:	53                   	push   %ebx
80105dd2:	83 ec 24             	sub    $0x24,%esp
  int num;

  num = proc->tf->eax;
80105dd5:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105ddb:	8b 40 18             	mov    0x18(%eax),%eax
80105dde:	8b 40 1c             	mov    0x1c(%eax),%eax
80105de1:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
80105de4:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105de8:	7e 44                	jle    80105e2e <syscall+0x60>
80105dea:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105ded:	83 f8 21             	cmp    $0x21,%eax
80105df0:	77 3c                	ja     80105e2e <syscall+0x60>
80105df2:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105df5:	8b 04 85 40 c0 10 80 	mov    -0x7fef3fc0(,%eax,4),%eax
80105dfc:	85 c0                	test   %eax,%eax
80105dfe:	74 2e                	je     80105e2e <syscall+0x60>
    proc->tf->eax = syscalls[num]();
80105e00:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105e06:	8b 58 18             	mov    0x18(%eax),%ebx
80105e09:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105e0c:	8b 04 85 40 c0 10 80 	mov    -0x7fef3fc0(,%eax,4),%eax
80105e13:	ff d0                	call   *%eax
80105e15:	89 43 1c             	mov    %eax,0x1c(%ebx)
    syscallstats_data.counts[num]++;
80105e18:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105e1b:	8b 14 85 20 63 11 80 	mov    -0x7fee9ce0(,%eax,4),%edx
80105e22:	83 c2 01             	add    $0x1,%edx
80105e25:	89 14 85 20 63 11 80 	mov    %edx,-0x7fee9ce0(,%eax,4)
syscall(void)
{
  int num;

  num = proc->tf->eax;
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
80105e2c:	eb 3d                	jmp    80105e6b <syscall+0x9d>
    proc->tf->eax = syscalls[num]();
    syscallstats_data.counts[num]++;
  } else {
    cprintf("%d %s: unknown sys call %d\n",
            proc->pid, proc->name, num);
80105e2e:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  num = proc->tf->eax;
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
    proc->tf->eax = syscalls[num]();
    syscallstats_data.counts[num]++;
  } else {
    cprintf("%d %s: unknown sys call %d\n",
80105e34:	8d 48 6c             	lea    0x6c(%eax),%ecx
            proc->pid, proc->name, num);
80105e37:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  num = proc->tf->eax;
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
    proc->tf->eax = syscalls[num]();
    syscallstats_data.counts[num]++;
  } else {
    cprintf("%d %s: unknown sys call %d\n",
80105e3d:	8b 40 10             	mov    0x10(%eax),%eax
80105e40:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105e43:	89 54 24 0c          	mov    %edx,0xc(%esp)
80105e47:	89 4c 24 08          	mov    %ecx,0x8(%esp)
80105e4b:	89 44 24 04          	mov    %eax,0x4(%esp)
80105e4f:	c7 04 24 4e 94 10 80 	movl   $0x8010944e,(%esp)
80105e56:	e8 6e a8 ff ff       	call   801006c9 <cprintf>
            proc->pid, proc->name, num);
    proc->tf->eax = -1;
80105e5b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105e61:	8b 40 18             	mov    0x18(%eax),%eax
80105e64:	c7 40 1c ff ff ff ff 	movl   $0xffffffff,0x1c(%eax)
  }
}
80105e6b:	83 c4 24             	add    $0x24,%esp
80105e6e:	5b                   	pop    %ebx
80105e6f:	5d                   	pop    %ebp
80105e70:	c3                   	ret    
80105e71:	00 00                	add    %al,(%eax)
	...

80105e74 <argfd>:

// Fetch the nth word-sized system call argument as a file descriptor
// and return both the descriptor and the corresponding struct file.
static int
argfd(int n, int *pfd, struct file **pf)
{
80105e74:	55                   	push   %ebp
80105e75:	89 e5                	mov    %esp,%ebp
80105e77:	83 ec 28             	sub    $0x28,%esp
  int fd;
  struct file *f;

  if(argint(n, &fd) < 0)
80105e7a:	8d 45 f0             	lea    -0x10(%ebp),%eax
80105e7d:	89 44 24 04          	mov    %eax,0x4(%esp)
80105e81:	8b 45 08             	mov    0x8(%ebp),%eax
80105e84:	89 04 24             	mov    %eax,(%esp)
80105e87:	e8 7a fe ff ff       	call   80105d06 <argint>
80105e8c:	85 c0                	test   %eax,%eax
80105e8e:	79 07                	jns    80105e97 <argfd+0x23>
    return -1;
80105e90:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105e95:	eb 50                	jmp    80105ee7 <argfd+0x73>
  if(fd < 0 || fd >= NOFILE || (f=proc->ofile[fd]) == 0)
80105e97:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105e9a:	85 c0                	test   %eax,%eax
80105e9c:	78 21                	js     80105ebf <argfd+0x4b>
80105e9e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105ea1:	83 f8 0f             	cmp    $0xf,%eax
80105ea4:	7f 19                	jg     80105ebf <argfd+0x4b>
80105ea6:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105eac:	8b 55 f0             	mov    -0x10(%ebp),%edx
80105eaf:	83 c2 08             	add    $0x8,%edx
80105eb2:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80105eb6:	89 45 f4             	mov    %eax,-0xc(%ebp)
80105eb9:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105ebd:	75 07                	jne    80105ec6 <argfd+0x52>
    return -1;
80105ebf:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105ec4:	eb 21                	jmp    80105ee7 <argfd+0x73>
  if(pfd)
80105ec6:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
80105eca:	74 08                	je     80105ed4 <argfd+0x60>
    *pfd = fd;
80105ecc:	8b 55 f0             	mov    -0x10(%ebp),%edx
80105ecf:	8b 45 0c             	mov    0xc(%ebp),%eax
80105ed2:	89 10                	mov    %edx,(%eax)
  if(pf)
80105ed4:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105ed8:	74 08                	je     80105ee2 <argfd+0x6e>
    *pf = f;
80105eda:	8b 45 10             	mov    0x10(%ebp),%eax
80105edd:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105ee0:	89 10                	mov    %edx,(%eax)
  return 0;
80105ee2:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105ee7:	c9                   	leave  
80105ee8:	c3                   	ret    

80105ee9 <fdalloc>:

// Allocate a file descriptor for the given file.
// Takes over file reference from caller on success.
static int
fdalloc(struct file *f)
{
80105ee9:	55                   	push   %ebp
80105eea:	89 e5                	mov    %esp,%ebp
80105eec:	83 ec 10             	sub    $0x10,%esp
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
80105eef:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
80105ef6:	eb 30                	jmp    80105f28 <fdalloc+0x3f>
    if(proc->ofile[fd] == 0){
80105ef8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105efe:	8b 55 fc             	mov    -0x4(%ebp),%edx
80105f01:	83 c2 08             	add    $0x8,%edx
80105f04:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80105f08:	85 c0                	test   %eax,%eax
80105f0a:	75 18                	jne    80105f24 <fdalloc+0x3b>
      proc->ofile[fd] = f;
80105f0c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105f12:	8b 55 fc             	mov    -0x4(%ebp),%edx
80105f15:	8d 4a 08             	lea    0x8(%edx),%ecx
80105f18:	8b 55 08             	mov    0x8(%ebp),%edx
80105f1b:	89 54 88 08          	mov    %edx,0x8(%eax,%ecx,4)
      return fd;
80105f1f:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105f22:	eb 0f                	jmp    80105f33 <fdalloc+0x4a>
static int
fdalloc(struct file *f)
{
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
80105f24:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80105f28:	83 7d fc 0f          	cmpl   $0xf,-0x4(%ebp)
80105f2c:	7e ca                	jle    80105ef8 <fdalloc+0xf>
    if(proc->ofile[fd] == 0){
      proc->ofile[fd] = f;
      return fd;
    }
  }
  return -1;
80105f2e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105f33:	c9                   	leave  
80105f34:	c3                   	ret    

80105f35 <sys_dup>:

int
sys_dup(void)
{
80105f35:	55                   	push   %ebp
80105f36:	89 e5                	mov    %esp,%ebp
80105f38:	83 ec 28             	sub    $0x28,%esp
  struct file *f;
  int fd;

  if(argfd(0, 0, &f) < 0)
80105f3b:	8d 45 f0             	lea    -0x10(%ebp),%eax
80105f3e:	89 44 24 08          	mov    %eax,0x8(%esp)
80105f42:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80105f49:	00 
80105f4a:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80105f51:	e8 1e ff ff ff       	call   80105e74 <argfd>
80105f56:	85 c0                	test   %eax,%eax
80105f58:	79 07                	jns    80105f61 <sys_dup+0x2c>
    return -1;
80105f5a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105f5f:	eb 29                	jmp    80105f8a <sys_dup+0x55>
  if((fd=fdalloc(f)) < 0)
80105f61:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105f64:	89 04 24             	mov    %eax,(%esp)
80105f67:	e8 7d ff ff ff       	call   80105ee9 <fdalloc>
80105f6c:	89 45 f4             	mov    %eax,-0xc(%ebp)
80105f6f:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105f73:	79 07                	jns    80105f7c <sys_dup+0x47>
    return -1;
80105f75:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105f7a:	eb 0e                	jmp    80105f8a <sys_dup+0x55>
  filedup(f);
80105f7c:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105f7f:	89 04 24             	mov    %eax,(%esp)
80105f82:	e8 92 b7 ff ff       	call   80101719 <filedup>
  return fd;
80105f87:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
80105f8a:	c9                   	leave  
80105f8b:	c3                   	ret    

80105f8c <sys_read>:

int
sys_read(void)
{
80105f8c:	55                   	push   %ebp
80105f8d:	89 e5                	mov    %esp,%ebp
80105f8f:	83 ec 28             	sub    $0x28,%esp
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80105f92:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105f95:	89 44 24 08          	mov    %eax,0x8(%esp)
80105f99:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80105fa0:	00 
80105fa1:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80105fa8:	e8 c7 fe ff ff       	call   80105e74 <argfd>
80105fad:	85 c0                	test   %eax,%eax
80105faf:	78 35                	js     80105fe6 <sys_read+0x5a>
80105fb1:	8d 45 f0             	lea    -0x10(%ebp),%eax
80105fb4:	89 44 24 04          	mov    %eax,0x4(%esp)
80105fb8:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
80105fbf:	e8 42 fd ff ff       	call   80105d06 <argint>
80105fc4:	85 c0                	test   %eax,%eax
80105fc6:	78 1e                	js     80105fe6 <sys_read+0x5a>
80105fc8:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105fcb:	89 44 24 08          	mov    %eax,0x8(%esp)
80105fcf:	8d 45 ec             	lea    -0x14(%ebp),%eax
80105fd2:	89 44 24 04          	mov    %eax,0x4(%esp)
80105fd6:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80105fdd:	e8 53 fd ff ff       	call   80105d35 <argptr>
80105fe2:	85 c0                	test   %eax,%eax
80105fe4:	79 07                	jns    80105fed <sys_read+0x61>
    return -1;
80105fe6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105feb:	eb 19                	jmp    80106006 <sys_read+0x7a>
  return fileread(f, p, n);
80105fed:	8b 4d f0             	mov    -0x10(%ebp),%ecx
80105ff0:	8b 55 ec             	mov    -0x14(%ebp),%edx
80105ff3:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105ff6:	89 4c 24 08          	mov    %ecx,0x8(%esp)
80105ffa:	89 54 24 04          	mov    %edx,0x4(%esp)
80105ffe:	89 04 24             	mov    %eax,(%esp)
80106001:	e8 80 b8 ff ff       	call   80101886 <fileread>
}
80106006:	c9                   	leave  
80106007:	c3                   	ret    

80106008 <sys_write>:

int
sys_write(void)
{
80106008:	55                   	push   %ebp
80106009:	89 e5                	mov    %esp,%ebp
8010600b:	83 ec 28             	sub    $0x28,%esp
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
8010600e:	8d 45 f4             	lea    -0xc(%ebp),%eax
80106011:	89 44 24 08          	mov    %eax,0x8(%esp)
80106015:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
8010601c:	00 
8010601d:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106024:	e8 4b fe ff ff       	call   80105e74 <argfd>
80106029:	85 c0                	test   %eax,%eax
8010602b:	78 35                	js     80106062 <sys_write+0x5a>
8010602d:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106030:	89 44 24 04          	mov    %eax,0x4(%esp)
80106034:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
8010603b:	e8 c6 fc ff ff       	call   80105d06 <argint>
80106040:	85 c0                	test   %eax,%eax
80106042:	78 1e                	js     80106062 <sys_write+0x5a>
80106044:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106047:	89 44 24 08          	mov    %eax,0x8(%esp)
8010604b:	8d 45 ec             	lea    -0x14(%ebp),%eax
8010604e:	89 44 24 04          	mov    %eax,0x4(%esp)
80106052:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80106059:	e8 d7 fc ff ff       	call   80105d35 <argptr>
8010605e:	85 c0                	test   %eax,%eax
80106060:	79 07                	jns    80106069 <sys_write+0x61>
    return -1;
80106062:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106067:	eb 19                	jmp    80106082 <sys_write+0x7a>
  return filewrite(f, p, n);
80106069:	8b 4d f0             	mov    -0x10(%ebp),%ecx
8010606c:	8b 55 ec             	mov    -0x14(%ebp),%edx
8010606f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106072:	89 4c 24 08          	mov    %ecx,0x8(%esp)
80106076:	89 54 24 04          	mov    %edx,0x4(%esp)
8010607a:	89 04 24             	mov    %eax,(%esp)
8010607d:	e8 c0 b8 ff ff       	call   80101942 <filewrite>
}
80106082:	c9                   	leave  
80106083:	c3                   	ret    

80106084 <sys_close>:

int
sys_close(void)
{
80106084:	55                   	push   %ebp
80106085:	89 e5                	mov    %esp,%ebp
80106087:	83 ec 28             	sub    $0x28,%esp
  int fd;
  struct file *f;

  if(argfd(0, &fd, &f) < 0)
8010608a:	8d 45 f0             	lea    -0x10(%ebp),%eax
8010608d:	89 44 24 08          	mov    %eax,0x8(%esp)
80106091:	8d 45 f4             	lea    -0xc(%ebp),%eax
80106094:	89 44 24 04          	mov    %eax,0x4(%esp)
80106098:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
8010609f:	e8 d0 fd ff ff       	call   80105e74 <argfd>
801060a4:	85 c0                	test   %eax,%eax
801060a6:	79 07                	jns    801060af <sys_close+0x2b>
    return -1;
801060a8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801060ad:	eb 24                	jmp    801060d3 <sys_close+0x4f>
  proc->ofile[fd] = 0;
801060af:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801060b5:	8b 55 f4             	mov    -0xc(%ebp),%edx
801060b8:	83 c2 08             	add    $0x8,%edx
801060bb:	c7 44 90 08 00 00 00 	movl   $0x0,0x8(%eax,%edx,4)
801060c2:	00 
  fileclose(f);
801060c3:	8b 45 f0             	mov    -0x10(%ebp),%eax
801060c6:	89 04 24             	mov    %eax,(%esp)
801060c9:	e8 93 b6 ff ff       	call   80101761 <fileclose>
  return 0;
801060ce:	b8 00 00 00 00       	mov    $0x0,%eax
}
801060d3:	c9                   	leave  
801060d4:	c3                   	ret    

801060d5 <sys_fstat>:

int
sys_fstat(void)
{
801060d5:	55                   	push   %ebp
801060d6:	89 e5                	mov    %esp,%ebp
801060d8:	83 ec 28             	sub    $0x28,%esp
  struct file *f;
  struct stat *st;

  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
801060db:	8d 45 f4             	lea    -0xc(%ebp),%eax
801060de:	89 44 24 08          	mov    %eax,0x8(%esp)
801060e2:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801060e9:	00 
801060ea:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
801060f1:	e8 7e fd ff ff       	call   80105e74 <argfd>
801060f6:	85 c0                	test   %eax,%eax
801060f8:	78 1f                	js     80106119 <sys_fstat+0x44>
801060fa:	8d 45 f0             	lea    -0x10(%ebp),%eax
801060fd:	c7 44 24 08 14 00 00 	movl   $0x14,0x8(%esp)
80106104:	00 
80106105:	89 44 24 04          	mov    %eax,0x4(%esp)
80106109:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80106110:	e8 20 fc ff ff       	call   80105d35 <argptr>
80106115:	85 c0                	test   %eax,%eax
80106117:	79 07                	jns    80106120 <sys_fstat+0x4b>
    return -1;
80106119:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010611e:	eb 12                	jmp    80106132 <sys_fstat+0x5d>
  return filestat(f, st);
80106120:	8b 55 f0             	mov    -0x10(%ebp),%edx
80106123:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106126:	89 54 24 04          	mov    %edx,0x4(%esp)
8010612a:	89 04 24             	mov    %eax,(%esp)
8010612d:	e8 05 b7 ff ff       	call   80101837 <filestat>
}
80106132:	c9                   	leave  
80106133:	c3                   	ret    

80106134 <sys_link>:

// Create the path new as a link to the same inode as old.
int
sys_link(void)
{
80106134:	55                   	push   %ebp
80106135:	89 e5                	mov    %esp,%ebp
80106137:	83 ec 38             	sub    $0x38,%esp
  char name[DIRSIZ], *new, *old;
  struct inode *dp, *ip;

  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
8010613a:	8d 45 d8             	lea    -0x28(%ebp),%eax
8010613d:	89 44 24 04          	mov    %eax,0x4(%esp)
80106141:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106148:	e8 4a fc ff ff       	call   80105d97 <argstr>
8010614d:	85 c0                	test   %eax,%eax
8010614f:	78 17                	js     80106168 <sys_link+0x34>
80106151:	8d 45 dc             	lea    -0x24(%ebp),%eax
80106154:	89 44 24 04          	mov    %eax,0x4(%esp)
80106158:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
8010615f:	e8 33 fc ff ff       	call   80105d97 <argstr>
80106164:	85 c0                	test   %eax,%eax
80106166:	79 0a                	jns    80106172 <sys_link+0x3e>
    return -1;
80106168:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010616d:	e9 41 01 00 00       	jmp    801062b3 <sys_link+0x17f>

  begin_op();
80106172:	e8 f3 d8 ff ff       	call   80103a6a <begin_op>
  if((ip = namei(old)) == 0){
80106177:	8b 45 d8             	mov    -0x28(%ebp),%eax
8010617a:	89 04 24             	mov    %eax,(%esp)
8010617d:	e8 9d ca ff ff       	call   80102c1f <namei>
80106182:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106185:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106189:	75 0f                	jne    8010619a <sys_link+0x66>
    end_op();
8010618b:	e8 5c d9 ff ff       	call   80103aec <end_op>
    return -1;
80106190:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106195:	e9 19 01 00 00       	jmp    801062b3 <sys_link+0x17f>
  }

  ilock(ip);
8010619a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010619d:	89 04 24             	mov    %eax,(%esp)
801061a0:	e8 cc be ff ff       	call   80102071 <ilock>
  if(ip->type == T_DIR){
801061a5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801061a8:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801061ac:	66 83 f8 01          	cmp    $0x1,%ax
801061b0:	75 1a                	jne    801061cc <sys_link+0x98>
    iunlockput(ip);
801061b2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801061b5:	89 04 24             	mov    %eax,(%esp)
801061b8:	e8 41 c1 ff ff       	call   801022fe <iunlockput>
    end_op();
801061bd:	e8 2a d9 ff ff       	call   80103aec <end_op>
    return -1;
801061c2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801061c7:	e9 e7 00 00 00       	jmp    801062b3 <sys_link+0x17f>
  }

  ip->nlink++;
801061cc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801061cf:	0f b7 40 16          	movzwl 0x16(%eax),%eax
801061d3:	8d 50 01             	lea    0x1(%eax),%edx
801061d6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801061d9:	66 89 50 16          	mov    %dx,0x16(%eax)
  iupdate(ip);
801061dd:	8b 45 f4             	mov    -0xc(%ebp),%eax
801061e0:	89 04 24             	mov    %eax,(%esp)
801061e3:	e8 c3 bc ff ff       	call   80101eab <iupdate>
  iunlock(ip);
801061e8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801061eb:	89 04 24             	mov    %eax,(%esp)
801061ee:	e8 d5 bf ff ff       	call   801021c8 <iunlock>

  if((dp = nameiparent(new, name)) == 0)
801061f3:	8b 45 dc             	mov    -0x24(%ebp),%eax
801061f6:	8d 55 e2             	lea    -0x1e(%ebp),%edx
801061f9:	89 54 24 04          	mov    %edx,0x4(%esp)
801061fd:	89 04 24             	mov    %eax,(%esp)
80106200:	e8 3c ca ff ff       	call   80102c41 <nameiparent>
80106205:	89 45 f0             	mov    %eax,-0x10(%ebp)
80106208:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010620c:	74 68                	je     80106276 <sys_link+0x142>
    goto bad;
  ilock(dp);
8010620e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106211:	89 04 24             	mov    %eax,(%esp)
80106214:	e8 58 be ff ff       	call   80102071 <ilock>
  if(dp->dev != ip->dev || dirlink(dp, name, ip->inum) < 0){
80106219:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010621c:	8b 10                	mov    (%eax),%edx
8010621e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106221:	8b 00                	mov    (%eax),%eax
80106223:	39 c2                	cmp    %eax,%edx
80106225:	75 20                	jne    80106247 <sys_link+0x113>
80106227:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010622a:	8b 40 04             	mov    0x4(%eax),%eax
8010622d:	89 44 24 08          	mov    %eax,0x8(%esp)
80106231:	8d 45 e2             	lea    -0x1e(%ebp),%eax
80106234:	89 44 24 04          	mov    %eax,0x4(%esp)
80106238:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010623b:	89 04 24             	mov    %eax,(%esp)
8010623e:	e8 1b c7 ff ff       	call   8010295e <dirlink>
80106243:	85 c0                	test   %eax,%eax
80106245:	79 0d                	jns    80106254 <sys_link+0x120>
    iunlockput(dp);
80106247:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010624a:	89 04 24             	mov    %eax,(%esp)
8010624d:	e8 ac c0 ff ff       	call   801022fe <iunlockput>
    goto bad;
80106252:	eb 23                	jmp    80106277 <sys_link+0x143>
  }
  iunlockput(dp);
80106254:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106257:	89 04 24             	mov    %eax,(%esp)
8010625a:	e8 9f c0 ff ff       	call   801022fe <iunlockput>
  iput(ip);
8010625f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106262:	89 04 24             	mov    %eax,(%esp)
80106265:	e8 c3 bf ff ff       	call   8010222d <iput>

  end_op();
8010626a:	e8 7d d8 ff ff       	call   80103aec <end_op>

  return 0;
8010626f:	b8 00 00 00 00       	mov    $0x0,%eax
80106274:	eb 3d                	jmp    801062b3 <sys_link+0x17f>
  ip->nlink++;
  iupdate(ip);
  iunlock(ip);

  if((dp = nameiparent(new, name)) == 0)
    goto bad;
80106276:	90                   	nop
  end_op();

  return 0;

bad:
  ilock(ip);
80106277:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010627a:	89 04 24             	mov    %eax,(%esp)
8010627d:	e8 ef bd ff ff       	call   80102071 <ilock>
  ip->nlink--;
80106282:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106285:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80106289:	8d 50 ff             	lea    -0x1(%eax),%edx
8010628c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010628f:	66 89 50 16          	mov    %dx,0x16(%eax)
  iupdate(ip);
80106293:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106296:	89 04 24             	mov    %eax,(%esp)
80106299:	e8 0d bc ff ff       	call   80101eab <iupdate>
  iunlockput(ip);
8010629e:	8b 45 f4             	mov    -0xc(%ebp),%eax
801062a1:	89 04 24             	mov    %eax,(%esp)
801062a4:	e8 55 c0 ff ff       	call   801022fe <iunlockput>
  end_op();
801062a9:	e8 3e d8 ff ff       	call   80103aec <end_op>
  return -1;
801062ae:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801062b3:	c9                   	leave  
801062b4:	c3                   	ret    

801062b5 <isdirempty>:

// Is the directory dp empty except for "." and ".." ?
static int
isdirempty(struct inode *dp)
{
801062b5:	55                   	push   %ebp
801062b6:	89 e5                	mov    %esp,%ebp
801062b8:	83 ec 38             	sub    $0x38,%esp
  int off;
  struct dirent de;

  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
801062bb:	c7 45 f4 20 00 00 00 	movl   $0x20,-0xc(%ebp)
801062c2:	eb 4b                	jmp    8010630f <isdirempty+0x5a>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801062c4:	8b 55 f4             	mov    -0xc(%ebp),%edx
801062c7:	8d 45 e4             	lea    -0x1c(%ebp),%eax
801062ca:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
801062d1:	00 
801062d2:	89 54 24 08          	mov    %edx,0x8(%esp)
801062d6:	89 44 24 04          	mov    %eax,0x4(%esp)
801062da:	8b 45 08             	mov    0x8(%ebp),%eax
801062dd:	89 04 24             	mov    %eax,(%esp)
801062e0:	e8 8b c2 ff ff       	call   80102570 <readi>
801062e5:	83 f8 10             	cmp    $0x10,%eax
801062e8:	74 0c                	je     801062f6 <isdirempty+0x41>
      panic("isdirempty: readi");
801062ea:	c7 04 24 6a 94 10 80 	movl   $0x8010946a,(%esp)
801062f1:	e8 73 a5 ff ff       	call   80100869 <panic>
    if(de.inum != 0)
801062f6:	0f b7 45 e4          	movzwl -0x1c(%ebp),%eax
801062fa:	66 85 c0             	test   %ax,%ax
801062fd:	74 07                	je     80106306 <isdirempty+0x51>
      return 0;
801062ff:	b8 00 00 00 00       	mov    $0x0,%eax
80106304:	eb 1b                	jmp    80106321 <isdirempty+0x6c>
isdirempty(struct inode *dp)
{
  int off;
  struct dirent de;

  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
80106306:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106309:	83 c0 10             	add    $0x10,%eax
8010630c:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010630f:	8b 55 f4             	mov    -0xc(%ebp),%edx
80106312:	8b 45 08             	mov    0x8(%ebp),%eax
80106315:	8b 40 18             	mov    0x18(%eax),%eax
80106318:	39 c2                	cmp    %eax,%edx
8010631a:	72 a8                	jb     801062c4 <isdirempty+0xf>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("isdirempty: readi");
    if(de.inum != 0)
      return 0;
  }
  return 1;
8010631c:	b8 01 00 00 00       	mov    $0x1,%eax
}
80106321:	c9                   	leave  
80106322:	c3                   	ret    

80106323 <sys_unlink>:

//PAGEBREAK!
int
sys_unlink(void)
{
80106323:	55                   	push   %ebp
80106324:	89 e5                	mov    %esp,%ebp
80106326:	83 ec 48             	sub    $0x48,%esp
  struct inode *ip, *dp;
  struct dirent de;
  char name[DIRSIZ], *path;
  uint off;

  if(argstr(0, &path) < 0)
80106329:	8d 45 cc             	lea    -0x34(%ebp),%eax
8010632c:	89 44 24 04          	mov    %eax,0x4(%esp)
80106330:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106337:	e8 5b fa ff ff       	call   80105d97 <argstr>
8010633c:	85 c0                	test   %eax,%eax
8010633e:	79 0a                	jns    8010634a <sys_unlink+0x27>
    return -1;
80106340:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106345:	e9 af 01 00 00       	jmp    801064f9 <sys_unlink+0x1d6>

  begin_op();
8010634a:	e8 1b d7 ff ff       	call   80103a6a <begin_op>
  if((dp = nameiparent(path, name)) == 0){
8010634f:	8b 45 cc             	mov    -0x34(%ebp),%eax
80106352:	8d 55 d2             	lea    -0x2e(%ebp),%edx
80106355:	89 54 24 04          	mov    %edx,0x4(%esp)
80106359:	89 04 24             	mov    %eax,(%esp)
8010635c:	e8 e0 c8 ff ff       	call   80102c41 <nameiparent>
80106361:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106364:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106368:	75 0f                	jne    80106379 <sys_unlink+0x56>
    end_op();
8010636a:	e8 7d d7 ff ff       	call   80103aec <end_op>
    return -1;
8010636f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106374:	e9 80 01 00 00       	jmp    801064f9 <sys_unlink+0x1d6>
  }

  ilock(dp);
80106379:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010637c:	89 04 24             	mov    %eax,(%esp)
8010637f:	e8 ed bc ff ff       	call   80102071 <ilock>

  // Cannot unlink "." or "..".
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
80106384:	c7 44 24 04 7c 94 10 	movl   $0x8010947c,0x4(%esp)
8010638b:	80 
8010638c:	8d 45 d2             	lea    -0x2e(%ebp),%eax
8010638f:	89 04 24             	mov    %eax,(%esp)
80106392:	e8 dd c4 ff ff       	call   80102874 <namecmp>
80106397:	85 c0                	test   %eax,%eax
80106399:	0f 84 45 01 00 00    	je     801064e4 <sys_unlink+0x1c1>
8010639f:	c7 44 24 04 7e 94 10 	movl   $0x8010947e,0x4(%esp)
801063a6:	80 
801063a7:	8d 45 d2             	lea    -0x2e(%ebp),%eax
801063aa:	89 04 24             	mov    %eax,(%esp)
801063ad:	e8 c2 c4 ff ff       	call   80102874 <namecmp>
801063b2:	85 c0                	test   %eax,%eax
801063b4:	0f 84 2a 01 00 00    	je     801064e4 <sys_unlink+0x1c1>
    goto bad;

  if((ip = dirlookup(dp, name, &off)) == 0)
801063ba:	8d 45 c8             	lea    -0x38(%ebp),%eax
801063bd:	89 44 24 08          	mov    %eax,0x8(%esp)
801063c1:	8d 45 d2             	lea    -0x2e(%ebp),%eax
801063c4:	89 44 24 04          	mov    %eax,0x4(%esp)
801063c8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801063cb:	89 04 24             	mov    %eax,(%esp)
801063ce:	e8 c3 c4 ff ff       	call   80102896 <dirlookup>
801063d3:	89 45 f0             	mov    %eax,-0x10(%ebp)
801063d6:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801063da:	0f 84 03 01 00 00    	je     801064e3 <sys_unlink+0x1c0>
    goto bad;
  ilock(ip);
801063e0:	8b 45 f0             	mov    -0x10(%ebp),%eax
801063e3:	89 04 24             	mov    %eax,(%esp)
801063e6:	e8 86 bc ff ff       	call   80102071 <ilock>

  if(ip->nlink < 1)
801063eb:	8b 45 f0             	mov    -0x10(%ebp),%eax
801063ee:	0f b7 40 16          	movzwl 0x16(%eax),%eax
801063f2:	66 85 c0             	test   %ax,%ax
801063f5:	7f 0c                	jg     80106403 <sys_unlink+0xe0>
    panic("unlink: nlink < 1");
801063f7:	c7 04 24 81 94 10 80 	movl   $0x80109481,(%esp)
801063fe:	e8 66 a4 ff ff       	call   80100869 <panic>
  if(ip->type == T_DIR && !isdirempty(ip)){
80106403:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106406:	0f b7 40 10          	movzwl 0x10(%eax),%eax
8010640a:	66 83 f8 01          	cmp    $0x1,%ax
8010640e:	75 1f                	jne    8010642f <sys_unlink+0x10c>
80106410:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106413:	89 04 24             	mov    %eax,(%esp)
80106416:	e8 9a fe ff ff       	call   801062b5 <isdirempty>
8010641b:	85 c0                	test   %eax,%eax
8010641d:	75 10                	jne    8010642f <sys_unlink+0x10c>
    iunlockput(ip);
8010641f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106422:	89 04 24             	mov    %eax,(%esp)
80106425:	e8 d4 be ff ff       	call   801022fe <iunlockput>
    goto bad;
8010642a:	e9 b5 00 00 00       	jmp    801064e4 <sys_unlink+0x1c1>
  }

  memset(&de, 0, sizeof(de));
8010642f:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
80106436:	00 
80106437:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
8010643e:	00 
8010643f:	8d 45 e0             	lea    -0x20(%ebp),%eax
80106442:	89 04 24             	mov    %eax,(%esp)
80106445:	e8 60 f5 ff ff       	call   801059aa <memset>
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
8010644a:	8b 55 c8             	mov    -0x38(%ebp),%edx
8010644d:	8d 45 e0             	lea    -0x20(%ebp),%eax
80106450:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
80106457:	00 
80106458:	89 54 24 08          	mov    %edx,0x8(%esp)
8010645c:	89 44 24 04          	mov    %eax,0x4(%esp)
80106460:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106463:	89 04 24             	mov    %eax,(%esp)
80106466:	e8 71 c2 ff ff       	call   801026dc <writei>
8010646b:	83 f8 10             	cmp    $0x10,%eax
8010646e:	74 0c                	je     8010647c <sys_unlink+0x159>
    panic("unlink: writei");
80106470:	c7 04 24 93 94 10 80 	movl   $0x80109493,(%esp)
80106477:	e8 ed a3 ff ff       	call   80100869 <panic>
  if(ip->type == T_DIR){
8010647c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010647f:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80106483:	66 83 f8 01          	cmp    $0x1,%ax
80106487:	75 1c                	jne    801064a5 <sys_unlink+0x182>
    dp->nlink--;
80106489:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010648c:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80106490:	8d 50 ff             	lea    -0x1(%eax),%edx
80106493:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106496:	66 89 50 16          	mov    %dx,0x16(%eax)
    iupdate(dp);
8010649a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010649d:	89 04 24             	mov    %eax,(%esp)
801064a0:	e8 06 ba ff ff       	call   80101eab <iupdate>
  }
  iunlockput(dp);
801064a5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801064a8:	89 04 24             	mov    %eax,(%esp)
801064ab:	e8 4e be ff ff       	call   801022fe <iunlockput>

  ip->nlink--;
801064b0:	8b 45 f0             	mov    -0x10(%ebp),%eax
801064b3:	0f b7 40 16          	movzwl 0x16(%eax),%eax
801064b7:	8d 50 ff             	lea    -0x1(%eax),%edx
801064ba:	8b 45 f0             	mov    -0x10(%ebp),%eax
801064bd:	66 89 50 16          	mov    %dx,0x16(%eax)
  iupdate(ip);
801064c1:	8b 45 f0             	mov    -0x10(%ebp),%eax
801064c4:	89 04 24             	mov    %eax,(%esp)
801064c7:	e8 df b9 ff ff       	call   80101eab <iupdate>
  iunlockput(ip);
801064cc:	8b 45 f0             	mov    -0x10(%ebp),%eax
801064cf:	89 04 24             	mov    %eax,(%esp)
801064d2:	e8 27 be ff ff       	call   801022fe <iunlockput>

  end_op();
801064d7:	e8 10 d6 ff ff       	call   80103aec <end_op>

  return 0;
801064dc:	b8 00 00 00 00       	mov    $0x0,%eax
801064e1:	eb 16                	jmp    801064f9 <sys_unlink+0x1d6>
  // Cannot unlink "." or "..".
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
    goto bad;

  if((ip = dirlookup(dp, name, &off)) == 0)
    goto bad;
801064e3:	90                   	nop
  end_op();

  return 0;

bad:
  iunlockput(dp);
801064e4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801064e7:	89 04 24             	mov    %eax,(%esp)
801064ea:	e8 0f be ff ff       	call   801022fe <iunlockput>
  end_op();
801064ef:	e8 f8 d5 ff ff       	call   80103aec <end_op>
  return -1;
801064f4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801064f9:	c9                   	leave  
801064fa:	c3                   	ret    

801064fb <create>:

static struct inode*
create(char *path, short type, short major, short minor)
{
801064fb:	55                   	push   %ebp
801064fc:	89 e5                	mov    %esp,%ebp
801064fe:	83 ec 48             	sub    $0x48,%esp
80106501:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80106504:	8b 55 10             	mov    0x10(%ebp),%edx
80106507:	8b 45 14             	mov    0x14(%ebp),%eax
8010650a:	66 89 4d d4          	mov    %cx,-0x2c(%ebp)
8010650e:	66 89 55 d0          	mov    %dx,-0x30(%ebp)
80106512:	66 89 45 cc          	mov    %ax,-0x34(%ebp)
  uint off;
  struct inode *ip, *dp;
  char name[DIRSIZ];

  if((dp = nameiparent(path, name)) == 0)
80106516:	8d 45 de             	lea    -0x22(%ebp),%eax
80106519:	89 44 24 04          	mov    %eax,0x4(%esp)
8010651d:	8b 45 08             	mov    0x8(%ebp),%eax
80106520:	89 04 24             	mov    %eax,(%esp)
80106523:	e8 19 c7 ff ff       	call   80102c41 <nameiparent>
80106528:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010652b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010652f:	75 0a                	jne    8010653b <create+0x40>
    return 0;
80106531:	b8 00 00 00 00       	mov    $0x0,%eax
80106536:	e9 7e 01 00 00       	jmp    801066b9 <create+0x1be>
  ilock(dp);
8010653b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010653e:	89 04 24             	mov    %eax,(%esp)
80106541:	e8 2b bb ff ff       	call   80102071 <ilock>

  if((ip = dirlookup(dp, name, &off)) != 0){
80106546:	8d 45 ec             	lea    -0x14(%ebp),%eax
80106549:	89 44 24 08          	mov    %eax,0x8(%esp)
8010654d:	8d 45 de             	lea    -0x22(%ebp),%eax
80106550:	89 44 24 04          	mov    %eax,0x4(%esp)
80106554:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106557:	89 04 24             	mov    %eax,(%esp)
8010655a:	e8 37 c3 ff ff       	call   80102896 <dirlookup>
8010655f:	89 45 f0             	mov    %eax,-0x10(%ebp)
80106562:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80106566:	74 47                	je     801065af <create+0xb4>
    iunlockput(dp);
80106568:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010656b:	89 04 24             	mov    %eax,(%esp)
8010656e:	e8 8b bd ff ff       	call   801022fe <iunlockput>
    ilock(ip);
80106573:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106576:	89 04 24             	mov    %eax,(%esp)
80106579:	e8 f3 ba ff ff       	call   80102071 <ilock>
    if(type == T_FILE && ip->type == T_FILE)
8010657e:	66 83 7d d4 02       	cmpw   $0x2,-0x2c(%ebp)
80106583:	75 15                	jne    8010659a <create+0x9f>
80106585:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106588:	0f b7 40 10          	movzwl 0x10(%eax),%eax
8010658c:	66 83 f8 02          	cmp    $0x2,%ax
80106590:	75 08                	jne    8010659a <create+0x9f>
      return ip;
80106592:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106595:	e9 1f 01 00 00       	jmp    801066b9 <create+0x1be>
    iunlockput(ip);
8010659a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010659d:	89 04 24             	mov    %eax,(%esp)
801065a0:	e8 59 bd ff ff       	call   801022fe <iunlockput>
    return 0;
801065a5:	b8 00 00 00 00       	mov    $0x0,%eax
801065aa:	e9 0a 01 00 00       	jmp    801066b9 <create+0x1be>
  }

  if((ip = ialloc(dp->dev, type)) == 0)
801065af:	0f bf 55 d4          	movswl -0x2c(%ebp),%edx
801065b3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801065b6:	8b 00                	mov    (%eax),%eax
801065b8:	89 54 24 04          	mov    %edx,0x4(%esp)
801065bc:	89 04 24             	mov    %eax,(%esp)
801065bf:	e8 13 b8 ff ff       	call   80101dd7 <ialloc>
801065c4:	89 45 f0             	mov    %eax,-0x10(%ebp)
801065c7:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801065cb:	75 0c                	jne    801065d9 <create+0xde>
    panic("create: ialloc");
801065cd:	c7 04 24 a2 94 10 80 	movl   $0x801094a2,(%esp)
801065d4:	e8 90 a2 ff ff       	call   80100869 <panic>

  ilock(ip);
801065d9:	8b 45 f0             	mov    -0x10(%ebp),%eax
801065dc:	89 04 24             	mov    %eax,(%esp)
801065df:	e8 8d ba ff ff       	call   80102071 <ilock>
  ip->major = major;
801065e4:	8b 45 f0             	mov    -0x10(%ebp),%eax
801065e7:	0f b7 55 d0          	movzwl -0x30(%ebp),%edx
801065eb:	66 89 50 12          	mov    %dx,0x12(%eax)
  ip->minor = minor;
801065ef:	8b 45 f0             	mov    -0x10(%ebp),%eax
801065f2:	0f b7 55 cc          	movzwl -0x34(%ebp),%edx
801065f6:	66 89 50 14          	mov    %dx,0x14(%eax)
  ip->nlink = 1;
801065fa:	8b 45 f0             	mov    -0x10(%ebp),%eax
801065fd:	66 c7 40 16 01 00    	movw   $0x1,0x16(%eax)
  iupdate(ip);
80106603:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106606:	89 04 24             	mov    %eax,(%esp)
80106609:	e8 9d b8 ff ff       	call   80101eab <iupdate>

  if(type == T_DIR){  // Create . and .. entries.
8010660e:	66 83 7d d4 01       	cmpw   $0x1,-0x2c(%ebp)
80106613:	75 6a                	jne    8010667f <create+0x184>
    dp->nlink++;  // for ".."
80106615:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106618:	0f b7 40 16          	movzwl 0x16(%eax),%eax
8010661c:	8d 50 01             	lea    0x1(%eax),%edx
8010661f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106622:	66 89 50 16          	mov    %dx,0x16(%eax)
    iupdate(dp);
80106626:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106629:	89 04 24             	mov    %eax,(%esp)
8010662c:	e8 7a b8 ff ff       	call   80101eab <iupdate>
    // No ip->nlink++ for ".": avoid cyclic ref count.
    if(dirlink(ip, ".", ip->inum) < 0 || dirlink(ip, "..", dp->inum) < 0)
80106631:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106634:	8b 40 04             	mov    0x4(%eax),%eax
80106637:	89 44 24 08          	mov    %eax,0x8(%esp)
8010663b:	c7 44 24 04 7c 94 10 	movl   $0x8010947c,0x4(%esp)
80106642:	80 
80106643:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106646:	89 04 24             	mov    %eax,(%esp)
80106649:	e8 10 c3 ff ff       	call   8010295e <dirlink>
8010664e:	85 c0                	test   %eax,%eax
80106650:	78 21                	js     80106673 <create+0x178>
80106652:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106655:	8b 40 04             	mov    0x4(%eax),%eax
80106658:	89 44 24 08          	mov    %eax,0x8(%esp)
8010665c:	c7 44 24 04 7e 94 10 	movl   $0x8010947e,0x4(%esp)
80106663:	80 
80106664:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106667:	89 04 24             	mov    %eax,(%esp)
8010666a:	e8 ef c2 ff ff       	call   8010295e <dirlink>
8010666f:	85 c0                	test   %eax,%eax
80106671:	79 0c                	jns    8010667f <create+0x184>
      panic("create dots");
80106673:	c7 04 24 b1 94 10 80 	movl   $0x801094b1,(%esp)
8010667a:	e8 ea a1 ff ff       	call   80100869 <panic>
  }

  if(dirlink(dp, name, ip->inum) < 0)
8010667f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106682:	8b 40 04             	mov    0x4(%eax),%eax
80106685:	89 44 24 08          	mov    %eax,0x8(%esp)
80106689:	8d 45 de             	lea    -0x22(%ebp),%eax
8010668c:	89 44 24 04          	mov    %eax,0x4(%esp)
80106690:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106693:	89 04 24             	mov    %eax,(%esp)
80106696:	e8 c3 c2 ff ff       	call   8010295e <dirlink>
8010669b:	85 c0                	test   %eax,%eax
8010669d:	79 0c                	jns    801066ab <create+0x1b0>
    panic("create: dirlink");
8010669f:	c7 04 24 bd 94 10 80 	movl   $0x801094bd,(%esp)
801066a6:	e8 be a1 ff ff       	call   80100869 <panic>

  iunlockput(dp);
801066ab:	8b 45 f4             	mov    -0xc(%ebp),%eax
801066ae:	89 04 24             	mov    %eax,(%esp)
801066b1:	e8 48 bc ff ff       	call   801022fe <iunlockput>

  return ip;
801066b6:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
801066b9:	c9                   	leave  
801066ba:	c3                   	ret    

801066bb <sys_open>:

int
sys_open(void)
{
801066bb:	55                   	push   %ebp
801066bc:	89 e5                	mov    %esp,%ebp
801066be:	83 ec 38             	sub    $0x38,%esp
  char *path;
  int fd, omode;
  struct file *f;
  struct inode *ip;

  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
801066c1:	8d 45 e8             	lea    -0x18(%ebp),%eax
801066c4:	89 44 24 04          	mov    %eax,0x4(%esp)
801066c8:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
801066cf:	e8 c3 f6 ff ff       	call   80105d97 <argstr>
801066d4:	85 c0                	test   %eax,%eax
801066d6:	78 17                	js     801066ef <sys_open+0x34>
801066d8:	8d 45 e4             	lea    -0x1c(%ebp),%eax
801066db:	89 44 24 04          	mov    %eax,0x4(%esp)
801066df:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
801066e6:	e8 1b f6 ff ff       	call   80105d06 <argint>
801066eb:	85 c0                	test   %eax,%eax
801066ed:	79 0a                	jns    801066f9 <sys_open+0x3e>
    return -1;
801066ef:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801066f4:	e9 5a 01 00 00       	jmp    80106853 <sys_open+0x198>

  begin_op();
801066f9:	e8 6c d3 ff ff       	call   80103a6a <begin_op>

  if(omode & O_CREATE){
801066fe:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106701:	25 00 02 00 00       	and    $0x200,%eax
80106706:	85 c0                	test   %eax,%eax
80106708:	74 3b                	je     80106745 <sys_open+0x8a>
    ip = create(path, T_FILE, 0, 0);
8010670a:	8b 45 e8             	mov    -0x18(%ebp),%eax
8010670d:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
80106714:	00 
80106715:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
8010671c:	00 
8010671d:	c7 44 24 04 02 00 00 	movl   $0x2,0x4(%esp)
80106724:	00 
80106725:	89 04 24             	mov    %eax,(%esp)
80106728:	e8 ce fd ff ff       	call   801064fb <create>
8010672d:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(ip == 0){
80106730:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106734:	75 6b                	jne    801067a1 <sys_open+0xe6>
      end_op();
80106736:	e8 b1 d3 ff ff       	call   80103aec <end_op>
      return -1;
8010673b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106740:	e9 0e 01 00 00       	jmp    80106853 <sys_open+0x198>
    }
  } else {
    if((ip = namei(path)) == 0){
80106745:	8b 45 e8             	mov    -0x18(%ebp),%eax
80106748:	89 04 24             	mov    %eax,(%esp)
8010674b:	e8 cf c4 ff ff       	call   80102c1f <namei>
80106750:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106753:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106757:	75 0f                	jne    80106768 <sys_open+0xad>
      end_op();
80106759:	e8 8e d3 ff ff       	call   80103aec <end_op>
      return -1;
8010675e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106763:	e9 eb 00 00 00       	jmp    80106853 <sys_open+0x198>
    }
    ilock(ip);
80106768:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010676b:	89 04 24             	mov    %eax,(%esp)
8010676e:	e8 fe b8 ff ff       	call   80102071 <ilock>
    if(ip->type == T_DIR && omode != O_RDONLY){
80106773:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106776:	0f b7 40 10          	movzwl 0x10(%eax),%eax
8010677a:	66 83 f8 01          	cmp    $0x1,%ax
8010677e:	75 21                	jne    801067a1 <sys_open+0xe6>
80106780:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106783:	85 c0                	test   %eax,%eax
80106785:	74 1a                	je     801067a1 <sys_open+0xe6>
      iunlockput(ip);
80106787:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010678a:	89 04 24             	mov    %eax,(%esp)
8010678d:	e8 6c bb ff ff       	call   801022fe <iunlockput>
      end_op();
80106792:	e8 55 d3 ff ff       	call   80103aec <end_op>
      return -1;
80106797:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010679c:	e9 b2 00 00 00       	jmp    80106853 <sys_open+0x198>
    }
  }

  if((f = filealloc()) == 0 || (fd = fdalloc(f)) < 0){
801067a1:	e8 12 af ff ff       	call   801016b8 <filealloc>
801067a6:	89 45 f0             	mov    %eax,-0x10(%ebp)
801067a9:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801067ad:	74 14                	je     801067c3 <sys_open+0x108>
801067af:	8b 45 f0             	mov    -0x10(%ebp),%eax
801067b2:	89 04 24             	mov    %eax,(%esp)
801067b5:	e8 2f f7 ff ff       	call   80105ee9 <fdalloc>
801067ba:	89 45 ec             	mov    %eax,-0x14(%ebp)
801067bd:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
801067c1:	79 28                	jns    801067eb <sys_open+0x130>
    if(f)
801067c3:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801067c7:	74 0b                	je     801067d4 <sys_open+0x119>
      fileclose(f);
801067c9:	8b 45 f0             	mov    -0x10(%ebp),%eax
801067cc:	89 04 24             	mov    %eax,(%esp)
801067cf:	e8 8d af ff ff       	call   80101761 <fileclose>
    iunlockput(ip);
801067d4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801067d7:	89 04 24             	mov    %eax,(%esp)
801067da:	e8 1f bb ff ff       	call   801022fe <iunlockput>
    end_op();
801067df:	e8 08 d3 ff ff       	call   80103aec <end_op>
    return -1;
801067e4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801067e9:	eb 68                	jmp    80106853 <sys_open+0x198>
  }
  iunlock(ip);
801067eb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801067ee:	89 04 24             	mov    %eax,(%esp)
801067f1:	e8 d2 b9 ff ff       	call   801021c8 <iunlock>
  end_op();
801067f6:	e8 f1 d2 ff ff       	call   80103aec <end_op>

  f->type = FD_INODE;
801067fb:	8b 45 f0             	mov    -0x10(%ebp),%eax
801067fe:	c7 00 02 00 00 00    	movl   $0x2,(%eax)
  f->ip = ip;
80106804:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106807:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010680a:	89 50 10             	mov    %edx,0x10(%eax)
  f->off = 0;
8010680d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106810:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)
  f->readable = !(omode & O_WRONLY);
80106817:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010681a:	83 e0 01             	and    $0x1,%eax
8010681d:	85 c0                	test   %eax,%eax
8010681f:	0f 94 c2             	sete   %dl
80106822:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106825:	88 50 08             	mov    %dl,0x8(%eax)
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
80106828:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010682b:	83 e0 01             	and    $0x1,%eax
8010682e:	84 c0                	test   %al,%al
80106830:	75 0a                	jne    8010683c <sys_open+0x181>
80106832:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106835:	83 e0 02             	and    $0x2,%eax
80106838:	85 c0                	test   %eax,%eax
8010683a:	74 07                	je     80106843 <sys_open+0x188>
8010683c:	b8 01 00 00 00       	mov    $0x1,%eax
80106841:	eb 05                	jmp    80106848 <sys_open+0x18d>
80106843:	b8 00 00 00 00       	mov    $0x0,%eax
80106848:	89 c2                	mov    %eax,%edx
8010684a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010684d:	88 50 09             	mov    %dl,0x9(%eax)
  return fd;
80106850:	8b 45 ec             	mov    -0x14(%ebp),%eax
}
80106853:	c9                   	leave  
80106854:	c3                   	ret    

80106855 <sys_mkdir>:

int
sys_mkdir(void)
{
80106855:	55                   	push   %ebp
80106856:	89 e5                	mov    %esp,%ebp
80106858:	83 ec 28             	sub    $0x28,%esp
  char *path;
  struct inode *ip;

  begin_op();
8010685b:	e8 0a d2 ff ff       	call   80103a6a <begin_op>
  if(argstr(0, &path) < 0 || (ip = create(path, T_DIR, 0, 0)) == 0){
80106860:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106863:	89 44 24 04          	mov    %eax,0x4(%esp)
80106867:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
8010686e:	e8 24 f5 ff ff       	call   80105d97 <argstr>
80106873:	85 c0                	test   %eax,%eax
80106875:	78 2c                	js     801068a3 <sys_mkdir+0x4e>
80106877:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010687a:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
80106881:	00 
80106882:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80106889:	00 
8010688a:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80106891:	00 
80106892:	89 04 24             	mov    %eax,(%esp)
80106895:	e8 61 fc ff ff       	call   801064fb <create>
8010689a:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010689d:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801068a1:	75 0c                	jne    801068af <sys_mkdir+0x5a>
    end_op();
801068a3:	e8 44 d2 ff ff       	call   80103aec <end_op>
    return -1;
801068a8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801068ad:	eb 15                	jmp    801068c4 <sys_mkdir+0x6f>
  }
  iunlockput(ip);
801068af:	8b 45 f4             	mov    -0xc(%ebp),%eax
801068b2:	89 04 24             	mov    %eax,(%esp)
801068b5:	e8 44 ba ff ff       	call   801022fe <iunlockput>
  end_op();
801068ba:	e8 2d d2 ff ff       	call   80103aec <end_op>
  return 0;
801068bf:	b8 00 00 00 00       	mov    $0x0,%eax
}
801068c4:	c9                   	leave  
801068c5:	c3                   	ret    

801068c6 <sys_mknod>:

int
sys_mknod(void)
{
801068c6:	55                   	push   %ebp
801068c7:	89 e5                	mov    %esp,%ebp
801068c9:	83 ec 28             	sub    $0x28,%esp
  struct inode *ip;
  char *path;
  int major, minor;

  begin_op();
801068cc:	e8 99 d1 ff ff       	call   80103a6a <begin_op>
  if((argstr(0, &path)) < 0 ||
801068d1:	8d 45 f0             	lea    -0x10(%ebp),%eax
801068d4:	89 44 24 04          	mov    %eax,0x4(%esp)
801068d8:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
801068df:	e8 b3 f4 ff ff       	call   80105d97 <argstr>
801068e4:	85 c0                	test   %eax,%eax
801068e6:	78 5e                	js     80106946 <sys_mknod+0x80>
     argint(1, &major) < 0 ||
801068e8:	8d 45 ec             	lea    -0x14(%ebp),%eax
801068eb:	89 44 24 04          	mov    %eax,0x4(%esp)
801068ef:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
801068f6:	e8 0b f4 ff ff       	call   80105d06 <argint>
  struct inode *ip;
  char *path;
  int major, minor;

  begin_op();
  if((argstr(0, &path)) < 0 ||
801068fb:	85 c0                	test   %eax,%eax
801068fd:	78 47                	js     80106946 <sys_mknod+0x80>
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
801068ff:	8d 45 e8             	lea    -0x18(%ebp),%eax
80106902:	89 44 24 04          	mov    %eax,0x4(%esp)
80106906:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
8010690d:	e8 f4 f3 ff ff       	call   80105d06 <argint>
  struct inode *ip;
  char *path;
  int major, minor;

  begin_op();
  if((argstr(0, &path)) < 0 ||
80106912:	85 c0                	test   %eax,%eax
80106914:	78 30                	js     80106946 <sys_mknod+0x80>
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
     (ip = create(path, T_DEV, major, minor)) == 0){
80106916:	8b 45 e8             	mov    -0x18(%ebp),%eax
80106919:	0f bf c8             	movswl %ax,%ecx
8010691c:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010691f:	0f bf d0             	movswl %ax,%edx
80106922:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106925:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
80106929:	89 54 24 08          	mov    %edx,0x8(%esp)
8010692d:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
80106934:	00 
80106935:	89 04 24             	mov    %eax,(%esp)
80106938:	e8 be fb ff ff       	call   801064fb <create>
  struct inode *ip;
  char *path;
  int major, minor;

  begin_op();
  if((argstr(0, &path)) < 0 ||
8010693d:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106940:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106944:	75 0c                	jne    80106952 <sys_mknod+0x8c>
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
     (ip = create(path, T_DEV, major, minor)) == 0){
    end_op();
80106946:	e8 a1 d1 ff ff       	call   80103aec <end_op>
    return -1;
8010694b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106950:	eb 15                	jmp    80106967 <sys_mknod+0xa1>
  }
  iunlockput(ip);
80106952:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106955:	89 04 24             	mov    %eax,(%esp)
80106958:	e8 a1 b9 ff ff       	call   801022fe <iunlockput>
  end_op();
8010695d:	e8 8a d1 ff ff       	call   80103aec <end_op>
  return 0;
80106962:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106967:	c9                   	leave  
80106968:	c3                   	ret    

80106969 <sys_chdir>:

int
sys_chdir(void)
{
80106969:	55                   	push   %ebp
8010696a:	89 e5                	mov    %esp,%ebp
8010696c:	83 ec 28             	sub    $0x28,%esp
  char *path;
  struct inode *ip;

  begin_op();
8010696f:	e8 f6 d0 ff ff       	call   80103a6a <begin_op>
  if(argstr(0, &path) < 0 || (ip = namei(path)) == 0){
80106974:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106977:	89 44 24 04          	mov    %eax,0x4(%esp)
8010697b:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106982:	e8 10 f4 ff ff       	call   80105d97 <argstr>
80106987:	85 c0                	test   %eax,%eax
80106989:	78 14                	js     8010699f <sys_chdir+0x36>
8010698b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010698e:	89 04 24             	mov    %eax,(%esp)
80106991:	e8 89 c2 ff ff       	call   80102c1f <namei>
80106996:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106999:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010699d:	75 0c                	jne    801069ab <sys_chdir+0x42>
    end_op();
8010699f:	e8 48 d1 ff ff       	call   80103aec <end_op>
    return -1;
801069a4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801069a9:	eb 61                	jmp    80106a0c <sys_chdir+0xa3>
  }
  ilock(ip);
801069ab:	8b 45 f4             	mov    -0xc(%ebp),%eax
801069ae:	89 04 24             	mov    %eax,(%esp)
801069b1:	e8 bb b6 ff ff       	call   80102071 <ilock>
  if(ip->type != T_DIR){
801069b6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801069b9:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801069bd:	66 83 f8 01          	cmp    $0x1,%ax
801069c1:	74 17                	je     801069da <sys_chdir+0x71>
    iunlockput(ip);
801069c3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801069c6:	89 04 24             	mov    %eax,(%esp)
801069c9:	e8 30 b9 ff ff       	call   801022fe <iunlockput>
    end_op();
801069ce:	e8 19 d1 ff ff       	call   80103aec <end_op>
    return -1;
801069d3:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801069d8:	eb 32                	jmp    80106a0c <sys_chdir+0xa3>
  }
  iunlock(ip);
801069da:	8b 45 f4             	mov    -0xc(%ebp),%eax
801069dd:	89 04 24             	mov    %eax,(%esp)
801069e0:	e8 e3 b7 ff ff       	call   801021c8 <iunlock>
  iput(proc->cwd);
801069e5:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801069eb:	8b 40 68             	mov    0x68(%eax),%eax
801069ee:	89 04 24             	mov    %eax,(%esp)
801069f1:	e8 37 b8 ff ff       	call   8010222d <iput>
  end_op();
801069f6:	e8 f1 d0 ff ff       	call   80103aec <end_op>
  proc->cwd = ip;
801069fb:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106a01:	8b 55 f4             	mov    -0xc(%ebp),%edx
80106a04:	89 50 68             	mov    %edx,0x68(%eax)
  return 0;
80106a07:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106a0c:	c9                   	leave  
80106a0d:	c3                   	ret    

80106a0e <sys_exec>:

int
sys_exec(void)
{
80106a0e:	55                   	push   %ebp
80106a0f:	89 e5                	mov    %esp,%ebp
80106a11:	81 ec a8 00 00 00    	sub    $0xa8,%esp
  char *path, *argv[MAXARG];
  int i;
  uint uargv, uarg;

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
80106a17:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106a1a:	89 44 24 04          	mov    %eax,0x4(%esp)
80106a1e:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106a25:	e8 6d f3 ff ff       	call   80105d97 <argstr>
80106a2a:	85 c0                	test   %eax,%eax
80106a2c:	78 1a                	js     80106a48 <sys_exec+0x3a>
80106a2e:	8d 85 6c ff ff ff    	lea    -0x94(%ebp),%eax
80106a34:	89 44 24 04          	mov    %eax,0x4(%esp)
80106a38:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80106a3f:	e8 c2 f2 ff ff       	call   80105d06 <argint>
80106a44:	85 c0                	test   %eax,%eax
80106a46:	79 0a                	jns    80106a52 <sys_exec+0x44>
    return -1;
80106a48:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106a4d:	e9 cd 00 00 00       	jmp    80106b1f <sys_exec+0x111>
  }
  memset(argv, 0, sizeof(argv));
80106a52:	c7 44 24 08 80 00 00 	movl   $0x80,0x8(%esp)
80106a59:	00 
80106a5a:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80106a61:	00 
80106a62:	8d 85 70 ff ff ff    	lea    -0x90(%ebp),%eax
80106a68:	89 04 24             	mov    %eax,(%esp)
80106a6b:	e8 3a ef ff ff       	call   801059aa <memset>
  for(i=0;; i++){
80106a70:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    if(i >= NELEM(argv))
80106a77:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106a7a:	83 f8 1f             	cmp    $0x1f,%eax
80106a7d:	76 0a                	jbe    80106a89 <sys_exec+0x7b>
      return -1;
80106a7f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106a84:	e9 96 00 00 00       	jmp    80106b1f <sys_exec+0x111>
    if(fetchint(uargv+4*i, (int*)&uarg) < 0)
80106a89:	8d 85 68 ff ff ff    	lea    -0x98(%ebp),%eax
80106a8f:	8b 55 f4             	mov    -0xc(%ebp),%edx
80106a92:	c1 e2 02             	shl    $0x2,%edx
80106a95:	89 d1                	mov    %edx,%ecx
80106a97:	8b 95 6c ff ff ff    	mov    -0x94(%ebp),%edx
80106a9d:	8d 14 11             	lea    (%ecx,%edx,1),%edx
80106aa0:	89 44 24 04          	mov    %eax,0x4(%esp)
80106aa4:	89 14 24             	mov    %edx,(%esp)
80106aa7:	e8 bc f1 ff ff       	call   80105c68 <fetchint>
80106aac:	85 c0                	test   %eax,%eax
80106aae:	79 07                	jns    80106ab7 <sys_exec+0xa9>
      return -1;
80106ab0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106ab5:	eb 68                	jmp    80106b1f <sys_exec+0x111>
    if(uarg == 0){
80106ab7:	8b 85 68 ff ff ff    	mov    -0x98(%ebp),%eax
80106abd:	85 c0                	test   %eax,%eax
80106abf:	75 26                	jne    80106ae7 <sys_exec+0xd9>
      argv[i] = 0;
80106ac1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106ac4:	c7 84 85 70 ff ff ff 	movl   $0x0,-0x90(%ebp,%eax,4)
80106acb:	00 00 00 00 
      break;
80106acf:	90                   	nop
    }
    if(fetchstr(uarg, &argv[i]) < 0)
      return -1;
  }
  return exec(path, argv);
80106ad0:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106ad3:	8d 95 70 ff ff ff    	lea    -0x90(%ebp),%edx
80106ad9:	89 54 24 04          	mov    %edx,0x4(%esp)
80106add:	89 04 24             	mov    %eax,(%esp)
80106ae0:	e8 9f a7 ff ff       	call   80101284 <exec>
80106ae5:	eb 38                	jmp    80106b1f <sys_exec+0x111>
      return -1;
    if(uarg == 0){
      argv[i] = 0;
      break;
    }
    if(fetchstr(uarg, &argv[i]) < 0)
80106ae7:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106aea:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80106af1:	8d 85 70 ff ff ff    	lea    -0x90(%ebp),%eax
80106af7:	01 d0                	add    %edx,%eax
80106af9:	8b 95 68 ff ff ff    	mov    -0x98(%ebp),%edx
80106aff:	89 44 24 04          	mov    %eax,0x4(%esp)
80106b03:	89 14 24             	mov    %edx,(%esp)
80106b06:	e8 97 f1 ff ff       	call   80105ca2 <fetchstr>
80106b0b:	85 c0                	test   %eax,%eax
80106b0d:	79 07                	jns    80106b16 <sys_exec+0x108>
      return -1;
80106b0f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106b14:	eb 09                	jmp    80106b1f <sys_exec+0x111>

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
    return -1;
  }
  memset(argv, 0, sizeof(argv));
  for(i=0;; i++){
80106b16:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      argv[i] = 0;
      break;
    }
    if(fetchstr(uarg, &argv[i]) < 0)
      return -1;
  }
80106b1a:	e9 58 ff ff ff       	jmp    80106a77 <sys_exec+0x69>
  return exec(path, argv);
}
80106b1f:	c9                   	leave  
80106b20:	c3                   	ret    

80106b21 <sys_pipe>:

int
sys_pipe(void)
{
80106b21:	55                   	push   %ebp
80106b22:	89 e5                	mov    %esp,%ebp
80106b24:	83 ec 38             	sub    $0x38,%esp
  int *fd;
  struct file *rf, *wf;
  int fd0, fd1;

  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
80106b27:	8d 45 ec             	lea    -0x14(%ebp),%eax
80106b2a:	c7 44 24 08 08 00 00 	movl   $0x8,0x8(%esp)
80106b31:	00 
80106b32:	89 44 24 04          	mov    %eax,0x4(%esp)
80106b36:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106b3d:	e8 f3 f1 ff ff       	call   80105d35 <argptr>
80106b42:	85 c0                	test   %eax,%eax
80106b44:	79 0a                	jns    80106b50 <sys_pipe+0x2f>
    return -1;
80106b46:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106b4b:	e9 9b 00 00 00       	jmp    80106beb <sys_pipe+0xca>
  if(pipealloc(&rf, &wf) < 0)
80106b50:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80106b53:	89 44 24 04          	mov    %eax,0x4(%esp)
80106b57:	8d 45 e8             	lea    -0x18(%ebp),%eax
80106b5a:	89 04 24             	mov    %eax,(%esp)
80106b5d:	e8 da da ff ff       	call   8010463c <pipealloc>
80106b62:	85 c0                	test   %eax,%eax
80106b64:	79 07                	jns    80106b6d <sys_pipe+0x4c>
    return -1;
80106b66:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106b6b:	eb 7e                	jmp    80106beb <sys_pipe+0xca>
  fd0 = -1;
80106b6d:	c7 45 f0 ff ff ff ff 	movl   $0xffffffff,-0x10(%ebp)
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
80106b74:	8b 45 e8             	mov    -0x18(%ebp),%eax
80106b77:	89 04 24             	mov    %eax,(%esp)
80106b7a:	e8 6a f3 ff ff       	call   80105ee9 <fdalloc>
80106b7f:	89 45 f0             	mov    %eax,-0x10(%ebp)
80106b82:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80106b86:	78 14                	js     80106b9c <sys_pipe+0x7b>
80106b88:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106b8b:	89 04 24             	mov    %eax,(%esp)
80106b8e:	e8 56 f3 ff ff       	call   80105ee9 <fdalloc>
80106b93:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106b96:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106b9a:	79 37                	jns    80106bd3 <sys_pipe+0xb2>
    if(fd0 >= 0)
80106b9c:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80106ba0:	78 14                	js     80106bb6 <sys_pipe+0x95>
      proc->ofile[fd0] = 0;
80106ba2:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106ba8:	8b 55 f0             	mov    -0x10(%ebp),%edx
80106bab:	83 c2 08             	add    $0x8,%edx
80106bae:	c7 44 90 08 00 00 00 	movl   $0x0,0x8(%eax,%edx,4)
80106bb5:	00 
    fileclose(rf);
80106bb6:	8b 45 e8             	mov    -0x18(%ebp),%eax
80106bb9:	89 04 24             	mov    %eax,(%esp)
80106bbc:	e8 a0 ab ff ff       	call   80101761 <fileclose>
    fileclose(wf);
80106bc1:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106bc4:	89 04 24             	mov    %eax,(%esp)
80106bc7:	e8 95 ab ff ff       	call   80101761 <fileclose>
    return -1;
80106bcc:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106bd1:	eb 18                	jmp    80106beb <sys_pipe+0xca>
  }
  fd[0] = fd0;
80106bd3:	8b 45 ec             	mov    -0x14(%ebp),%eax
80106bd6:	8b 55 f0             	mov    -0x10(%ebp),%edx
80106bd9:	89 10                	mov    %edx,(%eax)
  fd[1] = fd1;
80106bdb:	8b 45 ec             	mov    -0x14(%ebp),%eax
80106bde:	8d 50 04             	lea    0x4(%eax),%edx
80106be1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106be4:	89 02                	mov    %eax,(%edx)
  return 0;
80106be6:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106beb:	c9                   	leave  
80106bec:	c3                   	ret    

80106bed <syscallstats>:

void syscallstats()
{
80106bed:	55                   	push   %ebp
80106bee:	89 e5                	mov    %esp,%ebp
	
}
80106bf0:	5d                   	pop    %ebp
80106bf1:	c3                   	ret    
	...

80106bf4 <sys_fork>:
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
80106bf4:	55                   	push   %ebp
80106bf5:	89 e5                	mov    %esp,%ebp
80106bf7:	83 ec 08             	sub    $0x8,%esp
  return fork();
80106bfa:	e8 ef e0 ff ff       	call   80104cee <fork>
}
80106bff:	c9                   	leave  
80106c00:	c3                   	ret    

80106c01 <sys_exit>:

int
sys_exit(void)
{
80106c01:	55                   	push   %ebp
80106c02:	89 e5                	mov    %esp,%ebp
80106c04:	83 ec 08             	sub    $0x8,%esp
  exit();
80106c07:	e8 77 e2 ff ff       	call   80104e83 <exit>
  return 0;  // not reached
80106c0c:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106c11:	c9                   	leave  
80106c12:	c3                   	ret    

80106c13 <sys_wait>:

int
sys_wait(void)
{
80106c13:	55                   	push   %ebp
80106c14:	89 e5                	mov    %esp,%ebp
80106c16:	83 ec 08             	sub    $0x8,%esp
  return wait();
80106c19:	e8 ff e3 ff ff       	call   8010501d <wait>
}
80106c1e:	c9                   	leave  
80106c1f:	c3                   	ret    

80106c20 <sys_kill>:

int
sys_kill(void)
{
80106c20:	55                   	push   %ebp
80106c21:	89 e5                	mov    %esp,%ebp
80106c23:	83 ec 28             	sub    $0x28,%esp
  int pid;

  if(argint(0, &pid) < 0)
80106c26:	8d 45 f4             	lea    -0xc(%ebp),%eax
80106c29:	89 44 24 04          	mov    %eax,0x4(%esp)
80106c2d:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106c34:	e8 cd f0 ff ff       	call   80105d06 <argint>
80106c39:	85 c0                	test   %eax,%eax
80106c3b:	79 07                	jns    80106c44 <sys_kill+0x24>
    return -1;
80106c3d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106c42:	eb 0b                	jmp    80106c4f <sys_kill+0x2f>
  return kill(pid);
80106c44:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106c47:	89 04 24             	mov    %eax,(%esp)
80106c4a:	e8 d7 e8 ff ff       	call   80105526 <kill>
}
80106c4f:	c9                   	leave  
80106c50:	c3                   	ret    

80106c51 <sys_getpid>:

int
sys_getpid(void)
{
80106c51:	55                   	push   %ebp
80106c52:	89 e5                	mov    %esp,%ebp
  return proc->pid;
80106c54:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106c5a:	8b 40 10             	mov    0x10(%eax),%eax
}
80106c5d:	5d                   	pop    %ebp
80106c5e:	c3                   	ret    

80106c5f <sys_sbrk>:

int
sys_sbrk(void)
{
80106c5f:	55                   	push   %ebp
80106c60:	89 e5                	mov    %esp,%ebp
80106c62:	83 ec 28             	sub    $0x28,%esp
  int addr;
  int n;

  if(argint(0, &n) < 0)
80106c65:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106c68:	89 44 24 04          	mov    %eax,0x4(%esp)
80106c6c:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106c73:	e8 8e f0 ff ff       	call   80105d06 <argint>
80106c78:	85 c0                	test   %eax,%eax
80106c7a:	79 07                	jns    80106c83 <sys_sbrk+0x24>
    return -1;
80106c7c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106c81:	eb 24                	jmp    80106ca7 <sys_sbrk+0x48>
  addr = proc->sz;
80106c83:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106c89:	8b 00                	mov    (%eax),%eax
80106c8b:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(growproc(n) < 0)
80106c8e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106c91:	89 04 24             	mov    %eax,(%esp)
80106c94:	e8 b0 df ff ff       	call   80104c49 <growproc>
80106c99:	85 c0                	test   %eax,%eax
80106c9b:	79 07                	jns    80106ca4 <sys_sbrk+0x45>
    return -1;
80106c9d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106ca2:	eb 03                	jmp    80106ca7 <sys_sbrk+0x48>
  return addr;
80106ca4:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
80106ca7:	c9                   	leave  
80106ca8:	c3                   	ret    

80106ca9 <sys_sleep>:

int
sys_sleep(void)
{
80106ca9:	55                   	push   %ebp
80106caa:	89 e5                	mov    %esp,%ebp
80106cac:	83 ec 28             	sub    $0x28,%esp
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
80106caf:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106cb2:	89 44 24 04          	mov    %eax,0x4(%esp)
80106cb6:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106cbd:	e8 44 f0 ff ff       	call   80105d06 <argint>
80106cc2:	85 c0                	test   %eax,%eax
80106cc4:	79 07                	jns    80106ccd <sys_sleep+0x24>
    return -1;
80106cc6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106ccb:	eb 6c                	jmp    80106d39 <sys_sleep+0x90>
  acquire(&tickslock);
80106ccd:	c7 04 24 40 6b 11 80 	movl   $0x80116b40,(%esp)
80106cd4:	e8 6e ea ff ff       	call   80105747 <acquire>
  ticks0 = ticks;
80106cd9:	a1 74 6b 11 80       	mov    0x80116b74,%eax
80106cde:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while(ticks - ticks0 < n){
80106ce1:	eb 34                	jmp    80106d17 <sys_sleep+0x6e>
    if(proc->killed){
80106ce3:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106ce9:	8b 40 24             	mov    0x24(%eax),%eax
80106cec:	85 c0                	test   %eax,%eax
80106cee:	74 13                	je     80106d03 <sys_sleep+0x5a>
      release(&tickslock);
80106cf0:	c7 04 24 40 6b 11 80 	movl   $0x80116b40,(%esp)
80106cf7:	e8 b4 ea ff ff       	call   801057b0 <release>
      return -1;
80106cfc:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106d01:	eb 36                	jmp    80106d39 <sys_sleep+0x90>
    }
    sleep(&ticks, &tickslock);
80106d03:	c7 44 24 04 40 6b 11 	movl   $0x80116b40,0x4(%esp)
80106d0a:	80 
80106d0b:	c7 04 24 74 6b 11 80 	movl   $0x80116b74,(%esp)
80106d12:	e8 07 e7 ff ff       	call   8010541e <sleep>

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
80106d17:	a1 74 6b 11 80       	mov    0x80116b74,%eax
80106d1c:	89 c2                	mov    %eax,%edx
80106d1e:	2b 55 f4             	sub    -0xc(%ebp),%edx
80106d21:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106d24:	39 c2                	cmp    %eax,%edx
80106d26:	72 bb                	jb     80106ce3 <sys_sleep+0x3a>
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
80106d28:	c7 04 24 40 6b 11 80 	movl   $0x80116b40,(%esp)
80106d2f:	e8 7c ea ff ff       	call   801057b0 <release>
  return 0;
80106d34:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106d39:	c9                   	leave  
80106d3a:	c3                   	ret    

80106d3b <sys_uptime>:

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
80106d3b:	55                   	push   %ebp
80106d3c:	89 e5                	mov    %esp,%ebp
80106d3e:	83 ec 28             	sub    $0x28,%esp
  uint xticks;

  acquire(&tickslock);
80106d41:	c7 04 24 40 6b 11 80 	movl   $0x80116b40,(%esp)
80106d48:	e8 fa e9 ff ff       	call   80105747 <acquire>
  xticks = ticks;
80106d4d:	a1 74 6b 11 80       	mov    0x80116b74,%eax
80106d52:	89 45 f4             	mov    %eax,-0xc(%ebp)
  release(&tickslock);
80106d55:	c7 04 24 40 6b 11 80 	movl   $0x80116b40,(%esp)
80106d5c:	e8 4f ea ff ff       	call   801057b0 <release>
  return xticks;
80106d61:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
80106d64:	c9                   	leave  
80106d65:	c3                   	ret    
	...

80106d68 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80106d68:	55                   	push   %ebp
80106d69:	89 e5                	mov    %esp,%ebp
80106d6b:	83 ec 08             	sub    $0x8,%esp
80106d6e:	8b 55 08             	mov    0x8(%ebp),%edx
80106d71:	8b 45 0c             	mov    0xc(%ebp),%eax
80106d74:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80106d78:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80106d7b:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80106d7f:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80106d83:	ee                   	out    %al,(%dx)
}
80106d84:	c9                   	leave  
80106d85:	c3                   	ret    

80106d86 <timerinit>:
#define TIMER_RATEGEN   0x04    // mode 2, rate generator
#define TIMER_16BIT     0x30    // r/w counter 16 bits, LSB first

void
timerinit(void)
{
80106d86:	55                   	push   %ebp
80106d87:	89 e5                	mov    %esp,%ebp
80106d89:	83 ec 18             	sub    $0x18,%esp
  // Interrupt 100 times/sec.
  outb(TIMER_MODE, TIMER_SEL0 | TIMER_RATEGEN | TIMER_16BIT);
80106d8c:	c7 44 24 04 34 00 00 	movl   $0x34,0x4(%esp)
80106d93:	00 
80106d94:	c7 04 24 43 00 00 00 	movl   $0x43,(%esp)
80106d9b:	e8 c8 ff ff ff       	call   80106d68 <outb>
  outb(IO_TIMER1, TIMER_DIV(100) % 256);
80106da0:	c7 44 24 04 9c 00 00 	movl   $0x9c,0x4(%esp)
80106da7:	00 
80106da8:	c7 04 24 40 00 00 00 	movl   $0x40,(%esp)
80106daf:	e8 b4 ff ff ff       	call   80106d68 <outb>
  outb(IO_TIMER1, TIMER_DIV(100) / 256);
80106db4:	c7 44 24 04 2e 00 00 	movl   $0x2e,0x4(%esp)
80106dbb:	00 
80106dbc:	c7 04 24 40 00 00 00 	movl   $0x40,(%esp)
80106dc3:	e8 a0 ff ff ff       	call   80106d68 <outb>
  picenable(IRQ_TIMER);
80106dc8:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106dcf:	e8 f1 d6 ff ff       	call   801044c5 <picenable>
}
80106dd4:	c9                   	leave  
80106dd5:	c3                   	ret    
	...

80106dd8 <alltraps>:

  # vectors.S sends all traps here.
.globl alltraps
alltraps:
  # Build trap frame.
  pushl %ds
80106dd8:	1e                   	push   %ds
  pushl %es
80106dd9:	06                   	push   %es
  pushl %fs
80106dda:	0f a0                	push   %fs
  pushl %gs
80106ddc:	0f a8                	push   %gs
  pushal
80106dde:	60                   	pusha  
  
  # Set up data and per-cpu segments.
  movw $(SEG_KDATA<<3), %ax
80106ddf:	66 b8 10 00          	mov    $0x10,%ax
  movw %ax, %ds
80106de3:	8e d8                	mov    %eax,%ds
  movw %ax, %es
80106de5:	8e c0                	mov    %eax,%es
  movw $(SEG_KCPU<<3), %ax
80106de7:	66 b8 18 00          	mov    $0x18,%ax
  movw %ax, %fs
80106deb:	8e e0                	mov    %eax,%fs
  movw %ax, %gs
80106ded:	8e e8                	mov    %eax,%gs

  # Call trap(tf), where tf=%esp
  pushl %esp
80106def:	54                   	push   %esp
  call trap
80106df0:	e8 b9 02 00 00       	call   801070ae <trap>
  addl $4, %esp
80106df5:	83 c4 04             	add    $0x4,%esp

80106df8 <trapret>:

  # Return falls through to trapret...
.globl trapret
trapret:
  popal
80106df8:	61                   	popa   
  popl %gs
80106df9:	0f a9                	pop    %gs
  popl %fs
80106dfb:	0f a1                	pop    %fs
  popl %es
80106dfd:	07                   	pop    %es
  popl %ds
80106dfe:	1f                   	pop    %ds
  addl $0x8, %esp  # trapno and errcode
80106dff:	83 c4 08             	add    $0x8,%esp
  iret
80106e02:	cf                   	iret   
	...

80106e04 <lidt>:

struct gatedesc;

static inline void
lidt(struct gatedesc *p, int size)
{
80106e04:	55                   	push   %ebp
80106e05:	89 e5                	mov    %esp,%ebp
80106e07:	83 ec 10             	sub    $0x10,%esp
  volatile ushort pd[3];

  pd[0] = size-1;
80106e0a:	8b 45 0c             	mov    0xc(%ebp),%eax
80106e0d:	83 e8 01             	sub    $0x1,%eax
80106e10:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
  pd[1] = (uint)p;
80106e14:	8b 45 08             	mov    0x8(%ebp),%eax
80106e17:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  pd[2] = (uint)p >> 16;
80106e1b:	8b 45 08             	mov    0x8(%ebp),%eax
80106e1e:	c1 e8 10             	shr    $0x10,%eax
80106e21:	66 89 45 fe          	mov    %ax,-0x2(%ebp)

  asm volatile("lidt (%0)" : : "r" (pd));
80106e25:	8d 45 fa             	lea    -0x6(%ebp),%eax
80106e28:	0f 01 18             	lidtl  (%eax)
}
80106e2b:	c9                   	leave  
80106e2c:	c3                   	ret    

80106e2d <sti>:
  asm volatile("cli");
}

static inline void
sti(void)
{
80106e2d:	55                   	push   %ebp
80106e2e:	89 e5                	mov    %esp,%ebp
  asm volatile("sti");
80106e30:	fb                   	sti    
}
80106e31:	5d                   	pop    %ebp
80106e32:	c3                   	ret    

80106e33 <rcr2>:
  return result;
}

static inline uint
rcr2(void)
{
80106e33:	55                   	push   %ebp
80106e34:	89 e5                	mov    %esp,%ebp
80106e36:	83 ec 10             	sub    $0x10,%esp
  uint val;
  asm volatile("movl %%cr2,%0" : "=r" (val));
80106e39:	0f 20 d0             	mov    %cr2,%eax
80106e3c:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return val;
80106e3f:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80106e42:	c9                   	leave  
80106e43:	c3                   	ret    

80106e44 <tvinit>:
int capturing = 0;
struct capture_stats cs;

void
tvinit(void)
{
80106e44:	55                   	push   %ebp
80106e45:	89 e5                	mov    %esp,%ebp
80106e47:	83 ec 28             	sub    $0x28,%esp
  int i;

  for(i = 0; i < 256; i++)
80106e4a:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80106e51:	e9 bf 00 00 00       	jmp    80106f15 <tvinit+0xd1>
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
80106e56:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106e59:	8b 55 f4             	mov    -0xc(%ebp),%edx
80106e5c:	8b 14 95 c8 c0 10 80 	mov    -0x7fef3f38(,%edx,4),%edx
80106e63:	66 89 14 c5 c0 c6 10 	mov    %dx,-0x7fef3940(,%eax,8)
80106e6a:	80 
80106e6b:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106e6e:	66 c7 04 c5 c2 c6 10 	movw   $0x8,-0x7fef393e(,%eax,8)
80106e75:	80 08 00 
80106e78:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106e7b:	0f b6 14 c5 c4 c6 10 	movzbl -0x7fef393c(,%eax,8),%edx
80106e82:	80 
80106e83:	83 e2 e0             	and    $0xffffffe0,%edx
80106e86:	88 14 c5 c4 c6 10 80 	mov    %dl,-0x7fef393c(,%eax,8)
80106e8d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106e90:	0f b6 14 c5 c4 c6 10 	movzbl -0x7fef393c(,%eax,8),%edx
80106e97:	80 
80106e98:	83 e2 1f             	and    $0x1f,%edx
80106e9b:	88 14 c5 c4 c6 10 80 	mov    %dl,-0x7fef393c(,%eax,8)
80106ea2:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106ea5:	0f b6 14 c5 c5 c6 10 	movzbl -0x7fef393b(,%eax,8),%edx
80106eac:	80 
80106ead:	83 e2 f0             	and    $0xfffffff0,%edx
80106eb0:	83 ca 0e             	or     $0xe,%edx
80106eb3:	88 14 c5 c5 c6 10 80 	mov    %dl,-0x7fef393b(,%eax,8)
80106eba:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106ebd:	0f b6 14 c5 c5 c6 10 	movzbl -0x7fef393b(,%eax,8),%edx
80106ec4:	80 
80106ec5:	83 e2 ef             	and    $0xffffffef,%edx
80106ec8:	88 14 c5 c5 c6 10 80 	mov    %dl,-0x7fef393b(,%eax,8)
80106ecf:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106ed2:	0f b6 14 c5 c5 c6 10 	movzbl -0x7fef393b(,%eax,8),%edx
80106ed9:	80 
80106eda:	83 ca 60             	or     $0x60,%edx
80106edd:	88 14 c5 c5 c6 10 80 	mov    %dl,-0x7fef393b(,%eax,8)
80106ee4:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106ee7:	0f b6 14 c5 c5 c6 10 	movzbl -0x7fef393b(,%eax,8),%edx
80106eee:	80 
80106eef:	83 ca 80             	or     $0xffffff80,%edx
80106ef2:	88 14 c5 c5 c6 10 80 	mov    %dl,-0x7fef393b(,%eax,8)
80106ef9:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106efc:	8b 55 f4             	mov    -0xc(%ebp),%edx
80106eff:	8b 14 95 c8 c0 10 80 	mov    -0x7fef3f38(,%edx,4),%edx
80106f06:	c1 ea 10             	shr    $0x10,%edx
80106f09:	66 89 14 c5 c6 c6 10 	mov    %dx,-0x7fef393a(,%eax,8)
80106f10:	80 
void
tvinit(void)
{
  int i;

  for(i = 0; i < 256; i++)
80106f11:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80106f15:	81 7d f4 ff 00 00 00 	cmpl   $0xff,-0xc(%ebp)
80106f1c:	0f 8e 34 ff ff ff    	jle    80106e56 <tvinit+0x12>
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);
80106f22:	a1 c8 c1 10 80       	mov    0x8010c1c8,%eax
80106f27:	66 a3 c0 c8 10 80    	mov    %ax,0x8010c8c0
80106f2d:	66 c7 05 c2 c8 10 80 	movw   $0x8,0x8010c8c2
80106f34:	08 00 
80106f36:	0f b6 05 c4 c8 10 80 	movzbl 0x8010c8c4,%eax
80106f3d:	83 e0 e0             	and    $0xffffffe0,%eax
80106f40:	a2 c4 c8 10 80       	mov    %al,0x8010c8c4
80106f45:	0f b6 05 c4 c8 10 80 	movzbl 0x8010c8c4,%eax
80106f4c:	83 e0 1f             	and    $0x1f,%eax
80106f4f:	a2 c4 c8 10 80       	mov    %al,0x8010c8c4
80106f54:	0f b6 05 c5 c8 10 80 	movzbl 0x8010c8c5,%eax
80106f5b:	83 c8 0f             	or     $0xf,%eax
80106f5e:	a2 c5 c8 10 80       	mov    %al,0x8010c8c5
80106f63:	0f b6 05 c5 c8 10 80 	movzbl 0x8010c8c5,%eax
80106f6a:	83 e0 ef             	and    $0xffffffef,%eax
80106f6d:	a2 c5 c8 10 80       	mov    %al,0x8010c8c5
80106f72:	0f b6 05 c5 c8 10 80 	movzbl 0x8010c8c5,%eax
80106f79:	83 c8 60             	or     $0x60,%eax
80106f7c:	a2 c5 c8 10 80       	mov    %al,0x8010c8c5
80106f81:	0f b6 05 c5 c8 10 80 	movzbl 0x8010c8c5,%eax
80106f88:	83 c8 80             	or     $0xffffff80,%eax
80106f8b:	a2 c5 c8 10 80       	mov    %al,0x8010c8c5
80106f90:	a1 c8 c1 10 80       	mov    0x8010c1c8,%eax
80106f95:	c1 e8 10             	shr    $0x10,%eax
80106f98:	66 a3 c6 c8 10 80    	mov    %ax,0x8010c8c6

  initlock(&tickslock, "time");
80106f9e:	c7 44 24 04 d0 94 10 	movl   $0x801094d0,0x4(%esp)
80106fa5:	80 
80106fa6:	c7 04 24 40 6b 11 80 	movl   $0x80116b40,(%esp)
80106fad:	e8 74 e7 ff ff       	call   80105726 <initlock>

  ticks = 0;
80106fb2:	c7 05 74 6b 11 80 00 	movl   $0x0,0x80116b74
80106fb9:	00 00 00 
}
80106fbc:	c9                   	leave  
80106fbd:	c3                   	ret    

80106fbe <idtinit>:

void
idtinit(void)
{
80106fbe:	55                   	push   %ebp
80106fbf:	89 e5                	mov    %esp,%ebp
80106fc1:	83 ec 08             	sub    $0x8,%esp
  lidt(idt, sizeof(idt));
80106fc4:	c7 44 24 04 00 08 00 	movl   $0x800,0x4(%esp)
80106fcb:	00 
80106fcc:	c7 04 24 c0 c6 10 80 	movl   $0x8010c6c0,(%esp)
80106fd3:	e8 2c fe ff ff       	call   80106e04 <lidt>
}
80106fd8:	c9                   	leave  
80106fd9:	c3                   	ret    

80106fda <trap_exit>:

//PAGEBREAK: 41
// This is here so you can break on it in gdb
static void
trap_exit()
{
80106fda:	55                   	push   %ebp
80106fdb:	89 e5                	mov    %esp,%ebp
80106fdd:	83 ec 08             	sub    $0x8,%esp
  sti();
80106fe0:	e8 48 fe ff ff       	call   80106e2d <sti>
  exit();
80106fe5:	e8 99 de ff ff       	call   80104e83 <exit>
}
80106fea:	c9                   	leave  
80106feb:	c3                   	ret    

80106fec <start_capture>:

void start_capture(void)
{
80106fec:	55                   	push   %ebp
80106fed:	89 e5                	mov    %esp,%ebp
    cs.tick_count = 0;
80106fef:	c7 05 20 6b 11 80 00 	movl   $0x0,0x80116b20
80106ff6:	00 00 00 
    cs.proc_exit_count = 0;
80106ff9:	c7 05 24 6b 11 80 00 	movl   $0x0,0x80116b24
80107000:	00 00 00 
    cs.proc_tick_count = 0;
80107003:	c7 05 28 6b 11 80 00 	movl   $0x0,0x80116b28
8010700a:	00 00 00 
    cs.proc_firstrun_count = 0;
8010700d:	c7 05 2c 6b 11 80 00 	movl   $0x0,0x80116b2c
80107014:	00 00 00 
    capturing = 1;
80107017:	c7 05 a0 c6 10 80 01 	movl   $0x1,0x8010c6a0
8010701e:	00 00 00 
}
80107021:	5d                   	pop    %ebp
80107022:	c3                   	ret    

80107023 <stop_capture>:

void stop_capture(struct capture_stats* p)
{
80107023:	55                   	push   %ebp
80107024:	89 e5                	mov    %esp,%ebp
    capturing = 0;
80107026:	c7 05 a0 c6 10 80 00 	movl   $0x0,0x8010c6a0
8010702d:	00 00 00 
    p->tick_count = cs.tick_count;
80107030:	8b 15 20 6b 11 80    	mov    0x80116b20,%edx
80107036:	8b 45 08             	mov    0x8(%ebp),%eax
80107039:	89 10                	mov    %edx,(%eax)
    p->proc_exit_count = cs.proc_exit_count;
8010703b:	8b 15 24 6b 11 80    	mov    0x80116b24,%edx
80107041:	8b 45 08             	mov    0x8(%ebp),%eax
80107044:	89 50 04             	mov    %edx,0x4(%eax)
    p->proc_tick_count = cs.proc_tick_count;
80107047:	8b 15 28 6b 11 80    	mov    0x80116b28,%edx
8010704d:	8b 45 08             	mov    0x8(%ebp),%eax
80107050:	89 50 08             	mov    %edx,0x8(%eax)
    p->proc_firstrun_count = cs.proc_firstrun_count;
80107053:	8b 15 2c 6b 11 80    	mov    0x80116b2c,%edx
80107059:	8b 45 08             	mov    0x8(%ebp),%eax
8010705c:	89 50 0c             	mov    %edx,0xc(%eax)
}
8010705f:	5d                   	pop    %ebp
80107060:	c3                   	ret    

80107061 <sys_start_capture>:

int sys_start_capture(void)
{
80107061:	55                   	push   %ebp
80107062:	89 e5                	mov    %esp,%ebp
    start_capture();
80107064:	e8 83 ff ff ff       	call   80106fec <start_capture>
    return 0;
80107069:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010706e:	5d                   	pop    %ebp
8010706f:	c3                   	ret    

80107070 <sys_stop_capture>:

int sys_stop_capture(void)
{
80107070:	55                   	push   %ebp
80107071:	89 e5                	mov    %esp,%ebp
80107073:	83 ec 28             	sub    $0x28,%esp
    struct capture_stats *p;
    if(argptr(0, (void*)&p, sizeof(*p)) < 0)
80107076:	8d 45 f4             	lea    -0xc(%ebp),%eax
80107079:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
80107080:	00 
80107081:	89 44 24 04          	mov    %eax,0x4(%esp)
80107085:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
8010708c:	e8 a4 ec ff ff       	call   80105d35 <argptr>
80107091:	85 c0                	test   %eax,%eax
80107093:	79 07                	jns    8010709c <sys_stop_capture+0x2c>
        return -1;
80107095:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010709a:	eb 10                	jmp    801070ac <sys_stop_capture+0x3c>
    stop_capture(p);
8010709c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010709f:	89 04 24             	mov    %eax,(%esp)
801070a2:	e8 7c ff ff ff       	call   80107023 <stop_capture>
    return 0;
801070a7:	b8 00 00 00 00       	mov    $0x0,%eax
}
801070ac:	c9                   	leave  
801070ad:	c3                   	ret    

801070ae <trap>:

void
trap(struct trapframe *tf)
{
801070ae:	55                   	push   %ebp
801070af:	89 e5                	mov    %esp,%ebp
801070b1:	57                   	push   %edi
801070b2:	56                   	push   %esi
801070b3:	53                   	push   %ebx
801070b4:	83 ec 3c             	sub    $0x3c,%esp
  if(tf->trapno == T_SYSCALL){
801070b7:	8b 45 08             	mov    0x8(%ebp),%eax
801070ba:	8b 40 30             	mov    0x30(%eax),%eax
801070bd:	83 f8 40             	cmp    $0x40,%eax
801070c0:	75 3e                	jne    80107100 <trap+0x52>
    if(proc->killed)
801070c2:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801070c8:	8b 40 24             	mov    0x24(%eax),%eax
801070cb:	85 c0                	test   %eax,%eax
801070cd:	74 05                	je     801070d4 <trap+0x26>
      exit();
801070cf:	e8 af dd ff ff       	call   80104e83 <exit>
    proc->tf = tf;
801070d4:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801070da:	8b 55 08             	mov    0x8(%ebp),%edx
801070dd:	89 50 18             	mov    %edx,0x18(%eax)
    syscall();
801070e0:	e8 e9 ec ff ff       	call   80105dce <syscall>
    if(proc->killed)
801070e5:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801070eb:	8b 40 24             	mov    0x24(%eax),%eax
801070ee:	85 c0                	test   %eax,%eax
801070f0:	0f 84 4a 02 00 00    	je     80107340 <trap+0x292>
      exit();
801070f6:	e8 88 dd ff ff       	call   80104e83 <exit>
    return;
801070fb:	e9 41 02 00 00       	jmp    80107341 <trap+0x293>
  }

  switch(tf->trapno){
80107100:	8b 45 08             	mov    0x8(%ebp),%eax
80107103:	8b 40 30             	mov    0x30(%eax),%eax
80107106:	83 e8 20             	sub    $0x20,%eax
80107109:	83 f8 1f             	cmp    $0x1f,%eax
8010710c:	0f 87 d2 00 00 00    	ja     801071e4 <trap+0x136>
80107112:	8b 04 85 78 95 10 80 	mov    -0x7fef6a88(,%eax,4),%eax
80107119:	ff e0                	jmp    *%eax
  case T_IRQ0 + IRQ_TIMER:
    if(cpu->id == 0){
8010711b:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80107121:	0f b6 00             	movzbl (%eax),%eax
80107124:	84 c0                	test   %al,%al
80107126:	75 47                	jne    8010716f <trap+0xc1>
      acquire(&tickslock);
80107128:	c7 04 24 40 6b 11 80 	movl   $0x80116b40,(%esp)
8010712f:	e8 13 e6 ff ff       	call   80105747 <acquire>
      if(capturing)
80107134:	a1 a0 c6 10 80       	mov    0x8010c6a0,%eax
80107139:	85 c0                	test   %eax,%eax
8010713b:	74 0d                	je     8010714a <trap+0x9c>
          cs.tick_count++;
8010713d:	a1 20 6b 11 80       	mov    0x80116b20,%eax
80107142:	83 c0 01             	add    $0x1,%eax
80107145:	a3 20 6b 11 80       	mov    %eax,0x80116b20
      ticks++;
8010714a:	a1 74 6b 11 80       	mov    0x80116b74,%eax
8010714f:	83 c0 01             	add    $0x1,%eax
80107152:	a3 74 6b 11 80       	mov    %eax,0x80116b74
      wakeup(&ticks);
80107157:	c7 04 24 74 6b 11 80 	movl   $0x80116b74,(%esp)
8010715e:	e8 98 e3 ff ff       	call   801054fb <wakeup>
      release(&tickslock);
80107163:	c7 04 24 40 6b 11 80 	movl   $0x80116b40,(%esp)
8010716a:	e8 41 e6 ff ff       	call   801057b0 <release>
    }
    lapiceoi();
8010716f:	e8 e2 c5 ff ff       	call   80103756 <lapiceoi>
    break;
80107174:	e9 41 01 00 00       	jmp    801072ba <trap+0x20c>
  case T_IRQ0 + IRQ_IDE:
    ideintr();
80107179:	e8 cf bd ff ff       	call   80102f4d <ideintr>
    lapiceoi();
8010717e:	e8 d3 c5 ff ff       	call   80103756 <lapiceoi>
    break;
80107183:	e9 32 01 00 00       	jmp    801072ba <trap+0x20c>
  case T_IRQ0 + IRQ_IDE+1:
    // Bochs generates spurious IDE1 interrupts.
    break;
  case T_IRQ0 + IRQ_KBD:
    kbdintr();
80107188:	e8 ca c3 ff ff       	call   80103557 <kbdintr>
    lapiceoi();
8010718d:	e8 c4 c5 ff ff       	call   80103756 <lapiceoi>
    break;
80107192:	e9 23 01 00 00       	jmp    801072ba <trap+0x20c>
  case T_IRQ0 + IRQ_COM1:
    uartintr();
80107197:	e8 b9 03 00 00       	call   80107555 <uartintr>
    lapiceoi();
8010719c:	e8 b5 c5 ff ff       	call   80103756 <lapiceoi>
    break;
801071a1:	e9 14 01 00 00       	jmp    801072ba <trap+0x20c>
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
801071a6:	8b 45 08             	mov    0x8(%ebp),%eax
801071a9:	8b 48 38             	mov    0x38(%eax),%ecx
            cpu->id, tf->cs, tf->eip);
801071ac:	8b 45 08             	mov    0x8(%ebp),%eax
801071af:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
    uartintr();
    lapiceoi();
    break;
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
801071b3:	0f b7 d0             	movzwl %ax,%edx
            cpu->id, tf->cs, tf->eip);
801071b6:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801071bc:	0f b6 00             	movzbl (%eax),%eax
    uartintr();
    lapiceoi();
    break;
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
801071bf:	0f b6 c0             	movzbl %al,%eax
801071c2:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
801071c6:	89 54 24 08          	mov    %edx,0x8(%esp)
801071ca:	89 44 24 04          	mov    %eax,0x4(%esp)
801071ce:	c7 04 24 d8 94 10 80 	movl   $0x801094d8,(%esp)
801071d5:	e8 ef 94 ff ff       	call   801006c9 <cprintf>
            cpu->id, tf->cs, tf->eip);
    lapiceoi();
801071da:	e8 77 c5 ff ff       	call   80103756 <lapiceoi>
    break;
801071df:	e9 d6 00 00 00       	jmp    801072ba <trap+0x20c>

  //PAGEBREAK: 13
  default:
    if(proc == 0 || (tf->cs&3) == 0){
801071e4:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801071ea:	85 c0                	test   %eax,%eax
801071ec:	74 11                	je     801071ff <trap+0x151>
801071ee:	8b 45 08             	mov    0x8(%ebp),%eax
801071f1:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
801071f5:	0f b7 c0             	movzwl %ax,%eax
801071f8:	83 e0 03             	and    $0x3,%eax
801071fb:	85 c0                	test   %eax,%eax
801071fd:	75 46                	jne    80107245 <trap+0x197>
      // In kernel, it must be our mistake.
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
801071ff:	e8 2f fc ff ff       	call   80106e33 <rcr2>
80107204:	8b 55 08             	mov    0x8(%ebp),%edx
80107207:	8b 5a 38             	mov    0x38(%edx),%ebx
              tf->trapno, cpu->id, tf->eip, rcr2());
8010720a:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80107211:	0f b6 12             	movzbl (%edx),%edx

  //PAGEBREAK: 13
  default:
    if(proc == 0 || (tf->cs&3) == 0){
      // In kernel, it must be our mistake.
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
80107214:	0f b6 ca             	movzbl %dl,%ecx
80107217:	8b 55 08             	mov    0x8(%ebp),%edx
8010721a:	8b 52 30             	mov    0x30(%edx),%edx
8010721d:	89 44 24 10          	mov    %eax,0x10(%esp)
80107221:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
80107225:	89 4c 24 08          	mov    %ecx,0x8(%esp)
80107229:	89 54 24 04          	mov    %edx,0x4(%esp)
8010722d:	c7 04 24 fc 94 10 80 	movl   $0x801094fc,(%esp)
80107234:	e8 90 94 ff ff       	call   801006c9 <cprintf>
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
80107239:	c7 04 24 2e 95 10 80 	movl   $0x8010952e,(%esp)
80107240:	e8 24 96 ff ff       	call   80100869 <panic>
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80107245:	e8 e9 fb ff ff       	call   80106e33 <rcr2>
8010724a:	89 c2                	mov    %eax,%edx
8010724c:	8b 45 08             	mov    0x8(%ebp),%eax
8010724f:	8b 78 38             	mov    0x38(%eax),%edi
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip,
80107252:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80107258:	0f b6 00             	movzbl (%eax),%eax
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
8010725b:	0f b6 f0             	movzbl %al,%esi
8010725e:	8b 45 08             	mov    0x8(%ebp),%eax
80107261:	8b 58 34             	mov    0x34(%eax),%ebx
80107264:	8b 45 08             	mov    0x8(%ebp),%eax
80107267:	8b 48 30             	mov    0x30(%eax),%ecx
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip,
8010726a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80107270:	83 c0 6c             	add    $0x6c,%eax
80107273:	89 45 e4             	mov    %eax,-0x1c(%ebp)
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip,
80107276:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
8010727c:	8b 40 10             	mov    0x10(%eax),%eax
8010727f:	89 54 24 1c          	mov    %edx,0x1c(%esp)
80107283:	89 7c 24 18          	mov    %edi,0x18(%esp)
80107287:	89 74 24 14          	mov    %esi,0x14(%esp)
8010728b:	89 5c 24 10          	mov    %ebx,0x10(%esp)
8010728f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
80107293:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80107296:	89 54 24 08          	mov    %edx,0x8(%esp)
8010729a:	89 44 24 04          	mov    %eax,0x4(%esp)
8010729e:	c7 04 24 34 95 10 80 	movl   $0x80109534,(%esp)
801072a5:	e8 1f 94 ff ff       	call   801006c9 <cprintf>
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip,
            rcr2());
    proc->killed = 1;
801072aa:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801072b0:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
801072b7:	eb 01                	jmp    801072ba <trap+0x20c>
    ideintr();
    lapiceoi();
    break;
  case T_IRQ0 + IRQ_IDE+1:
    // Bochs generates spurious IDE1 interrupts.
    break;
801072b9:	90                   	nop
  }

  // Force process exit if it has been killed and is in user space.
  // (If it is still executing in the kernel, let it keep running
  // until it gets to the regular system call return.)
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
801072ba:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801072c0:	85 c0                	test   %eax,%eax
801072c2:	74 24                	je     801072e8 <trap+0x23a>
801072c4:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801072ca:	8b 40 24             	mov    0x24(%eax),%eax
801072cd:	85 c0                	test   %eax,%eax
801072cf:	74 17                	je     801072e8 <trap+0x23a>
801072d1:	8b 45 08             	mov    0x8(%ebp),%eax
801072d4:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
801072d8:	0f b7 c0             	movzwl %ax,%eax
801072db:	83 e0 03             	and    $0x3,%eax
801072de:	83 f8 03             	cmp    $0x3,%eax
801072e1:	75 05                	jne    801072e8 <trap+0x23a>
    trap_exit();
801072e3:	e8 f2 fc ff ff       	call   80106fda <trap_exit>

  // Force process to give up CPU on clock tick.
  // If interrupts were on while locks held, would need to check nlock.
  if(proc && proc->state == RUNNING && tf->trapno == T_IRQ0+IRQ_TIMER)
801072e8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801072ee:	85 c0                	test   %eax,%eax
801072f0:	74 1e                	je     80107310 <trap+0x262>
801072f2:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801072f8:	8b 40 0c             	mov    0xc(%eax),%eax
801072fb:	83 f8 04             	cmp    $0x4,%eax
801072fe:	75 10                	jne    80107310 <trap+0x262>
80107300:	8b 45 08             	mov    0x8(%ebp),%eax
80107303:	8b 40 30             	mov    0x30(%eax),%eax
80107306:	83 f8 20             	cmp    $0x20,%eax
80107309:	75 05                	jne    80107310 <trap+0x262>
    yield();
8010730b:	e8 9d e0 ff ff       	call   801053ad <yield>

  // Check if the process has been killed since we yielded
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
80107310:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107316:	85 c0                	test   %eax,%eax
80107318:	74 27                	je     80107341 <trap+0x293>
8010731a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107320:	8b 40 24             	mov    0x24(%eax),%eax
80107323:	85 c0                	test   %eax,%eax
80107325:	74 1a                	je     80107341 <trap+0x293>
80107327:	8b 45 08             	mov    0x8(%ebp),%eax
8010732a:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
8010732e:	0f b7 c0             	movzwl %ax,%eax
80107331:	83 e0 03             	and    $0x3,%eax
80107334:	83 f8 03             	cmp    $0x3,%eax
80107337:	75 08                	jne    80107341 <trap+0x293>
    trap_exit();
80107339:	e8 9c fc ff ff       	call   80106fda <trap_exit>
8010733e:	eb 01                	jmp    80107341 <trap+0x293>
      exit();
    proc->tf = tf;
    syscall();
    if(proc->killed)
      exit();
    return;
80107340:	90                   	nop
    yield();

  // Check if the process has been killed since we yielded
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
    trap_exit();
}
80107341:	83 c4 3c             	add    $0x3c,%esp
80107344:	5b                   	pop    %ebx
80107345:	5e                   	pop    %esi
80107346:	5f                   	pop    %edi
80107347:	5d                   	pop    %ebp
80107348:	c3                   	ret    
80107349:	00 00                	add    %al,(%eax)
	...

8010734c <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
8010734c:	55                   	push   %ebp
8010734d:	89 e5                	mov    %esp,%ebp
8010734f:	83 ec 14             	sub    $0x14,%esp
80107352:	8b 45 08             	mov    0x8(%ebp),%eax
80107355:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80107359:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
8010735d:	89 c2                	mov    %eax,%edx
8010735f:	ec                   	in     (%dx),%al
80107360:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80107363:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80107367:	c9                   	leave  
80107368:	c3                   	ret    

80107369 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80107369:	55                   	push   %ebp
8010736a:	89 e5                	mov    %esp,%ebp
8010736c:	83 ec 08             	sub    $0x8,%esp
8010736f:	8b 55 08             	mov    0x8(%ebp),%edx
80107372:	8b 45 0c             	mov    0xc(%ebp),%eax
80107375:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80107379:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010737c:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80107380:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80107384:	ee                   	out    %al,(%dx)
}
80107385:	c9                   	leave  
80107386:	c3                   	ret    

80107387 <uartinit>:

static int uart;    // is there a uart?

void
uartinit(void)
{
80107387:	55                   	push   %ebp
80107388:	89 e5                	mov    %esp,%ebp
8010738a:	83 ec 28             	sub    $0x28,%esp
  char *p;

  // Turn off the FIFO
  outb(COM1+UART_FIFO_CONTROL, 0);
8010738d:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80107394:	00 
80107395:	c7 04 24 fa 03 00 00 	movl   $0x3fa,(%esp)
8010739c:	e8 c8 ff ff ff       	call   80107369 <outb>

  // 9600 baud, 8 data bits, 1 stop bit, parity off.
  outb(COM1+UART_LINE_CONTROL, UART_DIVISOR_LATCH);     // Unlock divisor
801073a1:	c7 44 24 04 80 00 00 	movl   $0x80,0x4(%esp)
801073a8:	00 
801073a9:	c7 04 24 fb 03 00 00 	movl   $0x3fb,(%esp)
801073b0:	e8 b4 ff ff ff       	call   80107369 <outb>
  outb(COM1+UART_DIVISOR_LOW, 115200/9600);
801073b5:	c7 44 24 04 0c 00 00 	movl   $0xc,0x4(%esp)
801073bc:	00 
801073bd:	c7 04 24 f8 03 00 00 	movl   $0x3f8,(%esp)
801073c4:	e8 a0 ff ff ff       	call   80107369 <outb>
  outb(COM1+UART_DIVISOR_HIGH, 0);
801073c9:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801073d0:	00 
801073d1:	c7 04 24 f9 03 00 00 	movl   $0x3f9,(%esp)
801073d8:	e8 8c ff ff ff       	call   80107369 <outb>
  outb(COM1+UART_LINE_CONTROL, 0x03);   // Lock divisor, 8 data bits.
801073dd:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
801073e4:	00 
801073e5:	c7 04 24 fb 03 00 00 	movl   $0x3fb,(%esp)
801073ec:	e8 78 ff ff ff       	call   80107369 <outb>
  outb(COM1+UART_MODEM_CONTROL, 0);
801073f1:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801073f8:	00 
801073f9:	c7 04 24 fc 03 00 00 	movl   $0x3fc,(%esp)
80107400:	e8 64 ff ff ff       	call   80107369 <outb>

  // Enable receive interrupts.
  outb(COM1+UART_INTERRUPT_ENABLE, UART_RECEIVE_INTERRUPT);
80107405:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
8010740c:	00 
8010740d:	c7 04 24 f9 03 00 00 	movl   $0x3f9,(%esp)
80107414:	e8 50 ff ff ff       	call   80107369 <outb>

  // If status is 0xFF, no serial port.
  if(inb(COM1+UART_LINE_STATUS) == 0xFF)
80107419:	c7 04 24 fd 03 00 00 	movl   $0x3fd,(%esp)
80107420:	e8 27 ff ff ff       	call   8010734c <inb>
80107425:	3c ff                	cmp    $0xff,%al
80107427:	74 6c                	je     80107495 <uartinit+0x10e>
    return;
  uart = 1;
80107429:	c7 05 c0 ce 10 80 01 	movl   $0x1,0x8010cec0
80107430:	00 00 00 

  // Acknowledge pre-existing interrupt conditions;
  // enable interrupts.
  inb(COM1+UART_INTERRUPT_ID);
80107433:	c7 04 24 fa 03 00 00 	movl   $0x3fa,(%esp)
8010743a:	e8 0d ff ff ff       	call   8010734c <inb>
  inb(COM1+UART_RECEIVE_BUFFER);
8010743f:	c7 04 24 f8 03 00 00 	movl   $0x3f8,(%esp)
80107446:	e8 01 ff ff ff       	call   8010734c <inb>
  picenable(IRQ_COM1);
8010744b:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
80107452:	e8 6e d0 ff ff       	call   801044c5 <picenable>
  ioapicenable(IRQ_COM1, 0);
80107457:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
8010745e:	00 
8010745f:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
80107466:	e8 8f bd ff ff       	call   801031fa <ioapicenable>

  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
8010746b:	c7 45 f4 f8 95 10 80 	movl   $0x801095f8,-0xc(%ebp)
80107472:	eb 15                	jmp    80107489 <uartinit+0x102>
    uartputc(*p);
80107474:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107477:	0f b6 00             	movzbl (%eax),%eax
8010747a:	0f be c0             	movsbl %al,%eax
8010747d:	89 04 24             	mov    %eax,(%esp)
80107480:	e8 47 00 00 00       	call   801074cc <uartputc>
  inb(COM1+UART_RECEIVE_BUFFER);
  picenable(IRQ_COM1);
  ioapicenable(IRQ_COM1, 0);

  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
80107485:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80107489:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010748c:	0f b6 00             	movzbl (%eax),%eax
8010748f:	84 c0                	test   %al,%al
80107491:	75 e1                	jne    80107474 <uartinit+0xed>
80107493:	eb 01                	jmp    80107496 <uartinit+0x10f>
  // Enable receive interrupts.
  outb(COM1+UART_INTERRUPT_ENABLE, UART_RECEIVE_INTERRUPT);

  // If status is 0xFF, no serial port.
  if(inb(COM1+UART_LINE_STATUS) == 0xFF)
    return;
80107495:	90                   	nop
  ioapicenable(IRQ_COM1, 0);

  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
    uartputc(*p);
}
80107496:	c9                   	leave  
80107497:	c3                   	ret    

80107498 <cantransmit>:

static int
cantransmit(void)
{
80107498:	55                   	push   %ebp
80107499:	89 e5                	mov    %esp,%ebp
8010749b:	83 ec 04             	sub    $0x4,%esp
  return inb(COM1+UART_LINE_STATUS) & UART_TRANSMIT_READY;
8010749e:	c7 04 24 fd 03 00 00 	movl   $0x3fd,(%esp)
801074a5:	e8 a2 fe ff ff       	call   8010734c <inb>
801074aa:	0f b6 c0             	movzbl %al,%eax
801074ad:	83 e0 20             	and    $0x20,%eax
}
801074b0:	c9                   	leave  
801074b1:	c3                   	ret    

801074b2 <hasreceived>:

static int
hasreceived(void)
{
801074b2:	55                   	push   %ebp
801074b3:	89 e5                	mov    %esp,%ebp
801074b5:	83 ec 04             	sub    $0x4,%esp
  return inb(COM1+UART_LINE_STATUS) & UART_RECEIVE_READY;
801074b8:	c7 04 24 fd 03 00 00 	movl   $0x3fd,(%esp)
801074bf:	e8 88 fe ff ff       	call   8010734c <inb>
801074c4:	0f b6 c0             	movzbl %al,%eax
801074c7:	83 e0 01             	and    $0x1,%eax
}
801074ca:	c9                   	leave  
801074cb:	c3                   	ret    

801074cc <uartputc>:

void
uartputc(int c)
{
801074cc:	55                   	push   %ebp
801074cd:	89 e5                	mov    %esp,%ebp
801074cf:	83 ec 28             	sub    $0x28,%esp
  int i;

  if(!uart)
801074d2:	a1 c0 ce 10 80       	mov    0x8010cec0,%eax
801074d7:	85 c0                	test   %eax,%eax
801074d9:	74 40                	je     8010751b <uartputc+0x4f>
    return;
  for(i = 0; i < 128 && !cantransmit(); i++)
801074db:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
801074e2:	eb 10                	jmp    801074f4 <uartputc+0x28>
    microdelay(10);
801074e4:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
801074eb:	e8 8b c2 ff ff       	call   8010377b <microdelay>
{
  int i;

  if(!uart)
    return;
  for(i = 0; i < 128 && !cantransmit(); i++)
801074f0:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801074f4:	83 7d f4 7f          	cmpl   $0x7f,-0xc(%ebp)
801074f8:	7f 09                	jg     80107503 <uartputc+0x37>
801074fa:	e8 99 ff ff ff       	call   80107498 <cantransmit>
801074ff:	85 c0                	test   %eax,%eax
80107501:	74 e1                	je     801074e4 <uartputc+0x18>
    microdelay(10);
  outb(COM1+UART_TRANSMIT_BUFFER, c);
80107503:	8b 45 08             	mov    0x8(%ebp),%eax
80107506:	0f b6 c0             	movzbl %al,%eax
80107509:	89 44 24 04          	mov    %eax,0x4(%esp)
8010750d:	c7 04 24 f8 03 00 00 	movl   $0x3f8,(%esp)
80107514:	e8 50 fe ff ff       	call   80107369 <outb>
80107519:	eb 01                	jmp    8010751c <uartputc+0x50>
uartputc(int c)
{
  int i;

  if(!uart)
    return;
8010751b:	90                   	nop
  for(i = 0; i < 128 && !cantransmit(); i++)
    microdelay(10);
  outb(COM1+UART_TRANSMIT_BUFFER, c);
}
8010751c:	c9                   	leave  
8010751d:	c3                   	ret    

8010751e <uartgetc>:

static int
uartgetc(void)
{
8010751e:	55                   	push   %ebp
8010751f:	89 e5                	mov    %esp,%ebp
80107521:	83 ec 04             	sub    $0x4,%esp
  if(!uart)
80107524:	a1 c0 ce 10 80       	mov    0x8010cec0,%eax
80107529:	85 c0                	test   %eax,%eax
8010752b:	75 07                	jne    80107534 <uartgetc+0x16>
    return -1;
8010752d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107532:	eb 1f                	jmp    80107553 <uartgetc+0x35>
  if(!hasreceived())
80107534:	e8 79 ff ff ff       	call   801074b2 <hasreceived>
80107539:	85 c0                	test   %eax,%eax
8010753b:	75 07                	jne    80107544 <uartgetc+0x26>
    return -1;
8010753d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107542:	eb 0f                	jmp    80107553 <uartgetc+0x35>
  return inb(COM1+UART_RECEIVE_BUFFER);
80107544:	c7 04 24 f8 03 00 00 	movl   $0x3f8,(%esp)
8010754b:	e8 fc fd ff ff       	call   8010734c <inb>
80107550:	0f b6 c0             	movzbl %al,%eax
}
80107553:	c9                   	leave  
80107554:	c3                   	ret    

80107555 <uartintr>:

void
uartintr(void)
{
80107555:	55                   	push   %ebp
80107556:	89 e5                	mov    %esp,%ebp
80107558:	83 ec 18             	sub    $0x18,%esp
  consoleintr(uartgetc);
8010755b:	c7 04 24 1e 75 10 80 	movl   $0x8010751e,(%esp)
80107562:	e8 96 95 ff ff       	call   80100afd <consoleintr>
}
80107567:	c9                   	leave  
80107568:	c3                   	ret    
80107569:	00 00                	add    %al,(%eax)
	...

8010756c <vector0>:
# generated by vectors.pl - do not edit
# handlers
.globl alltraps
.globl vector0
vector0:
  pushl $0
8010756c:	6a 00                	push   $0x0
  pushl $0
8010756e:	6a 00                	push   $0x0
  jmp alltraps
80107570:	e9 63 f8 ff ff       	jmp    80106dd8 <alltraps>

80107575 <vector1>:
.globl vector1
vector1:
  pushl $0
80107575:	6a 00                	push   $0x0
  pushl $1
80107577:	6a 01                	push   $0x1
  jmp alltraps
80107579:	e9 5a f8 ff ff       	jmp    80106dd8 <alltraps>

8010757e <vector2>:
.globl vector2
vector2:
  pushl $0
8010757e:	6a 00                	push   $0x0
  pushl $2
80107580:	6a 02                	push   $0x2
  jmp alltraps
80107582:	e9 51 f8 ff ff       	jmp    80106dd8 <alltraps>

80107587 <vector3>:
.globl vector3
vector3:
  pushl $0
80107587:	6a 00                	push   $0x0
  pushl $3
80107589:	6a 03                	push   $0x3
  jmp alltraps
8010758b:	e9 48 f8 ff ff       	jmp    80106dd8 <alltraps>

80107590 <vector4>:
.globl vector4
vector4:
  pushl $0
80107590:	6a 00                	push   $0x0
  pushl $4
80107592:	6a 04                	push   $0x4
  jmp alltraps
80107594:	e9 3f f8 ff ff       	jmp    80106dd8 <alltraps>

80107599 <vector5>:
.globl vector5
vector5:
  pushl $0
80107599:	6a 00                	push   $0x0
  pushl $5
8010759b:	6a 05                	push   $0x5
  jmp alltraps
8010759d:	e9 36 f8 ff ff       	jmp    80106dd8 <alltraps>

801075a2 <vector6>:
.globl vector6
vector6:
  pushl $0
801075a2:	6a 00                	push   $0x0
  pushl $6
801075a4:	6a 06                	push   $0x6
  jmp alltraps
801075a6:	e9 2d f8 ff ff       	jmp    80106dd8 <alltraps>

801075ab <vector7>:
.globl vector7
vector7:
  pushl $0
801075ab:	6a 00                	push   $0x0
  pushl $7
801075ad:	6a 07                	push   $0x7
  jmp alltraps
801075af:	e9 24 f8 ff ff       	jmp    80106dd8 <alltraps>

801075b4 <vector8>:
.globl vector8
vector8:
  pushl $8
801075b4:	6a 08                	push   $0x8
  jmp alltraps
801075b6:	e9 1d f8 ff ff       	jmp    80106dd8 <alltraps>

801075bb <vector9>:
.globl vector9
vector9:
  pushl $0
801075bb:	6a 00                	push   $0x0
  pushl $9
801075bd:	6a 09                	push   $0x9
  jmp alltraps
801075bf:	e9 14 f8 ff ff       	jmp    80106dd8 <alltraps>

801075c4 <vector10>:
.globl vector10
vector10:
  pushl $10
801075c4:	6a 0a                	push   $0xa
  jmp alltraps
801075c6:	e9 0d f8 ff ff       	jmp    80106dd8 <alltraps>

801075cb <vector11>:
.globl vector11
vector11:
  pushl $11
801075cb:	6a 0b                	push   $0xb
  jmp alltraps
801075cd:	e9 06 f8 ff ff       	jmp    80106dd8 <alltraps>

801075d2 <vector12>:
.globl vector12
vector12:
  pushl $12
801075d2:	6a 0c                	push   $0xc
  jmp alltraps
801075d4:	e9 ff f7 ff ff       	jmp    80106dd8 <alltraps>

801075d9 <vector13>:
.globl vector13
vector13:
  pushl $13
801075d9:	6a 0d                	push   $0xd
  jmp alltraps
801075db:	e9 f8 f7 ff ff       	jmp    80106dd8 <alltraps>

801075e0 <vector14>:
.globl vector14
vector14:
  pushl $14
801075e0:	6a 0e                	push   $0xe
  jmp alltraps
801075e2:	e9 f1 f7 ff ff       	jmp    80106dd8 <alltraps>

801075e7 <vector15>:
.globl vector15
vector15:
  pushl $0
801075e7:	6a 00                	push   $0x0
  pushl $15
801075e9:	6a 0f                	push   $0xf
  jmp alltraps
801075eb:	e9 e8 f7 ff ff       	jmp    80106dd8 <alltraps>

801075f0 <vector16>:
.globl vector16
vector16:
  pushl $0
801075f0:	6a 00                	push   $0x0
  pushl $16
801075f2:	6a 10                	push   $0x10
  jmp alltraps
801075f4:	e9 df f7 ff ff       	jmp    80106dd8 <alltraps>

801075f9 <vector17>:
.globl vector17
vector17:
  pushl $17
801075f9:	6a 11                	push   $0x11
  jmp alltraps
801075fb:	e9 d8 f7 ff ff       	jmp    80106dd8 <alltraps>

80107600 <vector18>:
.globl vector18
vector18:
  pushl $0
80107600:	6a 00                	push   $0x0
  pushl $18
80107602:	6a 12                	push   $0x12
  jmp alltraps
80107604:	e9 cf f7 ff ff       	jmp    80106dd8 <alltraps>

80107609 <vector19>:
.globl vector19
vector19:
  pushl $0
80107609:	6a 00                	push   $0x0
  pushl $19
8010760b:	6a 13                	push   $0x13
  jmp alltraps
8010760d:	e9 c6 f7 ff ff       	jmp    80106dd8 <alltraps>

80107612 <vector20>:
.globl vector20
vector20:
  pushl $0
80107612:	6a 00                	push   $0x0
  pushl $20
80107614:	6a 14                	push   $0x14
  jmp alltraps
80107616:	e9 bd f7 ff ff       	jmp    80106dd8 <alltraps>

8010761b <vector21>:
.globl vector21
vector21:
  pushl $0
8010761b:	6a 00                	push   $0x0
  pushl $21
8010761d:	6a 15                	push   $0x15
  jmp alltraps
8010761f:	e9 b4 f7 ff ff       	jmp    80106dd8 <alltraps>

80107624 <vector22>:
.globl vector22
vector22:
  pushl $0
80107624:	6a 00                	push   $0x0
  pushl $22
80107626:	6a 16                	push   $0x16
  jmp alltraps
80107628:	e9 ab f7 ff ff       	jmp    80106dd8 <alltraps>

8010762d <vector23>:
.globl vector23
vector23:
  pushl $0
8010762d:	6a 00                	push   $0x0
  pushl $23
8010762f:	6a 17                	push   $0x17
  jmp alltraps
80107631:	e9 a2 f7 ff ff       	jmp    80106dd8 <alltraps>

80107636 <vector24>:
.globl vector24
vector24:
  pushl $0
80107636:	6a 00                	push   $0x0
  pushl $24
80107638:	6a 18                	push   $0x18
  jmp alltraps
8010763a:	e9 99 f7 ff ff       	jmp    80106dd8 <alltraps>

8010763f <vector25>:
.globl vector25
vector25:
  pushl $0
8010763f:	6a 00                	push   $0x0
  pushl $25
80107641:	6a 19                	push   $0x19
  jmp alltraps
80107643:	e9 90 f7 ff ff       	jmp    80106dd8 <alltraps>

80107648 <vector26>:
.globl vector26
vector26:
  pushl $0
80107648:	6a 00                	push   $0x0
  pushl $26
8010764a:	6a 1a                	push   $0x1a
  jmp alltraps
8010764c:	e9 87 f7 ff ff       	jmp    80106dd8 <alltraps>

80107651 <vector27>:
.globl vector27
vector27:
  pushl $0
80107651:	6a 00                	push   $0x0
  pushl $27
80107653:	6a 1b                	push   $0x1b
  jmp alltraps
80107655:	e9 7e f7 ff ff       	jmp    80106dd8 <alltraps>

8010765a <vector28>:
.globl vector28
vector28:
  pushl $0
8010765a:	6a 00                	push   $0x0
  pushl $28
8010765c:	6a 1c                	push   $0x1c
  jmp alltraps
8010765e:	e9 75 f7 ff ff       	jmp    80106dd8 <alltraps>

80107663 <vector29>:
.globl vector29
vector29:
  pushl $0
80107663:	6a 00                	push   $0x0
  pushl $29
80107665:	6a 1d                	push   $0x1d
  jmp alltraps
80107667:	e9 6c f7 ff ff       	jmp    80106dd8 <alltraps>

8010766c <vector30>:
.globl vector30
vector30:
  pushl $0
8010766c:	6a 00                	push   $0x0
  pushl $30
8010766e:	6a 1e                	push   $0x1e
  jmp alltraps
80107670:	e9 63 f7 ff ff       	jmp    80106dd8 <alltraps>

80107675 <vector31>:
.globl vector31
vector31:
  pushl $0
80107675:	6a 00                	push   $0x0
  pushl $31
80107677:	6a 1f                	push   $0x1f
  jmp alltraps
80107679:	e9 5a f7 ff ff       	jmp    80106dd8 <alltraps>

8010767e <vector32>:
.globl vector32
vector32:
  pushl $0
8010767e:	6a 00                	push   $0x0
  pushl $32
80107680:	6a 20                	push   $0x20
  jmp alltraps
80107682:	e9 51 f7 ff ff       	jmp    80106dd8 <alltraps>

80107687 <vector33>:
.globl vector33
vector33:
  pushl $0
80107687:	6a 00                	push   $0x0
  pushl $33
80107689:	6a 21                	push   $0x21
  jmp alltraps
8010768b:	e9 48 f7 ff ff       	jmp    80106dd8 <alltraps>

80107690 <vector34>:
.globl vector34
vector34:
  pushl $0
80107690:	6a 00                	push   $0x0
  pushl $34
80107692:	6a 22                	push   $0x22
  jmp alltraps
80107694:	e9 3f f7 ff ff       	jmp    80106dd8 <alltraps>

80107699 <vector35>:
.globl vector35
vector35:
  pushl $0
80107699:	6a 00                	push   $0x0
  pushl $35
8010769b:	6a 23                	push   $0x23
  jmp alltraps
8010769d:	e9 36 f7 ff ff       	jmp    80106dd8 <alltraps>

801076a2 <vector36>:
.globl vector36
vector36:
  pushl $0
801076a2:	6a 00                	push   $0x0
  pushl $36
801076a4:	6a 24                	push   $0x24
  jmp alltraps
801076a6:	e9 2d f7 ff ff       	jmp    80106dd8 <alltraps>

801076ab <vector37>:
.globl vector37
vector37:
  pushl $0
801076ab:	6a 00                	push   $0x0
  pushl $37
801076ad:	6a 25                	push   $0x25
  jmp alltraps
801076af:	e9 24 f7 ff ff       	jmp    80106dd8 <alltraps>

801076b4 <vector38>:
.globl vector38
vector38:
  pushl $0
801076b4:	6a 00                	push   $0x0
  pushl $38
801076b6:	6a 26                	push   $0x26
  jmp alltraps
801076b8:	e9 1b f7 ff ff       	jmp    80106dd8 <alltraps>

801076bd <vector39>:
.globl vector39
vector39:
  pushl $0
801076bd:	6a 00                	push   $0x0
  pushl $39
801076bf:	6a 27                	push   $0x27
  jmp alltraps
801076c1:	e9 12 f7 ff ff       	jmp    80106dd8 <alltraps>

801076c6 <vector40>:
.globl vector40
vector40:
  pushl $0
801076c6:	6a 00                	push   $0x0
  pushl $40
801076c8:	6a 28                	push   $0x28
  jmp alltraps
801076ca:	e9 09 f7 ff ff       	jmp    80106dd8 <alltraps>

801076cf <vector41>:
.globl vector41
vector41:
  pushl $0
801076cf:	6a 00                	push   $0x0
  pushl $41
801076d1:	6a 29                	push   $0x29
  jmp alltraps
801076d3:	e9 00 f7 ff ff       	jmp    80106dd8 <alltraps>

801076d8 <vector42>:
.globl vector42
vector42:
  pushl $0
801076d8:	6a 00                	push   $0x0
  pushl $42
801076da:	6a 2a                	push   $0x2a
  jmp alltraps
801076dc:	e9 f7 f6 ff ff       	jmp    80106dd8 <alltraps>

801076e1 <vector43>:
.globl vector43
vector43:
  pushl $0
801076e1:	6a 00                	push   $0x0
  pushl $43
801076e3:	6a 2b                	push   $0x2b
  jmp alltraps
801076e5:	e9 ee f6 ff ff       	jmp    80106dd8 <alltraps>

801076ea <vector44>:
.globl vector44
vector44:
  pushl $0
801076ea:	6a 00                	push   $0x0
  pushl $44
801076ec:	6a 2c                	push   $0x2c
  jmp alltraps
801076ee:	e9 e5 f6 ff ff       	jmp    80106dd8 <alltraps>

801076f3 <vector45>:
.globl vector45
vector45:
  pushl $0
801076f3:	6a 00                	push   $0x0
  pushl $45
801076f5:	6a 2d                	push   $0x2d
  jmp alltraps
801076f7:	e9 dc f6 ff ff       	jmp    80106dd8 <alltraps>

801076fc <vector46>:
.globl vector46
vector46:
  pushl $0
801076fc:	6a 00                	push   $0x0
  pushl $46
801076fe:	6a 2e                	push   $0x2e
  jmp alltraps
80107700:	e9 d3 f6 ff ff       	jmp    80106dd8 <alltraps>

80107705 <vector47>:
.globl vector47
vector47:
  pushl $0
80107705:	6a 00                	push   $0x0
  pushl $47
80107707:	6a 2f                	push   $0x2f
  jmp alltraps
80107709:	e9 ca f6 ff ff       	jmp    80106dd8 <alltraps>

8010770e <vector48>:
.globl vector48
vector48:
  pushl $0
8010770e:	6a 00                	push   $0x0
  pushl $48
80107710:	6a 30                	push   $0x30
  jmp alltraps
80107712:	e9 c1 f6 ff ff       	jmp    80106dd8 <alltraps>

80107717 <vector49>:
.globl vector49
vector49:
  pushl $0
80107717:	6a 00                	push   $0x0
  pushl $49
80107719:	6a 31                	push   $0x31
  jmp alltraps
8010771b:	e9 b8 f6 ff ff       	jmp    80106dd8 <alltraps>

80107720 <vector50>:
.globl vector50
vector50:
  pushl $0
80107720:	6a 00                	push   $0x0
  pushl $50
80107722:	6a 32                	push   $0x32
  jmp alltraps
80107724:	e9 af f6 ff ff       	jmp    80106dd8 <alltraps>

80107729 <vector51>:
.globl vector51
vector51:
  pushl $0
80107729:	6a 00                	push   $0x0
  pushl $51
8010772b:	6a 33                	push   $0x33
  jmp alltraps
8010772d:	e9 a6 f6 ff ff       	jmp    80106dd8 <alltraps>

80107732 <vector52>:
.globl vector52
vector52:
  pushl $0
80107732:	6a 00                	push   $0x0
  pushl $52
80107734:	6a 34                	push   $0x34
  jmp alltraps
80107736:	e9 9d f6 ff ff       	jmp    80106dd8 <alltraps>

8010773b <vector53>:
.globl vector53
vector53:
  pushl $0
8010773b:	6a 00                	push   $0x0
  pushl $53
8010773d:	6a 35                	push   $0x35
  jmp alltraps
8010773f:	e9 94 f6 ff ff       	jmp    80106dd8 <alltraps>

80107744 <vector54>:
.globl vector54
vector54:
  pushl $0
80107744:	6a 00                	push   $0x0
  pushl $54
80107746:	6a 36                	push   $0x36
  jmp alltraps
80107748:	e9 8b f6 ff ff       	jmp    80106dd8 <alltraps>

8010774d <vector55>:
.globl vector55
vector55:
  pushl $0
8010774d:	6a 00                	push   $0x0
  pushl $55
8010774f:	6a 37                	push   $0x37
  jmp alltraps
80107751:	e9 82 f6 ff ff       	jmp    80106dd8 <alltraps>

80107756 <vector56>:
.globl vector56
vector56:
  pushl $0
80107756:	6a 00                	push   $0x0
  pushl $56
80107758:	6a 38                	push   $0x38
  jmp alltraps
8010775a:	e9 79 f6 ff ff       	jmp    80106dd8 <alltraps>

8010775f <vector57>:
.globl vector57
vector57:
  pushl $0
8010775f:	6a 00                	push   $0x0
  pushl $57
80107761:	6a 39                	push   $0x39
  jmp alltraps
80107763:	e9 70 f6 ff ff       	jmp    80106dd8 <alltraps>

80107768 <vector58>:
.globl vector58
vector58:
  pushl $0
80107768:	6a 00                	push   $0x0
  pushl $58
8010776a:	6a 3a                	push   $0x3a
  jmp alltraps
8010776c:	e9 67 f6 ff ff       	jmp    80106dd8 <alltraps>

80107771 <vector59>:
.globl vector59
vector59:
  pushl $0
80107771:	6a 00                	push   $0x0
  pushl $59
80107773:	6a 3b                	push   $0x3b
  jmp alltraps
80107775:	e9 5e f6 ff ff       	jmp    80106dd8 <alltraps>

8010777a <vector60>:
.globl vector60
vector60:
  pushl $0
8010777a:	6a 00                	push   $0x0
  pushl $60
8010777c:	6a 3c                	push   $0x3c
  jmp alltraps
8010777e:	e9 55 f6 ff ff       	jmp    80106dd8 <alltraps>

80107783 <vector61>:
.globl vector61
vector61:
  pushl $0
80107783:	6a 00                	push   $0x0
  pushl $61
80107785:	6a 3d                	push   $0x3d
  jmp alltraps
80107787:	e9 4c f6 ff ff       	jmp    80106dd8 <alltraps>

8010778c <vector62>:
.globl vector62
vector62:
  pushl $0
8010778c:	6a 00                	push   $0x0
  pushl $62
8010778e:	6a 3e                	push   $0x3e
  jmp alltraps
80107790:	e9 43 f6 ff ff       	jmp    80106dd8 <alltraps>

80107795 <vector63>:
.globl vector63
vector63:
  pushl $0
80107795:	6a 00                	push   $0x0
  pushl $63
80107797:	6a 3f                	push   $0x3f
  jmp alltraps
80107799:	e9 3a f6 ff ff       	jmp    80106dd8 <alltraps>

8010779e <vector64>:
.globl vector64
vector64:
  pushl $0
8010779e:	6a 00                	push   $0x0
  pushl $64
801077a0:	6a 40                	push   $0x40
  jmp alltraps
801077a2:	e9 31 f6 ff ff       	jmp    80106dd8 <alltraps>

801077a7 <vector65>:
.globl vector65
vector65:
  pushl $0
801077a7:	6a 00                	push   $0x0
  pushl $65
801077a9:	6a 41                	push   $0x41
  jmp alltraps
801077ab:	e9 28 f6 ff ff       	jmp    80106dd8 <alltraps>

801077b0 <vector66>:
.globl vector66
vector66:
  pushl $0
801077b0:	6a 00                	push   $0x0
  pushl $66
801077b2:	6a 42                	push   $0x42
  jmp alltraps
801077b4:	e9 1f f6 ff ff       	jmp    80106dd8 <alltraps>

801077b9 <vector67>:
.globl vector67
vector67:
  pushl $0
801077b9:	6a 00                	push   $0x0
  pushl $67
801077bb:	6a 43                	push   $0x43
  jmp alltraps
801077bd:	e9 16 f6 ff ff       	jmp    80106dd8 <alltraps>

801077c2 <vector68>:
.globl vector68
vector68:
  pushl $0
801077c2:	6a 00                	push   $0x0
  pushl $68
801077c4:	6a 44                	push   $0x44
  jmp alltraps
801077c6:	e9 0d f6 ff ff       	jmp    80106dd8 <alltraps>

801077cb <vector69>:
.globl vector69
vector69:
  pushl $0
801077cb:	6a 00                	push   $0x0
  pushl $69
801077cd:	6a 45                	push   $0x45
  jmp alltraps
801077cf:	e9 04 f6 ff ff       	jmp    80106dd8 <alltraps>

801077d4 <vector70>:
.globl vector70
vector70:
  pushl $0
801077d4:	6a 00                	push   $0x0
  pushl $70
801077d6:	6a 46                	push   $0x46
  jmp alltraps
801077d8:	e9 fb f5 ff ff       	jmp    80106dd8 <alltraps>

801077dd <vector71>:
.globl vector71
vector71:
  pushl $0
801077dd:	6a 00                	push   $0x0
  pushl $71
801077df:	6a 47                	push   $0x47
  jmp alltraps
801077e1:	e9 f2 f5 ff ff       	jmp    80106dd8 <alltraps>

801077e6 <vector72>:
.globl vector72
vector72:
  pushl $0
801077e6:	6a 00                	push   $0x0
  pushl $72
801077e8:	6a 48                	push   $0x48
  jmp alltraps
801077ea:	e9 e9 f5 ff ff       	jmp    80106dd8 <alltraps>

801077ef <vector73>:
.globl vector73
vector73:
  pushl $0
801077ef:	6a 00                	push   $0x0
  pushl $73
801077f1:	6a 49                	push   $0x49
  jmp alltraps
801077f3:	e9 e0 f5 ff ff       	jmp    80106dd8 <alltraps>

801077f8 <vector74>:
.globl vector74
vector74:
  pushl $0
801077f8:	6a 00                	push   $0x0
  pushl $74
801077fa:	6a 4a                	push   $0x4a
  jmp alltraps
801077fc:	e9 d7 f5 ff ff       	jmp    80106dd8 <alltraps>

80107801 <vector75>:
.globl vector75
vector75:
  pushl $0
80107801:	6a 00                	push   $0x0
  pushl $75
80107803:	6a 4b                	push   $0x4b
  jmp alltraps
80107805:	e9 ce f5 ff ff       	jmp    80106dd8 <alltraps>

8010780a <vector76>:
.globl vector76
vector76:
  pushl $0
8010780a:	6a 00                	push   $0x0
  pushl $76
8010780c:	6a 4c                	push   $0x4c
  jmp alltraps
8010780e:	e9 c5 f5 ff ff       	jmp    80106dd8 <alltraps>

80107813 <vector77>:
.globl vector77
vector77:
  pushl $0
80107813:	6a 00                	push   $0x0
  pushl $77
80107815:	6a 4d                	push   $0x4d
  jmp alltraps
80107817:	e9 bc f5 ff ff       	jmp    80106dd8 <alltraps>

8010781c <vector78>:
.globl vector78
vector78:
  pushl $0
8010781c:	6a 00                	push   $0x0
  pushl $78
8010781e:	6a 4e                	push   $0x4e
  jmp alltraps
80107820:	e9 b3 f5 ff ff       	jmp    80106dd8 <alltraps>

80107825 <vector79>:
.globl vector79
vector79:
  pushl $0
80107825:	6a 00                	push   $0x0
  pushl $79
80107827:	6a 4f                	push   $0x4f
  jmp alltraps
80107829:	e9 aa f5 ff ff       	jmp    80106dd8 <alltraps>

8010782e <vector80>:
.globl vector80
vector80:
  pushl $0
8010782e:	6a 00                	push   $0x0
  pushl $80
80107830:	6a 50                	push   $0x50
  jmp alltraps
80107832:	e9 a1 f5 ff ff       	jmp    80106dd8 <alltraps>

80107837 <vector81>:
.globl vector81
vector81:
  pushl $0
80107837:	6a 00                	push   $0x0
  pushl $81
80107839:	6a 51                	push   $0x51
  jmp alltraps
8010783b:	e9 98 f5 ff ff       	jmp    80106dd8 <alltraps>

80107840 <vector82>:
.globl vector82
vector82:
  pushl $0
80107840:	6a 00                	push   $0x0
  pushl $82
80107842:	6a 52                	push   $0x52
  jmp alltraps
80107844:	e9 8f f5 ff ff       	jmp    80106dd8 <alltraps>

80107849 <vector83>:
.globl vector83
vector83:
  pushl $0
80107849:	6a 00                	push   $0x0
  pushl $83
8010784b:	6a 53                	push   $0x53
  jmp alltraps
8010784d:	e9 86 f5 ff ff       	jmp    80106dd8 <alltraps>

80107852 <vector84>:
.globl vector84
vector84:
  pushl $0
80107852:	6a 00                	push   $0x0
  pushl $84
80107854:	6a 54                	push   $0x54
  jmp alltraps
80107856:	e9 7d f5 ff ff       	jmp    80106dd8 <alltraps>

8010785b <vector85>:
.globl vector85
vector85:
  pushl $0
8010785b:	6a 00                	push   $0x0
  pushl $85
8010785d:	6a 55                	push   $0x55
  jmp alltraps
8010785f:	e9 74 f5 ff ff       	jmp    80106dd8 <alltraps>

80107864 <vector86>:
.globl vector86
vector86:
  pushl $0
80107864:	6a 00                	push   $0x0
  pushl $86
80107866:	6a 56                	push   $0x56
  jmp alltraps
80107868:	e9 6b f5 ff ff       	jmp    80106dd8 <alltraps>

8010786d <vector87>:
.globl vector87
vector87:
  pushl $0
8010786d:	6a 00                	push   $0x0
  pushl $87
8010786f:	6a 57                	push   $0x57
  jmp alltraps
80107871:	e9 62 f5 ff ff       	jmp    80106dd8 <alltraps>

80107876 <vector88>:
.globl vector88
vector88:
  pushl $0
80107876:	6a 00                	push   $0x0
  pushl $88
80107878:	6a 58                	push   $0x58
  jmp alltraps
8010787a:	e9 59 f5 ff ff       	jmp    80106dd8 <alltraps>

8010787f <vector89>:
.globl vector89
vector89:
  pushl $0
8010787f:	6a 00                	push   $0x0
  pushl $89
80107881:	6a 59                	push   $0x59
  jmp alltraps
80107883:	e9 50 f5 ff ff       	jmp    80106dd8 <alltraps>

80107888 <vector90>:
.globl vector90
vector90:
  pushl $0
80107888:	6a 00                	push   $0x0
  pushl $90
8010788a:	6a 5a                	push   $0x5a
  jmp alltraps
8010788c:	e9 47 f5 ff ff       	jmp    80106dd8 <alltraps>

80107891 <vector91>:
.globl vector91
vector91:
  pushl $0
80107891:	6a 00                	push   $0x0
  pushl $91
80107893:	6a 5b                	push   $0x5b
  jmp alltraps
80107895:	e9 3e f5 ff ff       	jmp    80106dd8 <alltraps>

8010789a <vector92>:
.globl vector92
vector92:
  pushl $0
8010789a:	6a 00                	push   $0x0
  pushl $92
8010789c:	6a 5c                	push   $0x5c
  jmp alltraps
8010789e:	e9 35 f5 ff ff       	jmp    80106dd8 <alltraps>

801078a3 <vector93>:
.globl vector93
vector93:
  pushl $0
801078a3:	6a 00                	push   $0x0
  pushl $93
801078a5:	6a 5d                	push   $0x5d
  jmp alltraps
801078a7:	e9 2c f5 ff ff       	jmp    80106dd8 <alltraps>

801078ac <vector94>:
.globl vector94
vector94:
  pushl $0
801078ac:	6a 00                	push   $0x0
  pushl $94
801078ae:	6a 5e                	push   $0x5e
  jmp alltraps
801078b0:	e9 23 f5 ff ff       	jmp    80106dd8 <alltraps>

801078b5 <vector95>:
.globl vector95
vector95:
  pushl $0
801078b5:	6a 00                	push   $0x0
  pushl $95
801078b7:	6a 5f                	push   $0x5f
  jmp alltraps
801078b9:	e9 1a f5 ff ff       	jmp    80106dd8 <alltraps>

801078be <vector96>:
.globl vector96
vector96:
  pushl $0
801078be:	6a 00                	push   $0x0
  pushl $96
801078c0:	6a 60                	push   $0x60
  jmp alltraps
801078c2:	e9 11 f5 ff ff       	jmp    80106dd8 <alltraps>

801078c7 <vector97>:
.globl vector97
vector97:
  pushl $0
801078c7:	6a 00                	push   $0x0
  pushl $97
801078c9:	6a 61                	push   $0x61
  jmp alltraps
801078cb:	e9 08 f5 ff ff       	jmp    80106dd8 <alltraps>

801078d0 <vector98>:
.globl vector98
vector98:
  pushl $0
801078d0:	6a 00                	push   $0x0
  pushl $98
801078d2:	6a 62                	push   $0x62
  jmp alltraps
801078d4:	e9 ff f4 ff ff       	jmp    80106dd8 <alltraps>

801078d9 <vector99>:
.globl vector99
vector99:
  pushl $0
801078d9:	6a 00                	push   $0x0
  pushl $99
801078db:	6a 63                	push   $0x63
  jmp alltraps
801078dd:	e9 f6 f4 ff ff       	jmp    80106dd8 <alltraps>

801078e2 <vector100>:
.globl vector100
vector100:
  pushl $0
801078e2:	6a 00                	push   $0x0
  pushl $100
801078e4:	6a 64                	push   $0x64
  jmp alltraps
801078e6:	e9 ed f4 ff ff       	jmp    80106dd8 <alltraps>

801078eb <vector101>:
.globl vector101
vector101:
  pushl $0
801078eb:	6a 00                	push   $0x0
  pushl $101
801078ed:	6a 65                	push   $0x65
  jmp alltraps
801078ef:	e9 e4 f4 ff ff       	jmp    80106dd8 <alltraps>

801078f4 <vector102>:
.globl vector102
vector102:
  pushl $0
801078f4:	6a 00                	push   $0x0
  pushl $102
801078f6:	6a 66                	push   $0x66
  jmp alltraps
801078f8:	e9 db f4 ff ff       	jmp    80106dd8 <alltraps>

801078fd <vector103>:
.globl vector103
vector103:
  pushl $0
801078fd:	6a 00                	push   $0x0
  pushl $103
801078ff:	6a 67                	push   $0x67
  jmp alltraps
80107901:	e9 d2 f4 ff ff       	jmp    80106dd8 <alltraps>

80107906 <vector104>:
.globl vector104
vector104:
  pushl $0
80107906:	6a 00                	push   $0x0
  pushl $104
80107908:	6a 68                	push   $0x68
  jmp alltraps
8010790a:	e9 c9 f4 ff ff       	jmp    80106dd8 <alltraps>

8010790f <vector105>:
.globl vector105
vector105:
  pushl $0
8010790f:	6a 00                	push   $0x0
  pushl $105
80107911:	6a 69                	push   $0x69
  jmp alltraps
80107913:	e9 c0 f4 ff ff       	jmp    80106dd8 <alltraps>

80107918 <vector106>:
.globl vector106
vector106:
  pushl $0
80107918:	6a 00                	push   $0x0
  pushl $106
8010791a:	6a 6a                	push   $0x6a
  jmp alltraps
8010791c:	e9 b7 f4 ff ff       	jmp    80106dd8 <alltraps>

80107921 <vector107>:
.globl vector107
vector107:
  pushl $0
80107921:	6a 00                	push   $0x0
  pushl $107
80107923:	6a 6b                	push   $0x6b
  jmp alltraps
80107925:	e9 ae f4 ff ff       	jmp    80106dd8 <alltraps>

8010792a <vector108>:
.globl vector108
vector108:
  pushl $0
8010792a:	6a 00                	push   $0x0
  pushl $108
8010792c:	6a 6c                	push   $0x6c
  jmp alltraps
8010792e:	e9 a5 f4 ff ff       	jmp    80106dd8 <alltraps>

80107933 <vector109>:
.globl vector109
vector109:
  pushl $0
80107933:	6a 00                	push   $0x0
  pushl $109
80107935:	6a 6d                	push   $0x6d
  jmp alltraps
80107937:	e9 9c f4 ff ff       	jmp    80106dd8 <alltraps>

8010793c <vector110>:
.globl vector110
vector110:
  pushl $0
8010793c:	6a 00                	push   $0x0
  pushl $110
8010793e:	6a 6e                	push   $0x6e
  jmp alltraps
80107940:	e9 93 f4 ff ff       	jmp    80106dd8 <alltraps>

80107945 <vector111>:
.globl vector111
vector111:
  pushl $0
80107945:	6a 00                	push   $0x0
  pushl $111
80107947:	6a 6f                	push   $0x6f
  jmp alltraps
80107949:	e9 8a f4 ff ff       	jmp    80106dd8 <alltraps>

8010794e <vector112>:
.globl vector112
vector112:
  pushl $0
8010794e:	6a 00                	push   $0x0
  pushl $112
80107950:	6a 70                	push   $0x70
  jmp alltraps
80107952:	e9 81 f4 ff ff       	jmp    80106dd8 <alltraps>

80107957 <vector113>:
.globl vector113
vector113:
  pushl $0
80107957:	6a 00                	push   $0x0
  pushl $113
80107959:	6a 71                	push   $0x71
  jmp alltraps
8010795b:	e9 78 f4 ff ff       	jmp    80106dd8 <alltraps>

80107960 <vector114>:
.globl vector114
vector114:
  pushl $0
80107960:	6a 00                	push   $0x0
  pushl $114
80107962:	6a 72                	push   $0x72
  jmp alltraps
80107964:	e9 6f f4 ff ff       	jmp    80106dd8 <alltraps>

80107969 <vector115>:
.globl vector115
vector115:
  pushl $0
80107969:	6a 00                	push   $0x0
  pushl $115
8010796b:	6a 73                	push   $0x73
  jmp alltraps
8010796d:	e9 66 f4 ff ff       	jmp    80106dd8 <alltraps>

80107972 <vector116>:
.globl vector116
vector116:
  pushl $0
80107972:	6a 00                	push   $0x0
  pushl $116
80107974:	6a 74                	push   $0x74
  jmp alltraps
80107976:	e9 5d f4 ff ff       	jmp    80106dd8 <alltraps>

8010797b <vector117>:
.globl vector117
vector117:
  pushl $0
8010797b:	6a 00                	push   $0x0
  pushl $117
8010797d:	6a 75                	push   $0x75
  jmp alltraps
8010797f:	e9 54 f4 ff ff       	jmp    80106dd8 <alltraps>

80107984 <vector118>:
.globl vector118
vector118:
  pushl $0
80107984:	6a 00                	push   $0x0
  pushl $118
80107986:	6a 76                	push   $0x76
  jmp alltraps
80107988:	e9 4b f4 ff ff       	jmp    80106dd8 <alltraps>

8010798d <vector119>:
.globl vector119
vector119:
  pushl $0
8010798d:	6a 00                	push   $0x0
  pushl $119
8010798f:	6a 77                	push   $0x77
  jmp alltraps
80107991:	e9 42 f4 ff ff       	jmp    80106dd8 <alltraps>

80107996 <vector120>:
.globl vector120
vector120:
  pushl $0
80107996:	6a 00                	push   $0x0
  pushl $120
80107998:	6a 78                	push   $0x78
  jmp alltraps
8010799a:	e9 39 f4 ff ff       	jmp    80106dd8 <alltraps>

8010799f <vector121>:
.globl vector121
vector121:
  pushl $0
8010799f:	6a 00                	push   $0x0
  pushl $121
801079a1:	6a 79                	push   $0x79
  jmp alltraps
801079a3:	e9 30 f4 ff ff       	jmp    80106dd8 <alltraps>

801079a8 <vector122>:
.globl vector122
vector122:
  pushl $0
801079a8:	6a 00                	push   $0x0
  pushl $122
801079aa:	6a 7a                	push   $0x7a
  jmp alltraps
801079ac:	e9 27 f4 ff ff       	jmp    80106dd8 <alltraps>

801079b1 <vector123>:
.globl vector123
vector123:
  pushl $0
801079b1:	6a 00                	push   $0x0
  pushl $123
801079b3:	6a 7b                	push   $0x7b
  jmp alltraps
801079b5:	e9 1e f4 ff ff       	jmp    80106dd8 <alltraps>

801079ba <vector124>:
.globl vector124
vector124:
  pushl $0
801079ba:	6a 00                	push   $0x0
  pushl $124
801079bc:	6a 7c                	push   $0x7c
  jmp alltraps
801079be:	e9 15 f4 ff ff       	jmp    80106dd8 <alltraps>

801079c3 <vector125>:
.globl vector125
vector125:
  pushl $0
801079c3:	6a 00                	push   $0x0
  pushl $125
801079c5:	6a 7d                	push   $0x7d
  jmp alltraps
801079c7:	e9 0c f4 ff ff       	jmp    80106dd8 <alltraps>

801079cc <vector126>:
.globl vector126
vector126:
  pushl $0
801079cc:	6a 00                	push   $0x0
  pushl $126
801079ce:	6a 7e                	push   $0x7e
  jmp alltraps
801079d0:	e9 03 f4 ff ff       	jmp    80106dd8 <alltraps>

801079d5 <vector127>:
.globl vector127
vector127:
  pushl $0
801079d5:	6a 00                	push   $0x0
  pushl $127
801079d7:	6a 7f                	push   $0x7f
  jmp alltraps
801079d9:	e9 fa f3 ff ff       	jmp    80106dd8 <alltraps>

801079de <vector128>:
.globl vector128
vector128:
  pushl $0
801079de:	6a 00                	push   $0x0
  pushl $128
801079e0:	68 80 00 00 00       	push   $0x80
  jmp alltraps
801079e5:	e9 ee f3 ff ff       	jmp    80106dd8 <alltraps>

801079ea <vector129>:
.globl vector129
vector129:
  pushl $0
801079ea:	6a 00                	push   $0x0
  pushl $129
801079ec:	68 81 00 00 00       	push   $0x81
  jmp alltraps
801079f1:	e9 e2 f3 ff ff       	jmp    80106dd8 <alltraps>

801079f6 <vector130>:
.globl vector130
vector130:
  pushl $0
801079f6:	6a 00                	push   $0x0
  pushl $130
801079f8:	68 82 00 00 00       	push   $0x82
  jmp alltraps
801079fd:	e9 d6 f3 ff ff       	jmp    80106dd8 <alltraps>

80107a02 <vector131>:
.globl vector131
vector131:
  pushl $0
80107a02:	6a 00                	push   $0x0
  pushl $131
80107a04:	68 83 00 00 00       	push   $0x83
  jmp alltraps
80107a09:	e9 ca f3 ff ff       	jmp    80106dd8 <alltraps>

80107a0e <vector132>:
.globl vector132
vector132:
  pushl $0
80107a0e:	6a 00                	push   $0x0
  pushl $132
80107a10:	68 84 00 00 00       	push   $0x84
  jmp alltraps
80107a15:	e9 be f3 ff ff       	jmp    80106dd8 <alltraps>

80107a1a <vector133>:
.globl vector133
vector133:
  pushl $0
80107a1a:	6a 00                	push   $0x0
  pushl $133
80107a1c:	68 85 00 00 00       	push   $0x85
  jmp alltraps
80107a21:	e9 b2 f3 ff ff       	jmp    80106dd8 <alltraps>

80107a26 <vector134>:
.globl vector134
vector134:
  pushl $0
80107a26:	6a 00                	push   $0x0
  pushl $134
80107a28:	68 86 00 00 00       	push   $0x86
  jmp alltraps
80107a2d:	e9 a6 f3 ff ff       	jmp    80106dd8 <alltraps>

80107a32 <vector135>:
.globl vector135
vector135:
  pushl $0
80107a32:	6a 00                	push   $0x0
  pushl $135
80107a34:	68 87 00 00 00       	push   $0x87
  jmp alltraps
80107a39:	e9 9a f3 ff ff       	jmp    80106dd8 <alltraps>

80107a3e <vector136>:
.globl vector136
vector136:
  pushl $0
80107a3e:	6a 00                	push   $0x0
  pushl $136
80107a40:	68 88 00 00 00       	push   $0x88
  jmp alltraps
80107a45:	e9 8e f3 ff ff       	jmp    80106dd8 <alltraps>

80107a4a <vector137>:
.globl vector137
vector137:
  pushl $0
80107a4a:	6a 00                	push   $0x0
  pushl $137
80107a4c:	68 89 00 00 00       	push   $0x89
  jmp alltraps
80107a51:	e9 82 f3 ff ff       	jmp    80106dd8 <alltraps>

80107a56 <vector138>:
.globl vector138
vector138:
  pushl $0
80107a56:	6a 00                	push   $0x0
  pushl $138
80107a58:	68 8a 00 00 00       	push   $0x8a
  jmp alltraps
80107a5d:	e9 76 f3 ff ff       	jmp    80106dd8 <alltraps>

80107a62 <vector139>:
.globl vector139
vector139:
  pushl $0
80107a62:	6a 00                	push   $0x0
  pushl $139
80107a64:	68 8b 00 00 00       	push   $0x8b
  jmp alltraps
80107a69:	e9 6a f3 ff ff       	jmp    80106dd8 <alltraps>

80107a6e <vector140>:
.globl vector140
vector140:
  pushl $0
80107a6e:	6a 00                	push   $0x0
  pushl $140
80107a70:	68 8c 00 00 00       	push   $0x8c
  jmp alltraps
80107a75:	e9 5e f3 ff ff       	jmp    80106dd8 <alltraps>

80107a7a <vector141>:
.globl vector141
vector141:
  pushl $0
80107a7a:	6a 00                	push   $0x0
  pushl $141
80107a7c:	68 8d 00 00 00       	push   $0x8d
  jmp alltraps
80107a81:	e9 52 f3 ff ff       	jmp    80106dd8 <alltraps>

80107a86 <vector142>:
.globl vector142
vector142:
  pushl $0
80107a86:	6a 00                	push   $0x0
  pushl $142
80107a88:	68 8e 00 00 00       	push   $0x8e
  jmp alltraps
80107a8d:	e9 46 f3 ff ff       	jmp    80106dd8 <alltraps>

80107a92 <vector143>:
.globl vector143
vector143:
  pushl $0
80107a92:	6a 00                	push   $0x0
  pushl $143
80107a94:	68 8f 00 00 00       	push   $0x8f
  jmp alltraps
80107a99:	e9 3a f3 ff ff       	jmp    80106dd8 <alltraps>

80107a9e <vector144>:
.globl vector144
vector144:
  pushl $0
80107a9e:	6a 00                	push   $0x0
  pushl $144
80107aa0:	68 90 00 00 00       	push   $0x90
  jmp alltraps
80107aa5:	e9 2e f3 ff ff       	jmp    80106dd8 <alltraps>

80107aaa <vector145>:
.globl vector145
vector145:
  pushl $0
80107aaa:	6a 00                	push   $0x0
  pushl $145
80107aac:	68 91 00 00 00       	push   $0x91
  jmp alltraps
80107ab1:	e9 22 f3 ff ff       	jmp    80106dd8 <alltraps>

80107ab6 <vector146>:
.globl vector146
vector146:
  pushl $0
80107ab6:	6a 00                	push   $0x0
  pushl $146
80107ab8:	68 92 00 00 00       	push   $0x92
  jmp alltraps
80107abd:	e9 16 f3 ff ff       	jmp    80106dd8 <alltraps>

80107ac2 <vector147>:
.globl vector147
vector147:
  pushl $0
80107ac2:	6a 00                	push   $0x0
  pushl $147
80107ac4:	68 93 00 00 00       	push   $0x93
  jmp alltraps
80107ac9:	e9 0a f3 ff ff       	jmp    80106dd8 <alltraps>

80107ace <vector148>:
.globl vector148
vector148:
  pushl $0
80107ace:	6a 00                	push   $0x0
  pushl $148
80107ad0:	68 94 00 00 00       	push   $0x94
  jmp alltraps
80107ad5:	e9 fe f2 ff ff       	jmp    80106dd8 <alltraps>

80107ada <vector149>:
.globl vector149
vector149:
  pushl $0
80107ada:	6a 00                	push   $0x0
  pushl $149
80107adc:	68 95 00 00 00       	push   $0x95
  jmp alltraps
80107ae1:	e9 f2 f2 ff ff       	jmp    80106dd8 <alltraps>

80107ae6 <vector150>:
.globl vector150
vector150:
  pushl $0
80107ae6:	6a 00                	push   $0x0
  pushl $150
80107ae8:	68 96 00 00 00       	push   $0x96
  jmp alltraps
80107aed:	e9 e6 f2 ff ff       	jmp    80106dd8 <alltraps>

80107af2 <vector151>:
.globl vector151
vector151:
  pushl $0
80107af2:	6a 00                	push   $0x0
  pushl $151
80107af4:	68 97 00 00 00       	push   $0x97
  jmp alltraps
80107af9:	e9 da f2 ff ff       	jmp    80106dd8 <alltraps>

80107afe <vector152>:
.globl vector152
vector152:
  pushl $0
80107afe:	6a 00                	push   $0x0
  pushl $152
80107b00:	68 98 00 00 00       	push   $0x98
  jmp alltraps
80107b05:	e9 ce f2 ff ff       	jmp    80106dd8 <alltraps>

80107b0a <vector153>:
.globl vector153
vector153:
  pushl $0
80107b0a:	6a 00                	push   $0x0
  pushl $153
80107b0c:	68 99 00 00 00       	push   $0x99
  jmp alltraps
80107b11:	e9 c2 f2 ff ff       	jmp    80106dd8 <alltraps>

80107b16 <vector154>:
.globl vector154
vector154:
  pushl $0
80107b16:	6a 00                	push   $0x0
  pushl $154
80107b18:	68 9a 00 00 00       	push   $0x9a
  jmp alltraps
80107b1d:	e9 b6 f2 ff ff       	jmp    80106dd8 <alltraps>

80107b22 <vector155>:
.globl vector155
vector155:
  pushl $0
80107b22:	6a 00                	push   $0x0
  pushl $155
80107b24:	68 9b 00 00 00       	push   $0x9b
  jmp alltraps
80107b29:	e9 aa f2 ff ff       	jmp    80106dd8 <alltraps>

80107b2e <vector156>:
.globl vector156
vector156:
  pushl $0
80107b2e:	6a 00                	push   $0x0
  pushl $156
80107b30:	68 9c 00 00 00       	push   $0x9c
  jmp alltraps
80107b35:	e9 9e f2 ff ff       	jmp    80106dd8 <alltraps>

80107b3a <vector157>:
.globl vector157
vector157:
  pushl $0
80107b3a:	6a 00                	push   $0x0
  pushl $157
80107b3c:	68 9d 00 00 00       	push   $0x9d
  jmp alltraps
80107b41:	e9 92 f2 ff ff       	jmp    80106dd8 <alltraps>

80107b46 <vector158>:
.globl vector158
vector158:
  pushl $0
80107b46:	6a 00                	push   $0x0
  pushl $158
80107b48:	68 9e 00 00 00       	push   $0x9e
  jmp alltraps
80107b4d:	e9 86 f2 ff ff       	jmp    80106dd8 <alltraps>

80107b52 <vector159>:
.globl vector159
vector159:
  pushl $0
80107b52:	6a 00                	push   $0x0
  pushl $159
80107b54:	68 9f 00 00 00       	push   $0x9f
  jmp alltraps
80107b59:	e9 7a f2 ff ff       	jmp    80106dd8 <alltraps>

80107b5e <vector160>:
.globl vector160
vector160:
  pushl $0
80107b5e:	6a 00                	push   $0x0
  pushl $160
80107b60:	68 a0 00 00 00       	push   $0xa0
  jmp alltraps
80107b65:	e9 6e f2 ff ff       	jmp    80106dd8 <alltraps>

80107b6a <vector161>:
.globl vector161
vector161:
  pushl $0
80107b6a:	6a 00                	push   $0x0
  pushl $161
80107b6c:	68 a1 00 00 00       	push   $0xa1
  jmp alltraps
80107b71:	e9 62 f2 ff ff       	jmp    80106dd8 <alltraps>

80107b76 <vector162>:
.globl vector162
vector162:
  pushl $0
80107b76:	6a 00                	push   $0x0
  pushl $162
80107b78:	68 a2 00 00 00       	push   $0xa2
  jmp alltraps
80107b7d:	e9 56 f2 ff ff       	jmp    80106dd8 <alltraps>

80107b82 <vector163>:
.globl vector163
vector163:
  pushl $0
80107b82:	6a 00                	push   $0x0
  pushl $163
80107b84:	68 a3 00 00 00       	push   $0xa3
  jmp alltraps
80107b89:	e9 4a f2 ff ff       	jmp    80106dd8 <alltraps>

80107b8e <vector164>:
.globl vector164
vector164:
  pushl $0
80107b8e:	6a 00                	push   $0x0
  pushl $164
80107b90:	68 a4 00 00 00       	push   $0xa4
  jmp alltraps
80107b95:	e9 3e f2 ff ff       	jmp    80106dd8 <alltraps>

80107b9a <vector165>:
.globl vector165
vector165:
  pushl $0
80107b9a:	6a 00                	push   $0x0
  pushl $165
80107b9c:	68 a5 00 00 00       	push   $0xa5
  jmp alltraps
80107ba1:	e9 32 f2 ff ff       	jmp    80106dd8 <alltraps>

80107ba6 <vector166>:
.globl vector166
vector166:
  pushl $0
80107ba6:	6a 00                	push   $0x0
  pushl $166
80107ba8:	68 a6 00 00 00       	push   $0xa6
  jmp alltraps
80107bad:	e9 26 f2 ff ff       	jmp    80106dd8 <alltraps>

80107bb2 <vector167>:
.globl vector167
vector167:
  pushl $0
80107bb2:	6a 00                	push   $0x0
  pushl $167
80107bb4:	68 a7 00 00 00       	push   $0xa7
  jmp alltraps
80107bb9:	e9 1a f2 ff ff       	jmp    80106dd8 <alltraps>

80107bbe <vector168>:
.globl vector168
vector168:
  pushl $0
80107bbe:	6a 00                	push   $0x0
  pushl $168
80107bc0:	68 a8 00 00 00       	push   $0xa8
  jmp alltraps
80107bc5:	e9 0e f2 ff ff       	jmp    80106dd8 <alltraps>

80107bca <vector169>:
.globl vector169
vector169:
  pushl $0
80107bca:	6a 00                	push   $0x0
  pushl $169
80107bcc:	68 a9 00 00 00       	push   $0xa9
  jmp alltraps
80107bd1:	e9 02 f2 ff ff       	jmp    80106dd8 <alltraps>

80107bd6 <vector170>:
.globl vector170
vector170:
  pushl $0
80107bd6:	6a 00                	push   $0x0
  pushl $170
80107bd8:	68 aa 00 00 00       	push   $0xaa
  jmp alltraps
80107bdd:	e9 f6 f1 ff ff       	jmp    80106dd8 <alltraps>

80107be2 <vector171>:
.globl vector171
vector171:
  pushl $0
80107be2:	6a 00                	push   $0x0
  pushl $171
80107be4:	68 ab 00 00 00       	push   $0xab
  jmp alltraps
80107be9:	e9 ea f1 ff ff       	jmp    80106dd8 <alltraps>

80107bee <vector172>:
.globl vector172
vector172:
  pushl $0
80107bee:	6a 00                	push   $0x0
  pushl $172
80107bf0:	68 ac 00 00 00       	push   $0xac
  jmp alltraps
80107bf5:	e9 de f1 ff ff       	jmp    80106dd8 <alltraps>

80107bfa <vector173>:
.globl vector173
vector173:
  pushl $0
80107bfa:	6a 00                	push   $0x0
  pushl $173
80107bfc:	68 ad 00 00 00       	push   $0xad
  jmp alltraps
80107c01:	e9 d2 f1 ff ff       	jmp    80106dd8 <alltraps>

80107c06 <vector174>:
.globl vector174
vector174:
  pushl $0
80107c06:	6a 00                	push   $0x0
  pushl $174
80107c08:	68 ae 00 00 00       	push   $0xae
  jmp alltraps
80107c0d:	e9 c6 f1 ff ff       	jmp    80106dd8 <alltraps>

80107c12 <vector175>:
.globl vector175
vector175:
  pushl $0
80107c12:	6a 00                	push   $0x0
  pushl $175
80107c14:	68 af 00 00 00       	push   $0xaf
  jmp alltraps
80107c19:	e9 ba f1 ff ff       	jmp    80106dd8 <alltraps>

80107c1e <vector176>:
.globl vector176
vector176:
  pushl $0
80107c1e:	6a 00                	push   $0x0
  pushl $176
80107c20:	68 b0 00 00 00       	push   $0xb0
  jmp alltraps
80107c25:	e9 ae f1 ff ff       	jmp    80106dd8 <alltraps>

80107c2a <vector177>:
.globl vector177
vector177:
  pushl $0
80107c2a:	6a 00                	push   $0x0
  pushl $177
80107c2c:	68 b1 00 00 00       	push   $0xb1
  jmp alltraps
80107c31:	e9 a2 f1 ff ff       	jmp    80106dd8 <alltraps>

80107c36 <vector178>:
.globl vector178
vector178:
  pushl $0
80107c36:	6a 00                	push   $0x0
  pushl $178
80107c38:	68 b2 00 00 00       	push   $0xb2
  jmp alltraps
80107c3d:	e9 96 f1 ff ff       	jmp    80106dd8 <alltraps>

80107c42 <vector179>:
.globl vector179
vector179:
  pushl $0
80107c42:	6a 00                	push   $0x0
  pushl $179
80107c44:	68 b3 00 00 00       	push   $0xb3
  jmp alltraps
80107c49:	e9 8a f1 ff ff       	jmp    80106dd8 <alltraps>

80107c4e <vector180>:
.globl vector180
vector180:
  pushl $0
80107c4e:	6a 00                	push   $0x0
  pushl $180
80107c50:	68 b4 00 00 00       	push   $0xb4
  jmp alltraps
80107c55:	e9 7e f1 ff ff       	jmp    80106dd8 <alltraps>

80107c5a <vector181>:
.globl vector181
vector181:
  pushl $0
80107c5a:	6a 00                	push   $0x0
  pushl $181
80107c5c:	68 b5 00 00 00       	push   $0xb5
  jmp alltraps
80107c61:	e9 72 f1 ff ff       	jmp    80106dd8 <alltraps>

80107c66 <vector182>:
.globl vector182
vector182:
  pushl $0
80107c66:	6a 00                	push   $0x0
  pushl $182
80107c68:	68 b6 00 00 00       	push   $0xb6
  jmp alltraps
80107c6d:	e9 66 f1 ff ff       	jmp    80106dd8 <alltraps>

80107c72 <vector183>:
.globl vector183
vector183:
  pushl $0
80107c72:	6a 00                	push   $0x0
  pushl $183
80107c74:	68 b7 00 00 00       	push   $0xb7
  jmp alltraps
80107c79:	e9 5a f1 ff ff       	jmp    80106dd8 <alltraps>

80107c7e <vector184>:
.globl vector184
vector184:
  pushl $0
80107c7e:	6a 00                	push   $0x0
  pushl $184
80107c80:	68 b8 00 00 00       	push   $0xb8
  jmp alltraps
80107c85:	e9 4e f1 ff ff       	jmp    80106dd8 <alltraps>

80107c8a <vector185>:
.globl vector185
vector185:
  pushl $0
80107c8a:	6a 00                	push   $0x0
  pushl $185
80107c8c:	68 b9 00 00 00       	push   $0xb9
  jmp alltraps
80107c91:	e9 42 f1 ff ff       	jmp    80106dd8 <alltraps>

80107c96 <vector186>:
.globl vector186
vector186:
  pushl $0
80107c96:	6a 00                	push   $0x0
  pushl $186
80107c98:	68 ba 00 00 00       	push   $0xba
  jmp alltraps
80107c9d:	e9 36 f1 ff ff       	jmp    80106dd8 <alltraps>

80107ca2 <vector187>:
.globl vector187
vector187:
  pushl $0
80107ca2:	6a 00                	push   $0x0
  pushl $187
80107ca4:	68 bb 00 00 00       	push   $0xbb
  jmp alltraps
80107ca9:	e9 2a f1 ff ff       	jmp    80106dd8 <alltraps>

80107cae <vector188>:
.globl vector188
vector188:
  pushl $0
80107cae:	6a 00                	push   $0x0
  pushl $188
80107cb0:	68 bc 00 00 00       	push   $0xbc
  jmp alltraps
80107cb5:	e9 1e f1 ff ff       	jmp    80106dd8 <alltraps>

80107cba <vector189>:
.globl vector189
vector189:
  pushl $0
80107cba:	6a 00                	push   $0x0
  pushl $189
80107cbc:	68 bd 00 00 00       	push   $0xbd
  jmp alltraps
80107cc1:	e9 12 f1 ff ff       	jmp    80106dd8 <alltraps>

80107cc6 <vector190>:
.globl vector190
vector190:
  pushl $0
80107cc6:	6a 00                	push   $0x0
  pushl $190
80107cc8:	68 be 00 00 00       	push   $0xbe
  jmp alltraps
80107ccd:	e9 06 f1 ff ff       	jmp    80106dd8 <alltraps>

80107cd2 <vector191>:
.globl vector191
vector191:
  pushl $0
80107cd2:	6a 00                	push   $0x0
  pushl $191
80107cd4:	68 bf 00 00 00       	push   $0xbf
  jmp alltraps
80107cd9:	e9 fa f0 ff ff       	jmp    80106dd8 <alltraps>

80107cde <vector192>:
.globl vector192
vector192:
  pushl $0
80107cde:	6a 00                	push   $0x0
  pushl $192
80107ce0:	68 c0 00 00 00       	push   $0xc0
  jmp alltraps
80107ce5:	e9 ee f0 ff ff       	jmp    80106dd8 <alltraps>

80107cea <vector193>:
.globl vector193
vector193:
  pushl $0
80107cea:	6a 00                	push   $0x0
  pushl $193
80107cec:	68 c1 00 00 00       	push   $0xc1
  jmp alltraps
80107cf1:	e9 e2 f0 ff ff       	jmp    80106dd8 <alltraps>

80107cf6 <vector194>:
.globl vector194
vector194:
  pushl $0
80107cf6:	6a 00                	push   $0x0
  pushl $194
80107cf8:	68 c2 00 00 00       	push   $0xc2
  jmp alltraps
80107cfd:	e9 d6 f0 ff ff       	jmp    80106dd8 <alltraps>

80107d02 <vector195>:
.globl vector195
vector195:
  pushl $0
80107d02:	6a 00                	push   $0x0
  pushl $195
80107d04:	68 c3 00 00 00       	push   $0xc3
  jmp alltraps
80107d09:	e9 ca f0 ff ff       	jmp    80106dd8 <alltraps>

80107d0e <vector196>:
.globl vector196
vector196:
  pushl $0
80107d0e:	6a 00                	push   $0x0
  pushl $196
80107d10:	68 c4 00 00 00       	push   $0xc4
  jmp alltraps
80107d15:	e9 be f0 ff ff       	jmp    80106dd8 <alltraps>

80107d1a <vector197>:
.globl vector197
vector197:
  pushl $0
80107d1a:	6a 00                	push   $0x0
  pushl $197
80107d1c:	68 c5 00 00 00       	push   $0xc5
  jmp alltraps
80107d21:	e9 b2 f0 ff ff       	jmp    80106dd8 <alltraps>

80107d26 <vector198>:
.globl vector198
vector198:
  pushl $0
80107d26:	6a 00                	push   $0x0
  pushl $198
80107d28:	68 c6 00 00 00       	push   $0xc6
  jmp alltraps
80107d2d:	e9 a6 f0 ff ff       	jmp    80106dd8 <alltraps>

80107d32 <vector199>:
.globl vector199
vector199:
  pushl $0
80107d32:	6a 00                	push   $0x0
  pushl $199
80107d34:	68 c7 00 00 00       	push   $0xc7
  jmp alltraps
80107d39:	e9 9a f0 ff ff       	jmp    80106dd8 <alltraps>

80107d3e <vector200>:
.globl vector200
vector200:
  pushl $0
80107d3e:	6a 00                	push   $0x0
  pushl $200
80107d40:	68 c8 00 00 00       	push   $0xc8
  jmp alltraps
80107d45:	e9 8e f0 ff ff       	jmp    80106dd8 <alltraps>

80107d4a <vector201>:
.globl vector201
vector201:
  pushl $0
80107d4a:	6a 00                	push   $0x0
  pushl $201
80107d4c:	68 c9 00 00 00       	push   $0xc9
  jmp alltraps
80107d51:	e9 82 f0 ff ff       	jmp    80106dd8 <alltraps>

80107d56 <vector202>:
.globl vector202
vector202:
  pushl $0
80107d56:	6a 00                	push   $0x0
  pushl $202
80107d58:	68 ca 00 00 00       	push   $0xca
  jmp alltraps
80107d5d:	e9 76 f0 ff ff       	jmp    80106dd8 <alltraps>

80107d62 <vector203>:
.globl vector203
vector203:
  pushl $0
80107d62:	6a 00                	push   $0x0
  pushl $203
80107d64:	68 cb 00 00 00       	push   $0xcb
  jmp alltraps
80107d69:	e9 6a f0 ff ff       	jmp    80106dd8 <alltraps>

80107d6e <vector204>:
.globl vector204
vector204:
  pushl $0
80107d6e:	6a 00                	push   $0x0
  pushl $204
80107d70:	68 cc 00 00 00       	push   $0xcc
  jmp alltraps
80107d75:	e9 5e f0 ff ff       	jmp    80106dd8 <alltraps>

80107d7a <vector205>:
.globl vector205
vector205:
  pushl $0
80107d7a:	6a 00                	push   $0x0
  pushl $205
80107d7c:	68 cd 00 00 00       	push   $0xcd
  jmp alltraps
80107d81:	e9 52 f0 ff ff       	jmp    80106dd8 <alltraps>

80107d86 <vector206>:
.globl vector206
vector206:
  pushl $0
80107d86:	6a 00                	push   $0x0
  pushl $206
80107d88:	68 ce 00 00 00       	push   $0xce
  jmp alltraps
80107d8d:	e9 46 f0 ff ff       	jmp    80106dd8 <alltraps>

80107d92 <vector207>:
.globl vector207
vector207:
  pushl $0
80107d92:	6a 00                	push   $0x0
  pushl $207
80107d94:	68 cf 00 00 00       	push   $0xcf
  jmp alltraps
80107d99:	e9 3a f0 ff ff       	jmp    80106dd8 <alltraps>

80107d9e <vector208>:
.globl vector208
vector208:
  pushl $0
80107d9e:	6a 00                	push   $0x0
  pushl $208
80107da0:	68 d0 00 00 00       	push   $0xd0
  jmp alltraps
80107da5:	e9 2e f0 ff ff       	jmp    80106dd8 <alltraps>

80107daa <vector209>:
.globl vector209
vector209:
  pushl $0
80107daa:	6a 00                	push   $0x0
  pushl $209
80107dac:	68 d1 00 00 00       	push   $0xd1
  jmp alltraps
80107db1:	e9 22 f0 ff ff       	jmp    80106dd8 <alltraps>

80107db6 <vector210>:
.globl vector210
vector210:
  pushl $0
80107db6:	6a 00                	push   $0x0
  pushl $210
80107db8:	68 d2 00 00 00       	push   $0xd2
  jmp alltraps
80107dbd:	e9 16 f0 ff ff       	jmp    80106dd8 <alltraps>

80107dc2 <vector211>:
.globl vector211
vector211:
  pushl $0
80107dc2:	6a 00                	push   $0x0
  pushl $211
80107dc4:	68 d3 00 00 00       	push   $0xd3
  jmp alltraps
80107dc9:	e9 0a f0 ff ff       	jmp    80106dd8 <alltraps>

80107dce <vector212>:
.globl vector212
vector212:
  pushl $0
80107dce:	6a 00                	push   $0x0
  pushl $212
80107dd0:	68 d4 00 00 00       	push   $0xd4
  jmp alltraps
80107dd5:	e9 fe ef ff ff       	jmp    80106dd8 <alltraps>

80107dda <vector213>:
.globl vector213
vector213:
  pushl $0
80107dda:	6a 00                	push   $0x0
  pushl $213
80107ddc:	68 d5 00 00 00       	push   $0xd5
  jmp alltraps
80107de1:	e9 f2 ef ff ff       	jmp    80106dd8 <alltraps>

80107de6 <vector214>:
.globl vector214
vector214:
  pushl $0
80107de6:	6a 00                	push   $0x0
  pushl $214
80107de8:	68 d6 00 00 00       	push   $0xd6
  jmp alltraps
80107ded:	e9 e6 ef ff ff       	jmp    80106dd8 <alltraps>

80107df2 <vector215>:
.globl vector215
vector215:
  pushl $0
80107df2:	6a 00                	push   $0x0
  pushl $215
80107df4:	68 d7 00 00 00       	push   $0xd7
  jmp alltraps
80107df9:	e9 da ef ff ff       	jmp    80106dd8 <alltraps>

80107dfe <vector216>:
.globl vector216
vector216:
  pushl $0
80107dfe:	6a 00                	push   $0x0
  pushl $216
80107e00:	68 d8 00 00 00       	push   $0xd8
  jmp alltraps
80107e05:	e9 ce ef ff ff       	jmp    80106dd8 <alltraps>

80107e0a <vector217>:
.globl vector217
vector217:
  pushl $0
80107e0a:	6a 00                	push   $0x0
  pushl $217
80107e0c:	68 d9 00 00 00       	push   $0xd9
  jmp alltraps
80107e11:	e9 c2 ef ff ff       	jmp    80106dd8 <alltraps>

80107e16 <vector218>:
.globl vector218
vector218:
  pushl $0
80107e16:	6a 00                	push   $0x0
  pushl $218
80107e18:	68 da 00 00 00       	push   $0xda
  jmp alltraps
80107e1d:	e9 b6 ef ff ff       	jmp    80106dd8 <alltraps>

80107e22 <vector219>:
.globl vector219
vector219:
  pushl $0
80107e22:	6a 00                	push   $0x0
  pushl $219
80107e24:	68 db 00 00 00       	push   $0xdb
  jmp alltraps
80107e29:	e9 aa ef ff ff       	jmp    80106dd8 <alltraps>

80107e2e <vector220>:
.globl vector220
vector220:
  pushl $0
80107e2e:	6a 00                	push   $0x0
  pushl $220
80107e30:	68 dc 00 00 00       	push   $0xdc
  jmp alltraps
80107e35:	e9 9e ef ff ff       	jmp    80106dd8 <alltraps>

80107e3a <vector221>:
.globl vector221
vector221:
  pushl $0
80107e3a:	6a 00                	push   $0x0
  pushl $221
80107e3c:	68 dd 00 00 00       	push   $0xdd
  jmp alltraps
80107e41:	e9 92 ef ff ff       	jmp    80106dd8 <alltraps>

80107e46 <vector222>:
.globl vector222
vector222:
  pushl $0
80107e46:	6a 00                	push   $0x0
  pushl $222
80107e48:	68 de 00 00 00       	push   $0xde
  jmp alltraps
80107e4d:	e9 86 ef ff ff       	jmp    80106dd8 <alltraps>

80107e52 <vector223>:
.globl vector223
vector223:
  pushl $0
80107e52:	6a 00                	push   $0x0
  pushl $223
80107e54:	68 df 00 00 00       	push   $0xdf
  jmp alltraps
80107e59:	e9 7a ef ff ff       	jmp    80106dd8 <alltraps>

80107e5e <vector224>:
.globl vector224
vector224:
  pushl $0
80107e5e:	6a 00                	push   $0x0
  pushl $224
80107e60:	68 e0 00 00 00       	push   $0xe0
  jmp alltraps
80107e65:	e9 6e ef ff ff       	jmp    80106dd8 <alltraps>

80107e6a <vector225>:
.globl vector225
vector225:
  pushl $0
80107e6a:	6a 00                	push   $0x0
  pushl $225
80107e6c:	68 e1 00 00 00       	push   $0xe1
  jmp alltraps
80107e71:	e9 62 ef ff ff       	jmp    80106dd8 <alltraps>

80107e76 <vector226>:
.globl vector226
vector226:
  pushl $0
80107e76:	6a 00                	push   $0x0
  pushl $226
80107e78:	68 e2 00 00 00       	push   $0xe2
  jmp alltraps
80107e7d:	e9 56 ef ff ff       	jmp    80106dd8 <alltraps>

80107e82 <vector227>:
.globl vector227
vector227:
  pushl $0
80107e82:	6a 00                	push   $0x0
  pushl $227
80107e84:	68 e3 00 00 00       	push   $0xe3
  jmp alltraps
80107e89:	e9 4a ef ff ff       	jmp    80106dd8 <alltraps>

80107e8e <vector228>:
.globl vector228
vector228:
  pushl $0
80107e8e:	6a 00                	push   $0x0
  pushl $228
80107e90:	68 e4 00 00 00       	push   $0xe4
  jmp alltraps
80107e95:	e9 3e ef ff ff       	jmp    80106dd8 <alltraps>

80107e9a <vector229>:
.globl vector229
vector229:
  pushl $0
80107e9a:	6a 00                	push   $0x0
  pushl $229
80107e9c:	68 e5 00 00 00       	push   $0xe5
  jmp alltraps
80107ea1:	e9 32 ef ff ff       	jmp    80106dd8 <alltraps>

80107ea6 <vector230>:
.globl vector230
vector230:
  pushl $0
80107ea6:	6a 00                	push   $0x0
  pushl $230
80107ea8:	68 e6 00 00 00       	push   $0xe6
  jmp alltraps
80107ead:	e9 26 ef ff ff       	jmp    80106dd8 <alltraps>

80107eb2 <vector231>:
.globl vector231
vector231:
  pushl $0
80107eb2:	6a 00                	push   $0x0
  pushl $231
80107eb4:	68 e7 00 00 00       	push   $0xe7
  jmp alltraps
80107eb9:	e9 1a ef ff ff       	jmp    80106dd8 <alltraps>

80107ebe <vector232>:
.globl vector232
vector232:
  pushl $0
80107ebe:	6a 00                	push   $0x0
  pushl $232
80107ec0:	68 e8 00 00 00       	push   $0xe8
  jmp alltraps
80107ec5:	e9 0e ef ff ff       	jmp    80106dd8 <alltraps>

80107eca <vector233>:
.globl vector233
vector233:
  pushl $0
80107eca:	6a 00                	push   $0x0
  pushl $233
80107ecc:	68 e9 00 00 00       	push   $0xe9
  jmp alltraps
80107ed1:	e9 02 ef ff ff       	jmp    80106dd8 <alltraps>

80107ed6 <vector234>:
.globl vector234
vector234:
  pushl $0
80107ed6:	6a 00                	push   $0x0
  pushl $234
80107ed8:	68 ea 00 00 00       	push   $0xea
  jmp alltraps
80107edd:	e9 f6 ee ff ff       	jmp    80106dd8 <alltraps>

80107ee2 <vector235>:
.globl vector235
vector235:
  pushl $0
80107ee2:	6a 00                	push   $0x0
  pushl $235
80107ee4:	68 eb 00 00 00       	push   $0xeb
  jmp alltraps
80107ee9:	e9 ea ee ff ff       	jmp    80106dd8 <alltraps>

80107eee <vector236>:
.globl vector236
vector236:
  pushl $0
80107eee:	6a 00                	push   $0x0
  pushl $236
80107ef0:	68 ec 00 00 00       	push   $0xec
  jmp alltraps
80107ef5:	e9 de ee ff ff       	jmp    80106dd8 <alltraps>

80107efa <vector237>:
.globl vector237
vector237:
  pushl $0
80107efa:	6a 00                	push   $0x0
  pushl $237
80107efc:	68 ed 00 00 00       	push   $0xed
  jmp alltraps
80107f01:	e9 d2 ee ff ff       	jmp    80106dd8 <alltraps>

80107f06 <vector238>:
.globl vector238
vector238:
  pushl $0
80107f06:	6a 00                	push   $0x0
  pushl $238
80107f08:	68 ee 00 00 00       	push   $0xee
  jmp alltraps
80107f0d:	e9 c6 ee ff ff       	jmp    80106dd8 <alltraps>

80107f12 <vector239>:
.globl vector239
vector239:
  pushl $0
80107f12:	6a 00                	push   $0x0
  pushl $239
80107f14:	68 ef 00 00 00       	push   $0xef
  jmp alltraps
80107f19:	e9 ba ee ff ff       	jmp    80106dd8 <alltraps>

80107f1e <vector240>:
.globl vector240
vector240:
  pushl $0
80107f1e:	6a 00                	push   $0x0
  pushl $240
80107f20:	68 f0 00 00 00       	push   $0xf0
  jmp alltraps
80107f25:	e9 ae ee ff ff       	jmp    80106dd8 <alltraps>

80107f2a <vector241>:
.globl vector241
vector241:
  pushl $0
80107f2a:	6a 00                	push   $0x0
  pushl $241
80107f2c:	68 f1 00 00 00       	push   $0xf1
  jmp alltraps
80107f31:	e9 a2 ee ff ff       	jmp    80106dd8 <alltraps>

80107f36 <vector242>:
.globl vector242
vector242:
  pushl $0
80107f36:	6a 00                	push   $0x0
  pushl $242
80107f38:	68 f2 00 00 00       	push   $0xf2
  jmp alltraps
80107f3d:	e9 96 ee ff ff       	jmp    80106dd8 <alltraps>

80107f42 <vector243>:
.globl vector243
vector243:
  pushl $0
80107f42:	6a 00                	push   $0x0
  pushl $243
80107f44:	68 f3 00 00 00       	push   $0xf3
  jmp alltraps
80107f49:	e9 8a ee ff ff       	jmp    80106dd8 <alltraps>

80107f4e <vector244>:
.globl vector244
vector244:
  pushl $0
80107f4e:	6a 00                	push   $0x0
  pushl $244
80107f50:	68 f4 00 00 00       	push   $0xf4
  jmp alltraps
80107f55:	e9 7e ee ff ff       	jmp    80106dd8 <alltraps>

80107f5a <vector245>:
.globl vector245
vector245:
  pushl $0
80107f5a:	6a 00                	push   $0x0
  pushl $245
80107f5c:	68 f5 00 00 00       	push   $0xf5
  jmp alltraps
80107f61:	e9 72 ee ff ff       	jmp    80106dd8 <alltraps>

80107f66 <vector246>:
.globl vector246
vector246:
  pushl $0
80107f66:	6a 00                	push   $0x0
  pushl $246
80107f68:	68 f6 00 00 00       	push   $0xf6
  jmp alltraps
80107f6d:	e9 66 ee ff ff       	jmp    80106dd8 <alltraps>

80107f72 <vector247>:
.globl vector247
vector247:
  pushl $0
80107f72:	6a 00                	push   $0x0
  pushl $247
80107f74:	68 f7 00 00 00       	push   $0xf7
  jmp alltraps
80107f79:	e9 5a ee ff ff       	jmp    80106dd8 <alltraps>

80107f7e <vector248>:
.globl vector248
vector248:
  pushl $0
80107f7e:	6a 00                	push   $0x0
  pushl $248
80107f80:	68 f8 00 00 00       	push   $0xf8
  jmp alltraps
80107f85:	e9 4e ee ff ff       	jmp    80106dd8 <alltraps>

80107f8a <vector249>:
.globl vector249
vector249:
  pushl $0
80107f8a:	6a 00                	push   $0x0
  pushl $249
80107f8c:	68 f9 00 00 00       	push   $0xf9
  jmp alltraps
80107f91:	e9 42 ee ff ff       	jmp    80106dd8 <alltraps>

80107f96 <vector250>:
.globl vector250
vector250:
  pushl $0
80107f96:	6a 00                	push   $0x0
  pushl $250
80107f98:	68 fa 00 00 00       	push   $0xfa
  jmp alltraps
80107f9d:	e9 36 ee ff ff       	jmp    80106dd8 <alltraps>

80107fa2 <vector251>:
.globl vector251
vector251:
  pushl $0
80107fa2:	6a 00                	push   $0x0
  pushl $251
80107fa4:	68 fb 00 00 00       	push   $0xfb
  jmp alltraps
80107fa9:	e9 2a ee ff ff       	jmp    80106dd8 <alltraps>

80107fae <vector252>:
.globl vector252
vector252:
  pushl $0
80107fae:	6a 00                	push   $0x0
  pushl $252
80107fb0:	68 fc 00 00 00       	push   $0xfc
  jmp alltraps
80107fb5:	e9 1e ee ff ff       	jmp    80106dd8 <alltraps>

80107fba <vector253>:
.globl vector253
vector253:
  pushl $0
80107fba:	6a 00                	push   $0x0
  pushl $253
80107fbc:	68 fd 00 00 00       	push   $0xfd
  jmp alltraps
80107fc1:	e9 12 ee ff ff       	jmp    80106dd8 <alltraps>

80107fc6 <vector254>:
.globl vector254
vector254:
  pushl $0
80107fc6:	6a 00                	push   $0x0
  pushl $254
80107fc8:	68 fe 00 00 00       	push   $0xfe
  jmp alltraps
80107fcd:	e9 06 ee ff ff       	jmp    80106dd8 <alltraps>

80107fd2 <vector255>:
.globl vector255
vector255:
  pushl $0
80107fd2:	6a 00                	push   $0x0
  pushl $255
80107fd4:	68 ff 00 00 00       	push   $0xff
  jmp alltraps
80107fd9:	e9 fa ed ff ff       	jmp    80106dd8 <alltraps>
	...

80107fe0 <lgdt>:

struct segdesc;

static inline void
lgdt(struct segdesc *p, int size)
{
80107fe0:	55                   	push   %ebp
80107fe1:	89 e5                	mov    %esp,%ebp
80107fe3:	83 ec 10             	sub    $0x10,%esp
  volatile ushort pd[3];

  pd[0] = size-1;
80107fe6:	8b 45 0c             	mov    0xc(%ebp),%eax
80107fe9:	83 e8 01             	sub    $0x1,%eax
80107fec:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
  pd[1] = (uint)p;
80107ff0:	8b 45 08             	mov    0x8(%ebp),%eax
80107ff3:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  pd[2] = (uint)p >> 16;
80107ff7:	8b 45 08             	mov    0x8(%ebp),%eax
80107ffa:	c1 e8 10             	shr    $0x10,%eax
80107ffd:	66 89 45 fe          	mov    %ax,-0x2(%ebp)

  asm volatile("lgdt (%0)" : : "r" (pd));
80108001:	8d 45 fa             	lea    -0x6(%ebp),%eax
80108004:	0f 01 10             	lgdtl  (%eax)
}
80108007:	c9                   	leave  
80108008:	c3                   	ret    

80108009 <ltr>:
  asm volatile("lidt (%0)" : : "r" (pd));
}

static inline void
ltr(ushort sel)
{
80108009:	55                   	push   %ebp
8010800a:	89 e5                	mov    %esp,%ebp
8010800c:	83 ec 04             	sub    $0x4,%esp
8010800f:	8b 45 08             	mov    0x8(%ebp),%eax
80108012:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  asm volatile("ltr %0" : : "r" (sel));
80108016:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
8010801a:	0f 00 d8             	ltr    %ax
}
8010801d:	c9                   	leave  
8010801e:	c3                   	ret    

8010801f <loadgs>:
  return eflags;
}

static inline void
loadgs(ushort v)
{
8010801f:	55                   	push   %ebp
80108020:	89 e5                	mov    %esp,%ebp
80108022:	83 ec 04             	sub    $0x4,%esp
80108025:	8b 45 08             	mov    0x8(%ebp),%eax
80108028:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  asm volatile("movw %0, %%gs" : : "r" (v));
8010802c:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
80108030:	8e e8                	mov    %eax,%gs
}
80108032:	c9                   	leave  
80108033:	c3                   	ret    

80108034 <lcr3>:
  return val;
}

static inline void
lcr3(uint val)
{
80108034:	55                   	push   %ebp
80108035:	89 e5                	mov    %esp,%ebp
  asm volatile("movl %0,%%cr3" : : "r" (val));
80108037:	8b 45 08             	mov    0x8(%ebp),%eax
8010803a:	0f 22 d8             	mov    %eax,%cr3
}
8010803d:	5d                   	pop    %ebp
8010803e:	c3                   	ret    

8010803f <seginit>:

// Set up CPU's kernel segment descriptors.
// Run once on entry on each CPU.
void
seginit(void)
{
8010803f:	55                   	push   %ebp
80108040:	89 e5                	mov    %esp,%ebp
80108042:	53                   	push   %ebx
80108043:	83 ec 24             	sub    $0x24,%esp

  // Map "logical" addresses to virtual addresses using identity map.
  // Cannot share a CODE descriptor for both kernel and user
  // because it would have to have DPL_USR, but the CPU forbids
  // an interrupt from CPL=0 to DPL=3.
  c = &cpus[cpunum()];
80108046:	e8 af b6 ff ff       	call   801036fa <cpunum>
8010804b:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
80108051:	05 e0 3b 11 80       	add    $0x80113be0,%eax
80108056:	89 45 f4             	mov    %eax,-0xc(%ebp)
  c->gdt[SEG_KCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, 0);
80108059:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010805c:	66 c7 40 78 ff ff    	movw   $0xffff,0x78(%eax)
80108062:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108065:	66 c7 40 7a 00 00    	movw   $0x0,0x7a(%eax)
8010806b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010806e:	c6 40 7c 00          	movb   $0x0,0x7c(%eax)
80108072:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108075:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
80108079:	83 e2 f0             	and    $0xfffffff0,%edx
8010807c:	83 ca 0a             	or     $0xa,%edx
8010807f:	88 50 7d             	mov    %dl,0x7d(%eax)
80108082:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108085:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
80108089:	83 ca 10             	or     $0x10,%edx
8010808c:	88 50 7d             	mov    %dl,0x7d(%eax)
8010808f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108092:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
80108096:	83 e2 9f             	and    $0xffffff9f,%edx
80108099:	88 50 7d             	mov    %dl,0x7d(%eax)
8010809c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010809f:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
801080a3:	83 ca 80             	or     $0xffffff80,%edx
801080a6:	88 50 7d             	mov    %dl,0x7d(%eax)
801080a9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801080ac:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
801080b0:	83 ca 0f             	or     $0xf,%edx
801080b3:	88 50 7e             	mov    %dl,0x7e(%eax)
801080b6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801080b9:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
801080bd:	83 e2 ef             	and    $0xffffffef,%edx
801080c0:	88 50 7e             	mov    %dl,0x7e(%eax)
801080c3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801080c6:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
801080ca:	83 e2 df             	and    $0xffffffdf,%edx
801080cd:	88 50 7e             	mov    %dl,0x7e(%eax)
801080d0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801080d3:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
801080d7:	83 ca 40             	or     $0x40,%edx
801080da:	88 50 7e             	mov    %dl,0x7e(%eax)
801080dd:	8b 45 f4             	mov    -0xc(%ebp),%eax
801080e0:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
801080e4:	83 ca 80             	or     $0xffffff80,%edx
801080e7:	88 50 7e             	mov    %dl,0x7e(%eax)
801080ea:	8b 45 f4             	mov    -0xc(%ebp),%eax
801080ed:	c6 40 7f 00          	movb   $0x0,0x7f(%eax)
  c->gdt[SEG_KDATA] = SEG(STA_W, 0, 0xffffffff, 0);
801080f1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801080f4:	66 c7 80 80 00 00 00 	movw   $0xffff,0x80(%eax)
801080fb:	ff ff 
801080fd:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108100:	66 c7 80 82 00 00 00 	movw   $0x0,0x82(%eax)
80108107:	00 00 
80108109:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010810c:	c6 80 84 00 00 00 00 	movb   $0x0,0x84(%eax)
80108113:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108116:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
8010811d:	83 e2 f0             	and    $0xfffffff0,%edx
80108120:	83 ca 02             	or     $0x2,%edx
80108123:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
80108129:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010812c:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
80108133:	83 ca 10             	or     $0x10,%edx
80108136:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
8010813c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010813f:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
80108146:	83 e2 9f             	and    $0xffffff9f,%edx
80108149:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
8010814f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108152:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
80108159:	83 ca 80             	or     $0xffffff80,%edx
8010815c:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
80108162:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108165:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
8010816c:	83 ca 0f             	or     $0xf,%edx
8010816f:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
80108175:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108178:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
8010817f:	83 e2 ef             	and    $0xffffffef,%edx
80108182:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
80108188:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010818b:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
80108192:	83 e2 df             	and    $0xffffffdf,%edx
80108195:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
8010819b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010819e:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
801081a5:	83 ca 40             	or     $0x40,%edx
801081a8:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
801081ae:	8b 45 f4             	mov    -0xc(%ebp),%eax
801081b1:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
801081b8:	83 ca 80             	or     $0xffffff80,%edx
801081bb:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
801081c1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801081c4:	c6 80 87 00 00 00 00 	movb   $0x0,0x87(%eax)
  c->gdt[SEG_UCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, DPL_USER);
801081cb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801081ce:	66 c7 80 90 00 00 00 	movw   $0xffff,0x90(%eax)
801081d5:	ff ff 
801081d7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801081da:	66 c7 80 92 00 00 00 	movw   $0x0,0x92(%eax)
801081e1:	00 00 
801081e3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801081e6:	c6 80 94 00 00 00 00 	movb   $0x0,0x94(%eax)
801081ed:	8b 45 f4             	mov    -0xc(%ebp),%eax
801081f0:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
801081f7:	83 e2 f0             	and    $0xfffffff0,%edx
801081fa:	83 ca 0a             	or     $0xa,%edx
801081fd:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
80108203:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108206:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
8010820d:	83 ca 10             	or     $0x10,%edx
80108210:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
80108216:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108219:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
80108220:	83 ca 60             	or     $0x60,%edx
80108223:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
80108229:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010822c:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
80108233:	83 ca 80             	or     $0xffffff80,%edx
80108236:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
8010823c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010823f:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
80108246:	83 ca 0f             	or     $0xf,%edx
80108249:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
8010824f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108252:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
80108259:	83 e2 ef             	and    $0xffffffef,%edx
8010825c:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80108262:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108265:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
8010826c:	83 e2 df             	and    $0xffffffdf,%edx
8010826f:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80108275:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108278:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
8010827f:	83 ca 40             	or     $0x40,%edx
80108282:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80108288:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010828b:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
80108292:	83 ca 80             	or     $0xffffff80,%edx
80108295:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
8010829b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010829e:	c6 80 97 00 00 00 00 	movb   $0x0,0x97(%eax)
  c->gdt[SEG_UDATA] = SEG(STA_W, 0, 0xffffffff, DPL_USER);
801082a5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801082a8:	66 c7 80 98 00 00 00 	movw   $0xffff,0x98(%eax)
801082af:	ff ff 
801082b1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801082b4:	66 c7 80 9a 00 00 00 	movw   $0x0,0x9a(%eax)
801082bb:	00 00 
801082bd:	8b 45 f4             	mov    -0xc(%ebp),%eax
801082c0:	c6 80 9c 00 00 00 00 	movb   $0x0,0x9c(%eax)
801082c7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801082ca:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
801082d1:	83 e2 f0             	and    $0xfffffff0,%edx
801082d4:	83 ca 02             	or     $0x2,%edx
801082d7:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
801082dd:	8b 45 f4             	mov    -0xc(%ebp),%eax
801082e0:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
801082e7:	83 ca 10             	or     $0x10,%edx
801082ea:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
801082f0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801082f3:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
801082fa:	83 ca 60             	or     $0x60,%edx
801082fd:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
80108303:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108306:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
8010830d:	83 ca 80             	or     $0xffffff80,%edx
80108310:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
80108316:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108319:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
80108320:	83 ca 0f             	or     $0xf,%edx
80108323:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
80108329:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010832c:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
80108333:	83 e2 ef             	and    $0xffffffef,%edx
80108336:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
8010833c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010833f:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
80108346:	83 e2 df             	and    $0xffffffdf,%edx
80108349:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
8010834f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108352:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
80108359:	83 ca 40             	or     $0x40,%edx
8010835c:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
80108362:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108365:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
8010836c:	83 ca 80             	or     $0xffffff80,%edx
8010836f:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
80108375:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108378:	c6 80 9f 00 00 00 00 	movb   $0x0,0x9f(%eax)

  // Map cpu, and curproc
  c->gdt[SEG_KCPU] = SEG(STA_W, &c->cpu, 8, 0);
8010837f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108382:	05 b4 00 00 00       	add    $0xb4,%eax
80108387:	89 c3                	mov    %eax,%ebx
80108389:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010838c:	05 b4 00 00 00       	add    $0xb4,%eax
80108391:	c1 e8 10             	shr    $0x10,%eax
80108394:	89 c1                	mov    %eax,%ecx
80108396:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108399:	05 b4 00 00 00       	add    $0xb4,%eax
8010839e:	c1 e8 18             	shr    $0x18,%eax
801083a1:	89 c2                	mov    %eax,%edx
801083a3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801083a6:	66 c7 80 88 00 00 00 	movw   $0x0,0x88(%eax)
801083ad:	00 00 
801083af:	8b 45 f4             	mov    -0xc(%ebp),%eax
801083b2:	66 89 98 8a 00 00 00 	mov    %bx,0x8a(%eax)
801083b9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801083bc:	88 88 8c 00 00 00    	mov    %cl,0x8c(%eax)
801083c2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801083c5:	0f b6 88 8d 00 00 00 	movzbl 0x8d(%eax),%ecx
801083cc:	83 e1 f0             	and    $0xfffffff0,%ecx
801083cf:	83 c9 02             	or     $0x2,%ecx
801083d2:	88 88 8d 00 00 00    	mov    %cl,0x8d(%eax)
801083d8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801083db:	0f b6 88 8d 00 00 00 	movzbl 0x8d(%eax),%ecx
801083e2:	83 c9 10             	or     $0x10,%ecx
801083e5:	88 88 8d 00 00 00    	mov    %cl,0x8d(%eax)
801083eb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801083ee:	0f b6 88 8d 00 00 00 	movzbl 0x8d(%eax),%ecx
801083f5:	83 e1 9f             	and    $0xffffff9f,%ecx
801083f8:	88 88 8d 00 00 00    	mov    %cl,0x8d(%eax)
801083fe:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108401:	0f b6 88 8d 00 00 00 	movzbl 0x8d(%eax),%ecx
80108408:	83 c9 80             	or     $0xffffff80,%ecx
8010840b:	88 88 8d 00 00 00    	mov    %cl,0x8d(%eax)
80108411:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108414:	0f b6 88 8e 00 00 00 	movzbl 0x8e(%eax),%ecx
8010841b:	83 e1 f0             	and    $0xfffffff0,%ecx
8010841e:	88 88 8e 00 00 00    	mov    %cl,0x8e(%eax)
80108424:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108427:	0f b6 88 8e 00 00 00 	movzbl 0x8e(%eax),%ecx
8010842e:	83 e1 ef             	and    $0xffffffef,%ecx
80108431:	88 88 8e 00 00 00    	mov    %cl,0x8e(%eax)
80108437:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010843a:	0f b6 88 8e 00 00 00 	movzbl 0x8e(%eax),%ecx
80108441:	83 e1 df             	and    $0xffffffdf,%ecx
80108444:	88 88 8e 00 00 00    	mov    %cl,0x8e(%eax)
8010844a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010844d:	0f b6 88 8e 00 00 00 	movzbl 0x8e(%eax),%ecx
80108454:	83 c9 40             	or     $0x40,%ecx
80108457:	88 88 8e 00 00 00    	mov    %cl,0x8e(%eax)
8010845d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108460:	0f b6 88 8e 00 00 00 	movzbl 0x8e(%eax),%ecx
80108467:	83 c9 80             	or     $0xffffff80,%ecx
8010846a:	88 88 8e 00 00 00    	mov    %cl,0x8e(%eax)
80108470:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108473:	88 90 8f 00 00 00    	mov    %dl,0x8f(%eax)

  lgdt(c->gdt, sizeof(c->gdt));
80108479:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010847c:	83 c0 70             	add    $0x70,%eax
8010847f:	c7 44 24 04 38 00 00 	movl   $0x38,0x4(%esp)
80108486:	00 
80108487:	89 04 24             	mov    %eax,(%esp)
8010848a:	e8 51 fb ff ff       	call   80107fe0 <lgdt>
  loadgs(SEG_KCPU << 3);
8010848f:	c7 04 24 18 00 00 00 	movl   $0x18,(%esp)
80108496:	e8 84 fb ff ff       	call   8010801f <loadgs>

  // Initialize cpu-local storage.
  cpu = c;
8010849b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010849e:	65 a3 00 00 00 00    	mov    %eax,%gs:0x0
  proc = 0;
801084a4:	65 c7 05 04 00 00 00 	movl   $0x0,%gs:0x4
801084ab:	00 00 00 00 
}
801084af:	83 c4 24             	add    $0x24,%esp
801084b2:	5b                   	pop    %ebx
801084b3:	5d                   	pop    %ebp
801084b4:	c3                   	ret    

801084b5 <walkpgdir>:
// Return the address of the PTE in page table pgdir
// that corresponds to virtual address va.  If alloc!=0,
// create any required page table pages.
static pte_t *
walkpgdir(pde_t *pgdir, const void *va, int alloc)
{
801084b5:	55                   	push   %ebp
801084b6:	89 e5                	mov    %esp,%ebp
801084b8:	83 ec 28             	sub    $0x28,%esp
  pde_t *pde;
  pte_t *pgtab;

  pde = &pgdir[PDX(va)];
801084bb:	8b 45 0c             	mov    0xc(%ebp),%eax
801084be:	c1 e8 16             	shr    $0x16,%eax
801084c1:	c1 e0 02             	shl    $0x2,%eax
801084c4:	03 45 08             	add    0x8(%ebp),%eax
801084c7:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(*pde & PTE_P){
801084ca:	8b 45 f0             	mov    -0x10(%ebp),%eax
801084cd:	8b 00                	mov    (%eax),%eax
801084cf:	83 e0 01             	and    $0x1,%eax
801084d2:	84 c0                	test   %al,%al
801084d4:	74 14                	je     801084ea <walkpgdir+0x35>
    pgtab = (pte_t*)P2V(PTE_ADDR(*pde));
801084d6:	8b 45 f0             	mov    -0x10(%ebp),%eax
801084d9:	8b 00                	mov    (%eax),%eax
801084db:	25 00 f0 ff ff       	and    $0xfffff000,%eax
801084e0:	2d 00 00 00 80       	sub    $0x80000000,%eax
801084e5:	89 45 f4             	mov    %eax,-0xc(%ebp)
801084e8:	eb 48                	jmp    80108532 <walkpgdir+0x7d>
  } else {
    if(!alloc || (pgtab = (pte_t*)kalloc()) == 0)
801084ea:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
801084ee:	74 0e                	je     801084fe <walkpgdir+0x49>
801084f0:	e8 a9 ae ff ff       	call   8010339e <kalloc>
801084f5:	89 45 f4             	mov    %eax,-0xc(%ebp)
801084f8:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801084fc:	75 07                	jne    80108505 <walkpgdir+0x50>
      return 0;
801084fe:	b8 00 00 00 00       	mov    $0x0,%eax
80108503:	eb 3e                	jmp    80108543 <walkpgdir+0x8e>
    // Make sure all those PTE_P bits are zero.
    memset(pgtab, 0, PGSIZE);
80108505:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
8010850c:	00 
8010850d:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80108514:	00 
80108515:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108518:	89 04 24             	mov    %eax,(%esp)
8010851b:	e8 8a d4 ff ff       	call   801059aa <memset>
    // The permissions here are overly generous, but they can
    // be further restricted by the permissions in the page table
    // entries, if necessary.
    *pde = V2P(pgtab) | PTE_P | PTE_W | PTE_U;
80108520:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108523:	2d 00 00 00 80       	sub    $0x80000000,%eax
80108528:	89 c2                	mov    %eax,%edx
8010852a:	83 ca 07             	or     $0x7,%edx
8010852d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108530:	89 10                	mov    %edx,(%eax)
  }
  return &pgtab[PTX(va)];
80108532:	8b 45 0c             	mov    0xc(%ebp),%eax
80108535:	c1 e8 0c             	shr    $0xc,%eax
80108538:	25 ff 03 00 00       	and    $0x3ff,%eax
8010853d:	c1 e0 02             	shl    $0x2,%eax
80108540:	03 45 f4             	add    -0xc(%ebp),%eax
}
80108543:	c9                   	leave  
80108544:	c3                   	ret    

80108545 <mappages>:
// Create PTEs for virtual addresses starting at va that refer to
// physical addresses starting at pa. va and size might not
// be page-aligned.
static int
mappages(pde_t *pgdir, void *va, uint size, uint pa, int perm)
{
80108545:	55                   	push   %ebp
80108546:	89 e5                	mov    %esp,%ebp
80108548:	83 ec 28             	sub    $0x28,%esp
  char *a, *last;
  pte_t *pte;

  a = (char*)PGROUNDDOWN((uint)va);
8010854b:	8b 45 0c             	mov    0xc(%ebp),%eax
8010854e:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108553:	89 45 ec             	mov    %eax,-0x14(%ebp)
  last = (char*)PGROUNDDOWN(((uint)va) + size - 1);
80108556:	8b 45 0c             	mov    0xc(%ebp),%eax
80108559:	03 45 10             	add    0x10(%ebp),%eax
8010855c:	83 e8 01             	sub    $0x1,%eax
8010855f:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108564:	89 45 f0             	mov    %eax,-0x10(%ebp)
  for(;;){
    if((pte = walkpgdir(pgdir, a, 1)) == 0)
80108567:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
8010856e:	00 
8010856f:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108572:	89 44 24 04          	mov    %eax,0x4(%esp)
80108576:	8b 45 08             	mov    0x8(%ebp),%eax
80108579:	89 04 24             	mov    %eax,(%esp)
8010857c:	e8 34 ff ff ff       	call   801084b5 <walkpgdir>
80108581:	89 45 f4             	mov    %eax,-0xc(%ebp)
80108584:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80108588:	75 07                	jne    80108591 <mappages+0x4c>
      return -1;
8010858a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010858f:	eb 46                	jmp    801085d7 <mappages+0x92>
    if(*pte & PTE_P)
80108591:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108594:	8b 00                	mov    (%eax),%eax
80108596:	83 e0 01             	and    $0x1,%eax
80108599:	84 c0                	test   %al,%al
8010859b:	74 0c                	je     801085a9 <mappages+0x64>
      panic("remap");
8010859d:	c7 04 24 00 96 10 80 	movl   $0x80109600,(%esp)
801085a4:	e8 c0 82 ff ff       	call   80100869 <panic>
    *pte = pa | perm | PTE_P;
801085a9:	8b 45 18             	mov    0x18(%ebp),%eax
801085ac:	0b 45 14             	or     0x14(%ebp),%eax
801085af:	89 c2                	mov    %eax,%edx
801085b1:	83 ca 01             	or     $0x1,%edx
801085b4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801085b7:	89 10                	mov    %edx,(%eax)
    if(a == last)
801085b9:	8b 45 ec             	mov    -0x14(%ebp),%eax
801085bc:	3b 45 f0             	cmp    -0x10(%ebp),%eax
801085bf:	74 10                	je     801085d1 <mappages+0x8c>
      break;
    a += PGSIZE;
801085c1:	81 45 ec 00 10 00 00 	addl   $0x1000,-0x14(%ebp)
    pa += PGSIZE;
801085c8:	81 45 14 00 10 00 00 	addl   $0x1000,0x14(%ebp)
  }
801085cf:	eb 96                	jmp    80108567 <mappages+0x22>
      return -1;
    if(*pte & PTE_P)
      panic("remap");
    *pte = pa | perm | PTE_P;
    if(a == last)
      break;
801085d1:	90                   	nop
    a += PGSIZE;
    pa += PGSIZE;
  }
  return 0;
801085d2:	b8 00 00 00 00       	mov    $0x0,%eax
}
801085d7:	c9                   	leave  
801085d8:	c3                   	ret    

801085d9 <setupkvm>:
};

// Set up kernel part of a page table.
pde_t*
setupkvm(void)
{
801085d9:	55                   	push   %ebp
801085da:	89 e5                	mov    %esp,%ebp
801085dc:	53                   	push   %ebx
801085dd:	83 ec 34             	sub    $0x34,%esp
  pde_t *pgdir;
  struct kmap *k;

  if((pgdir = (pde_t*)kalloc()) == 0)
801085e0:	e8 b9 ad ff ff       	call   8010339e <kalloc>
801085e5:	89 45 f0             	mov    %eax,-0x10(%ebp)
801085e8:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801085ec:	75 07                	jne    801085f5 <setupkvm+0x1c>
    return 0;
801085ee:	b8 00 00 00 00       	mov    $0x0,%eax
801085f3:	eb 7b                	jmp    80108670 <setupkvm+0x97>
  memset(pgdir, 0, PGSIZE);
801085f5:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
801085fc:	00 
801085fd:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80108604:	00 
80108605:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108608:	89 04 24             	mov    %eax,(%esp)
8010860b:	e8 9a d3 ff ff       	call   801059aa <memset>
  if(P2V(PHYSTOP) > (void*)DEVSPACE)
80108610:	90                   	nop
    panic("setupkvm: PHYSTOP too high");
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
80108611:	c7 45 f4 e0 c4 10 80 	movl   $0x8010c4e0,-0xc(%ebp)
80108618:	eb 49                	jmp    80108663 <setupkvm+0x8a>
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start,
8010861a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010861d:	8b 48 0c             	mov    0xc(%eax),%ecx
80108620:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108623:	8b 50 04             	mov    0x4(%eax),%edx
80108626:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108629:	8b 58 08             	mov    0x8(%eax),%ebx
8010862c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010862f:	8b 40 04             	mov    0x4(%eax),%eax
80108632:	29 c3                	sub    %eax,%ebx
80108634:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108637:	8b 00                	mov    (%eax),%eax
80108639:	89 4c 24 10          	mov    %ecx,0x10(%esp)
8010863d:	89 54 24 0c          	mov    %edx,0xc(%esp)
80108641:	89 5c 24 08          	mov    %ebx,0x8(%esp)
80108645:	89 44 24 04          	mov    %eax,0x4(%esp)
80108649:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010864c:	89 04 24             	mov    %eax,(%esp)
8010864f:	e8 f1 fe ff ff       	call   80108545 <mappages>
80108654:	85 c0                	test   %eax,%eax
80108656:	79 07                	jns    8010865f <setupkvm+0x86>
                (uint)k->phys_start, k->perm) < 0)
      return 0;
80108658:	b8 00 00 00 00       	mov    $0x0,%eax
8010865d:	eb 11                	jmp    80108670 <setupkvm+0x97>
  if((pgdir = (pde_t*)kalloc()) == 0)
    return 0;
  memset(pgdir, 0, PGSIZE);
  if(P2V(PHYSTOP) > (void*)DEVSPACE)
    panic("setupkvm: PHYSTOP too high");
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
8010865f:	83 45 f4 10          	addl   $0x10,-0xc(%ebp)
80108663:	b8 20 c5 10 80       	mov    $0x8010c520,%eax
80108668:	39 45 f4             	cmp    %eax,-0xc(%ebp)
8010866b:	72 ad                	jb     8010861a <setupkvm+0x41>
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start,
                (uint)k->phys_start, k->perm) < 0)
      return 0;
  return pgdir;
8010866d:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
80108670:	83 c4 34             	add    $0x34,%esp
80108673:	5b                   	pop    %ebx
80108674:	5d                   	pop    %ebp
80108675:	c3                   	ret    

80108676 <kvmalloc>:

// Allocate one page table for the machine for the kernel address
// space for scheduler processes.
void
kvmalloc(void)
{
80108676:	55                   	push   %ebp
80108677:	89 e5                	mov    %esp,%ebp
80108679:	83 ec 18             	sub    $0x18,%esp
  kpgdir = setupkvm();
8010867c:	e8 58 ff ff ff       	call   801085d9 <setupkvm>
80108681:	a3 c4 ce 10 80       	mov    %eax,0x8010cec4
  if(kpgdir == 0)
80108686:	a1 c4 ce 10 80       	mov    0x8010cec4,%eax
8010868b:	85 c0                	test   %eax,%eax
8010868d:	75 0c                	jne    8010869b <kvmalloc+0x25>
    panic("kvmalloc: could not create kernel page table");
8010868f:	c7 04 24 08 96 10 80 	movl   $0x80109608,(%esp)
80108696:	e8 ce 81 ff ff       	call   80100869 <panic>
  switchkvm();
8010869b:	e8 02 00 00 00       	call   801086a2 <switchkvm>
}
801086a0:	c9                   	leave  
801086a1:	c3                   	ret    

801086a2 <switchkvm>:

// Switch h/w page table register to the kernel-only page table,
// for when no process is running.
void
switchkvm(void)
{
801086a2:	55                   	push   %ebp
801086a3:	89 e5                	mov    %esp,%ebp
801086a5:	83 ec 04             	sub    $0x4,%esp
  lcr3(V2P(kpgdir));   // switch to the kernel page table
801086a8:	a1 c4 ce 10 80       	mov    0x8010cec4,%eax
801086ad:	2d 00 00 00 80       	sub    $0x80000000,%eax
801086b2:	89 04 24             	mov    %eax,(%esp)
801086b5:	e8 7a f9 ff ff       	call   80108034 <lcr3>
}
801086ba:	c9                   	leave  
801086bb:	c3                   	ret    

801086bc <switchuvm>:

// Switch TSS and h/w page table to correspond to process p.
void
switchuvm(struct proc *p)
{
801086bc:	55                   	push   %ebp
801086bd:	89 e5                	mov    %esp,%ebp
801086bf:	53                   	push   %ebx
801086c0:	83 ec 14             	sub    $0x14,%esp
  pushcli();
801086c3:	e8 dd d1 ff ff       	call   801058a5 <pushcli>
  cpu->gdt[SEG_TSS] = SEG16(STS_T32A, &cpu->ts, sizeof(cpu->ts)-1, 0);
801086c8:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801086ce:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
801086d5:	83 c2 08             	add    $0x8,%edx
801086d8:	89 d3                	mov    %edx,%ebx
801086da:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
801086e1:	83 c2 08             	add    $0x8,%edx
801086e4:	c1 ea 10             	shr    $0x10,%edx
801086e7:	89 d1                	mov    %edx,%ecx
801086e9:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
801086f0:	83 c2 08             	add    $0x8,%edx
801086f3:	c1 ea 18             	shr    $0x18,%edx
801086f6:	66 c7 80 a0 00 00 00 	movw   $0x67,0xa0(%eax)
801086fd:	67 00 
801086ff:	66 89 98 a2 00 00 00 	mov    %bx,0xa2(%eax)
80108706:	88 88 a4 00 00 00    	mov    %cl,0xa4(%eax)
8010870c:	0f b6 88 a5 00 00 00 	movzbl 0xa5(%eax),%ecx
80108713:	83 e1 f0             	and    $0xfffffff0,%ecx
80108716:	83 c9 09             	or     $0x9,%ecx
80108719:	88 88 a5 00 00 00    	mov    %cl,0xa5(%eax)
8010871f:	0f b6 88 a5 00 00 00 	movzbl 0xa5(%eax),%ecx
80108726:	83 c9 10             	or     $0x10,%ecx
80108729:	88 88 a5 00 00 00    	mov    %cl,0xa5(%eax)
8010872f:	0f b6 88 a5 00 00 00 	movzbl 0xa5(%eax),%ecx
80108736:	83 e1 9f             	and    $0xffffff9f,%ecx
80108739:	88 88 a5 00 00 00    	mov    %cl,0xa5(%eax)
8010873f:	0f b6 88 a5 00 00 00 	movzbl 0xa5(%eax),%ecx
80108746:	83 c9 80             	or     $0xffffff80,%ecx
80108749:	88 88 a5 00 00 00    	mov    %cl,0xa5(%eax)
8010874f:	0f b6 88 a6 00 00 00 	movzbl 0xa6(%eax),%ecx
80108756:	83 e1 f0             	and    $0xfffffff0,%ecx
80108759:	88 88 a6 00 00 00    	mov    %cl,0xa6(%eax)
8010875f:	0f b6 88 a6 00 00 00 	movzbl 0xa6(%eax),%ecx
80108766:	83 e1 ef             	and    $0xffffffef,%ecx
80108769:	88 88 a6 00 00 00    	mov    %cl,0xa6(%eax)
8010876f:	0f b6 88 a6 00 00 00 	movzbl 0xa6(%eax),%ecx
80108776:	83 e1 df             	and    $0xffffffdf,%ecx
80108779:	88 88 a6 00 00 00    	mov    %cl,0xa6(%eax)
8010877f:	0f b6 88 a6 00 00 00 	movzbl 0xa6(%eax),%ecx
80108786:	83 c9 40             	or     $0x40,%ecx
80108789:	88 88 a6 00 00 00    	mov    %cl,0xa6(%eax)
8010878f:	0f b6 88 a6 00 00 00 	movzbl 0xa6(%eax),%ecx
80108796:	83 e1 7f             	and    $0x7f,%ecx
80108799:	88 88 a6 00 00 00    	mov    %cl,0xa6(%eax)
8010879f:	88 90 a7 00 00 00    	mov    %dl,0xa7(%eax)
  cpu->gdt[SEG_TSS].s = 0;
801087a5:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801087ab:	0f b6 90 a5 00 00 00 	movzbl 0xa5(%eax),%edx
801087b2:	83 e2 ef             	and    $0xffffffef,%edx
801087b5:	88 90 a5 00 00 00    	mov    %dl,0xa5(%eax)
  cpu->ts.ss0 = SEG_KDATA << 3;
801087bb:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801087c1:	66 c7 40 10 10 00    	movw   $0x10,0x10(%eax)
  cpu->ts.esp0 = (uint)proc->kstack + KSTACKSIZE;
801087c7:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801087cd:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
801087d4:	8b 52 08             	mov    0x8(%edx),%edx
801087d7:	81 c2 00 10 00 00    	add    $0x1000,%edx
801087dd:	89 50 0c             	mov    %edx,0xc(%eax)
  ltr(SEG_TSS << 3);
801087e0:	c7 04 24 30 00 00 00 	movl   $0x30,(%esp)
801087e7:	e8 1d f8 ff ff       	call   80108009 <ltr>
  if(p->pgdir == 0)
801087ec:	8b 45 08             	mov    0x8(%ebp),%eax
801087ef:	8b 40 04             	mov    0x4(%eax),%eax
801087f2:	85 c0                	test   %eax,%eax
801087f4:	75 0c                	jne    80108802 <switchuvm+0x146>
    panic("switchuvm: no pgdir");
801087f6:	c7 04 24 35 96 10 80 	movl   $0x80109635,(%esp)
801087fd:	e8 67 80 ff ff       	call   80100869 <panic>
  lcr3(V2P(p->pgdir));  // switch to new address space
80108802:	8b 45 08             	mov    0x8(%ebp),%eax
80108805:	8b 40 04             	mov    0x4(%eax),%eax
80108808:	2d 00 00 00 80       	sub    $0x80000000,%eax
8010880d:	89 04 24             	mov    %eax,(%esp)
80108810:	e8 1f f8 ff ff       	call   80108034 <lcr3>
  popcli();
80108815:	e8 d3 d0 ff ff       	call   801058ed <popcli>
}
8010881a:	83 c4 14             	add    $0x14,%esp
8010881d:	5b                   	pop    %ebx
8010881e:	5d                   	pop    %ebp
8010881f:	c3                   	ret    

80108820 <inituvm>:

// Load the initcode into address 0 of pgdir.
// sz must be less than a page.
void
inituvm(pde_t *pgdir, char *init, uint sz)
{
80108820:	55                   	push   %ebp
80108821:	89 e5                	mov    %esp,%ebp
80108823:	83 ec 38             	sub    $0x38,%esp
  char *mem;

  if(sz >= PGSIZE)
80108826:	81 7d 10 ff 0f 00 00 	cmpl   $0xfff,0x10(%ebp)
8010882d:	76 0c                	jbe    8010883b <inituvm+0x1b>
    panic("inituvm: more than a page");
8010882f:	c7 04 24 49 96 10 80 	movl   $0x80109649,(%esp)
80108836:	e8 2e 80 ff ff       	call   80100869 <panic>
  if ((mem = kalloc()) == 0)
8010883b:	e8 5e ab ff ff       	call   8010339e <kalloc>
80108840:	89 45 f4             	mov    %eax,-0xc(%ebp)
80108843:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80108847:	75 0c                	jne    80108855 <inituvm+0x35>
    panic("inituvm: cannot allocate memory");
80108849:	c7 04 24 64 96 10 80 	movl   $0x80109664,(%esp)
80108850:	e8 14 80 ff ff       	call   80100869 <panic>
  memset(mem, 0, PGSIZE);
80108855:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
8010885c:	00 
8010885d:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80108864:	00 
80108865:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108868:	89 04 24             	mov    %eax,(%esp)
8010886b:	e8 3a d1 ff ff       	call   801059aa <memset>
  if (mappages(pgdir, 0, PGSIZE, V2P(mem), PTE_W|PTE_U) < 0)
80108870:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108873:	2d 00 00 00 80       	sub    $0x80000000,%eax
80108878:	c7 44 24 10 06 00 00 	movl   $0x6,0x10(%esp)
8010887f:	00 
80108880:	89 44 24 0c          	mov    %eax,0xc(%esp)
80108884:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
8010888b:	00 
8010888c:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80108893:	00 
80108894:	8b 45 08             	mov    0x8(%ebp),%eax
80108897:	89 04 24             	mov    %eax,(%esp)
8010889a:	e8 a6 fc ff ff       	call   80108545 <mappages>
8010889f:	85 c0                	test   %eax,%eax
801088a1:	79 0c                	jns    801088af <inituvm+0x8f>
    panic("inituvm: cannot create pagetable");
801088a3:	c7 04 24 84 96 10 80 	movl   $0x80109684,(%esp)
801088aa:	e8 ba 7f ff ff       	call   80100869 <panic>
  memmove(mem, init, sz);
801088af:	8b 45 10             	mov    0x10(%ebp),%eax
801088b2:	89 44 24 08          	mov    %eax,0x8(%esp)
801088b6:	8b 45 0c             	mov    0xc(%ebp),%eax
801088b9:	89 44 24 04          	mov    %eax,0x4(%esp)
801088bd:	8b 45 f4             	mov    -0xc(%ebp),%eax
801088c0:	89 04 24             	mov    %eax,(%esp)
801088c3:	e8 b5 d1 ff ff       	call   80105a7d <memmove>
}
801088c8:	c9                   	leave  
801088c9:	c3                   	ret    

801088ca <loaduvm>:

// Load a program segment into pgdir.  addr must be page-aligned
// and the pages from addr to addr+sz must already be mapped.
int
loaduvm(pde_t *pgdir, char *addr, struct inode *ip, uint offset, uint sz)
{
801088ca:	55                   	push   %ebp
801088cb:	89 e5                	mov    %esp,%ebp
801088cd:	83 ec 28             	sub    $0x28,%esp
  uint i, pa, n;
  pte_t *pte;

  if((uint) addr % PGSIZE != 0)
801088d0:	8b 45 0c             	mov    0xc(%ebp),%eax
801088d3:	25 ff 0f 00 00       	and    $0xfff,%eax
801088d8:	85 c0                	test   %eax,%eax
801088da:	74 0c                	je     801088e8 <loaduvm+0x1e>
    panic("loaduvm: addr must be page aligned");
801088dc:	c7 04 24 a8 96 10 80 	movl   $0x801096a8,(%esp)
801088e3:	e8 81 7f ff ff       	call   80100869 <panic>
  for(i = 0; i < sz; i += PGSIZE){
801088e8:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
801088ef:	e9 ab 00 00 00       	jmp    8010899f <loaduvm+0xd5>
    if((pte = walkpgdir(pgdir, addr+i, 0)) == 0)
801088f4:	8b 45 e8             	mov    -0x18(%ebp),%eax
801088f7:	8b 55 0c             	mov    0xc(%ebp),%edx
801088fa:	8d 04 02             	lea    (%edx,%eax,1),%eax
801088fd:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80108904:	00 
80108905:	89 44 24 04          	mov    %eax,0x4(%esp)
80108909:	8b 45 08             	mov    0x8(%ebp),%eax
8010890c:	89 04 24             	mov    %eax,(%esp)
8010890f:	e8 a1 fb ff ff       	call   801084b5 <walkpgdir>
80108914:	89 45 f4             	mov    %eax,-0xc(%ebp)
80108917:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010891b:	75 0c                	jne    80108929 <loaduvm+0x5f>
      panic("loaduvm: address should exist");
8010891d:	c7 04 24 cb 96 10 80 	movl   $0x801096cb,(%esp)
80108924:	e8 40 7f ff ff       	call   80100869 <panic>
    pa = PTE_ADDR(*pte);
80108929:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010892c:	8b 00                	mov    (%eax),%eax
8010892e:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108933:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(sz - i < PGSIZE)
80108936:	8b 45 e8             	mov    -0x18(%ebp),%eax
80108939:	8b 55 18             	mov    0x18(%ebp),%edx
8010893c:	89 d1                	mov    %edx,%ecx
8010893e:	29 c1                	sub    %eax,%ecx
80108940:	89 c8                	mov    %ecx,%eax
80108942:	3d ff 0f 00 00       	cmp    $0xfff,%eax
80108947:	77 11                	ja     8010895a <loaduvm+0x90>
      n = sz - i;
80108949:	8b 45 e8             	mov    -0x18(%ebp),%eax
8010894c:	8b 55 18             	mov    0x18(%ebp),%edx
8010894f:	89 d1                	mov    %edx,%ecx
80108951:	29 c1                	sub    %eax,%ecx
80108953:	89 c8                	mov    %ecx,%eax
80108955:	89 45 f0             	mov    %eax,-0x10(%ebp)
80108958:	eb 07                	jmp    80108961 <loaduvm+0x97>
    else
      n = PGSIZE;
8010895a:	c7 45 f0 00 10 00 00 	movl   $0x1000,-0x10(%ebp)
    if(readi(ip, P2V(pa), offset+i, n) != n)
80108961:	8b 45 e8             	mov    -0x18(%ebp),%eax
80108964:	8b 55 14             	mov    0x14(%ebp),%edx
80108967:	8d 0c 02             	lea    (%edx,%eax,1),%ecx
8010896a:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010896d:	2d 00 00 00 80       	sub    $0x80000000,%eax
80108972:	8b 55 f0             	mov    -0x10(%ebp),%edx
80108975:	89 54 24 0c          	mov    %edx,0xc(%esp)
80108979:	89 4c 24 08          	mov    %ecx,0x8(%esp)
8010897d:	89 44 24 04          	mov    %eax,0x4(%esp)
80108981:	8b 45 10             	mov    0x10(%ebp),%eax
80108984:	89 04 24             	mov    %eax,(%esp)
80108987:	e8 e4 9b ff ff       	call   80102570 <readi>
8010898c:	3b 45 f0             	cmp    -0x10(%ebp),%eax
8010898f:	74 07                	je     80108998 <loaduvm+0xce>
      return -1;
80108991:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80108996:	eb 18                	jmp    801089b0 <loaduvm+0xe6>
  uint i, pa, n;
  pte_t *pte;

  if((uint) addr % PGSIZE != 0)
    panic("loaduvm: addr must be page aligned");
  for(i = 0; i < sz; i += PGSIZE){
80108998:	81 45 e8 00 10 00 00 	addl   $0x1000,-0x18(%ebp)
8010899f:	8b 45 e8             	mov    -0x18(%ebp),%eax
801089a2:	3b 45 18             	cmp    0x18(%ebp),%eax
801089a5:	0f 82 49 ff ff ff    	jb     801088f4 <loaduvm+0x2a>
    else
      n = PGSIZE;
    if(readi(ip, P2V(pa), offset+i, n) != n)
      return -1;
  }
  return 0;
801089ab:	b8 00 00 00 00       	mov    $0x0,%eax
}
801089b0:	c9                   	leave  
801089b1:	c3                   	ret    

801089b2 <allocuvm>:

// Allocate page tables and physical memory to grow process from oldsz to
// newsz, which need not be page aligned.  Returns new size or 0 on error.
int
allocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
801089b2:	55                   	push   %ebp
801089b3:	89 e5                	mov    %esp,%ebp
801089b5:	83 ec 38             	sub    $0x38,%esp
  char *mem;
  uint a;

  if(newsz >= KERNBASE)
801089b8:	8b 45 10             	mov    0x10(%ebp),%eax
801089bb:	85 c0                	test   %eax,%eax
801089bd:	79 0a                	jns    801089c9 <allocuvm+0x17>
    return 0;
801089bf:	b8 00 00 00 00       	mov    $0x0,%eax
801089c4:	e9 cf 00 00 00       	jmp    80108a98 <allocuvm+0xe6>
  if(newsz < oldsz)
801089c9:	8b 45 10             	mov    0x10(%ebp),%eax
801089cc:	3b 45 0c             	cmp    0xc(%ebp),%eax
801089cf:	73 08                	jae    801089d9 <allocuvm+0x27>
    return oldsz;
801089d1:	8b 45 0c             	mov    0xc(%ebp),%eax
801089d4:	e9 bf 00 00 00       	jmp    80108a98 <allocuvm+0xe6>

  a = PGROUNDUP(oldsz);
801089d9:	8b 45 0c             	mov    0xc(%ebp),%eax
801089dc:	05 ff 0f 00 00       	add    $0xfff,%eax
801089e1:	25 00 f0 ff ff       	and    $0xfffff000,%eax
801089e6:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(; a < newsz; a += PGSIZE){
801089e9:	e9 9b 00 00 00       	jmp    80108a89 <allocuvm+0xd7>
    mem = kalloc();
801089ee:	e8 ab a9 ff ff       	call   8010339e <kalloc>
801089f3:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(mem == 0){
801089f6:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801089fa:	75 2c                	jne    80108a28 <allocuvm+0x76>
      cprintf("allocuvm out of memory\n");
801089fc:	c7 04 24 e9 96 10 80 	movl   $0x801096e9,(%esp)
80108a03:	e8 c1 7c ff ff       	call   801006c9 <cprintf>
      deallocuvm(pgdir, newsz, oldsz);
80108a08:	8b 45 0c             	mov    0xc(%ebp),%eax
80108a0b:	89 44 24 08          	mov    %eax,0x8(%esp)
80108a0f:	8b 45 10             	mov    0x10(%ebp),%eax
80108a12:	89 44 24 04          	mov    %eax,0x4(%esp)
80108a16:	8b 45 08             	mov    0x8(%ebp),%eax
80108a19:	89 04 24             	mov    %eax,(%esp)
80108a1c:	e8 79 00 00 00       	call   80108a9a <deallocuvm>
      return 0;
80108a21:	b8 00 00 00 00       	mov    $0x0,%eax
80108a26:	eb 70                	jmp    80108a98 <allocuvm+0xe6>
    }
    memset(mem, 0, PGSIZE);
80108a28:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
80108a2f:	00 
80108a30:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80108a37:	00 
80108a38:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108a3b:	89 04 24             	mov    %eax,(%esp)
80108a3e:	e8 67 cf ff ff       	call   801059aa <memset>
    if (mappages(pgdir, (char*)a, PGSIZE, V2P(mem), PTE_W|PTE_U) < 0)
80108a43:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108a46:	8d 90 00 00 00 80    	lea    -0x80000000(%eax),%edx
80108a4c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108a4f:	c7 44 24 10 06 00 00 	movl   $0x6,0x10(%esp)
80108a56:	00 
80108a57:	89 54 24 0c          	mov    %edx,0xc(%esp)
80108a5b:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
80108a62:	00 
80108a63:	89 44 24 04          	mov    %eax,0x4(%esp)
80108a67:	8b 45 08             	mov    0x8(%ebp),%eax
80108a6a:	89 04 24             	mov    %eax,(%esp)
80108a6d:	e8 d3 fa ff ff       	call   80108545 <mappages>
80108a72:	85 c0                	test   %eax,%eax
80108a74:	79 0c                	jns    80108a82 <allocuvm+0xd0>
      panic("allocuvm: cannot create pagetable");
80108a76:	c7 04 24 04 97 10 80 	movl   $0x80109704,(%esp)
80108a7d:	e8 e7 7d ff ff       	call   80100869 <panic>
    return 0;
  if(newsz < oldsz)
    return oldsz;

  a = PGROUNDUP(oldsz);
  for(; a < newsz; a += PGSIZE){
80108a82:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
80108a89:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108a8c:	3b 45 10             	cmp    0x10(%ebp),%eax
80108a8f:	0f 82 59 ff ff ff    	jb     801089ee <allocuvm+0x3c>
    }
    memset(mem, 0, PGSIZE);
    if (mappages(pgdir, (char*)a, PGSIZE, V2P(mem), PTE_W|PTE_U) < 0)
      panic("allocuvm: cannot create pagetable");
  }
  return newsz;
80108a95:	8b 45 10             	mov    0x10(%ebp),%eax
}
80108a98:	c9                   	leave  
80108a99:	c3                   	ret    

80108a9a <deallocuvm>:
// newsz.  oldsz and newsz need not be page-aligned, nor does newsz
// need to be less than oldsz.  oldsz can be larger than the actual
// process size.  Returns the new process size.
int
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
80108a9a:	55                   	push   %ebp
80108a9b:	89 e5                	mov    %esp,%ebp
80108a9d:	83 ec 28             	sub    $0x28,%esp
  pte_t *pte;
  uint a, pa;

  if(newsz >= oldsz)
80108aa0:	8b 45 10             	mov    0x10(%ebp),%eax
80108aa3:	3b 45 0c             	cmp    0xc(%ebp),%eax
80108aa6:	72 08                	jb     80108ab0 <deallocuvm+0x16>
    return oldsz;
80108aa8:	8b 45 0c             	mov    0xc(%ebp),%eax
80108aab:	e9 9e 00 00 00       	jmp    80108b4e <deallocuvm+0xb4>

  a = PGROUNDUP(newsz);
80108ab0:	8b 45 10             	mov    0x10(%ebp),%eax
80108ab3:	05 ff 0f 00 00       	add    $0xfff,%eax
80108ab8:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108abd:	89 45 ec             	mov    %eax,-0x14(%ebp)
  for(; a  < oldsz; a += PGSIZE){
80108ac0:	eb 7d                	jmp    80108b3f <deallocuvm+0xa5>
    pte = walkpgdir(pgdir, (char*)a, 0);
80108ac2:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108ac5:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80108acc:	00 
80108acd:	89 44 24 04          	mov    %eax,0x4(%esp)
80108ad1:	8b 45 08             	mov    0x8(%ebp),%eax
80108ad4:	89 04 24             	mov    %eax,(%esp)
80108ad7:	e8 d9 f9 ff ff       	call   801084b5 <walkpgdir>
80108adc:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(!pte)
80108adf:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
80108ae3:	75 09                	jne    80108aee <deallocuvm+0x54>
      a += (NPTENTRIES - 1) * PGSIZE;
80108ae5:	81 45 ec 00 f0 3f 00 	addl   $0x3ff000,-0x14(%ebp)
80108aec:	eb 4a                	jmp    80108b38 <deallocuvm+0x9e>
    else if((*pte & PTE_P) != 0){
80108aee:	8b 45 e8             	mov    -0x18(%ebp),%eax
80108af1:	8b 00                	mov    (%eax),%eax
80108af3:	83 e0 01             	and    $0x1,%eax
80108af6:	84 c0                	test   %al,%al
80108af8:	74 3e                	je     80108b38 <deallocuvm+0x9e>
      pa = PTE_ADDR(*pte);
80108afa:	8b 45 e8             	mov    -0x18(%ebp),%eax
80108afd:	8b 00                	mov    (%eax),%eax
80108aff:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108b04:	89 45 f0             	mov    %eax,-0x10(%ebp)
      if(pa == 0)
80108b07:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80108b0b:	75 0c                	jne    80108b19 <deallocuvm+0x7f>
        panic("deallocuvm");
80108b0d:	c7 04 24 26 97 10 80 	movl   $0x80109726,(%esp)
80108b14:	e8 50 7d ff ff       	call   80100869 <panic>
      char *v = P2V(pa);
80108b19:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108b1c:	2d 00 00 00 80       	sub    $0x80000000,%eax
80108b21:	89 45 f4             	mov    %eax,-0xc(%ebp)
      kfree(v);
80108b24:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108b27:	89 04 24             	mov    %eax,(%esp)
80108b2a:	e8 d9 a7 ff ff       	call   80103308 <kfree>
      *pte = 0;
80108b2f:	8b 45 e8             	mov    -0x18(%ebp),%eax
80108b32:	c7 00 00 00 00 00    	movl   $0x0,(%eax)

  if(newsz >= oldsz)
    return oldsz;

  a = PGROUNDUP(newsz);
  for(; a  < oldsz; a += PGSIZE){
80108b38:	81 45 ec 00 10 00 00 	addl   $0x1000,-0x14(%ebp)
80108b3f:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108b42:	3b 45 0c             	cmp    0xc(%ebp),%eax
80108b45:	0f 82 77 ff ff ff    	jb     80108ac2 <deallocuvm+0x28>
      char *v = P2V(pa);
      kfree(v);
      *pte = 0;
    }
  }
  return newsz;
80108b4b:	8b 45 10             	mov    0x10(%ebp),%eax
}
80108b4e:	c9                   	leave  
80108b4f:	c3                   	ret    

80108b50 <freevm>:

// Free a page table and all the physical memory pages
// in the user part.
void
freevm(pde_t *pgdir)
{
80108b50:	55                   	push   %ebp
80108b51:	89 e5                	mov    %esp,%ebp
80108b53:	83 ec 28             	sub    $0x28,%esp
  uint i;

  if(pgdir == 0)
80108b56:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80108b5a:	75 0c                	jne    80108b68 <freevm+0x18>
    panic("freevm: no pgdir");
80108b5c:	c7 04 24 31 97 10 80 	movl   $0x80109731,(%esp)
80108b63:	e8 01 7d ff ff       	call   80100869 <panic>
  deallocuvm(pgdir, KERNBASE, 0);
80108b68:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80108b6f:	00 
80108b70:	c7 44 24 04 00 00 00 	movl   $0x80000000,0x4(%esp)
80108b77:	80 
80108b78:	8b 45 08             	mov    0x8(%ebp),%eax
80108b7b:	89 04 24             	mov    %eax,(%esp)
80108b7e:	e8 17 ff ff ff       	call   80108a9a <deallocuvm>
  for(i = 0; i < NPDENTRIES; i++){
80108b83:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
80108b8a:	eb 39                	jmp    80108bc5 <freevm+0x75>
    if(pgdir[i] & PTE_P){
80108b8c:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108b8f:	c1 e0 02             	shl    $0x2,%eax
80108b92:	03 45 08             	add    0x8(%ebp),%eax
80108b95:	8b 00                	mov    (%eax),%eax
80108b97:	83 e0 01             	and    $0x1,%eax
80108b9a:	84 c0                	test   %al,%al
80108b9c:	74 23                	je     80108bc1 <freevm+0x71>
      char * v = P2V(PTE_ADDR(pgdir[i]));
80108b9e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108ba1:	c1 e0 02             	shl    $0x2,%eax
80108ba4:	03 45 08             	add    0x8(%ebp),%eax
80108ba7:	8b 00                	mov    (%eax),%eax
80108ba9:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108bae:	2d 00 00 00 80       	sub    $0x80000000,%eax
80108bb3:	89 45 f4             	mov    %eax,-0xc(%ebp)
      kfree(v);
80108bb6:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108bb9:	89 04 24             	mov    %eax,(%esp)
80108bbc:	e8 47 a7 ff ff       	call   80103308 <kfree>
  uint i;

  if(pgdir == 0)
    panic("freevm: no pgdir");
  deallocuvm(pgdir, KERNBASE, 0);
  for(i = 0; i < NPDENTRIES; i++){
80108bc1:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
80108bc5:	81 7d f0 ff 03 00 00 	cmpl   $0x3ff,-0x10(%ebp)
80108bcc:	76 be                	jbe    80108b8c <freevm+0x3c>
    if(pgdir[i] & PTE_P){
      char * v = P2V(PTE_ADDR(pgdir[i]));
      kfree(v);
    }
  }
  kfree((char*)pgdir);
80108bce:	8b 45 08             	mov    0x8(%ebp),%eax
80108bd1:	89 04 24             	mov    %eax,(%esp)
80108bd4:	e8 2f a7 ff ff       	call   80103308 <kfree>
}
80108bd9:	c9                   	leave  
80108bda:	c3                   	ret    

80108bdb <clearpteu>:

// Clear PTE_U on a page. Used to create an inaccessible
// page beneath the user stack.
void
clearpteu(pde_t *pgdir, char *uva)
{
80108bdb:	55                   	push   %ebp
80108bdc:	89 e5                	mov    %esp,%ebp
80108bde:	83 ec 28             	sub    $0x28,%esp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
80108be1:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80108be8:	00 
80108be9:	8b 45 0c             	mov    0xc(%ebp),%eax
80108bec:	89 44 24 04          	mov    %eax,0x4(%esp)
80108bf0:	8b 45 08             	mov    0x8(%ebp),%eax
80108bf3:	89 04 24             	mov    %eax,(%esp)
80108bf6:	e8 ba f8 ff ff       	call   801084b5 <walkpgdir>
80108bfb:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(pte == 0)
80108bfe:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80108c02:	75 0c                	jne    80108c10 <clearpteu+0x35>
    panic("clearpteu");
80108c04:	c7 04 24 42 97 10 80 	movl   $0x80109742,(%esp)
80108c0b:	e8 59 7c ff ff       	call   80100869 <panic>
  *pte &= ~PTE_U;
80108c10:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108c13:	8b 00                	mov    (%eax),%eax
80108c15:	89 c2                	mov    %eax,%edx
80108c17:	83 e2 fb             	and    $0xfffffffb,%edx
80108c1a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108c1d:	89 10                	mov    %edx,(%eax)
}
80108c1f:	c9                   	leave  
80108c20:	c3                   	ret    

80108c21 <copyuvm>:

// Given a parent process's page table, create a copy
// of it for a child.
pde_t*
copyuvm(pde_t *pgdir, uint sz)
{
80108c21:	55                   	push   %ebp
80108c22:	89 e5                	mov    %esp,%ebp
80108c24:	83 ec 48             	sub    $0x48,%esp
  pde_t *d;
  pte_t *pte;
  uint pa, i, flags;
  char *mem;

  if((d = setupkvm()) == 0)
80108c27:	e8 ad f9 ff ff       	call   801085d9 <setupkvm>
80108c2c:	89 45 e0             	mov    %eax,-0x20(%ebp)
80108c2f:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
80108c33:	75 0a                	jne    80108c3f <copyuvm+0x1e>
    return 0;
80108c35:	b8 00 00 00 00       	mov    $0x0,%eax
80108c3a:	e9 f8 00 00 00       	jmp    80108d37 <copyuvm+0x116>
  for(i = 0; i < sz; i += PGSIZE){
80108c3f:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
80108c46:	e9 c7 00 00 00       	jmp    80108d12 <copyuvm+0xf1>
    if((pte = walkpgdir(pgdir, (void *) i, 0)) == 0)
80108c4b:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108c4e:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80108c55:	00 
80108c56:	89 44 24 04          	mov    %eax,0x4(%esp)
80108c5a:	8b 45 08             	mov    0x8(%ebp),%eax
80108c5d:	89 04 24             	mov    %eax,(%esp)
80108c60:	e8 50 f8 ff ff       	call   801084b5 <walkpgdir>
80108c65:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80108c68:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
80108c6c:	75 0c                	jne    80108c7a <copyuvm+0x59>
      panic("copyuvm: pte should exist");
80108c6e:	c7 04 24 4c 97 10 80 	movl   $0x8010974c,(%esp)
80108c75:	e8 ef 7b ff ff       	call   80100869 <panic>
    if(!(*pte & PTE_P))
80108c7a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80108c7d:	8b 00                	mov    (%eax),%eax
80108c7f:	83 e0 01             	and    $0x1,%eax
80108c82:	85 c0                	test   %eax,%eax
80108c84:	75 0c                	jne    80108c92 <copyuvm+0x71>
      panic("copyuvm: page not present");
80108c86:	c7 04 24 66 97 10 80 	movl   $0x80109766,(%esp)
80108c8d:	e8 d7 7b ff ff       	call   80100869 <panic>
    pa = PTE_ADDR(*pte);
80108c92:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80108c95:	8b 00                	mov    (%eax),%eax
80108c97:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108c9c:	89 45 e8             	mov    %eax,-0x18(%ebp)
    flags = PTE_FLAGS(*pte);
80108c9f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80108ca2:	8b 00                	mov    (%eax),%eax
80108ca4:	25 ff 0f 00 00       	and    $0xfff,%eax
80108ca9:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if((mem = kalloc()) == 0)
80108cac:	e8 ed a6 ff ff       	call   8010339e <kalloc>
80108cb1:	89 45 f4             	mov    %eax,-0xc(%ebp)
80108cb4:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80108cb8:	74 69                	je     80108d23 <copyuvm+0x102>
      goto bad;
    memmove(mem, (char*)P2V(pa), PGSIZE);
80108cba:	8b 45 e8             	mov    -0x18(%ebp),%eax
80108cbd:	2d 00 00 00 80       	sub    $0x80000000,%eax
80108cc2:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
80108cc9:	00 
80108cca:	89 44 24 04          	mov    %eax,0x4(%esp)
80108cce:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108cd1:	89 04 24             	mov    %eax,(%esp)
80108cd4:	e8 a4 cd ff ff       	call   80105a7d <memmove>
    if(mappages(d, (void*)i, PGSIZE, V2P(mem), flags) < 0)
80108cd9:	8b 55 f0             	mov    -0x10(%ebp),%edx
80108cdc:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108cdf:	8d 88 00 00 00 80    	lea    -0x80000000(%eax),%ecx
80108ce5:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108ce8:	89 54 24 10          	mov    %edx,0x10(%esp)
80108cec:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
80108cf0:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
80108cf7:	00 
80108cf8:	89 44 24 04          	mov    %eax,0x4(%esp)
80108cfc:	8b 45 e0             	mov    -0x20(%ebp),%eax
80108cff:	89 04 24             	mov    %eax,(%esp)
80108d02:	e8 3e f8 ff ff       	call   80108545 <mappages>
80108d07:	85 c0                	test   %eax,%eax
80108d09:	78 1b                	js     80108d26 <copyuvm+0x105>
  uint pa, i, flags;
  char *mem;

  if((d = setupkvm()) == 0)
    return 0;
  for(i = 0; i < sz; i += PGSIZE){
80108d0b:	81 45 ec 00 10 00 00 	addl   $0x1000,-0x14(%ebp)
80108d12:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108d15:	3b 45 0c             	cmp    0xc(%ebp),%eax
80108d18:	0f 82 2d ff ff ff    	jb     80108c4b <copyuvm+0x2a>
      goto bad;
    memmove(mem, (char*)P2V(pa), PGSIZE);
    if(mappages(d, (void*)i, PGSIZE, V2P(mem), flags) < 0)
      goto bad;
  }
  return d;
80108d1e:	8b 45 e0             	mov    -0x20(%ebp),%eax
80108d21:	eb 14                	jmp    80108d37 <copyuvm+0x116>
    if(!(*pte & PTE_P))
      panic("copyuvm: page not present");
    pa = PTE_ADDR(*pte);
    flags = PTE_FLAGS(*pte);
    if((mem = kalloc()) == 0)
      goto bad;
80108d23:	90                   	nop
80108d24:	eb 01                	jmp    80108d27 <copyuvm+0x106>
    memmove(mem, (char*)P2V(pa), PGSIZE);
    if(mappages(d, (void*)i, PGSIZE, V2P(mem), flags) < 0)
      goto bad;
80108d26:	90                   	nop
  }
  return d;

bad:
  freevm(d);
80108d27:	8b 45 e0             	mov    -0x20(%ebp),%eax
80108d2a:	89 04 24             	mov    %eax,(%esp)
80108d2d:	e8 1e fe ff ff       	call   80108b50 <freevm>
  return 0;
80108d32:	b8 00 00 00 00       	mov    $0x0,%eax
}
80108d37:	c9                   	leave  
80108d38:	c3                   	ret    

80108d39 <uva2ka>:

//PAGEBREAK!
// Map user virtual address to kernel address.
char*
uva2ka(pde_t *pgdir, char *uva)
{
80108d39:	55                   	push   %ebp
80108d3a:	89 e5                	mov    %esp,%ebp
80108d3c:	83 ec 28             	sub    $0x28,%esp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
80108d3f:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80108d46:	00 
80108d47:	8b 45 0c             	mov    0xc(%ebp),%eax
80108d4a:	89 44 24 04          	mov    %eax,0x4(%esp)
80108d4e:	8b 45 08             	mov    0x8(%ebp),%eax
80108d51:	89 04 24             	mov    %eax,(%esp)
80108d54:	e8 5c f7 ff ff       	call   801084b5 <walkpgdir>
80108d59:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((*pte & PTE_P) == 0)
80108d5c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108d5f:	8b 00                	mov    (%eax),%eax
80108d61:	83 e0 01             	and    $0x1,%eax
80108d64:	85 c0                	test   %eax,%eax
80108d66:	75 07                	jne    80108d6f <uva2ka+0x36>
    return 0;
80108d68:	b8 00 00 00 00       	mov    $0x0,%eax
80108d6d:	eb 22                	jmp    80108d91 <uva2ka+0x58>
  if((*pte & PTE_U) == 0)
80108d6f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108d72:	8b 00                	mov    (%eax),%eax
80108d74:	83 e0 04             	and    $0x4,%eax
80108d77:	85 c0                	test   %eax,%eax
80108d79:	75 07                	jne    80108d82 <uva2ka+0x49>
    return 0;
80108d7b:	b8 00 00 00 00       	mov    $0x0,%eax
80108d80:	eb 0f                	jmp    80108d91 <uva2ka+0x58>
  return (char*)P2V(PTE_ADDR(*pte));
80108d82:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108d85:	8b 00                	mov    (%eax),%eax
80108d87:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108d8c:	2d 00 00 00 80       	sub    $0x80000000,%eax
}
80108d91:	c9                   	leave  
80108d92:	c3                   	ret    

80108d93 <copyout>:
// Copy len bytes from p to user address va in page table pgdir.
// Most useful when pgdir is not the current page table.
// uva2ka ensures this only works for PTE_U pages.
int
copyout(pde_t *pgdir, uint va, void *p, uint len)
{
80108d93:	55                   	push   %ebp
80108d94:	89 e5                	mov    %esp,%ebp
80108d96:	83 ec 28             	sub    $0x28,%esp
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
80108d99:	8b 45 10             	mov    0x10(%ebp),%eax
80108d9c:	89 45 e8             	mov    %eax,-0x18(%ebp)
  while(len > 0){
80108d9f:	e9 8b 00 00 00       	jmp    80108e2f <copyout+0x9c>
    va0 = (uint)PGROUNDDOWN(va);
80108da4:	8b 45 0c             	mov    0xc(%ebp),%eax
80108da7:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108dac:	89 45 f4             	mov    %eax,-0xc(%ebp)
    pa0 = uva2ka(pgdir, (char*)va0);
80108daf:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108db2:	89 44 24 04          	mov    %eax,0x4(%esp)
80108db6:	8b 45 08             	mov    0x8(%ebp),%eax
80108db9:	89 04 24             	mov    %eax,(%esp)
80108dbc:	e8 78 ff ff ff       	call   80108d39 <uva2ka>
80108dc1:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(pa0 == 0)
80108dc4:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80108dc8:	75 07                	jne    80108dd1 <copyout+0x3e>
      return -1;
80108dca:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80108dcf:	eb 6d                	jmp    80108e3e <copyout+0xab>
    n = PGSIZE - (va - va0);
80108dd1:	8b 45 0c             	mov    0xc(%ebp),%eax
80108dd4:	8b 55 f4             	mov    -0xc(%ebp),%edx
80108dd7:	89 d1                	mov    %edx,%ecx
80108dd9:	29 c1                	sub    %eax,%ecx
80108ddb:	89 c8                	mov    %ecx,%eax
80108ddd:	05 00 10 00 00       	add    $0x1000,%eax
80108de2:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(n > len)
80108de5:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108de8:	3b 45 14             	cmp    0x14(%ebp),%eax
80108deb:	76 06                	jbe    80108df3 <copyout+0x60>
      n = len;
80108ded:	8b 45 14             	mov    0x14(%ebp),%eax
80108df0:	89 45 f0             	mov    %eax,-0x10(%ebp)
    memmove(pa0 + (va - va0), buf, n);
80108df3:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108df6:	8b 55 0c             	mov    0xc(%ebp),%edx
80108df9:	89 d1                	mov    %edx,%ecx
80108dfb:	29 c1                	sub    %eax,%ecx
80108dfd:	89 c8                	mov    %ecx,%eax
80108dff:	03 45 ec             	add    -0x14(%ebp),%eax
80108e02:	8b 55 f0             	mov    -0x10(%ebp),%edx
80108e05:	89 54 24 08          	mov    %edx,0x8(%esp)
80108e09:	8b 55 e8             	mov    -0x18(%ebp),%edx
80108e0c:	89 54 24 04          	mov    %edx,0x4(%esp)
80108e10:	89 04 24             	mov    %eax,(%esp)
80108e13:	e8 65 cc ff ff       	call   80105a7d <memmove>
    len -= n;
80108e18:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108e1b:	29 45 14             	sub    %eax,0x14(%ebp)
    buf += n;
80108e1e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108e21:	01 45 e8             	add    %eax,-0x18(%ebp)
    va = va0 + PGSIZE;
80108e24:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108e27:	05 00 10 00 00       	add    $0x1000,%eax
80108e2c:	89 45 0c             	mov    %eax,0xc(%ebp)
{
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
  while(len > 0){
80108e2f:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
80108e33:	0f 85 6b ff ff ff    	jne    80108da4 <copyout+0x11>
    memmove(pa0 + (va - va0), buf, n);
    len -= n;
    buf += n;
    va = va0 + PGSIZE;
  }
  return 0;
80108e39:	b8 00 00 00 00       	mov    $0x0,%eax
}
80108e3e:	c9                   	leave  
80108e3f:	c3                   	ret    
