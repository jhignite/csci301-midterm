
_idttest:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "types.h"
#include "stat.h"
#include "user.h"

int main(void)
{
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	83 e4 f0             	and    $0xfffffff0,%esp
   6:	83 ec 20             	sub    $0x20,%esp
    printf(1, "What kind of interrupt do you want? Give a number between 0 and 7 (inclusive): ");
   9:	c7 44 24 04 50 08 00 	movl   $0x850,0x4(%esp)
  10:	00 
  11:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  18:	e8 6b 04 00 00       	call   488 <printf>
    char buf[10];
    gets(buf, 10);
  1d:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
  24:	00 
  25:	8d 44 24 12          	lea    0x12(%esp),%eax
  29:	89 04 24             	mov    %eax,(%esp)
  2c:	e8 6c 01 00 00       	call   19d <gets>
    int interrupt = atoi(buf);
  31:	8d 44 24 12          	lea    0x12(%esp),%eax
  35:	89 04 24             	mov    %eax,(%esp)
  38:	e8 1d 02 00 00       	call   25a <atoi>
  3d:	89 44 24 1c          	mov    %eax,0x1c(%esp)
    printf(1, "You wanted interrupt %d. Ok, here it goes...\n", interrupt);
  41:	8b 44 24 1c          	mov    0x1c(%esp),%eax
  45:	89 44 24 08          	mov    %eax,0x8(%esp)
  49:	c7 44 24 04 a0 08 00 	movl   $0x8a0,0x4(%esp)
  50:	00 
  51:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  58:	e8 2b 04 00 00       	call   488 <printf>
    switch(interrupt) {
  5d:	83 7c 24 1c 07       	cmpl   $0x7,0x1c(%esp)
  62:	77 1e                	ja     82 <main+0x82>
  64:	8b 44 24 1c          	mov    0x1c(%esp),%eax
  68:	c1 e0 02             	shl    $0x2,%eax
  6b:	8b 80 d0 08 00 00    	mov    0x8d0(%eax),%eax
  71:	ff e0                	jmp    *%eax
        case 0: asm volatile("int $0\n"); // divide error
  73:	cd 00                	int    $0x0
        case 1: asm volatile("int $1\n"); // debug exception
  75:	cd 01                	int    $0x1
        case 2: asm volatile("int $2\n"); // non-maskable interrupt
  77:	cd 02                	int    $0x2
        case 3: asm volatile("int $3\n"); // breakpoint
  79:	cc                   	int3   
        case 4: asm volatile("int $4\n"); // overflow
  7a:	cd 04                	int    $0x4
        case 5: asm volatile("int $5\n"); // bounds check
  7c:	cd 05                	int    $0x5
        case 6: asm volatile("int $6\n"); // illegal opcode
  7e:	cd 06                	int    $0x6
        case 7: asm volatile("int $7\n"); // device not available
  80:	cd 07                	int    $0x7
    }
    exit();
  82:	e8 65 02 00 00       	call   2ec <exit>
  87:	90                   	nop

00000088 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
  88:	55                   	push   %ebp
  89:	89 e5                	mov    %esp,%ebp
  8b:	57                   	push   %edi
  8c:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
  8d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  90:	8b 55 10             	mov    0x10(%ebp),%edx
  93:	8b 45 0c             	mov    0xc(%ebp),%eax
  96:	89 cb                	mov    %ecx,%ebx
  98:	89 df                	mov    %ebx,%edi
  9a:	89 d1                	mov    %edx,%ecx
  9c:	fc                   	cld    
  9d:	f3 aa                	rep stos %al,%es:(%edi)
  9f:	89 ca                	mov    %ecx,%edx
  a1:	89 fb                	mov    %edi,%ebx
  a3:	89 5d 08             	mov    %ebx,0x8(%ebp)
  a6:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
  a9:	5b                   	pop    %ebx
  aa:	5f                   	pop    %edi
  ab:	5d                   	pop    %ebp
  ac:	c3                   	ret    

000000ad <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
  ad:	55                   	push   %ebp
  ae:	89 e5                	mov    %esp,%ebp
  b0:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
  b3:	8b 45 08             	mov    0x8(%ebp),%eax
  b6:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
  b9:	8b 45 0c             	mov    0xc(%ebp),%eax
  bc:	0f b6 10             	movzbl (%eax),%edx
  bf:	8b 45 08             	mov    0x8(%ebp),%eax
  c2:	88 10                	mov    %dl,(%eax)
  c4:	8b 45 08             	mov    0x8(%ebp),%eax
  c7:	0f b6 00             	movzbl (%eax),%eax
  ca:	84 c0                	test   %al,%al
  cc:	0f 95 c0             	setne  %al
  cf:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  d3:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
  d7:	84 c0                	test   %al,%al
  d9:	75 de                	jne    b9 <strcpy+0xc>
    ;
  return os;
  db:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  de:	c9                   	leave  
  df:	c3                   	ret    

000000e0 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  e0:	55                   	push   %ebp
  e1:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
  e3:	eb 08                	jmp    ed <strcmp+0xd>
    p++, q++;
  e5:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  e9:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
  ed:	8b 45 08             	mov    0x8(%ebp),%eax
  f0:	0f b6 00             	movzbl (%eax),%eax
  f3:	84 c0                	test   %al,%al
  f5:	74 10                	je     107 <strcmp+0x27>
  f7:	8b 45 08             	mov    0x8(%ebp),%eax
  fa:	0f b6 10             	movzbl (%eax),%edx
  fd:	8b 45 0c             	mov    0xc(%ebp),%eax
 100:	0f b6 00             	movzbl (%eax),%eax
 103:	38 c2                	cmp    %al,%dl
 105:	74 de                	je     e5 <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 107:	8b 45 08             	mov    0x8(%ebp),%eax
 10a:	0f b6 00             	movzbl (%eax),%eax
 10d:	0f b6 d0             	movzbl %al,%edx
 110:	8b 45 0c             	mov    0xc(%ebp),%eax
 113:	0f b6 00             	movzbl (%eax),%eax
 116:	0f b6 c0             	movzbl %al,%eax
 119:	89 d1                	mov    %edx,%ecx
 11b:	29 c1                	sub    %eax,%ecx
 11d:	89 c8                	mov    %ecx,%eax
}
 11f:	5d                   	pop    %ebp
 120:	c3                   	ret    

00000121 <strlen>:

uint
strlen(char *s)
{
 121:	55                   	push   %ebp
 122:	89 e5                	mov    %esp,%ebp
 124:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 127:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 12e:	eb 04                	jmp    134 <strlen+0x13>
 130:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 134:	8b 45 fc             	mov    -0x4(%ebp),%eax
 137:	03 45 08             	add    0x8(%ebp),%eax
 13a:	0f b6 00             	movzbl (%eax),%eax
 13d:	84 c0                	test   %al,%al
 13f:	75 ef                	jne    130 <strlen+0xf>
    ;
  return n;
 141:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 144:	c9                   	leave  
 145:	c3                   	ret    

00000146 <memset>:

void*
memset(void *dst, int c, uint n)
{
 146:	55                   	push   %ebp
 147:	89 e5                	mov    %esp,%ebp
 149:	83 ec 0c             	sub    $0xc,%esp
  stosb(dst, c, n);
 14c:	8b 45 10             	mov    0x10(%ebp),%eax
 14f:	89 44 24 08          	mov    %eax,0x8(%esp)
 153:	8b 45 0c             	mov    0xc(%ebp),%eax
 156:	89 44 24 04          	mov    %eax,0x4(%esp)
 15a:	8b 45 08             	mov    0x8(%ebp),%eax
 15d:	89 04 24             	mov    %eax,(%esp)
 160:	e8 23 ff ff ff       	call   88 <stosb>
  return dst;
 165:	8b 45 08             	mov    0x8(%ebp),%eax
}
 168:	c9                   	leave  
 169:	c3                   	ret    

0000016a <strchr>:

char*
strchr(const char *s, char c)
{
 16a:	55                   	push   %ebp
 16b:	89 e5                	mov    %esp,%ebp
 16d:	83 ec 04             	sub    $0x4,%esp
 170:	8b 45 0c             	mov    0xc(%ebp),%eax
 173:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 176:	eb 14                	jmp    18c <strchr+0x22>
    if(*s == c)
 178:	8b 45 08             	mov    0x8(%ebp),%eax
 17b:	0f b6 00             	movzbl (%eax),%eax
 17e:	3a 45 fc             	cmp    -0x4(%ebp),%al
 181:	75 05                	jne    188 <strchr+0x1e>
      return (char*)s;
 183:	8b 45 08             	mov    0x8(%ebp),%eax
 186:	eb 13                	jmp    19b <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 188:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 18c:	8b 45 08             	mov    0x8(%ebp),%eax
 18f:	0f b6 00             	movzbl (%eax),%eax
 192:	84 c0                	test   %al,%al
 194:	75 e2                	jne    178 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 196:	b8 00 00 00 00       	mov    $0x0,%eax
}
 19b:	c9                   	leave  
 19c:	c3                   	ret    

0000019d <gets>:

char*
gets(char *buf, int max)
{
 19d:	55                   	push   %ebp
 19e:	89 e5                	mov    %esp,%ebp
 1a0:	83 ec 28             	sub    $0x28,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 1a3:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 1aa:	eb 44                	jmp    1f0 <gets+0x53>
    cc = read(0, &c, 1);
 1ac:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 1b3:	00 
 1b4:	8d 45 ef             	lea    -0x11(%ebp),%eax
 1b7:	89 44 24 04          	mov    %eax,0x4(%esp)
 1bb:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 1c2:	e8 3d 01 00 00       	call   304 <read>
 1c7:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(cc < 1)
 1ca:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 1ce:	7e 2d                	jle    1fd <gets+0x60>
      break;
    buf[i++] = c;
 1d0:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1d3:	03 45 08             	add    0x8(%ebp),%eax
 1d6:	0f b6 55 ef          	movzbl -0x11(%ebp),%edx
 1da:	88 10                	mov    %dl,(%eax)
 1dc:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
    if(c == '\n' || c == '\r')
 1e0:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 1e4:	3c 0a                	cmp    $0xa,%al
 1e6:	74 16                	je     1fe <gets+0x61>
 1e8:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 1ec:	3c 0d                	cmp    $0xd,%al
 1ee:	74 0e                	je     1fe <gets+0x61>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 1f0:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1f3:	83 c0 01             	add    $0x1,%eax
 1f6:	3b 45 0c             	cmp    0xc(%ebp),%eax
 1f9:	7c b1                	jl     1ac <gets+0xf>
 1fb:	eb 01                	jmp    1fe <gets+0x61>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 1fd:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 1fe:	8b 45 f0             	mov    -0x10(%ebp),%eax
 201:	03 45 08             	add    0x8(%ebp),%eax
 204:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 207:	8b 45 08             	mov    0x8(%ebp),%eax
}
 20a:	c9                   	leave  
 20b:	c3                   	ret    

0000020c <stat>:

int
stat(char *n, struct stat *st)
{
 20c:	55                   	push   %ebp
 20d:	89 e5                	mov    %esp,%ebp
 20f:	83 ec 28             	sub    $0x28,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 212:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 219:	00 
 21a:	8b 45 08             	mov    0x8(%ebp),%eax
 21d:	89 04 24             	mov    %eax,(%esp)
 220:	e8 07 01 00 00       	call   32c <open>
 225:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(fd < 0)
 228:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 22c:	79 07                	jns    235 <stat+0x29>
    return -1;
 22e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 233:	eb 23                	jmp    258 <stat+0x4c>
  r = fstat(fd, st);
 235:	8b 45 0c             	mov    0xc(%ebp),%eax
 238:	89 44 24 04          	mov    %eax,0x4(%esp)
 23c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 23f:	89 04 24             	mov    %eax,(%esp)
 242:	e8 fd 00 00 00       	call   344 <fstat>
 247:	89 45 f4             	mov    %eax,-0xc(%ebp)
  close(fd);
 24a:	8b 45 f0             	mov    -0x10(%ebp),%eax
 24d:	89 04 24             	mov    %eax,(%esp)
 250:	e8 bf 00 00 00       	call   314 <close>
  return r;
 255:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
 258:	c9                   	leave  
 259:	c3                   	ret    

0000025a <atoi>:

int
atoi(const char *s)
{
 25a:	55                   	push   %ebp
 25b:	89 e5                	mov    %esp,%ebp
 25d:	83 ec 10             	sub    $0x10,%esp
  int n;

  n = 0;
 260:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while('0' <= *s && *s <= '9')
 267:	eb 24                	jmp    28d <atoi+0x33>
    n = n*10 + *s++ - '0';
 269:	8b 55 fc             	mov    -0x4(%ebp),%edx
 26c:	89 d0                	mov    %edx,%eax
 26e:	c1 e0 02             	shl    $0x2,%eax
 271:	01 d0                	add    %edx,%eax
 273:	01 c0                	add    %eax,%eax
 275:	89 c2                	mov    %eax,%edx
 277:	8b 45 08             	mov    0x8(%ebp),%eax
 27a:	0f b6 00             	movzbl (%eax),%eax
 27d:	0f be c0             	movsbl %al,%eax
 280:	8d 04 02             	lea    (%edx,%eax,1),%eax
 283:	83 e8 30             	sub    $0x30,%eax
 286:	89 45 fc             	mov    %eax,-0x4(%ebp)
 289:	83 45 08 01          	addl   $0x1,0x8(%ebp)
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 28d:	8b 45 08             	mov    0x8(%ebp),%eax
 290:	0f b6 00             	movzbl (%eax),%eax
 293:	3c 2f                	cmp    $0x2f,%al
 295:	7e 0a                	jle    2a1 <atoi+0x47>
 297:	8b 45 08             	mov    0x8(%ebp),%eax
 29a:	0f b6 00             	movzbl (%eax),%eax
 29d:	3c 39                	cmp    $0x39,%al
 29f:	7e c8                	jle    269 <atoi+0xf>
    n = n*10 + *s++ - '0';
  return n;
 2a1:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 2a4:	c9                   	leave  
 2a5:	c3                   	ret    

000002a6 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 2a6:	55                   	push   %ebp
 2a7:	89 e5                	mov    %esp,%ebp
 2a9:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 2ac:	8b 45 08             	mov    0x8(%ebp),%eax
 2af:	89 45 f8             	mov    %eax,-0x8(%ebp)
  src = vsrc;
 2b2:	8b 45 0c             	mov    0xc(%ebp),%eax
 2b5:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0)
 2b8:	eb 13                	jmp    2cd <memmove+0x27>
    *dst++ = *src++;
 2ba:	8b 45 fc             	mov    -0x4(%ebp),%eax
 2bd:	0f b6 10             	movzbl (%eax),%edx
 2c0:	8b 45 f8             	mov    -0x8(%ebp),%eax
 2c3:	88 10                	mov    %dl,(%eax)
 2c5:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
 2c9:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 2cd:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
 2d1:	0f 9f c0             	setg   %al
 2d4:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
 2d8:	84 c0                	test   %al,%al
 2da:	75 de                	jne    2ba <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 2dc:	8b 45 08             	mov    0x8(%ebp),%eax
}
 2df:	c9                   	leave  
 2e0:	c3                   	ret    
 2e1:	90                   	nop
 2e2:	90                   	nop
 2e3:	90                   	nop

000002e4 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 2e4:	b8 01 00 00 00       	mov    $0x1,%eax
 2e9:	cd 40                	int    $0x40
 2eb:	c3                   	ret    

000002ec <exit>:
SYSCALL(exit)
 2ec:	b8 02 00 00 00       	mov    $0x2,%eax
 2f1:	cd 40                	int    $0x40
 2f3:	c3                   	ret    

000002f4 <wait>:
SYSCALL(wait)
 2f4:	b8 03 00 00 00       	mov    $0x3,%eax
 2f9:	cd 40                	int    $0x40
 2fb:	c3                   	ret    

000002fc <pipe>:
SYSCALL(pipe)
 2fc:	b8 04 00 00 00       	mov    $0x4,%eax
 301:	cd 40                	int    $0x40
 303:	c3                   	ret    

00000304 <read>:
SYSCALL(read)
 304:	b8 05 00 00 00       	mov    $0x5,%eax
 309:	cd 40                	int    $0x40
 30b:	c3                   	ret    

0000030c <write>:
SYSCALL(write)
 30c:	b8 10 00 00 00       	mov    $0x10,%eax
 311:	cd 40                	int    $0x40
 313:	c3                   	ret    

00000314 <close>:
SYSCALL(close)
 314:	b8 15 00 00 00       	mov    $0x15,%eax
 319:	cd 40                	int    $0x40
 31b:	c3                   	ret    

0000031c <kill>:
SYSCALL(kill)
 31c:	b8 06 00 00 00       	mov    $0x6,%eax
 321:	cd 40                	int    $0x40
 323:	c3                   	ret    

00000324 <exec>:
SYSCALL(exec)
 324:	b8 07 00 00 00       	mov    $0x7,%eax
 329:	cd 40                	int    $0x40
 32b:	c3                   	ret    

0000032c <open>:
SYSCALL(open)
 32c:	b8 0f 00 00 00       	mov    $0xf,%eax
 331:	cd 40                	int    $0x40
 333:	c3                   	ret    

00000334 <mknod>:
SYSCALL(mknod)
 334:	b8 11 00 00 00       	mov    $0x11,%eax
 339:	cd 40                	int    $0x40
 33b:	c3                   	ret    

0000033c <unlink>:
SYSCALL(unlink)
 33c:	b8 12 00 00 00       	mov    $0x12,%eax
 341:	cd 40                	int    $0x40
 343:	c3                   	ret    

00000344 <fstat>:
SYSCALL(fstat)
 344:	b8 08 00 00 00       	mov    $0x8,%eax
 349:	cd 40                	int    $0x40
 34b:	c3                   	ret    

0000034c <link>:
SYSCALL(link)
 34c:	b8 13 00 00 00       	mov    $0x13,%eax
 351:	cd 40                	int    $0x40
 353:	c3                   	ret    

00000354 <mkdir>:
SYSCALL(mkdir)
 354:	b8 14 00 00 00       	mov    $0x14,%eax
 359:	cd 40                	int    $0x40
 35b:	c3                   	ret    

0000035c <chdir>:
SYSCALL(chdir)
 35c:	b8 09 00 00 00       	mov    $0x9,%eax
 361:	cd 40                	int    $0x40
 363:	c3                   	ret    

00000364 <dup>:
SYSCALL(dup)
 364:	b8 0a 00 00 00       	mov    $0xa,%eax
 369:	cd 40                	int    $0x40
 36b:	c3                   	ret    

0000036c <getpid>:
SYSCALL(getpid)
 36c:	b8 0b 00 00 00       	mov    $0xb,%eax
 371:	cd 40                	int    $0x40
 373:	c3                   	ret    

00000374 <sbrk>:
SYSCALL(sbrk)
 374:	b8 0c 00 00 00       	mov    $0xc,%eax
 379:	cd 40                	int    $0x40
 37b:	c3                   	ret    

0000037c <sleep>:
SYSCALL(sleep)
 37c:	b8 0d 00 00 00       	mov    $0xd,%eax
 381:	cd 40                	int    $0x40
 383:	c3                   	ret    

00000384 <uptime>:
SYSCALL(uptime)
 384:	b8 0e 00 00 00       	mov    $0xe,%eax
 389:	cd 40                	int    $0x40
 38b:	c3                   	ret    

0000038c <start_capture>:
SYSCALL(start_capture)
 38c:	b8 1e 00 00 00       	mov    $0x1e,%eax
 391:	cd 40                	int    $0x40
 393:	c3                   	ret    

00000394 <stop_capture>:
SYSCALL(stop_capture)
 394:	b8 1f 00 00 00       	mov    $0x1f,%eax
 399:	cd 40                	int    $0x40
 39b:	c3                   	ret    

0000039c <switch_scheduler>:
SYSCALL(switch_scheduler)
 39c:	b8 20 00 00 00       	mov    $0x20,%eax
 3a1:	cd 40                	int    $0x40
 3a3:	c3                   	ret    

000003a4 <syscallstats>:
SYSCALL(syscallstats)
 3a4:	b8 21 00 00 00       	mov    $0x21,%eax
 3a9:	cd 40                	int    $0x40
 3ab:	c3                   	ret    

000003ac <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 3ac:	55                   	push   %ebp
 3ad:	89 e5                	mov    %esp,%ebp
 3af:	83 ec 28             	sub    $0x28,%esp
 3b2:	8b 45 0c             	mov    0xc(%ebp),%eax
 3b5:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 3b8:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 3bf:	00 
 3c0:	8d 45 f4             	lea    -0xc(%ebp),%eax
 3c3:	89 44 24 04          	mov    %eax,0x4(%esp)
 3c7:	8b 45 08             	mov    0x8(%ebp),%eax
 3ca:	89 04 24             	mov    %eax,(%esp)
 3cd:	e8 3a ff ff ff       	call   30c <write>
}
 3d2:	c9                   	leave  
 3d3:	c3                   	ret    

000003d4 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 3d4:	55                   	push   %ebp
 3d5:	89 e5                	mov    %esp,%ebp
 3d7:	53                   	push   %ebx
 3d8:	83 ec 44             	sub    $0x44,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 3db:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 3e2:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 3e6:	74 17                	je     3ff <printint+0x2b>
 3e8:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 3ec:	79 11                	jns    3ff <printint+0x2b>
    neg = 1;
 3ee:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 3f5:	8b 45 0c             	mov    0xc(%ebp),%eax
 3f8:	f7 d8                	neg    %eax
 3fa:	89 45 f4             	mov    %eax,-0xc(%ebp)
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
 3fd:	eb 06                	jmp    405 <printint+0x31>
    neg = 1;
    x = -xx;
  } else {
    x = xx;
 3ff:	8b 45 0c             	mov    0xc(%ebp),%eax
 402:	89 45 f4             	mov    %eax,-0xc(%ebp)
  }

  i = 0;
 405:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  do{
    buf[i++] = digits[x % base];
 40c:	8b 4d ec             	mov    -0x14(%ebp),%ecx
 40f:	8b 5d 10             	mov    0x10(%ebp),%ebx
 412:	8b 45 f4             	mov    -0xc(%ebp),%eax
 415:	ba 00 00 00 00       	mov    $0x0,%edx
 41a:	f7 f3                	div    %ebx
 41c:	89 d0                	mov    %edx,%eax
 41e:	0f b6 80 f8 08 00 00 	movzbl 0x8f8(%eax),%eax
 425:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
 429:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
  }while((x /= base) != 0);
 42d:	8b 45 10             	mov    0x10(%ebp),%eax
 430:	89 45 d4             	mov    %eax,-0x2c(%ebp)
 433:	8b 45 f4             	mov    -0xc(%ebp),%eax
 436:	ba 00 00 00 00       	mov    $0x0,%edx
 43b:	f7 75 d4             	divl   -0x2c(%ebp)
 43e:	89 45 f4             	mov    %eax,-0xc(%ebp)
 441:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 445:	75 c5                	jne    40c <printint+0x38>
  if(neg)
 447:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 44b:	74 2a                	je     477 <printint+0xa3>
    buf[i++] = '-';
 44d:	8b 45 ec             	mov    -0x14(%ebp),%eax
 450:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)
 455:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)

  while(--i >= 0)
 459:	eb 1d                	jmp    478 <printint+0xa4>
    putc(fd, buf[i]);
 45b:	8b 45 ec             	mov    -0x14(%ebp),%eax
 45e:	0f b6 44 05 dc       	movzbl -0x24(%ebp,%eax,1),%eax
 463:	0f be c0             	movsbl %al,%eax
 466:	89 44 24 04          	mov    %eax,0x4(%esp)
 46a:	8b 45 08             	mov    0x8(%ebp),%eax
 46d:	89 04 24             	mov    %eax,(%esp)
 470:	e8 37 ff ff ff       	call   3ac <putc>
 475:	eb 01                	jmp    478 <printint+0xa4>
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 477:	90                   	nop
 478:	83 6d ec 01          	subl   $0x1,-0x14(%ebp)
 47c:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 480:	79 d9                	jns    45b <printint+0x87>
    putc(fd, buf[i]);
}
 482:	83 c4 44             	add    $0x44,%esp
 485:	5b                   	pop    %ebx
 486:	5d                   	pop    %ebp
 487:	c3                   	ret    

00000488 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 488:	55                   	push   %ebp
 489:	89 e5                	mov    %esp,%ebp
 48b:	83 ec 38             	sub    $0x38,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 48e:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 495:	8d 45 0c             	lea    0xc(%ebp),%eax
 498:	83 c0 04             	add    $0x4,%eax
 49b:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(i = 0; fmt[i]; i++){
 49e:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 4a5:	e9 7e 01 00 00       	jmp    628 <printf+0x1a0>
    c = fmt[i] & 0xff;
 4aa:	8b 55 0c             	mov    0xc(%ebp),%edx
 4ad:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4b0:	8d 04 02             	lea    (%edx,%eax,1),%eax
 4b3:	0f b6 00             	movzbl (%eax),%eax
 4b6:	0f be c0             	movsbl %al,%eax
 4b9:	25 ff 00 00 00       	and    $0xff,%eax
 4be:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(state == 0){
 4c1:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 4c5:	75 2c                	jne    4f3 <printf+0x6b>
      if(c == '%'){
 4c7:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 4cb:	75 0c                	jne    4d9 <printf+0x51>
        state = '%';
 4cd:	c7 45 f0 25 00 00 00 	movl   $0x25,-0x10(%ebp)
 4d4:	e9 4b 01 00 00       	jmp    624 <printf+0x19c>
      } else {
        putc(fd, c);
 4d9:	8b 45 e8             	mov    -0x18(%ebp),%eax
 4dc:	0f be c0             	movsbl %al,%eax
 4df:	89 44 24 04          	mov    %eax,0x4(%esp)
 4e3:	8b 45 08             	mov    0x8(%ebp),%eax
 4e6:	89 04 24             	mov    %eax,(%esp)
 4e9:	e8 be fe ff ff       	call   3ac <putc>
 4ee:	e9 31 01 00 00       	jmp    624 <printf+0x19c>
      }
    } else if(state == '%'){
 4f3:	83 7d f0 25          	cmpl   $0x25,-0x10(%ebp)
 4f7:	0f 85 27 01 00 00    	jne    624 <printf+0x19c>
      if(c == 'd'){
 4fd:	83 7d e8 64          	cmpl   $0x64,-0x18(%ebp)
 501:	75 2d                	jne    530 <printf+0xa8>
        printint(fd, *ap, 10, 1);
 503:	8b 45 f4             	mov    -0xc(%ebp),%eax
 506:	8b 00                	mov    (%eax),%eax
 508:	c7 44 24 0c 01 00 00 	movl   $0x1,0xc(%esp)
 50f:	00 
 510:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
 517:	00 
 518:	89 44 24 04          	mov    %eax,0x4(%esp)
 51c:	8b 45 08             	mov    0x8(%ebp),%eax
 51f:	89 04 24             	mov    %eax,(%esp)
 522:	e8 ad fe ff ff       	call   3d4 <printint>
        ap++;
 527:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 52b:	e9 ed 00 00 00       	jmp    61d <printf+0x195>
      } else if(c == 'x' || c == 'p'){
 530:	83 7d e8 78          	cmpl   $0x78,-0x18(%ebp)
 534:	74 06                	je     53c <printf+0xb4>
 536:	83 7d e8 70          	cmpl   $0x70,-0x18(%ebp)
 53a:	75 2d                	jne    569 <printf+0xe1>
        printint(fd, *ap, 16, 0);
 53c:	8b 45 f4             	mov    -0xc(%ebp),%eax
 53f:	8b 00                	mov    (%eax),%eax
 541:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
 548:	00 
 549:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
 550:	00 
 551:	89 44 24 04          	mov    %eax,0x4(%esp)
 555:	8b 45 08             	mov    0x8(%ebp),%eax
 558:	89 04 24             	mov    %eax,(%esp)
 55b:	e8 74 fe ff ff       	call   3d4 <printint>
        ap++;
 560:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
      }
    } else if(state == '%'){
      if(c == 'd'){
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 564:	e9 b4 00 00 00       	jmp    61d <printf+0x195>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 569:	83 7d e8 73          	cmpl   $0x73,-0x18(%ebp)
 56d:	75 46                	jne    5b5 <printf+0x12d>
        s = (char*)*ap;
 56f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 572:	8b 00                	mov    (%eax),%eax
 574:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        ap++;
 577:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
        if(s == 0)
 57b:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 57f:	75 27                	jne    5a8 <printf+0x120>
          s = "(null)";
 581:	c7 45 e4 f0 08 00 00 	movl   $0x8f0,-0x1c(%ebp)
        while(*s != 0){
 588:	eb 1f                	jmp    5a9 <printf+0x121>
          putc(fd, *s);
 58a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 58d:	0f b6 00             	movzbl (%eax),%eax
 590:	0f be c0             	movsbl %al,%eax
 593:	89 44 24 04          	mov    %eax,0x4(%esp)
 597:	8b 45 08             	mov    0x8(%ebp),%eax
 59a:	89 04 24             	mov    %eax,(%esp)
 59d:	e8 0a fe ff ff       	call   3ac <putc>
          s++;
 5a2:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
 5a6:	eb 01                	jmp    5a9 <printf+0x121>
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 5a8:	90                   	nop
 5a9:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 5ac:	0f b6 00             	movzbl (%eax),%eax
 5af:	84 c0                	test   %al,%al
 5b1:	75 d7                	jne    58a <printf+0x102>
 5b3:	eb 68                	jmp    61d <printf+0x195>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 5b5:	83 7d e8 63          	cmpl   $0x63,-0x18(%ebp)
 5b9:	75 1d                	jne    5d8 <printf+0x150>
        putc(fd, *ap);
 5bb:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5be:	8b 00                	mov    (%eax),%eax
 5c0:	0f be c0             	movsbl %al,%eax
 5c3:	89 44 24 04          	mov    %eax,0x4(%esp)
 5c7:	8b 45 08             	mov    0x8(%ebp),%eax
 5ca:	89 04 24             	mov    %eax,(%esp)
 5cd:	e8 da fd ff ff       	call   3ac <putc>
        ap++;
 5d2:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 5d6:	eb 45                	jmp    61d <printf+0x195>
      } else if(c == '%'){
 5d8:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 5dc:	75 17                	jne    5f5 <printf+0x16d>
        putc(fd, c);
 5de:	8b 45 e8             	mov    -0x18(%ebp),%eax
 5e1:	0f be c0             	movsbl %al,%eax
 5e4:	89 44 24 04          	mov    %eax,0x4(%esp)
 5e8:	8b 45 08             	mov    0x8(%ebp),%eax
 5eb:	89 04 24             	mov    %eax,(%esp)
 5ee:	e8 b9 fd ff ff       	call   3ac <putc>
 5f3:	eb 28                	jmp    61d <printf+0x195>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 5f5:	c7 44 24 04 25 00 00 	movl   $0x25,0x4(%esp)
 5fc:	00 
 5fd:	8b 45 08             	mov    0x8(%ebp),%eax
 600:	89 04 24             	mov    %eax,(%esp)
 603:	e8 a4 fd ff ff       	call   3ac <putc>
        putc(fd, c);
 608:	8b 45 e8             	mov    -0x18(%ebp),%eax
 60b:	0f be c0             	movsbl %al,%eax
 60e:	89 44 24 04          	mov    %eax,0x4(%esp)
 612:	8b 45 08             	mov    0x8(%ebp),%eax
 615:	89 04 24             	mov    %eax,(%esp)
 618:	e8 8f fd ff ff       	call   3ac <putc>
      }
      state = 0;
 61d:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 624:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
 628:	8b 55 0c             	mov    0xc(%ebp),%edx
 62b:	8b 45 ec             	mov    -0x14(%ebp),%eax
 62e:	8d 04 02             	lea    (%edx,%eax,1),%eax
 631:	0f b6 00             	movzbl (%eax),%eax
 634:	84 c0                	test   %al,%al
 636:	0f 85 6e fe ff ff    	jne    4aa <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 63c:	c9                   	leave  
 63d:	c3                   	ret    
 63e:	90                   	nop
 63f:	90                   	nop

00000640 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 640:	55                   	push   %ebp
 641:	89 e5                	mov    %esp,%ebp
 643:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 646:	8b 45 08             	mov    0x8(%ebp),%eax
 649:	83 e8 08             	sub    $0x8,%eax
 64c:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 64f:	a1 14 09 00 00       	mov    0x914,%eax
 654:	89 45 fc             	mov    %eax,-0x4(%ebp)
 657:	eb 24                	jmp    67d <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 659:	8b 45 fc             	mov    -0x4(%ebp),%eax
 65c:	8b 00                	mov    (%eax),%eax
 65e:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 661:	77 12                	ja     675 <free+0x35>
 663:	8b 45 f8             	mov    -0x8(%ebp),%eax
 666:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 669:	77 24                	ja     68f <free+0x4f>
 66b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 66e:	8b 00                	mov    (%eax),%eax
 670:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 673:	77 1a                	ja     68f <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 675:	8b 45 fc             	mov    -0x4(%ebp),%eax
 678:	8b 00                	mov    (%eax),%eax
 67a:	89 45 fc             	mov    %eax,-0x4(%ebp)
 67d:	8b 45 f8             	mov    -0x8(%ebp),%eax
 680:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 683:	76 d4                	jbe    659 <free+0x19>
 685:	8b 45 fc             	mov    -0x4(%ebp),%eax
 688:	8b 00                	mov    (%eax),%eax
 68a:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 68d:	76 ca                	jbe    659 <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 68f:	8b 45 f8             	mov    -0x8(%ebp),%eax
 692:	8b 40 04             	mov    0x4(%eax),%eax
 695:	c1 e0 03             	shl    $0x3,%eax
 698:	89 c2                	mov    %eax,%edx
 69a:	03 55 f8             	add    -0x8(%ebp),%edx
 69d:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6a0:	8b 00                	mov    (%eax),%eax
 6a2:	39 c2                	cmp    %eax,%edx
 6a4:	75 24                	jne    6ca <free+0x8a>
    bp->s.size += p->s.ptr->s.size;
 6a6:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6a9:	8b 50 04             	mov    0x4(%eax),%edx
 6ac:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6af:	8b 00                	mov    (%eax),%eax
 6b1:	8b 40 04             	mov    0x4(%eax),%eax
 6b4:	01 c2                	add    %eax,%edx
 6b6:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6b9:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 6bc:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6bf:	8b 00                	mov    (%eax),%eax
 6c1:	8b 10                	mov    (%eax),%edx
 6c3:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6c6:	89 10                	mov    %edx,(%eax)
 6c8:	eb 0a                	jmp    6d4 <free+0x94>
  } else
    bp->s.ptr = p->s.ptr;
 6ca:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6cd:	8b 10                	mov    (%eax),%edx
 6cf:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6d2:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 6d4:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6d7:	8b 40 04             	mov    0x4(%eax),%eax
 6da:	c1 e0 03             	shl    $0x3,%eax
 6dd:	03 45 fc             	add    -0x4(%ebp),%eax
 6e0:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 6e3:	75 20                	jne    705 <free+0xc5>
    p->s.size += bp->s.size;
 6e5:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6e8:	8b 50 04             	mov    0x4(%eax),%edx
 6eb:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6ee:	8b 40 04             	mov    0x4(%eax),%eax
 6f1:	01 c2                	add    %eax,%edx
 6f3:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6f6:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 6f9:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6fc:	8b 10                	mov    (%eax),%edx
 6fe:	8b 45 fc             	mov    -0x4(%ebp),%eax
 701:	89 10                	mov    %edx,(%eax)
 703:	eb 08                	jmp    70d <free+0xcd>
  } else
    p->s.ptr = bp;
 705:	8b 45 fc             	mov    -0x4(%ebp),%eax
 708:	8b 55 f8             	mov    -0x8(%ebp),%edx
 70b:	89 10                	mov    %edx,(%eax)
  freep = p;
 70d:	8b 45 fc             	mov    -0x4(%ebp),%eax
 710:	a3 14 09 00 00       	mov    %eax,0x914
}
 715:	c9                   	leave  
 716:	c3                   	ret    

00000717 <morecore>:

static Header*
morecore(uint nu)
{
 717:	55                   	push   %ebp
 718:	89 e5                	mov    %esp,%ebp
 71a:	83 ec 28             	sub    $0x28,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 71d:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 724:	77 07                	ja     72d <morecore+0x16>
    nu = 4096;
 726:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 72d:	8b 45 08             	mov    0x8(%ebp),%eax
 730:	c1 e0 03             	shl    $0x3,%eax
 733:	89 04 24             	mov    %eax,(%esp)
 736:	e8 39 fc ff ff       	call   374 <sbrk>
 73b:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(p == (char*)-1)
 73e:	83 7d f0 ff          	cmpl   $0xffffffff,-0x10(%ebp)
 742:	75 07                	jne    74b <morecore+0x34>
    return 0;
 744:	b8 00 00 00 00       	mov    $0x0,%eax
 749:	eb 22                	jmp    76d <morecore+0x56>
  hp = (Header*)p;
 74b:	8b 45 f0             	mov    -0x10(%ebp),%eax
 74e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  hp->s.size = nu;
 751:	8b 45 f4             	mov    -0xc(%ebp),%eax
 754:	8b 55 08             	mov    0x8(%ebp),%edx
 757:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 75a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 75d:	83 c0 08             	add    $0x8,%eax
 760:	89 04 24             	mov    %eax,(%esp)
 763:	e8 d8 fe ff ff       	call   640 <free>
  return freep;
 768:	a1 14 09 00 00       	mov    0x914,%eax
}
 76d:	c9                   	leave  
 76e:	c3                   	ret    

0000076f <malloc>:

void*
malloc(uint nbytes)
{
 76f:	55                   	push   %ebp
 770:	89 e5                	mov    %esp,%ebp
 772:	83 ec 28             	sub    $0x28,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 775:	8b 45 08             	mov    0x8(%ebp),%eax
 778:	83 c0 07             	add    $0x7,%eax
 77b:	c1 e8 03             	shr    $0x3,%eax
 77e:	83 c0 01             	add    $0x1,%eax
 781:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((prevp = freep) == 0){
 784:	a1 14 09 00 00       	mov    0x914,%eax
 789:	89 45 f0             	mov    %eax,-0x10(%ebp)
 78c:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 790:	75 23                	jne    7b5 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 792:	c7 45 f0 0c 09 00 00 	movl   $0x90c,-0x10(%ebp)
 799:	8b 45 f0             	mov    -0x10(%ebp),%eax
 79c:	a3 14 09 00 00       	mov    %eax,0x914
 7a1:	a1 14 09 00 00       	mov    0x914,%eax
 7a6:	a3 0c 09 00 00       	mov    %eax,0x90c
    base.s.size = 0;
 7ab:	c7 05 10 09 00 00 00 	movl   $0x0,0x910
 7b2:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 7b5:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7b8:	8b 00                	mov    (%eax),%eax
 7ba:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(p->s.size >= nunits){
 7bd:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7c0:	8b 40 04             	mov    0x4(%eax),%eax
 7c3:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 7c6:	72 4d                	jb     815 <malloc+0xa6>
      if(p->s.size == nunits)
 7c8:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7cb:	8b 40 04             	mov    0x4(%eax),%eax
 7ce:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 7d1:	75 0c                	jne    7df <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 7d3:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7d6:	8b 10                	mov    (%eax),%edx
 7d8:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7db:	89 10                	mov    %edx,(%eax)
 7dd:	eb 26                	jmp    805 <malloc+0x96>
      else {
        p->s.size -= nunits;
 7df:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7e2:	8b 40 04             	mov    0x4(%eax),%eax
 7e5:	89 c2                	mov    %eax,%edx
 7e7:	2b 55 f4             	sub    -0xc(%ebp),%edx
 7ea:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7ed:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 7f0:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7f3:	8b 40 04             	mov    0x4(%eax),%eax
 7f6:	c1 e0 03             	shl    $0x3,%eax
 7f9:	01 45 ec             	add    %eax,-0x14(%ebp)
        p->s.size = nunits;
 7fc:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7ff:	8b 55 f4             	mov    -0xc(%ebp),%edx
 802:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 805:	8b 45 f0             	mov    -0x10(%ebp),%eax
 808:	a3 14 09 00 00       	mov    %eax,0x914
      return (void*)(p + 1);
 80d:	8b 45 ec             	mov    -0x14(%ebp),%eax
 810:	83 c0 08             	add    $0x8,%eax
 813:	eb 38                	jmp    84d <malloc+0xde>
    }
    if(p == freep)
 815:	a1 14 09 00 00       	mov    0x914,%eax
 81a:	39 45 ec             	cmp    %eax,-0x14(%ebp)
 81d:	75 1b                	jne    83a <malloc+0xcb>
      if((p = morecore(nunits)) == 0)
 81f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 822:	89 04 24             	mov    %eax,(%esp)
 825:	e8 ed fe ff ff       	call   717 <morecore>
 82a:	89 45 ec             	mov    %eax,-0x14(%ebp)
 82d:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 831:	75 07                	jne    83a <malloc+0xcb>
        return 0;
 833:	b8 00 00 00 00       	mov    $0x0,%eax
 838:	eb 13                	jmp    84d <malloc+0xde>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 83a:	8b 45 ec             	mov    -0x14(%ebp),%eax
 83d:	89 45 f0             	mov    %eax,-0x10(%ebp)
 840:	8b 45 ec             	mov    -0x14(%ebp),%eax
 843:	8b 00                	mov    (%eax),%eax
 845:	89 45 ec             	mov    %eax,-0x14(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 848:	e9 70 ff ff ff       	jmp    7bd <malloc+0x4e>
}
 84d:	c9                   	leave  
 84e:	c3                   	ret    
