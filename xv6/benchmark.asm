
_benchmark:     file format elf32-i386


Disassembly of section .text:

00000000 <workload>:
#include "fcntl.h"

#define DEBUG 0

void workload(int i)
{
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	81 ec 88 20 00 00    	sub    $0x2088,%esp
    if(i == 0)
   9:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
   d:	75 57                	jne    66 <workload+0x66>
#if DEBUG
        printf(1, "short io pid = %d\n", getpid());
#endif
        char buf[8192];
        int j;
        for(j = 0; j < 100; j++)
   f:	c7 45 90 00 00 00 00 	movl   $0x0,-0x70(%ebp)
  16:	eb 43                	jmp    5b <workload+0x5b>
        {
            int fd = open("benchmark", O_RDONLY);
  18:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  1f:	00 
  20:	c7 04 24 8c 0b 00 00 	movl   $0xb8c,(%esp)
  27:	e8 3c 06 00 00       	call   668 <open>
  2c:	89 45 94             	mov    %eax,-0x6c(%ebp)
            read(fd, buf, 2000);
  2f:	c7 44 24 08 d0 07 00 	movl   $0x7d0,0x8(%esp)
  36:	00 
  37:	8d 85 90 df ff ff    	lea    -0x2070(%ebp),%eax
  3d:	89 44 24 04          	mov    %eax,0x4(%esp)
  41:	8b 45 94             	mov    -0x6c(%ebp),%eax
  44:	89 04 24             	mov    %eax,(%esp)
  47:	e8 f4 05 00 00       	call   640 <read>
            close(fd);
  4c:	8b 45 94             	mov    -0x6c(%ebp),%eax
  4f:	89 04 24             	mov    %eax,(%esp)
  52:	e8 f9 05 00 00       	call   650 <close>
#if DEBUG
        printf(1, "short io pid = %d\n", getpid());
#endif
        char buf[8192];
        int j;
        for(j = 0; j < 100; j++)
  57:	83 45 90 01          	addl   $0x1,-0x70(%ebp)
  5b:	83 7d 90 63          	cmpl   $0x63,-0x70(%ebp)
  5f:	7e b7                	jle    18 <workload+0x18>
  61:	e9 eb 01 00 00       	jmp    251 <workload+0x251>
            int fd = open("benchmark", O_RDONLY);
            read(fd, buf, 2000);
            close(fd);
        }
    }
    else if(i == 1)
  66:	83 7d 08 01          	cmpl   $0x1,0x8(%ebp)
  6a:	75 43                	jne    af <workload+0xaf>
#if DEBUG
        printf(1, "add pid = %d\n", getpid());
#endif
        double r;
        int j;
        for(j = 0; j < 10000; j++)
  6c:	c7 45 a0 00 00 00 00 	movl   $0x0,-0x60(%ebp)
  73:	eb 2c                	jmp    a1 <workload+0xa1>
        {
            int i;
            r = 0.0;
  75:	d9 ee                	fldz   
  77:	dd 5d 98             	fstpl  -0x68(%ebp)
            for(i = 0; i < 100; i++)
  7a:	c7 45 a4 00 00 00 00 	movl   $0x0,-0x5c(%ebp)
  81:	eb 14                	jmp    97 <workload+0x97>
            {
                r = r + r / i;
  83:	db 45 a4             	fildl  -0x5c(%ebp)
  86:	dd 45 98             	fldl   -0x68(%ebp)
  89:	de f1                	fdivp  %st,%st(1)
  8b:	dd 45 98             	fldl   -0x68(%ebp)
  8e:	de c1                	faddp  %st,%st(1)
  90:	dd 5d 98             	fstpl  -0x68(%ebp)
        int j;
        for(j = 0; j < 10000; j++)
        {
            int i;
            r = 0.0;
            for(i = 0; i < 100; i++)
  93:	83 45 a4 01          	addl   $0x1,-0x5c(%ebp)
  97:	83 7d a4 63          	cmpl   $0x63,-0x5c(%ebp)
  9b:	7e e6                	jle    83 <workload+0x83>
#if DEBUG
        printf(1, "add pid = %d\n", getpid());
#endif
        double r;
        int j;
        for(j = 0; j < 10000; j++)
  9d:	83 45 a0 01          	addl   $0x1,-0x60(%ebp)
  a1:	81 7d a0 0f 27 00 00 	cmpl   $0x270f,-0x60(%ebp)
  a8:	7e cb                	jle    75 <workload+0x75>
  aa:	e9 a2 01 00 00       	jmp    251 <workload+0x251>
            {
                r = r + r / i;
            }
        }
    }
    else if(i == 2)
  af:	83 7d 08 02          	cmpl   $0x2,0x8(%ebp)
  b3:	75 41                	jne    f6 <workload+0xf6>
#if DEBUG
        printf(1, "mult pid = %d\n", getpid());
#endif
        double r;
        int j;
        for(j = 0; j < 20000; j++)
  b5:	c7 45 b0 00 00 00 00 	movl   $0x0,-0x50(%ebp)
  bc:	eb 2a                	jmp    e8 <workload+0xe8>
        {
            int i;
            r = 1.0;
  be:	d9 e8                	fld1   
  c0:	dd 5d a8             	fstpl  -0x58(%ebp)
            for(i = 0; i < 100; i++)
  c3:	c7 45 b4 00 00 00 00 	movl   $0x0,-0x4c(%ebp)
  ca:	eb 12                	jmp    de <workload+0xde>
            {
                r = r + r * i;
  cc:	db 45 b4             	fildl  -0x4c(%ebp)
  cf:	dc 4d a8             	fmull  -0x58(%ebp)
  d2:	dd 45 a8             	fldl   -0x58(%ebp)
  d5:	de c1                	faddp  %st,%st(1)
  d7:	dd 5d a8             	fstpl  -0x58(%ebp)
        int j;
        for(j = 0; j < 20000; j++)
        {
            int i;
            r = 1.0;
            for(i = 0; i < 100; i++)
  da:	83 45 b4 01          	addl   $0x1,-0x4c(%ebp)
  de:	83 7d b4 63          	cmpl   $0x63,-0x4c(%ebp)
  e2:	7e e8                	jle    cc <workload+0xcc>
#if DEBUG
        printf(1, "mult pid = %d\n", getpid());
#endif
        double r;
        int j;
        for(j = 0; j < 20000; j++)
  e4:	83 45 b0 01          	addl   $0x1,-0x50(%ebp)
  e8:	81 7d b0 1f 4e 00 00 	cmpl   $0x4e1f,-0x50(%ebp)
  ef:	7e cd                	jle    be <workload+0xbe>
  f1:	e9 5b 01 00 00       	jmp    251 <workload+0x251>
            {
                r = r + r * i;
            }
        }
    }
    else if(i == 3)
  f6:	83 7d 08 03          	cmpl   $0x3,0x8(%ebp)
  fa:	0f 85 83 00 00 00    	jne    183 <workload+0x183>
        printf(1, "add and io pid = %d\n", getpid());
#endif
        double r;
        int j;
        char buf[8192];
        for(j = 0; j < 1000; j++)
 100:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%ebp)
 107:	eb 6c                	jmp    175 <workload+0x175>
        {
            int fd = open("benchmark", O_RDONLY);
 109:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 110:	00 
 111:	c7 04 24 8c 0b 00 00 	movl   $0xb8c,(%esp)
 118:	e8 4b 05 00 00       	call   668 <open>
 11d:	89 45 c8             	mov    %eax,-0x38(%ebp)
            int i;
            r = 1.0;
 120:	d9 e8                	fld1   
 122:	dd 5d b8             	fstpl  -0x48(%ebp)
            for(i = 0; i < 1000; i++)
 125:	c7 45 cc 00 00 00 00 	movl   $0x0,-0x34(%ebp)
 12c:	eb 12                	jmp    140 <workload+0x140>
            {
                r = r + r * i;
 12e:	db 45 cc             	fildl  -0x34(%ebp)
 131:	dc 4d b8             	fmull  -0x48(%ebp)
 134:	dd 45 b8             	fldl   -0x48(%ebp)
 137:	de c1                	faddp  %st,%st(1)
 139:	dd 5d b8             	fstpl  -0x48(%ebp)
        for(j = 0; j < 1000; j++)
        {
            int fd = open("benchmark", O_RDONLY);
            int i;
            r = 1.0;
            for(i = 0; i < 1000; i++)
 13c:	83 45 cc 01          	addl   $0x1,-0x34(%ebp)
 140:	81 7d cc e7 03 00 00 	cmpl   $0x3e7,-0x34(%ebp)
 147:	7e e5                	jle    12e <workload+0x12e>
            {
                r = r + r * i;
            }
            read(fd, buf, 2000);
 149:	c7 44 24 08 d0 07 00 	movl   $0x7d0,0x8(%esp)
 150:	00 
 151:	8d 85 90 df ff ff    	lea    -0x2070(%ebp),%eax
 157:	89 44 24 04          	mov    %eax,0x4(%esp)
 15b:	8b 45 c8             	mov    -0x38(%ebp),%eax
 15e:	89 04 24             	mov    %eax,(%esp)
 161:	e8 da 04 00 00       	call   640 <read>
            close(fd);
 166:	8b 45 c8             	mov    -0x38(%ebp),%eax
 169:	89 04 24             	mov    %eax,(%esp)
 16c:	e8 df 04 00 00       	call   650 <close>
        printf(1, "add and io pid = %d\n", getpid());
#endif
        double r;
        int j;
        char buf[8192];
        for(j = 0; j < 1000; j++)
 171:	83 45 c4 01          	addl   $0x1,-0x3c(%ebp)
 175:	81 7d c4 e7 03 00 00 	cmpl   $0x3e7,-0x3c(%ebp)
 17c:	7e 8b                	jle    109 <workload+0x109>
 17e:	e9 ce 00 00 00       	jmp    251 <workload+0x251>
            }
            read(fd, buf, 2000);
            close(fd);
        }
    }
    else if(i == 4)
 183:	83 7d 08 04          	cmpl   $0x4,0x8(%ebp)
 187:	75 7d                	jne    206 <workload+0x206>
        printf(1, "add and io pid = %d\n", getpid());
#endif
        double r;
        int j;
        char buf[8192];
        for(j = 0; j < 1000; j++)
 189:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
 190:	eb 69                	jmp    1fb <workload+0x1fb>
        {
            int fd = open("benchmark", O_RDONLY);
 192:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 199:	00 
 19a:	c7 04 24 8c 0b 00 00 	movl   $0xb8c,(%esp)
 1a1:	e8 c2 04 00 00       	call   668 <open>
 1a6:	89 45 e0             	mov    %eax,-0x20(%ebp)
            int i;
            r = 1.0;
 1a9:	d9 e8                	fld1   
 1ab:	dd 5d d0             	fstpl  -0x30(%ebp)
            for(i = 0; i < 50; i++)
 1ae:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
 1b5:	eb 12                	jmp    1c9 <workload+0x1c9>
            {
                r = r + r * i;
 1b7:	db 45 e4             	fildl  -0x1c(%ebp)
 1ba:	dc 4d d0             	fmull  -0x30(%ebp)
 1bd:	dd 45 d0             	fldl   -0x30(%ebp)
 1c0:	de c1                	faddp  %st,%st(1)
 1c2:	dd 5d d0             	fstpl  -0x30(%ebp)
        for(j = 0; j < 1000; j++)
        {
            int fd = open("benchmark", O_RDONLY);
            int i;
            r = 1.0;
            for(i = 0; i < 50; i++)
 1c5:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
 1c9:	83 7d e4 31          	cmpl   $0x31,-0x1c(%ebp)
 1cd:	7e e8                	jle    1b7 <workload+0x1b7>
            {
                r = r + r * i;
            }
            read(fd, buf, 2000);
 1cf:	c7 44 24 08 d0 07 00 	movl   $0x7d0,0x8(%esp)
 1d6:	00 
 1d7:	8d 85 90 df ff ff    	lea    -0x2070(%ebp),%eax
 1dd:	89 44 24 04          	mov    %eax,0x4(%esp)
 1e1:	8b 45 e0             	mov    -0x20(%ebp),%eax
 1e4:	89 04 24             	mov    %eax,(%esp)
 1e7:	e8 54 04 00 00       	call   640 <read>
            close(fd);
 1ec:	8b 45 e0             	mov    -0x20(%ebp),%eax
 1ef:	89 04 24             	mov    %eax,(%esp)
 1f2:	e8 59 04 00 00       	call   650 <close>
        printf(1, "add and io pid = %d\n", getpid());
#endif
        double r;
        int j;
        char buf[8192];
        for(j = 0; j < 1000; j++)
 1f7:	83 45 dc 01          	addl   $0x1,-0x24(%ebp)
 1fb:	81 7d dc e7 03 00 00 	cmpl   $0x3e7,-0x24(%ebp)
 202:	7e 8e                	jle    192 <workload+0x192>
 204:	eb 4b                	jmp    251 <workload+0x251>
            }
            read(fd, buf, 2000);
            close(fd);
        }
    }
    else if(i == 5)
 206:	83 7d 08 05          	cmpl   $0x5,0x8(%ebp)
 20a:	75 45                	jne    251 <workload+0x251>
#if DEBUG
        printf(1, "sleeper pid = %d\n", getpid());
#endif
        int i, j;
        double r;
        for(i = 0; i < 100; i++) {
 20c:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
 213:	eb 36                	jmp    24b <workload+0x24b>
            r = 1.0;
 215:	d9 e8                	fld1   
 217:	dd 5d f0             	fstpl  -0x10(%ebp)
            for(j = 0; j < 100; j++)
 21a:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 221:	eb 12                	jmp    235 <workload+0x235>
            {
                r = r + r * j;
 223:	db 45 ec             	fildl  -0x14(%ebp)
 226:	dc 4d f0             	fmull  -0x10(%ebp)
 229:	dd 45 f0             	fldl   -0x10(%ebp)
 22c:	de c1                	faddp  %st,%st(1)
 22e:	dd 5d f0             	fstpl  -0x10(%ebp)
#endif
        int i, j;
        double r;
        for(i = 0; i < 100; i++) {
            r = 1.0;
            for(j = 0; j < 100; j++)
 231:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
 235:	83 7d ec 63          	cmpl   $0x63,-0x14(%ebp)
 239:	7e e8                	jle    223 <workload+0x223>
            {
                r = r + r * j;
            }
            sleep(10);
 23b:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
 242:	e8 71 04 00 00       	call   6b8 <sleep>
#if DEBUG
        printf(1, "sleeper pid = %d\n", getpid());
#endif
        int i, j;
        double r;
        for(i = 0; i < 100; i++) {
 247:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
 24b:	83 7d e8 63          	cmpl   $0x63,-0x18(%ebp)
 24f:	7e c4                	jle    215 <workload+0x215>
                r = r + r * j;
            }
            sleep(10);
        }
    }
}
 251:	c9                   	leave  
 252:	c3                   	ret    

00000253 <main>:

int main(int argc, char *argv[])
{
 253:	55                   	push   %ebp
 254:	89 e5                	mov    %esp,%ebp
 256:	83 e4 f0             	and    $0xfffffff0,%esp
 259:	53                   	push   %ebx
 25a:	83 ec 5c             	sub    $0x5c,%esp
    printf(1, "sched,rep,ticks,procexits,procticks,procfirstrun\n");
 25d:	c7 44 24 04 98 0b 00 	movl   $0xb98,0x4(%esp)
 264:	00 
 265:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 26c:	e8 53 05 00 00       	call   7c4 <printf>
    int sched_type;
    for(sched_type = SCHED_ROUND_ROBIN; sched_type <= SCHED_MLFQ; sched_type++)
 271:	c7 44 24 3c 00 00 00 	movl   $0x0,0x3c(%esp)
 278:	00 
 279:	e9 34 01 00 00       	jmp    3b2 <main+0x15f>
    {
        switch_scheduler(sched_type);
 27e:	8b 44 24 3c          	mov    0x3c(%esp),%eax
 282:	89 04 24             	mov    %eax,(%esp)
 285:	e8 4e 04 00 00       	call   6d8 <switch_scheduler>
        int pid;
        int rep;
        for(rep = 1; rep <= 30; rep++)
 28a:	c7 44 24 44 01 00 00 	movl   $0x1,0x44(%esp)
 291:	00 
 292:	e9 0b 01 00 00       	jmp    3a2 <main+0x14f>
        {
            start_capture();
 297:	e8 2c 04 00 00       	call   6c8 <start_capture>

            int forki;
            for(forki = 0; forki < 10; forki++) {
 29c:	c7 44 24 48 00 00 00 	movl   $0x0,0x48(%esp)
 2a3:	00 
 2a4:	eb 55                	jmp    2fb <main+0xa8>
                pid = fork();
 2a6:	e8 75 03 00 00       	call   620 <fork>
 2ab:	89 44 24 40          	mov    %eax,0x40(%esp)
                if(pid == 0)
 2af:	83 7c 24 40 00       	cmpl   $0x0,0x40(%esp)
 2b4:	75 34                	jne    2ea <main+0x97>
                {
                    workload((rep + forki) % 6);
 2b6:	8b 44 24 48          	mov    0x48(%esp),%eax
 2ba:	8b 54 24 44          	mov    0x44(%esp),%edx
 2be:	8d 0c 02             	lea    (%edx,%eax,1),%ecx
 2c1:	ba ab aa aa 2a       	mov    $0x2aaaaaab,%edx
 2c6:	89 c8                	mov    %ecx,%eax
 2c8:	f7 ea                	imul   %edx
 2ca:	89 c8                	mov    %ecx,%eax
 2cc:	c1 f8 1f             	sar    $0x1f,%eax
 2cf:	29 c2                	sub    %eax,%edx
 2d1:	89 d0                	mov    %edx,%eax
 2d3:	01 c0                	add    %eax,%eax
 2d5:	01 d0                	add    %edx,%eax
 2d7:	01 c0                	add    %eax,%eax
 2d9:	89 ca                	mov    %ecx,%edx
 2db:	29 c2                	sub    %eax,%edx
 2dd:	89 14 24             	mov    %edx,(%esp)
 2e0:	e8 1b fd ff ff       	call   0 <workload>
                    exit();
 2e5:	e8 3e 03 00 00       	call   628 <exit>
                }
                sleep(5);
 2ea:	c7 04 24 05 00 00 00 	movl   $0x5,(%esp)
 2f1:	e8 c2 03 00 00       	call   6b8 <sleep>
        for(rep = 1; rep <= 30; rep++)
        {
            start_capture();

            int forki;
            for(forki = 0; forki < 10; forki++) {
 2f6:	83 44 24 48 01       	addl   $0x1,0x48(%esp)
 2fb:	83 7c 24 48 09       	cmpl   $0x9,0x48(%esp)
 300:	7e a4                	jle    2a6 <main+0x53>
                    exit();
                }
                sleep(5);
            }

            while(wait() > 0);
 302:	e8 29 03 00 00       	call   630 <wait>
 307:	85 c0                	test   %eax,%eax
 309:	7f f7                	jg     302 <main+0xaf>

            struct capture_stats cs;
            stop_capture(&cs);
 30b:	8d 44 24 2c          	lea    0x2c(%esp),%eax
 30f:	89 04 24             	mov    %eax,(%esp)
 312:	e8 b9 03 00 00       	call   6d0 <stop_capture>
            char *sched_name;
            if(sched_type == SCHED_ROUND_ROBIN)
 317:	83 7c 24 3c 00       	cmpl   $0x0,0x3c(%esp)
 31c:	75 0a                	jne    328 <main+0xd5>
                sched_name = "rr";
 31e:	c7 44 24 4c ca 0b 00 	movl   $0xbca,0x4c(%esp)
 325:	00 
 326:	eb 31                	jmp    359 <main+0x106>
            else if(sched_type == SCHED_FIFO)
 328:	83 7c 24 3c 01       	cmpl   $0x1,0x3c(%esp)
 32d:	75 0a                	jne    339 <main+0xe6>
                sched_name = "fifo";
 32f:	c7 44 24 4c cd 0b 00 	movl   $0xbcd,0x4c(%esp)
 336:	00 
 337:	eb 20                	jmp    359 <main+0x106>
            else if(sched_type == SCHED_LIFO)
 339:	83 7c 24 3c 02       	cmpl   $0x2,0x3c(%esp)
 33e:	75 0a                	jne    34a <main+0xf7>
                sched_name = "lifo";
 340:	c7 44 24 4c d2 0b 00 	movl   $0xbd2,0x4c(%esp)
 347:	00 
 348:	eb 0f                	jmp    359 <main+0x106>
            else if(sched_type == SCHED_MLFQ)
 34a:	83 7c 24 3c 03       	cmpl   $0x3,0x3c(%esp)
 34f:	75 08                	jne    359 <main+0x106>
                sched_name = "mlfq";
 351:	c7 44 24 4c d7 0b 00 	movl   $0xbd7,0x4c(%esp)
 358:	00 
            printf(1, "%s,%d,%d,%d,%d,%d\n",
 359:	8b 5c 24 38          	mov    0x38(%esp),%ebx
 35d:	8b 4c 24 34          	mov    0x34(%esp),%ecx
 361:	8b 54 24 30          	mov    0x30(%esp),%edx
 365:	8b 44 24 2c          	mov    0x2c(%esp),%eax
 369:	89 5c 24 1c          	mov    %ebx,0x1c(%esp)
 36d:	89 4c 24 18          	mov    %ecx,0x18(%esp)
 371:	89 54 24 14          	mov    %edx,0x14(%esp)
 375:	89 44 24 10          	mov    %eax,0x10(%esp)
 379:	8b 44 24 44          	mov    0x44(%esp),%eax
 37d:	89 44 24 0c          	mov    %eax,0xc(%esp)
 381:	8b 44 24 4c          	mov    0x4c(%esp),%eax
 385:	89 44 24 08          	mov    %eax,0x8(%esp)
 389:	c7 44 24 04 dc 0b 00 	movl   $0xbdc,0x4(%esp)
 390:	00 
 391:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 398:	e8 27 04 00 00       	call   7c4 <printf>
    for(sched_type = SCHED_ROUND_ROBIN; sched_type <= SCHED_MLFQ; sched_type++)
    {
        switch_scheduler(sched_type);
        int pid;
        int rep;
        for(rep = 1; rep <= 30; rep++)
 39d:	83 44 24 44 01       	addl   $0x1,0x44(%esp)
 3a2:	83 7c 24 44 1e       	cmpl   $0x1e,0x44(%esp)
 3a7:	0f 8e ea fe ff ff    	jle    297 <main+0x44>

int main(int argc, char *argv[])
{
    printf(1, "sched,rep,ticks,procexits,procticks,procfirstrun\n");
    int sched_type;
    for(sched_type = SCHED_ROUND_ROBIN; sched_type <= SCHED_MLFQ; sched_type++)
 3ad:	83 44 24 3c 01       	addl   $0x1,0x3c(%esp)
 3b2:	83 7c 24 3c 03       	cmpl   $0x3,0x3c(%esp)
 3b7:	0f 8e c1 fe ff ff    	jle    27e <main+0x2b>
                    sched_name, rep, cs.tick_count,
                    cs.proc_exit_count, cs.proc_tick_count,
                    cs.proc_firstrun_count);
        }
    }
    exit();
 3bd:	e8 66 02 00 00       	call   628 <exit>
 3c2:	90                   	nop
 3c3:	90                   	nop

000003c4 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
 3c4:	55                   	push   %ebp
 3c5:	89 e5                	mov    %esp,%ebp
 3c7:	57                   	push   %edi
 3c8:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
 3c9:	8b 4d 08             	mov    0x8(%ebp),%ecx
 3cc:	8b 55 10             	mov    0x10(%ebp),%edx
 3cf:	8b 45 0c             	mov    0xc(%ebp),%eax
 3d2:	89 cb                	mov    %ecx,%ebx
 3d4:	89 df                	mov    %ebx,%edi
 3d6:	89 d1                	mov    %edx,%ecx
 3d8:	fc                   	cld    
 3d9:	f3 aa                	rep stos %al,%es:(%edi)
 3db:	89 ca                	mov    %ecx,%edx
 3dd:	89 fb                	mov    %edi,%ebx
 3df:	89 5d 08             	mov    %ebx,0x8(%ebp)
 3e2:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
 3e5:	5b                   	pop    %ebx
 3e6:	5f                   	pop    %edi
 3e7:	5d                   	pop    %ebp
 3e8:	c3                   	ret    

000003e9 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
 3e9:	55                   	push   %ebp
 3ea:	89 e5                	mov    %esp,%ebp
 3ec:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 3ef:	8b 45 08             	mov    0x8(%ebp),%eax
 3f2:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 3f5:	8b 45 0c             	mov    0xc(%ebp),%eax
 3f8:	0f b6 10             	movzbl (%eax),%edx
 3fb:	8b 45 08             	mov    0x8(%ebp),%eax
 3fe:	88 10                	mov    %dl,(%eax)
 400:	8b 45 08             	mov    0x8(%ebp),%eax
 403:	0f b6 00             	movzbl (%eax),%eax
 406:	84 c0                	test   %al,%al
 408:	0f 95 c0             	setne  %al
 40b:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 40f:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
 413:	84 c0                	test   %al,%al
 415:	75 de                	jne    3f5 <strcpy+0xc>
    ;
  return os;
 417:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 41a:	c9                   	leave  
 41b:	c3                   	ret    

0000041c <strcmp>:

int
strcmp(const char *p, const char *q)
{
 41c:	55                   	push   %ebp
 41d:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 41f:	eb 08                	jmp    429 <strcmp+0xd>
    p++, q++;
 421:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 425:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 429:	8b 45 08             	mov    0x8(%ebp),%eax
 42c:	0f b6 00             	movzbl (%eax),%eax
 42f:	84 c0                	test   %al,%al
 431:	74 10                	je     443 <strcmp+0x27>
 433:	8b 45 08             	mov    0x8(%ebp),%eax
 436:	0f b6 10             	movzbl (%eax),%edx
 439:	8b 45 0c             	mov    0xc(%ebp),%eax
 43c:	0f b6 00             	movzbl (%eax),%eax
 43f:	38 c2                	cmp    %al,%dl
 441:	74 de                	je     421 <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 443:	8b 45 08             	mov    0x8(%ebp),%eax
 446:	0f b6 00             	movzbl (%eax),%eax
 449:	0f b6 d0             	movzbl %al,%edx
 44c:	8b 45 0c             	mov    0xc(%ebp),%eax
 44f:	0f b6 00             	movzbl (%eax),%eax
 452:	0f b6 c0             	movzbl %al,%eax
 455:	89 d1                	mov    %edx,%ecx
 457:	29 c1                	sub    %eax,%ecx
 459:	89 c8                	mov    %ecx,%eax
}
 45b:	5d                   	pop    %ebp
 45c:	c3                   	ret    

0000045d <strlen>:

uint
strlen(char *s)
{
 45d:	55                   	push   %ebp
 45e:	89 e5                	mov    %esp,%ebp
 460:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 463:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 46a:	eb 04                	jmp    470 <strlen+0x13>
 46c:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 470:	8b 45 fc             	mov    -0x4(%ebp),%eax
 473:	03 45 08             	add    0x8(%ebp),%eax
 476:	0f b6 00             	movzbl (%eax),%eax
 479:	84 c0                	test   %al,%al
 47b:	75 ef                	jne    46c <strlen+0xf>
    ;
  return n;
 47d:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 480:	c9                   	leave  
 481:	c3                   	ret    

00000482 <memset>:

void*
memset(void *dst, int c, uint n)
{
 482:	55                   	push   %ebp
 483:	89 e5                	mov    %esp,%ebp
 485:	83 ec 0c             	sub    $0xc,%esp
  stosb(dst, c, n);
 488:	8b 45 10             	mov    0x10(%ebp),%eax
 48b:	89 44 24 08          	mov    %eax,0x8(%esp)
 48f:	8b 45 0c             	mov    0xc(%ebp),%eax
 492:	89 44 24 04          	mov    %eax,0x4(%esp)
 496:	8b 45 08             	mov    0x8(%ebp),%eax
 499:	89 04 24             	mov    %eax,(%esp)
 49c:	e8 23 ff ff ff       	call   3c4 <stosb>
  return dst;
 4a1:	8b 45 08             	mov    0x8(%ebp),%eax
}
 4a4:	c9                   	leave  
 4a5:	c3                   	ret    

000004a6 <strchr>:

char*
strchr(const char *s, char c)
{
 4a6:	55                   	push   %ebp
 4a7:	89 e5                	mov    %esp,%ebp
 4a9:	83 ec 04             	sub    $0x4,%esp
 4ac:	8b 45 0c             	mov    0xc(%ebp),%eax
 4af:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 4b2:	eb 14                	jmp    4c8 <strchr+0x22>
    if(*s == c)
 4b4:	8b 45 08             	mov    0x8(%ebp),%eax
 4b7:	0f b6 00             	movzbl (%eax),%eax
 4ba:	3a 45 fc             	cmp    -0x4(%ebp),%al
 4bd:	75 05                	jne    4c4 <strchr+0x1e>
      return (char*)s;
 4bf:	8b 45 08             	mov    0x8(%ebp),%eax
 4c2:	eb 13                	jmp    4d7 <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 4c4:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 4c8:	8b 45 08             	mov    0x8(%ebp),%eax
 4cb:	0f b6 00             	movzbl (%eax),%eax
 4ce:	84 c0                	test   %al,%al
 4d0:	75 e2                	jne    4b4 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 4d2:	b8 00 00 00 00       	mov    $0x0,%eax
}
 4d7:	c9                   	leave  
 4d8:	c3                   	ret    

000004d9 <gets>:

char*
gets(char *buf, int max)
{
 4d9:	55                   	push   %ebp
 4da:	89 e5                	mov    %esp,%ebp
 4dc:	83 ec 28             	sub    $0x28,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 4df:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 4e6:	eb 44                	jmp    52c <gets+0x53>
    cc = read(0, &c, 1);
 4e8:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 4ef:	00 
 4f0:	8d 45 ef             	lea    -0x11(%ebp),%eax
 4f3:	89 44 24 04          	mov    %eax,0x4(%esp)
 4f7:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 4fe:	e8 3d 01 00 00       	call   640 <read>
 503:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(cc < 1)
 506:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 50a:	7e 2d                	jle    539 <gets+0x60>
      break;
    buf[i++] = c;
 50c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 50f:	03 45 08             	add    0x8(%ebp),%eax
 512:	0f b6 55 ef          	movzbl -0x11(%ebp),%edx
 516:	88 10                	mov    %dl,(%eax)
 518:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
    if(c == '\n' || c == '\r')
 51c:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 520:	3c 0a                	cmp    $0xa,%al
 522:	74 16                	je     53a <gets+0x61>
 524:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 528:	3c 0d                	cmp    $0xd,%al
 52a:	74 0e                	je     53a <gets+0x61>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 52c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 52f:	83 c0 01             	add    $0x1,%eax
 532:	3b 45 0c             	cmp    0xc(%ebp),%eax
 535:	7c b1                	jl     4e8 <gets+0xf>
 537:	eb 01                	jmp    53a <gets+0x61>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 539:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 53a:	8b 45 f0             	mov    -0x10(%ebp),%eax
 53d:	03 45 08             	add    0x8(%ebp),%eax
 540:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 543:	8b 45 08             	mov    0x8(%ebp),%eax
}
 546:	c9                   	leave  
 547:	c3                   	ret    

00000548 <stat>:

int
stat(char *n, struct stat *st)
{
 548:	55                   	push   %ebp
 549:	89 e5                	mov    %esp,%ebp
 54b:	83 ec 28             	sub    $0x28,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 54e:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 555:	00 
 556:	8b 45 08             	mov    0x8(%ebp),%eax
 559:	89 04 24             	mov    %eax,(%esp)
 55c:	e8 07 01 00 00       	call   668 <open>
 561:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(fd < 0)
 564:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 568:	79 07                	jns    571 <stat+0x29>
    return -1;
 56a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 56f:	eb 23                	jmp    594 <stat+0x4c>
  r = fstat(fd, st);
 571:	8b 45 0c             	mov    0xc(%ebp),%eax
 574:	89 44 24 04          	mov    %eax,0x4(%esp)
 578:	8b 45 f0             	mov    -0x10(%ebp),%eax
 57b:	89 04 24             	mov    %eax,(%esp)
 57e:	e8 fd 00 00 00       	call   680 <fstat>
 583:	89 45 f4             	mov    %eax,-0xc(%ebp)
  close(fd);
 586:	8b 45 f0             	mov    -0x10(%ebp),%eax
 589:	89 04 24             	mov    %eax,(%esp)
 58c:	e8 bf 00 00 00       	call   650 <close>
  return r;
 591:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
 594:	c9                   	leave  
 595:	c3                   	ret    

00000596 <atoi>:

int
atoi(const char *s)
{
 596:	55                   	push   %ebp
 597:	89 e5                	mov    %esp,%ebp
 599:	83 ec 10             	sub    $0x10,%esp
  int n;

  n = 0;
 59c:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while('0' <= *s && *s <= '9')
 5a3:	eb 24                	jmp    5c9 <atoi+0x33>
    n = n*10 + *s++ - '0';
 5a5:	8b 55 fc             	mov    -0x4(%ebp),%edx
 5a8:	89 d0                	mov    %edx,%eax
 5aa:	c1 e0 02             	shl    $0x2,%eax
 5ad:	01 d0                	add    %edx,%eax
 5af:	01 c0                	add    %eax,%eax
 5b1:	89 c2                	mov    %eax,%edx
 5b3:	8b 45 08             	mov    0x8(%ebp),%eax
 5b6:	0f b6 00             	movzbl (%eax),%eax
 5b9:	0f be c0             	movsbl %al,%eax
 5bc:	8d 04 02             	lea    (%edx,%eax,1),%eax
 5bf:	83 e8 30             	sub    $0x30,%eax
 5c2:	89 45 fc             	mov    %eax,-0x4(%ebp)
 5c5:	83 45 08 01          	addl   $0x1,0x8(%ebp)
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 5c9:	8b 45 08             	mov    0x8(%ebp),%eax
 5cc:	0f b6 00             	movzbl (%eax),%eax
 5cf:	3c 2f                	cmp    $0x2f,%al
 5d1:	7e 0a                	jle    5dd <atoi+0x47>
 5d3:	8b 45 08             	mov    0x8(%ebp),%eax
 5d6:	0f b6 00             	movzbl (%eax),%eax
 5d9:	3c 39                	cmp    $0x39,%al
 5db:	7e c8                	jle    5a5 <atoi+0xf>
    n = n*10 + *s++ - '0';
  return n;
 5dd:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 5e0:	c9                   	leave  
 5e1:	c3                   	ret    

000005e2 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 5e2:	55                   	push   %ebp
 5e3:	89 e5                	mov    %esp,%ebp
 5e5:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 5e8:	8b 45 08             	mov    0x8(%ebp),%eax
 5eb:	89 45 f8             	mov    %eax,-0x8(%ebp)
  src = vsrc;
 5ee:	8b 45 0c             	mov    0xc(%ebp),%eax
 5f1:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0)
 5f4:	eb 13                	jmp    609 <memmove+0x27>
    *dst++ = *src++;
 5f6:	8b 45 fc             	mov    -0x4(%ebp),%eax
 5f9:	0f b6 10             	movzbl (%eax),%edx
 5fc:	8b 45 f8             	mov    -0x8(%ebp),%eax
 5ff:	88 10                	mov    %dl,(%eax)
 601:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
 605:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 609:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
 60d:	0f 9f c0             	setg   %al
 610:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
 614:	84 c0                	test   %al,%al
 616:	75 de                	jne    5f6 <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 618:	8b 45 08             	mov    0x8(%ebp),%eax
}
 61b:	c9                   	leave  
 61c:	c3                   	ret    
 61d:	90                   	nop
 61e:	90                   	nop
 61f:	90                   	nop

00000620 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 620:	b8 01 00 00 00       	mov    $0x1,%eax
 625:	cd 40                	int    $0x40
 627:	c3                   	ret    

00000628 <exit>:
SYSCALL(exit)
 628:	b8 02 00 00 00       	mov    $0x2,%eax
 62d:	cd 40                	int    $0x40
 62f:	c3                   	ret    

00000630 <wait>:
SYSCALL(wait)
 630:	b8 03 00 00 00       	mov    $0x3,%eax
 635:	cd 40                	int    $0x40
 637:	c3                   	ret    

00000638 <pipe>:
SYSCALL(pipe)
 638:	b8 04 00 00 00       	mov    $0x4,%eax
 63d:	cd 40                	int    $0x40
 63f:	c3                   	ret    

00000640 <read>:
SYSCALL(read)
 640:	b8 05 00 00 00       	mov    $0x5,%eax
 645:	cd 40                	int    $0x40
 647:	c3                   	ret    

00000648 <write>:
SYSCALL(write)
 648:	b8 10 00 00 00       	mov    $0x10,%eax
 64d:	cd 40                	int    $0x40
 64f:	c3                   	ret    

00000650 <close>:
SYSCALL(close)
 650:	b8 15 00 00 00       	mov    $0x15,%eax
 655:	cd 40                	int    $0x40
 657:	c3                   	ret    

00000658 <kill>:
SYSCALL(kill)
 658:	b8 06 00 00 00       	mov    $0x6,%eax
 65d:	cd 40                	int    $0x40
 65f:	c3                   	ret    

00000660 <exec>:
SYSCALL(exec)
 660:	b8 07 00 00 00       	mov    $0x7,%eax
 665:	cd 40                	int    $0x40
 667:	c3                   	ret    

00000668 <open>:
SYSCALL(open)
 668:	b8 0f 00 00 00       	mov    $0xf,%eax
 66d:	cd 40                	int    $0x40
 66f:	c3                   	ret    

00000670 <mknod>:
SYSCALL(mknod)
 670:	b8 11 00 00 00       	mov    $0x11,%eax
 675:	cd 40                	int    $0x40
 677:	c3                   	ret    

00000678 <unlink>:
SYSCALL(unlink)
 678:	b8 12 00 00 00       	mov    $0x12,%eax
 67d:	cd 40                	int    $0x40
 67f:	c3                   	ret    

00000680 <fstat>:
SYSCALL(fstat)
 680:	b8 08 00 00 00       	mov    $0x8,%eax
 685:	cd 40                	int    $0x40
 687:	c3                   	ret    

00000688 <link>:
SYSCALL(link)
 688:	b8 13 00 00 00       	mov    $0x13,%eax
 68d:	cd 40                	int    $0x40
 68f:	c3                   	ret    

00000690 <mkdir>:
SYSCALL(mkdir)
 690:	b8 14 00 00 00       	mov    $0x14,%eax
 695:	cd 40                	int    $0x40
 697:	c3                   	ret    

00000698 <chdir>:
SYSCALL(chdir)
 698:	b8 09 00 00 00       	mov    $0x9,%eax
 69d:	cd 40                	int    $0x40
 69f:	c3                   	ret    

000006a0 <dup>:
SYSCALL(dup)
 6a0:	b8 0a 00 00 00       	mov    $0xa,%eax
 6a5:	cd 40                	int    $0x40
 6a7:	c3                   	ret    

000006a8 <getpid>:
SYSCALL(getpid)
 6a8:	b8 0b 00 00 00       	mov    $0xb,%eax
 6ad:	cd 40                	int    $0x40
 6af:	c3                   	ret    

000006b0 <sbrk>:
SYSCALL(sbrk)
 6b0:	b8 0c 00 00 00       	mov    $0xc,%eax
 6b5:	cd 40                	int    $0x40
 6b7:	c3                   	ret    

000006b8 <sleep>:
SYSCALL(sleep)
 6b8:	b8 0d 00 00 00       	mov    $0xd,%eax
 6bd:	cd 40                	int    $0x40
 6bf:	c3                   	ret    

000006c0 <uptime>:
SYSCALL(uptime)
 6c0:	b8 0e 00 00 00       	mov    $0xe,%eax
 6c5:	cd 40                	int    $0x40
 6c7:	c3                   	ret    

000006c8 <start_capture>:
SYSCALL(start_capture)
 6c8:	b8 1e 00 00 00       	mov    $0x1e,%eax
 6cd:	cd 40                	int    $0x40
 6cf:	c3                   	ret    

000006d0 <stop_capture>:
SYSCALL(stop_capture)
 6d0:	b8 1f 00 00 00       	mov    $0x1f,%eax
 6d5:	cd 40                	int    $0x40
 6d7:	c3                   	ret    

000006d8 <switch_scheduler>:
SYSCALL(switch_scheduler)
 6d8:	b8 20 00 00 00       	mov    $0x20,%eax
 6dd:	cd 40                	int    $0x40
 6df:	c3                   	ret    

000006e0 <syscallstats>:
SYSCALL(syscallstats)
 6e0:	b8 21 00 00 00       	mov    $0x21,%eax
 6e5:	cd 40                	int    $0x40
 6e7:	c3                   	ret    

000006e8 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 6e8:	55                   	push   %ebp
 6e9:	89 e5                	mov    %esp,%ebp
 6eb:	83 ec 28             	sub    $0x28,%esp
 6ee:	8b 45 0c             	mov    0xc(%ebp),%eax
 6f1:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 6f4:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 6fb:	00 
 6fc:	8d 45 f4             	lea    -0xc(%ebp),%eax
 6ff:	89 44 24 04          	mov    %eax,0x4(%esp)
 703:	8b 45 08             	mov    0x8(%ebp),%eax
 706:	89 04 24             	mov    %eax,(%esp)
 709:	e8 3a ff ff ff       	call   648 <write>
}
 70e:	c9                   	leave  
 70f:	c3                   	ret    

00000710 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 710:	55                   	push   %ebp
 711:	89 e5                	mov    %esp,%ebp
 713:	53                   	push   %ebx
 714:	83 ec 44             	sub    $0x44,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 717:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 71e:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 722:	74 17                	je     73b <printint+0x2b>
 724:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 728:	79 11                	jns    73b <printint+0x2b>
    neg = 1;
 72a:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 731:	8b 45 0c             	mov    0xc(%ebp),%eax
 734:	f7 d8                	neg    %eax
 736:	89 45 f4             	mov    %eax,-0xc(%ebp)
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
 739:	eb 06                	jmp    741 <printint+0x31>
    neg = 1;
    x = -xx;
  } else {
    x = xx;
 73b:	8b 45 0c             	mov    0xc(%ebp),%eax
 73e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  }

  i = 0;
 741:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  do{
    buf[i++] = digits[x % base];
 748:	8b 4d ec             	mov    -0x14(%ebp),%ecx
 74b:	8b 5d 10             	mov    0x10(%ebp),%ebx
 74e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 751:	ba 00 00 00 00       	mov    $0x0,%edx
 756:	f7 f3                	div    %ebx
 758:	89 d0                	mov    %edx,%eax
 75a:	0f b6 80 f8 0b 00 00 	movzbl 0xbf8(%eax),%eax
 761:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
 765:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
  }while((x /= base) != 0);
 769:	8b 45 10             	mov    0x10(%ebp),%eax
 76c:	89 45 d4             	mov    %eax,-0x2c(%ebp)
 76f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 772:	ba 00 00 00 00       	mov    $0x0,%edx
 777:	f7 75 d4             	divl   -0x2c(%ebp)
 77a:	89 45 f4             	mov    %eax,-0xc(%ebp)
 77d:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 781:	75 c5                	jne    748 <printint+0x38>
  if(neg)
 783:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 787:	74 2a                	je     7b3 <printint+0xa3>
    buf[i++] = '-';
 789:	8b 45 ec             	mov    -0x14(%ebp),%eax
 78c:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)
 791:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)

  while(--i >= 0)
 795:	eb 1d                	jmp    7b4 <printint+0xa4>
    putc(fd, buf[i]);
 797:	8b 45 ec             	mov    -0x14(%ebp),%eax
 79a:	0f b6 44 05 dc       	movzbl -0x24(%ebp,%eax,1),%eax
 79f:	0f be c0             	movsbl %al,%eax
 7a2:	89 44 24 04          	mov    %eax,0x4(%esp)
 7a6:	8b 45 08             	mov    0x8(%ebp),%eax
 7a9:	89 04 24             	mov    %eax,(%esp)
 7ac:	e8 37 ff ff ff       	call   6e8 <putc>
 7b1:	eb 01                	jmp    7b4 <printint+0xa4>
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 7b3:	90                   	nop
 7b4:	83 6d ec 01          	subl   $0x1,-0x14(%ebp)
 7b8:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 7bc:	79 d9                	jns    797 <printint+0x87>
    putc(fd, buf[i]);
}
 7be:	83 c4 44             	add    $0x44,%esp
 7c1:	5b                   	pop    %ebx
 7c2:	5d                   	pop    %ebp
 7c3:	c3                   	ret    

000007c4 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 7c4:	55                   	push   %ebp
 7c5:	89 e5                	mov    %esp,%ebp
 7c7:	83 ec 38             	sub    $0x38,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 7ca:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 7d1:	8d 45 0c             	lea    0xc(%ebp),%eax
 7d4:	83 c0 04             	add    $0x4,%eax
 7d7:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(i = 0; fmt[i]; i++){
 7da:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 7e1:	e9 7e 01 00 00       	jmp    964 <printf+0x1a0>
    c = fmt[i] & 0xff;
 7e6:	8b 55 0c             	mov    0xc(%ebp),%edx
 7e9:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7ec:	8d 04 02             	lea    (%edx,%eax,1),%eax
 7ef:	0f b6 00             	movzbl (%eax),%eax
 7f2:	0f be c0             	movsbl %al,%eax
 7f5:	25 ff 00 00 00       	and    $0xff,%eax
 7fa:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(state == 0){
 7fd:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 801:	75 2c                	jne    82f <printf+0x6b>
      if(c == '%'){
 803:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 807:	75 0c                	jne    815 <printf+0x51>
        state = '%';
 809:	c7 45 f0 25 00 00 00 	movl   $0x25,-0x10(%ebp)
 810:	e9 4b 01 00 00       	jmp    960 <printf+0x19c>
      } else {
        putc(fd, c);
 815:	8b 45 e8             	mov    -0x18(%ebp),%eax
 818:	0f be c0             	movsbl %al,%eax
 81b:	89 44 24 04          	mov    %eax,0x4(%esp)
 81f:	8b 45 08             	mov    0x8(%ebp),%eax
 822:	89 04 24             	mov    %eax,(%esp)
 825:	e8 be fe ff ff       	call   6e8 <putc>
 82a:	e9 31 01 00 00       	jmp    960 <printf+0x19c>
      }
    } else if(state == '%'){
 82f:	83 7d f0 25          	cmpl   $0x25,-0x10(%ebp)
 833:	0f 85 27 01 00 00    	jne    960 <printf+0x19c>
      if(c == 'd'){
 839:	83 7d e8 64          	cmpl   $0x64,-0x18(%ebp)
 83d:	75 2d                	jne    86c <printf+0xa8>
        printint(fd, *ap, 10, 1);
 83f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 842:	8b 00                	mov    (%eax),%eax
 844:	c7 44 24 0c 01 00 00 	movl   $0x1,0xc(%esp)
 84b:	00 
 84c:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
 853:	00 
 854:	89 44 24 04          	mov    %eax,0x4(%esp)
 858:	8b 45 08             	mov    0x8(%ebp),%eax
 85b:	89 04 24             	mov    %eax,(%esp)
 85e:	e8 ad fe ff ff       	call   710 <printint>
        ap++;
 863:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 867:	e9 ed 00 00 00       	jmp    959 <printf+0x195>
      } else if(c == 'x' || c == 'p'){
 86c:	83 7d e8 78          	cmpl   $0x78,-0x18(%ebp)
 870:	74 06                	je     878 <printf+0xb4>
 872:	83 7d e8 70          	cmpl   $0x70,-0x18(%ebp)
 876:	75 2d                	jne    8a5 <printf+0xe1>
        printint(fd, *ap, 16, 0);
 878:	8b 45 f4             	mov    -0xc(%ebp),%eax
 87b:	8b 00                	mov    (%eax),%eax
 87d:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
 884:	00 
 885:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
 88c:	00 
 88d:	89 44 24 04          	mov    %eax,0x4(%esp)
 891:	8b 45 08             	mov    0x8(%ebp),%eax
 894:	89 04 24             	mov    %eax,(%esp)
 897:	e8 74 fe ff ff       	call   710 <printint>
        ap++;
 89c:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
      }
    } else if(state == '%'){
      if(c == 'd'){
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 8a0:	e9 b4 00 00 00       	jmp    959 <printf+0x195>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 8a5:	83 7d e8 73          	cmpl   $0x73,-0x18(%ebp)
 8a9:	75 46                	jne    8f1 <printf+0x12d>
        s = (char*)*ap;
 8ab:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8ae:	8b 00                	mov    (%eax),%eax
 8b0:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        ap++;
 8b3:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
        if(s == 0)
 8b7:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 8bb:	75 27                	jne    8e4 <printf+0x120>
          s = "(null)";
 8bd:	c7 45 e4 ef 0b 00 00 	movl   $0xbef,-0x1c(%ebp)
        while(*s != 0){
 8c4:	eb 1f                	jmp    8e5 <printf+0x121>
          putc(fd, *s);
 8c6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 8c9:	0f b6 00             	movzbl (%eax),%eax
 8cc:	0f be c0             	movsbl %al,%eax
 8cf:	89 44 24 04          	mov    %eax,0x4(%esp)
 8d3:	8b 45 08             	mov    0x8(%ebp),%eax
 8d6:	89 04 24             	mov    %eax,(%esp)
 8d9:	e8 0a fe ff ff       	call   6e8 <putc>
          s++;
 8de:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
 8e2:	eb 01                	jmp    8e5 <printf+0x121>
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 8e4:	90                   	nop
 8e5:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 8e8:	0f b6 00             	movzbl (%eax),%eax
 8eb:	84 c0                	test   %al,%al
 8ed:	75 d7                	jne    8c6 <printf+0x102>
 8ef:	eb 68                	jmp    959 <printf+0x195>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 8f1:	83 7d e8 63          	cmpl   $0x63,-0x18(%ebp)
 8f5:	75 1d                	jne    914 <printf+0x150>
        putc(fd, *ap);
 8f7:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8fa:	8b 00                	mov    (%eax),%eax
 8fc:	0f be c0             	movsbl %al,%eax
 8ff:	89 44 24 04          	mov    %eax,0x4(%esp)
 903:	8b 45 08             	mov    0x8(%ebp),%eax
 906:	89 04 24             	mov    %eax,(%esp)
 909:	e8 da fd ff ff       	call   6e8 <putc>
        ap++;
 90e:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 912:	eb 45                	jmp    959 <printf+0x195>
      } else if(c == '%'){
 914:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 918:	75 17                	jne    931 <printf+0x16d>
        putc(fd, c);
 91a:	8b 45 e8             	mov    -0x18(%ebp),%eax
 91d:	0f be c0             	movsbl %al,%eax
 920:	89 44 24 04          	mov    %eax,0x4(%esp)
 924:	8b 45 08             	mov    0x8(%ebp),%eax
 927:	89 04 24             	mov    %eax,(%esp)
 92a:	e8 b9 fd ff ff       	call   6e8 <putc>
 92f:	eb 28                	jmp    959 <printf+0x195>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 931:	c7 44 24 04 25 00 00 	movl   $0x25,0x4(%esp)
 938:	00 
 939:	8b 45 08             	mov    0x8(%ebp),%eax
 93c:	89 04 24             	mov    %eax,(%esp)
 93f:	e8 a4 fd ff ff       	call   6e8 <putc>
        putc(fd, c);
 944:	8b 45 e8             	mov    -0x18(%ebp),%eax
 947:	0f be c0             	movsbl %al,%eax
 94a:	89 44 24 04          	mov    %eax,0x4(%esp)
 94e:	8b 45 08             	mov    0x8(%ebp),%eax
 951:	89 04 24             	mov    %eax,(%esp)
 954:	e8 8f fd ff ff       	call   6e8 <putc>
      }
      state = 0;
 959:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 960:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
 964:	8b 55 0c             	mov    0xc(%ebp),%edx
 967:	8b 45 ec             	mov    -0x14(%ebp),%eax
 96a:	8d 04 02             	lea    (%edx,%eax,1),%eax
 96d:	0f b6 00             	movzbl (%eax),%eax
 970:	84 c0                	test   %al,%al
 972:	0f 85 6e fe ff ff    	jne    7e6 <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 978:	c9                   	leave  
 979:	c3                   	ret    
 97a:	90                   	nop
 97b:	90                   	nop

0000097c <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 97c:	55                   	push   %ebp
 97d:	89 e5                	mov    %esp,%ebp
 97f:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 982:	8b 45 08             	mov    0x8(%ebp),%eax
 985:	83 e8 08             	sub    $0x8,%eax
 988:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 98b:	a1 14 0c 00 00       	mov    0xc14,%eax
 990:	89 45 fc             	mov    %eax,-0x4(%ebp)
 993:	eb 24                	jmp    9b9 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 995:	8b 45 fc             	mov    -0x4(%ebp),%eax
 998:	8b 00                	mov    (%eax),%eax
 99a:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 99d:	77 12                	ja     9b1 <free+0x35>
 99f:	8b 45 f8             	mov    -0x8(%ebp),%eax
 9a2:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 9a5:	77 24                	ja     9cb <free+0x4f>
 9a7:	8b 45 fc             	mov    -0x4(%ebp),%eax
 9aa:	8b 00                	mov    (%eax),%eax
 9ac:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 9af:	77 1a                	ja     9cb <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 9b1:	8b 45 fc             	mov    -0x4(%ebp),%eax
 9b4:	8b 00                	mov    (%eax),%eax
 9b6:	89 45 fc             	mov    %eax,-0x4(%ebp)
 9b9:	8b 45 f8             	mov    -0x8(%ebp),%eax
 9bc:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 9bf:	76 d4                	jbe    995 <free+0x19>
 9c1:	8b 45 fc             	mov    -0x4(%ebp),%eax
 9c4:	8b 00                	mov    (%eax),%eax
 9c6:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 9c9:	76 ca                	jbe    995 <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 9cb:	8b 45 f8             	mov    -0x8(%ebp),%eax
 9ce:	8b 40 04             	mov    0x4(%eax),%eax
 9d1:	c1 e0 03             	shl    $0x3,%eax
 9d4:	89 c2                	mov    %eax,%edx
 9d6:	03 55 f8             	add    -0x8(%ebp),%edx
 9d9:	8b 45 fc             	mov    -0x4(%ebp),%eax
 9dc:	8b 00                	mov    (%eax),%eax
 9de:	39 c2                	cmp    %eax,%edx
 9e0:	75 24                	jne    a06 <free+0x8a>
    bp->s.size += p->s.ptr->s.size;
 9e2:	8b 45 f8             	mov    -0x8(%ebp),%eax
 9e5:	8b 50 04             	mov    0x4(%eax),%edx
 9e8:	8b 45 fc             	mov    -0x4(%ebp),%eax
 9eb:	8b 00                	mov    (%eax),%eax
 9ed:	8b 40 04             	mov    0x4(%eax),%eax
 9f0:	01 c2                	add    %eax,%edx
 9f2:	8b 45 f8             	mov    -0x8(%ebp),%eax
 9f5:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 9f8:	8b 45 fc             	mov    -0x4(%ebp),%eax
 9fb:	8b 00                	mov    (%eax),%eax
 9fd:	8b 10                	mov    (%eax),%edx
 9ff:	8b 45 f8             	mov    -0x8(%ebp),%eax
 a02:	89 10                	mov    %edx,(%eax)
 a04:	eb 0a                	jmp    a10 <free+0x94>
  } else
    bp->s.ptr = p->s.ptr;
 a06:	8b 45 fc             	mov    -0x4(%ebp),%eax
 a09:	8b 10                	mov    (%eax),%edx
 a0b:	8b 45 f8             	mov    -0x8(%ebp),%eax
 a0e:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 a10:	8b 45 fc             	mov    -0x4(%ebp),%eax
 a13:	8b 40 04             	mov    0x4(%eax),%eax
 a16:	c1 e0 03             	shl    $0x3,%eax
 a19:	03 45 fc             	add    -0x4(%ebp),%eax
 a1c:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 a1f:	75 20                	jne    a41 <free+0xc5>
    p->s.size += bp->s.size;
 a21:	8b 45 fc             	mov    -0x4(%ebp),%eax
 a24:	8b 50 04             	mov    0x4(%eax),%edx
 a27:	8b 45 f8             	mov    -0x8(%ebp),%eax
 a2a:	8b 40 04             	mov    0x4(%eax),%eax
 a2d:	01 c2                	add    %eax,%edx
 a2f:	8b 45 fc             	mov    -0x4(%ebp),%eax
 a32:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 a35:	8b 45 f8             	mov    -0x8(%ebp),%eax
 a38:	8b 10                	mov    (%eax),%edx
 a3a:	8b 45 fc             	mov    -0x4(%ebp),%eax
 a3d:	89 10                	mov    %edx,(%eax)
 a3f:	eb 08                	jmp    a49 <free+0xcd>
  } else
    p->s.ptr = bp;
 a41:	8b 45 fc             	mov    -0x4(%ebp),%eax
 a44:	8b 55 f8             	mov    -0x8(%ebp),%edx
 a47:	89 10                	mov    %edx,(%eax)
  freep = p;
 a49:	8b 45 fc             	mov    -0x4(%ebp),%eax
 a4c:	a3 14 0c 00 00       	mov    %eax,0xc14
}
 a51:	c9                   	leave  
 a52:	c3                   	ret    

00000a53 <morecore>:

static Header*
morecore(uint nu)
{
 a53:	55                   	push   %ebp
 a54:	89 e5                	mov    %esp,%ebp
 a56:	83 ec 28             	sub    $0x28,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 a59:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 a60:	77 07                	ja     a69 <morecore+0x16>
    nu = 4096;
 a62:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 a69:	8b 45 08             	mov    0x8(%ebp),%eax
 a6c:	c1 e0 03             	shl    $0x3,%eax
 a6f:	89 04 24             	mov    %eax,(%esp)
 a72:	e8 39 fc ff ff       	call   6b0 <sbrk>
 a77:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(p == (char*)-1)
 a7a:	83 7d f0 ff          	cmpl   $0xffffffff,-0x10(%ebp)
 a7e:	75 07                	jne    a87 <morecore+0x34>
    return 0;
 a80:	b8 00 00 00 00       	mov    $0x0,%eax
 a85:	eb 22                	jmp    aa9 <morecore+0x56>
  hp = (Header*)p;
 a87:	8b 45 f0             	mov    -0x10(%ebp),%eax
 a8a:	89 45 f4             	mov    %eax,-0xc(%ebp)
  hp->s.size = nu;
 a8d:	8b 45 f4             	mov    -0xc(%ebp),%eax
 a90:	8b 55 08             	mov    0x8(%ebp),%edx
 a93:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 a96:	8b 45 f4             	mov    -0xc(%ebp),%eax
 a99:	83 c0 08             	add    $0x8,%eax
 a9c:	89 04 24             	mov    %eax,(%esp)
 a9f:	e8 d8 fe ff ff       	call   97c <free>
  return freep;
 aa4:	a1 14 0c 00 00       	mov    0xc14,%eax
}
 aa9:	c9                   	leave  
 aaa:	c3                   	ret    

00000aab <malloc>:

void*
malloc(uint nbytes)
{
 aab:	55                   	push   %ebp
 aac:	89 e5                	mov    %esp,%ebp
 aae:	83 ec 28             	sub    $0x28,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 ab1:	8b 45 08             	mov    0x8(%ebp),%eax
 ab4:	83 c0 07             	add    $0x7,%eax
 ab7:	c1 e8 03             	shr    $0x3,%eax
 aba:	83 c0 01             	add    $0x1,%eax
 abd:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((prevp = freep) == 0){
 ac0:	a1 14 0c 00 00       	mov    0xc14,%eax
 ac5:	89 45 f0             	mov    %eax,-0x10(%ebp)
 ac8:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 acc:	75 23                	jne    af1 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 ace:	c7 45 f0 0c 0c 00 00 	movl   $0xc0c,-0x10(%ebp)
 ad5:	8b 45 f0             	mov    -0x10(%ebp),%eax
 ad8:	a3 14 0c 00 00       	mov    %eax,0xc14
 add:	a1 14 0c 00 00       	mov    0xc14,%eax
 ae2:	a3 0c 0c 00 00       	mov    %eax,0xc0c
    base.s.size = 0;
 ae7:	c7 05 10 0c 00 00 00 	movl   $0x0,0xc10
 aee:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 af1:	8b 45 f0             	mov    -0x10(%ebp),%eax
 af4:	8b 00                	mov    (%eax),%eax
 af6:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(p->s.size >= nunits){
 af9:	8b 45 ec             	mov    -0x14(%ebp),%eax
 afc:	8b 40 04             	mov    0x4(%eax),%eax
 aff:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 b02:	72 4d                	jb     b51 <malloc+0xa6>
      if(p->s.size == nunits)
 b04:	8b 45 ec             	mov    -0x14(%ebp),%eax
 b07:	8b 40 04             	mov    0x4(%eax),%eax
 b0a:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 b0d:	75 0c                	jne    b1b <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 b0f:	8b 45 ec             	mov    -0x14(%ebp),%eax
 b12:	8b 10                	mov    (%eax),%edx
 b14:	8b 45 f0             	mov    -0x10(%ebp),%eax
 b17:	89 10                	mov    %edx,(%eax)
 b19:	eb 26                	jmp    b41 <malloc+0x96>
      else {
        p->s.size -= nunits;
 b1b:	8b 45 ec             	mov    -0x14(%ebp),%eax
 b1e:	8b 40 04             	mov    0x4(%eax),%eax
 b21:	89 c2                	mov    %eax,%edx
 b23:	2b 55 f4             	sub    -0xc(%ebp),%edx
 b26:	8b 45 ec             	mov    -0x14(%ebp),%eax
 b29:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 b2c:	8b 45 ec             	mov    -0x14(%ebp),%eax
 b2f:	8b 40 04             	mov    0x4(%eax),%eax
 b32:	c1 e0 03             	shl    $0x3,%eax
 b35:	01 45 ec             	add    %eax,-0x14(%ebp)
        p->s.size = nunits;
 b38:	8b 45 ec             	mov    -0x14(%ebp),%eax
 b3b:	8b 55 f4             	mov    -0xc(%ebp),%edx
 b3e:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 b41:	8b 45 f0             	mov    -0x10(%ebp),%eax
 b44:	a3 14 0c 00 00       	mov    %eax,0xc14
      return (void*)(p + 1);
 b49:	8b 45 ec             	mov    -0x14(%ebp),%eax
 b4c:	83 c0 08             	add    $0x8,%eax
 b4f:	eb 38                	jmp    b89 <malloc+0xde>
    }
    if(p == freep)
 b51:	a1 14 0c 00 00       	mov    0xc14,%eax
 b56:	39 45 ec             	cmp    %eax,-0x14(%ebp)
 b59:	75 1b                	jne    b76 <malloc+0xcb>
      if((p = morecore(nunits)) == 0)
 b5b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 b5e:	89 04 24             	mov    %eax,(%esp)
 b61:	e8 ed fe ff ff       	call   a53 <morecore>
 b66:	89 45 ec             	mov    %eax,-0x14(%ebp)
 b69:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 b6d:	75 07                	jne    b76 <malloc+0xcb>
        return 0;
 b6f:	b8 00 00 00 00       	mov    $0x0,%eax
 b74:	eb 13                	jmp    b89 <malloc+0xde>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 b76:	8b 45 ec             	mov    -0x14(%ebp),%eax
 b79:	89 45 f0             	mov    %eax,-0x10(%ebp)
 b7c:	8b 45 ec             	mov    -0x14(%ebp),%eax
 b7f:	8b 00                	mov    (%eax),%eax
 b81:	89 45 ec             	mov    %eax,-0x14(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 b84:	e9 70 ff ff ff       	jmp    af9 <malloc+0x4e>
}
 b89:	c9                   	leave  
 b8a:	c3                   	ret    
